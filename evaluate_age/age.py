#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 14:52:24 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob
import numpy as np
import pandas as pd

from scipy.interpolate import interp1d
from scipy.interpolate import griddata, LinearNDInterpolator
from scipy import integrate

from statistics import mode
from itertools import product
import random

import pyarrow.feather as feather
from tqdm import tqdm

from python_modules.numerical_tools_module import *

R_J = 69911000
M_J = 1.898*1e27
R_s = 696340000
M_s = 1.989*1e30
R_J = 69911000
M_J = 1.898*1e27
R_E = 6371*1e3
M_E = 5.972*1e24
G_u = 6.674*1e-11
Na = 6.022*1e23
Kb = 1.380649*1e-23
sigma = 5.67*1e-8

def get_mass(x):
    return float(x.split('/')[-1].split('_')[0].replace('.ftr',''))

def get_met(x):
    return float(x.split('/')[-1].split('_')[1].replace('.ftr',''))

def get_core(x):
    return float(x.split('/')[-1].split('_')[2].replace('.ftr',''))

if __name__ == "__main__" : 
    print ('In code')

    Mass = 1
    T_ints = np.arange(0,1000,50)
    T_irrs = np.arange(0,1000,100)
    Met = -1
    core = 10

    files = glob.glob('../retrieve_spectrum/data/current_exoevol_grid/ker/*_*_*')
    print(files)

    df = pd.DataFrame()
    df['files'] = files
    df['Mass'] = df['files'].apply(get_mass)
    df['Met'] = df['files'].apply(get_met)
    df['Core'] = df['files'].apply(get_core)

    df = df[(df['Mass'] >= 1) & (df['Mass'] <= (5))]
    df = df[(np.log10(df['Met']) >= (Met - 1)) & (np.log10(df['Met']) <= (Met + 1))]
    df = df[(np.log10(df['Core']) > (core - 40)) & (np.log10(df['Core']) < (core + 40))]
        
    test = []
    for file in tqdm(df['files']):
        test.append(feather.read_feather(file))

    test = pd.concat(test,ignore_index=True)
    
    test = test[test['T_irr']<400]

    init_S = pd.read_csv('./hot_start.csv')
    f_init_S = interp1d(init_S['M'],init_S['S'],fill_value='extrapolate')

    ds_dt = []
    L_int = []

    for ii in tqdm(range(0,len(test))):
        rho = np.array(test['rho_profile'].iloc[ii])
        T = np.array(test['T_profile'].iloc[ii])
        R = np.array(test['R_profile'].iloc[ii])
        R_p = test['Radius_Jup'].iloc[ii]*R_J
        T_int = test['T_int'].iloc[ii]
        to_int = rho*T*R**2
        integral = -integrate.simpson(to_int, R)
        if integral > 0:
            #print(integral)
            L_int = sigma*R_p**2*T_int**4
            ds_dt.append(-L_int/integral)
        else :
            ds_dt.append(np.nan)
        
    test['ds_dt'] = ds_dt
    test = test[test['ds_dt'].notnull()]
    print(test)
    
    f_ds_dt = LinearNDInterpolator(list(zip(test['Mass_Jup'],test['T_int'],test['T_irr'],np.log10(test['Met']),test['core_Earth'])),test['ds_dt'],rescale=True)
    print('ds_dt interpolator made')
    f_s = LinearNDInterpolator(list(zip(test['Mass_Jup'],test['T_int'],test['T_irr'],np.log10(test['Met']),test['core_Earth'])),test['S'],rescale=True)
    print('s interpolator made')
    f_r = LinearNDInterpolator(list(zip(test['Mass_Jup'],test['T_int'],test['T_irr'],np.log10(test['Met']),test['core_Earth'])),test['Radius_Jup'],rescale=True)
    print('r interpolator made')
    
    with open('./evol_interp.pkl', 'wb') as f:
        pkl.dump(f_ds_dt, f)
        
    with open('./entropy_interp.pkl', 'wb') as f:
        pkl.dump(f_s, f)
        
    with open('./radius_interp.pkl', 'wb') as f:
        pkl.dump(f_r, f)

    
    Mets = np.sort(list(set(test['Met'])))
    cores = np.sort(list(set(test['core_Earth'])))
    Masses = np.sort(list(set(test['Mass_Jup'])))
    T_irrs = np.sort(list(set(test['T_irr'])))
    
    parameters = list(set(list(product(Mets,cores,Masses,T_irrs))))

    df = pd.DataFrame()
    for parameter in tqdm(parameters) :
        Met, core, Mass, T_irr = parameter
        print(Met)
        print(core)
        print(Mass)
        print(T_irr)
        
        entropy_at_0 = f_init_S(Mass)
        print(entropy_at_0)
        S0 = (Kb*entropy_at_0/(1.6735*1e-27))
        
        df_temp = pd.DataFrame()
        df_temp['T_int'] = T_ints
        df_temp['T_irr'] = T_irr
        #df_temp['ds_dt'] = np.minimum.accumulate(f_ds_dt(Mass,T_ints,T_irr,Met))
        #df_temp['S'] = np.maximum.accumulate(f_s(Mass,T_ints,T_irr,Met))
        df_temp['ds_dt'] = f_ds_dt(Mass,T_ints,T_irr,np.log10(Met),core)
        df_temp['S'] = f_s(Mass,T_ints,T_irr,np.log10(Met),core)
        df_temp['Radius_Jup'] = f_r(Mass,T_ints,T_irr,np.log10(Met),core)
        df_temp['Met'] = Met
        df_temp['Mass_Jup'] = Mass
        df_temp['core_Earth'] = core

        df_init = pd.DataFrame({'ds_dt':[df_temp['ds_dt'].min()],'S':[S0]})
        df_temp = df_temp.append(df_init)
        df_temp = df_temp.sort_values('S',ascending=False)

        f_S_dsdt = interp1d(df_temp['S'],1/df_temp['ds_dt'],fill_value="extrapolate")
        S, t = rk4_solver(f=f_S_dsdt, x_vals=list(df_temp['S']), y0=0)
        
        df_temp['t'] = t/(24*3600*365)
        
        print(df_temp)
        print(len(list(set(df_temp['Met']))))
        print(list(set(df_temp['Met'])))
        print(len(list(set(df_temp['T_irr']))))
        print(list(set(df_temp['T_irr'])))
        print(len(list(set(df_temp['core_Earth']))))
        print(list(set(df_temp['core_Earth'])))
        
        df_temp = df_temp[df_temp['t']<1e14]
        df_temp = df_temp[df_temp['t']>0]
        df_temp = df_temp[df_temp['t'].notnull()]
        df_temp = df_temp[df_temp['ds_dt'].notnull()]
        df_temp = df_temp[df_temp['S'].notnull()]
        df_temp = df_temp[df_temp['T_irr'].notnull()]
        
        print(df_temp)
        print(len(list(set(df_temp['Met']))))
        print(list(set(df_temp['Met'])))
        print(len(list(set(df_temp['T_irr']))))
        print(list(set(df_temp['T_irr'])))
        print(len(list(set(df_temp['core_Earth']))))
        print(list(set(df_temp['core_Earth'])))
        
        df = df.append(df_temp)
        

    #df = df.sort_values(by='t')
    print(df)
    df = df.iloc[::50]
    
    f_t = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_int'],df['T_irr'],np.log10(df['Met']),df['core_Earth'])),df['t'],rescale=True)
    print('t interpolator made')

    with open('./Age_interp.pkl', 'wb') as f:
        pkl.dump(f_t, f)