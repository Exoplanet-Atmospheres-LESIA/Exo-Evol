"""
Created on Unknown

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob

from python_modules.interface_module import *

import warnings
warnings.filterwarnings("ignore")

def main() :
    slurm_files = glob.glob('../slurm_files/*.slurm')
    for file in slurm_files :
        os.remove(file)

    os.system('rm -rf ./../temp/running_exoris')
    os.system('mkdir ./../temp/running_exoris')

    os.system('rm -rf ./../temp/running_exorem')
    os.system('mkdir ./../temp/running_exorem')
    
    os.system('rm -rf ./../slurm_files')
    os.system('mkdir ./../slurm_files')

    #os.system('rm -rf ../outputs')
    #os.system('mkdir ../outputs')
    
    n_tasks = input_cluster('n_tasks')
    n_jobs = input_cluster('n_jobs')

    job = 0
    for task in range(0,n_tasks) :
        print('task {}'.format(task))
        file_name = '../slurm_files/{}.slurm'.format(task)
        with open(file_name, 'a') as slurm_file:
            for job in range(0,n_jobs):
                if job == 0 :
                    header = '#!/bin/sh\n#SBATCH --job-name=exo_evol_iterative\n#SBATCH --time=1-6:00:00  \n#SBATCH --mail-user=christian.wilkinson@obspm.fr\n#SBATCH --mail-type=begin        # send email when job begins\n#SBATCH --mail-type=end          # send email when job ends\n#SBATCH --mail-type=fail         # send email if job fails\n#SBATCH --clusters=astro_thin\n#SBATCH --partition=def\n#SBATCH --qos=astro_thin_def_long\n#SBATCH --account=cwilkinson\n#SBATCH --nodes=1 \n#SBATCH --ntasks-per-node=24\n\n\nRUNDIR=./.${SLURM_JOB_ID}\n\n'
                    #header = '#!/bin/sh\n#SBATCH --job-name=exo_evol_iterative\n#SBATCH --time=0-12:00:00  \n#SBATCH --mail-user=christian.wilkinson@obspm.fr\n#SBATCH --mail-type=begin        # send email when job begins\n#SBATCH --mail-type=end          # send email when job ends\n#SBATCH --mail-type=fail         # send email if job fails\n#SBATCH --clusters=astro_thin\n#SBATCH --partition=hi\n#SBATCH --qos=astro_thin_hi_normal\n#SBATCH --account=cwilkinson_hi\n#SBATCH --nodes=1 \n#SBATCH --ntasks-per-node=24\n\n\nRUNDIR=./.${SLURM_JOB_ID}\n\n'            

                    line = '\nsrun -u --exclusive --ntasks 1 python3 ../main/create_grid.py {} {}'\
                        ' > ../temp/exo_evol_planet_{}_{}.txt &'.format(task,job+1,task,job+1)
                    slurm_file.write(header)
                    slurm_file.write(line)
                                
                if (job < n_jobs) & (job > 0) :
                    line = '\nsrun -u --exclusive --ntasks 1 python3 ../main/create_grid.py {} {}'\
                        ' > ../temp/outputs/exo_evol_planet_{}_{}.txt &'.format(task,job+1,task,job+1)
                    slurm_file.write(line)

                if (job%24==0) and (job>0) and (job<n_jobs):
                    line = '\nwait'
                    slurm_file.write(line)

            line = '\nwait\nexit 0'
            slurm_file.write(line)

if __name__  ==  "__main__":
    main()