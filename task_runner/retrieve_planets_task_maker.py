"""
Created on Unknown

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob

from python_modules.interface_module import *
from python_modules.interface_module import *

import warnings
warnings.filterwarnings("ignore")

def main() :
    slurm_files = glob.glob('../slurm_files/*.slurm')
    for file in slurm_files :
        os.remove(file)
    
    spectra = glob.glob('{}/*.txt'.format(retrieval_paths('input_spectrum_list')))
    
    spectrum_list = bool(retrieval_parameters('spectrum_list'))
    if spectrum_list :
        n_tasks = len(spectra)
    else :
        n_tasks = 1

    jobs = 1
    n_tasks = n_tasks//jobs
    idx = 0
    for task, spectrum in zip(range(0,n_tasks),spectra) :
        print('task {}'.format(task))
        file_name = '../slurm_files/retrieve_{}.slurm'.format(task)
        with open(file_name, 'a') as slurm_file :
            header = '#!/bin/sh\n#SBATCH --job-name=spectral_retrieval_'+'{}_{}'.format(task,idx)+'#SBATCH --time=0-05:00:00  \n#SBATCH --mail-user=christian.wilkinson@obspm.fr\n#SBATCH --mail-type=begin        # send email when job begins\n#SBATCH --mail-type=end          # send email when job ends\n#SBATCH --mail-type=fail         # send email if job fails\n#SBATCH --clusters=astro_thin\n#SBATCH --partition=def\n#SBATCH --qos=astro_thin_def_long\n#SBATCH --account=cwilkinson\n#SBATCH --nodes=1 \n#SBATCH --ntasks-per-node=24\n\n\nRUNDIR=./.${SLURM_JOB_ID}\n\n'
            #header = '#!/bin/sh\n#SBATCH --job-name=spectral_retrieval_'+'{}_{}'.format(task,idx)+'#SBATCH --time=0-05:00:00  \n#SBATCH --mail-user=christian.wilkinson@obspm.fr\n#SBATCH --mail-type=begin        # send email when job begins\n#SBATCH --mail-type=end          # send email when job ends\n#SBATCH --mail-type=fail         # send email if job fails\n#SBATCH --clusters=astro_thin\n#SBATCH --partition=hi\n#SBATCH --qos=astro_thin_hi_normal\n#SBATCH --account=cwilkinson_hi\n#SBATCH --nodes=1 \n#SBATCH --ntasks-per-node=24\n\n\nRUNDIR=./.${SLURM_JOB_ID}\n\n'            
            slurm_file.write(header)
            for job in range(jobs) :
                line = '\nsrun -u --ntasks 1 python3 ../main/spectrum_retrieval.py {}'\
                    ' > ../temp/outputs/spectrum_retrieval_{}.txt &'.format(idx,idx)
                slurm_file.write(line)
                idx += 1
                                
            line = '\nwait\nexit 0'
            slurm_file.write(line)

if __name__  ==  "__main__":
    main()