#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created in 2024

@author: cwilkinson
"""

import sys
sys.path.insert(0, '..')

import matplotlib.pyplot as plt
from python_modules.interface_module import load_transit_spectrum, load_spectrum, get_closest_interpolator, load_pkl
from python_modules.instrument_module import read_filter_docs, photo_columns, identify_observation, diluted_planet, model_at_observation
from python_modules.physics_module import flux_to_Jy
from matplotlib.cm import jet
from matplotlib.colors import Normalize
import emcee
import difflib
import pandas as pd
import glob
import numpy as np
import corner
import h5py
from python_modules.regression_module import MLmodels

class PlotRetrieval:
    """
    A class to handle the analysis and plotting of retrieval data for exoplanet spectra.

    Attributes:
        analysed_planet (str): The name of the planet being analyzed.
        retrieval_info (str): Retrieval metadata identifier.
        distance_planet (float): Distance to the planet in parsecs.
        n_spectra (int): Number of spectra to use in fit plots.
        discard (int): Number of initial MCMC samples to discard.
        thin (int): Thinning factor for MCMC samples.
        max_mass (float): filter high mass samples.
        use_jy (bool): If True, convert fluxes to Jy units.
        custom_ticks (bool): If True, use custom tick marks in plots.
    """

    def __init__(
        self,
        interpolator,
        analysed_planet,
        retrieval_info,
        distance_planet,
        n_spectra,
        discard,
        thin,
        recompute_samples=False,
        Tirr=50,
        max_mass=1e10,
        use_jy=False,
        custom_ticks=False,
    ):
        # Initialize attributes
        self.analysed_planet = analysed_planet
        self.retrieval_info = retrieval_info
        self.distance_planet = distance_planet

        # Load the observed spectrum for the planet
        self.observed_spectrum = load_spectrum(
            difflib.get_close_matches(
                self.analysed_planet,
                glob.glob('../data/spectra/spectra_list/*'),
                n=1,
                cutoff=0.2
            )[0]
        )
        self.observed_spectrum = identify_observation(self.observed_spectrum)

        # Find the relevant MCMC file
        self.MCMC_file = difflib.get_close_matches(
            self.analysed_planet + self.retrieval_info,
            glob.glob('../data/retrievals/planets/*.h5'),
            n=1,
            cutoff=0.2
        )[0]
        # Find the relevant additional data file
        self.additional_data = difflib.get_close_matches(
            self.analysed_planet + self.retrieval_info,
            glob.glob('../data/retrievals/planets/*additional_data.h5'),
            n=1,
            cutoff=0.2
        )[0]
        
        print(self.MCMC_file,self.additional_data)

        # MCMC analysis parameters
        self.n_spectra = n_spectra
        self.discard = discard
        self.thin = thin
        self.Tirr = Tirr
        self.max_mass = max_mass
        self.use_jy = use_jy
        self.custom_ticks = custom_ticks

        # Load MCMC data
        self.reader = emcee.backends.HDFBackend(self.MCMC_file)
        self.probabilities = self.reader.get_log_prob(
            discard=self.discard, thin=self.thin, flat=True
        )
        self.flat_samples = self.reader.get_chain(
            discard=self.discard, thin=self.thin, flat=True
        )

        # Photometric filter information
        wls = [ele.split('_') for ele in read_filter_docs()['wl'].to_list()]
        self.photo_wls = [float(x) for xs in wls for x in xs]

        self.photo_df = pd.DataFrame({
            'filter': photo_columns(),
            'wavelength (um)': self.photo_wls
        })

        # Load interpolator and wavelength grid
        #self.interpolator, self.interpolator_file = get_closest_interpolator(
        #    files_to_load=interpolator)
        #self.wavelength = load_pkl(
        #    f'../data/interpolators/flux/example_'
        #    + '_'.join(self.interpolator_file.split('_')[-2:])
        #)['wavelength_up'].iloc[0]
        ML = MLmodels(n_estimators=1000)
        self.wavelength = ML.wavelength
        self.recompute_samples = recompute_samples
        if self.recompute_samples :
            #target = 'flux_photo'
            #model_flux = ML.load_model(target)
            #target = 'Radius_Jup'
            #model_radius = ML.load_model(target)
            #target = 'age'
            #model_t = ML.load_model(target)
            #self.interpolator = lambda x: np.concatenate((ML.use_model(model_flux,x),np.expand_dims(ML.use_model(model_radius,x), axis=1),np.expand_dims(ML.use_model(model_t,x), axis=1)), axis=1)
            model_combined = ML.load_model('flux_radius_age')
            self.interpolator = lambda x: model_combined.predict(x)
            self.wavelength = ML.wavelength
        
        self.labels = [
                'Mass (Jup)\n', 'log(Met)\n', u'Core (\u2295)\n',
                'T$_{int}$ (K)\n', 'f$_{sed}$\n', 'kzz\n', 'S0\n', 'Radius (Jup)\n',
                'Age (Myr)\n', 'llh\n'#, 'b', 'c', 'd', 'e'
            ]
        
        '''
        # Parameter labels for corner plot
        if 'ageless' not in self.interpolator_file:
            self.labels = [
                'Mass (Jup)\n', 'log(Met)\n', u'Core (\u2295)\n',
                'T$_{int}$ (K)\n', 'f$_{sed}$\n', 'Radius (Jup)\n',
                'Age (Myr)\n', 'llh\n', 'b', 'c', 'd', 'e'
            ]
        else :
            self.labels = [
                'Mass (Jup)\n', 'log(Met)\n', u'Core (\u2295)\n',
                'T$_{int}$ (K)\n', 'f$_{sed}$\n', 'Radius (Jup)\n',
                'llh\n', 'a', 'b', 'c', 'd', 'e'
            ]
        '''

        # Filter and prepare samples
        self.filter()
        self.complete_samples()

    def filter(self):
        """Filter MCMC samples to remove invalid or poor likelihood values."""
        
        print(len(self.flat_samples), ' unfiltered samples')
        self.samples = pd.DataFrame()
        self.samples['m'] = self.flat_samples[:, 0]
        self.samples['Met'] = self.flat_samples[:, 1]
        self.samples['core'] = self.flat_samples[:, 2]
        self.samples['T'] = self.flat_samples[:, 3]
        self.samples['f_sed'] = self.flat_samples[:, 4]
        self.samples['kzz'] = self.flat_samples[:, 5]
        self.flat_samples[:, 5] = np.log10(self.flat_samples[:, 5])
        self.samples['samples'] = list(self.flat_samples)
        self.samples['llh'] = self.probabilities
        self.samples = self.samples[np.isfinite(self.samples['llh'])]
        #self.samples = self.samples[self.samples['llh'] > -100]
        self.samples = self.samples.sort_values(
            by=['llh', 'm', 'Met', ], ascending=[False, True, True])
        self.samples = self.samples.dropna()
        print('Samples filtered')
        # temp conditions : 
        self.samples = self.samples[self.samples['m'] <= self.max_mass]

    def complete_samples(self):
        """Add radii, ages, and fluxes to the filtered samples."""
        if self.recompute_samples :
            samples = np.array(self.samples['samples'].to_list())
            print(samples)
            samples = np.insert(samples, 1, self.Tirr, axis=1)
            flux_radius_batch = self.interpolator(
                samples)
            fluxes = flux_radius_batch[:, :-2]
            radii = flux_radius_batch[:, -2]
            ages = np.log10(np.array(flux_radius_batch[:, -1]))
            fluxes = np.array([diluted_planet(flux, radius, self.distance_planet)
                            for flux, radius in zip(fluxes, radii)])
        else :
            addional_data = h5py.File(self.additional_data,'r')
            radii = addional_data['radii'][()][-len(self.samples):]
            ages = np.log10(addional_data['ages'][()][-len(self.samples):])
            fluxes = np.array([diluted_planet(flux, radius, self.distance_planet)
                            for flux, radius in zip(addional_data['fluxes'][()][-len(self.samples):], radii)])
            self.samples['llh'] = addional_data['log_probabilities'][-len(self.samples):]
            self.samples['m'] = addional_data['theta_batch'][()][-len(self.samples):, 0]
            self.samples['Met'] = addional_data['theta_batch'][()][-len(self.samples):, 2]
            self.samples['core'] = addional_data['theta_batch'][()][-len(self.samples):, 3]
            self.samples['T'] = addional_data['theta_batch'][()][-len(self.samples):, 4]
            self.samples['f_sed'] = addional_data['theta_batch'][()][-len(self.samples):, 5]
            self.samples['kzz'] = addional_data['theta_batch'][()][-len(self.samples):, 6]
            
        self.samples['R'] = radii
        self.samples['age'] = ages
        self.samples['flux'] = [list(flux)
                                for flux in fluxes[:, :len(self.wavelength)]]
        self.samples['photo'] = [list(photo)
                                 for photo in fluxes[:, len(self.wavelength):]]
        if self.use_jy:
            self.samples['flux'] = flux_to_Jy(self.wavelength, fluxes)
            self.samples['photo'] = flux_to_Jy(self.wavelength, fluxes)
        self.samples = self.samples.dropna()
        self.samples = self.samples[np.isfinite(self.samples['llh'])]
        self.samples = self.samples.sort_values(
            by=['llh', 'm', 'Met', ], ascending=[False, True, True])
        print(len(self.samples), ' Samples completed')

    def corner_plot(self):
        """Generate and save a corner plot of the MCMC samples."""
        corner_elements = np.concatenate((np.array(self.samples['samples'].to_list()),
                    np.expand_dims(np.array(self.samples['R']), axis=1),
                np.expand_dims(np.array(self.samples['age']), axis=1),
            np.expand_dims(np.array(self.samples['llh']), axis=1)), axis=1)
        figure = corner.corner(
            corner_elements,
            quantiles=[
                0.16,
                0.5,
                0.84],
            labels=self.labels,
            show_titles=True,
            smooth=1,
            plot_datapoints=True,
            plot_density=True,
            fill_contours=True,
            scale=[
                'linear',
                'linear',
                'linear',
                'linear',
                'linear',
                'log',
                'log',
                'log'],
            label_kwargs=dict(
                fontsize=16),
            title_kwargs=dict(
                fontsize=16),
        )
            
        plt.savefig('../data/retrievals/plots/corner_plots/{}.pdf'.format(
            self.MCMC_file.split('/')[-1]), bbox_inches='tight')
        plt.savefig('../data/retrievals/plots/corner_plots/{}.jpg'.format(
            self.MCMC_file.split('/')[-1]), bbox_inches='tight')
        plt.show()
        print('Corner plot made')

    def plot_fits(self):
        """Generate and save spectrum fits compared to observed data."""
        
        df_fit = self.samples.round(2).drop_duplicates(
            subset=['m', 'Met', 'core', 'T', 'f_sed'])
        df_fit['Met_r1'] = df_fit['Met'].round(2)
        df_fit = df_fit.drop_duplicates('Met_r1').drop('Met_r1', axis=1)
        df_fit = df_fit.iloc[:self.n_spectra]

        norm = Normalize(
            vmin=np.min(
                df_fit['Met']), vmax=np.max(
                df_fit['Met']))
        colors = jet(norm(list(df_fit['Met'])))
        df_fit['colors'] = list(colors)

        fig, (ax1, ax2) = plt.subplots(2, 1, gridspec_kw={'height_ratios': [3, 1]}, sharex=True, figsize=(16, 8))
        fig.subplots_adjust(hspace=0)
        
        for kk, value in enumerate(np.array(df_fit)):
            flux = df_fit['flux'].iloc[kk]
            photo = df_fit['photo'].iloc[kk]
            Mass_Jup, Met, core_Earth, T_int, f_sed, radius = df_fit['m'].iloc[kk], df_fit['Met'].iloc[
                kk], df_fit['core'].iloc[kk], df_fit['T'].iloc[kk], df_fit['f_sed'].iloc[kk], df_fit['R'].iloc[kk]
            color = df_fit['colors'].iloc[kk]
            
            model = model_at_observation(
                self.observed_spectrum, self.wavelength, flux, photo)

            ax1.plot(
                self.wavelength,
                flux,
                '-',
                color=color,
                alpha=1,
                label='Mass$_{{Jup}}$: {:.2f}, Met {:.2f}\nT$_{{eff}}$ : {:.2f}, f$_{{sed}}$ : {:.1f}\nRadius$_{{Jup}}$ : {:.1f}'.format(
                    Mass_Jup,
                    Met,
                    T_int,
                    f_sed,
                    radius))
            #ax.plot([1.58569037, 1.666105], photo[8:10], 'o', color=color,
            #        alpha=1, markersize=15, markeredgecolor='black')
            #ax.plot([4.44], photo[-2], 'o', color=color, alpha=1,
            #        markersize=15, markeredgecolor='black')

        observed_flux = self.observed_spectrum[self.observed_spectrum['UL'] == 0]
        for instrument in list(set(observed_flux['instrument'])):
            observed_flux_intrument = observed_flux[observed_flux['instrument'] == instrument]
            ax1.errorbar(
                observed_flux_intrument['wavelength'],
                observed_flux_intrument['flux'],
                yerr=observed_flux_intrument['error'],
                fmt='o',
                alpha=1,
                label=instrument)

        observed_flux_UL = self.observed_spectrum[self.observed_spectrum['UL'] == 1]
        for instrument in list(set(observed_flux_UL['instrument'])):
            observed_flux_UL_instrument = observed_flux_UL[observed_flux_UL['instrument'] == instrument]
            for wavelength, flux in zip(
                    observed_flux_UL_instrument['wavelength'], observed_flux_UL_instrument['flux']):
                ax1.annotate(
                    '', xy=(
                        wavelength, (flux * 0.9)), xytext=(
                        wavelength, flux), arrowprops=dict(
                        facecolor='r', shrink=0., headwidth=10))
                
        ax1.plot(
                model['wavelength'],
                model['model_flux'],
                'o',
                color='k',
                alpha=1)
        
        ax2.plot(
                model[model['UL'] != 1]['wavelength'],
                (model[model['UL'] != 1]['model_flux']-model[model['UL'] != 1]['flux'])/model[model['UL'] != 1]['flux'],
                'o',
                color='k',
                alpha=1)
        ax2.axhline(y=0, color='r', linestyle='-')

        #ax.set_ylim([1e-22, 4e-18])
        #ax.set_xlim([1, 15])
        ax1.set_ylim([np.min(model[['model_flux','flux']].min().min())*0.5, np.max(model[['model_flux','flux']].max().max())*1.1])
        ax1.set_xlim([0.9, 18])
        #ax1.set_xlabel(r'Wavelength $\mu m$', fontsize=13)
        ax1.set_ylabel(r'Flux $W/m^2/\mu m$', fontsize=13)
        ax1.tick_params(axis='both', which='major', labelsize=10)
        ax1.tick_params(axis='both', which='minor', labelsize=8)
        ax1.set_xscale('log')
        ax1.set_yscale('log')
        ax2.set_yscale('symlog')
        if self.custom_ticks:
            ticks = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15]
            ax1.set_xticks(ticks)
            ax1.get_xaxis().set_major_formatter(plt.ScalarFormatter())
        ax1.legend(fontsize=8)
        ax2.legend(fontsize=8)
        ax2ticks = [-1,-0.5,-0.1,0.1,0.5,1]
        ax2.set_yticks(ax2ticks)
        ax2.get_yaxis().set_major_formatter(plt.ScalarFormatter())
        ax2.set_ylabel('Residuals\n' + r'$\frac{model-observed}{observed}$', fontsize=13)
        ax2.set_xlabel(r'Wavelength $\mu m$', fontsize=13)
        plt.savefig('../data/retrievals/plots/fits/{}.pdf'.format(
            self.MCMC_file.split('/')[-1]), bbox_inches='tight')
        plt.savefig('../data/retrievals/plots/fits/{}.jpeg'.format(
            self.MCMC_file.split('/')[-1]), bbox_inches='tight')
        plt.show()
        print('Spectrum completed')
        
class IdentifySpectra:
    """
    A class to identify and plot the observed spectra for a given exoplanet.

    Attributes:
        planet (str): The name of the planet being analyzed.
        distance_planet (float): Distance to the planet in parsecs.
        use_jy (bool): If True, convert fluxes to Jy units.
        custom_ticks (bool): If True, use custom tick marks in plots.
    """

    def __init__(self, planet, distance_planet, use_jy=False, custom_ticks=False):
        # Initialize attributes
        self.planet = planet
        self.distance_planet = distance_planet
        self.use_jy = use_jy
        self.custom_ticks = custom_ticks

        # Load the observed spectrum for the planet
        self.file = difflib.get_close_matches(
                self.planet,
                glob.glob('../data/spectra/spectra_list/*'),
                n=1,
                cutoff=0.15
            )[0]
        self.observed_spectrum = load_spectrum(self.file)
        self.observed_spectrum = identify_observation(self.observed_spectrum)

        # Load interpolator and wavelength grid
        ML = MLmodels(n_estimators=1000)
        self.wavelength = ML.wavelength

    def plot_spectrum(self,added_spectrum='None'):
        """Generate and save a plot of the observed spectrum."""
        fig, ax = plt.subplots(figsize=(16, 8))
        
        observed_flux = self.observed_spectrum[self.observed_spectrum['UL'] == 0]
        for instrument in list(set(observed_flux['instrument'])):
            observed_flux_intrument = observed_flux[observed_flux['instrument'] == instrument]
            ax.errorbar(
                observed_flux_intrument['wavelength'],
                observed_flux_intrument['flux'],
                yerr=observed_flux_intrument['error'],
                fmt='o',
                alpha=1,
                label=instrument)

        observed_flux_UL = self.observed_spectrum[self.observed_spectrum['UL'] == 1]
        for instrument in list(set(observed_flux_UL['instrument'])):
            observed_flux_UL_instrument = observed_flux_UL[observed_flux_UL['instrument'] == instrument]
            for wavelength, flux in zip(
                    observed_flux_UL_instrument['wavelength'], observed_flux_UL_instrument['flux']):
                ax.annotate(
                    '', xy=(
                        wavelength, (flux * 0.9)), xytext=(
                        wavelength, flux), arrowprops=dict(
                        facecolor='r', shrink=0., headwidth=10))
        ax.set_ylabel(r'Flux $W/m^2/\mu m$', fontsize=13)
        ax.set_xlabel(r'Wavelength $\mu m$', fontsize=13)
        
        if added_spectrum != 'None' :
            ax.plot(added_spectrum[0],added_spectrum[1],'-',label='Added spectrum')
        
        plt.title(self.file)
        plt.legend()
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim([self.observed_spectrum['wavelength'].min()*(0.9),self.observed_spectrum['wavelength'].max()*(1.1)])
        plt.ylim([self.observed_spectrum['flux'].min()*(0.9),self.observed_spectrum['flux'].max()*(1.1)])
        plt.show()

class IdentifyTransit:
    """
    A class to identify and plot the transit spectraa of a given exoplanet.

    Attributes:
        planet (str): The name of the planet being analyzed.
        custom_ticks (bool): If True, use custom tick marks in plots.
    """

    def __init__(self, planet, custom_ticks=False):
        # Initialize attributes
        self.planet = planet
        self.custom_ticks = custom_ticks

        # Load the observed spectrum for the planet
        self.file = difflib.get_close_matches(
                self.planet,
                glob.glob('../data/spectra/transit_spectra/*.txt'),
                n=1,
                cutoff=0.15
            )[0]
        self.observed_spectrum = load_transit_spectrum(self.file)

        # Load interpolator and wavelength grid
        ML = MLmodels(n_estimators=1000)
        self.wavelength = ML.wavelength
        
    def plot_transit(self,added_spectrum='None'):
        fig, ax = plt.subplots(figsize=(16, 8))
        ax.errorbar(self.observed_spectrum['wavelength'],
                    self.observed_spectrum['depth'],
                    yerr=self.observed_spectrum['error'],
                    fmt='o',
                    label=self.file)
        
        if added_spectrum != 'None' :
            ax.plot(added_spectrum[0],added_spectrum[1],'-',label='Added spectrum')
            
        ax.set_ylabel(r'Depth $\frac{R_p^2}{R_s^2}$', fontsize=13)
        ax.set_xlabel(r'Wavelength $\mu m$', fontsize=13)
        plt.title(self.file)
        plt.legend()
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim([self.observed_spectrum['wavelength'].min()*(0.9),self.observed_spectrum['wavelength'].max()*(1.1)])
        #plt.ylim([self.observed_spectrum['depth'].min()*(0.9),self.observed_spectrum['depth'].max()*(1.1)])
        plt.show()
        
class PlotTransitRetreival:
    def __init__(self, analysed_planet, shorten=0, retrieval_info=''):
        self.analysed_planet = analysed_planet
        self.retrieval_info = retrieval_info
        self.shorten = shorten
        
        # Load the observed spectrum for the planet
        self.observed_spectrum = load_transit_spectrum(
            difflib.get_close_matches(
                analysed_planet,
                glob.glob('../data/spectra/transit_spectra/*'),
                n=1,
                cutoff=0.2
            )[0]
        )
        
        # Find the relevant additional data file
        self.additional_data = h5py.File(difflib.get_close_matches(
            self.analysed_planet + self.retrieval_info,
            glob.glob('../data/retrievals/planets/*additional_data.h5'),
            n=1,
            cutoff=0.2
        )[0])
        
        self.depths = self.additional_data['depths'][self.shorten:]
        self.ages = self.additional_data['ages'][self.shorten:]
        self.llh = self.additional_data['log_probabilities'][self.shorten:]
        self.radii = self.additional_data['radii'][self.shorten:]
        self.samples = self.additional_data['theta_batch'][self.shorten:]
        self.n_walkers = self.additional_data['n_walkers'][0]
        self.n_iterations = self.additional_data['n_iterations'][0]
        
        self.corner_elements = np.concatenate((np.array(self.samples),
                    np.expand_dims(np.array(self.radii), axis=1),
            np.expand_dims(np.array(self.llh), axis=1)), axis=1)
        self.corner_elements = np.delete(self.corner_elements, 1, axis=1)
        self.corner_elements[:, 5][self.corner_elements[:, 5]<=0] = 1e-4
        self.corner_elements[:, 5] = np.log10(self.corner_elements[:, 5])
        self.infinite_llh()
        self.corner_elements[:, -1] = np.log10(np.abs(self.corner_elements[:, -1]))
        
        self.labels = [
                'Mass (Jup)\n', 'log(Met)\n', u'Core (\u2295)\n',
                'T$_{int}$ (K)\n', 'f$_{sed}$\n', 'log10(kzz)\n', 'Radius (Jup)\n',
                'log10(llh)\n'#, 'b', 'c', 'd', 'e'
            ]
            
    def plot_transit_fit(self,type='best'):
        
        fig, ax = plt.subplots(figsize=(16, 8))
        if 'all' in type :
            for depth in self.depths :
                ax.plot(self.observed_spectrum['wavelength'],depth, '-', color='k', alpha=0.01)
        
        if 'sigma' in type :
            mean_value = np.mean(self.corner_elements[:, -1])
            std_deviation = np.std(self.corner_elements[:, -1])
            
            depths_sig = self.depths[
                (self.corner_elements[:, -1] >= mean_value - std_deviation) & 
                (self.corner_elements[:, -1] <= mean_value + std_deviation)
            ]
            for depth in depths_sig :
                ax.plot(self.observed_spectrum['wavelength'],depth, '-', color='k', alpha=0.01)
                
        if 'best' in type :
            ax.plot(self.observed_spectrum['wavelength'],self.depths[np.argmax(self.llh)], '-', color='r', alpha=1, label='Best fit')

        ax.errorbar(self.observed_spectrum['wavelength'],
                self.observed_spectrum['depth'],
                yerr=self.observed_spectrum['error'],
                fmt='o',
                label=self.analysed_planet)
                    
        ax.set_ylabel(r'Depth $\frac{R_p^2}{R_s^2}$', fontsize=13)
        ax.set_xlabel(r'Wavelength $\mu m$', fontsize=13)
        plt.title(self.analysed_planet)
        plt.legend()
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim([self.observed_spectrum['wavelength'].min()*(0.9),self.observed_spectrum['wavelength'].max()*(1.1)])
        #plt.ylim([self.observed_spectrum['depth'].min()*(0.9),self.observed_spectrum['depth'].max()*(1.1)])
        plt.show()
        
    def plot_chains(self):
        fig, ax = plt.subplots(np.shape(self.corner_elements)[1]//2, 2, figsize=(16, 8))
        for ii, label in enumerate(self.labels):
            for walker in range(self.n_walkers):
                ax[ii//2, ii%2].plot(self.corner_elements[walker::self.n_walkers, ii],alpha=0.1)
            ax[ii//2, ii%2].set_ylabel(label)
        plt.show()
        
    def infinite_llh(self):
        # Convert to NumPy array for easy manipulation
        self.corner_elements = np.array(self.corner_elements)

        # Find indices where log-likelihood (last column) is NaN or Inf
        logL_col = -1  # Assuming the log-likelihood is the last column
        finite_logL = self.corner_elements[np.isfinite(self.corner_elements[:, logL_col]), logL_col]

        if len(finite_logL) > 0:  # Ensure we have valid values
            min_finite_logL = 10*np.min(finite_logL)  # Slightly lower to keep it distinguishable
            self.corner_elements[:, logL_col] = np.where(
                np.isfinite(self.corner_elements[:, logL_col]),
                self.corner_elements[:, logL_col],
                min_finite_logL  # Replace NaN/Inf with a finite value
            )
        else:
            raise ValueError("No finite log-likelihood values found!")  # Handle extreme case
        
    def corner_plot(self):
        """Generate and save a corner plot of the MCMC samples."""
        figure = corner.corner(
            self.corner_elements,
            quantiles=[
                0.16,
                0.5,
                0.84],
            labels=self.labels,
            show_titles=True,
            smooth=1,
            plot_datapoints=True,
            plot_density=True,
            fill_contours=True,
            scale=[
                'linear',
                'linear',
                'linear',
                'linear',
                'linear',
                'log',
                'log',
                'log'],
            label_kwargs=dict(
                fontsize=16),
            title_kwargs=dict(
                fontsize=16),
            range = [0.999, 
                    0.999, 
                    0.999, 
                    0.999, 
                    0.999, 
                    (4, 12), 
                    0.999, 
                    0.999],
        )
            
        plt.savefig('../data/retrievals/plots/corner_plots/{}.pdf'.format(
            self.analysed_planet.split('/')[-1]), bbox_inches='tight')
        plt.savefig('../data/retrievals/plots/corner_plots/{}.jpg'.format(
            self.analysed_planet.split('/')[-1]), bbox_inches='tight')
        plt.show()
        print('Corner plot made')
