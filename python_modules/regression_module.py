import sys
sys.path.append("..")

from python_modules.grid_data_processing_module import load_grid
from python_modules.quality_module import QualityControl
from python_modules.numerical_tools_module import interpolate_negatives_rowwise


import time
import numpy as np
import pandas as pd
import joblib
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
import lightgbm as lgb
#import optuna
from sklearn.multioutput import MultiOutputRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from python_modules.physics_module import evaluate_age, get_init_s_extremums, get_init_s
import joblib
import pickle as pkl
import glob
import time
from tqdm import tqdm

R_J = 69911000          # Jupiter radius in meters  
M_J = 1.898e27          # Jupiter mass in kilograms  
R_s = 696340000         # Solar radius in meters  
M_s = 1.989e30         # Solar mass in kilograms  
R_E = 6371e3           # Earth radius in meters  
M_E = 5.972e24         # Earth mass in kilograms  
G_u = 6.674e-11        # Gravitational constant (m^3 kg^-1 s^-2)  
Na = 6.022e23          # Avogadro's number (mol^-1)  
Kb = 1.380649e-23      # Boltzmann constant (J K^-1)  
R_g = 8.31446261815324 # Universal gas constant (J mol^-1 K^-1)  
sigma = 5.67e-8        # Stefan-Boltzmann constant (W m^-2 K^-4)  

class MLmodels:
    def __init__(self,split_fraction=0.1,n_estimators=1000) :
        self.split_fraction = split_fraction
        self.n_estimators = n_estimators
        self.n_plot = 5
        self.star = R_s
        self.output_path = '../data/interpolators/nonlinear/'
        self.pressure = np.logspace(-4,10,1000)
        if len(glob.glob('{}/example.pkl'.format('/'.join(self.output_path.split('/')[:-1])))) == 1 :
            with open('{}/example.pkl'.format('/'.join(self.output_path.split('/')[:-1])), 'rb') as f:
                self.example_model = pd.read_pickle(f)
            self.wavelength = self.example_model['wavelength'].iloc[0]
            self.wavelength_photo = self.example_model['wavelength_photo'].iloc[0]
            print('loaded example wavelength')
            
        
    def prepare_data(self) :
        self.grid =  load_grid()
        self.wavelength = self.grid['wavelength'].iloc[0]
        self.wavelength_photo = self.grid['wavelength_photo'].iloc[0]
        self.grid = self.grid[self.grid['n_neg_flux'] == 0]
        QC = QualityControl()
        self.grid =  QC.use_model(self.grid)
        self.grid = self.grid[self.grid['quality_pred']==1]
        self.grid = self.grid[self.grid['T_int']>100]
        self.grid['Met'] = np.log10(self.grid['Met'])
        self.grid = self.grid.dropna(subset=['Mass_Jup', 'Radius_Jup', 'T_int', 'T_irr', 'Met', 'core_Earth', 'f_sed', 'kzz'])
        self.grid = self.grid[~self.grid.isin([np.nan, np.inf, -np.inf]).any(axis=1)]
        self.grid = self.grid[self.grid['flux_photo'].apply(lambda x: not any(np.isnan(item) for item in x))]
        self.grid[['ds_dt', 'Radius_Jup', 'S', 'flux', 'flux_photo','thermal','depth','Tstandard']] = self.grid.applymap(self.apply_log10)[['ds_dt', 'Radius_Jup', 'S', 'flux', 'flux_photo','thermal','depth','Tstandard']]
        self.grid['depth_radius'] = self.grid.apply(lambda row: list(row['depth']) + [row['Radius_Jup']], axis=1)
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.grid[['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'T_int', 'f_sed','kzz']], 
                                                                                self.grid.applymap(np.array)[['ds_dt', 'Radius_Jup', 'S', 'flux', 'flux_photo','thermal','depth', 'depth_radius', 'Tstandard']], test_size=self.split_fraction, random_state=42)
        with open('{}/example.pkl'.format('/'.join(self.output_path.split('/')[:-1])), 'wb') as f:
            pkl.dump(self.grid.iloc[:1], f)
        print('saved example wavelength')
        print('Data prepared with {} samples'.format(len(self.grid)))
        return self.grid
    
    @staticmethod
    # Function to apply log10 to a value
    def apply_log10(value):
        if isinstance(value, (float, int)):  # Handle scalar values
            return np.log10(value + 1e-14)  # Add a small value to avoid log(0)
        elif isinstance(value, list):  # Handle lists
            return [np.log10(v + 1e-14) for v in value]
        elif isinstance(value, np.ndarray):  # Handle arrays
            return np.log10(value + 1e-14)
        else:
            return value  # If not numeric or list/array, return as is
            
    def make_model(self,target) :
        y = self.y_train[target].values
        if isinstance(y[0], np.ndarray) :
            y = np.array([ele for ele in y ])
        
        if len(np.shape(y)) == 1 :
            model = RandomForestRegressor(n_jobs=24,n_estimators=self.n_estimators, random_state=42, verbose=0)
        else :
            model = MultiOutputRegressor(lgb.LGBMRegressor(
                n_estimators=500,   # Number of boosting rounds
                learning_rate=0.1,  # Step size shrinkage
                random_state=42,
                n_jobs=24
            ))
        
        #model = RandomForestRegressor(n_jobs=24,n_estimators=self.n_estimators, random_state=42, verbose=0)
        
        model = model.fit(self.X_train, y)
        #joblib.dump(model, '{}/{}_{}.pkl'.format(self.output_path,target,self.n_estimators))
        joblib.dump(model, '{}/{}.pkl'.format(self.output_path,target))
        
        return model
    
    def load_model(self, target):
        model_path = f"{self.output_path}/{target}.pkl"        
        print(f"Loading {model_path}")
        model = joblib.load(model_path)
        print(f"Loaded {model_path}")
        return model
    
    def use_model(self, scaled_model,values):
        return 10**(scaled_model.predict(np.array(values)))
    
    def test_model(self,target,n_plots,model=None) :
        if (model is None) :
            model= self.make_model(target)
        y_pred = self.use_model(model,self.X_test)
        if len(np.shape(y_pred)) == 1 :
            perc_error = 100*(y_pred-(10**self.y_test['Radius_Jup']))/(10**self.y_test['Radius_Jup'])
            print(target,"sqrt(Mean squared error) : ", np.sqrt(mean_squared_error(10**self.y_test['Radius_Jup'], y_pred)))
            print(target,"Upper ; Lower ; std;  mean abs percentage difference : {:.2f} ; {:.2f} ; {:.2f} ; {:.2f}".format(perc_error.max(),perc_error.min(),perc_error.std(),perc_error.abs().mean()))
        elif ('depth' in target) or ('flux' in target) or ('flux_photo' in target) :
            self.plot_spectrum(target,y_pred,n_plots)
        elif ('Tstandard' in target) :
            self.plot_profile(y_pred,n_plots)
        return model
    
    def evaluate_array_model(self,model,target):
        y_pred = self.use_model(model,self.X_test)
        return np.mean([np.sum((y_pred[i]-10**np.array(self.y_test[target])[i])**2) for i in range(len(y_pred))])
        
    def plot_spectrum(self,target,y_pred,n_plots):
        plt.figure()
        for i in range(n_plots):
            p = plt.plot(self.wavelength_photo[:len(self.wavelength)],y_pred[i][:len(self.wavelength)], '-')
            plt.plot(self.wavelength_photo[:len(self.wavelength)],10**self.y_test[target].iloc[i][:len(self.wavelength)], '--',color = p[0].get_color())
            if len(self.y_test[target].iloc[i]) > len(self.wavelength) :
                plt.plot(self.wavelength_photo[len(self.wavelength):],y_pred[i][len(self.wavelength):], 'o', color = p[0].get_color())
                plt.plot(self.wavelength_photo[len(self.wavelength):],10**self.y_test[target].iloc[i][len(self.wavelength):], 'x', color = p[0].get_color())
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim([0.8,20])
        plt.xlabel(r'Wavelength $(\mu m)$')
        plt.ylabel(r'Flux $(W/m^2/\mu m)$')
        plt.show()
        
        plt.figure()
        for i in range(n_plots):
            plt.plot(self.wavelength_photo[:len(self.wavelength)],100*np.abs(10**self.y_test[target].iloc[i][:len(self.wavelength)]-y_pred[i][:len(self.wavelength)])/10**self.y_test[target].iloc[i][:len(self.wavelength)], '-')
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim([0.8,20])
        plt.xlabel(r'Wavelength $(\mu m)$')
        plt.show()
        
        plt.figure()
        for i in range(n_plots):
            plt.plot(self.wavelength_photo[:len(self.wavelength)],100*np.abs(10**self.y_test[target].iloc[i][:len(self.wavelength)]-y_pred[i][:len(self.wavelength)])/10**self.y_test[target].iloc[i][:len(self.wavelength)], '-')
        plt.yscale('log')
        plt.xscale('log')
        plt.xlim([0.8,20])
        plt.ylim([0.01,100])
        plt.xlabel(r'Wavelength $(\mu m)$')
        plt.show()
    
    def plot_profile(self,y_pred,n_plots):
        plt.figure()
        for i in range(n_plots): 
            plt.plot(10**self.y_test['Tstandard'].iloc[i], self.pressure)
            plt.plot(y_pred[i], self.pressure)
            plt.gca().invert_yaxis()
            plt.yscale('log')
            plt.xscale('log')
            plt.xlabel(r'Temperature $(K)$')
            plt.ylabel(r'Pressure $(bar)$')
            plt.show()
        
    def feature_importance(self,model) :
        importances = model.feature_importances_
        std = np.std([tree.feature_importances_ for tree in model.estimators_], axis=0)
        forest_importances = pd.Series(importances, index=list(self.X_train.columns))
        fig, ax = plt.subplots()
        forest_importances.plot.bar(yerr=std, ax=ax)
        ax.set_title("Feature importances using MDI")
        ax.set_ylabel("Mean decrease in impurity")
        fig.tight_layout()
        return
    
    def profile_to_target(self,target) :
        features = self.grid.Tstandard.apply(pd.Series)
        features.columns = ['T_'+str(ii) for ii in range(len(features.columns))]
        features = pd.concat([self.grid[['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'T_int', 'f_sed','kzz']],features],axis=1)
        X_train, X_test, y_train, y_test = train_test_split(features,
                                                            self.grid[['ds_dt', 'Radius_Jup', 'S', 'flux','thermal','depth','Tstandard']], test_size=0.1, random_state=42)
        model = RandomForestRegressor(n_jobs=24,n_estimators=self.n_estimators, random_state=42, verbose=0)
        y = y_train[target]
        model = model.fit(X_train, y)    
        return model
    
    def age_regressor(self):
        grid = self.grid
        
        target = 'ds_dt'
        model_evol = self.load_model(target)
        f_ds_dt = lambda x: self.use_model(model_evol,x)

        target = 'Radius_Jup'
        model_radius = self.load_model(target)
        f_r = lambda x: self.use_model(model_radius,x)

        target = 'S'
        model_S = self.load_model(target)
        f_s = lambda x: self.use_model(model_S,x)
        
        n_entropy_bins = 5
        
        df = evaluate_age(grid,(f_ds_dt,f_s,f_r),n_entropy_bins=n_entropy_bins)
        
        model_age = RandomForestRegressor(n_jobs=24,n_estimators=self.n_estimators, random_state=42, verbose=0)
        model_age = model_age.fit(df[['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'T_int', 'f_sed', 'kzz', 'S0']], self.apply_log10(df['t'].values))
        joblib.dump(model_age, '{}/{}.pkl'.format(self.output_path,'age'))
        
        target = 'age'
        model_age = self.load_model(target)
        
        S_cold, S_hot = get_init_s_extremums()
        grids = []
        for entropy_ii in tqdm(range(n_entropy_bins)) :
            f_S0 = lambda x: get_init_s(x, S_cold, S_hot, entropy_ii, n_bins=n_entropy_bins)
            grid['S0'] = grid['Mass_Jup'].apply(f_S0)
            grid['age'] = self.use_model(model_age,grid[['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'T_int', 'f_sed','kzz','S0']])
            grids.append(grid)
        self.grid = pd.concat(grids).sample(frac=1)
        print(self.grid['age'])
        self.grid['flux_radius_age'] = self.grid.apply(lambda row: list(row['flux_photo']) + [row['Radius_Jup'], self.apply_log10(row['age'])], axis=1)
        self.grid = self.grid[self.grid['flux_radius_age'].apply(lambda x: not any(np.isinf(item) for item in x))]
        print('New length of grid : ',len(self.grid))
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.grid[['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'T_int', 'f_sed','kzz','S0']], 
                                                                                self.grid.applymap(np.array)[['ds_dt', 'Radius_Jup', 'S', 'flux', 'flux_photo','thermal','depth','Tstandard','flux_radius_age']], test_size=self.split_fraction, random_state=42)
        
        self.X_test.to_pickle('{}/X_test.pkl'.format(self.output_path))
        self.y_test.to_pickle('{}/y_test.pkl'.format(self.output_path))
        
        model_combined = self.make_model('flux_radius_age')
        
        model_combined.fit(self.X_train, np.array(self.y_train['flux_radius_age'].to_list()))
        joblib.dump(model_combined, '{}/{}.pkl'.format(self.output_path,'flux_radius_age_lgb'))
        
        
        return model_combined
