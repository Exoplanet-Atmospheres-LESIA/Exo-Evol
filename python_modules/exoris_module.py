"""
Created on Fri Jun 16 11:39:11 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob
import numpy as np
import pandas as pd
import time

from scipy.interpolate import interp1d

from molmass import Formula

from python_modules.exoplanet_data_module import *
from python_modules.numerical_tools_module import *
from python_modules.exorem_module import *

import warnings
warnings.filterwarnings("ignore")

R_J = 69911000  # Radius of Jupiter in meters
M_J = 1.898*1e27  # Mass of Jupiter in kilograms
R_s = 696340000  # Radius of the Sun in meters
M_s = 1.989*1e30  # Mass of the Sun in kilograms
R_J = 69911000  # Radius of Jupiter in meters (repeated variable, potentially redundant)
M_J = 1.898*1e27  # Mass of Jupiter in kilograms (repeated variable, potentially redundant)
M_E = 5.972*1e24  # Mass of Earth in kilograms
G_u = 6.674*1e-11  # Universal gravitational constant in m^3 kg^(-1) s^(-2)
Na = 6.022*1e23  # Avogadro's number in mol^(-1)
Kb = 1.380649*1e-23  # Boltzmann constant in m^2 kg s^(-2) K^(-1)


""" Exoris : computing giants' planets structure
    Author : Adrien Licari version 2015
    Stephane Mazevet version depuis  
    this module interfaces with exoris so as to not have to manually use exoris """

def make_exoris(file_prefix,exoris_path):
    current_location = os.getcwd()
    os.system('rm -r ../temp/running_exoris/exoris_'+file_prefix)
    os.system('cp -r {}/exoris ../temp/running_exoris/exoris_'.format(exoris_path) + file_prefix )
    os.chdir('../temp/running_exoris/exoris_' + file_prefix + '/obj')
    os.system('make clean')
    os.system('make')
    os.system('make install')
    os.chdir(current_location)

def remove_exoris(file_prefix):
    os.system('rm -r ./temp/running_exoris/exoris_'+file_prefix)
    
def extract_H_He_ratio(atmosphere_new) :
    He_H = []
    x_H = []
    for idx in range(len(atmosphere_new)) :
        x = np.zeros(np.shape(np.array(atmosphere_new['/outputs/layers/volume_mixing_ratios/elements_gas_phase/Al'].iloc[idx])))
        for ele in atmosphere_new.columns :
            if '/outputs/layers/volume_mixing_ratios/elements_gas_phase/' in ele :
                element = ele.split('/')[-1]
                e = Formula(element)
                if (element == 'H') :
                    H = e.mass*np.array(atmosphere_new[ele].iloc[idx])
                elif (element == 'He') :
                    He = e.mass*np.array(atmosphere_new[ele].iloc[idx])
                else :
                    x = x + e.mass*np.array(atmosphere_new[ele].iloc[idx])
        He_H.append(He/H)
        x_H.append(x/H)
                
    return list(np.array([a+b for a,b in zip(He_H,x_H)]))

def extract_H_He_P_link(atmosphere_new) :
    yhe = []
    for idx in range(len(atmosphere_new)) :
        yhe.append(interp1d(np.array(atmosphere_new['/outputs/levels/pressure'].iloc[idx][:-1])*1e-5,atmosphere_new['He_H'].iloc[idx])(atmosphere_new['P_link'].iloc[idx]))
    return yhe

def run_exoris (output_file,file_prefix, atmosphere_new, model, M, core, eos_env, rock, eos_ice, eos_core, exoris_data) :
    current_location = os.getcwd()
    if len(model) > 0:
        R_init = model['Req'].iloc[-1]*(M*M_J*1e3/model['mass'].iloc[-1])
    else :
        R_init = 1*R_J
    print('\nInitating with a radius of {}cm'.format(R_init))
    
    P_link = atmosphere_new['P_link'].iloc[-1]
    T_atm = atmosphere_new['T_at_P'].iloc[-1]
    
    atmosphere_new['He_H'] = extract_H_He_ratio(atmosphere_new)
    atmosphere_new['yhe'] = extract_H_He_P_link(atmosphere_new)

    yhe = atmosphere_new['yhe'].iloc[-1]    
    y_h2o = 0
    
    #if Met  == 1 :
    #   yhe = 0.289363
       
    #if Met  == 10 :
    #   yhe = 0.426422
       
    #if Met  == 30 :
    #   yhe = 0.642256
    
    print('\n')
    print('Mass : {}'.format(M))
    print('T : {}'.format(T_atm))
    print('Chosen yhe : {}'.format(yhe))
    print('P : {}'.format(P_link))
    print('\n')
    
    P = 1
    core_type = 'mass'

    if (core_type=='mass'):
        core_value = core*M_E/(M*M_J)
    else: 
        core_value = core
        
    if core_value == 0 :
        use_core = 'false'
        core_value = 10*M_E/(M*M_J)
    else :
        use_core = 'true'

    with open('../temp/running_exoris/exoris_'+file_prefix+'/bin/parameters.txt',"w") as f:
        L = [" ! Parameter file \n"]
        L = L + ["&horedt_nml \n\n"]
        L = L + ["computation = 'model' \n"]
        L = L + ["Nint = 10000 \n\n"] 
        L = L + ["prot = 9.925\n"]
        L = L + ["measured_r = ", str(np.round(R_init,1)), " \n"] # In cm
        L = L + ["mass = ", str(np.round(M*M_J*1e3,1)), " \n"] # In grams
        L = L + ["surface_T = ", str(np.round(T_atm,4))," \n"] # In K
        L = L + ["surface_P = ", str(P_link)," \n"] # In bars
        L = L + ["core = ", str(np.round(core_value,6))," \n"]
        L = L + ["rock = ", str(np.round(rock,6))," \n"]
        L = L + ["yhe = ", str(np.round(yhe,6))," \n"]
        #L = L + ["yhe = 0.2919 \n"] # if Philips et al. fraction desired
        #L = L + ["yh2o = ", str(np.round(y_h2o,6))," \n"]
        L = L + ["eos_env = '", eos_env,"' \n"]
        L = L + ["eos_ice = '", eos_ice,"' \n"]
        L = L + ["eos_core = '", eos_core,"' \n"]
        #if core_value == 0 :
        #    L = L + ["use_core = false' \n"]
        #else :
        #    L = L + ["use_core = true' \n"]
        L = L + ["verbose = true\n"]
        L = L + ["figure = false\n"]
        L = L + ["giant = true\n"]
        L = L + ["/\n"]
        L = L + [" "]

        f.writelines(L)
        f.close()

    os.chdir('../temp/running_exoris/exoris_'+file_prefix+'/bin')
    os.system("./horedt5")
    os.chdir(current_location)
    
    try : 
        df_new = extract_json('../temp/running_exoris/exoris_'+file_prefix+'/bin/model.json')
        os.system('cp ../temp/running_exoris/exoris_{}/bin/model.json {}/{}/{:.4f}_{:.4f}_{:.4f}_{:.1f}_{:.1f}.json'.format(file_prefix,exoris_data,group_masses(M),np.round(M,4),core,yhe,T_atm,P_link))
        os.system('cp ../temp/running_exoris/exoris_{}/bin/fort.99 {}/thermo/{:.4f}_{:.4f}_{:.4f}_{:.1f}_{:.1f}_{}.dat'.format(file_prefix,exoris_data,np.round(M,4),core,yhe,T_atm,P_link,output_file.split('/')[-1]))
        print('-----Exoris succeeded----')
    except :
        print('-----Exoris failed----')
        df_new = pd.DataFrame()
    return df_new

def group_masses(x) :
    if x <= 0.1 :
        return 0.1
    elif (x > 0.1) & (x <= 0.5) :
        return 0.25
    elif (x > 0.5) & (x <= 1) :
        return 0.75
    elif (x > 1) & (x <= 5) :
        return 2.5
    elif (x > 5) & (x <= 10) :
        return 7.5
    elif (x > 10) :
        return 10.0
            
def extract_json(obj) :
    with open(obj, encoding='utf-8') as inputfile:
        df = pd.read_json(inputfile)
        physical = pd.DataFrame.from_dict(df['parameters'].loc['physical'])
        model = pd.DataFrame.from_dict(df['parameters'].loc['model'])
        df = pd.concat([df.loc['info':'val'], physical,model], axis=1)
        G_u = 6.674*1e-11
        if (None not in df['rho'].iloc[-1]) and (None not in df['P'].iloc[-1]) :
            df['gs_exoris'] = [np.nan,'m.s-2',G_u*(df['mass'].iloc[-1]*1e-3)/(df['Req'].iloc[-1]*1e-2)**2]
            df['gs'] = [np.nan,'m.s-2',G_u*(df['mass'].iloc[-1]*1e-3)/(df['R'].iloc[-1][-2]*1e-2)**2]
            df['rhos'] = [np.nan,'kg.m-3',(df['rho'].iloc[-1][-1])*(1e-3)/(1e-2)**3]
            df['Ps'] = [np.nan,'bar',np.min(df['P'].iloc[-1])]
            
            rho = np.array(df['rho'].iloc[-1])*1000
            r = np.array(df['R'].iloc[-1])*1e-2
            gravity = [0]
            for kk in range(1,len(r)) :
                gravity.append(G_u*compute_mass(rho[:kk],r[:kk])/r[kk]**2)
            df['gravity'] = [np.nan,'m.s-2',gravity]
            df['pressure_link_gravity'] = [np.nan,'bar',df['P'].iloc[-1][-2]]
        else :
            df['gs'] = [np.nan,np.nan,np.nan]
            df['rhos'] = [np.nan,np.nan,np.nan]
            df['Ps'] = [np.nan,np.nan,np.nan]
        df = df.loc[:,~df.columns.duplicated()].copy()
    return df
