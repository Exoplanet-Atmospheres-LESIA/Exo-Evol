"""
Created on Tue Jan  9 15:51:00 2024

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import numpy as np
import pandas as pd
import glob

import f90nml
import h5py
import pickle as pkl

def read_input () :
    return f90nml.read('../inputs/input_parameters.nml')

def input_parameters(key) :
    parameters = read_input ()
    parameter = parameters['grid_parameters'][key]
    if (type(parameter) == list) : 
        if (len(parameter) == 4):
            if ('lin' in str(parameter[3])) & (type(parameter[3])==str) :
                return np.arange(parameter[0],parameter[1],parameter[2])
            elif ('log' in str(parameter[3])) & (type(parameter[3])==str) :
                return np.logspace(parameter[0],parameter[1],parameter[2])
            else :
                return np.array(parameter[:-1])
        else :
            return np.array(parameter[:-1])
    else :
        return parameter
    
def input_paths(key) :
    parameters = read_input ()
    path = parameters['grid_paths'][key]
    return path

def input_convergence(key) :
    parameters = read_input ()
    parameter = parameters['grid_convergence'][key]
    return parameter

def input_cluster(key) :
    parameters = read_input ()
    parameter = parameters['cluster_control'][key]
    return parameter

def interp_parameters(key) :
    parameters = read_input ()
    parameter = parameters['interp_parameters'][key]
    return parameter

def interp_paths(key) :
    parameters = read_input ()
    parameter = parameters['interp_paths'][key]
    return parameter

def retrieval_parameters_direct(key) :
    parameters = read_input ()
    parameter = parameters['retrieval_parameters_direct'][key]
    return parameter

def retrieval_paths_direct(key) :
    parameters = read_input ()
    parameter = parameters['retrieval_paths_direct'][key]
    return parameter

def retrieval_parameters_transit(key) :
    parameters = read_input ()
    parameter = parameters['retrieval_parameters_transit'][key]
    return parameter

def retrieval_paths_transit(key) :
    parameters = read_input ()
    parameter = parameters['retrieval_paths_transit'][key]
    return parameter
       
def allkeys(df, obj):
    "Recursively find all keys in an h5py.Group."
    if isinstance(obj,str):
        obj = h5py.File(obj,'r+')
    keys = (obj.name,)
    if isinstance(obj, h5py.Group):
        for key, value in obj.items():
            if isinstance(value, h5py.Group):
                df, new_key = allkeys(df, value)
                keys = keys + new_key
            else:
                keys = keys + (value.name,)
                
                key_name = keys[-1]
                try:
                    df[key_name] = [list(obj[keys[-1]][:])]
                except :
                    df[key_name] = [obj[keys[-1]][()]]

    return df, keys

def load_spectrum(file):
    spectrum = pd.read_csv(file, comment='#', sep=',', header=None, names=['wavelength', 'flux', 'error', 'UL'], encoding='latin-1').apply(pd.to_numeric, errors='coerce')
    return spectrum [spectrum ['wavelength'].notna()]

def load_transit_spectrum(file):
    transit = pd.read_csv(file, comment='#', sep=',', header=None, names=['wavelength', 'depth', 'error'], encoding='latin-1').apply(pd.to_numeric, errors='coerce')
    return transit [transit ['wavelength'].notna()]

def load_filter(file):
    return pd.read_csv(file, comment='#',delim_whitespace=True,names=['wavelength','a','b'])

def load_pkl(file):
    with open(file, 'rb') as f1:
        interpolator = pkl.load(f1)
    return interpolator

def get_interpolator_type(x):
    return str("_".join(x.split('/')[-1].split('_')[:-2]))

def get_closest_interpolator_grid(mass,T_irr,interpolator_type,files_to_load='') :
    if files_to_load == '' :
        files = glob.glob('/travail/cwilkinson/data/interpolators/*/*clouds_5kk*')
    else :
        files = glob.glob('/travail/cwilkinson/data/interpolators/*/'+files_to_load)
    files = [file for file in files if interpolator_type in file]
    
    interp_df = pd.DataFrame()
    interp_df['file'] = files
    
    if ('radiosity' in interpolator_type) or ('transit' in interpolator_type) :
        interp_df['type'] = interp_df['file'].apply(get_interpolator_type)
        interp_df = interp_df[interp_df['type'].str.contains(interpolator_type)]

    interp_df['mass_range_min'] = [eval(interpolator.split('/')[-1].split('_')[-1].replace('.pkl',''))[0] for interpolator in list(interp_df['file'])]
    interp_df['mass_range_max'] = [eval(interpolator.split('/')[-1].split('_')[-1].replace('.pkl',''))[1] for interpolator in list(interp_df['file'])]
    interp_df['mass_range_center'] = interp_df['mass_range_min']+(interp_df['mass_range_max']-interp_df['mass_range_min'])/2
    interp_df['mass_distance'] = np.abs(interp_df['mass_range_center'] - mass)
    interp_df = interp_df[(interp_df['mass_range_min']< mass) & (interp_df['mass_range_max'] > mass)]
    interp_df = interp_df.sort_values(by=['mass_distance'], ascending=True)
    interpolator = load_pkl(interp_df['file'].iloc[0])
    return interpolator, interp_df['file'].iloc[0]

def get_closest_interpolator(files_to_load='') :
    file = glob.glob('/travail/cwilkinson/Exo-Evol/data/interpolators/linear/*/'+files_to_load)[0]
    interpolator = load_pkl(file)
    return interpolator, file
