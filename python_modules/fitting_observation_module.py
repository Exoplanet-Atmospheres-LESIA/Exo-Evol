"""
Created on Sat Aug 12 00:24:02 2023

@author: cwilkinson
"""

import sys
import os
import glob
import numpy as np
import pandas as pd
import emcee
import psutil
import time
import warnings
import multiprocessing
import matplotlib.pyplot as plt
import h5py

from tqdm import tqdm, trange
from multiprocessing import Pool
from joblib import Parallel, delayed
from functools import reduce
import random
from scipy.interpolate import interp1d
from python_modules.interface_module import get_closest_interpolator, load_pkl
from python_modules.instrument_module import identify_observation, model_at_observation, diluted_planet
from python_modules.regression_module import MLmodels
from python_modules.physics_module import get_init_s_extremums, convert_transit

from python_modules.plotting_tools_module import IdentifyTransit

from ultranest import ReactiveNestedSampler

# Suppress warnings for cleaner output
warnings.filterwarnings("ignore")

# Add the parent directory to the system path
sys.path.append("..")

R_J = 69911000          # Jupiter radius in meters  
M_J = 1.898e27          # Jupiter mass in kilograms  
R_s = 696340000         # Solar radius in meters  
M_s = 1.989e30         # Solar mass in kilograms  
R_E = 6371e3           # Earth radius in meters  
M_E = 5.972e24         # Earth mass in kilograms  
G_u = 6.674e-11        # Gravitational constant (m^3 kg^-1 s^-2)  
Na = 6.022e23          # Avogadro's number (mol^-1)  
Kb = 1.380649e-23      # Boltzmann constant (J K^-1)  
R_g = 8.31446261815324 # Universal gas constant (J mol^-1 K^-1)  
sigma = 5.67e-8        # Stefan-Boltzmann constant (W m^-2 K^-4)  

class RetrieveObservation:
    """
    A class to perform MCMC retrievals for exoplanet observations.
    """

    def __init__(self, initial_parameters, T_irr, bounds, n_walkers, n_iterations,
                 observed_spectrum, spectrum_name, arguments, regressor_type, interpolator, output_file,
                 distance_planet, planet_age, planet_age_error, reset, debug=False):
        """
        Initialize the retrieval object with parameters, bounds, and other metadata.
        """
        self.initial_parameters = initial_parameters
        self.T_irr = T_irr
        self.bounds = bounds
        self.n_walkers = n_walkers
        self.n_iterations = n_iterations
        self.observed_spectrum = observed_spectrum
        self.spectrum_name = spectrum_name
        self.arguments = arguments
        self.output_file = output_file
        self.distance_planet = distance_planet
        self.planet_age = planet_age
        self.planet_age_error = planet_age_error
        self.reset = reset
        self.debug = debug
        self.additional_output_file = f"{self.output_file}/{self.spectrum_name}_{self.arguments}_additional_data.h5"
        print('Initializing Entropy')
        self.cold, self.hot = get_init_s_extremums()

        # Initialize MCMC starting positions
        print('Initializing positions')
        self.init_positions()

        # Process observed spectrum
        print('Identifying observation')
        self.observed_spectrum = identify_observation(observed_spectrum)

        # Load interpolator and wavelength grid
        self.regressor_type = regressor_type
        if regressor_type != 'ML':
            self.interpolator, self.interpolator_file = get_closest_interpolator(
                files_to_load=interpolator
            )
            self.wavelength = load_pkl(
                f'./../data/interpolators/flux/example_'
                + '_'.join(self.interpolator_file.split('_')[-2:])
            )['wavelength_up'].iloc[0]
        else:
            ML = MLmodels(n_estimators=1000)
            #target = 'flux_photo'
            #model_flux = ML.load_model(target)
            #target = 'Radius_Jup'
            #model_radius = ML.load_model(target)
            #target = 'age'
            #model_t = ML.load_model(target)
            #self.interpolator = lambda x: np.concatenate((ML.use_model(model_flux,x),np.expand_dims(ML.use_model(model_radius,x), axis=1),np.expand_dims(ML.use_model(model_t,x), axis=1)), axis=1)
            print('Loading model')
            model_combined = ML.load_model('flux_radius_age')
            self.interpolator = lambda x: ML.use_model(model_combined,x)
            self.wavelength = ML.wavelength
            print('Loaded model')
            
            # Print all variables for debugging
            print("Initial Parameters:", self.initial_parameters)
            print("T_irr:", self.T_irr)
            print("Bounds:", self.bounds)
            print("Number of Walkers:", self.n_walkers)
            print("Number of Iterations:", self.n_iterations)
            print("Observed Spectrum:", self.observed_spectrum)
            print("Spectrum Name:", self.spectrum_name)
            print("Arguments:", self.arguments)
            print("Regressor Type:", self.regressor_type)
            print("Interpolator:", self.interpolator)
            print("Output File:", self.output_file)
            print("Distance to Planet:", self.distance_planet)
            print("Planet Age:", self.planet_age)
            print("Planet Age Error:", self.planet_age_error)
            print("Reset:", self.reset)
            print("Additional Output File:", self.additional_output_file)
            print("Wavelength:", self.wavelength)

    def MCMC(self):
        """
        Perform the MCMC retrieval using the emcee EnsembleSampler.
        """
        filename = f"{self.output_file}/{self.spectrum_name}_{self.arguments}.h5"

        # Check if previous MCMC results exist
        if not glob.glob(filename):
            self.reset = True
            print('H5 file not found, restarting MCMC')

        backend = emcee.backends.HDFBackend(filename)

        # Reset backend if needed
        if self.reset:
            backend.reset(self.n_walkers, len(self.initial_parameters))

        print(f"Initial size: {backend.iteration}")
        print(f"{multiprocessing.cpu_count()} CPUs available")

        # Create the sampler
        sampler = emcee.EnsembleSampler(
            self.n_walkers,
            len(self.initial_parameters),
            self.ln_posterior_batch,
            vectorize=True,
            backend=backend
        )

        # Run MCMC
        if self.reset:
            if len(glob.glob(self.additional_output_file)) > 0:
                os.remove(self.additional_output_file)
            sampler.run_mcmc(self.initial_positions, self.n_iterations, store=True, progress=True)
        else:
            try:
                sampler.run_mcmc(None, self.n_iterations, progress=True)
            except RuntimeError:
                print('Run failed, resetting MCMC')
                backend.reset(self.n_walkers, len(self.initial_parameters))
                sampler.run_mcmc(self.initial_positions, self.n_iterations, store=True, progress=True)
                
    def save_additional_data(self, theta_batch, fluxes, radii, ages, log_likelihoods) :
        """
        Save additional data (log likelihoods, parameters, fluxes, etc.) to an HDF5 file.

        Parameters:
        - theta_batch: Array of sampled parameters.
        - fluxes: Array of flux values.
        - radii: Array of radii values.
        - ages: Array of ages.
        - log_likelihoods: Array of log-likelihoods for the batch.
        """
        
        # To store additional data
        with h5py.File(self.additional_output_file, "a") as h5file:
            # Helper function to handle dataset creation or resizing
            def append_to_dataset(name, data):
                if name not in h5file:
                    # Create a resizable dataset
                    h5file.create_dataset(name, 
                                        data=data, 
                                        maxshape=(None,) + data.shape[1:], 
                                        chunks=True)
                else:
                    # Resize the dataset to accommodate new data
                    current_size = h5file[name].shape[0]
                    new_size = current_size + data.shape[0]
                    h5file[name].resize((new_size,) + h5file[name].shape[1:])
                    
                    # Append new data
                    h5file[name][current_size:new_size] = data

            # Append data for each variable
            append_to_dataset("log_probabilities", log_likelihoods)
            append_to_dataset("theta_batch", theta_batch)
            append_to_dataset("fluxes", fluxes)
            append_to_dataset("radii", radii)
            append_to_dataset("ages", ages)

            # Flush changes to disk
            h5file.flush()

    def init_positions(self):
        """
        Initialize MCMC walker positions based on initial parameters and bounds.
        """
        self.initial_positions = []
        for _ in range(self.n_walkers):
            new_position = []
            for k, position in enumerate(self.initial_parameters) :
                pos = position * (1 + np.random.normal(0, 0.1))
                if k < np.shape(self.initial_parameters)[0] - 1 :
                    while (pos < self.bounds[k][0]) or (pos > self.bounds[k][1]):
                        pos = position * (1 + np.random.normal(0, 0.1))
                else :
                    while (pos < self.cold(new_position[0])) or (pos > self.hot(new_position[0])):
                        pos = position * (1 + np.random.normal(0, 0.1))
                new_position.append(pos)
                print(f'Parameter {k} : {pos}')
            self.initial_positions.append(new_position)
        self.initial_positions = np.array(self.initial_positions)
        if self.debug:
            print("Initial positions shape:", self.initial_positions.shape)

    def ln_posterior(self, parameters):
        """
        Compute the posterior probability for a given parameter set.
        """
        prior = self.ln_prior(parameters)
        if not np.isinf(prior):
            likelihood = self.ln_likelihood(parameters) + prior
            return likelihood
        return -np.inf
    
    def ln_posterior_batch(self, theta_batch):
        """
        Compute the posterior and return additional outputs.
        """
        if self.debug:
            print("Shape of theta_batch:", np.shape(theta_batch))
            print("theta_batch:", theta_batch)
        
        priors = np.array([self.ln_prior(theta) for theta in theta_batch])
        valid_indices = np.isfinite(priors)
        theta_batch = np.insert(theta_batch, 1, self.T_irr, axis=1)
        log_likelihoods = np.full(theta_batch.shape[0], -np.inf)

        flux_radius_batch = self.interpolator(theta_batch)
        fluxes = flux_radius_batch[:, :-2]
        radii = flux_radius_batch[:, -2]
        ages = flux_radius_batch[:, -1]
        if self.debug:
            print("fluxes :", fluxes)
            print("radii :", radii)
            print("ages :", ages)

        models = [
            model_at_observation(
                self.observed_spectrum, self.wavelength, flux[:len(self.wavelength)],
                flux[len(self.wavelength):]
            )
            for flux in fluxes
        ]
        chi_squared_values = []
        for model, radius, age in zip(models, radii, ages):
            model['model_flux'] = diluted_planet(model['model_flux'], radius, self.distance_planet)
            chi_squared = self.compute_chi_squared(model, age)
            chi_squared_values.append(chi_squared if not np.isnan(chi_squared) else np.inf)
        
        log_likelihoods[valid_indices] = -0.5 * np.array(chi_squared_values)[valid_indices] + priors[valid_indices]
        
        if self.debug:
            print("ln_posterior_batch output shape:", log_likelihoods.shape)
            print("log_likelihoods:", log_likelihoods)
            
        self.save_additional_data(theta_batch, fluxes, radii, ages, log_likelihoods)

        return log_likelihoods

    def ln_prior(self, parameters):
        """
        Define a uniform prior for the parameters.
        """
        Mass, Met, core, T_int, f_sed, kzz, S0, *offsets = parameters
        if not (self.bounds[0][0] <= Mass <= self.bounds[0][1]):
            if self.debug:
                print('Mass out of bounds')
            return -np.inf
        if not (self.bounds[1][0] <= Met <= self.bounds[1][1]):
            if self.debug:
                print(Met)
                print('Met out of bounds')
            return -np.inf
        if not (self.bounds[2][0] <= core <= self.bounds[2][1]):
            if self.debug:
                print('core out of bounds') 
            return -np.inf
        if not (self.bounds[3][0] <= T_int <= self.bounds[3][1]):
            if self.debug:
                print('T_int out of bounds')
            return -np.inf
        if not (self.bounds[4][0] <= f_sed <= self.bounds[4][1]):
            if self.debug:
                print('f_sed out of bounds')
            return -np.inf
        if not (self.bounds[5][0] <= kzz <= self.bounds[5][1]):
            if self.debug:
                print('kzz out of bounds')
            return -np.inf
        if not (self.cold(Mass) <= S0 <= self.hot(Mass)):
            if self.debug:
                print('S0 out of bounds')
            return -np.inf
        return 0

    def compute_chi_squared(self, model, evaluated_age):
        """
        Compute the chi-squared statistic for a given model and age.
        """
        #self.temp_plot(model,0)
        
        if model['model_flux'].isnull().sum() > 0:
            if self.debug:
                print('Model flux is null')
            return np.inf
        
        if 'UL' in self.arguments:
            model_UL = model[model['UL'] == 1]
            model_UL = model_UL[model_UL['flux']<model_UL['model_flux']]
            if len(model_UL) > 0:
                if self.debug:
                    print(model_UL)
                    print('UL detected')
                return np.inf
        diff = np.array(model[model['UL'] != 1]['flux']) - model[model['UL'] != 1]['model_flux']
        chi_squared = np.sum(diff**2 / np.array(model[model['UL'] != 1]['error'])**2)
        if self.debug:
            print('model :', model)
            print('diff:', diff)
            print(f'Chi2 without age: {chi_squared}')
            
        
        if self.planet_age > 1e-4 :
            if self.debug:
                print('Computing X2 with age')
                print(f'Evaluated age: {evaluated_age/1e6} Myr')
            chi_squared += (evaluated_age/1e6 - self.planet_age)**2 / (self.planet_age_error)**2
            
        if (random.randint(1,1000) == 1) & (not np.isinf(chi_squared)):
            self.temp_plot(model,chi_squared)
            
        return chi_squared        
    
    def temp_plot(self,model,Chi2):
        """
        Plot samples of the model to check for convergence.
        """
        
        fig, ax = plt.subplots()
        plt.plot(
                model['wavelength'],
                model['model_flux'],
                'o',
                color='k',
                alpha=1)

        observed_flux = model[model['UL'] == 0]
        for instrument in list(set(observed_flux['instrument'])):
            observed_flux_intrument = observed_flux[observed_flux['instrument'] == instrument]
            ax.errorbar(
                observed_flux_intrument['wavelength'],
                observed_flux_intrument['flux'],
                fmt='o',
                yerr=observed_flux_intrument['error'],
                alpha=1,
                label=instrument)

        observed_flux_UL = model[model['UL'] != 0]
        for instrument in list(set(observed_flux_UL['instrument'])):
            observed_flux_UL_instrument = observed_flux_UL[observed_flux_UL['instrument'] == instrument]
            for wavelength, flux in zip(
                    observed_flux_UL_instrument['wavelength'], observed_flux_UL_instrument['flux']):
                ax.annotate(
                    '', xy=(
                        wavelength, (flux * 0.9)), xytext=(
                        wavelength, flux), arrowprops=dict(
                        facecolor='r', shrink=0., headwidth=10))
        
        plt.yscale('log')
        plt.title(f'Chi2 = {Chi2}')
        plt.savefig(f"../temp/plots/temp_plot.png")
        plt.close()

        return  
    
    
class RetrieveTransit:
    """
    A generic class to host retrievals methods for transit observations.
    """

    def __init__(self, T_irr, planet_mass, bounds,
                 observed_spectrum, spectrum_name, output_file,
                 radius_star, planet_age, planet_age_error, parameters_name = ["$M_{tot}$", "Met", "$M_c$", "$T_{int}$", "$f_{sed}$", "$k_{zz}$"], arguments='', debug=False, normalise_priors=True):
        """
        Initialize the retrieval object with parameters, bounds, and other metadata.
        """
        self.T_irr = T_irr
        self.planet_mass = planet_mass
        self.bounds = bounds
        self.observed_spectrum = observed_spectrum
        self.spectrum_name = spectrum_name
        self.arguments = arguments
        self.output_file = output_file
        self.radius_star = radius_star
        self.planet_age = planet_age
        self.planet_age_error = planet_age_error
        self.parameters_name = parameters_name
        self.debug = debug
        self.normalise_priors = normalise_priors
        self.additional_output_file = f"{self.output_file}/{self.spectrum_name}_{self.arguments}_additional_data.h5"

        print('Initializing Entropy')
        self.cold, self.hot = get_init_s_extremums()

        # Load interpolator and wavelength grid
        ML = MLmodels(n_estimators=1000)
        #target = 'flux_photo'
        #model_flux = ML.load_model(target)
        #target = 'Radius_Jup'
        #model_radius = ML.load_model(target)
        #target = 'age'
        #model_t = ML.load_model(target)
        #self.interpolator = lambda x: np.concatenate((ML.use_model(model_flux,x),np.expand_dims(ML.use_model(model_radius,x), axis=1),np.expand_dims(ML.use_model(model_t,x), axis=1)), axis=1)
        print('Loading model')
        model_combined = ML.load_model('depth_radius')
        self.interpolator = lambda x: ML.use_model(model_combined,x)
        self.wavelength = ML.wavelength
        print('Loaded model')

        def interp_spectrum(depth):
            return interp1d(self.wavelength,depth)(self.observed_spectrum['wavelength'])

        self.spectrum_interpolator = interp_spectrum
    
    def ln_posterior(self, parameters):
        """
        Compute the posterior probability for a given parameter set.
        """
        prior = self.ln_prior(parameters)
        if not np.isinf(prior):
            likelihood = self.ln_likelihood(parameters) + prior
            return likelihood
        return -np.inf
    
    def ln_posterior_batch(self, theta_batch):
        """
        Compute the posterior and return additional outputs.
        """
        if self.debug:
            print("Shape of theta_batch:", np.shape(theta_batch))
            print("theta_batch:", theta_batch)
            
        priors = np.array([self.ln_prior(theta) for theta in theta_batch])
        valid_indices = np.isfinite(np.sum(priors, axis=1)) # If one value in prior is np.inf, the wlaker has not a valid theta
        theta_batch = np.insert(theta_batch, 1, self.T_irr, axis=1)
        log_likelihoods = np.full(theta_batch.shape[0], -np.inf)

        depth_radius_batch = self.interpolator(theta_batch)
        depths = depth_radius_batch[:, :-1]
        radii = depth_radius_batch[:, -1]
        ages = np.zeros(np.shape(radii))
        if self.debug:
            print("depth :", depths)
            print("radii :", radii)
            print("ages :", ages)

        depths = np.array([
                interp1d(self.wavelength,depth)(self.observed_spectrum['wavelength'])*radius**2/(0.9*R_s)**2
            for depth, radius in zip(depths, radii)
        ])
        chi_squared_values = []
        for parameters, model, radius, age in zip(theta_batch, depths, radii, ages):
            chi_squared = self.compute_chi_squared(parameters, model, age)
            chi_squared_values.append(chi_squared if not np.isnan(chi_squared) else np.inf)
        
        log_likelihoods[valid_indices] = -0.5 * np.array(chi_squared_values)[valid_indices] + np.sum(priors[valid_indices], axis=1)
        
        if self.debug:
            print("ln_posterior_batch output shape:", log_likelihoods.shape)
            print("log_likelihoods:", log_likelihoods)
            
        self.save_additional_data(theta_batch, depths, radii, ages, log_likelihoods)

        return log_likelihoods
    
    def ln_prior(self, parameters):
        """
        Define priors for the parameters.
        """
        Mass, Met, core, T_int, f_sed, kzz, *offsets = parameters
        ln_prior_values = np.zeros(parameters.shape)

        # Mass
        if not (self.bounds[0][0] <= Mass <= self.bounds[0][1]):
            if self.debug:
                print('Mass out of bounds')
            ln_prior_values[0] = -np.inf
        else:
            ln_prior_values[0] = -np.log(self.bounds[0][1] - self.bounds[0][0])

        # Metallicity
        if not (self.bounds[1][0] <= Met <= self.bounds[1][1]):
            if self.debug:
                print(Met)
                print('Met out of bounds')
            ln_prior_values[1] = -np.inf
        else:
            ln_prior_values[1] = -np.log(self.bounds[1][1] - self.bounds[1][0])

        # Core mass
        if not (self.bounds[2][0] <= core <= self.bounds[2][1]):
            if self.debug:
                print('core out of bounds') 
            ln_prior_values[2] = -np.inf
        else:
            Mcore_max_value = max(self.bounds[2][1], Mass)
            ln_prior_values[2] = -np.log(Mcore_max_value - self.bounds[2][0])

        # T_int
        if not (self.bounds[3][0] <= T_int <= self.bounds[3][1]):
            if self.debug:
                print('T_int out of bounds')
            ln_prior_values[3] = -np.inf
        else:
            ln_prior_values[3] = -np.log(self.bounds[3][1] - self.bounds[3][0])

        # f_sed
        if not (self.bounds[4][0] <= f_sed <= self.bounds[4][1]):
            if self.debug:
                print('f_sed out of bounds')
            ln_prior_values[4] = -np.inf
        else:
            ln_prior_values[4] = -np.log(self.bounds[4][1] - self.bounds[4][0])

        # K_zz
        if not (self.bounds[5][0] <= kzz <= self.bounds[5][1]):
            if self.debug:
                print('kzz out of bounds')
            ln_prior_values[5] = -np.inf
        else:
            ln_prior_values[5] = -np.log(self.bounds[5][1] - self.bounds[5][0])

        # Entropy check
        #if not (self.cold(Mass) <= S0 <= self.hot(Mass)):
        #    if self.debug:
        #        print('S0 out of bounds')
        #    return -np.inf

        if self.normalise_priors:
            return ln_prior_values
        else:
            return 0
        
    def _prior_transform(self, cube):
        """
        Defining priors.
        Transforms a parameter random cube theta ~ U[0,1)^N into a proper prior cube theta ~ pi.
        Cf UltraNest doc at https://johannesbuchner.github.io/UltraNest/priors.html
        """
        transformed_parameters = np.empty_like(cube) # Array to fill with new theta values to replace 'cube'

        # Mass
        transformed_parameters[0] = self.bounds[0][0] + cube[0] * (self.bounds[0][1] - self.bounds[0][0]) # Uniform distribution between bounds

        # Metallicity
        transformed_parameters[1] = self.bounds[1][0] + cube[1] * (self.bounds[1][1] - self.bounds[1][0]) # Uniform distribution between bounds

        # Core mass
        Mcore_max_value = max(self.bounds[2][1], transformed_parameters[0])
        transformed_parameters[2] = self.bounds[2][0] + cube[2] * (Mcore_max_value - self.bounds[2][0]) # Uniform distribution between bounds, with caution not to exceed the total planet mass

        # T_int
        transformed_parameters[3] = self.bounds[3][0] + cube[3] * (self.bounds[3][1] - self.bounds[3][0]) # Uniform distribution between bounds

        # f_sed
        transformed_parameters[4] = self.bounds[4][0] + cube[4] * (self.bounds[4][1] - self.bounds[4][0]) # Uniform distribution between bounds

        # K_zz
        transformed_parameters[5] = self.bounds[5][0] + cube[5] * (self.bounds[5][1] - self.bounds[5][0]) # Uniform distribution between bounds

        return transformed_parameters
    
    def _vectorized_prior_transform(self, cube):
        """
        Defining priors.
        Transforms a set of parameter random cubes theta_i ~ U[0,1)^N into a proper ensemble of prior cubes theta_i ~ pi.
        Cf UltraNest doc at https://johannesbuchner.github.io/UltraNest/priors.html
        """
        transformed_parameters = np.empty_like(cube) # Array to fill with new theta values to replace 'cube'

        # Mass
        transformed_parameters[:, 0] = self.bounds[0][0] + cube[:, 0] * (self.bounds[0][1] - self.bounds[0][0]) # Uniform distribution between bounds

        # Metallicity
        transformed_parameters[:, 1] = self.bounds[1][0] + cube[:, 1] * (self.bounds[1][1] - self.bounds[1][0]) # Uniform distribution between bounds

        # Core mass
        Mcore_max_value = np.maximum(self.bounds[2][1], transformed_parameters[:, 0])
        transformed_parameters[:, 2] = self.bounds[2][0] + cube[:, 2] * (Mcore_max_value - self.bounds[2][0]) # Uniform distribution between bounds, with caution not to exceed the total planet mass

        # T_int
        transformed_parameters[:, 3] = self.bounds[3][0] + cube[:, 3] * (self.bounds[3][1] - self.bounds[3][0]) # Uniform distribution between bounds

        # f_sed
        transformed_parameters[:, 4] = self.bounds[4][0] + cube[:, 4] * (self.bounds[4][1] - self.bounds[4][0]) # Uniform distribution between bounds

        # K_zz
        transformed_parameters[:, 5] = self.bounds[5][0] + cube[:, 5] * (self.bounds[5][1] - self.bounds[5][0]) # Uniform distribution between bounds

        return transformed_parameters
    
    def _log_likelihood(self, parameters):
        """
        Return the log likelihood of a parameters set
        """
        parameters = np.insert(parameters, 1, self.T_irr)

        depth_radius = self.interpolator(np.array([parameters,]))
        depth = depth_radius[0,:-1]
        radius = depth_radius[0,-1]
        age = 0

        depths = np.array(
                interp1d(self.wavelength,depth)(self.observed_spectrum['wavelength'])*radius**2/(0.9*R_s)**2
        )

        return -0.5 * self.compute_chi_squared(parameters, depths, age)
    
    def _vectorized_log_likelihood(self, parameters):
        """
        Return the log likelihood of an ensemble of parameters sets
        """
        parameters_batch = np.insert(parameters, 1, self.T_irr, axis=1)

        depth_radius_batch = self.interpolator(parameters_batch)
        depths = depth_radius_batch[:,:-1]
        radii = np.expand_dims(depth_radius_batch[:,-1], axis=1) # Expand dims serves to match dimensions with depths and allow broadcasting
        ages = np.zeros(np.shape(radii))

        depths = np.apply_along_axis(self.spectrum_interpolator, 1, depths) * radii /(0.9*R_s**2)    

        chi_squared_values_batch = self._vectorized_compute_chi_squared(parameters_batch, depths, ages)

        return -0.5 * chi_squared_values_batch
    
    def compute_chi_squared(self, parameters, model, evaluated_age):
        """
        Compute the chi-squared statistic for a given model and age.
        """
        #self.temp_plot(model,0)
        
        if np.isnan(model).sum() > 0:
            if self.debug:
                print('Model flux is null')
            return np.inf
        
        diff = np.array(self.observed_spectrum['depth']) - model
        chi_squared = np.sum(diff**2 / self.observed_spectrum['error']**2)
        if self.debug:
            print('model :', model)
            print('diff:', diff)
            print(f'Chi2 without age: {chi_squared}')
            
        
        if (self.planet_age > 1e-4) & (evaluated_age > 1e-4):
            chi_squared += (evaluated_age/1e6 - self.planet_age)**2 / (self.planet_age_error)**2
            if self.debug:
                print('Computing X2 with age')
                print(f'Evaluated age: {evaluated_age/1e6} Myr')
                print(f'Chi2 with age: {chi_squared}')
            
        if self.planet_mass[0] > 1e-4:
            chi_squared += (parameters[0] - self.planet_mass[0])**2 / (self.planet_mass[1])**2
            if self.debug:
                print('Computing X2 with mass')
                print(f'Input mass: {parameters[0]} Jup')
                print(f'Chi2 with Mass: {chi_squared}')
            
        if (random.randint(1,1000) == 1) & (not np.isinf(chi_squared)):
            self.temp_plot(model,chi_squared)
            
        return chi_squared
    
    def _vectorized_compute_chi_squared(self, parameters_batch, models, evaluated_ages):
        """
        Compute the chi-squared statistic for a given model and age.
        """
        #self.temp_plot(model,0)
        
        if np.isnan(models).sum() > 0:
            if self.debug:
                print('Model flux is null')
            return np.inf

        diffs = np.expand_dims(np.array(self.observed_spectrum['depth']), axis=0) - models  #np.array(observed_specrum['width']) is of dimension (207,) where we need (1, 207) for correct np broadcasting

        chi_squared_batch = np.sum(diffs**2 / np.expand_dims(np.array(self.observed_spectrum['error']), axis=0)**2, axis=1) # idem for np.array(observed_specrum['error']) 

        if self.debug:
            print('model :', models)
            print('diff:', diffs)
            print(f'Chi2 without age: {chi_squared_batch}')
            
        """ # Evaluating Chi2 with ages # NOT FOR NOW
        if (self.planet_age > 1e-4) & (evaluated_age > 1e-4):
            chi_squared += (evaluated_age/1e6 - self.planet_age)**2 / (self.planet_age_error)**2
            if self.debug:
                print('Computing X2 with age')
                print(f'Evaluated age: {evaluated_age/1e6} Myr')
                print(f'Chi2 with age: {chi_squared}')
        """
        
        # We could use planet mass estimation in priors instead of affecting Chi2 !!!
        """ # So we do not take mass into account for Chi2 estimation
        if self.planet_mass[0] > 1e-4:
            chi_squared += (parameters_batch[:, 0] - self.planet_mass[0])**2 / (self.planet_mass[1])**2
            if self.debug:
                print('Computing X2 with mass')
                print(f'Input mass: {parameters_batch[:, 0]} Jup')
                print(f'Chi2 with Mass: {chi_squared}')
        """
        
        """ Disabling temp_plot for efficiency (no random number to process)
        if (random.randint(1,1000) == 1) & (not np.isinf(chi_squared_batch)):
            self.temp_plot(models[0],chi_squared_batch)
        """

        return chi_squared_batch
    
    def temp_plot(self,model,Chi2):
        """
        Plot samples of the model to check for convergence.
        """
        
        fig, ax = plt.subplots(figsize=(16, 8))
        ax.errorbar(self.observed_spectrum['wavelength'],
                    self.observed_spectrum['depth'],
                    yerr=self.observed_spectrum['error'],
                    fmt='o')
        
        ax.plot(self.observed_spectrum['wavelength'],
                model,
                'o')

        plt.yscale('log')
        plt.title(f'Chi2 = {Chi2}')
        plt.savefig(f"../temp/plots/transit_temp_plot.png")
        plt.close()

        return  
    

class RetrieveTransit_MCMC(RetrieveTransit):
    """
    A class to perform MCMC retrievals for transit observations.
    """

    def __init__(self, initial_parameters, T_irr, planet_mass, bounds, n_walkers, n_iterations, observed_spectrum, spectrum_name, output_file,  radius_star, planet_age, planet_age_error, reset, arguments='', debug=False, normalise_priors=True):
        """
        Initialize the retrieval object with parameters, bounds, and other metadata.
        """
        # Generic variables
        RetrieveTransit.__init__(self, T_irr, planet_mass, bounds, observed_spectrum, spectrum_name, output_file, radius_star, planet_age, planet_age_error, arguments, debug, normalise_priors)

        # MCMC specific variables
        self.initial_parameters = initial_parameters
        self.n_walkers = n_walkers
        self.n_iterations = n_iterations
        self.reset = reset

        self.additional_output_file = f"{self.output_file}/{self.spectrum_name}_{self.arguments}_additional_data.h5"
        print('Initializing Entropy')
        self.cold, self.hot = get_init_s_extremums()

        # Initialize MCMC starting positions
        print('Initializing positions')
        self.init_positions()
        
        # Print all variables for debugging
        print("Initial Parameters:", self.initial_parameters)
        print("T_irr:", self.T_irr)
        print("Bounds:", self.bounds)
        print("Number of Walkers:", self.n_walkers)
        print("Number of Iterations:", self.n_iterations)
        print("Observed Spectrum:", self.observed_spectrum)
        print("Spectrum Name:", self.spectrum_name)
        print("Arguments:", self.arguments)
        print("Interpolator:", self.interpolator)
        print("Output File:", self.output_file)
        print("Planet Age:", self.planet_age)
        print("Planet Age Error:", self.planet_age_error)
        print("Reset:", self.reset)
        print("Additional Output File:", self.additional_output_file)
        print("Wavelength:", self.wavelength)
            
    def MCMC(self):
        """
        Perform the MCMC retrieval using the emcee EnsembleSampler.
        """
        filename = f"{self.output_file}/{self.spectrum_name}_{self.arguments}.h5"

        # Check if previous MCMC results exist
        if not glob.glob(filename):
            self.reset = True
            print('H5 file not found, restarting MCMC')

        backend = emcee.backends.HDFBackend(filename)

        # Reset backend if needed
        if self.reset:
            backend.reset(self.n_walkers, len(self.initial_parameters))

        print(f"Initial size: {backend.iteration}")
        print(f"Initial parameters: {self.initial_positions}")
        print(f"{multiprocessing.cpu_count()} CPUs available")

        # Create the sampler
        sampler = emcee.EnsembleSampler(
            self.n_walkers,
            len(self.initial_parameters),
            self.ln_posterior_batch,
            vectorize=True,
            backend=backend
        )

        # Run MCMC
        if self.reset:
            if len(glob.glob(self.additional_output_file)) > 0:
                os.remove(self.additional_output_file)
            sampler.run_mcmc(self.initial_positions, self.n_iterations, store=True, progress=True)
        else:
            try:
                sampler.run_mcmc(None, self.n_iterations, progress=True)
            except RuntimeError:
                print('Run failed, resetting MCMC')
                backend.reset(self.n_walkers, len(self.initial_parameters))
                sampler.run_mcmc(self.initial_positions, self.n_iterations, store=True, progress=True)
                
    def save_additional_data(self, theta_batch, depths, radii, ages, log_likelihoods) :
        """
        Save additional data (log likelihoods, parameters, fluxes, etc.) to an HDF5 file.

        Parameters:
        - theta_batch: Array of sampled parameters.
        - depths: Array of flux values.
        - radii: Array of radii values.
        - ages: Array of ages.
        - log_likelihoods: Array of log-likelihoods for the batch.
        """
        
        # To store additional data
        with h5py.File(self.additional_output_file, "a") as h5file:
            # Helper function to handle dataset creation or resizing
            def append_to_dataset(name, data):
                if name not in h5file:
                    h5file.create_dataset(name, 
                                    data=data, 
                                    maxshape=(None,) + data.shape[1:], 
                                    chunks=True)
                else:
                    # Resize the dataset to accommodate new data
                    current_size = h5file[name].shape[0]
                    new_size = current_size + data.shape[0]
                    h5file[name].resize((new_size,) + h5file[name].shape[1:])
                    # Append new data
                    h5file[name][current_size:new_size] = data

            # Append data for each variable
            append_to_dataset("log_probabilities", log_likelihoods)
            append_to_dataset("theta_batch", theta_batch)
            append_to_dataset("depths", depths)
            append_to_dataset("radii", radii)
            append_to_dataset("ages", ages)
            append_to_dataset("n_walkers", np.array([self.n_walkers]))
            append_to_dataset("n_iterations", np.array([self.n_iterations]))

            # Flush changes to disk
            h5file.flush()

    def init_positions(self):
        """
        Initialize MCMC walker positions based on initial parameters and bounds.
        """
        self.initial_positions = []
        for _ in range(self.n_walkers):
            new_position = []
            for k, position in enumerate(self.initial_parameters) :
                pos = position * (1 + np.random.normal(0, 0.1))
                #if k < np.shape(self.initial_parameters)[0] - 1 :
                if k < np.shape(self.initial_parameters)[0] :
                    while (pos < self.bounds[k][0]) or (pos > self.bounds[k][1]):
                        pos = position * (1 + np.random.normal(0, 0.1))
                else :
                    while (pos < self.cold(new_position[0])) or (pos > self.hot(new_position[0])):
                        pos = position * (1 + np.random.normal(0, 0.1))
                new_position.append(pos)
                print(f'Parameter {k} : {pos}')
            self.initial_positions.append(new_position)
        self.initial_positions = np.array(self.initial_positions)
        if self.debug:
            print("Initial positions shape:", self.initial_positions.shape)

class RetrieveTransit_UltraNest(RetrieveTransit):

    def ultranest(self, n_iterations_max=100000, resume="resume"):
        self.ultranest_sampler = ReactiveNestedSampler(self.parameters_name, self._vectorized_log_likelihood, self._vectorized_prior_transform, log_dir=os.path.join(self.output_file, "..", "..", "ultranest_retrievals", self.spectrum_name), vectorized=True, resume=resume)

        results = self.ultranest_sampler.run(max_iters=n_iterations_max)

        return results
