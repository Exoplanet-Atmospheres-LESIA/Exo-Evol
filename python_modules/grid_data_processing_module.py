"""
Created on unknown

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob
import numpy as np
import pandas as pd

from scipy.interpolate import interp1d
from itertools import product
import psutil

import pyarrow.feather as feather
import pickle as pkl
import h5py
from tqdm import tqdm
from joblib import Parallel, delayed, parallel_backend

from python_modules.physics_module import wavenumber_to_wavelength, convert_radiosity, ds_dt,  convert_transit, compute_mass
from python_modules.EOS_module import load_thermo_tables_CMS, get_entropy
from python_modules.instrument_module import read_filter_docs, evaluate_all_filters, get_wl_all_filters
from python_modules.numerical_tools_module import interpolate_negatives_rowwise

import warnings
warnings.filterwarnings("ignore")

R_J = 69911000
M_J = 1.898*1e27
R_s = 696340000
M_s = 1.989*1e30
R_J = 69911000
M_J = 1.898*1e27
R_E = 6371*1e3
M_E = 5.972*1e24
G_u = 6.674*1e-11
Na = 6.022*1e23
Kb = 1.380649*1e-23
R_g = 8.31446261815324
sigma = 5.67*1e-8

class ProcessGrid:
    def __init__(self, grid_files, split=24):
        self.grid_files = glob.glob('../data/planets/'+grid_files)
        self.split = split
        print('Number of files found :', len(self.grid_files))
        self.organiser = self.get_file_properties()
        self.mass_index_values = np.sort(list(set(self.organiser.index)))
        self.check_folders()
        self.filters = read_filter_docs()
        self.wavelength_photo = get_wl_all_filters(self.filters)
        
    @staticmethod
    def clear_files() :
        os.system('rm -r ../data/current_exoevol_grid')
        os.system('rm -r ../data/current_exoevol_grid_eq')
        print('Files removed')
        return

    @staticmethod
    def get_parameters(x):
        values = x.split('/')[-1].replace('.pkl', '').split('_')
        mass, T_int, T_irr, met, core, f_sed, kzz, resolution = map(float, values[:7]+[values[8]])
        if core < 1:
            core *= mass * M_J / M_E
        EOS = values[7]
        equilibrium = '_eq_' in x
        file_type = x.split('/')[-2]
        return mass, T_irr, T_int, met, core, f_sed, kzz, EOS, resolution, equilibrium, file_type
    
    @staticmethod
    def get_gravity(pressure,gravity,P=1) :    
        return float(interp1d(1e-5 * np.array(pressure),np.array(gravity))(P))    
    
    @staticmethod
    def get_temperature(pressure,temperature,P=1):
        return float(interp1d(1e-5 * np.array(pressure),np.array(temperature))(P))
    
    @staticmethod
    def interpolate_to_standard(T_array, P_array, P_new=np.logspace(-4,10,1000)):
        T_new = np.interp(P_new, P_array, T_array)
        return T_new
    
    def check_folders(self) :
        for eos in list(set(self.organiser['eos'])) :
            print('Status of output file : ', glob.glob('../data/current_exoevol_grid/{}'.format(eos)))
            if len(glob.glob('../data/current_exoevol_grid/{}'.format(eos))) == 0 :
                if len(glob.glob('../data/current_exoevol_grid')) == 0 :
                    os.system('mkdir ../data/current_exoevol_grid')
                os.system('mkdir ../data/current_exoevol_grid/{}'.format(eos))
        
        for eos in list(set(self.organiser['eos'])) :
            print('Status of output file : ', glob.glob('../data/current_exoevol_grid_eq/{}'.format(eos)))
            if len(glob.glob('../data/current_exoevol_grid_eq/{}'.format(eos))) == 0 :
                if len(glob.glob('../data/current_exoevol_grid_eq')) == 0 :
                    os.system('mkdir ../data/current_exoevol_grid_eq')
                os.system('mkdir ../data/current_exoevol_grid_eq/{}'.format(eos))
        return
        
    def get_file_properties(self) :
        organiser = pd.DataFrame()
        organiser['file'] = self.grid_files
        organiser['size'] = [os.stat(file).st_size for file in self.grid_files]
        organiser['date'] = [os.stat(file).st_mtime for file in self.grid_files]
        organiser['mass'], organiser['T_irr'], organiser['T_int'], organiser['Met'], organiser['core'], organiser['f_sed'], organiser['kzz'], organiser['eos'], organiser['resolution'], organiser['equilibrium'], organiser['file_type'] = zip(*organiser['file'].map(self.get_parameters))
        [os.remove(file) for file in organiser[(organiser['T_irr']<0) | (organiser['T_int']<0)]['file']]
        organiser = organiser[(organiser['T_irr']>0) & (organiser['T_int']>0)]
        organiser = organiser.sort_values(by=['mass'])
        organiser = organiser.set_index('mass')
        return organiser
    
    def create_file_groups(self, grid_section) :
        if grid_section>=0:
            self.mass_group = np.array_split(self.mass_index_values, self.split)[grid_section]
        else :
            self.mass_group = self.mass_index_values
        print('Mass group : ', self.mass_group)
        return self.mass_group
    
    def open_file_groups(self,mass) :
        csv_frames = []
        if self.organiser.groupby('mass').count().loc[mass]['file'] > 1 :
            files, equlibriums, model_types  = self.organiser.loc[mass]['file'], self.organiser.loc[mass]['equilibrium'], self.organiser.loc[mass]['file_type']
        else :
            files, equlibriums, model_types  = [self.organiser.loc[mass]['file']], [self.organiser.loc[mass]['equilibrium']], [self.organiser.loc[mass]['file_type']]
        print('\nGrouping {} new files at mass {}'.format(len(files),mass))
        for file, equlibrium, model_type in tqdm(zip(files,equlibriums,model_types)) :
            with open(file, "rb") as f:
                model = pd.read_pickle(f).iloc[:1]
                model = model.loc[:,~model.columns.duplicated()]
                if 'quality' not in list(model.columns) :
                    model['quality'] = [-1]
                model = self.reduce_columns_load(model)
                model['equilibrium'] = [equlibrium]
                model['model_type'] = [model_type]
                model['file'] = [file]
                model.reset_index(inplace=True, drop=True)
                csv_frames.append(model)
            data = pd.concat(csv_frames,ignore_index=True)
        return data
    
    def load_files(self,grid_section) :
        with parallel_backend("loky", inner_max_num_threads=1):
            datasets = Parallel(n_jobs=-1)(delayed(self.open_file_groups)(mass) for mass in tqdm(self.create_file_groups(grid_section)))
        self.data = pd.concat(datasets,ignore_index=True)
        return self.data
        
    def add_columns(self) :
        self.data = self.data.loc[:,~self.data.columns.duplicated()].copy()
        self.data['yhe'] = self.data['yhe'].astype(float)
        self.data['warning_flag_max_pressure'] = self.data['warning_flag_max_pressure'].astype('bool')
        self.data.rename(columns={'/outputs/spectra/wavenumber': 'wavenumber', '/outputs/spectra/transmission/transit_depth': 'depth',
                                  '/outputs/spectra/emission/spectral_radiosity' : 'radiosity','/outputs/spectra/emission/contributions/thermal': 'thermal' ,
                                  '/outputs/layers/temperature': 'T_atm', '/outputs/layers/pressure': 'P_atm', 
                                  '/outputs/layers/gravity' : 'g_atm', '/outputs/layers/molar_mass' : 'mu_atm',
                                  '/outputs/levels/altitude' : 'altitude',
                                  'P' : 'P_in', 'T' : 'T_in', 'rho' : 'rho_in'}, inplace=True)
        self.data = self.data[self.data['thermal'].notna()]
        self.data = self.join_profiles()
        print('Joined profiles')
        self.data['Mass_Jup'] = [compute_mass(self.data['rho_profile'].iloc[ii][::-1],self.data['R_profile'].iloc[ii][::-1])/M_J for ii in range(0,len(self.data))]
        print('Recalculated mass')
        self.data['wavelength'] = [wavenumber_to_wavelength(self.data['wavenumber'].iloc[ii]) for ii in range(0,len(self.data))]
        print('Converted wavenumber to wavelength')
        self.data['flux'] = [convert_radiosity(self.data['thermal'].iloc[ii],self.data['wavenumber'].iloc[ii]) for ii in range(0,len(self.data))]
        print('Converted radiosity to flux')
        self.data['depth'] = [convert_transit(depth = self.data['new_depth'].iloc[ii], old_radius=self.data['/model_parameters/target/radius_1e5Pa'].iloc[ii], computed_radius=R_J, light_source_radius=self.data['/model_parameters/light_source/radius'].iloc[ii]) for ii in range(0,len(self.data))]
        #self.data['depth'] = [convert_transit(depth = self.data['depth'].iloc[ii], old_radius=self.data['/model_parameters/target/radius_1e5Pa'].iloc[ii], computed_radius=R_J, light_source_radius=self.data['/model_parameters/light_source/radius'].iloc[ii]) for ii in range(0,len(self.data))]
        print('Standardized depth')
        self.data['n_neg_flux'] = [np.sum(self.data['flux'].iloc[ii]<0) for ii in range(0,len(self.data))]
        #self.data['flux_filled'] = [np.reshape(interpolate_negatives_rowwise(np.reshape(self.data['flux'].iloc[ii], (1,1496))),(1496,1)) for ii in range(0,len(self.data))]
        self.data['g_1bar'] = [self.get_gravity(self.data['P_atm'].iloc[ii],self.data['g_atm'].iloc[ii]) for ii in range(0,len(self.data))]
        self.data['T_1bar'] = [self.get_temperature(self.data['P_atm'].iloc[ii],self.data['g_atm'].iloc[ii]) for ii in range(0,len(self.data))]
        self.data['R_1bar'] = (np.sqrt(G_u*self.data['Mass_Jup'].astype('float64')*M_J/self.data['g_1bar'].astype('float64')))
        print('Added 1 bar values')
        self.data['Radius_Jup'] = self.data['R_1bar']/R_J
        self.data['core_Earth'] = self.data['core']*self.data['Mass_Jup']*M_J/M_E
        H, He = load_thermo_tables_CMS()
        self.data['S'] = [get_entropy(H, He, self.data['T_at_P'].iloc[ii], self.data['P_link'].iloc[ii], self.data['yhe'].iloc[ii]) for ii in range(0,len(self.data))]
        print('Added mass, radius, core, entropy')
        self.data['wavelength_photo'] = [list(self.data['wavelength'].iloc[ii]) + get_wl_all_filters(self.filters) for ii in range(0,len(self.data))]
        photometry = pd.concat([evaluate_all_filters(self.filters, self.data['wavelength'].iloc[ii], self.data['flux'].iloc[ii]) for ii in range(0,len(self.data))], ignore_index=True)
        self.photometry_columns = ['ph ' + ele for ele in list(photometry.columns)]
        photometry.columns = self.photometry_columns
        self.data = pd.concat([self.data, photometry], axis=1)
        self.data['flux_photo'] = [list(self.data['flux'].iloc[ii])+list(self.data[self.photometry_columns].values[ii,:]) for ii in range(0,len(self.data))]
        print('Added photometry')
        self.data['ds_dt'], self.data['L_int'] = zip(*[ds_dt(self.data['P_profile'].iloc[ii],self.data['rho_profile'].iloc[ii],
                            self.data['T_profile'].iloc[ii],self.data['R_profile'].iloc[ii],
                            self.data['T_int'].iloc[ii],self.data['R_1bar'].iloc[ii],
                            self.data['P_in'].iloc[ii][self.data['iindex'].iloc[ii]],
                            self.data['P_link'].iloc[ii]) for ii in range(0,len(self.data))])
        self.data['ds_dt'] = np.abs(self.data['ds_dt'])
        print('Added ds_dt and L_int')
        self.data['Tstandard'] = [self.interpolate_to_standard(self.data['T_profile'].iloc[ii], self.data['P_profile'].iloc[ii]) for ii in range(len(self.data))]
        self.data['Pstandard'] = [np.logspace(-4,10,1000) for ii in range(len(self.data))]
        print('Added standard profiles')
        return self.data
        
    def join_profiles(self) :

        # Prepare the lists for new profiles
        new_profiles = {'P_profile': [], 'T_profile': [], 'rho_profile': [], 'R_profile': []}

        for idx, row in self.data.iterrows():
            # Extract necessary values from the dataset
            P_rem = np.array(row['P_atm'])
            T_rem = np.array(row['T_atm'])
            mu_rem = np.array(row['mu_atm'])
            r_rem = np.array(row['altitude'][:-1])

            P_ris, T_ris = row['P_in'], row['T_in']
            rho_ris = np.array(row['rho_in']) * 1e3
            r_ris = np.array(row['R']) * 1e-2
            P_link = row['P_link']

            # Determine indices where pressure is below the link value
            valid_indices = P_rem * 1e-5 < P_link
            P_rem_filtered = P_rem[valid_indices][::-1]
            T_rem_filtered = T_rem[valid_indices][::-1]
            mu_rem_filtered = mu_rem[valid_indices][::-1]
            r_rem_filtered = r_rem[valid_indices][::-1]

            # Compute density and adjusted altitude for the filtered values
            rho_rem_filtered = (P_rem_filtered * mu_rem_filtered) / (R_g * T_rem_filtered)
            r_rem_filtered += -r_rem_filtered.min() + row['Req'] * 1e-2
                
            # Combine with rising profiles
            combined_P = np.concatenate([P_rem_filtered * 1e-5, P_ris[::-1]])
            combined_T = np.concatenate([T_rem_filtered, T_ris[::-1]])
            combined_rho = np.concatenate([rho_rem_filtered, rho_ris[::-1]])
            combined_r = np.concatenate([r_rem_filtered, r_ris[::-1]])

            # Store in the profile lists
            new_profiles['P_profile'].append(combined_P)
            new_profiles['T_profile'].append(combined_T)
            new_profiles['rho_profile'].append(combined_rho)
            new_profiles['R_profile'].append(combined_r)

        # Add the new profiles as columns to the dataset
        for key, values in new_profiles.items():
            self.data[key] = values
            
        return self.data
    
    def reduce_wavelengths(self) :
        wavelengths = self.data['wavelength'].iloc[0]
        new_index = np.where((wavelengths>0.5) & (wavelengths<20))[0]
        self.data['wavelength'] = [np.array(self.data['wavelength'].iloc[ii])[new_index] for ii in range(0,len(self.data))]
        self.data['flux'] = [np.array(self.data['flux'].iloc[ii])[new_index] for ii in range(0,len(self.data))]
        self.data['thermal'] = [np.array(self.data['thermal'].iloc[ii])[new_index] for ii in range(0,len(self.data))]
        self.data['depth'] = [np.array(self.data['depth'].iloc[ii])[new_index] for ii in range(0,len(self.data))]
        self.data['wavelength_photo'] = [list(np.array(self.data['wavelength'].iloc[ii])) + list(self.wavelength_photo) for ii in range(0,len(self.data))]
        self.data['flux_photo'] = [list(np.array(self.data['flux'].iloc[ii])) +  list(np.array(self.data['flux_photo'].iloc[ii])[-len(self.wavelength_photo):]) for ii in range(0,len(self.data))]
        return
    
    @staticmethod
    def reduce_columns_load(model) :
        columns_to_keep = ['/outputs/spectra/wavenumber', '/outputs/spectra/transmission/transit_depth',
                                  '/outputs/spectra/emission/spectral_radiosity','/outputs/spectra/emission/contributions/thermal',
                                  '/outputs/layers/temperature', '/outputs/layers/pressure', 
                                  '/outputs/layers/gravity', '/outputs/layers/molar_mass',
                                  '/outputs/levels/altitude','/model_parameters/target/radius_1e5Pa','/model_parameters/light_source/radius',
                                  'P', 'T','R', 'rho', 'P_link','core',
                                  'T_int', 'T_irr', 'Met', 'f_sed', 'kzz', 'yhe', 'S','mass','T_at_P','Req',
                                  'iindex','warning_flag_max_pressure','eos_env','quality',
                                  'new_depth', 'new_pressure', 'new_temperature']
        return model[columns_to_keep]
   
    def reduce_columns_post(self) :
        self.data = self.data[['P_profile', 'T_profile', 'rho_profile', 'R_profile', 'wavenumber', 'wavelength', 'wavelength_photo', 'depth',
                                  'radiosity', 'thermal' , 'flux', 'flux_photo', 'T_atm', 'P_atm', 'g_atm', 'mu_atm', 'altitude',
                                  'P_in', 'T_in', 'rho_in', 'P_link', 'Radius_Jup', 'Mass_Jup','core', 'core_Earth', 'S', 'ds_dt', 'L_int', 
                                  'Tstandard', 'Pstandard', 'quality', 'file', 'model_type', 'equilibrium', 'eos_env',
                                  'T_int', 'T_irr', 'Met', 'f_sed', 'kzz', 'yhe', 'warning_flag_max_pressure', 'n_neg_flux',
                                  'g_1bar', 'T_1bar', 'R_1bar', 'new_pressure', 'new_temperature',]+self.photometry_columns]
        return self.data
    
    def save_files(self) :
        self.reduce_columns_post()
        self.reduce_wavelengths()
        for mass in self.mass_group :
            print('Saving : ', mass)
            
            if mass < 0.01 :
                round_level = int(3)
            elif mass < 0.1 :
                round_level = int(2)
            else  :
                round_level = int(1)
                
            if len(self.data[(self.data['equilibrium']==0) & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level))]) > 0 :
                for EOS in list(set(self.data[(self.data['equilibrium']==0) & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level))]['eos_env'])) :
                    for model_type in list(set(self.data[(self.data['equilibrium']==0) & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level)) & (self.data['eos_env']==EOS)]['model_type'])) :
                        print('Saving non equilibrium chemistry : ', mass, EOS, model_type)
                        feather.write_feather(self.data[(self.data['equilibrium']==0) & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level)) & (self.data['eos_env']==EOS) & (self.data['model_type']==model_type)] , '../data/current_exoevol_grid/{}/{}_{}.ftr'.format(EOS,model_type,mass))
                                
            if len(self.data[self.data['equilibrium']==1 & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level))]) > 0 :
                for EOS in list(set(self.data[(self.data['equilibrium']==1) & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level))]['eos_env'])) :
                    for model_type in list(set(self.data[(self.data['equilibrium']==1) & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level)) & (self.data['eos_env']==EOS)]['model_type'])) :
                        print('Saving equilibrium chemistry : ', mass, EOS, model_type)
                        feather.write_feather(self.data[self.data['equilibrium']==1 & (self.data['Mass_Jup'].astype('float').round(round_level)==mass.round(round_level)) & (self.data['eos_env']==EOS) & (self.data['model_type']==model_type)] , '../data/current_exoevol_grid_eq/{}/{}_{}.ftr'.format(EOS,model_type,mass))
        
        print('completed')
        return
        
def read_load(file,Radius,T_int,T_irr,T_eff,Met,clouds,process) :
    try :
        with open(file, 'rb') as f:
            df_temp = feather.read_feather(f)
    except :
        print('Failed file {}'.format(file))
        return pd.DataFrame()
    
    df_temp = df_temp[df_temp['warning_flag_max_pressure']==False]
    if len(df_temp) == 0 :
            return pd.DataFrame()

    if not clouds :
        df_temp = df_temp[df_temp['f_sed']>=10]
    else :
        df_temp = df_temp[df_temp['f_sed']<10]

    if type(Radius) != type(None) : 
        Radius = [float(x) for x in Radius]
        df_temp = df_temp[(df_temp['Radius_Jup']>=Radius[0]) & (df_temp['Radius_Jup']<=Radius[1])]
    if type(T_int) != type(None) : 
        T_int = [float(x) for x in T_int]
        df_temp = df_temp[(df_temp['T_int']>=T_int[0]) & (df_temp['T_int']<=T_int[1])]
    if type(T_irr) != type(None) :
        T_irr = [float(x) for x in T_irr]
        df_temp = df_temp[(df_temp['T_irr']>=T_irr[0]) & (df_temp['T_irr']<=T_irr[1])]
    if type(T_eff) != type(None) :
        T_eff = [float(x) for x in T_eff]
        df_temp = df_temp[(df_temp['T_eff']>=T_eff[0]) & (df_temp['T_eff']<=T_eff[1])]
    if type(Met) != type(None) :
        Met = [float(x) for x in Met]
        df_temp = df_temp[(df_temp['Met']>=Met[0]) & (df_temp['Met']<=Met[1])]
    
    print(f"Memory usage: {process.memory_info().rss / (1024 ** 2):.2f} MB")
    if process.memory_info().rss / (1024 ** 2) > 60000 :
        return pd.DataFrame()
    return df_temp

def load_grid(planet_type='dec',EOS='ker',Mass=None,Radius=None,Met=None,Core=None,T_int=None,T_irr=None,T_eff=None,equilibrium=False,clouds=True,jobs=1,exception=False,exeception_file='') :
    process = psutil.Process(os.getpid())
    if exception :
        to_remove = exeception_file
    
    if not equilibrium :
        data_folder = '../data/current_exoevol_grid/{}'.format(EOS)
    else :
        data_folder = '../data/current_exoevol_grid_eq/'.format(EOS)
                
    df = pd.DataFrame()
    df['files'] = glob.glob(data_folder+'/*.ftr')
    df['planet_type'] = [x.split('/')[-1].split('_')[0] for x in df['files']]
    df['mass'] = [float(x.split('/')[-1].split('_')[-1].replace('.ftr','')) for x in df['files']] 
    print(df)
    
    if type(planet_type) != type(None) :
        print(planet_type)
        print(df['planet_type'])
        df = df[df['planet_type'].str.contains(planet_type)]
    if type(Mass) != type(None) :
        print(Mass)
        Mass = [float(x) for x in Mass]
        print(len(df))
        df = df[(df['mass']>=Mass[0]) & (df['mass']<=Mass[1])]
        print(len(df))
        
    files = list(df['files'])
            
    datasets = Parallel(n_jobs=jobs)(delayed(read_load)(file,Radius,T_int,T_irr,T_eff,Met,clouds,process) for file in tqdm(files))
    print(f"Memory usage: {process.memory_info().rss / (1024 ** 2):.2f} MB")
    dataset = pd.concat(datasets,ignore_index=True)
    print('Dataset loaded')
    if exception :
        dataset = dataset.drop(dataset[dataset.file == to_remove].index)
    return dataset

def allkeys(df, obj):
    "Recursively find all keys in an h5py.Group."
    if isinstance(obj,str):
        obj = h5py.File(obj,'r+')
    keys = (obj.name,)
    if isinstance(obj, h5py.Group):
        for key, value in obj.items():
            if isinstance(value, h5py.Group):
                df, new_key = allkeys(df, value)
                keys = keys + new_key
            else:
                keys = keys + (value.name,)
                
                key_name = keys[-1]
                try:
                    df[key_name] = [list(obj[keys[-1]][:])]
                except :
                    df[key_name] = [obj[keys[-1]][()]]

    return df, keys

def replace_none_nan(df,columns):
    for i in range(0,len(df)) :
        for column in columns :
            if type(df[column].iloc[i][0]) == type(None):
                df[column].iloc[i] = np.nan
    return df
                   
def quality_check(df):
    model = pkl.load(open('../../data/ML_model/sort_profiles.pkl', 'rb'))
    df[[f"P_level_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.P_adj.apply(pd.Series)
    df[[f"T_value_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.T_adj.apply(pd.Series)
    df[[f"dT_value_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.dT_adj.apply(pd.Series)
    features = df[[x for x in df.columns if 'T_value' in x]+['T_int','T_irr','Met','mass','f_sed','S']]
    features = features.dropna(axis=1)
    features = np.array(features)
    y_pred = model.predict(features)
    df['predicted_quality'] = y_pred
    return df
                
def merge_lists(*lists):
    # Convert input lists to numpy arrays
    arrays = [np.array(lst) for lst in lists]
    
    # Stack arrays into a 2D array
    stacked = np.stack(arrays)
    
    # Calculate the mean while ignoring NaN values
    mean_values = np.nanmean(stacked, axis=0)
    
    # Check for non-NaN values in the first list, use them if available
    merged_list = np.where(np.isnan(stacked[0]), mean_values, stacked[0])
    
    return merged_list

def split_dataframe(df, n, col='Mass_Jup') :
    # Sort the dataframe by the 'mass' column
    df_sorted = df.sort_values(by='Mass_Jup').reset_index(drop=True)
    
    # Calculate the size of each chunk
    chunk_size = len(df_sorted) // n
    remainder = len(df_sorted) % n  # To account for uneven division
    overlap = int(chunk_size*0.1)  # Overlap between chunks
    
    # Split the dataframe into n chunks
    dfs = []
    start = 0
    for i in range(n):
        extra = 1 if i < remainder else 0  # Distribute the remainder
        overlap = 0 if i == n else overlap
        end = start + chunk_size + extra + overlap
        dfs.append(df_sorted.iloc[start:end])
        start = end - overlap
    
    return dfs