"""
Created on Tue May  2 15:02:54 2023

@author: cwilkinson
"""
import sys
sys.path.append("..")

import numpy as np
import pandas as pd

from python_modules.numerical_tools_module import *

def load_thermo_tables_CMS () :
    # Load hydrogen thermodynamic table and define columns (P,T,rho,S) in (bar,K,g/cc,MJ/kg/K)
    names = ['log T [K]','log P [GPa]','log rho [g/cc]','log U [MJ/kg]','log S [MJ/kg/K]','dlrho/dlT_P','dlrho/dlP_T','dlS/dlT_P','dlS/dlP_T','grad_ad']
    H = pd.read_csv('../../data/EOS/CMS_2019/DirTABLES-EOS2019/TABLE_H_TP_v1',sep='\s+',header=1, index_col=False, names=names)
    H = H.apply(pd.to_numeric, errors='coerce').dropna()
    H['P'] = (1e9/1e5)*10**H['log P [GPa]']
    H['T'] = 10**H['log T [K]']
    H['rho'] = 10**H['log rho [g/cc]']
    H['S'] = 1e6*(10**H['log S [MJ/kg/K]'])
                             
    # Load helium thermodynamic table and define columns (P,T,rho,S) in (bar,K,g/cc,MJ/kg/K)          
    He = pd.read_csv('../../data/EOS/CMS_2019/DirTABLES-EOS2019/TABLE_HE_TP_v1',sep='\s+',header=1, index_col=False, names=names)
    He = He.apply(pd.to_numeric, errors='coerce').dropna()
    He['P'] = (1e9/1e5)*10**He['log P [GPa]']
    He['T'] = 10**He['log T [K]']
    He['rho'] = 10**He['log rho [g/cc]']
    He['S'] = 1e6*(10**He['log S [MJ/kg/K]'])

    return H, He

def thermo_mix_at_y(H, He, y) :
    # We suppose that the hydrogen and helium dataframes are of the shape (T,P) -> (rho,S) and that they hare given on the same (T,P grid)
    
    data = pd.DataFrame()
    data['P'] = list(H['P']) # Set the P axis
    data['T'] = list(H['T']) # Set the T axis
    data['S'] = (1-y)*H['S']+y*He['S'] # Set a new DataFrame from the prexisting Hydrogen DataFrame
    data['rho'] = ((1-y)/H['rho']+y/He['rho'])**(-1)
    return data

def get_entropy(H,He,T,P,y) :
    data_EOS = thermo_mix_at_y(H, He,0.28003)
    S = bilinear_interp(data_EOS[['T','P','S']],T,P) # Perform bilinear interpolation to get the entropy value at (T,P)
    return S

def get_entropy_mixing(y) :
    Na = 6.022*1e23
    Kb = 1.380649*1e-23

    x_H = (4-4*y)/(4-3*y)
    x_He = y/(4-3*y)
    A_mean = 4/(4-3*y)
    Smix = -Kb*(x_H*np.log(x_H) + x_He*np.log(x_He))/(A_mean)
    
    return Smix