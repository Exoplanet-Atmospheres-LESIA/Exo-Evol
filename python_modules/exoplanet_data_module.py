"""
Created on Mon Apr 17 11:48:43 2023

@author: cwilkinson
"""
import sys
sys.path.append("..")

import os
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

import difflib
from statistics import mode

import warnings

warnings.filterwarnings("ignore")

R_J = 69911000
M_J = 1.898*1e27
R_s = 696340000
M_s = 1.989*1e30
R_J = 69911000
M_J = 1.898*1e27
R_E = 6371*1e3
M_E = 5.972*1e24
G_u = 6.674*1e-11
Na = 6.022*1e23
Kb = 1.380649*1e-23
sigma = 5.67*1e-8

name = 'HD209458b'

def get_all_planet_names() :
    data = pd.read_csv('./data/exoplanet.eu_catalog.csv')
    names = list(set(data['# name']))
    return names

def T_irr_at_planet (Radius_star,Temperature_star,distance_star_planet) :
    Radius_star = Radius_star*696340000
    distance_star_planet = distance_star_planet*1.495978707*1e11
    return ((1/4)*Temperature_star**4*(Radius_star**2/distance_star_planet**2))**(1/4)

def get_planet_data(name) :
    data = pd.read_csv('./data/exoplanet.eu_catalog.csv')
    closest_match = difflib.get_close_matches(name, data['# name'], n=1, cutoff=0.6)[0]
    print('Matched name to inputed planet is {}'.format(closest_match))
    
    return data[data['# name']==closest_match]

def get_planet_T_irr(name) :
    data = get_planet_data(name).iloc[0]
    return T_irr_at_planet (data['star_radius'],data['star_teff'],data['semi_major_axis'])

def get_planet_parameters(name) :
    parameters = pd.DataFrame()
    data = get_planet_data(name).iloc[0]
    parameters['mass'] = [data['mass']*M_J]
    parameters['Req'] = data['radius']*R_J
    parameters['T_eff'] = data['temp_measured']
    if np.isnan(data['temp_measured']) :
        parameters['T_eff'] = data['temp_calculated']
    else :
        parameters['T_eff'] = data['temp_measured']
    parameters['Met'] = 10**data['star_metallicity']
    parameters['T_irr'] = T_irr_at_planet (data['star_radius'],data['star_teff'],data['semi_major_axis'])
    return parameters
    
def nasa_data(file = '../data/NASA_archive.csv') :
    exclude = [i for i, line in enumerate(open(file)) if line.startswith('#')]
    df = pd.read_csv(file, skiprows = exclude[-1]+1)
    return df.groupby('pl_name').mean().reset_index()

def nasa_data_full() :
    file = '../data/NASA_archive.csv'
    exclude = [i for i, line in enumerate(open(file)) if line.startswith('#')]
    df = pd.read_csv(file, skiprows = exclude[-1]+1)
    return df
    

def get_spectra(planet,plot=False) :
    files = glob.glob('./data/spectra/*{}.csv'.format(planet))
    df = []
    for i, file in enumerate(files) :
        print(file)
        df_temp = pd.read_csv(file)
        if 'file_length' not in df_temp.columns :
            df_temp['file_length'] = len(df_temp)
        df.append(df_temp)
    df = pd.concat(df,ignore_index=True)
    if 'UL' in df.columns :
        df.loc[df['UL']==1,'Flux[W/m2/um]'] = (df[df['UL']==1]['Flux[W/m2/um]']/2)
        df.loc[df['UL']==1,'(error)'] = (df[df['UL']==1]['Flux[W/m2/um]'])
    if 'CENTRALWAVELNG' in df.columns :
        df = df.sort_values(by='CENTRALWAVELNG')
        if plot :
            plt.errorbar(df['CENTRALWAVELNG'],10000*(df['PL_TRANDEP']),yerr=10000*(df['PL_TRANDEPERR1']),fmt='o')
            plt.xscale('log')
            plt.show()
    elif 'lambda[um]' in df.columns :
        df = df.sort_values(by='lambda[um]')
        if plot :
            plt.errorbar(df['lambda[um]'],(df['Flux[W/m2/um]']),yerr=(df['(error)']),fmt='o')
            plt.xscale('log')
            plt.show()
    
    return df


