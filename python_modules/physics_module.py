"""
Created on Thu May  4 10:30:08 2023

@author: cwilkinson
"""

import sys
sys.path.insert(0, './exoevol/python_modules')

import numpy as np
import pandas as pd
import glob

from scipy.interpolate import interp1d, LinearNDInterpolator
from scipy import integrate
from scipy import optimize

from itertools import product

from tqdm import tqdm

import pickle as pkl

from python_modules.numerical_tools_module import *

import warnings
warnings.filterwarnings("ignore")

R_J = 69911000
M_J = 1.898*1e27
R_s = 696340000
M_s = 1.989*1e30
R_J = 69911000
M_J = 1.898*1e27
R_E = 6371*1e3
M_E = 5.972*1e24
G_u = 6.674*1e-11
Na = 6.022*1e23
Kb = 1.380649*1e-23
sigma = 5.67*1e-8
S_l = 3.828*1e26
h = 6.62607015e-34  # Planck's constant in J·s
c = 2.99792458e8    # Speed of light in m/s

def nbaryons_kg_H_He(y):
    Na = 6.022*1e23
    nbaryons_kg_h = (1/(1e-3))*Na
    nbaryons_kg_he = (1/(4e-3))*4*Na
    n_tot = 1/((1-y)/nbaryons_kg_h + y/nbaryons_kg_he)
    
    return n_tot
    
def T_irr_at_planet (Radius_star,Temperature_star,distance_star_planet) :
    Radius_star = Radius_star*696340000
    distance_star_planet = distance_star_planet*1.495978707*1e11
    
    return ((1/4)*Temperature_star**4*(Radius_star**2/distance_star_planet**2))**(1/4)

def f_x_y(x,y,order = 2) :
    y_smoothed = spleen_smooth(x,y,order)
    
    return interp1d(x,y_smoothed,fill_value='extrapolate')

def convert_radiosity(radiosity,wavenumber):
    #try :
        return np.array(radiosity)*(1e-4)*np.array(wavenumber)**2
    #except :
    #    return np.nan

def convert_transit(depth,old_radius, computed_radius, light_source_radius):
    #try :
        return  np.array(depth)*(computed_radius/old_radius)**2*(light_source_radius)**2
    #except :
    #    return np.nan

def wavenumber_to_wavelength(wavenumber):
    return (1e4)/np.array(wavenumber)

def ds_dt(P,rho,T,R,T_int,R_p,core_pressure,P_link) :
    T = T[(P>=P_link) & (P<=core_pressure)]
    rho = rho[(P>=P_link) & (P<=core_pressure)]
    R = R[(P>=P_link) & (P<=core_pressure)]
    
    to_int = rho*T*R**2
    integral = -integrate.simpson(to_int, R)
    L_int = sigma*R_p**2*T_int**4
    ds_dt = -L_int/integral
    return ds_dt, L_int

def find_max_tint(T_int,f_s,S0,parameters) :
    parameters = list(parameters)
    parameters.insert(4,T_int)
    if np.isnan(f_s(np.array(parameters).reshape(1, -1))):
        return S0
    else :
        return f_s(np.array(parameters).reshape(1, -1))-S0
    
def get_init_s_extremums():
    init_s = pd.concat([pd.read_csv(file) for file in glob.glob('../data/age_data/*')])

    # Define custom mass bins
    mass_bins = np.logspace(np.log10(init_s['M'].min()),np.log10(init_s['M'].max()),10)

    # Bin the mass column
    init_s['M_bin_intervals'] = pd.cut(init_s['M'], bins=mass_bins, right=False)
    init_s['M_bin'] = init_s['M_bin_intervals'].apply(lambda x: x.mid)
    result = init_s.groupby('M_bin')['S'].agg(['min', 'max']).reset_index()
    
    S_cold = interp1d(result['M_bin'],result['min'], fill_value='extrapolate')
    S_hot = interp1d(result['M_bin'],result['max'], fill_value='extrapolate')
    
    return S_cold, S_hot

def get_init_s(mass, S_cold, S_hot, i, n_bins=10):
    return np.linspace(S_cold(mass),S_hot(mass),n_bins)[i]
    

def evaluate_age(df, interpolators, n_entropy_bins = 10, grid_mode=1, verbose=False):
    """
    Evaluate the age of planets based on given parameters.

    Parameters:
    - df: DataFrame containing input parameters like Mass, T_irr, core, Met, f_sed, etc.
    - M: Mass parameter
    - output_path: Path to save the output
    - clouds: Flag indicating whether to consider clouds in the evaluation
    - path_entropy_evol: Path to the entropy evolution file
    - path_entropy: Path to the entropy file
    - path_radius: Path to the radius file

    Returns:
    - DataFrame containing the results of age evaluation for each parameter combination
    """
        
    # Load interpolators for entropy evolution, entropy, and radius
    midpoint_mass = df['Mass_Jup'].min() + (df['Mass_Jup'].max() - df['Mass_Jup'].min()) / 2
    print(midpoint_mass)
    if interpolators is None :
        f_ds_dt, _  = get_closest_interpolator(midpoint_mass, T_irr=100, interpolator_type='entropy_evol',files_to_load='*cloudy*')
        f_s, _      = get_closest_interpolator(midpoint_mass, T_irr=100, interpolator_type='entropy_',files_to_load='*cloudy*')
        f_r, _      = get_closest_interpolator(midpoint_mass, T_irr=100, interpolator_type='radius_',files_to_load='*cloudy*')
    else :
        f_ds_dt, f_s, f_r = interpolators
    
    # Randomly select 25% of the DataFrame
    df = df.sample(frac=0.25, random_state=42) 

    print(df)
    
    # Load initial entropy data and create interpolators
    S_cold, S_hot = get_init_s_extremums()
    
    df = df.drop_duplicates(subset=['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'f_sed', 'kzz'],keep='first')
    parameters = df[['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'f_sed', 'kzz']].values
    
    parameters = np.unique(parameters, axis=0)
    print(parameters)
    
    # Initialize results DataFrame
    results = []
    
    # Iterate over parameter combinations
    for ii in range(n_entropy_bins):
        for parameter in tqdm(parameters):
            Mass, T_irr, Met, core, f_sed, kzz = parameter
            
            # Calculate initial entropy
            S0 = (Kb * get_init_s(Mass, S_cold, S_hot, ii, n_bins=n_entropy_bins)) / (1.6735 * 1e-27)
            if verbose:
                print(parameter)
            
            initial_temperature_found = False
            try : 
                _ , initial_temperature = optimize.brentq(find_max_tint, 100, 1800,args=(f_s,S0,parameter),full_output=True, xtol=10, rtol=10, maxiter=20)
                initial_temperature_found = initial_temperature.converged 
            except :
                pass
            
            if initial_temperature_found :
                initial_temperature = initial_temperature.root
                T_ints = np.sort(initial_temperature - np.logspace(0, np.log10(initial_temperature-1), 100))
                T_ints = np.linspace(0, initial_temperature, int(initial_temperature/10))
                
                parameter_Tint = (Mass, T_irr, Met, core, T_ints, f_sed, kzz)
                parameter_Tint = np.array(list(product([Mass], [T_irr],[Met],[core],T_ints,[f_sed], [kzz]))).reshape(-1,7)

            
                # Evaluate interpolators and filter values
                df_temp = pd.DataFrame({'T_int': T_ints, 'T_irr': T_irr})
                df_temp['ds_dt'] = -1*f_ds_dt(parameter_Tint)
                df_temp['S'] = f_s(parameter_Tint)
                df_temp['Radius_Jup'] = f_r(parameter_Tint)
                df_temp['Met'] = Met
                df_temp['Mass_Jup'] = Mass
                df_temp['core_Earth'] = core
                df_temp['f_sed'] = f_sed
                df_temp['kzz'] = kzz
                
                # Filter out NaN values and apply constraints
                df_temp = df_temp.dropna(subset=['T_int', 'S', 'ds_dt', 'Radius_Jup']).sort_values('T_int',ascending=False)
                df_temp = df_temp[df_temp['S'] <= S0]
                
                # Sort and accumulate filters on S, Radius, and ds/dt
                df_temp = apply_filters(df_temp)
                df_temp = df_temp.sort_values('T_int',ascending=False)
                
                if verbose:
                    print(df_temp)
                
                if len(df_temp) > 4:
                    # Solve for age using rk4 solver
                    f_S_dsdt = interp1d(df_temp['S'], 1 / df_temp['ds_dt'], fill_value="extrapolate")
                    S, t = rk4_solver(f=f_S_dsdt, x_vals=list(df_temp['S']), y0=0)
                    
                    # Convert time to years and filter again
                    df_temp['t'] = t / (24 * 3600 * 365)
                    df_temp = df_temp[(df_temp['t'] < 1e14) & (df_temp['t'] > 0)]
                    # Append result to the list
                    df_temp['S0'] = S0
                    results.append(df_temp)
        
        # Concatenate results
        df_result = pd.concat(results, ignore_index=True)
        
        # Final drop of NaN values based on all parameters
        drop_columns = ['Mass_Jup', 'T_irr', 'Met', 'core_Earth', 'S', 'ds_dt', 'Radius_Jup', 't']
        drop_columns.append('f_sed')
        if grid_mode :
            df_result = df_result.dropna(subset=drop_columns).sample(frac=1, random_state=42) 
        if verbose:
            print(len(df_result))
    return df_result

def apply_filters(df_temp):
    """
    Applies the minimum and maximum accumulation filters on S, Radius, and ds/dt values.
    """
    df_temp = df_temp.sort_values('T_int', ascending=False)
    S_filtered_min = list(np.minimum.accumulate(df_temp['S'].to_list()))
    df_temp = df_temp.sort_values('T_int', ascending=True)
    S_filtered_max = list(np.maximum.accumulate(df_temp['S'].to_list()))
    
    df_temp = df_temp[df_temp['S'].isin(S_filtered_min) & df_temp['S'].isin(S_filtered_max)]
    
    df_temp = df_temp.sort_values('T_int', ascending=False)
    R_filtered_min = list(np.minimum.accumulate(df_temp['Radius_Jup'].to_list()))
    df_temp = df_temp.sort_values('T_int', ascending=True)
    R_filtered_max = list(np.maximum.accumulate(df_temp['Radius_Jup'].to_list()))
    
    df_temp = df_temp[df_temp['Radius_Jup'].isin(R_filtered_min) & df_temp['Radius_Jup'].isin(R_filtered_max)]
    
    df_temp = df_temp.sort_values('T_int', ascending=False)
    ds_dt_filtered_min = list(np.maximum.accumulate(df_temp['ds_dt'].to_list()))
    df_temp = df_temp.sort_values('T_int', ascending=True)
    ds_dt_filtered_max = list(np.minimum.accumulate(df_temp['ds_dt'].to_list()))
    
    return df_temp[df_temp['ds_dt'].isin(ds_dt_filtered_min) & df_temp['ds_dt'].isin(ds_dt_filtered_max)]


def get_condensation_vmr(data, species):
    """
    Get the VMR of a species approximately at its condensation level based on cloud information.
    :param species: species to get the condensation VMR
    :param file: file containing the data
    :return: the VMR at the condensation level
    """

    try:
        return data['/outputs/layers/condensates/volume_mixing_ration_condensation/'+species]
    except KeyError:
        try:
            species_vmr = np.asarray(data['/outputs/layers/volume_mixing_ratios/absorbers/'+species])
        except KeyError:
            species_vmr = np.asarray(data['/outputs/layers/volume_mixing_ratios/gases/'+species])

    try:
        species_cloud_vmr = np.asarray(data['/outputs/layers/clouds/opacity/'+species])
    except KeyError:
        # The cloud opacity was not detected, find the latest level the VMR profile was constant
        wh = np.where(species_vmr[1:] - species_vmr[:-1] == 0)[0]

        if np.size(wh) == 0:
            # The profile is never constant, take the VMR at the top of the atmosphere
            wh = 0
        else:
            wh = wh[-1]

        return species_vmr[wh]

    wh = np.where(species_cloud_vmr > 0)

    if np.size(wh) > 0:
        wh = wh[0][-1]

        return species_vmr[wh]
    else:  # cloud has not condensed, return the VMR at the top of the atmosphere
        return species_vmr[0]

def get_h2o_saturation_pressure(temperature):
    """
    Calculate the H2O pressure of saturation.
    Sources:
        - Fray & Schmitt 2009 (Ice-I sublimation)
        - Wagner and Pruss 1993 (evaporation)
        - Wagner and Pruss 1994 (Ice-I, III, V and VI evaporation)
        - Lin et al. 2004 (Ice-VII sublimation)
    :param temperature: (K) temperature
    :return: (bar) H2O saturation pressure
    """
    temperature_triple_point = 273.16  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
    pressure_triple_point = 6.11657e-3  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

    temperature_ice7_triple_point = 355  # (K) temperature of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)
    pressure_ice7_triple_point = 2.17e4  # (bar) pressure of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)

    temperature_critical_point = 647.096  # (K) temperature of H2O critical point (IAPWS 2011)
    pressure_critical_point = 220.64  # (bar) pressure of H2O critical point (IAPWS 2011)

    if temperature <= temperature_triple_point:  # semi-empirical sublimation pressure
        # Coefficients from Feistel and Wagner 2007
        e = [20.996967, 3.724375, -13.920548, 29.698877, -40.197239, 29.788048, -9.130510]
        eta = 0

        for i, ei in enumerate(e):
            eta += ei * (temperature / temperature_triple_point) ** i

        saturation_pressure = pressure_triple_point * \
            np.exp(3 / 2 * np.log(temperature / temperature_triple_point) +
                   (1 - temperature_triple_point / temperature) * eta)
    elif temperature <= temperature_critical_point:  # semi-empirical condensation pressure
        # Coefficients from Wagner and Pruss 1993 (IAPWS 1992)
        # This formula misses the triple point by less than 0.08 percent
        a = [-7.85951783, 1.84408259, -11.7866497, 22.6807411, -15.9618719, 1.80122502]
        tau = 1 - temperature / temperature_critical_point

        saturation_pressure = pressure_critical_point * np.exp(temperature_critical_point / temperature * (
                a[0] * tau +
                a[1] * tau ** 1.5 +
                a[2] * tau ** 3 +
                a[3] * tau ** 3.5 +
                a[4] * tau ** 4 +
                a[5] * tau ** 7.5
        ))
    else:  # species behave like a super-critical fluid
        # Formulae from Lin et al. 2004
        pressure_c = 0.85e4
        alpha = 3.47
        saturation_pressure = \
            pressure_c * ((temperature / temperature_ice7_triple_point) ** alpha) + pressure_ice7_triple_point

    return saturation_pressure


def get_h2o_melting_pressure(temperature):
    """
    Calculate the melting perssure of H2O.
    :param temperature: (K) temperature
    :return: (bar) array containing the melting pressure of ice-1 and ice-3, 5, 6 and 7 at the given temperature
    """
    temperature_triple_point = 273.16  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
    pressure_triple_point = 6.11657e-3  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

    temperature_ice1_ice3_triple_point = 251.165  # (K) temperature of the liquid-ice1-ice3 triple point (Wagner 1994)
    pressure_ice1_ice3_triple_point = 209.9e1  # (bar) pressure of the liquid-ice1-ice3 triple point (Wagner 1994)

    temperature_ice3_ice5_triple_point = 256.164  # (K) temperature of the liquid-ice1-ice3 triple point (Wagner 1994)
    pressure_ice3_ice5_triple_point = 350.1e1  # (bar) pressure of the liquid-ice1-ice3 triple point (Wagner 1994)

    temperature_ice5_ice6_triple_point = 273.31  # (K) temperature of the liquid-ice1-ice3 triple point (Wagner 1994)
    pressure_ice5_ice6_triple_point = 632.4e1  # (bar) pressure of the liquid-ice1-ice3 triple point (Wagner 1994)

    temperature_ice7_triple_point = 355  # (K) temperature of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)
    pressure_ice7_triple_point = 2.17e4  # (bar) pressure of H2O liquid-ice6-ice7 triple point (Lin et al. 2004)

    if temperature_triple_point >= temperature >= temperature_ice1_ice3_triple_point:
        melting_pressure_ice1 = 1 - 0.626000e6 * (1 - (temperature / temperature_triple_point) ** -3) + \
                                0.197135e6 * (1 - (temperature / temperature_triple_point) ** 21.2)
        melting_pressure_ice1 *= pressure_triple_point
    else:
        melting_pressure_ice1 = np.nan

    if temperature < temperature_ice1_ice3_triple_point:
        melting_pressure = np.nan
    elif temperature <= temperature_ice3_ice5_triple_point:
        melting_pressure = 1 - 0.295252 * (1 - (temperature / temperature_ice1_ice3_triple_point) ** 60)
        melting_pressure *= pressure_ice1_ice3_triple_point
    elif temperature <= temperature_ice5_ice6_triple_point:
        melting_pressure = 1 - 1.18721 * (1 - (temperature / temperature_ice3_ice5_triple_point) ** 8)
        melting_pressure *= pressure_ice3_ice5_triple_point
    elif temperature <= temperature_ice7_triple_point:
        # The power coefficient in the paper is 4.6, but this overshoot the l-ice6-ice7 3 point
        melting_pressure = 1 - 1.07476 * (1 - (temperature / temperature_ice5_ice6_triple_point) ** 4.5)
        melting_pressure *= pressure_ice5_ice6_triple_point
    else:  # liquid up to the temperature triple point, then super-critical fluid
        # Formulae from Lin et al. 2004
        pressure_c = 0.85e4
        alpha = 3.47
        melting_pressure = \
            pressure_c * ((temperature / temperature_ice7_triple_point) ** alpha - 1) + pressure_ice7_triple_point
    return melting_pressure_ice1, melting_pressure


def get_nh3_saturation_pressure(temperature):
    """
    Get the NH3 saturation pressure.
    :param temperature: (K) temp
    :return: (bar) saturation pressure of NH3 at the given temperature
    """
    temperature_triple_point = 195.41  # (K) temperature of H2O triple point (Fray & Schmitt 2009)
    pressure_triple_point = 6.09e-2  # (bar) pressure of H2O triple point (Fray & Schmitt 2009)

    temperature_critical_point = 405.5  # (K) temperature of H2O critical point (Lide 2006)
    pressure_critical_point = 113.5  # (bar) pressure of H2O critical point (Lide 2006)

    a = np.array([1.596E1, -3.537E3, -3.310E4, 1.742E6, -2.995E7])
    saturation_pressures_ref = np.array([
        pressure_triple_point * 1e2,
        8.7,
        12.6,
        17.9,
        24.9,
        34.1,
        45.9,
        60.8,
        79.6,
        103,
        131,
        165,
        207,
        256,
        313,
        381,
        460,
        552,
        655,
        774,
        909,
        1062,
        pressure_critical_point * 1e2
    ]) * 1e-2
    temperatures_ref = np.append([temperature_triple_point], np.linspace(200, 300, 21))
    temperatures_ref = np.append(temperatures_ref, [temperature_critical_point])
    nh3_saturation_pressure = 0

    if temperature <= temperature_triple_point:
        for i, ai in enumerate(a):
            nh3_saturation_pressure = nh3_saturation_pressure + ai / temperature ** i

        return np.exp(nh3_saturation_pressure)
    elif temperature <= temperature_critical_point:
        return np.exp(np.interp(np.log(temperature), np.log(temperatures_ref), np.log(saturation_pressures_ref)))
    else:
        return 1e10  # arbitrary value

def get_h2o_saturation_temperature(data):
    """
    Get the H2O saturation temperatures at given pressures.
    :param data: file containing the data (Pandas series)
    :return: (K) array of H2O condensation temperatures
    """
    condensation_pressure = []
    condensation_temperature_h2o = []
    temperature_grid = np.linspace(1, 3000, 3000)

    for i, t in enumerate(temperature_grid):
        condensation_pressure.append(get_h2o_saturation_pressure(t))

    condensation_pressure = np.asarray(condensation_pressure)
    condensation_vmr = get_condensation_vmr(data, 'H2O')

    pressures = np.array(data['/outputs/layers/pressure'][::-1]) * 1e-5

    for i, p in enumerate(pressures):
        condensation_temperature_h2o.append(
            np.interp(p * condensation_vmr, condensation_pressure, temperature_grid)
        )

    return np.asarray(condensation_temperature_h2o)
 
def get_nh3_saturation_temperature(data):
    """
    Get the NH3 saturation temperatures at given pressures.
    :param data: file containing the data (Pandas series)
    :return: (K) array of NH3 condensation temperatures
    """
    condensation_pressure = []
    condensation_temperature_nh3 = []
    temperature_grid = np.linspace(1, 3000, 3000)

    for i, t in enumerate(temperature_grid):
        condensation_pressure.append(get_nh3_saturation_pressure(t))

    condensation_pressure = np.asarray(condensation_pressure)
    condensation_vmr = get_condensation_vmr(data, 'NH3')

    pressures = np.array(data['/outputs/layers/pressure'][::-1]) * 1e-5

    for i, p in enumerate(pressures):
        condensation_temperature_nh3.append(np.interp(p * condensation_vmr, condensation_pressure, temperature_grid))

    return np.asarray(condensation_temperature_nh3)

def get_condensation_temperatures(data, species):
    """
    Get the condensation temperatures of a given species.
    :param data: file containing the data (Pandas series)
    :param species: species name (e.g. 'H2O')
    :return: the condensation temperature of the given species
    """
    if species == 'H2O':
        condensation_temperature = get_h2o_saturation_temperature(data)
    elif species == 'NH3':
        condensation_temperature = get_nh3_saturation_temperature(data)
    else:
        pressure = data['/outputs/layers/pressure'][::-1] * 1e-5
        meta = {}

        for ele in data.index :
            if '/model_parameters/species/elemental_abundances' in ele :
                key = ele.split('/')[-1]
                meta[key] = data['/model_parameters/species/elemental_abundances/'+key] / \
                            data['/model_parameters/species/solar_elemental_abundances/'+key]

        condensation_temperature = {
            'NH4SH': 10 ** 4 / (48.91 - 4.15 * np.log10(pressure) - 4.15 * np.log10(np.minimum(meta['N'], meta['S']))),
            'NH4Cl': 10 ** 4 / (
                        27.46 - 2.23 * (np.log10(pressure) + 0.33 * np.log10(np.minimum(meta['N'], meta['Cl'])))),
            # NH4Cl source: https://pubchem.ncbi.nlm.nih.gov/source/hsdb/483#section=Taste
            'ZnS': 10 ** 4 / (12.527 - 0.63 * np.log10(pressure) - 1.26 * np.log10(np.minimum(meta['Zn'], meta['S']))),
            'KCl': 10 ** 4 / (
                        12.479 - 0.879 * np.log10(pressure) - 0.879 * np.log10(np.minimum(meta['K'], meta['Cl']))),
            'Na2S': 10 ** 4 / (10.045 - 0.72 * np.log10(pressure) - 0.5 * np.log10(np.minimum(meta['Na'], meta['S']))),
            'MnS': 10 ** 4 / (7.447 - 0.42 * np.log10(pressure) - 0.84 * np.log10(np.minimum(meta['Mn'], meta['S']))),
            'Cr': 10 ** 4 / (6.576 - 0.486 * np.log10(pressure) - 0.486 * np.log10(meta['Cr'])),
            'MgSiO3': 10 ** 4 / (6.26 - 0.35 * np.log10(pressure) - 0.7 * np.log10(np.minimum(meta['Mg'], meta['Si']))),
            'Mg2SiO4': 10 ** 4 / (
                        5.89 - 0.37 * np.log10(pressure) - 0.73 * np.log10(np.minimum(meta['Mg'], meta['Si']))),
            'Fe': 10 ** 4 / (5.44 - 0.48 * np.log10(pressure) - 0.48 * np.log10(meta['Fe'])),
            'Al2O3': 10 ** 4 / (5.014 - 0.2179 * np.log10(pressure) - 0.58 * np.log10(meta['Al'])),
        }

        condensation_temperature = condensation_temperature[species]

    return condensation_temperature

def Met_converter(Met) :
    return np.log10(Met)

def diluted_planet(flux, Rp, dstar):
    # “Rp en rayon de jupiter, dstar en pc”
    Rp = Rp * 69911.#conversion en km
    d = dstar * 3.26 * 9.46e12 #conversion en km : dstar en pc *3.26 al
    flux_dil = flux*(Rp**2/d**2)
    return flux_dil

def compute_mass(rho,r) :
    return 4*np.pi*np.trapz(rho*r**2, x=r)

def get_Teff(spectral_radiosity,wavenumber) :
    sigma = 5.67*1e-8
    T_eff = (np.sum(np.mean(np.diff(wavenumber))*np.array(spectral_radiosity))/sigma)**(1/4)
    return T_eff

def blackbody_spectrum_wm2um(wavelengths_um, temperatures_K):
    """
    Calculate the blackbody spectrum for given temperatures and wavelengths in W/m²/µm.
    
    Parameters:
    - wavelengths_um: Array of wavelengths in micrometers (µm)
    - temperatures_K: Array of temperatures in Kelvin
    
    Returns:
    - Spectral radiance in W/m²/µm for each temperature and wavelength
    """
    # Convert wavelengths from micrometers to meters
    wavelengths_m = wavelengths_um * 1e-6
    
    # Ensure wavelengths_m is 2D for broadcasting
    if wavelengths_m.ndim == 1:
        wavelengths_m = wavelengths_m[:, np.newaxis]
    
    # Ensure temperatures_K is 2D for broadcasting
    if temperatures_K.ndim == 1:
        temperatures_K = temperatures_K[np.newaxis, :]
    
    # Apply Planck's law (broadcasted over wavelength and temperature)
    term1 = (2 * h * c**2) / (wavelengths_m**5)
    term2 = np.pi / (np.exp((h * c) / (wavelengths_m * Kb * temperatures_K)) - 1)
    
    # Spectral radiance in W/sr/m^3, convert to W/m²/µm by multiplying by 1e-6
    spectral_radiance = (term1 * term2) * 1e-6
    
    return spectral_radiance

def flux_to_Jy(wl,flux) :
    return 333564095198152064*np.array(flux)*np.array(wl)**2

def Jy_to_flux(jy,wl) :
    '''Convert Jy to W/m^2/um
    wl in um'''
    return jy*(2.99792458E+14/1.0E+26)*(1/wl**2)

def erg_s_cm2_A_to_W_m2_um(erg_s_cm2_A):
    """
    Convert erg/s/cm²/Å to W/m²/µm.
    
    Parameters:
    - erg_s_cm2_A: Array of fluxes in erg/s/cm²/Å
    
    Returns:
    - Array of fluxes in W/m²/µm
    """
    return 1.e-7*np.array(erg_s_cm2_A)/(1e-4)/(1e-4)

def pc_to_m(pc) :
    return pc*3.086e16
