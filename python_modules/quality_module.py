import sys
sys.path.append("..")

from python_modules.grid_data_processing_module import load_grid
from python_modules.numerical_tools_module import interpolate_negatives_rowwise, gradient


import time
import numpy as np
import pandas as pd
import joblib
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import ConfusionMatrixDisplay
import pickle as pkl
import joblib
import time

class QualityControl :
    def __init__(self,split_fraction=0.1,n_estimators=1000) :
        self.split_fraction = split_fraction
        self.n_estimators = n_estimators
        self.output_path = '../data/quality/'
        
    def prepare_data(self) :
        self.grid = load_grid().sample(frac=1)
        self.wavelength = self.grid['wavelength'].iloc[0]
        self.grid = self.clean_data(self.grid)
        self.features = self.make_features(self.grid[self.grid['quality']>-1])
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.features[self.features.columns[:-1]], self.features[['y']], test_size=self.split_fraction, random_state=42)
        return self.grid
    
    @staticmethod
    def clean_data(grid) :
        grid = grid.dropna(subset=['Mass_Jup', 'Radius_Jup', 'T_int', 'T_irr', 'Met', 'core_Earth', 'f_sed','kzz'])
        grid = grid[~grid.isin([np.nan, np.inf, -np.inf]).any(axis=1)]
        return grid
    
    @staticmethod
    def get_model(file) :
        with open(file, "rb") as f:
            model = pkl.load(f).iloc[:1]
        return model
    
    @staticmethod
    def save_model(model,quality,file) :        
        model['quality'] = quality
        model.to_pickle(file)
        return
    
    def tagging_data(self) :
        grid_unchecked = self.grid[self.grid['quality']<0]
        grid_unchecked = grid_unchecked[grid_unchecked['Mass_Jup']<0.7]
        #grid_unchecked = self.grid[(self.grid['T_irr']>250) & (self.grid['T_int']>150)]
        for ii in range(len(grid_unchecked)) :
            self.file = grid_unchecked['file'].iloc[ii]
            T = grid_unchecked['T_profile'].iloc[ii]
            P = grid_unchecked['P_profile'].iloc[ii]
            plt.plot(T, P)
            plt.gca().invert_yaxis()
            plt.axhline(y=grid_unchecked['P_link'].iloc[ii], color='r', linestyle='--')
            plt.yscale('log')
            plt.xscale('log')
            plt.xlabel(r'Temperature $(K)$')
            plt.ylabel(r'Pressure $(bar)$')
            plt.title(self.file)
            plt.show()
            self.model = self.get_model(self.file)
            quality = int(input("Quality of the data: "))
            self.save_model(self.model,quality,self.file)
        return

    @staticmethod
    def make_features(grid) :
        features = grid.Tstandard.apply(pd.Series)
        features = pd.concat([features,grid.Tstandard.apply(gradient).apply(pd.Series)],axis=1)
        features.columns = ['T_'+str(ii) for ii in range(int(len(features.columns)/2))] + ['Tdiff_'+str(ii) for ii in range(int(len(features.columns)/2))]
        #self.features = pd.concat([self.features, grid_checked[['Mass_Jup', 'Radius_Jup', 'T_int', 'T_irr', 'Met', 'core_Earth', 'f_sed','kzz']]],axis=1)
        features['y'] = grid['quality']
        return features
    
    def train_model(self) :
        self.model = RandomForestClassifier(n_estimators=self.n_estimators, random_state=42)
        self.model.fit(self.X_train, self.y_train)
        joblib.dump(self.model, '{}/{}.pkl'.format(self.output_path,'quality'))
        return self.model
    
    def evaluate_model(self) :
        self.y_pred = self.model.predict(self.X_test)
        self.mse = accuracy_score(self.y_test, self.y_pred)
        _ = ConfusionMatrixDisplay.from_estimator(self.model, self.X_test, self.y_test)
        print("Accuracy : ", self.mse)
        return self.mse
    
    def feature_importance(self,model) :
        importances = model.feature_importances_
        std = np.std([tree.feature_importances_ for tree in model.estimators_], axis=0)
        forest_importances = pd.DataFrame()
        forest_importances['importance'] = pd.Series(importances, index=list(self.X_train.columns))
        forest_importances['std'] = pd.Series(std, index=list(self.X_train.columns))
        forest_importances = forest_importances.sort_values(by='importance',ascending=False)
        forest_importances = forest_importances.head(30)
        fig, ax = plt.subplots()
        forest_importances['importance'].plot.bar(yerr=forest_importances['std'], ax=ax)
        ax.set_title("Feature importances using MDI")
        ax.set_ylabel("Mean decrease in impurity")
        fig.tight_layout()
        return
    
    def load_model(self) :
        self.model = joblib.load('{}/{}.pkl'.format(self.output_path,'quality'))
        return self.model
        
    def use_model(self, grid):
        self.model = self.load_model()
        grid = self.clean_data(grid)
        features = self.make_features(grid)
        grid['quality_pred'] = self.model.predict(features[features.columns[:-1]])
        return grid
    