"""
Created 2023

@author: Christian Wilkinson
"""

import glob
import numpy as np
import pandas as pd
import random
import pickle as pkl
import warnings
import math
import matplotlib.pyplot as plt
from molmass import Formula
from python_modules.numerical_tools_module import custom_round, monotonic
from python_modules.interface_module import input_parameters, input_convergence, input_paths
from python_modules.exorem_module import run_exorem
from python_modules.exoris_module import run_exoris
from python_modules.regression_module import MLmodels
from python_modules.quality_module import QualityControl
import psutil
import os
import time
import gc

warnings.filterwarnings("ignore")

'''
The order of the configurations (config) is as follows
0 : mass
1 : T_int
2 : T_irr
3 : Met
4 : core
5 : f_sed
6 : kzz
7 : rock
8 : eos_env
9 : eos_ice
10 : eos_core
'''

R_J = 69911000  # Radius of Jupiter in meters
M_J = 1.898e27  # Mass of Jupiter in kilograms
R_s = 696340000  # Radius of the Sun in meters
M_s = 1.989e30  # Mass of the Sun in kilograms
M_E = 5.972e24  # Mass of Earth in kilograms
G_u = 6.674e-11  # Gravitational constant
Na = 6.022e23  # Avogadro's number
Kb = 1.380649e-23  # Boltzmann constant


class LinkPlanet:
    def __init__(self, file_prefix, idx):
        """Generate planetary parameters grid."""
        self.idx = int(idx)
        self.max_iteration = input_convergence('max_iterations')
        self.min_iteration = input_convergence('min_iterations')
        self.min_P_link = input_convergence('min_P_link')
        self.file_prefix = file_prefix
        self.current_files = '../data/planets/dec2024'
        #self.current_files = '../data/planets'
        self.output_file = input_paths('output_file')
        self.exoris_path = input_paths('exoris')
        self.exorem_path = input_paths('exorem')
        self.exoris_data = input_paths('exoris_data')
        self.exorem_data = input_paths('exorem_data')
        self.get_samples()
        self.rock = input_parameters('rock')
        self.eos_ice = input_parameters('eos_ice')
        self.eos_core = input_parameters('eos_core')
        self.eos_env = input_parameters('eos_env')
        self.ML = MLmodels(n_estimators=1000)
        self.QC = QualityControl()

    def get_samples(self):
        self.T_int = random.sample(
            list(input_parameters('intrinsic_temperature')), 1)[0]
        self.T_irr = random.sample(
            list(input_parameters('irradiation_temperature')), 1)[0]
        self.T_int_resolution = np.diff(
            list(input_parameters('intrinsic_temperature')))[0]
        self.T_irr_resolution = np.diff(
            list(input_parameters('irradiation_temperature')))[0]
        Masses = list(input_parameters('mass'))
        self.M_resolution = np.diff(Masses)[0]
        if len(Masses) < 25:
            Masses += random.sample(Masses, 25 - len(Masses))

        self.M = random.sample(Masses[(len(
            Masses) // 25) * (int(self.idx)):(len(Masses) // 25) * (int(self.idx) + 1)], 1)[0]
        self.Met = random.sample(list(input_parameters('metallicity')), 1)[0]
        self.core = random.sample(list(input_parameters('core')), 1)[0]
        self.f_sed = random.sample(list(input_parameters('f_sed')), 1)[0]
        self.kzz = random.sample(list(input_parameters('kzz')), 1)[0]
        self.resolutions = [self.M_resolution, self.T_int_resolution, self.T_irr_resolution]
        
        print('\n--')
        print('Chosen sample : {}'.format(self.idx))
        print('Mass : {}Jupiter'.format(self.M))
        print('T_int : {}K'.format(self.T_int))
        print('T_irr : {}K'.format(self.T_irr))
        print('core : {}'.format(self.core))
        print('Met : {}'.format(self.Met))
        print('f_sed : {}'.format(self.f_sed))
        print('kzz : {}'.format(self.kzz))
        print('--')

    def get_current_results(self):
        """Retrieve current results from output files."""
        def Mass(string): return float(string.split('/')[-1].split('_')[0])
        def Tint(string): return float(string.split('/')[-1].split('_')[1])
        def Tirr(string): return float(string.split('/')[-1].split('_')[2])
        def Met(string): return float(string.split('/')[-1].split('_')[3])
        def core(string): return float(string.split('/')
                                       [-1].split('_')[4].replace('.pkl', ''))
        def sed(string): return float(string.split('/')[-1].
                                      split('_')[5].replace('.pkl', ''))
        def kzz(string): return float(string.split('/')
                                       [-1].split('_')[6].replace('.pkl', ''))

        def env_eos(string): return 'ker' if 'ker' in string else 'scvh'

        files = glob.glob(f"{self.current_files}/*.pkl") + glob.glob(
            f"{self.current_files}/*/*.pkl") + glob.glob(f"{self.current_files}/*/*/*.pkl")
        self.current_results = pd.DataFrame({'files': files})
        self.current_results['mass'] = self.current_results['files'].apply(
            Mass)
        self.current_results['T_int'] = self.current_results['files'].apply(
            Tint)
        self.current_results['T_irr'] = self.current_results['files'].apply(
            Tirr)
        self.current_results['Met'] = self.current_results['files'].apply(Met)
        self.current_results['core'] = (self.current_results['files'].apply(
            core) * self.current_results['mass'] * M_J / M_E).round(0)
        self.current_results['f_sed'] = self.current_results['files'].apply(
            sed)
        if (self.f_sed < 10) or (self.f_sed > 0) :
            self.current_results = self.current_results[(self.current_results['f_sed']<10) & (self.current_results['f_sed']>0)]
        self.current_results['kzz'] = self.current_results['files'].apply(
            kzz) # To change when kzz is better implemented
        self.current_results['eos_env'] = self.current_results['files'].apply(
            env_eos)
        return
    
    def get_closest_model(self):
        """
        Find the closest model in the DataFrame that matches the given parameters with
        priority given to mass, T_int, and T_irr through weighted distance calculations.

        Parameters:
        - output_file: File path to load the DataFrame containing model information.
        - M, T_int, T_irr, Met, core, f_sed: Target grid point coordinates.
        - eos_env: Specific EOS environment to filter the models.

        Returns:
        - closest_model: The closest valid model loaded from the file.
        - If no valid model is found, an empty DataFrame is returned.
        """

        # Define weights for the different parameters
        weight_mass = 1000.0
        weight_T_int = 5000.0
        weight_T_irr = 5000.0
        weight_Met = 100.0
        weight_core = 1.0
        weight_f_sed = 1.0
        weight_kzz = 1.0

        # Load the current results into a DataFrame and filter by EOS
        # environment
        df = self.current_results
        df = df[df['eos_env'] == self.eos_env]

        # Vectorized weighted distance calculation for all rows at once
        df['distance'] = (weight_mass * (np.log10(df['mass'] + 1e-6) - np.log10(self.M + 1e-6))**2) + \
            (weight_T_int * (np.log10(df['T_int'] + 1e-6) - np.log10(self.T_int + 1e-6))**2) + \
            (weight_T_irr * (np.log10(df['T_irr'] + 1e-6) - np.log10(self.T_irr + 1e-6))**2) + \
            (weight_Met * (np.log10(df['Met'] + 1e-6) - np.log10(self.Met + 1e-6))**2) + \
            (weight_core * (np.log10(df['core'] + 1e-6) - np.log10(self.core + 1e-6))**2) + \
            (weight_f_sed * (np.log10(df['f_sed'] + 1e-6) - np.log10(self.f_sed + 1e-6))**2) + \
            (weight_kzz * (np.log10(df['kzz'] + 1e-6) - np.log10(self.kzz + 1e-6))**2)
        
        if (self.f_sed<10) & (self.f_sed>0):
            df = df[(df['f_sed']<10) & (df['f_sed']>0)]

        df['distance_T'] = (df['T_int'] - self.T_int)
        if not df[df['distance_T']>self.T_int].empty :
            df = df[df['distance_T']>self.T_int]
        
        # Sort DataFrame by weighted distance
        df = df.sort_values(by='distance').reset_index(drop=True)
        print(df[['distance','files']].head())

        # Iterate over sorted rows until a valid model is found
        for idx, row in df.iterrows():
            try:
                # Load the model from the corresponding file
                with open(row['files'], "rb") as f:
                    closest_model = pkl.load(f)

                # Check if the model has valid conditions
                if not closest_model['warning_flag_max_pressure'].iloc[0]:
                    # Check the quality if the column exists
                    if 'quality' in closest_model.columns :
                        if closest_model['quality'].iloc[0] == 1:
                            print(f"{row['files']} is the closest valid model")
                            self.closest_model = closest_model
                            return self.closest_model
                
                '''
                print(f"{row['files']} is the closest valid model")
                self.closest_model = closest_model
                return self.closest_model
                '''

            except FileNotFoundError:
                # Handle missing files gracefully
                print(f"File {row['files']} not found. Skipping...")
            except Exception as e:
                # Catch and report any other issues during model loading
                print(f"Error loading {row['files']}: {e}. Skipping...")

        # If no valid model was found, return an empty DataFrame
        print("No valid model found.")
        return pd.DataFrame()

    def fold_out(self):
        parameter_space = [
            self.M,
            self.T_int,
            self.T_irr,
            self.Met,
            self.core,
            self.f_sed,
            self.kzz]
        print(parameter_space)
        converged_model = self.get_closest_model()
        converged_model['Mass_Jup'] = converged_model['mass'] * 1e-3 / M_J
        converged_model = converged_model[[
            'Mass_Jup', 'T_int', 'T_irr', 'Met', 'core', 'f_sed', 'kzz']].values[0]
        #converged_model = converged_model[[
        #    'mass', 'T_int', 'T_irr', 'Met', 'core', 'f_sed']].values[0] # To change when kzz is better implemented
        idx = random.randint(1, len(converged_model) - 1)
        idx = random.randint(1, 2) # Force T_irr to be the parameter to fold out !!! to be changed !!!
        print(parameter_space)
        if idx > 2 :
            converged_model[idx] = parameter_space[idx]
        else :
            valid_direction = False
            while not valid_direction:
                direction = random.choice([-1,1])
                #direction = 1 # Force direction to be positive !!! to be changed !!!
                if converged_model[idx]+direction*self.resolutions[idx] > 0 :
                    converged_model[idx] = converged_model[idx]+direction*self.resolutions[idx]
                    valid_direction = True
      
        self.T_int, self.T_irr, self.Met, self.core, self.f_sed, self.kzz = tuple(
            converged_model[1:])
        #self.M, self.T_int, self.T_irr, self.Met, self.core , self.f_sed = tuple(
        #    converged_model) # To change when kzz is better implemented, add back core once corrected
        self.select_unique_parameters()
        if self.core < 0.1:
            self.core = self.core*converged_model[4]*M_J/M_E
        if self.core < 0.1:
            self.core = random.choice([1,10,20,30,40,50,60])
            while self.core*M_E>self.M*M_J:
                self.core = random.choice([1,10,20,30,40,50,60])
        print(self.M,self.T_int, self.T_irr, self.Met, self.core, self.f_sed, self.kzz)
        print(self.core * M_E / (self.M * M_J))
        return

    def select_unique_parameters(self):
        """Select unique parameters that are not present in the current results."""
        config = [
            self.M,
            self.T_int,
            self.T_irr,
            self.Met,
            self.core,
            self.f_sed,
            self.kzz,
            self.rock,
            self.eos_env,
            self.eos_ice,
            self.eos_core]
        unique = False
        while not unique:
            config[1:3] = list(map(custom_round, config[1:3]))
            if not self.current_results.empty:
                self.max_temperatures = self.current_results.groupby(
                    'mass')['T_int'].max().reset_index()
                max_temperature_converged = self.max_temperatures[np.abs(
                    self.max_temperatures['mass'] - config[0]) == np.min(np.abs(self.max_temperatures['mass'] - config[0]))]['T_int'].iloc[0]
                if config[1] >= max_temperature_converged + \
                        self.T_int_resolution:
                    config[1] = max_temperature_converged + \
                        self.T_int_resolution
            else:
                return
            df_ = self.current_results[
                (self.current_results['eos_env'] == config[7]) &
                (self.current_results['mass'] == np.round(config[0], 4)) &
                (self.current_results['Met'] == config[3]) &
                (self.current_results['core'] == config[4]) &
                (self.current_results['f_sed'] == config[5]) &
                (self.current_results['kzz'] == config[6]) & # To change when kzz is better implemented
                (self.current_results['T_irr'] == config[2]) &
                (self.current_results['T_int'] == config[1])
            ]

            if df_.empty:
                return
            else:
                #first_choice = random.choice([0, 1, 2, 3])
                first_choice = random.choice([2, 3])

                if (first_choice == 0) & (
                        (config[2] - self.T_irr_resolution-1) > 0):
                    config[2] -= self.T_irr_resolution
                elif first_choice == 1:
                    config[2] += self.T_irr_resolution
                elif first_choice == 2 & ((config[1] - self.T_int_resolution) >= 0):
                    config[1] -= self.T_int_resolution
                else:
                    config[1] += self.T_int_resolution
                    
                if config[1] < 0:
                    config[1] = 0
                if config[2] < 0:
                    config[2] = 0

                config[1:3] = list(map(custom_round, config[1:3]))
                df_ = df_[
                    (df_['T_irr'] == config[2]) & (
                        df_['T_int'] == config[1])]
                if df_.empty:
                    unique = True
        self.M, self.T_int, self.T_irr, self.Met, self.core, self.f_sed, self.kzz, self.rock, self.eos_env, self.eos_ice, self.eos_core = tuple(
            config)
        return

    def make_grid(self):
        if self.core * M_E / (self.M * M_J) > 0.95:
            print('Core too large')
            return

        self.model = pd.DataFrame()
        P_links = []
        self.g_errors = []
        new_model_exists = False
        
        print('\nMass : {}Jupiter'.format(self.M))
        print('T_int : {}K'.format(self.T_int))
        print('T_irr : {}K'.format(self.T_irr))
        print('core : {}'.format(self.core))
        print('Met : {}'.format(self.Met))
        print('f_sed : {}'.format(self.f_sed))
        print('kzz : {}'.format(self.kzz))
        print('EOS_env : {}'.format(self.eos_env))

        iteration = 0
        n_iterations_atmosphere = 50
        while (iteration < self.max_iteration) :

            if not new_model_exists:
                print('\nInitiating from closest model\n')
                self.model = self.closest_model

            if len(self.model) > 0:
                P_links.append(self.model['P_link'].iloc[-1])
                self.g_errors.append(self.model['g_erreur'].iloc[-1])

            T_int_convergence = 100
            #T_P = self.make_TP()
            T_P = (self.model['/outputs/layers/temperature'].iloc[0],self.model['/outputs/layers/pressure'].iloc[0])
            print('\nRunning exorem\n')
            self.memory_usage()
            iteration += 1
            atmosphere_new = run_exorem(
                self.file_prefix,
                self.model,
                self.Met,
                self.f_sed,
                self.M,
                self.T_int,
                self.T_irr,
                self.kzz,
                iteration,
                self.g_errors,
                self.exorem_data,
                self.min_P_link,
                n_iterations_atmosphere,
                T_P = T_P)  
            self.memory_usage()
            gc.collect()  
            self.memory_usage()
            if 'T_at_P' in atmosphere_new.columns :
                T_int_convergence = 100*np.abs(
                    self.T_int - atmosphere_new['/outputs/run_quality/actual_internal_temperature'].iloc[0]) / self.T_int
                if not self.quality_check(atmosphere_new) :
                    atmosphere_new = pd.DataFrame()
                T_P = (np.array(atmosphere_new['/outputs/layers/temperature'].iloc[0]),np.array(atmosphere_new['/outputs/layers/pressure'].iloc[0]))
                n_iterations_atmosphere += 10
            else:
                with open("./../temp/failed/failed_planets.txt", "a") as f:
                    f.write(self.file)
                break
            if n_iterations_atmosphere > 200:
                atmosphere_new = pd.DataFrame()
                with open("./../temp/failed/failed_planets.txt", "a") as f:
                    f.write(self.file)
                break
            
            if 'T_at_P' in atmosphere_new.columns:
                print('\nT_rem {} K'.format(atmosphere_new['T_at_P'].iloc[-1]))
                print(
                    'g_rem {} m.s-2'.format(atmosphere_new['g_at_P'].iloc[-1]))
                print('P_link {} bar\n'.format(
                    atmosphere_new['P_link'].iloc[-1]))
                P_links.append(atmosphere_new['P_link'].iloc[-1])
                print('\nRunning exoris\n')
                interior_new = run_exoris(
                    self.output_file,
                    self.file_prefix,
                    atmosphere_new,
                    self.model,
                    self.M,
                    self.core,
                    self.eos_env,
                    self.rock,
                    self.eos_ice,
                    self.eos_core,
                    self.exoris_data)
                self.memory_usage()
                gc.collect()
                self.memory_usage()
            else:
                new_model_exists = False
                interior_new = pd.DataFrame()
                iteration += 30
                with open("./../temp/failed/failed_planets.txt", "a") as f:
                    f.write(self.file)
                break

            if 'Req' in interior_new.columns:
                if isinstance(interior_new['Req'].iloc[-1], type(None)):
                    print('\nReq is of type {}\n'.format(
                        type(interior_new['Req'].iloc[-1])))
                    new_model_exists = False
                    iteration += 30
                    with open("./../temp/failed/failed_planets.txt", "a") as f:
                        f.write(self.file)
                    break
                else:
                    new_model_exists = True
            else:
                new_model_exists = False
                iteration += 30
            self.memory_usage()
            self.model = pd.concat([atmosphere_new.reset_index(
                drop=True), interior_new.iloc[-1:].reset_index(drop=True)], axis=1)
            self.model['T_int_convergence'] = T_int_convergence
            if ('T_at_P' in self.model.columns) & ('Req' in self.model.columns):
                if not (isinstance(self.model['Req'].iloc[0], type(None))):
                    self.temp_plot()
            self.evaluate_errors()
            self.memory_usage()

            if (iteration > self.min_iteration) & ('mass' in self.model.columns) & (
                    '/model_parameters/target/internal_temperature' in self.model.columns):
                if self.check_convergence():
                    break
            print(self.model)

        print('Finished after {} iterations'.format(iteration))
        return
    
    @staticmethod
    def memory_usage():
        process = psutil.Process(os.getpid())
        memory_info = process.memory_info()
        total_memory = psutil.virtual_memory().total
        memory_used = memory_info.rss
        memory_percent = (memory_used / total_memory) * 100
        print(f"Memory usage: {memory_used / (1024 * 1024):.2f} MB "
          f"({memory_percent:.2f}% of total system memory)")
        return memory_percent
    
    def make_TP(self):
        model_available = False
        while (not model_available) :
            if (self.memory_usage() < 50) :
                try :
                    model_T = self.ML.load_model('Tstandard')
                    model_available = True
                except :    
                    print('T_P model not found, waiting for 30 secondes')
                    gc.collect()
                    time.sleep(30)
            else :
                print('Memory full, waiting for 30 secondes')
                gc.collect()
                time.sleep(30)
        sample = np.array([self.M,self.T_irr,self.Met,self.core,self.T_int+1,self.f_sed,self.kzz]).reshape(1, -1)
        T_P = (self.ML.use_model(model_T,sample)[0],self.ML.pressure*1e5)
        del model_T
        gc.collect()
        print('T_P profile made')
        return T_P
        
    def temp_plot(self):
        quality = self.quality_check(self.model)

        P_profile = self.model['P'].iloc[0]
        T_profile = self.model['T'].iloc[0]
        rho_profile = self.model['rho'].iloc[0]
        R_profile = self.model['R'].iloc[0]

        change = np.diff(P_profile) / np.diff(T_profile)
        change = [
            math.nan if x == float('inf') or x == -
            float('inf') else x for x in change]
        if len(np.where(change > 0.89 * np.nanmax(change))) > 0:
            jump_idx = np.where(change > 0.89 * np.nanmax(change))[0][-1]
        else:
            jump_idx = int(self.model['iindex'].iloc[0])

        plt.figure()
        plt.plot(
            self.model['/outputs/levels/temperature'].iloc[0],
            np.array(
                self.model['/outputs/levels/pressure'].iloc[0]) * 1e-5,
            label='Mass : {:.2f} Radius : {:.2f}\nTint : {:.1f} Tirr : {:.1f}'.format(
                self.model['mass'].iloc[0] * 1e-3 / M_J,
                self.model['Req'].iloc[0] * 1e-2 / R_J,
                self.model['T_int'].iloc[0],
                self.model['T_irr'].iloc[0]))
        plt.plot(
            self.model['/outputs/levels/temperature'].iloc[0],
            np.array(
                self.model['/outputs/levels/is_convective_smoothed'].iloc[0]) *
            np.array(
                self.model['/outputs/levels/pressure'].iloc[0]) *
            1e-5,
            linewidth=15,
            alpha=0.5)
        plt.plot(self.model['T'].iloc[0], self.model['P'].iloc[0])
        plt.gca().invert_yaxis()
        plt.yscale('log')
        plt.xscale('log')
        plt.axhline(y=self.model['P'].iloc[0][int(
            self.model['iindex'].iloc[0])], color='r', linestyle='--')
        #plt.axhline(y=df['P'].iloc[0][int(jump_idx)], color='k', linestyle='--')
        plt.legend()
        plt.xlabel('Temperature (K)')
        plt.ylabel('Pressure (bar)')
        plt.savefig('./../temp/plots/linked_TP_{}.pdf'.format(self.file_prefix))
        plt.close()

        plt.figure()
        # plt.plot(df['/outputs/layers/temperature'].iloc[0],np.array(df['/outputs/layers/pressure'].iloc[0])*1e-5)
        plt.plot(self.model['rho'].iloc[0], self.model['P'].iloc[0])
        plt.gca().invert_yaxis()
        plt.title('{}_{}'.format(self.file_prefix, quality))
        plt.yscale('log')
        plt.xscale('log')
        plt.xlabel(r'Density ($\rho$)')
        plt.ylabel('Pressure (bar)')
        plt.axhline(y=self.model['P'].iloc[0][int(
            self.model['iindex'].iloc[0])], color='r', linestyle='--')
        plt.axhline(y=self.model['P'].iloc[0]
                    [int(jump_idx)], color='k', linestyle='--')
        plt.savefig(
            './../temp/plots/linked_rhoP_{}.pdf'.format(self.file_prefix))
        plt.close()

    def evaluate_errors(self):
        if 'Ts' in self.model:
            self.model['T_erreur'] = 100 * \
                (self.model['Ts'] - self.model['T_at_P']) / self.model['T_at_P']
            self.model['P_erreur'] = 100 * \
                (self.model['Ps'] - self.model['P_link']) / self.model['P_link']
            self.model['g_erreur'] = 100 * \
                (self.model['gs'] - self.model['g_at_P']) / self.model['g_at_P']
            self.model['rho_erreur'] = 100 * \
                (self.model['rhos'] - self.model['rho_at_P']) / self.model['rho_at_P']
            self.model['Total_erreur'] = np.abs(self.model['T_erreur']) + np.abs(
                self.model['P_erreur']) + np.abs(self.model['g_erreur']) + np.abs(self.model['rho_erreur'])
            print('\nT_erreur : {}'.format(self.model['T_erreur'].iloc[0]))
            print('P_erreur : {}'.format(self.model['P_erreur'].iloc[0]))
            print('g_erreur : {}'.format(self.model['g_erreur'].iloc[0]))
            print('rho_erreur : {}'.format(self.model['rho_erreur'].iloc[0]))
            print(
                'Total_erreur : {}'.format(
                    self.model['Total_erreur'].iloc[0]))
            self.g_errors.append(self.model['g_erreur'].iloc[-1])
        else:
            print('Missing interior data')
        return

    def check_convergence(self):
        mass = np.round((self.model['mass'].iloc[-1] * 1e-3) / M_J, 4)
        T_int = np.round(
            self.model['/model_parameters/target/internal_temperature'].iloc[-1], 2)
        T_irr = np.round(self.model['T_irr'].iloc[-1], 2)
        Met = self.model['Met'].iloc[-1]
        core = self.model['core'].iloc[-1]
        eos_env = self.model['eos_env'].iloc[-1]
        f_sed = self.model['f_sed'].iloc[-1]
        kzz = self.model['kzz'].iloc[-1]
        if (len(self.model) > 0) & ('Ts' in self.model.columns):
            if (np.abs(self.model['T_erreur'].iloc[0]) < 0.2) & (np.abs(self.model['P_erreur'].iloc[0]) < 0.1) & (np.abs(self.model['g_erreur'].iloc[0]) < 0.2) & (np.abs(
                    self.model['Total_erreur'].iloc[0]) < 100) & (not pd.isnull(self.model['S'].iloc[-1])) & (np.abs(self.model['T_int_convergence'].iloc[-1])<1):  # & (np.abs(df['rho_erreur'].iloc[0]) < 1) & (np.abs(df['Total_erreur'].iloc[0]) < 1):
                print('Convergence criteria met')
                self.model.to_pickle(self.file)
                return True
        return False

    @staticmethod
    def quality_check(atmosphere):
        T = atmosphere['/outputs/layers/temperature'].iloc[0]
        P = atmosphere['/outputs/layers/pressure'].iloc[0]
        if np.max(T) != T[0]:
            print('Atmosphere model failed to meet basic convergence criterea')
            return False
        convective = np.array(atmosphere['/outputs/levels/is_convective_smoothed'].iloc[0]) * \
            np.array(atmosphere['/outputs/levels/temperature'].iloc[0]) * 1e-5
        convective = convective[~np.isnan(convective)]
        print('Convective layers : {}'.format(convective))
        if not monotonic(convective) :
            return False
        return True
    
    def check_if_model_failed(self):
        with open("./../temp/failed/failed_planets.txt", "r") as f:
            failed = f.readlines()
        failed = [x.strip() for x in failed]
        failed = ''.join(failed)
        if self.file in failed:
            print('Model found as failed')
            return True
        return False

    def run_make_grid(self, iterations):
        for _ in range(iterations):
            self.get_samples()
            self.get_current_results()
            if len(self.get_closest_model()) > 0 :
                self.fold_out()
                self.file = self.output_file + \
                    '/{:.4f}_{}_{}_{}_{}_{}_{:.1f}_{}_500.pkl'.format(self.M, self.T_int, self.T_irr, self.Met, self.core, self.f_sed, self.kzz, self.eos_env)
                if not self.check_if_model_failed():
                    self.make_grid()
        return
