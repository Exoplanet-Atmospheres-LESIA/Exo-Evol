"""
Created on Unknown

@author: cwilkinson
"""

import sys
sys.path.append("..")

import numpy as np
import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

from scipy.interpolate import interp1d
from sklearn.linear_model import LinearRegression

from inspect import signature

import warnings
warnings.filterwarnings("ignore")

def bilinear_interp(df, xi, yi):
    """
    Bilinear interpolation on a non-regular grid with missing values.
    
    Parameters:
    x (ndarray): 1D array of x-coordinates.
    y (ndarray): 1D array of y-coordinates.
    z (ndarray): 1D array of values at grid points.
    xi (float): x-coordinate at which to interpolate.
    yi (float): y-coordinate at which to interpolate.
    
    Returns:
    zi (float): Interpolated value at (xi, yi).
    """
    
    val_pivot_df = df[[df.columns[0],df.columns[1],df.columns[2]]].pivot_table(index=df.columns[0], columns=df.columns[1], values=df.columns[2])
    x = val_pivot_df.index
    y = val_pivot_df.columns
    z = val_pivot_df.to_numpy()
    
    # Find the indices of the four grid points surrounding (xi, yi).
    
    i0 = np.searchsorted(x, xi, side='right') - 1
    i1 = i0 + 1
    
    j0 = np.searchsorted(y, yi, side='right') - 1
    j1 = j0 + 1
    
    non_valid = True
    while non_valid :
        change = [False,False]
        if np.isnan(z[i0, j0]) or np.isnan(z[i1, j0]):
            j0 = j0-1
            change[0] = True
        if np.isnan(z[i0, j1]) or np.isnan(z[i1, j1]):
            j1 = j1+1
            change[1] = True
        if (True in change) :
            non_valid = True
        else :
            non_valid = False
    
    # Compute the distances between (xi, yi) and the four grid points.
    dx = xi - x[i0]
    dy = yi - y[j0]
    dxi = x[i1] - xi
    dyi = y[j1] - yi
    
    # Compute the weights for the four grid points.
    w00 = dxi * dyi
    w10 = dx * dyi
    w01 = dxi * dy
    w11 = dx * dy
    
    # Compute the interpolated value using bilinear interpolation.
    zi = w00 * z[i0, j0] + w10 * z[i1, j0] + w01 * z[i0, j1] + w11 * z[i1, j1]
    
    return zi/np.sum([w00,w10,w01,w11])


def rk4_solver(f, x_vals, y0):
    """
    Fourth-order Runge-Kutta (RK4) solver for a system of ODEs.

    Arguments:
    f: Function that defines the system of ODEs. The function should take two arguments:
       t - current time
       y - current state vector
       and return the derivative of y with respect to t.
    x_vals : solve along x axis.
    y0: Initial state vector.

    Returns:
    x_vals: Array of x values.
    y_vals: Array of state vectors corresponding to the x values.
    """
    
    sig = signature(f)
    params = sig.parameters
    
    y_vals = np.zeros(np.shape(x_vals))  # Array to store state vectors
    
    y_vals[0] = y0  # Initial state vector
    
    for i in range(0,len(x_vals)-1):
        x = x_vals[i]
        y = y_vals[i]
        dx = x_vals[i+1] - x_vals[i]
        
        if  len(params) == 2 :
            k1 = dx * f(x, y)
            k2 = dx * f(x + dx/2, y + k1/2)
            k3 = dx * f(x + dx/2, y + k2/2)
            k4 = dx * f(x + dx, y + k3)
        else :
            k1 = dx * f(x)
            k2 = dx * f(x + dx/2)
            k3 = dx * f(x + dx/2)
            k4 = dx * f(x + dx)            
        
        y_vals[i + 1] = y + (k1 + 2*k2 + 2*k3 + k4) / 6
        
    return x_vals, y_vals


def custom_round(x, base=25):
    """
    Rounds a given number to the nearest multiple of a specified base.

    Args:
        x (float): The number to be rounded.
        base (int): The base to round to. Default is 50.

    Returns:
        int: The rounded number.

    Examples:
        custom_round(147)  # Returns 150
        custom_round(162, base=10)  # Returns 160
    """
    rounded_number = int(base * round(float(x) / base))
    return rounded_number


def spleen_smooth(X, Y, df=2):
    """
    Performs spline smoothing on the given data.

    Args:
        X (array-like): The input data for the predictor variable.
        Y (array-like): The input data for the response variable.
        df (int): The degrees of freedom for the spline basis. Default is 2.

    Returns:
        array-like: The predicted values obtained from the spline smoothing.

    Examples:
        X = [1, 2, 3, 4, 5]
        Y = [2, 4, 6, 8, 10]
        spleen_smooth(X, Y, df=3)  # Returns [2.0, 4.0, 6.0, 8.0, 10.0]
    """
    x_basis = cr(X, df, constraints="center")  # Generate spline basis with different degrees of freedom
    model = LinearRegression().fit(x_basis, Y)  # Fit model to the data
    y_hat = model.predict(x_basis)  # Get estimates
    return y_hat


def f_x_y(x, y, order=2):
    """
    Creates a function that interpolates smoothed data points.

    Args:
        x (array-like): The input data for the x-coordinate.
        y (array-like): The input data for the y-coordinate.
        order (int): The degrees of freedom for spline smoothing. Default is 2.

    Returns:
        callable: A function that performs interpolation on the smoothed data points.

    Examples:
        x = [1, 2, 3, 4, 5]
        y = [2, 4, 6, 8, 10]
        f = f_x_y(x, y, order=3)
        f(2.5)  # Returns the interpolated value at x=2.5 based on smoothed data points
    """
    y_smoothed = spleen_smooth(x, y, order)  # Perform spline smoothing on the data
    return interp1d(x, y_smoothed)


def clump(a):
    """
    Extracts clumps of unmasked values from an array.

    Args:
        a (array-like): The input array.

    Returns:
        list: A list of clumps (subarrays) containing unmasked values.

    Examples:
        a = [1, np.nan, 2, 3, np.nan, np.nan, 4, 5]
        using_clump(a)  # Returns [[1], [2, 3], [4, 5]]
    """
    masked_array = np.ma.masked_invalid(a)  # Create a masked array with invalid values masked
    clumps = np.ma.clump_unmasked(masked_array)  # Get clumps of unmasked values from the masked array
    result = [a[s] for s in clumps]  # Extract clumps from the original array

    return result

def identify_edge_and_interior_points(grid_points):
    """
    Identify edge and interior points in a 6D grid.

    Parameters:
    - grid_points: np.array of shape (n_points, 6), where each row is a point in 6D space.

    Returns:
    - edge_points: np.array of points on the edge of the grid.
    - interior_points: np.array of points inside the grid.
    """

    # Calculate min and max values for each dimension
    mins = np.min(grid_points, axis=0)  # Shape (6,)
    maxs = np.max(grid_points, axis=0)  # Shape (6,)
    
    edge_points = []
    interior_points = []
    
    # Iterate through each point in the grid
    for point in grid_points:
        # Check if the point is on the edge by comparing to mins or maxs
        if np.any(point == mins) or np.any(point == maxs):
            edge_points.append(point)
        else:
            interior_points.append(point)
    
    # Convert back to numpy arrays
    edge_points = np.array(edge_points)
    interior_points = np.array(interior_points)
    
    return edge_points, interior_points

def check_point_edge_or_interior(new_point, mins, maxs):
    """
    Check if a new point is on the edge or in the interior of the grid.

    Parameters:
    - new_point: np.array of shape (6,), the point in 6D space to check.
    - mins: np.array of shape (6,), the minimum values along each dimension.
    - maxs: np.array of shape (6,), the maximum values along each dimension.

    Returns:
    - "Edge" or "Interior" depending on whether the point is on the edge or inside.
    """

    # Check if the point is on the edge by comparing to mins or maxs
    if np.any(new_point == mins) or np.any(new_point == maxs):
        return "Edge"
    else:
        return "Interior"
    
def generate_edge_points(mins, maxs):
    """
    Generates all possible edge points of a grid defined by 6 coordinates.
    
    Parameters:
    - mins: np.array of shape (6,), the minimum values along each dimension.
    - maxs: np.array of shape (6,), the maximum values along each dimension.
    
    Returns:
    - edge_points: np.array of shape (n, 6), where n is the number of edge points.
    """
    
    # Prepare an array to store the edge points
    edge_points = []
    
    # Create a list of ranges for each dimension, where each dimension can either take
    # the min or max value, or any value in between (i.e. interior points will be ignored).
    ranges = [(mins[i], maxs[i]) for i in range(6)]
    
    # Generate all possible combinations of min and max at the edges (ignores interior points)
    for combination in itertools.product(*ranges):
        # Add this combination as a possible edge point
        edge_points.append(combination)
    
    # Convert list to np.array for easier manipulation
    edge_points = np.array(edge_points)
    
    return edge_points

def find_missing_points(grid_points):
    """
    Finds all missing points from a list of grid points.
    
    Parameters:
    - grid_points: np.array of shape (n, d), where n is the number of grid points and
      d is the dimensionality (e.g., d=6 for a 6D grid).
    
    Returns:
    - missing_points: np.array of all missing points that should be in the grid but are not.
    """
    
    # Get the dimensionality of the grid
    num_dims = grid_points.shape[1]
    
    # Find the minimum and maximum values for each dimension
    mins = np.min(grid_points, axis=0)
    maxs = np.max(grid_points, axis=0)
    
    # Generate the full grid of points based on the min and max values
    grid_ranges = [np.arange(mins[dim], maxs[dim] + 1) for dim in range(num_dims)]
    full_grid = np.array(list(itertools.product(*grid_ranges)))
    
    # Convert the provided grid points and full grid points to sets for comparison
    grid_points_set = set(map(tuple, grid_points))
    full_grid_set = set(map(tuple, full_grid))
    
    # The missing points are those in the full grid but not in the provided grid points
    missing_points_set = full_grid_set - grid_points_set
    
    # Convert the set of missing points back to a numpy array
    missing_points = np.array(list(missing_points_set))
    
    return missing_points

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        # linear interpolation of NaNs
            nans, x= nan_helper(y)
            y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """
    return np.isnan(y), lambda z: z.nonzero()[0]

def interpolate_negatives_rowwise(arr):
    # Create a copy to avoid modifying the original array
    interpolated_arr = arr.copy()
    
    for i in range(arr.shape[0]):  # Iterate over each row
        row = arr[i]
        x = np.arange(row.size)  # Indices of the row
        mask = row >= 0  # Mask of non-negative values
        
        if np.any(mask):  # Ensure there are valid values to interpolate
            # Interpolation function for valid (non-negative) values
            f = interp1d(x[mask], row[mask], kind='linear', bounds_error=False, fill_value="extrapolate")
            interpolated_arr[i] = f(x)  # Interpolate all values
        else:
            # If no valid values, set the row to NaN or handle appropriately
            interpolated_arr[i] = np.nan
    
    return interpolated_arr

def gradient(arr, n=4) :
    return np.gradient(np.array(arr), n)

def monotonic(x):
    dx = np.diff(x)
    return np.all(dx <= 0) or np.all(dx >= 0)
    
