"""
Created on Thu Aug 10 15:58:45 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

import pandas as pd
import numpy as np
import glob
import matplotlib.pyplot as plt 

from scipy.interpolate import interp1d, LinearNDInterpolator

from statistics import mode

from tqdm import tqdm
from joblib import Parallel, delayed
import json

import pyarrow.feather as feather
import pickle as pkl

from python_modules.grid_data_processing_module import *
from python_modules.physics_module import *

import warnings
warnings.filterwarnings("ignore")

R_J = 69911000
M_J = 1.898*1e27
R_s = 696340000
M_s = 1.989*1e30
R_J = 69911000
M_J = 1.898*1e27
R_E = 6371*1e3
M_E = 5.972*1e24
G_u = 6.674*1e-11
Na = 6.022*1e23
Kb = 1.380649*1e-23
sigma = 5.67*1e-8

def T_int_interpolator(data_folder,planet_type,M,T_irr,Met,core,clouds=False) :
    print(M)
    print(data_folder)
    df = load_grid(data_folder,planet_type,EOS='ker',Mass=M,Met=Met,
                                Core=core,T_irr=T_irr,T_int=[0,2500],clouds=clouds)
    
    x = np.array(df['Mass_Jup'])
    y = np.array(df['T_irr'])
    z =  np.log10(np.array(df['Met']))
    k = np.array(df['core_Earth'])
    r = np.array(df['Radius_Jup'])
    if clouds :
        l = np.array(df['f_sed'])
        interp_T_int = LinearNDInterpolator(list(zip(x,y,z,k,r,l)), df['T_int'],rescale=True)
    else :
        interp_T_int = LinearNDInterpolator(list(zip(x,y,z,k,r)), df['T_int'],rescale=True)
        
    return interp_T_int  

def create_T_int_interpolator_grid(M,n,clouds,planet_type,output_path) :
    data_folder = output_path.split('data')[0]+'data/current_exoevol_grid/ker'
    if n < 1 :
        print('The number of interpolators should at least be equal to or greater than 1')
        print('Setting n to 1')
        n = 1
    Masses = np.linspace(M[0],M[1],n+1)
    Masses = np.round(Masses,5)
    mass_bounds = []
    for ii in range(len(Masses)-1):
        mass_bounds.append((Masses[ii]*0.8,Masses[ii+1]*(1.2)))
                
    for mass_bound in mass_bounds  :
        interpolator = T_int_interpolator(data_folder,planet_type,M=mass_bound,T_irr=[0,3000],Met=[-2,2],core=[0,50],clouds=clouds) 
        if not clouds :
            file_out = '{}/Tint_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        else :
            file_out = '{}/Tint_clouds_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        with open(file_out, 'wb') as f:
                pkl.dump(interpolator, f)
                
    return
                
                
def R_interpolator(data_folder,planet_type,M,T_irr,Met,core, clouds=False) :
    print(M)
    df = load_grid(data_folder,planet_type,EOS='ker',Mass=M,Met=Met,
                                Core=core,T_irr=T_irr,T_int=[0,2500],clouds=clouds)

    x = np.array(df['Mass_Jup'])
    y = np.array(df['T_irr'])
    z =  np.log10(np.array(df['Met']))
    k = np.array(df['core_Earth'])
    j = np.array(df['T_int'])
    if clouds :
        l = np.array(df['f_sed'])
        interp_R = LinearNDInterpolator(list(zip(x,y,z,k,j,l)), df['Radius_Jup'],rescale=True)
    else :
        interp_R = LinearNDInterpolator(list(zip(x,y,z,k,j)), df['Radius_Jup'],rescale=True)
    
    return interp_R  

def create_R_interpolator_grid(M,n,clouds,planet_type,output_path) :
    data_folder = output_path.split('data')[0]+'data/current_exoevol_grid/ker'
    print(data_folder)
    Masses = np.linspace(M[0],M[1],n+1)
    Masses = np.round(Masses,5)
    mass_bounds = []
    for ii in range(len(Masses)-1):
        mass_bounds.append((Masses[ii]*0.8,Masses[ii+1]*(1.2)))
        
    #interpolators = Parallel(n_jobs=2)(delayed(T_int_interpolator)(mass_bound,[0,3000],[-2,2],core=[0,50]) for mass_bound in mass_bounds)

    #for mass_bound, interpolator in zip(mass_bounds,interpolators)  :
        #interpolator = T_int_interpolator(M=mass_bound,T_irr=[0,3000],Met=[-2,2],core=[0,50]) 
        #with open('./data/interpolators/Tint_interpolators/{}.pkl'.format(mass_bound), 'wb') as f:
       #        pkl.dump(interpolator, f)
                
    for mass_bound in mass_bounds  :
        interpolator = R_interpolator(data_folder,planet_type,M=mass_bound,T_irr=[0,2000],Met=[-2,2],core=[0,50],clouds=clouds) 
        if not clouds :
            file_out = '{}/radius_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        else :
            file_out = '{}/radius_clouds_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        with open(file_out, 'wb') as f:
                pkl.dump(interpolator, f)
                
    return
                         
def S_interpolator(data_folder,planet_type,M,T_irr,Met,core, clouds=False) :
    print(M)
    df = load_grid(data_folder,planet_type,EOS='ker',Mass=M,Met=Met,
                                Core=core,T_irr=T_irr,T_int=[0,2500],clouds=clouds)

    x = np.array(df['Mass_Jup'])
    y = np.array(df['T_irr'])
    z =  np.log10(np.array(df['Met']))
    k = np.array(df['core_Earth'])
    j = np.array(df['T_int'])
    if clouds :
        l = np.array(df['f_sed'])
        interp_S = LinearNDInterpolator(list(zip(x,y,z,k,j,l)), df['S'],rescale=True)
    else :
        interp_S = LinearNDInterpolator(list(zip(x,y,z,k,j)), df['S'],rescale=True)
    
    return interp_S  

def create_S_interpolator_grid(M,n,clouds,planet_type,output_path) :
    data_folder = output_path.split('data')[0]+'data/current_exoevol_grid/ker'
    Masses = np.linspace(M[0],M[1],n+1)
    Masses = np.round(Masses,5)
    mass_bounds = []
    for ii in range(len(Masses)-1):
        mass_bounds.append((Masses[ii]*0.8,Masses[ii+1]*(1.2)))
        
    #interpolators = Parallel(n_jobs=2)(delayed(T_int_interpolator)(mass_bound,[0,3000],[-2,2],core=[0,50]) for mass_bound in mass_bounds)

    #for mass_bound, interpolator in zip(mass_bounds,interpolators)  :
        #interpolator = T_int_interpolator(M=mass_bound,T_irr=[0,3000],Met=[-2,2],core=[0,50]) 
        #with open('./data/interpolators/Tint_interpolators/{}.pkl'.format(mass_bound), 'wb') as f:
       #        pkl.dump(interpolator, f)
                
    for mass_bound in mass_bounds  :
        interpolator = S_interpolator(data_folder,planet_type,M=mass_bound,T_irr=[0,2000],Met=[-2,2],core=[0,50],clouds=clouds) 
        if not clouds :
            file_out = '{}/entropy_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        else :
            file_out = '{}/entropy_clouds_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        with open(file_out, 'wb') as f:
                pkl.dump(interpolator, f)
                
    return
                
def dS_dt_interpolator(data_folder,planet_type,M,T_irr,Met,core, clouds=False) :
    print(M)
    df = load_grid(data_folder,planet_type,EOS='ker',Mass=M,Met=Met,
                                Core=core,T_irr=T_irr,T_int=[0,2500],clouds=clouds)
    
    df = ds_dt(df)

    x = np.array(df['Mass_Jup'])
    y = np.array(df['T_irr'])
    z =  np.log10(np.array(df['Met']))
    k = np.array(df['core_Earth'])
    j = np.array(df['T_int'])
    if clouds :
        l = np.array(df['f_sed'])
        interp_ds_dt = LinearNDInterpolator(list(zip(x,y,z,k,j,l)), df['ds_dt'],rescale=True)
    else :
        interp_ds_dt = LinearNDInterpolator(list(zip(x,y,z,k,j)), df['ds_dt'],rescale=True)
    
    return interp_ds_dt 

def create_dS_dt_interpolator_grid(M,n,clouds,planet_type,output_path) :
    data_folder = output_path.split('data')[0]+'data/current_exoevol_grid/ker'
    Masses = np.linspace(M[0],M[1],n+1)
    Masses = np.round(Masses,5)
    mass_bounds = []
    for ii in range(len(Masses)-1):
        mass_bounds.append((Masses[ii]*0.8,Masses[ii+1]*(1.2)))
        
    #interpolators = Parallel(n_jobs=2)(delayed(T_int_interpolator)(mass_bound,[0,3000],[-2,2],core=[0,50]) for mass_bound in mass_bounds)

    #for mass_bound, interpolator in zip(mass_bounds,interpolators)  :
        #interpolator = T_int_interpolator(M=mass_bound,T_irr=[0,3000],Met=[-2,2],core=[0,50]) 
        #with open('./data/interpolators/Tint_interpolators/{}.pkl'.format(mass_bound), 'wb') as f:
       #        pkl.dump(interpolator, f)
                
    for mass_bound in mass_bounds  :
        interpolator = dS_dt_interpolator(data_folder,planet_type,M=mass_bound,T_irr=[0,2000],Met=[-2,2],core=[0,50],clouds=clouds) 
        if not clouds :
            file_out = '{}/entropy_evol_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        else :
            file_out = '{}/entropy_evol_clouds_{}_{}.pkl'.format(output_path,planet_type,mass_bound)
        with open(file_out, 'wb') as f:
                pkl.dump(interpolator, f)
                
    return
                
def age_interpolator(df, n_age_interpolators, planet_type, output_path, path_entropy_evol, path_entropy, path_radius, clouds=False) :
    print('In making age interpolator(s)')
    
    df = evaluate_age(df, clouds)

    x = np.array(df['Mass_Jup'])
    y = np.array(df['T_irr'])
    z =  np.log10(np.array(df['Met']))
    k = np.array(df['core_Earth'])
    j = np.array(df['T_int'])
    
    if n_age_interpolators > 0 :
        dfs = split_dataframe(df, int(n_age_interpolators)+1)
    else :
        dfs  = [df]
        
    for df in tqdm(dfs) :
        interp_age = None
        if (clouds) & (len(df)>10):
            print('Making age interpolator')
            Mass_range = (df['Mass_Jup'].min(),df['Mass_Jup'].max())
            print(Mass_range)
            l = np.array(df['f_sed'])
            interp_age = LinearNDInterpolator(list(zip(x,y,z,k,j,l)), df['t'],rescale=True)
        elif (len(df)>10) :
            print('Making age interpolator')
            Mass_range = (df['Mass_Jup'].min(),df['Mass_Jup'].max())
            print(Mass_range)
            interp_age = LinearNDInterpolator(list(zip(x,y,z,k,j)), df['t'],rescale=True)
            
        if (not clouds)  :
            file_out = '{}/age_{}_{}.pkl'.format(output_path,planet_type,Mass_range)
        else :
            file_out = '{}/age_clouds_{}_{}.pkl'.format(output_path,planet_type,Mass_range)
            
        if interp_age is not None :
            print('Saving age interpolator')
            with open(file_out, 'wb') as f:
                    pkl.dump(interp_age, f)

    return interp_age 

def create_age_interpolator_grid(df,n_age_interpolators,clouds,planet_type,output_path,path_entropy_evol,path_entropy,path_radius) :
    print('Number of data points',len(df))
    interpolator = age_interpolator(df,n_age_interpolators,planet_type,output_path,path_entropy_evol,path_entropy,path_radius, clouds=clouds) 
    return

def remove_neg_rad (df) :
    neg_rad = []
    for ii, rad in enumerate(df['thermal_up']):
        rad = np.array(rad)
        try :
            if (len(rad[rad<0]) < 10) :
                neg_rad.append(0)
            else :
                neg_rad.append(1)
        except :
            neg_rad.append(1)
            print('here',rad)
    
    df['neg_thermal'] = neg_rad
        
    return  df[df['neg_thermal']==0]
                  
def flux_interpolator(data_folder,planet_type,M,T_irr,Met,core,output_path,clouds=False,wavelength_bounds=[-np.inf,np.inf]) :
    print(M)
    print(wavelength_bounds)
    print('T_irr is',T_irr)
    df = load_grid(data_folder,planet_type,EOS='ker',Mass=M,Met=Met,
                                Core=core,T_irr=T_irr,T_int=[0,2500],clouds=clouds)
    
    #age_interp, _ = get_closest_interpolator(mass= M[0] + (M[1]-M[0])/2,T_irr=T_irr,interpolator_type='age')

    
    print(len(df))
    photometric_filters = photo_columns()
    df = remove_neg_rad (df)
    replace_none_nan(df,['radiosity_up','depth_up','wavenumber_up'])
    
    df = df[df['thermal_up'].notna()]
    df = df[df['depth_up'].notna()]
    df = df[df['wavenumber_up'].notna()]
    print('data length',len(df))
    for photo_filter in photometric_filters :
        df = df[df[photo_filter].notna()]
        
    #df['radiosity_up'] = convert_radiosity(df['radiosity_up'],df['wavenumber_up'])
    df['thermal_up'] = convert_radiosity(df['thermal_up'],df['wavenumber_up'])
    df['depth_up'] = convert_transit(depth = df['depth_up'], old_radius=df['/model_parameters/target/radius_1e5Pa'], computed_radius=df['Radius_Jup'], light_source_radius=df['/model_parameters/light_source/radius'])
    df['wavelength_up'] = wavenumber_to_wavelength(df['wavenumber_up'])
        
    radiosity_radius = np.array(df['thermal_up'].tolist())
    for photo_filter in photometric_filters :
        radiosity_radius = np.c_[radiosity_radius, np.array(df[photo_filter])]
        print(photo_filter)
        print(df[photo_filter])
    radiosity_radius_ageless = np.c_[radiosity_radius,np.array(df['Radius_Jup'])]
    
    if clouds :
        print('Using clouds')
        radiosity_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])), radiosity_radius_ageless,rescale=True)
    else :
        radiosity_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'])), radiosity_radius_ageless,rescale=True)
    
    if not clouds : 
        with open('{}/radiosity_ageless_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(radiosity_interpolator, f)
    else :
        with open('{}/radiosity_ageless_clouds_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(radiosity_interpolator, f)
                
    #if clouds :
    #    age = age_interp (df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])
    #else :
    #    age = age_interp (df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'])

    if clouds :
        print('Using clouds - Evaluating age')
        age_interpolators = glob.glob('./../../data/interpolators/age/*cloudy*')
        age_interpolators = [load_pkl(age_interpolator) for age_interpolator in age_interpolators]
        age_values = [age_interpolator(df[['Mass_Jup','T_irr','Met','core_Earth','T_int','f_sed']].values.tolist()) for age_interpolator in age_interpolators]
        df['age'] = merge_lists(*age_values)
    else :
        age_interpolators = glob.glob('./../../data/interpolators/age/*')
        age_interpolators = [load_pkl(age_interpolator) for age_interpolator in age_interpolators]
        age_values = [age_interpolator(df[['Mass_Jup','T_irr','Met','core_Earth','T_int']].values.tolist()) for age_interpolator in age_interpolators]
        df['age'] = merge_lists(*age_values)
        
    df = df[df['age'].notna()]
    corrected_df = []
    if clouds :
        df_ = df.drop_duplicates(subset=['Mass_Jup','T_irr','Met','core_Earth','f_sed'])
        parameters = df_[['Mass_Jup','T_irr','Met','core_Earth','f_sed']].values.tolist()
    else :
        df_ = df.drop_duplicates(subset=['Mass_Jup','T_irr','Met','core_Earth'])
        parameters = df_[['Mass_Jup','T_irr','Met','core_Earth']].values.tolist()
    
    print('Number of parameters',len(parameters))
    for parameter in tqdm(parameters):
        if clouds :
            Mass, T_irr_, Met, core, f_sed = parameter
            df_ = df[(df['Mass_Jup']==Mass) & (df['T_irr']==T_irr_) & (df['Met']==Met) & (df['core_Earth']==core) & (df['f_sed']==f_sed)]
        else :
            Mass, T_irr_, Met, core = parameter
            df_ = df[(df['Mass_Jup']==Mass) & (df['T_irr']==T_irr_) & (df['Met']==Met) & (df['core_Earth']==core)]
        
        df_ = df_.sort_values(by='T_int')
        df_ = df_[df_['age'].isin(list(np.minimum.accumulate(df_['age'])))]
        corrected_df.append(df_)
        
    df = pd.concat(corrected_df,ignore_index=True) 
    
    print('Length after filter',len(df)) 
    
    with open('{}/example_{}_{}.pkl'.format(output_path,T_irr,M), 'wb') as f:
        pkl.dump(df.iloc[:1], f)
    
    radiosity_radius = np.array(df['thermal_up'].tolist())
    for photo_filter in photometric_filters :
        radiosity_radius = np.c_[radiosity_radius, np.array(df[photo_filter])]
        print(photo_filter)
        print(df[photo_filter])
    radiosity_radius = np.c_[radiosity_radius,np.array(df['Radius_Jup'])]
    radiosity_radius = np.c_[radiosity_radius,np.array(df['age'])]
    
    if clouds :
        print('Using clouds - Making age flux interpolator')
        radiosity_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])), radiosity_radius,rescale=True)
    else :
        radiosity_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'])), radiosity_radius,rescale=True)
    
    if not clouds : 
        with open('{}/radiosity_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(radiosity_interpolator, f)
    else :
        with open('{}/radiosity_clouds_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(radiosity_interpolator, f)
    
    transit_radius = np.c_[np.array(df['depth_up'].tolist()),np.array(df['Radius_Jup'])]               
    if clouds :
        transit_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])), transit_radius,rescale=True)
    else :
        transit_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr'],np.log10(df['Met']),df['core_Earth'],df['T_int'])), transit_radius,rescale=True)

    if not clouds : 
        with open('{}/transit_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(transit_interpolator, f)
    else :
        with open('{}/transit_clouds_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(transit_interpolator, f)
    
    return 

def create_flux_interpolator_grid(M,n,clouds,planet_type,output_path,idx) :
    data_folder = output_path.split('data')[0]+'data/current_exoevol_grid/ker'
    mass_bound = (M[0],M[-1])
    flux_interpolator(data_folder,planet_type,M=mass_bound,T_irr=(0,2000),Met=[-10,10],core=[0,50],output_path=output_path,clouds=clouds) 
    return
                
def profile_interpolator(data_folder,planet_type,M,T_irr,Met,core,output_path,clouds=False,wavelength_bounds=[-np.inf,np.inf]) :
    print(M)
    new_pressure = np.logspace(-4,8,100)
    print(new_pressure)
    df = load_grid(data_folder,planet_type,EOS='ker',Mass=M,Met=Met,
                                Core=core,T_irr=T_irr,T_int=[0,2500],clouds=clouds)
    
    replace_none_nan(df,['T_profile','R_profile','rho_profile'])
    df = df[df['T_profile'].notna()]
    df = df[df['R_profile'].notna()]
    df = df[df['rho_profile'].notna()]
    
    new_R_profile = []
    new_T_profile = []
    new_rho_profile = []
    for old_pressure, R_profile, T_profile, rho_profile in zip(df['P_profile'],df['R_profile'],df['T_profile'],df['rho_profile']) :
        new_R_profile.append(profile_at_pressure(R_profile,old_pressure,new_pressure))
        new_T_profile.append(profile_at_pressure(T_profile,old_pressure,new_pressure))
        new_rho_profile.append(profile_at_pressure(rho_profile,old_pressure,new_pressure))
     
    df['R_profile_new'] = new_R_profile   
    df['T_profile_new'] = new_T_profile
    df['rho_profile_new'] = new_rho_profile
        
    Temperature_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr']
                                                         ,np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])), np.log10(np.array(df['T_profile_new'].tolist())),rescale=True)
    
    Radius_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr']
                                                         ,np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])), np.log10(np.array(df['R_profile_new'].tolist())),rescale=True)
    
    rho_interpolator = LinearNDInterpolator(list(zip(df['Mass_Jup'],df['T_irr']
                                                         ,np.log10(df['Met']),df['core_Earth'],df['T_int'],df['f_sed'])), np.log10(np.array(df['rho_profile_new'].tolist())),rescale=True)
    
    if not clouds : 
        with open('{}/T_profile_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(Temperature_interpolator, f)
        with open('{}/R_profile_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(Radius_interpolator, f)
        with open('{}/rho_profile_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(rho_interpolator, f)
    else :
        with open('{}/T_profile_clouds_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(Temperature_interpolator, f)
        with open('{}/R_profile_clouds_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(Radius_interpolator, f)
        with open('{}/rho_profile_clouds_{}_{}_{}.pkl'.format(output_path,planet_type,T_irr,M), 'wb') as f:
                pkl.dump(rho_interpolator, f)
                
    return 

def create_profile_interpolator_grid(M,n,clouds,planet_type,output_path,idx) :
    data_folder = output_path.split('data')[0]+'data/current_exoevol_grid/ker'
    Masses = np.linspace(M[0],M[1],n+1)
    Masses = np.round(Masses,5)
    mass_bounds = []
    for ii in range(len(Masses)-1):
        mass_bounds.append((Masses[ii]*0.8,Masses[ii+1]*(1.2)))
        
    #interpolators = Parallel(n_jobs=2)(delayed(T_int_interpolator)(mass_bound,[0,3000],[-2,2],core=[0,50]) for mass_bound in mass_bounds)

    #for mass_bound, interpolator in zip(mass_bounds,interpolators)  :
        #interpolator = T_int_interpolator(M=mass_bound,T_irr=[0,3000],Met=[-2,2],core=[0,50]) 
        #with open('./data/interpolators/Tint_interpolators/{}.pkl'.format(mass_bound), 'wb') as f:
       #        pkl.dump(interpolator, f)
    mass_bounds = mass_bounds[int((n/4)*idx):int((n/4)*(idx+1))]     
    for mass_bound in mass_bounds  :
        profile_interpolator(data_folder,planet_type,M=mass_bound,T_irr=(0,2000),Met=[-2,2],core=[0,50],output_path=output_path,clouds=clouds) 

def spectra_at_wavelength(spectra,old_wavelength,new_wavelength) :
    return np.array(interp1d(old_wavelength,spectra,bounds_error=False)(new_wavelength))

def profile_at_pressure(profile,old_pressure,new_pressure) :
    return np.array(interp1d(old_pressure,profile,bounds_error=False,fill_value=0)(new_pressure))

def interpolate_to_regular_grid_ML_features(df) :
    P_max = 1e9
    P_min = 1e-4
    
    P_range = np.logspace(np.log10(P_min),np.log10(P_max),50)
    new_T = []
    new_dT = []
    for ii in range(0,len(df)):
        T = interp1d(df['P_profile'].iloc[ii],df['T_profile'].iloc[ii],bounds_error=False,fill_value=(np.min(df['P_profile'].iloc[ii]), np.max(df['P_profile'].iloc[ii])))(P_range)
        new_T.append(T)
        new_dT.append(np.append(np.diff(T),0))
    
    df['T_adj'] = new_T
    df['dT_adj'] = new_dT
    df['P_adj'] = [P_range for ii in range(len(new_T))]
    
    return df
