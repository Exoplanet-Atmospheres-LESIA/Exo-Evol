"""
Created on Tue Jan  7 16:54:00 2024

@author: cwilkinson
"""

import sys
sys.path.append("..")

import glob
import numpy as np
import difflib
import pandas as pd
import random
import itertools
from scipy.interpolate import interp1d

from python_modules.physics_module import diluted_planet, wavenumber_to_wavelength, convert_radiosity
from python_modules.interface_module import load_filter

def spectra_at_wavelength(spectra,old_wavelength,new_wavelength) :
    return np.array(interp1d(old_wavelength,spectra,bounds_error=False)(new_wavelength))

def SPHERE_IFS(bands) :
    YJH = np.array([0.9575,0.9719,0.9868,1.0022,1.0181,1.0344,1.0512,1.0683,\
    1.0859,1.1037,1.1218,1.1402,1.1589,1.1778,1.1968,1.2161,1.2354,1.2549,1.2744,\
    1.294,1.3135,1.3331,1.3527,1.3721,1.3915,1.4107,1.4297,1.4486,1.4673,1.4857,\
    1.5039,1.5217,1.5392,1.5564,1.5731,1.5895,1.6054,1.6208,1.6358])
    YJ = np.array([0.9576,0.966,0.9746,0.9834,0.9924,1.0017,1.0111,1.0206,1.0304,1.0402,1.0502,\
    1.0603,1.0705,1.0808,1.0911,1.1015,1.112,1.1225,1.133,1.1436,1.1541,\
    1.1646,1.1751,1.1855,1.196,1.2063,1.2165,1.2267,1.2368,1.2467,1.2565,\
    1.2662,1.2757,1.2851,1.2942,1.3032,1.312,1.3205,1.3288])
    df = pd.DataFrame()
    df['bands'] = ['YJH','YJ']
    df['wavelengths'] = [YJH,YJ]
    return df[df['bands']==bands]['wavelengths'].iloc[0]

def SPHERE_IRDIS(filter_name) :
    IRDIS_filters = ['B_Y','B_J','B_H','B_Ks','D_Y23','D_J23','D_H23','D_ND-H23','D_H34','D_K12']
    IRDIS_Central_Wavelength = [[1.043],[1.245],[1.625],[2.182],[1.022,1.076],[1.190,1.273],[1.593,1.667],[1.593,1.667],[1.667,1.733],[2.110,2.251]]
    IRDIS_wavelength_errors = [[0.140],[0.240],[0.290],[0.300],[0.049, 0.050],[0.042, 0.046],[0.052, 0.054],[0.052, 0.054],[0.054, 0.057],[0.102, 0.109]]
    filter_info = pd.DataFrame()
    filter_info['filters'] = IRDIS_filters 
    filter_info['Wavelength'] = IRDIS_Central_Wavelength
    filters_files = glob.glob('./../../data/filters/SPHERE/*.dat')
    filters_names = [filter.split('/')[-1].replace('SPHERE_IRDIS_','').replace('.dat','') for filter in filters_files]
    filter = difflib.get_close_matches(filter_name, filters_names,n=1,cutoff=0)[0]
    df = load_filter(filters_files[filters_names.index(filter)])
    df['wavelength'] = df['wavelength']*1e-3
    df['filter'] = filter
    return df, filter_info[filter_info['filters']==difflib.get_close_matches(filter_name, filter_info['filters'],n=1,cutoff=0)[0]]

def apply_spectro(model_wavelengths,model_flux,wavelength_spectro):
    spectrum_at_wavelength_spectro = spectra_at_wavelength(model_flux,model_wavelengths,wavelength_spectro)
    return wavelength_spectro, spectrum_at_wavelength_spectro

def apply_filter(model_wavelengths,model_flux,filter_wl,filter_tp):
    if (np.mean(filter_wl)>100) & (np.mean(filter_wl)<10000) :
        filter_wl = np.array(filter_wl)*1e-3
    elif  np.mean(filter_wl)>10000 :
        filter_wl = np.array(filter_wl)*1e-4
    spectrum_at_filter = spectra_at_wavelength(model_flux,model_wavelengths,np.array(filter_wl))
    integrated_flux = np.trapz(np.array(filter_tp)*np.array(spectrum_at_filter), x=np.array(filter_wl))
    norm_filter = np.trapz(np.array(filter_tp), x=np.array(filter_wl))
    return integrated_flux/norm_filter

def make_IFS_IRDIS_spectrum(model_wavelengths,model_flux,bands,filters):
    if len(bands) > 0 :
        wavelength_spectro, spectrum_at_wavelength_spectro = apply_spectro(model_wavelengths,model_flux,SPHERE_IFS(bands))
    else :
        wavelength_spectro, spectrum_at_wavelength_spectro = ([],[])
    if len(filters) > 0 :
        filter, filter_info = SPHERE_IRDIS(filters)
        wavelength_photo, spectrum_at_wavelength_photo = apply_filter(model_wavelengths,model_flux,filter,filter_info)
    else :
        wavelength_photo, spectrum_at_wavelength_photo = ([],[])
    wavelengths = np.array(list(wavelength_spectro)+list(wavelength_photo))
    spectrum = np.array(list(spectrum_at_wavelength_spectro)+list(spectrum_at_wavelength_photo))
    return wavelengths, spectrum

def spectro_filter_combinations():
    return [('YJ','H23'),
    ('YJH','K12'),
    ('YJ','BBH'),
    ('YJH','BKs'),
    ('','BBY'),
    ('','BBJ'),
    ('','Y23'),
    ('','J23')]
    
def find_spectro_filter_combination(observed_wavelengths) :
    wavelength_difference = []
    combinations = spectro_filter_combinations()
    for bands,filter in combinations :
        _, filter_info  = SPHERE_IRDIS(filter)
        wavelengths_IRDIS = filter_info['Wavelength'].iloc[0]
        if len(bands)>0 :
            wavelengths_IFS = SPHERE_IFS(bands)
        else :
            wavelengths_IFS = []
        IRDIS_found = all(item in np.array(observed_wavelengths).round(4) for item in np.array(wavelengths_IRDIS).round(4))
        IFS_found = all(item in np.array(observed_wavelengths).round(4) for item in np.array(wavelengths_IFS).round(4))
        if IRDIS_found & IFS_found :
            return (bands,filter)
    return ('','')
            
def generate_all_combinations(model_wavelengths,model_flux):
    wavelengths = []
    fluxes = []
    combinations = spectro_filter_combinations()
    for bands,filters in combinations :
        wavelength_combi, flux_combi = make_IFS_IRDIS_spectrum(model_wavelengths,model_flux,bands,filters)
        wavelengths.append(wavelength_combi)
        fluxes.append(flux_combi)
        
    return wavelengths, fluxes, combinations

def generate_erreurs(model_wavelengths,model_flux,bands,filters) :
    df_model = pd.DataFrame()
    df_model['wavelength'] = model_wavelengths
    df_model['flux'] = model_flux
    flux = []
    wavelength = []
    erreurs = []
    filter_found = False
    bands_found = False
    stored_spectrums = glob.glob('./../../data/spectra/spectra_list/*txt')
    for spectrum in stored_spectrums :
        observed_spectrum = load_spectrum(spectrum)
        observed_bands, observed_filters = find_spectro_filter_combination(np.array(observed_spectrum['wavelength']))
        
        if (observed_filters == filters) & (filter_found == False) :
            filter_found = True
            _, filter_info = SPHERE_IRDIS(observed_filters)
            flux += list(df_model[df_model['wavelength'].isin(filter_info['Wavelength'].iloc[0])]['flux'])
            wavelength += list(df_model[df_model['wavelength'].isin(filter_info['Wavelength'].iloc[0])]['wavelength'])
            observed_error = np.array(observed_spectrum[observed_spectrum['wavelength'].round(4).isin(filter_info['Wavelength'].iloc[0])]['error'])
            observed_error = observed_error/np.array(observed_spectrum[observed_spectrum['wavelength'].round(4).isin(filter_info['Wavelength'].iloc[0])]['flux'])
            erreurs += list(observed_error*np.array(df_model[df_model['wavelength'].isin(filter_info['Wavelength'].iloc[0])]['flux']))
            
        if (observed_bands == bands) & (bands_found == False):
            bands_found = True
            IFS_wavelengths = SPHERE_IFS(observed_bands)
            flux += list(df_model[df_model['wavelength'].isin(IFS_wavelengths)]['flux'])
            wavelength += list(df_model[df_model['wavelength'].isin(IFS_wavelengths)]['wavelength'])
            observed_error = np.array(observed_spectrum[observed_spectrum.round(4)['wavelength'].isin(IFS_wavelengths)]['error'])
            observed_error = observed_error/np.array(observed_spectrum[observed_spectrum.round(4)['wavelength'].isin(IFS_wavelengths)]['flux'])
            erreurs += list(observed_error*np.array(df_model[df_model['wavelength'].isin(IFS_wavelengths)]['flux']))
                
    
    if (filter_found == False) :
        flux += list(df_model[~df_model['wavelength'].isin(wavelength)]['flux'])
        erreurs += list(df_model[~df_model['wavelength'].isin(wavelength)]['flux']*0.1)
        wavelength += list(df_model[~df_model['wavelength'].isin(wavelength)]['wavelength'])
    
    df_model = pd.DataFrame()
    df_model['wavelength'] = wavelength
    df_model['flux'] = flux    
    df_model['error'] = erreurs
    df_model['combination'] = '{}_{}'.format(bands,filters)
    return df_model

def generate_synthetic_SPHERE_spectrum(output_folder,file_name,model_wavelengths,model_flux,bands,filters) :
    model_wavelengths,model_flux = make_IFS_IRDIS_spectrum(model_wavelengths,model_flux,bands,filters)
    model = generate_erreurs(model_wavelengths,model_flux,bands,filters)
    
    new_randomized_flux = []
    for wavelength, flux, error in zip(model['wavelength'],model['flux'],model['error']) :
        new_randomized_flux.append(random.gauss(flux, error))
        
    model['flux'] = new_randomized_flux
    model.columns = ['#wavelength','flux','error','combination']
    model.to_csv('{}/{}.txt'.format(output_folder,file_name), index=False)
    
    return model

def create_synthetic_SPHERE_spectrum_grid() :
    distance = 10
    output_folder = './../../data/spectra/synthetic_spectra_list'
    df = load_grid('./../../data/current_exoevol_grid/ker',EOS='ker',Mass=[1,5],Radius=None,Met=[-2,2],Core=[1,20],T_int=[100,10000],T_irr=[100,200],T_eff=None,equilibrium=False,clouds=True)
    df = df.sample(frac=0.1)
    SPHERE_combinations = spectro_filter_combinations()
    for ii in range(10) :
        for combination in SPHERE_combinations :
            model_wavelengths,model_flux = (wavenumber_to_wavelength(df['wavenumber_up'].iloc[ii]),convert_radiosity(df['radiosity_up'].iloc[ii],df['wavenumber_up'].iloc[ii]))
            model_flux = diluted_planet(model_flux, df['Radius_Jup'].iloc[ii], distance)
            file_name = 'synthetic_{}_{}_{}_{}_{}_{}_{}'.format(combination,df['Mass_Jup'].iloc[ii],df['T_int'].iloc[ii],df['T_irr'].iloc[ii],df['Met'].iloc[ii],df['core_Earth'].iloc[ii],df['f_sed'].iloc[ii])
            generate_synthetic_SPHERE_spectrum(output_folder,file_name,model_wavelengths,model_flux,combination[0],combination[1])
    return

def JWST_miri_range():
    filter = ['F1065C','F1140C','F1550C']
    wavelengths = [10.575,11.30,15.50]
    return wavelengths, filter

def JWST_miri(filter_name) :
    MIRI_filters = ['F1065C','F1140C','F1550C']
    MIRI_Central_Wavelength = [[10.575],[11.30],[15.50]]
    MIRI_wavelength_errors = [[0.75],[0.8],[0.9]]
    filter_info = pd.DataFrame()
    filter_info['filters'] = MIRI_filters
    filter_info['Wavelength'] = MIRI_Central_Wavelength
    filters_files = glob.glob('./../../data/filters/JWST/*.dat')
    filters_names = [filter.split('/')[-1].replace('JWST_MIRI._','').replace('.dat','') for filter in filters_files]
    filter = difflib.get_close_matches(filter_name, filters_names,n=1,cutoff=0)[0]
    df = load_filter(filters_files[filters_names.index(filter)])
    df['wavelength'] = df['wavelength']*1e-4
    df['filter'] = filter
    return df, filter_info[filter_info['filters']==difflib.get_close_matches(filter_name, filter_info['filters'],n=1,cutoff=0)[0]]

def JWST_photo(model_wavelengths,model_flux):
    MIRI_filters = JWST_miri_range()[1]
    wavelength_photo_MIRI = []
    spectrum_at_wavelength_photo_MIRI = []
    for MIRI_filter in MIRI_filters :
        filter, filter_info = JWST_miri(MIRI_filter)
        wavelength_photo_MIRI_temp, spectrum_at_wavelength_photo_MIRI_temp = apply_filter(model_wavelengths,model_flux,filter,filter_info)
        wavelength_photo_MIRI += wavelength_photo_MIRI_temp
        spectrum_at_wavelength_photo_MIRI += spectrum_at_wavelength_photo_MIRI_temp
        
    return wavelength_photo_MIRI, spectrum_at_wavelength_photo_MIRI

def keck_range():
    filter = ['Lp']
    wavelengths = [3.776]
    return wavelengths, filter

def keck_photo(model_wavelengths,model_flux):
    keck_wavelength = keck_range()[0]
    wavelength_keck = []
    spectrum_at_wavelength_photo_keck = []
    keck_filter = glob.glob('./../../data/filters/NIRC2/Lp.dat')
    filter_info = pd.DataFrame({'filters':'Lp','Wavelength':[keck_wavelength]})
    filter = load_filter('./../../data/filters/NIRC2/Lp.dat')
    keck_wavelength, spectrum_at_wavelength_photo_keck = apply_filter(model_wavelengths,model_flux,filter,filter_info)
        
    return keck_wavelength, spectrum_at_wavelength_photo_keck

def NaCo_range():
    filter = ['J','H','Ks','Lp','Mp']
    wavelengths = [1.265,1.66,2.18,3.80,4.78]
    return wavelengths, filter

def NaCo_photo(model_wavelengths,model_flux):
    NaCo_wavelength, NaCo_filters = NaCo_range()
    wavelength_NaCo = []
    spectrum_at_wavelength_photo_NaCo = []
    for filter, wavelength in zip(NaCo_filters,NaCo_wavelength) :
        filter_info = pd.DataFrame({'filters':filter,'Wavelength':[[wavelength]]})
        filter = load_filter('./../../data/filters/NaCo/{}.dat'.format(filter))
        NaCo_wavelength_filter, spectrum_at_wavelength_photo_NaCo_filter = apply_filter(model_wavelengths,model_flux,filter,filter_info)
        wavelength_NaCo += NaCo_wavelength_filter
        spectrum_at_wavelength_photo_NaCo += spectrum_at_wavelength_photo_NaCo_filter
        
    return wavelength_NaCo, spectrum_at_wavelength_photo_NaCo

def make_IFS_IRDIS_MIRI_spectrum(model_wavelengths,model_flux,bands,filters,use_MIRI=False):
    if len(bands) > 0 :
        wavelength_spectro, spectrum_at_wavelength_spectro = apply_spectro(model_wavelengths,model_flux,SPHERE_IFS(bands))
    else :
        wavelength_spectro, spectrum_at_wavelength_spectro = ([],[])
    if len(filters) > 0 :
        filter, filter_info = SPHERE_IRDIS(filters)
        wavelength_photo_IRDIS, spectrum_at_wavelength_photo_IRDIS = apply_filter(model_wavelengths,model_flux,filter,filter_info)
    else :
        wavelength_photo_IRDIS, spectrum_at_wavelength_photo_IRDIS = ([],[])
    if use_MIRI :
        wavelength_photo_MIRI, spectrum_at_wavelength_photo_MIRI = JWST_photo(model_wavelengths,model_flux)
    else :
        wavelength_photo_MIRI, spectrum_at_wavelength_photo_MIRI = ([],[])
    wavelengths = np.array(list(wavelength_spectro)+list(wavelength_photo_IRDIS)+list(wavelength_photo_MIRI))
    spectrum = np.array(list(spectrum_at_wavelength_spectro)+list(spectrum_at_wavelength_photo_IRDIS)+list(spectrum_at_wavelength_photo_MIRI))
    return wavelengths, spectrum

def get_n_instruments (observed_spectrum) :
    instruments = 0
    use_MIRI = False
    bands,filters = find_spectro_filter_combination(observed_spectrum['wavelength'])
    print(bands,filters)
    IFS, IRDIS, JWST, keck, NaCo = [], [], [], [], []
    if len(bands)>0 :
        instruments += 1
        IFS = list(SPHERE_IFS(bands))
        print('IFS detected')
    if len(filters)>0 :
        instruments += 1
        IRDIS = SPHERE_IRDIS(filters)[1]['Wavelength'].iloc[0]
        print('IRDIS detected')
    if  True in list(np.abs(observed_spectrum['wavelength']-JWST_miri_range()[0][0]).round(4)==0) :
        instruments += 1
        JWST = list(JWST_miri_range()[0])
        print('JWST detected')
    if  True in list(np.abs(observed_spectrum['wavelength']-keck_range()[0][0]).round(4)==0) :
        instruments += 1
        keck = list(keck_range()[0])
        print('keck detected')
    if  True in list(np.abs(observed_spectrum['wavelength']-NaCo_range()[0][0]).round(4)==0) :
        instruments += 1
        NaCo = list(NaCo_range()[0])
        print('NaCo detected')
    if len(IFS+IRDIS+JWST+keck+NaCo) < len(observed_spectrum['wavelength']) :
        instruments += 1
        print('GPI detected')
    print('{} Instruments detected'.format(instruments))
    if (instruments <= 2) :
        instruments = 0
    return instruments
""" 
def model_at_observation(observed_spectrum,model_wavelengths,model_flux,offsets) :
    model = pd.DataFrame()
    
    offsets = list(offsets)
    if len(offsets) == 0:
        offsets = [1,1,1,1,1,1,1]
    use_MIRI = False
    use_keck = False
    use_NaCo = False
    bands,filters = find_spectro_filter_combination(observed_spectrum['wavelength'])
    
    if  True in list(np.abs(observed_spectrum['wavelength']-JWST_miri_range()[0][0]).round(4)==0) :
        use_MIRI = True
        
    if  True in list(np.abs(observed_spectrum['wavelength']-keck_range()[0][0]).round(4)==0) :
        use_keck = True
        
    if  True in list(np.abs(observed_spectrum['wavelength']-NaCo_range()[0][0]).round(4)==0) :
        use_NaCo = True
    
    if len(bands) > 0 :
        wavelength_IFS, spectrum_IFS = apply_spectro(model_wavelengths,model_flux,SPHERE_IFS(bands))
        spectrum_IFS = list(np.array(spectrum_IFS)+(offsets[0]))
        offsets.pop(0)
        model = model.append(pd.DataFrame({'wavelength':wavelength_IFS,'model_flux':spectrum_IFS,'instrument':'IFS'}))
        
    if len(filters) > 0 :
        filter, filter_info = SPHERE_IRDIS(filters)
        wavelength_IRDIS, spectrum_IRDIS = apply_filter(model_wavelengths,model_flux,filter,filter_info)
        spectrum_IRDIS = list(np.array(spectrum_IRDIS)+(offsets[0]))
        offsets.pop(0)
        model = model.append(pd.DataFrame({'wavelength':wavelength_IRDIS,'model_flux':spectrum_IRDIS,'instrument':'IRDIS'}))
    
    if use_MIRI :
        wavelength_MIRI, spectrum_MIRI = JWST_photo(model_wavelengths,model_flux)
        spectrum_MIRI = list(np.array(spectrum_MIRI)+(offsets[0]))
        offsets.pop(0)
        model = model.append(pd.DataFrame({'wavelength':wavelength_MIRI,'model_flux':spectrum_MIRI,'instrument':'MIRI'}))
        
    if use_keck :
        wavelength_keck, spectrum_keck = keck_photo(model_wavelengths,model_flux)
        spectrum_keck = list(np.array(spectrum_keck)+(offsets[0]))
        offsets.pop(0)
        model = model.append(pd.DataFrame({'wavelength':wavelength_keck,'model_flux':spectrum_keck,'instrument':'keck'}))
        
    if use_NaCo :
        wavelength_NaCo, spectrum_NaCo = NaCo_photo(model_wavelengths,model_flux)
        spectrum_NaCo = list(np.array(spectrum_NaCo)+(offsets[0]))
        offsets.pop(0)
        model = model.append(pd.DataFrame({'wavelength':wavelength_NaCo,'model_flux':spectrum_NaCo,'instrument':'NaCo'}))
        
    if len(model['wavelength']) < len(observed_spectrum) :
        observed_spectrum = observed_spectrum[~observed_spectrum['wavelength'].round(4).isin(list(model['wavelength']))]
        wavelengths_other, spectrum_other = apply_spectro(model_wavelengths,model_flux,observed_spectrum['wavelength'])
        spectrum_other = list(np.array(spectrum_other)+(offsets[0]))
        model = model.append(pd.DataFrame({'wavelength':wavelengths_other,'model_flux':spectrum_other,'instrument':'GPI'}))
    
    return model """

def read_filter_docs(filter_loc='./../../data/filters/filter_doc.csv'):
    df_filter = pd.read_csv(filter_loc)
    df_filter[['instrument', 'filter']] = df_filter['name'].str.split(' ', n=1, expand=True)
    filter_path = []
    for inst, filter in zip(df_filter['instrument'],df_filter['filter']) :
        filter_path += glob.glob('{}*{}*/*{}*'.format(filter_loc.split('filter_doc')[0],inst, filter))
    df_filter['filter_path'] = filter_path
    return df_filter
    
def read_spectro_docs(spectro_loc='./../../data/filters/spectro_doc.csv'):
    df_spectro = pd.read_csv(spectro_loc)
    return df_spectro

def photo_columns(filter_loc='./../../data/filters/filter_doc.csv'):
    df_photo = read_filter_docs(filter_loc=filter_loc)
    filter_list = df_photo['name'].to_list()
    columns = []
    for ele in filter_list :
        if 'D_' not in ele :
            columns.append(ele)
        else :
            columns.append(ele+'_0')
            columns.append(ele+'_1')
    return columns
    
def identify_observation(observation) :
    #observation = load_spectrum('./../../data/spectra/spectra_list/Af_Lep_b_YJH_spectrum_flux.txt')
    df_filter = read_filter_docs()
    df_spectro = read_spectro_docs().apply(pd.to_numeric, errors='coerce')
    
    observation['instrument'] = np.nan
    observation['type'] = np.nan
    
    for spectro in df_spectro.columns :
        matches = pd.merge_asof(observation.sort_values('wavelength').apply(pd.to_numeric, errors='coerce'), df_spectro[[spectro]].dropna().sort_values(spectro), left_on='wavelength', right_on=spectro, direction='nearest', tolerance=1e-3)
        matches = matches[matches[spectro].notna()]
        if len(matches) >= len(df_spectro[[spectro]].dropna())-3:
            observation.loc[observation['wavelength'].isin(list(matches['wavelength'].dropna())),'instrument'] = spectro.split(' ')[0].split('_')[0]
            observation.loc[observation['wavelength'].isin(list(matches['wavelength'].dropna())),'type'] = 'spectro'
    observation_spectro = observation[observation['instrument'].notna()]
    
    observation = observation[~observation['wavelength'].isin(list(observation_spectro['wavelength']))]
    tol = 1e-3
    for wls_photo, filter, filter_path in zip(df_filter['wl'],df_filter['name'],df_filter['filter_path']) :
        wls = [float(wl) for wl in wls_photo.split('_')]
        for wl in wls :
            if len(observation[np.abs(observation['wavelength']-wl)<tol]) >= 1: 
                observation.loc[np.abs(observation['wavelength']-wl)<tol,'instrument'] = filter.split(' ')[0].split('_')[0]
                observation.loc[np.abs(observation['wavelength']-wl)<tol,'type'] = 'photo'
                observation.loc[np.abs(observation['wavelength']-wl)<tol,'path'] = filter_path
    observation_filter = observation[observation['instrument'].notna()]    
    
    #observation = observation_spectro.append(observation_filter).reset_index(drop=True) 
    observation = pd.concat([observation_spectro, observation_filter], ignore_index=True)
    return observation

def model_at_observation(observed_spectrum,model_wavelengths,model_flux_spectrum,model_flux_photo) :
    observed_spectro = observed_spectrum[observed_spectrum['type']=='spectro']
    wavelength_spectro, spectrum_at_wavelength_spectro = [], []
    if len(observed_spectro) > 0 :
        wavelength_spectro, spectrum_at_wavelength_spectro = apply_spectro(model_wavelengths,model_flux_spectrum,list(observed_spectro['wavelength']))
    observed_spectro['model_flux'] = spectrum_at_wavelength_spectro
    observed_photo = observed_spectrum[observed_spectrum['type']=='photo']
    df_filter = read_filter_docs()
    wls = [wl.split('_') for wl in df_filter['wl']]
    wls = list(itertools.chain.from_iterable(wls))
    wls = [float(wl) for wl in wls]
    temp = pd.DataFrame()
    temp['wls'] = wls
    temp['model_flux_photo'] = model_flux_photo
    if len(observed_photo) > 0 :
        for wl in observed_photo['wavelength'] :
            temp['diff wls'] = np.abs(temp['wls']-wl)
            closest_match_photo = temp.sort_values(by='diff wls',ascending=True)['model_flux_photo'].iloc[0]
            observed_photo.loc[observed_photo['wavelength']==wl,'model_flux'] = closest_match_photo 
    return pd.concat([observed_spectro, observed_photo], ignore_index=True)

def apply_offsets(observed_spectrum,offsets):
    instruments = np.sort(list(set(observed_spectrum['instrument'])))
    for instrument, offset in zip(instruments,offsets):
        print(instrument)
        observed_spectrum.loc[observed_spectrum['instrument']==instrument,'model_flux'] *= offset
    return observed_spectrum

def evaluate_all_filters(filters, model_wavelengths, model_flux) :
    df_photo = pd.DataFrame()
    for kk in range(0,len(filters)):
        wls = filters['wl'].iloc[kk].split('_')
        filter = load_filter(filters['filter_path'].iloc[kk])
        filter_wavelength = list(filter['wavelength'])
        for ii, wl in enumerate(wls) :
            filter_throughput = list(filter[filter.columns[ii+1]])
            df_photo[filters['name'].iloc[kk]+'_{}'.format(ii)] = [apply_filter(model_wavelengths,model_flux,filter_wavelength,filter_throughput)]
    return df_photo

def get_wl_all_filters(filters) :
    wls = []
    for kk in range(0,len(filters)):
        wls += [float(ele) for ele in filters['wl'].iloc[kk].split('_')]
    return wls
