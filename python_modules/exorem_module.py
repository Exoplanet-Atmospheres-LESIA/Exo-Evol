"""
Created on Fri Jun 16 11:25:25 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob
import numpy as np
import pandas as pd
import time

from scipy.interpolate import interp1d
from scipy.interpolate import InterpolatedUnivariateSpline

from itertools import groupby

import pickle as pkl
import f90nml
import h5py

import warnings
warnings.filterwarnings("ignore")

from python_modules.exoplanet_data_module import *
from python_modules.numerical_tools_module import *
from python_modules.physics_module import *
from python_modules.interface_module import *


""" Exorem is a 1D self-consistent radiative-equilibrium model for exoplanetary atmospheres
    available here https://gitlab.obspm.fr/Exoplanet-Atmospheres-LESIA/exorem 
    this module interfaces exorem with exoris so as to not have to manually use exorem
    it requires an exorem to be available in the folder : './exorem' """


def remove_exorem(file_prefix):
    os.system('rm -r ../temp/running_exorem/'+file_prefix)
 
def make_exorem(file_prefix,exorem_path):
    remove_exorem(file_prefix)
    os.system('cp -r {}/exorem ../temp/running_exorem/'.format(exorem_path)+file_prefix)

def get_T_irr(x) :
    return float(x.split('_')[-2])

def get_T_int(x) :
    return float(x.split('_')[-3])

def get_Met(x) :
    return float(x.split('_')[-1].replace('.h5',''))

def get_files() :
    df_files = pd.DataFrame()
    df_files['files'] = glob.glob('../exorem_hdf5/*.h5')
    df_files['Met'] = df_files['files'].apply(get_Met)
    df_files['T_irr'] = df_files['files'].apply(get_T_irr)
    df_files['T_int'] = df_files['files'].apply(get_T_int)

    return df_files

def get_at_P(y,Pressure,P) :
    y = np.array(y)
    Pressure = np.array(Pressure)
    if len(y) < len(Pressure) :
        Pressure = Pressure[:-1]
    len(y)
    len(Pressure)
    y_new = float(interp1d(Pressure,y,kind='linear',fill_value='extrapolate')(P))
    return np.abs(y_new)

def get_y(y, Pressure,P) :
    Pressure = np.array(Pressure)
    coef = np.diff(np.log(y),n=1)/np.diff(np.log(Pressure),n=1) # Sloap at linkage
    y = 1/(1-coef) # Adiabatic index
    if len(y) < len(Pressure) :
        Pressure = Pressure[:-1]
    y = float(interp1d(Pressure,y,kind='linear',fill_value='extrapolate')(P))
    return y

def get_exorem_at_P_link(exorem, P_link_min, pressure_link=0, pressure_link_gravity=0) :    
    P_link = []
    dR = []
    mu = []
    g = []
    g_s = []
    T = []
    y = []
    warning_flag = False

    for ii in range(0,len(exorem)) :
        if pressure_link == 0 :
            convective_zone = smooth_convective_zone(exorem['/outputs/levels/is_convective'].iloc[ii])
            #convective_zone = np.array(exorem['/outputs/levels/is_convective'].iloc[ii])
            P_convective = 1e-5*np.array(exorem['/outputs/levels/pressure'].iloc[ii])*np.array(convective_zone)
            print(convective_zone)
            print(P_convective)
            if (1 in convective_zone) :
                sub_convective_zones = [list(g) for k, g in groupby(list(P_convective), lambda x:x>0)]
                print(sub_convective_zones)
                sub_convective_zones = [ls for ls in sub_convective_zones if ((np.nan or 0) not in ls)]
                print(sub_convective_zones)
                last_sub_convective_zone = sub_convective_zones[0]
                print(last_sub_convective_zone)
                if len(last_sub_convective_zone) > 1 :
                    pressure_link = last_sub_convective_zone[-2]
                else :
                    pressure_link = last_sub_convective_zone[-1]
            else :
                pressure_link = 1e-5*np.max(exorem['/outputs/levels/pressure'].iloc[ii])
                warning_flag = True
                
            print('\nFound pressure link is {:.5f} bars\n'.format(pressure_link))
            #pressure_link = get_closest_link_p(pressure_link) # We use actual Pressure link in this version

            if (pressure_link>1e-5*np.max(exorem['/outputs/levels/pressure'].iloc[ii])) or np.isnan(pressure_link):
                pressure_link = 1e-5*np.max(exorem['/outputs/levels/pressure'].iloc[ii])
                warning_flag = True
                print('\nPressure link above range pressure range or not a number, setting it at maximum available\n')
                
            if (pressure_link < P_link_min) :
                pressure_link = P_link_min

        if not np.isnan(pressure_link) :
            dR.append(get_at_P(exorem['/outputs/levels/altitude'].iloc[ii],np.array(exorem['/outputs/levels/pressure'].iloc[ii])*1e-5,pressure_link))
            mu.append(get_at_P(np.array(exorem['/outputs/layers/molar_mass'].iloc[ii])*1e3,np.array(exorem['/outputs/levels/pressure'].iloc[ii])[:-1]*1e-5,pressure_link)) #in g/mol
            
            if pressure_link_gravity == 0 :
                g.append(get_at_P(exorem['/outputs/layers/gravity'].iloc[ii],np.array(exorem['/outputs/levels/pressure'].iloc[ii])[:-1]*1e-5,pressure_link))
            else: 
                g.append(get_at_P(exorem['/outputs/layers/gravity'].iloc[ii],np.array(exorem['/outputs/levels/pressure'].iloc[ii])[:-1]*1e-5,pressure_link_gravity))     
                           
            T.append(get_at_P(exorem['/outputs/levels/temperature'].iloc[ii],np.array(exorem['/outputs/levels/pressure'].iloc[ii])*1e-5,pressure_link))
            y.append(get_y(exorem['/outputs/levels/temperature'].iloc[ii],np.array(exorem['/outputs/levels/pressure'].iloc[ii])*1e-5,pressure_link))
            P_link.append(pressure_link) # in bars
        else :
            print('\nErreur in thermo at P\n')
            dR.append(np.nan)
            mu.append(np.nan)
            g.append(np.nan)
            T.append(np.nan)
            y.append(np.nan)
            P_link.append(np.nan)
                    
        g_s.append(get_at_P(exorem['/outputs/layers/gravity'].iloc[ii],np.array(exorem['/outputs/levels/pressure'].iloc[ii])[:-1]*1e-5,1))

    exorem['dR'] = dR
    exorem['mu_at_P'] = mu
    exorem['g_at_P'] = g
    exorem['T_at_P'] = T
    exorem['y_at_P'] = y
    exorem['P_link'] = P_link
    exorem['g_s'] = g_s
    rho = (exorem['mu_at_P']*1e-3*exorem['P_link']*1e5)/(8.314*exorem['T_at_P']) # In kg/mol
    exorem['rho_at_P'] = rho
    
    return exorem, warning_flag
                                            
def get_g_surface(exorem):
    g_s = []
    for ii in range(0,len(exorem)):
        g_s.append(float(interp1d(np.array(exorem['pressure'].iloc[ii])[:-1]*1e-5,np.array(exorem['gravity'].iloc[ii]),kind='linear')(1)))

    exorem['g_s'] = g_s
    return exorem

def load_init(file):
    data = np.loadtxt(file, skiprows = 2)
    return data 

def save_profile(file, data):
    
    header = """pressure temperature temparature_adiabatic temperature_uncertainty temperature_uncertainty_b delta_temperature_convection radiosity radiosity_convective is_convective grada altitude
Pa K K K K K W.m-2 W.m-2 None None m m2.s-1 None None m None"""
    np.savetxt(file, data, header = header)

def load_reference_to_dat(file_prefix) :
    ref_file = '../temp/running_exorem/'+file_prefix+'/inputs/atmospheres/temperature_profiles/temperature_profile_example_ref.dat'
    ref_data = load_init(ref_file)
    T_P = (np.flip(ref_data[:,1]),np.flip(ref_data[:,0]))
    return T_P

def load_T_P_to_dat(T_P,file_prefix) :
    temperature, pressure = T_P
    ref_file = '../temp/running_exorem/'+file_prefix+'/inputs/atmospheres/temperature_profiles/temperature_profile_example_ref.dat'
    ref_data = load_init(ref_file)
    ref_data[:, 1] = interp1d(pressure,temperature,kind='linear',fill_value="extrapolate")(np.logspace(-2,9,len(ref_data[:, 0])))
    ref_data[:, 0] = np.logspace(-1,9,len(ref_data[:, 0]))
    file_name = '../temp/running_exorem/'+file_prefix+'/inputs/atmospheres/temperature_profiles/example_to_use.dat'
    save_profile(file_name, ref_data)

    return 'example_to_use.dat'

def smooth_convective_zone(convective):
    # Smooth convective zone by replacing sublists with less than 3 elements with continous convective zone
    sub_lists = [list(g) for k, g in groupby(list(convective), lambda x:x>0)]
    
    # Modify the sublists according to the conditions
    for i in range(len(sub_lists)):
        if len(sub_lists[i]) <= 3:
            sub_lists[i] = list(np.ones(len(sub_lists[i])))
        
    # If it's the last sublist and its length is less than 5, change it to ones
    if (i == 0) and (len(sub_lists[i]) <= 5):
        sub_lists[0] = list(np.ones(len(sub_lists[0])))
        
    convective = [j for i in sub_lists for j in i]
    convective = [np.nan if x == 0 else x for x in convective]
    return convective

def find_points(exorem,  g) :  
    exorem = get_g_surface(exorem)
    exorem = exorem[exorem['radius_1e5Pa']>0]
    print(exorem)

    if len(exorem)>3:
        try :
            exorem = exorem.drop_duplicates('g_at_P',keep='last')
            exorem = exorem.sort_values('g_at_P')
            f_g = InterpolatedUnivariateSpline(exorem['g_at_P'],exorem['g_s'])
            g_surface = float(f_g(g))
            print("G found by interpolation {} m.s-2".format(g_surface))
            return g_surface
        except :
            print('Failed g interpolation')
            return g

    else :
        print('\nToo few exorem data points\n')
        return g
    
def derive_at_point(x,y,point,order) :
    if x[0]>x[-1] :
        x = x[::-1]
        y = y[::-1]
    for ii in range(0,order) :
        dy = np.diff(y)/np.diff(x)
        dy_at_point = interp1d(x[1:],dy)(point)
        x= x[1:]
        y = dy
    return dy_at_point


def run_exorem(file_prefix, closest_model, Met, f_sed, M, T_int, T_irr, kzz, iteration, g_erreurs, exorem_data, min_P_link, n_iterations, T_P = ()) :
    Met = float(Met)
    M = float(M)
    T_int = int(T_int)
    T_irr = int(T_irr)
    iteration = int(iteration)
    
    if f_sed > 10 :
        f_sed = -1
    
    if len(closest_model) > 0 : 
        P_link = closest_model['Ps'].iloc[-1]
        g_ris = closest_model['gs'].iloc[-1]
        R = closest_model['Req'].iloc[-1]*10**(-2)
    else :
        P_link = 1 
        g_ris = 10
        R = 1*R_J
    print('M, g, R, T_int, T_irr, prefix')
    print(M, g_ris, R, T_int, T_irr, file_prefix)

    print('In Exorem computation\n')
    P = 1e8
    
    g = float(g_ris)
    
    if len(closest_model) > 0:
        if ('gs' in closest_model.columns) and ('pressure_link_gravity' in closest_model.columns)  :
            g = float(interp1d(1e-5*np.array(closest_model['/outputs/layers/pressure'].iloc[0]),np.array(closest_model['/outputs/layers/gravity'].iloc[0]))(1))
            pressure_link_gravity = float(closest_model['pressure_link_gravity'].iloc[-1])
        else :
            pressure_link_gravity = float(P_link)
    else :
        g = 10
        pressure_link_gravity = 1
    
    print("g erreur")
    print(g_erreurs)
    if len(g_erreurs) > 1 :
        g = float(g + g*(g_erreurs[-1]/100))
        
    print('\n')
    print('Mass : {:.2e}'.format(M))
    print('T_int : {:.4f}'.format(T_int))
    print('T_irr : {:.2f}'.format(T_irr))
    print('Met : {:.2f}'.format(Met))
    print('f_sed : {:.2f}'.format(f_sed))
    print('kzz : {:.2f}'.format(kzz))
    print('g_ris : {:.2f}'.format(g_ris))
    print('g_used : {:.2f}'.format(g))
    print('P : {:.2f}'.format(P_link))
    if len(closest_model) > 0 : 
        print('core (from interior) : {:.2f}'.format(closest_model['core'].iloc[-1]))
    else :
        print('No grid values')
    print('\n')

    weight_apriori = 1e-3*(10**iteration)
    if weight_apriori > 1 :
        weight_apriori = 1
    retrieval_flux_error = 1e-2
    
    if (len (T_P) == 0) & (len(closest_model)>0):
        T_P = (closest_model['/outputs/layers/temperature'].iloc[0],closest_model['/outputs/layers/pressure'].iloc[0])
        profile = load_T_P_to_dat(T_P,file_prefix)
        print('\nUsing profile - {}\n'.format(profile))
    else :
        profile = load_T_P_to_dat(T_P,file_prefix)
        print('\nUsing inputted T_P\n')
    
    input_file = f90nml.read('../temp/running_exorem/'+file_prefix + '/inputs/example.nml')
    sigma = 5.67*1e-8
    
    file_name = '{}_{}_{}_{}_{}'.format(g,T_int,T_irr,Met,f_sed)
    
    input_file['output_files']['output_files_suffix'] =  file_name
    input_file['target_parameters']['use_gravity'] = True
    input_file['target_parameters']['target_equatorial_gravity'] = g
    input_file['target_parameters']['target_internal_temperature'] = T_int
    input_file['target_parameters']['target_equatorial_radius'] = R
    
    input_file['species_parameters']['use_atmospheric_metallicity'] = False
    input_file['species_parameters']['use_elements_metallicity'] = True
    input_file['species_parameters']['elements_names'] = ['He', 'Ne', 'Ar', 'Kr', 'Xe']
    input_file['species_parameters']['elements_metallicity'] = [1.0, 1.0, 1.0, 1.0, 1.0]
    input_file['species_parameters']['species_names'] = ['CH4', 'CO', 'CO2', 'FeH', 'H2O', 'H2S', 'HCN', 'K', 
                                                        'Na', 'NH3', 'PH3', 'TiO', 'VO']
    input_file['species_parameters']['species_at_equilibrium'] = [False, False, False, False, False, 
                                                                False, False, False, False, False, 
                                                                False, False, False]

    input_file['retrieval_parameters']['temperature_profile_file'] = profile
    #input_file['retrieval_parameters']['temperature_profile_file'] = 'temperature_profile_example_ref.dat'
    input_file['retrieval_parameters']['retrieval_level_bottom'] = 2
    input_file['retrieval_parameters']['retrieval_level_top'] = 71
    input_file['retrieval_parameters']['retrieval_flux_error_bottom'] = retrieval_flux_error
    input_file['retrieval_parameters']['retrieval_flux_error_top'] = retrieval_flux_error
    input_file['retrieval_parameters']['n_iterations'] = n_iterations
    input_file['retrieval_parameters']['n_non_adiabatic_iterations'] = 1
    input_file['retrieval_parameters']['chemistry_iteration_interval'] = 2
    input_file['retrieval_parameters']['cloud_iteration_interval'] = 4
    input_file['retrieval_parameters']['n_burn_iterations'] = n_iterations-1-20
    input_file['retrieval_parameters']['smoothing_bottom'] = 0.5
    input_file['retrieval_parameters']['smoothing_top'] = 0.5
    input_file['retrieval_parameters']['weight_apriori'] = weight_apriori
    
    input_file['spectrum_parameters']['wavenumber_min'] = 100
    input_file['spectrum_parameters']['wavenumber_max'] = 30000
    input_file['spectrum_parameters']['wavenumber_step'] = 20 
    
    input_file['atmosphere_parameters']['metallicity'] = Met
    input_file['atmosphere_parameters']['use_metallicity'] = True
    input_file['atmosphere_parameters']['eddy_mode'] = 'constant'
    # Weak :
    #input_file['atmosphere_parameters']['eddy_diffusion_coefficient'] = 10**(-2*np.log10(g)+13)
    # Strong :
    #input_file['atmosphere_parameters']['eddy_diffusion_coefficient'] = 10**(-2*np.log10(g)+15)
    # fixed :
    input_file['atmosphere_parameters']['eddy_diffusion_coefficient'] = kzz

    input_file['options']['output_species_spectral_contributions'] = True
    input_file['options']['output_cia_spectral_contribution'] = True
    input_file['options']['output_thermal_spectral_contribution'] = True
    input_file['options']['output_hdf5'] = True
    input_file['options']['output_full'] = True
    input_file['options']['output_transmission_spectra'] = True
    
    if f_sed > 0 :
        
        input_file['atmosphere_parameters']['n_clouds'] = 5
        input_file['clouds_parameters']['cloud_mode'] = 'fixedSedimentation'
        input_file['clouds_parameters']['cloud_fraction'] = 1.0
        input_file['clouds_parameters']['cloud_names'] = ['Fe','Mg2SiO4','KCl', 'Na2S','H2O']
        input_file['clouds_parameters']['cloud_particle_radius'] = [5e-05, 5e-05, 5e-05, 5e-06,5e-05]
        input_file['clouds_parameters']['sedimentation_parameter'] = [f_sed, f_sed, f_sed,f_sed,f_sed]
        input_file['clouds_parameters']['supersaturation_parameter'] = [0.1, 0.1, 0.1, 0.1, 0.1]
        input_file['clouds_parameters']['sticking_efficiency'] = [1.0, 1.0, 1.0, 1.0, 1.0]
        input_file['clouds_parameters']['cloud_particle_density'] = [7874.0, 3670, 2000.0, 1860.0, 917.0]
        input_file['clouds_parameters']['reference_wavenumber'] = [10000.0, 10000.0, 10000.0, 10000.0, 10000.0]
        input_file['clouds_parameters']['load_cloud_profiles'] = False

        '''
        input_file['atmosphere_parameters']['n_clouds'] = 2
        input_file['clouds_parameters']['cloud_mode'] = 'fixedSedimentation'
        input_file['clouds_parameters']['cloud_fraction'] = 1.0
        input_file['clouds_parameters']['cloud_names'] = ['Fe','Mg2SiO4']
        input_file['clouds_parameters']['cloud_particle_radius'] = [5e-05, 5e-05]
        input_file['clouds_parameters']['sedimentation_parameter'] = [f_sed, f_sed]
        input_file['clouds_parameters']['supersaturation_parameter'] = [0.1, 0.1]
        input_file['clouds_parameters']['sticking_efficiency'] = [1.0, 1.0]
        input_file['clouds_parameters']['cloud_particle_density'] = [7874.0, 3670]
        input_file['clouds_parameters']['reference_wavenumber'] = [10000.0, 10000.0]

        
        input_file['atmosphere_parameters']['n_clouds'] = 1
        input_file['clouds_parameters']['cloud_mode'] = 'fixedSedimentation'
        input_file['clouds_parameters']['cloud_fraction'] = 1.0
        input_file['clouds_parameters']['cloud_names'] = ['H2O']
        input_file['clouds_parameters']['cloud_particle_radius'] = [5e-05]
        input_file['clouds_parameters']['sedimentation_parameter'] = [f_sed]
        input_file['clouds_parameters']['supersaturation_parameter'] = [0.1]
        input_file['clouds_parameters']['sticking_efficiency'] = [1.0]
        input_file['clouds_parameters']['cloud_particle_density'] = [917.0]
        input_file['clouds_parameters']['reference_wavenumber'] = [10000.0]
        '''

    else :
        input_file['atmosphere_parameters']['n_clouds'] = 0
        
    input_file['paths']['path_data'] = exorem_data + '/'
    input_file['paths']['path_cia'] =  exorem_data + '/cia/'
    input_file['paths']['path_clouds'] =  exorem_data + '/cloud_optical_constants/'
    #input_file['paths']['path_k_coefficients'] =  exorem_data + '/k_coefficients_tables/R500/'
    input_file['paths']['path_k_coefficients'] =  exorem_data + '/k_coefficients_tables/R500twa7b/' # To change ALICE RADCLIFFE
    input_file['paths']['path_temperature_profile'] = '../temp/running_exorem/'+file_prefix+'/inputs/atmospheres/temperature_profiles/'
    input_file['paths']['path_thermochemical_tables'] =  exorem_data + '/thermochemical_tables/'
    input_file['paths']['path_light_source_spectra'] =  exorem_data + '/stellar_spectra/'
    input_file['paths']['path_outputs'] = '../temp/running_exorem/'+ file_prefix + '/outputs/exorem/'
    
    if T_irr == 0 :
        input_file['light_source_parameters']['add_light_source'] = False
        input_file['light_source_parameters']['use_irradiation'] = False
        input_file['light_source_parameters']['use_light_source_spectrum'] = False
        input_file['light_source_parameters']['light_source_irradiation'] = 0

    else :
        input_file['light_source_parameters']['add_light_source'] = True
        input_file['light_source_parameters']['use_irradiation'] = True
        input_file['light_source_parameters']['use_light_source_spectrum'] = False
        input_file['light_source_parameters']['light_source_radius'] = 774263682.6811
        input_file['light_source_parameters']['light_source_range'] = 77426368268112.78
        input_file['light_source_parameters']['light_source_effective_temperature'] = 3555.5556
        input_file['light_source_parameters']['light_source_irradiation'] = 4*sigma*T_irr**4

    input_file['light_source_parameters']['light_source_spectrum_file'] = 'spectrum_BTSettl_3500K_logg5_met0.dat'
    input_file['light_source_parameters']['incidence_angle'] = 0.0

    input_file['atmosphere_parameters']['pressure_min'] = 1
    input_file['atmosphere_parameters']['pressure_max'] = 10000*1e5
    #input_file['atmosphere_parameters']['pressure_max'] = 100*1e5
        
    if len(glob.glob('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')) : 
        os.remove('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
        
    print(type(g))
    print('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
    input_file.write('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')

    os.system('../temp/running_exorem/'+file_prefix+'/bin/exorem.exe ../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
    os.remove('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
    print('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')
    
    hdf5_file = glob.glob('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')

    if (len(hdf5_file) == 0): 
        print('\nExorem output file NOT generated and NOT found\n')
        df = pd.DataFrame()
        
    else :
        print('\nExorem output file generated and found\n')

        hdf5_file = glob.glob('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')

        df, keys = allkeys(pd.DataFrame(), hdf5_file[0])
        df['T_irr'] = T_irr
        df['T_int'] = T_int
        df['Met'] = Met

        new_smoothed_convective = smooth_convective_zone(df['/outputs/levels/is_convective'].iloc[-1])
        try :
            T_eff = get_Teff(df['/outputs/spectra/emission/spectral_radiosity'].iloc[-1], df['/outputs/spectra/wavenumber'].iloc[-1])
        except :
            T_eff = (np.nan)

        df['T_eff'] = [T_eff]
        df['f_sed'] = [f_sed]
        df['kzz'] = [kzz]
        df['/outputs/levels/is_convective_smoothed'] = [new_smoothed_convective]
        df, warning_flag_pressure = get_exorem_at_P_link(df, min_P_link, pressure_link_gravity=pressure_link_gravity)
        df['warning_flag_max_pressure'] = warning_flag_pressure
        temp_plot(df,file_prefix,T_P)
        os.remove('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')

    return df

def T_eff(file,T_irr) :
    sigma = 5.67*1e-8
    data = load_dat(file)
    del data['units']
    data = pd.DataFrame(data)
    data = data[data['spectral_flux']>0]
    return ((((data['wavenumber'].diff()*data['spectral_flux']).sum(skipna=True))/sigma)+T_irr**4)**(1/4)

def load_dat(file, **kwargs):
    """
    Load an Exo-REM data file.
    :param file: data file
    :param kwargs: keyword arguments for loadtxt
    :return: the data
    """
    with open(file, 'r') as f:
        header = f.readline()
        unit_line = f.readline()

    header_keys = header.rsplit('!')[0].split('#')[-1].split()
    units = unit_line.split('#')[-1].split()

    data = np.loadtxt(file, **kwargs)
    data_dict = {}

    for i, key in enumerate(header_keys):
        data_dict[key] = data[:, i]

    data_dict['units'] = units

    return data_dict    
    
def temp_plot(df,file_prefix,T_P=()) :
    quality = quality_check(df)
    plt.figure()
    plt.plot(df['/outputs/levels/temperature'].iloc[0],np.array(df['/outputs/levels/pressure'].iloc[0])*1e-5)
    plt.plot(df['/outputs/levels/temperature'].iloc[0],np.array(df['/outputs/levels/is_convective_smoothed'].iloc[0])*np.array(df['/outputs/levels/pressure'].iloc[0])*1e-5,linewidth=12,alpha=0.5)
    if len(T_P) > 0 :
        print(T_P[0],T_P[1])
        plt.plot(T_P[0],np.array(T_P[1])*1e-5,'--',linewidth=2)
    plt.gca().invert_yaxis()
    plt.title('{}_{}'.format(file_prefix,quality))
    
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel('Temperature (K)')
    plt.ylabel('Pressure (bar)')
    plt.savefig('./../temp/plots/{}.pdf'.format(file_prefix))
    plt.close()

def quality_check(df) : 
    T = df['/outputs/layers/temperature'].iloc[0]
    P = df['/outputs/layers/pressure'].iloc[0]
    quality = 1
    if np.max(T) != T[0]:
        quality = 0
    
    return quality


############################################################################################################

def run_exorem_again(model,exorem_data,file_prefix) :
    
    Met = model['Met'].iloc[0]
    M = model['mass'].iloc[0]*1e2
    T_int = model['T_int'].iloc[0]
    T_irr = model['T_irr'].iloc[0]
    kzz = model['kzz'].iloc[0]
    f_sed = model['f_sed'].iloc[0]
    
    if f_sed > 10 :
        f_sed = -1
    
    P_link = model['P_link'].iloc[0]
    g = model['gs'].iloc[-1]
    R = model['Req'].iloc[-1]*10**(-2)
    pressure_link_gravity = float(model['pressure_link_gravity'].iloc[-1])
    n_iterations = 1
    
    print('In Exorem computation\n')
    P = 1e8
    g = float(interp1d(1e-5*np.array(model['/outputs/layers/pressure'].iloc[0]),np.array(model['/outputs/layers/gravity'].iloc[0]))(1))
        
    print('\n')
    print('Mass : {:.2e}'.format(M))
    print('T_int : {:.4f}'.format(T_int))
    print('T_irr : {:.2f}'.format(T_irr))
    print('Met : {:.2f}'.format(Met))
    print('f_sed : {:.2f}'.format(f_sed))
    print('kzz : {:.2f}'.format(kzz))
    print('g_ris : {:.2f}'.format(g))
    print('g_used : {:.2f}'.format(g))
    print('P : {:.2f}'.format(P_link))
    print('\n')

    weight_apriori = 1e-10
    retrieval_flux_error = 1e-2
    min_P_link = 1e-2
    
    T_P = (model['/outputs/layers/temperature'].iloc[0],model['/outputs/layers/pressure'].iloc[0])
    profile = load_T_P_to_dat(T_P,file_prefix)
    print('\nUsing profile - {}\n'.format(profile))
    
    input_file = f90nml.read('../temp/running_exorem/'+file_prefix + '/inputs/example.nml')
    sigma = 5.67*1e-8
    
    file_name = '{}_{}_{}_{}_{}'.format(g,T_int,T_irr,Met,f_sed)
    
    input_file['output_files']['output_files_suffix'] =  file_name
    input_file['target_parameters']['use_gravity'] = True
    input_file['target_parameters']['target_equatorial_gravity'] = g
    input_file['target_parameters']['target_internal_temperature'] = T_int
    input_file['target_parameters']['target_equatorial_radius'] = R
    
    input_file['species_parameters']['use_atmospheric_metallicity'] = False
    input_file['species_parameters']['use_elements_metallicity'] = True
    input_file['species_parameters']['elements_names'] = ['He', 'Ne', 'Ar', 'Kr', 'Xe']
    input_file['species_parameters']['elements_metallicity'] = [1.0, 1.0, 1.0, 1.0, 1.0]
    input_file['species_parameters']['species_names'] = ['CH4', 'CO', 'CO2', 'FeH', 'H2O', 'H2S', 'HCN', 'K', 
                                                        'Na', 'NH3', 'PH3', 'TiO', 'VO']
    input_file['species_parameters']['species_at_equilibrium'] = [False, False, False, False, False, 
                                                                False, False, False, False, False, 
                                                                False, False, False]

    input_file['retrieval_parameters']['temperature_profile_file'] = profile
    #input_file['retrieval_parameters']['temperature_profile_file'] = 'temperature_profile_example_ref.dat'
    input_file['retrieval_parameters']['retrieval_level_bottom'] = 2
    input_file['retrieval_parameters']['retrieval_level_top'] = 80
    input_file['retrieval_parameters']['retrieval_flux_error_bottom'] = retrieval_flux_error
    input_file['retrieval_parameters']['retrieval_flux_error_top'] = retrieval_flux_error
    input_file['retrieval_parameters']['n_iterations'] = n_iterations
    input_file['retrieval_parameters']['n_non_adiabatic_iterations'] = 1
    input_file['retrieval_parameters']['chemistry_iteration_interval'] = 1
    input_file['retrieval_parameters']['cloud_iteration_interval'] = 1
    input_file['retrieval_parameters']['n_burn_iterations'] = 10
    input_file['retrieval_parameters']['smoothing_bottom'] = 0.5
    input_file['retrieval_parameters']['smoothing_top'] = 0.5
    input_file['retrieval_parameters']['weight_apriori'] = weight_apriori
    
    input_file['spectrum_parameters']['wavenumber_min'] = 100
    input_file['spectrum_parameters']['wavenumber_max'] = 30000
    input_file['spectrum_parameters']['wavenumber_step'] = 20 
    
    input_file['atmosphere_parameters']['metallicity'] = Met
    input_file['atmosphere_parameters']['use_metallicity'] = True
    input_file['atmosphere_parameters']['eddy_mode'] = 'constant'
    input_file['atmosphere_parameters']['eddy_diffusion_coefficient'] = kzz

    input_file['options']['output_species_spectral_contributions'] = True
    input_file['options']['output_cia_spectral_contribution'] = True
    input_file['options']['output_thermal_spectral_contribution'] = True
    input_file['options']['output_hdf5'] = True
    input_file['options']['output_full'] = True
    input_file['options']['output_transmission_spectra'] = True
    
    if f_sed > 0 :
        
        input_file['atmosphere_parameters']['n_clouds'] = 5
        input_file['clouds_parameters']['cloud_mode'] = 'fixedSedimentation'
        input_file['clouds_parameters']['cloud_fraction'] = 1.0
        input_file['clouds_parameters']['cloud_names'] = ['Fe','Mg2SiO4','KCl', 'Na2S','H2O']
        input_file['clouds_parameters']['cloud_particle_radius'] = [5e-05, 5e-05, 5e-05, 5e-06,5e-05]
        input_file['clouds_parameters']['sedimentation_parameter'] = [f_sed, f_sed, f_sed,f_sed,f_sed]
        input_file['clouds_parameters']['supersaturation_parameter'] = [0.1, 0.1, 0.1, 0.1, 0.1]
        input_file['clouds_parameters']['sticking_efficiency'] = [1.0, 1.0, 1.0, 1.0, 1.0]
        input_file['clouds_parameters']['cloud_particle_density'] = [7874.0, 3670, 2000.0, 1860.0, 917.0]
        input_file['clouds_parameters']['reference_wavenumber'] = [10000.0, 10000.0, 10000.0, 10000.0, 10000.0]
        input_file['clouds_parameters']['load_cloud_profiles'] = False

        '''
        input_file['atmosphere_parameters']['n_clouds'] = 2
        input_file['clouds_parameters']['cloud_mode'] = 'fixedSedimentation'
        input_file['clouds_parameters']['cloud_fraction'] = 1.0
        input_file['clouds_parameters']['cloud_names'] = ['Fe','Mg2SiO4']
        input_file['clouds_parameters']['cloud_particle_radius'] = [5e-05, 5e-05]
        input_file['clouds_parameters']['sedimentation_parameter'] = [f_sed, f_sed]
        input_file['clouds_parameters']['supersaturation_parameter'] = [0.1, 0.1]
        input_file['clouds_parameters']['sticking_efficiency'] = [1.0, 1.0]
        input_file['clouds_parameters']['cloud_particle_density'] = [7874.0, 3670]
        input_file['clouds_parameters']['reference_wavenumber'] = [10000.0, 10000.0]

        
        input_file['atmosphere_parameters']['n_clouds'] = 1
        input_file['clouds_parameters']['cloud_mode'] = 'fixedSedimentation'
        input_file['clouds_parameters']['cloud_fraction'] = 1.0
        input_file['clouds_parameters']['cloud_names'] = ['H2O']
        input_file['clouds_parameters']['cloud_particle_radius'] = [5e-05]
        input_file['clouds_parameters']['sedimentation_parameter'] = [f_sed]
        input_file['clouds_parameters']['supersaturation_parameter'] = [0.1]
        input_file['clouds_parameters']['sticking_efficiency'] = [1.0]
        input_file['clouds_parameters']['cloud_particle_density'] = [917.0]
        input_file['clouds_parameters']['reference_wavenumber'] = [10000.0]
        '''

    else :
        input_file['atmosphere_parameters']['n_clouds'] = 0
        
    input_file['paths']['path_data'] = exorem_data + '/'
    input_file['paths']['path_cia'] =  exorem_data + '/cia/'
    input_file['paths']['path_clouds'] =  exorem_data + '/cloud_optical_constants/'
    #input_file['paths']['path_k_coefficients'] =  exorem_data + '/k_coefficients_tables/R500/'
    input_file['paths']['path_k_coefficients'] =  exorem_data + '/k_coefficients_tables/R500twa7b/' # To change ALICE RADCLIFFE
    input_file['paths']['path_temperature_profile'] = '../temp/running_exorem/'+file_prefix+'/inputs/atmospheres/temperature_profiles/'
    input_file['paths']['path_thermochemical_tables'] =  exorem_data + '/thermochemical_tables/'
    input_file['paths']['path_light_source_spectra'] =  exorem_data + '/stellar_spectra/'
    input_file['paths']['path_outputs'] = '../temp/running_exorem/'+ file_prefix + '/outputs/exorem/'
    
    if T_irr == 0 :
        input_file['light_source_parameters']['add_light_source'] = False
        input_file['light_source_parameters']['use_irradiation'] = False
        input_file['light_source_parameters']['use_light_source_spectrum'] = False
        input_file['light_source_parameters']['light_source_irradiation'] = 0

    else :
        input_file['light_source_parameters']['add_light_source'] = True
        input_file['light_source_parameters']['use_irradiation'] = True
        input_file['light_source_parameters']['use_light_source_spectrum'] = False
        input_file['light_source_parameters']['light_source_radius'] = 774263682.6811
        input_file['light_source_parameters']['light_source_range'] = 77426368268112.78
        input_file['light_source_parameters']['light_source_effective_temperature'] = 3555.5556
        input_file['light_source_parameters']['light_source_irradiation'] = 4*sigma*T_irr**4

    input_file['light_source_parameters']['light_source_spectrum_file'] = 'spectrum_BTSettl_3500K_logg5_met0.dat'
    input_file['light_source_parameters']['incidence_angle'] = 0.0

    input_file['atmosphere_parameters']['pressure_min'] = 1e-2
    input_file['atmosphere_parameters']['pressure_max'] = 10000*1e5
    input_file['atmosphere_parameters']['n_levels'] = 80
    #input_file['atmosphere_parameters']['pressure_max'] = 100*1e5
        
    if len(glob.glob('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')) : 
        os.remove('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
        
    print(type(g))
    print('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
    input_file.write('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')

    os.system('../temp/running_exorem/'+file_prefix+'/bin/exorem.exe ../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
    os.remove('../temp/running_exorem/'+file_prefix+'/inputs/input-'+file_name+'.nml')
    print('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')
    
    hdf5_file = glob.glob('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')

    if (len(hdf5_file) == 0): 
        print('\nExorem output file NOT generated and NOT found\n')
        df = pd.DataFrame()
        
    else :
        print('\nExorem output file generated and found\n')

        hdf5_file = glob.glob('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')

        df, keys = allkeys(pd.DataFrame(), hdf5_file[0])
        df['T_irr'] = T_irr
        df['T_int'] = T_int
        df['Met'] = Met

        new_smoothed_convective = smooth_convective_zone(df['/outputs/levels/is_convective'].iloc[-1])
        try :
            T_eff = get_Teff(df['/outputs/spectra/emission/spectral_radiosity'].iloc[-1], df['/outputs/spectra/wavenumber'].iloc[-1])
        except :
            T_eff = (np.nan)

        df['T_eff'] = [T_eff]
        df['f_sed'] = [f_sed]
        df['kzz'] = [kzz]
        df['/outputs/levels/is_convective_smoothed'] = [new_smoothed_convective]
        df, warning_flag_pressure = get_exorem_at_P_link(df, min_P_link, pressure_link_gravity=pressure_link_gravity)
        df['warning_flag_max_pressure'] = warning_flag_pressure
        temp_plot(df,file_prefix,T_P)
        os.remove('../temp/running_exorem/'+file_prefix+'/outputs/exorem/'+file_name+'.h5')

    return df