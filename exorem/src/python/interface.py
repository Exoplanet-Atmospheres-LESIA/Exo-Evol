"""
Manage Python/Fortran interface.
"""
import h5py
import numpy as np
from scipy.interpolate import interp1d

from sys import float_info

# Paths
path_to_results = 'outputs/exorem/'


# Loaders/savers
def load_cia(file):
    """
    Load a collision induced absorption file.
    :param file:
    :return:
    """
    with open(file, 'r') as f:
        header = f.readline()
        header = header.strip()
        n_cols, n_lines = header.split()
        n_cols = int(n_cols)

        temperatures = np.zeros(n_cols)

        for i in range(n_cols):
            line = f.readline()
            line = line.strip()
            temperatures[i] = float(line)

    data = np.loadtxt(file, skiprows=n_cols + 1)
    wavenumbers = data[:, 0]
    cia = data[:, 1:]

    return wavenumbers, temperatures, cia


def load_dat(file, **kwargs):
    """
    Load an Exo-REM data file.
    :param file: data file
    :param kwargs: keyword arguments for loadtxt
    :return: the data
    """
    with open(file, 'r') as f:
        header = f.readline()
        unit_line = f.readline()

    header_keys = header.rsplit('!')[0].split('#')[-1].split()
    units = unit_line.split('#')[-1].split()

    data = np.loadtxt(file, **kwargs)
    data_dict = {}

    for i, key in enumerate(header_keys):
        data_dict[key] = data[:, i]

    data_dict['units'] = units

    return data_dict


def load_hdf5(file, **kwargs):
    """
    Load an HDF5 file file.
    :param file: HDF5 file
    :param kwargs: keyword arguments for h5py.file
    :return: the data
    """
    data_dict = h5py.File(file, mode='r', **kwargs)

    return data_dict


def load_matrix(file, **kwargs):
    """
    Loaf a (n, n) matrix data file.
    :param file: matrix file
    :param kwargs: keyword arguments for loadtxt
    :return: the matrix data
    """
    with open(file, 'r') as f:
        header = f.readline()
        unit_line = f.readline()

    header_keys = header.rsplit('!')[0].split('#')[-1].split()
    units = unit_line.split('#')[-1].split()

    data = np.loadtxt(file, **kwargs)
    data_dict = {
        header_keys[0]: data[:, 0],
        header_keys[1]: data[:, 1:],
        'units': units
    }

    return data_dict


def load_result(file, **kwargs):
    """
    Load an Exo-REM data file.
    :param file: data file
    :param kwargs: keyword arguments for loadtxt or h5py.File
    :return: the data
    """
    ext = file.rsplit('.', 1)[1]

    if ext == 'h5' or ext == 'hdf5':
        data_dict = load_hdf5(file, **kwargs)
    else:
        data_dict = load_dat(file, **kwargs)

    return data_dict


def load_rectangular_matrix(file, **kwargs):
    """
    Load a (n, m) matrix data file.
    :param file: matrix data file
    :param kwargs: keyword arguments for loadtxt or h5py.File
    :return: the rectangular matrix data
    """
    with open(file, 'r') as f:
        header = f.readline()
        unit_line = f.readline()
        pressures = f.readline()

    header_keys = header.rsplit('!')[0].split('#')[-1].split()
    units = unit_line.split('#')[-1].split()
    pressures = list(pressures.split())

    for i, p in enumerate(pressures):
        pressures[i] = float(p)

    pressures = np.asarray(pressures)

    data = np.loadtxt(file, skiprows=3, **kwargs)
    data_dict = {
        header_keys[0]: data[1:, 0],
        header_keys[1]: pressures,
        header_keys[2]: data[1:, 1:],
        'units': units
    }

    return data_dict


def save_data(file, data_dict, comment=None, fmt=None):
    """
    Save a multi-columns data table with labelled columns and units into a file.
    :param file: file where to save the data
    :param data_dict: dictionary containing n keys with the values of the columns as values, the labels of the columns
    as keys, plus a 'units' key containing a list of strings of size n-1 representing the columns units.
    :param comment: string to add at the end of the first line of the file header, separated with columns labels by a
    '!'
    :param fmt: savetxt format
    """
    units = ' '.join(data_dict.pop('units'))
    keys = list(data_dict.keys())

    if comment is None:
        comment = ''
    else:
        comment = '  ! ' + comment

    header = ' '.join(keys) + comment + '\n' + units

    data_array = np.transpose(np.asarray(list(data_dict.values())))
    np.savetxt(file, data_array, header=header, fmt=fmt)


# Converters
def janaf2dat(file, path_output='data/thermochemical_tables/', path_gases='gases/', path_condensates='condensates/'):
    """
    Convert a JANAF tab-delimited text format (https://janaf.nist.gov/periodic_table.html) into a .dat file.
    If a transition between states is occurring in the file at temperature T, the next data line is set at T + 0.001
    instead of T. Undefined values at these transitions are linearly interpolated to T and T + 0.001. Infinity is
    replaced by the max float number.
    Source: https://janaf.nist.gov/pdf/JANAF-FourthEd-1998-1Vol1-Intro.pdf
    :param file: JANAF tab-delimited test format to be converted
    :param path_output: path to thermochemical tables
    :param path_gases: path to thermochemical tables of gases
    :param path_condensates: path to thermochemical tables of condensates
    :return: data into a dictionary format
    """
    # Initialization
    condensate_phase_symbols = ['l', 's', 'cd', 'cr', 'am', 'vit', 'mon', 'pol', 'sln', 'aq']
    gas_phase_symbols = ['g']

    janaf_columns_label = ['T(K)', 'Cp', 'S', '-[G-H(Tr)]/T', 'H-H(Tr)', 'delta-f H', 'delta-f G', 'log Kf']
    dat_keys = ['temperature', 'isobaric_molar_heat_capacity', 'standard_molar_entropy', 'gibbs_energy_function',
                'enthalpy_difference', 'enthalpy_formation', 'gibbs_free_energy_formation',
                'equilibrium_constant_formation']
    dat_units = ['K', 'J.K-1.mol-1', 'J.K-1.mol-1', 'J.K-1.mol-1', 'kJ.mol-1', 'kJ.mol-1', 'kJ.mol-1', 'None']

    is_condensate = False
    is_gas = False

    data_dict = {}

    with open(file, 'r') as f:
        # Header
        header = f.readline()
        header = header.strip()

        species_name, species_ref = header.split('\t')

        species_name, species_symbol = species_name.replace(')', '').split('(', 1)
        species_name = species_name.strip()

        species_ref, species_states = species_ref.split('(', 1)
        species_states = species_states.replace(')', '')
        species_states = species_states.split(',')

        for species_state in species_states:
            if species_state in condensate_phase_symbols:
                is_condensate = True
            elif species_state in gas_phase_symbols:
                is_gas = True
            else:
                raise ValueError(f"state '{species_state}' is not recognized or ambiguous")

        if is_condensate and is_gas:
            raise ValueError(f"states '{species_states}' contains both gaseous and condensate phases, uses separate "
                             f"files for the two phases")
        elif is_gas:
            path_output += path_gases
        elif is_condensate:
            path_output += path_condensates
        else:
            raise ValueError(f"states '{species_states}' contains neither gas or condensate phase")

        # Columns
        columns_labels = f.readline()
        columns_labels = columns_labels.strip()
        columns_labels = columns_labels.split('\t')

        for i, label in enumerate(columns_labels):
            if label != janaf_columns_label[i]:
                raise ValueError(f"expected column label '{janaf_columns_label[i]}' got '{label}' instead")

        # Data
        data_dict['units'] = dat_units

        for key in dat_keys:
            data_dict[key] = []

        for line in f:
            line = line.strip()
            cols = line.split('\t')

            for i, col in enumerate(cols):
                if dat_keys[i] == 'temperature' and np.size(data_dict['temperature']) > 0:
                    if data_dict['temperature'][-1] == float(col):
                        data_dict[dat_keys[i]].append(float(col) + 0.001)
                    else:
                        data_dict[dat_keys[i]].append(float(col))
                elif col == 'INFINITE':
                    data_dict[dat_keys[i]].append(float_info.max)
                else:
                    try:
                        print(dat_keys[i], col)
                        data_dict[dat_keys[i]].append(float(col))
                    except ValueError:
                        try:
                            # Manage when spaces are used instead of tabs
                            col = col.split(' ')
                            j = 0

                            for split_col in col:
                                if split_col != '':
                                    data_dict[dat_keys[i + j]].append(float(split_col))
                                    j += 1
                        except ValueError:
                            # Manage mid-right column at transitions
                            data_dict[dat_keys[i]].append(np.nan)

            # Manage right columns at transitions
            if np.size(cols) < np.size(dat_keys):
                for i in range(np.size(cols), np.size(dat_keys)):
                    if np.size(data_dict[dat_keys[i]]) < np.size(data_dict[dat_keys[np.size(cols) - 1]]):
                        data_dict[dat_keys[i]].append(np.nan)

    # Interpolate right columns values at transitions
    temp = np.asarray(data_dict['temperature'])
    print(data_dict)
    for key in dat_keys:
        if key == 'temperature':
            continue

        for i, value in enumerate(data_dict[key]):
            if np.isnan(value):
                print(i, temp[i], key, value)
                wh = np.where(np.logical_not(np.isnan(data_dict[key])))
                arr = np.asarray(data_dict[key])
                interp_val = interp1d(temp[wh], arr[wh], fill_value='extrapolate')
                data_dict[key][i] = interp_val(data_dict['temperature'][i])

    # Save
    file = path_output + species_symbol + '.dat'
    save_data(
        file, data_dict, fmt='%.6e',
        comment=f'{species_name} ({species_symbol}) {species_ref} ({",".join(species_states)}), '
                f'reference_temperature = 298.16 K, standard_state_pressure = 0.1 MPa, '
                f'source: NIST-JANAF Thermochemical Tables (https://janaf.nist.gov/)'
    )

    return data_dict
