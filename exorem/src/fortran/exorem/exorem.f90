program exorem
    ! """
    ! 1D radiative-equilibrium model.
    ! Fluxes are calculated using the two-stream approximation assuming hemispheric closure. The radiative-convective
    ! equilibrium is solved assuming that the net flux (radiative + convective) is conservative. The conservation of
    ! flux over the pressure grid is solved iteratively using a constrained linear inversion method.
    ! Includes Rayleigh scattering and absorption and scattering by clouds -- calculated from extinction coefficient,
    ! single scattering albedo, and asymmetry factor interpolated from precomputed tables for a set of wavelengths and
    ! particle radii.
    !
    ! Useful references:
    !   - Baudino et al. 2015 (https://doi.org/10.1051/0004-6361/201526332)
    !   - Baudino et al. 2017 (https://doi.org/10.3847%2F1538-4357%2Faa95be)
    !   - Charnay et al. 2018 (https://doi.org/10.3847%2F1538-4357%2Faaac7d)
    !   - Blain et al. 2020 (https://doi.org/10.1051/0004-6361/202039072)
    ! """
    use atmosphere, only: gravities_layers, molar_masses_layers, scale_height, &
        n_layers, n_levels, pressures, pressures_layers, temperatures, temperatures_layers, &
        eddy_diffusion_coefficient, &
        h2_vmr, he_vmr, z_vmr, pressure_min, tau, tau_rayleigh
    use chemistry, only: calculate_chemistry, condensate_names, n_condensates, calculate_gases_molar_mass, gas_id, &
        gases_molar_mass, gases_names, n_gases, element_is_in_gases_list, gases_c_p, temperatures_thermochemistry
    use cloud, only: n_clouds, cloud_particle_radius, cloud_particle_density, &
        reference_wavenumber, cloud_fraction, cloud_names
    use cloud_mixing, only: calculate_cloud_mixing, calculate_cloud_mixing2
    use exorem_interface, only: read_exorem_input_parameters, load_themochemical_tables, load_vmr_profiles, &
        vmr_profiles_file, retrieval_tolerance, use_irradiation, use_pressure_grid, output_fluxes, output_hdf5, &
        output_transmission_spectra, hdf5_file_out, eddy_mode, load_kzz_profile, load_cloud_profiles
    use exorem_retrieval, only: retrieval_level_bottom, retrieval_level_top, &
        retrieval_flux_error_bottom, retrieval_flux_error_top, smoothing_bottom, smoothing_top, weight_apriori, &
        n_iterations, n_non_adiabatic_iterations, chemistry_iteration_interval, cloud_iteration_interval, &
        n_burn_iterations
    use files, only: file_name_size
    use interface, only: path_outputs, path_k_coefficients, &
        kernel_file_out, spectrum_derivative_file_out, temperature_profile_file, spectrum_file_out, &
        temperature_profile_file_out, vmr_file_out, &
        output_species_spectral_contributions, output_cia_spectral_contribution, output_thermal_spectral_contribution, &
        output_full,read_data_file, read_cia_file, add_light_source, light_source_spectrum_file, &
        reallocate_1Ddouble, reallocate_2Ddouble
    use k_coefficients_interface_h5, only: wavenumbers_k, &
        samples_k, kcoeff_species, n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
        p_k_species, t_k_species, weights_k, n_k_samples, read_k_coeff_h5, n_k_samples_max
    use light_source, only: light_source_effective_temperature, light_source_irradiation, light_source_radius, &
        light_source_range, light_source_irradiance
    use math, only: matinv, pi, interp, interp_ex, interp_ex_0d, convolve, chi2_reduced
    use optics, only: get_refractive_index, default_refractive_index, rayleigh_scattering_coefficient
    use physics, only: cst_R, cst_sigma, n_elements, elements_symbol, spherical_black_body_spectral_radiance, &
        planck_function
    use radiative_transfer, only: calculate_radiative_transfer
    use species, only: n_species, species_names, species_vmr_layers, cia_files, &
        elemental_h_ratio, solar_h_ratio, species_at_equilibrium, rayleigh_scattering_coefficients
    use spectrometrics, only: wavenumber_max, wavenumber_min, wavenumber_step, n_wavenumbers, wavenumbers, &
        spectral_radius
    use target, only: latitude, target_gravity, target_internal_temperature, target_radius
    use transit_spectrum, only: calculate_transit_spectrum

    implicit none

    double precision, parameter :: a_1bar = log(1d3)

    logical :: any_cloud_condensed, retrieval_converged

    integer :: &
        i, ic, ik, j, k, &      ! loop indices
        n_retrieved_levels, &   ! number of retrieved levels
        iter, &                 ! iteration index
        level_1bar              ! 1-bar level index

    integer, dimension(:), allocatable :: &
        layer_clouds, &         ! clouds condensation layer
        layer_condensates       ! condensates condensation layer

    doubleprecision :: &
        chi2_0, chi2_1, &                   ! reduced chi2 of flux of previous and current iteration
        corr, &                             !
        cpr, &                              ! (J.mol-1.K-1) isobaric molar heat capacity
        dcpr, &                             ! derivative of isobaric molar heat capacity
        gr, grt, &                          !
        metallicity,  &                     ! atmopsheric metallicity
        mixing_length, &                    ! (m) cloud miwing length
        solution_deviation, &               ! ratio between the inversed internal flux and the target internal flux
        t_layer, &                          ! temperature inside the layers
        t_1bar, &                           ! temperature at the 1-bar level
        temperature_variation               ! maiximum ratio between the temperature and its variation in each level

    doubleprecision :: t0, tf   ! start and end time of run

    integer, allocatable :: is_convective(:)
    doubleprecision, allocatable :: tad(:), errt(:), errtb(:), grada(:)

    doubleprecision, allocatable :: &
        cloud_radius_tmp(:), &              !
        cloud_vmr(:, :), &                  !
        cloud_vmr_tmp(:), &                 !
        cloud_vmr_sat(:), &                 !
        dt(:), &                            !
        dt_conv(:), &                       !
        eddy_overshoot(:), &                !
        flux(:, :), &                       !
        flux_cloud(:, :), &                 !
        flux_clear(:, :), &                 !
        flux_conv(:), &                     !
        gas_element_vmr(:, :), &            !
        gases_vmr(:, :), &                  !
        grad(:), &                          !
        h2_h2_cia(:, :), &                  !
        h2_he_cia(:, :), &                  !
        h2O_n2_cia(:, :), &                 !
        h2o_h2o_cia(:, :), &                !
        log_pressure(:), &                  !
        matrix_inv(:, :), &                 !
        matrix_inv_diag(:), &               !
        matrix_r(:, :), &                   !
        matrix_s(:, :), &                   !
        matrix_sk(:, :), &                  !
        matrix_ssk(:, :), &                 !
        matrix_t(:, :), &                   !
        matrix_t_cloud(:, :), &             !
        p_c_condensates(:), &               !
        pressures_apriori(:), &             !
        rad_diff(:), &                      !
        rad_noise(:), &                     !
        radiosity_internal(:), &            !
        radiosity_internal_cloud(:), &      !
        radiosity_internal_target(:), &     !
        spectral_radiosity_species(:, :), & !
        spectral_radiosity(:, :), &         !
        spectral_radiosity_clear(:, :), &   !
        spectral_radiosity_cloud(:, :), &   !
        spectral_radius_derivative(:, :), & !
        spectral_radius_derivative_clear(:, :), &
        spectral_radius_species(:, :), &    !
        spectral_radiosity_cia(:), &        !
        spectral_radius_cia(:), &           !
        spectral_radiosity_clouds(:), &     !
        spectral_radius_full_cover(:), &    !
        spectral_radius_clear(:), &         !
        spectral_radius_clouds(:), &        !
        spectral_radiosity_thermal(:), &    !
        tau_cloud(:, :), &                  !
        tau_clear(: ,: ,:), &               !
        tau_rayleigh_clear(: ,:), &         !
        temperatures_apriori(:), &          !
        v_sed(:), &                         !
        v_mixing(:), &                      !
        vmr_sat_condensates(:, :), &        !
        vmr_c_condensates(:), &             !
        sum_t(:)                            !

    ! Store start time
    call cpu_time(t0)

    ! Load parameters and initalize variables
    call init_exorem()

    ! Main loop
    do iter = 0, n_iterations
        write(*, '(/, "Iteration ", I0, /, "____")') iter

        ! At iter=n_non_adiabatic_iterations: set adiabatic gradient (+ d_grad) below upper adiabatic level
        if((iter == n_non_adiabatic_iterations).and.(n_iterations>0)) then
            call init_adiabat()
        end if

        ! Check for cloud condensation
        call check_cloud_condensation()

        ! Compute radiative transfer without clouds
        if(n_clouds > 0 .and. cloud_fraction >= 1d0 - 1d-12 .and. any_cloud_condensed) then
            ! Cloud fraction is 1, no need to calculate the radiative transfer without clouds
            if(cloud_fraction > 1d0) then
                write(*, '("Error: cloud fraction", F0.2," greater than 1")') cloud_fraction
                stop
            end if

            radiosity_internal(:) = 0d0
            matrix_t(:, :) = 0d0
            flux(:, :) = 0d0
            spectral_radiosity(:, :) = 0d0
            tau_clear(:, :, :) = 0d0
            tau_rayleigh_clear(:, :) = 0d0
        else
            call calculate_radiative_transfer(&
                gases_vmr(:, :), &
                pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                pressures, temperatures, &
                light_source_irradiance, &
                n_species, -1, &
                wavenumber_min, wavenumber_step, &
                0, cloud_vmr, tau_cloud, cloud_particle_density, cloud_particle_radius, &
                n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                wavenumbers_k, n_k_samples, &
                p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                radiosity_internal, matrix_t, flux, spectral_radiosity, wavenumbers&
            )

            tau_clear(:, :, :) = tau(:, :, :)
            tau_rayleigh_clear(:, :) = tau_rayleigh(:, :)
        end if

        ! Fill flux without clouds ("clear") and initialize flux with clouds
        flux_clear(:, :) = flux(:, :)
        flux_cloud(:, :) = 0d0
        spectral_radiosity_clear(:, :) = spectral_radiosity(:, :)
        spectral_radiosity_cloud(:, :) = 0d0

        ! Compute radiative transfer with clouds
        if(n_clouds > 0 .and. any_cloud_condensed) then
            call calculate_radiative_transfer(&
                gases_vmr(:, :), &
                pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                pressures, temperatures, &
                light_source_irradiance, &
                n_species, -1, &
                wavenumber_min, wavenumber_step, &
                n_clouds, cloud_vmr, tau_cloud, cloud_particle_density, cloud_particle_radius, &
                n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                wavenumbers_k, n_k_samples, &
                p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                radiosity_internal_cloud, matrix_t_cloud, flux_cloud, spectral_radiosity_cloud, wavenumbers &
            )

            ! Modify flux and matrix K to take into account the clouds
            spectral_radiosity(:, 1:n_wavenumbers) = &
                (1d0 - cloud_fraction) * spectral_radiosity_clear(:, 1:n_wavenumbers) + &
                cloud_fraction * spectral_radiosity_cloud(:, 1:n_wavenumbers)

            do j = 1, n_levels
                radiosity_internal(j) = &
                    (1d0 - cloud_fraction) * radiosity_internal(j) + cloud_fraction * radiosity_internal_cloud(j)

                do i = 1, n_levels
                    matrix_t(i, j) = (1d0 - cloud_fraction) * matrix_t(i, j) + cloud_fraction * matrix_t_cloud(i, j)
                end do

                do i = 1, n_wavenumbers
                    flux(j, i) = (1d0 - cloud_fraction) * flux_clear(j, i) + cloud_fraction * flux_cloud(j, i)
                end do
            end do
        end if

        solution_deviation = abs(1d0 - radiosity_internal(n_levels) / radiosity_internal_target(1))

        ! Writing the spectral fluxes
        if(iter == n_iterations .or. retrieval_converged) then
            write(*, '()')

            if(retrieval_converged) then
                write(6, '("Info: tolerance reached (", ES13.6,")")') retrieval_tolerance
            else
                write(6, '("Info: max number of iterations reached")')
            end if

            ! Check for convergence, ideally "ratio" = 1
            write(*, '(1X, "J_int / (sigma * T_int_th**4) = ", G0)') &
                radiosity_internal(n_levels) / radiosity_internal_target(n_levels)
            write(*, '(4X, "T_int = ", F0.2, " K (T_int_th = ", F0.2, " K)")') &
                sign((abs(radiosity_internal(n_levels)) / (cst_sigma * 1d3)) ** (1d0/4d0), &
                radiosity_internal(n_levels)), target_internal_temperature
            write(*, '(4X, "T_eq = ", F0.2, " K")') &
                (sum(light_source_irradiance(:) * pi * wavenumber_step) / (cst_sigma * 1d3)) ** (1d0/4d0)
            write(*, '(4X, "T_eff = ", F0.2, " K")') &
                (sum(spectral_radiosity(n_levels, :) * wavenumber_step) / (cst_sigma * 1d3)) ** (1d0/4d0)

            ! Initialise ouput variables
            call get_outputs()

            if(output_hdf5) then
                call write_hdf5_output()
            else
                call write_outputs()
            end if

            exit
        end if

        ! Add convective term
        if(iter >= n_non_adiabatic_iterations) then
            call add_convective_term()
        end if

        do i = 1, n_retrieved_levels
            rad_diff(i) = &
                radiosity_internal_target(retrieval_level_bottom + (i - 1)) - &
                radiosity_internal(retrieval_level_bottom + (i - 1))
        end do

        ! Retrieve the temperature profile and update the temperatures
        call temperature_profile_retrieval()

        ! Recalculate z
        call calculate_altitude()

        call display_levels_summary()

        ! Calculate the vertical profile of Kzz
        if(.not. load_kzz_profile) then
            call calculate_eddy_diffusion_coefficient()
        endif

        ! Calculate thermochemical equilibrium
        if (modulo(iter, chemistry_iteration_interval) == 0 .or. iter >= n_iterations - n_burn_iterations) then
            call calculate_thermochemical_equilibrium()
        else
            write(*, '("No thermochemical calculations")')
        end if

        ! Compute cloud vertical mixing
        if(n_clouds > 0 .and. &
                (modulo(iter, cloud_iteration_interval) == 0 .or. iter >= n_iterations - n_burn_iterations)) then
            call calculate_cloud_vmr()
        else
            write(*, '("No cloud mixing calculations")')
        end if

        call display_layers_summary()

        chi2_1 = chi2_0

        chi2_0 = chi2_reduced(&
            radiosity_internal(retrieval_level_bottom:n_retrieved_levels), &
            radiosity_internal_target(retrieval_level_bottom:n_retrieved_levels), &
            rad_noise(retrieval_level_bottom:n_retrieved_levels) &
        )

        ! End of loop useful info
        do ic = 1, n_clouds
            write(*, '(A, " cloud: ")') trim(cloud_names(ic))
            write(*, '(1X, "tau max = ", ES10.3)') maxval(tau_cloud(ic, :))
            write(*, '(1X, "VMR max = ", ES10.3)') maxval(cloud_vmr(ic, :))
        end do

        ! Check for convergence, ideally "ratio" = 1
        write(*, '(/, "J_int / (sigma * T_int_th**4) = ", G0)') &
            radiosity_internal(n_levels) / radiosity_internal_target(n_levels)
        write(*, '(1X, "T_int = ", F0.2, " K (T_int_th = ", F0.2, " K)")') &
            sign((abs(radiosity_internal(n_levels)) / (cst_sigma * 1d3)) ** (1d0/4d0), &
                radiosity_internal(n_levels)), target_internal_temperature
        write(*, '(1X, "T_eq = ", F0.2, " K")') &
            (sum(light_source_irradiance(:) * pi * wavenumber_step) / (cst_sigma * 1d3)) ** (1d0/4d0)
        write(*, '(1X, "T_eff = ", F0.2, " K")') &
            (sum(spectral_radiosity(n_levels, :) * wavenumber_step) / (cst_sigma * 1d3)) ** (1d0/4d0)
        write(*, '("Chi2 =", es11.4, ", Chi2_var = ", es11.4, " (", i0, " points)")') &
            chi2_0, (chi2_0 - chi2_1) / chi2_1, n_retrieved_levels
    end do

    ! Ending
    call cpu_time(tf)
    write(*, '(/, "Done in ", f0.3, " s")') tf - t0

    contains
        subroutine add_convective_term()
            use light_source, only: light_source_irradiance

            implicit none

            doubleprecision :: total_flux, gradiant

            total_flux = radiosity_internal_target(n_levels) + sum(light_source_irradiance(:))
            flux_conv(:) = 0d0
            gradiant = 0d0

            do j = n_levels, 2, -1
                gr = log(temperatures(j - 1) / temperatures(j)) / log(pressures(j - 1) / pressures(j))
                t_layer = sqrt(temperatures(j - 1) * temperatures(j))

                call corr_adia(t_layer, &
                    gases_vmr(gas_id('H2'), min(j, n_layers)), gases_vmr(gas_id('H'), min(j, n_layers)), &
                    corr, dcpr)

                cpr = c_p_level(j) / cst_R + dcpr

                gradiant = corr / cpr
                dt_conv(j - 1) = 0d0
                dt_conv(1) = 0d0

                if(gr > gradiant) then
                    matrix_t(j - 1, j) = matrix_t(j - 1, j) + 2d3 * total_flux * &
                            (gr / gradiant - 1d0) / &
                            (temperatures(j - 1) * gradiant * log(pressures(j - 1) / pressures(j)))
                    matrix_t(j, j) = matrix_t(j, j) - 2d3 * total_flux * &
                            (gr / gradiant - 1d0) / &
                            (temperatures(j) * gradiant * log(pressures(j - 1) / pressures(j)))
                    matrix_t(j, j - 1) = matrix_t(j, j - 1) - 2d3 * total_flux * &
                            (gr / gradiant - 1d0) / &
                            (temperatures(j) * gradiant * log(pressures(j - 1) / pressures(j)))
                    matrix_t(j - 1, j - 1) = matrix_t(j - 1, j - 1) + 2d3 * total_flux * &
                            (gr / gradiant - 1d0) / &
                            (temperatures(j - 1) * gradiant * log(pressures(j - 1) / pressures(j)))
                    radiosity_internal(j) = radiosity_internal(j) + 1d3 * total_flux * &
                            (gr / gradiant - 1d0) * (gr / gradiant - 1d0)
                    radiosity_internal(j - 1) = radiosity_internal(j - 1) + 1d3 * total_flux * &
                            (gr / gradiant - 1d0) * (gr / gradiant - 1d0)
                    flux_conv(j) = flux_conv(j) + 1d3 * total_flux * (gr / gradiant - 1d0) * (gr / gradiant - 1d0)
                    flux_conv(j - 1) = flux_conv(j - 1) + 1d3 * total_flux * &
                            (gr / gradiant - 1d0) * (gr / gradiant - 1d0)
                end if

                dt_conv(j) = 1d-7 * target_gravity / (cpr *cst_R / molar_masses_layers(j) * 1d3) * &
                    (flux_conv(j - 1) - flux_conv(j)) / (pressures(j - 1) - pressures(j))
            end do

            if(gr > gradiant) then
                radiosity_internal(1) = radiosity_internal(1) + 1d3 * total_flux * &
                        (gr / gradiant - 1d0) * (gr / gradiant - 1d0)
                matrix_t(1, 1) = matrix_t(1, 1) + 2d3 * total_flux * &
                        (gr / gradiant - 1d0) / (temperatures(1) * gradiant * log(pressures(1) / pressures(2)))
            end if

            dt_conv(1) = 1d-7 * target_gravity / (cpr *cst_R/ molar_masses_layers(1) * 1d3) * (0d0 - flux_conv(1)) / &
                (pressures(1) * (pressure_min / pressures(1))**(-1d0 / dble(n_levels - 1)) - pressures(1))
        end subroutine add_convective_term

        function c_p_level(level_id)
            ! """
            ! Return the isobaric molar heat capacity in the current atmosphere at a given temperature.
            ! :param temperature: (K) temperature
            ! :return c_p_level: (J.K-1.mol-1) isobaric molar heat capacity
            ! """
            implicit none

            integer, intent(in) :: level_id
            integer :: i, layer_id
            doubleprecision :: c_p_level, t_layer

            t_layer = sqrt(temperatures(level_id - 1) * temperatures(level_id))
            layer_id = max(level_id - 1, 1)

            c_p_level = 0d0

            do i = 1, n_gases
                c_p_level = c_p_level + &
                    (gases_vmr(i, layer_id) * interp_ex_0d(t_layer, temperatures_thermochemistry, gases_c_p(i, :)))
            end do

            return
        end function c_p_level

        subroutine calculate_altitude()
            use atmosphere, only: z, scale_height

            implicit none

            integer :: i, j
            double precision :: gg

            ! Calculation of the molecular weight in the layers
            molar_masses_layers(:) = 0d0

            do i = 1, n_gases
                molar_masses_layers(:n_layers) = molar_masses_layers(:n_layers) + &
                    gases_vmr(i, :) * gases_molar_mass(i) * 1d3
            end do

            molar_masses_layers(n_levels) = molar_masses_layers(n_layers)

            write(*, '("Mean molar mass: ", ES10.3, " kg.mol-1")') sum(molar_masses_layers(:)) / n_levels * 1d-3

            if(level_1bar > 1) then
                scale_height(level_1bar) = 1d2 * cst_R * 0.5d0 * &
                    (t_1bar + temperatures_layers(level_1bar)) / (molar_masses_layers(level_1bar) * target_gravity)
                z(level_1bar) = scale_height(level_1bar) * (a_1bar - log_pressure(level_1bar))

                do j = level_1bar, 2, -1
                    gg = target_gravity * (target_radius / (target_radius + z(j)))**2
                    scale_height(j - 1) = 1d2 * cst_R * temperatures_layers(j - 1) / (molar_masses_layers(j - 1) * gg)
                    z(j - 1) = z(j) + scale_height(j - 1) * (log_pressure(j) - log_pressure(j - 1))
                    gravities_layers(j - 1) = &
                        target_gravity * (target_radius / (target_radius + 0.5d0 * (z(j) + z(j - 1)))) ** 2
                    scale_height(j - 1) = 1d2 * cst_R * temperatures_layers(j - 1) / &
                        (molar_masses_layers(j - 1) * gravities_layers(j - 1))
                end do
            else
                level_1bar = 1
                scale_height(1) = 1d2 * cst_R * 0.5d0 * (t_1bar + temperatures_layers(1)) / &
                    (molar_masses_layers(1) * target_gravity)
                z(1) = scale_height(1) * (a_1bar - log_pressure(1))
            end if

            do j = level_1bar, n_layers
                gg = target_gravity * (target_radius / (target_radius + z(j)))**2
                scale_height(j) = 1d2 * cst_R * temperatures_layers(j) / (molar_masses_layers(j) * gg)
                z(j + 1) = z(j) + scale_height(j) * (log_pressure(j) - log_pressure(j + 1))
                gravities_layers(j) = target_gravity * (target_radius / (target_radius + 0.5d0 * (z(j) + z(j + 1))))**2
                scale_height(j) = 1d2 * cst_R * temperatures_layers(j) / &
                        (molar_masses_layers(j) * gravities_layers(j))
            end do
        end subroutine calculate_altitude

        subroutine calculate_cloud_vmr()
            use cloud, only: n_clouds, cloud_names, cloud_mode, cloud_molar_mass, sticking_efficiency, &
                cloud_particle_density, cloud_particle_radius, sedimentation_parameter, supersaturation_parameter
            use math, only: interp

            implicit none

            integer :: ic2, ind1, condensation_level(n_clouds)
            doubleprecision :: q0, pbot

            ! Initialisation and display
            do ic = 1, n_clouds
                do ic2 = 1, n_condensates
                    if (trim(condensate_names(ic2)) == trim(cloud_names(ic))) then
                        pbot = p_c_condensates(ic2) * 1d3  ! bar to mbar
                        condensation_level(ic) = layer_condensates(ic2)

                        exit
                    else if (ic2 == n_condensates) then
                        pbot = 500d0   ! reference pressure (in mbar)

                        write(*, '("Warning: cloud ''", A, "'' not implemented ! &
                        &Assuming bottom pressure of ", F0.3, " Pa")') &
                        trim(cloud_names(ic)), pbot * 1d2
                    end if
                end do

                if(pbot > 0d0) then
                    write(*, '(A, " cloud from ", ES10.3, " Pa:")') trim(cloud_names(ic)), pbot * 1d2
                    write(*, '(1X, "Condensation level = ", I0)') condensation_level(ic)
                else
                    write(*, '(A, " cloud (has not condensed):")') trim(cloud_names(ic))
                end if

                if(cloud_mode == 'fixedRadius') then
                    write(*, '(1X, "Radius (fixed) = ", ES10.3, " m")') cloud_particle_radius(ic, 1)
                else if(cloud_mode == 'fixedSedimentation' .or. cloud_mode == 'fixedRadiusCondensation') then
                    write(*, '(1X, "Sedimentation parameter (fixed) = ", ES10.3)') sedimentation_parameter(ic)
                else if(cloud_mode == 'fixedRadiusTime') then
                    write(*, '(1X, "Supersaturation parameter (fixed) = ", ES10.3)') supersaturation_parameter(ic)
                    write(*, '(1X, "Sticking efficiency (fixed) = ", ES10.3)') sticking_efficiency(ic)
                end if

                write(*, '(1X, "Density = ", ES10.3, " kg.m-3")') cloud_particle_density(ic)
                write(*, '(1X, "Molar mass = ", ES10.3, " kg.mol-1")') cloud_molar_mass(ic) * 1d-3
                write(*, '(1X, "Reference wavelength (for diagnostic) = ", ES10.3, " m")') &
                    1d-2 / reference_wavenumber(ic)

                if(pbot <= pressures(n_levels) .and. pbot > tiny(0d0)) then
                    write(*, '("Warning: ", A, " cloud bottom pressure (", ES10.3, ") &
                        &is higher than the top pressure level (", ES10.3, ")")') &
                        trim(cloud_names(ic)), pbot, pressures(n_layers)
                end if

                ! Compute cloud vertical mixing
                layer_clouds(ic) = n_layers

                do i = 1, n_layers
                    cloud_radius_tmp(i) = cloud_particle_radius(ic, i)

                    do ic2 = 1, n_condensates
                        if (trim(condensate_names(ic2)) == trim(cloud_names(ic))) then
                            cloud_vmr_sat(i) = &
                                vmr_sat_condensates(ic2, i) * cloud_molar_mass(ic) / molar_masses_layers(i)

                            if (layer_condensates(ic2) >= n_layers .and. i == n_layers) then
                                layer_clouds(ic) = n_layers + 1
                                q0 = vmr_sat_condensates(ic2, i) * cloud_molar_mass(ic) / molar_masses_layers(i)
                            else if (i == layer_condensates(ic2)) then
                                layer_clouds(ic) = layer_condensates(ic2)
                                q0 = vmr_c_condensates(ic2) * cloud_molar_mass(ic) / molar_masses_layers(i)
                            end if

                            exit
                        else if (ic2 == n_condensates) then
                            cloud_vmr_sat(i) = 1d0
                            q0 = 0d0
                            layer_clouds(ic) = n_layers
                        end if
                    end do

                    cloud_vmr_sat(i) = max(0d0, cloud_vmr_sat(i))
                end do

                write(*, '(1X, "Max cloud saturation VMR: ", ES10.3)') maxval(cloud_vmr_sat(layer_clouds(ic):n_layers))

                if(cloud_mode == 'fixedRadiusTime') then
                    call calculate_cloud_mixing2(n_layers, pressures, temperatures, &
                        pressures_layers, temperatures_layers, gravities_layers, molar_masses_layers, &
                        layer_clouds(ic), pbot, cloud_vmr_sat, &
                        cloud_vmr_tmp, q0, eddy_diffusion_coefficient, cloud_radius_tmp, cloud_particle_density(ic), &
                        v_sed(:), v_mixing, sticking_efficiency(ic), supersaturation_parameter(ic))
                else
                    call calculate_cloud_mixing(n_layers, pressures, temperatures, &
                        pressures_layers, temperatures_layers, gravities_layers, molar_masses_layers, &
                        layer_clouds(ic), pbot, cloud_vmr_sat, &
                        cloud_vmr_tmp, q0, eddy_diffusion_coefficient, cloud_mode, eddy_mode, &
                        sedimentation_parameter(ic), &
                        cloud_radius_tmp, &
                        cloud_particle_density(ic), &
                        v_sed, v_mixing)
                end if

                do i = 1, n_layers
                    cloud_vmr(ic, i) = max(0d0, cloud_vmr_tmp(i))
                    cloud_particle_radius(ic, i) = cloud_radius_tmp(i)
                end do

                ind1 = min(n_layers, layer_clouds(ic))
                ind1 = max(1, ind1)
            end do
        end subroutine calculate_cloud_vmr

        subroutine calculate_eddy_diffusion_coefficient()
            implicit none

            integer :: iconv
            doubleprecision :: alpha

            iconv = 1
            alpha = 1d0 + 2d0 * (1500d0 - target_internal_temperature) / 1200d0
            alpha = max(min(alpha, 3d0), 1d0)

            do i = 1, n_layers - 1
                t_layer = sqrt(temperatures_layers(i) * temperatures_layers(i + 1))

                cpr = c_p_level(i + 1) / cst_R + dcpr

                gr = cpr * log(temperatures_layers(i + 1) / temperatures_layers(i)) / &
                    log(pressures_layers(i + 1) / pressures_layers(i))

                if(trim(eddy_mode) == 'Ackerman') then
                    mixing_length = scale_height(i) * 1d3 * max(0.1d0, gr)
                    eddy_diffusion_coefficient(i) = &
                        1d4 * scale_height(i)*1d3 / 3d0 * (mixing_length / scale_height(i)*1d-3)**(4d0 / 3d0) * &
                        (1d-3 * radiosity_internal_target(i) / cpr * &
                         scale_height(i)*1d3 * 1d-2 * gravities_layers(i) / (1d2 * pressures_layers(i)))**(1d0 / 3d0) !(kzz in cm**2/s)
                else if(trim(eddy_mode) == 'AckermanConvective' .or. trim(eddy_mode) == 'infinity') then
                    mixing_length = 1d0 * scale_height(i)*1d3
                    eddy_diffusion_coefficient(i) = &
                        1d4 * scale_height(i)*1d3 / 3d0 * (mixing_length / scale_height(i)*1d-3)**(4d0 / 3d0) * &
                        (1d-3 * max(flux_conv(i), 1d-6 * radiosity_internal_target(1)) / cpr * &
                            scale_height(i)*1d3 * 1d-2 * gravities_layers(i) / (1d2 * pressures_layers(i)))**(1d0 / 3d0) !(kzz in cm**2/s)
                    if(flux_conv(i) > 1d0) then
                        iconv = i
                        eddy_overshoot(i) = 0d0
                    else
                        eddy_overshoot(i) = &
                            eddy_diffusion_coefficient(iconv) * (scale_height(i)*1d3 / scale_height(iconv)*1d-3)**2 * &
                            (pressures_layers(i) / pressures_layers(iconv))**(alpha * sqrt(gravities_layers(i) / 1d5))
                    end if

                    eddy_diffusion_coefficient(i) = max(eddy_diffusion_coefficient(i), eddy_overshoot(i))
                end if
            end do

            eddy_diffusion_coefficient(n_layers) = eddy_diffusion_coefficient(n_layers - 1)
        end subroutine calculate_eddy_diffusion_coefficient

        subroutine calculate_thermochemical_equilibrium()
            implicit none

            do ik = 1, n_species
                if(species_at_equilibrium(ik)) then
                    call calculate_chemistry(&
                        .True., &
                        gases_vmr, &
                        gas_element_vmr, p_c_condensates, vmr_sat_condensates, vmr_c_condensates, layer_condensates&
                    )

                    do i = 1, n_species
                        do j = 1, n_layers
                            if(species_at_equilibrium(i)) then
                                species_vmr_layers(j, i) = gases_vmr(gas_id(species_names(i)), j)
                            end if
                        end do
                    end do

                    exit
                end if
            end do

            do ik = 1, n_species
                if (.not. species_at_equilibrium(ik)) then
                    call calculate_chemistry(&
                        .False., &
                        gases_vmr, &
                        gas_element_vmr, p_c_condensates, vmr_sat_condensates, vmr_c_condensates, layer_condensates&
                    )

                    do i = 1, n_species
                        do j = 1, n_layers
                            if(.not. species_at_equilibrium(i)) then
                                species_vmr_layers(j, i) = gases_vmr(gas_id(species_names(i)), j)
                            end if
                        end do
                    end do

                    exit
                end if
            end do
        end subroutine calculate_thermochemical_equilibrium

        subroutine check_cloud_condensation()
            implicit none

            any_cloud_condensed = .False.

            if (n_clouds > 0) then
                do i = 1, n_clouds
                    if (layer_clouds(i) < n_layers) then
                        any_cloud_condensed = .True.

                        exit
                    end if
                end do
            end if
        end subroutine check_cloud_condensation

        subroutine corr_adia(t, h2_vmr, h_vmr, corr, dcpr)
            implicit none

            doubleprecision, intent(in) :: &
                t, h2_vmr, h_vmr
            doubleprecision, intent(out) :: &
                corr, dcpr

            doubleprecision, parameter :: dh0 = 230.65d3
            doubleprecision :: h_abd, tdqhdt

            h_abd = max(2d0 * h2_vmr + h_vmr, tiny(0d0))  ! max is used to handle a case where H abundance is 0

            tdqhdt = 2d0 * h_vmr * h2_vmr * dh0 / (cst_R * t) / (h_abd + h2_vmr)
            corr = 1d0 + tdqhdt / (2d0 - h_vmr)
            dcpr = tdqhdt * dh0 / (cst_R * t) / (1d0 - 0.5d0 * h_vmr)
        end subroutine corr_adia

        subroutine display_infos()
            use atmosphere, only: z
            use interface, only: output_files_suffix
            use species, only: elemental_h_ratio, solar_h_ratio

            implicit none

            integer :: ik1, ik2

            write(*, '(/, "Atmosphere summary:")')
            write(*, '("lvl", T5, "z", T14, "P(lvl)", T24, "T(lvl)", T33, "P(lay)", T43, "T(lay)", T52, &
                &"Molar mass", T63, "Gravity")')
            write(*, '(T5, "(km)", T14, "(Pa)", T24, "(K)", T33, "(Pa)", T43, "(K)", T52, "(kg.mol-1)", T63, &
                &"(m.s-2)")')

            do j = n_layers, 1, -1
                write(*, '(I3, F9.2, ES10.3, F9.2)') j + 1, z(j + 1), pressures(j + 1), temperatures(j + 1)
                write(*, '(T33, ES10.3, F9.3, ES10.3, F9.2)') &
                    pressures_layers(j) * 1d2, temperatures_layers(j), molar_masses_layers(j) * 1d-3, &
                    gravities_layers(j) * 1d-2
            end do

            write(*, '(I3, F9.2, ES10.3, F9.2)') 1, z(1), pressures(1), temperatures(1)

            ! Printing the mole fractions in the atmospheric layers
            do i = 1, (n_species + 4) / 5
                ik1 = 1 + 5 * (i - 1)
                ik2 = min0(ik1 + 4, n_species)

                write(6, 208) (species_names(ik), ik = ik1, ik2)

                do j = n_layers, 1, -1
                    write(6, 209) j, pressures_layers(j), temperatures_layers(j), &
                        (species_vmr_layers(j, ik), ik = ik1, ik2)
                end do
            end do

            208  format(/, 10x, 'P', 10x, 'T', 5(6x, a4, 1x))
            209  format(i4, ES12.4, f8.2, 5ES11.3)

            write(*, '()')
            write(*, '("H2 VMR: ", T9, ES10.3)') h2_vmr
            write(*, '("He VMR: ", T9, ES10.3)') he_vmr
            write(*, '("Z VMR: ", T9, ES10.3)') z_vmr

            do i = 2, n_elements
                if(element_is_in_gases_list(i)) then
                    write(*, '("(", A, "/H): ", T9, ES10.3, 1X, "(", ES10.3, " solar)")') &
                        elements_symbol(i), elemental_h_ratio(i) / elemental_h_ratio(1), &
                        elemental_h_ratio(i) / solar_h_ratio(i)
                end if
            end do

            write(*, '("(Z/H): ", T9, ES10.3, 1X, "(", ES10.3, " solar)")') &
                sum(elemental_h_ratio(3:)) / elemental_h_ratio(1), metallicity
            write(*, '("Mean molar mass (initial): ", ES10.3, " kg.mol-1")') &
                sum(molar_masses_layers(:)) * 1d-3 / n_layers

            write(*, '(/, "Main parameters:")')
            write(*, '(1X, "Output files suffix: ''", A, "''")') trim(output_files_suffix)

            write(*, 201) latitude, target_gravity * 1d-2, target_radius
            201  format(1X, 'Latitude: ', f0.2, ' deg,  gravity at 1 bar:', f8.3, &
                    ' m.s-2', ', target radius at 1 bar:', f8.1, ' km')
            ! TODO native conversion from erg.s-1... to W.m-2
            write(*, 320) retrieval_level_bottom, retrieval_level_top, &
                    retrieval_flux_error_bottom, retrieval_flux_error_top, &
                    smoothing_bottom, smoothing_top, &
                    weight_apriori, n_iterations, &
                    target_internal_temperature, radiosity_internal_target(n_levels) * 1d-3, light_source_irradiation
            320  format(1X, 'First and last levels used for constraining net flux: ', I0, 1X, I0, /, &
                    1X, 'Relative error on fluxes at these levels:', 2ES11.4, /, &
                    1X, 'Correlation length at P(1) and P(n_levels): ', F0.2, ' and ', F0.2, ' scale height', /, &
                    1X, 'Weight apriori =', ES10.3, 5x, I0, ' iterations', /, &
                    1X, 'Internal temperature: ', f0.2, ' K', /, &
                    1X, 'Internal flux: ', ES10.3, ' W.m-2', /, &
                    1X, 'Received flux: ', ES10.3, ' W.m-2' &
            )
        end subroutine display_infos

        subroutine display_layers_summary()
            implicit none

            character(len=file_name_size) :: &
                str_tmp, str_tmp2

            integer :: int_tmp

            str_tmp = ''

            do i = 1, n_clouds
                str_tmp = trim(str_tmp) // '|' // trim(cloud_names(i)) // ' cloud  radius'
            end do

            write(*, '(/, "Layers summary:")')
            write(*, '("lay", T5, "Pressure", T15, "T", T24, "Molar mass", T35, &
                &"Gravity", T43, "Kzz", T52, A)') trim(str_tmp)

            str_tmp = ''

            do j = 1, n_clouds
                int_tmp = max(len_trim(cloud_names(j)) + 1 + len_trim(' cloud'), 10)

                write(str_tmp2, '(A, I0, A)') '(A, A', int_tmp, ', 5X, "(m)")'
                write(str_tmp, str_tmp2) trim(str_tmp), 'VMR'
            end do

            write(*, '(T5, "(Pa)", T15, "(K)", T24, "(kg.mol-1)", T35, &
                &"(m.s-2)", T43, "(cm2.s-1)", T52, A)') trim(str_tmp)

            do i = n_layers, 1, -1
                str_tmp = ''

                do j = 1, n_clouds
                    int_tmp = max(len_trim(cloud_names(j)) + 1 + len_trim(' cloud'), 10)

                    if(i >= layer_clouds(j)) then
                        write(str_tmp2, '(A, I0, A)') '(A, ES', int_tmp, '.3, ES8.1)'
                        write(str_tmp, str_tmp2)  &
                            trim(str_tmp), cloud_vmr(j, i), cloud_particle_radius(j, i)
                    else
                        write(str_tmp2, '(A, I0, A)') '(A, A', int_tmp, ', A8)'
                        write(str_tmp, str_tmp2) trim(str_tmp), '-', '-'
                    end if
                end do

                write(*, '(I3, ES10.3, F9.3, ES10.3, F9.2, ES9.2, 1X, A)') &
                    i, pressures_layers(i) * 1d2, temperatures_layers(i), molar_masses_layers(i) * 1d-3, &
                    gravities_layers(i) * 1d-2, eddy_diffusion_coefficient(i), trim(str_tmp)
            end do
        end subroutine display_layers_summary

        subroutine display_levels_summary()
            use atmosphere, only: z

            implicit none

            integer :: i

            write(*, '("lvl", T5, "z (km)", T15, "p (Pa)", T25, "T (K)", T35, "dT")')

            do i = n_levels, 1, -1
                write(*, '(I3, F10.2, ES10.3, F10.3, F10.3)') i, z(i), pressures(i) * 1d2, temperatures(i), dt(i)
            end do
        end subroutine display_levels_summary

        subroutine get_cloud_optical_constants()
            ! """
            !
            ! """
            use cloud, only: cloud_opacity_files, q_ext_ref, asymetry_factor, single_scattering_albedo, &
                q_scat, q_ext_ref, q_ext, n_cloud_particle_radii, qext, gfactor, omeg, qscat, qabs, &
                cloud_particle_radius_data, update_cloud_optical_parameters
            use math, only: interp_ex_0d, reallocate_3Ddouble
            use spectrometrics, only: n_wavenumbers
            use exorem_interface, only: load_cloud_optical_constants
            use atmosphere, only: n_layers

            implicit none

            integer :: i, j, k, n_rad_max, nond(n_clouds)
            doubleprecision, allocatable :: radius_file(:), sigi(:), omegi(:, :), epi(:, :), gfactori(:, :)
            doubleprecision, dimension(:), allocatable :: qext_i, omeg_i, g_factor_i, qabs_i, qscat_i

            ! Initialisation
            allocate(&
                n_cloud_particle_radii(n_clouds), &
                q_ext_ref(n_clouds, n_layers), &
                asymetry_factor(n_clouds, n_wavenumbers, n_layers), &
                single_scattering_albedo(n_clouds, n_wavenumbers, n_layers), &
                q_scat(n_clouds, n_wavenumbers, n_layers), &
                q_ext(n_clouds, n_wavenumbers, n_layers) &
            )

            nond(:) = 0
            n_cloud_particle_radii(:) = 0

            do i = 1, n_clouds
                call load_cloud_optical_constants(&
                    cloud_opacity_files(i), sigi, radius_file, omegi, epi, gfactori&
                )

                nond(i) = size(sigi)
                n_cloud_particle_radii(i) = size(radius_file)

                ! Allocations
                allocate(&
                    qext_i(n_cloud_particle_radii(i)), &
                    omeg_i(n_cloud_particle_radii(i)), &
                    g_factor_i(n_cloud_particle_radii(i)), &
                    qabs_i(n_cloud_particle_radii(i)), &
                    qscat_i(n_cloud_particle_radii(i))&
                )

                if(i > 1) then
                    if(n_cloud_particle_radii(i) > n_rad_max) then
                        n_rad_max = n_cloud_particle_radii(i)

                        call reallocate_3Ddouble(cloud_particle_radius_data, n_clouds, n_rad_max, n_wavenumbers)
                        call reallocate_3Ddouble(gfactor, n_clouds, n_rad_max, n_wavenumbers)
                        call reallocate_3Ddouble(omeg, n_clouds, n_rad_max, n_wavenumbers)
                        call reallocate_3Ddouble(qext, n_clouds, n_rad_max, n_wavenumbers)
                        call reallocate_3Ddouble(qscat, n_clouds, n_rad_max, n_wavenumbers)
                        call reallocate_3Ddouble(qabs, n_clouds, n_rad_max, n_wavenumbers)
                    end if
                else
                    n_rad_max = n_cloud_particle_radii(i)

                    allocate(&
                        cloud_particle_radius_data(n_clouds, n_rad_max, n_wavenumbers), &
                        gfactor(n_clouds, n_rad_max, n_wavenumbers), &
                        omeg(n_clouds, n_rad_max, n_wavenumbers), &
                        qext(n_clouds, n_rad_max, n_wavenumbers), &
                        qscat(n_clouds, n_rad_max, n_wavenumbers), &
                        qabs(n_clouds, n_rad_max, n_wavenumbers) &
                    )
                end if

                ! Wavenumber interpolation and storage
                do j = 1, n_wavenumbers
                    do k = 1, n_cloud_particle_radii(i)
                        omeg_i(k) = &
                            interp_ex_0d(wavenumbers(j), sigi, omegi(nond(i):1:-1, k))
                        g_factor_i(k) = &
                            interp_ex_0d(wavenumbers(j), sigi, gfactori(nond(i):1:-1, k))

                        qabs_i(k) = &
                            interp_ex_0d(wavenumbers(j), sigi, epi(nond(i):1:-1, k)) * &
                            (1 - interp_ex_0d(wavenumbers(j), sigi, omegi(nond(i):1:-1, k)))
                        qscat_i(k) = &
                            interp_ex_0d(wavenumbers(j), sigi, epi(nond(i):1:-1, k)) * &
                            interp_ex_0d(wavenumbers(j), sigi, omegi(nond(i):1:-1, k))
                    end do

                    do k = 1, n_cloud_particle_radii(i)
                        qext_i(k) = qabs_i(k) + qscat_i(k)

                        cloud_particle_radius_data(i, k, j) = radius_file(k)
                        qabs(i, k, j) = qabs_i(k)

                        ! Scattering
                        qscat(i, k, j) = qscat_i(k)
                        omeg(i, k, j) = omeg_i(k)
                        gfactor(i, k, j) = g_factor_i(k)

                        qext(i, k, j) = qabs(i, k, j) + qscat(i, k, j)
                    end do
                end do

                deallocate(&
                    qext_i, omeg_i, g_factor_i, qabs_i, qscat_i &
                )
            end do

            ! Radius interpolation
            call update_cloud_optical_parameters()
        end subroutine get_cloud_optical_constants

        subroutine init_adiabat()
            implicit none

            integer :: level_max_adiabat
            doubleprecision, parameter :: dgrad = 5d-3

            level_max_adiabat = 0

            do i = n_levels - 1, 3, -1
                gr = log(temperatures(i - 1) / temperatures(i)) / log(pressures(i - 1) / pressures(i))
                t_layer = sqrt(temperatures(i - 1) * temperatures(i))

                call corr_adia(t_layer, &
                    gases_vmr(gas_id('H2'), i), gases_vmr(gas_id('H'), i), &
                    corr, dcpr)

                cpr = c_p_level(i) / cst_R + dcpr

                grad(i - 1) = corr / cpr + dgrad
                grt = gr

                if(grt > grad(i - 1)) then
                    if(level_max_adiabat == 0) then
                        level_max_adiabat = i
                    end if

                    temperatures(i - 1) = temperatures(i) * (pressures(i - 1) / pressures(i)) ** grad(i - 1)

                    t_layer = sqrt(temperatures(i - 1) * temperatures(i))

                    call corr_adia(t_layer, &
                        gases_vmr(gas_id('H2'), i), gases_vmr(gas_id('H'), i), &
                        corr, dcpr)

                    cpr = c_p_level(i) / cst_R + dcpr

                    grad(i - 1) = corr / cpr + dgrad
                    temperatures(i - 1) = temperatures(i) * (pressures(i - 1) / pressures(i)) ** grad(i - 1)
                else
                    temperatures(i - 1) = temperatures(i) * (pressures(i - 1) / pressures(i)) ** grt
                end if
            end do

            gr = log(temperatures(1) / temperatures(2)) / log(pressures(1) / pressures(2))
            t_layer = sqrt(temperatures(1) * temperatures(2))

            call corr_adia(t_layer, &
                gases_vmr(gas_id('H2'), 1), gases_vmr(gas_id('H'), 1), &
                corr, dcpr)

            cpr = c_p_level(2) / cst_R + dcpr

            grad(1) = corr / cpr + dgrad

            if(gr > grad(1)) then
                temperatures(1) = temperatures(2) * (pressures(1) / pressures(2)) ** grad(1)

                t_layer = sqrt(temperatures(1) * temperatures(2))

                call corr_adia(t_layer, &
                    gases_vmr(gas_id('H2'), 1), gases_vmr(gas_id('H'), 1), &
                    corr, dcpr)

                cpr = c_p_level(2) / cst_R + dcpr

                grad(1) = corr / cpr + dgrad
                temperatures(1) = temperatures(2) * (pressures(1) / pressures(2))**grad(1)
            else
                temperatures(1) = temperatures(2) * (pressures(1) / pressures(2))**gr
            end if

            write(*, '(/, 1X, "Adiabat at levels  < ", I0, ": grad     T", /)') level_max_adiabat

            do i = level_max_adiabat, 2, -1
                write(*, '(23X, F7.4, F8.1)') grad(i - 1), temperatures(i - 1)
            end do
        end subroutine init_adiabat

        subroutine init_atmosphere()
            use atmosphere, only: pressure_max, z
            use h5_interface, only: hdf5_files_extension
            use exorem_interface, only: load_temperature_profile_h5
            use math, only: deg2rad

            implicit none

            integer :: &
                i_ext, &
                i_pressure, &
                i_temperature, &
                ninput, &
                id(1)
            doubleprecision :: &
                d_1bar, &
                ep, &
                pressure_factor
            character(len=file_name_size), dimension(:), allocatable :: labels, units
            integer, dimension(:), allocatable :: tab_i
            doubleprecision, dimension(:), allocatable :: api, ati, apl, p_tmp, t_tmp
            doubleprecision, dimension(:, :), allocatable :: columns

            write(*, '("Initializing atmosphere...")')

            if(.not. use_pressure_grid) then
                n_layers = n_levels - 1

                allocate(&
                    gravities_layers(n_layers), &
                    scale_height(n_layers), &
                    pressures(n_levels), &
                    temperatures(n_levels), &
                    pressures_layers(n_layers), &
                    temperatures_layers(n_layers), &
                    molar_masses_layers(n_levels) &  ! molar_mass_layers needs to have a size of n_levels instead of n_layers due to how convection is calculated
                )

                call reallocate_1Ddouble(eddy_diffusion_coefficient, n_layers)

                allocate(log_pressure(n_levels), apl(n_layers), tab_i(n_layers))

                log_pressure = 0d0

                pressures(1) = pressure_max
                log_pressure(1) = log(pressure_max)

                do j = 2, n_levels
                    ep = dble(j - 1) / dble(n_levels - 1)
                    pressures(j) = pressure_max * (pressure_min / pressure_max) ** ep
                    log_pressure(j) = log(pressures(j))
                end do

                ! Calculation of the pressure in the layers (mbar)
                do j = 1, n_layers
                    pressures_layers(j) = sqrt(pressures(j) * pressures(j + 1))
                    apl(j) = log(pressures_layers(j))
                end do
            end if

            write(*, '(1X, "Reading a-priori temperature profile in file ''", A, "''")') &
                trim(temperature_profile_file)

            i_ext = index(trim(temperature_profile_file), hdf5_files_extension)

            if(i_ext == 0) then
                call read_data_file(temperature_profile_file, columns, labels, units)
            else
                call load_temperature_profile_h5(temperature_profile_file, p_tmp, t_tmp)

                allocate(columns(2, size(p_tmp)), labels(2), units(2))

                columns(1, :) = p_tmp(:)
                columns(2, :) = t_tmp(:)
                labels(1) = 'pressure'
                labels(2) = 'temperature'
                units(1) = 'Pa'
                units(2) = 'K'
            end if

            i_pressure = 0
            i_temperature = 0
            ninput = size(columns(1, :))

            allocate(api(ninput), ati(ninput))

            do i = 1, size(columns(:, 1))
                ! TODO native conversion from mbar to Pa
                if (trim(labels(i)) == 'pressure') then
                    if (units(i) == 'Pa') then
                        pressure_factor = 1d-2
                    elseif (units(i) == 'mbar') then
                        pressure_factor = 1d0
                    else
                        write(*, '("Error: pressures must be in mbar or Pa, but are in ", A)') trim(units(i))
                        stop
                    end if

                    api(:) = log(columns(i, :) * pressure_factor)

                    i_pressure = i
                else if (trim(labels(i)) == 'temperature') then
                    if (units(i) /= 'K') then
                        write(*, '("Error: temperature must be in K")')
                        stop
                    end if

                    ati(:) = log(columns(i, :))

                    i_temperature = i
                end if
            end do

            if (i_pressure == 0) then
                write(*, '("Error: label ''pressure'' not found")')
                stop
            end if

            if (i_temperature == 0) then
                write(*, '("Error: label ''temperature'' not found")')
                stop
            end if

            if(use_pressure_grid) then
                n_levels = size(columns(i_pressure, :))
                n_layers = n_levels - 1

                allocate(&
                    gravities_layers(n_layers), &
                    scale_height(n_layers), &
                    pressures(n_levels), &
                    temperatures(n_levels), &
                    temperatures_apriori(n_levels), &
                    pressures_layers(n_layers), &
                    temperatures_layers(n_layers), &
                    molar_masses_layers(n_levels) &  ! molar_mass_layers needs to have a size of n_levels instead of n_layers due to how convection is calculated
                )

                allocate(log_pressure(n_levels), apl(n_layers), tab_i(n_layers))

                call reallocate_1Ddouble(eddy_diffusion_coefficient, n_layers)

                if(columns(i_pressure, n_levels) > columns(i_pressure, 1)) then
                    ! Reverse pressures and temperatures so that the bottom level is at index 1
                    do j = 1, n_levels
                        pressures(j) = exp(api(n_levels - j + 1))
                        temperatures(j) = exp(ati(n_levels - j + 1))
                    end do
                else
                    pressures(:) = exp(api(:))
                    temperatures(:) = exp(ati(:))
                end if

                log_pressure(:) = log(pressures(:))

                do j = 1, n_layers
                    pressures_layers(j) = sqrt(pressures(j) * pressures(j + 1))
                    apl(j) = log(pressures_layers(j))
                    temperatures_layers(j) = exp(interp_ex_0d(apl(j), api, ati))
                end do
            else
                do j = 1, n_layers
                    if(exp(log_pressure(j)) < exp(minval(api))) then
                        id = minloc(api)
                        temperatures(j) = exp(ati(id(1)))
                        temperatures_layers(j) = exp(ati(id(1)))
                    else if (exp(log_pressure(j)) <= exp(maxval(api))) then
                        temperatures(j) = exp(interp_ex_0d(log_pressure(j), api, ati))
                        temperatures_layers(j) = exp(interp_ex_0d(apl(j), api, ati))
                    else
                        id = maxloc(api)
                        temperatures(j) = exp(ati(id(1)))
                        temperatures_layers(j) = exp(ati(id(1)))
                    end if
                end do

                if(log_pressure(n_levels) <= maxval(api)) then
                    temperatures(n_levels) = exp(interp_ex_0d(log_pressure(n_levels), api, ati))
                else
                    id = maxloc(api)
                    temperatures(n_levels) = exp(ati(id(1)))
                end if
            end if

            allocate(z(n_levels), pressures_apriori(n_levels), temperatures_apriori(n_levels))

            temperatures_apriori = temperatures
            pressures_apriori = pressures

            z(:) = 0d0

            ! Calculation of the altitude levels (km) and of the gravity in the layers (cm sec-2)
            do j = 1, n_layers
                tab_i(j) = j
            end do

            d_1bar = interp_ex_0d(a_1bar, apl, dble(tab_i))
            t_1bar = interp_ex_0d(a_1bar, apl, temperatures_layers)

            if(d_1bar < 0.4999d0) then
                write(*, '("Warning: the 1-bar level is not included in the grid")')
            end if

            level_1bar = idnint(d_1bar + 1d-4)

            allocate(gas_element_vmr(n_elements, n_layers))

            gas_element_vmr(1, :) = h2_vmr
            gas_element_vmr(2, :) = he_vmr

            do i = 3, n_elements
                gas_element_vmr(i, :) = 2d0 * h2_vmr * elemental_h_ratio(i)
            end do

            allocate(gases_vmr(n_gases, n_layers))

            gases_vmr(:, :) = 0d0

            gases_vmr(gas_id('H2'), :) = h2_vmr
            gases_vmr(gas_id('He'), :) = he_vmr
            gases_vmr(gas_id('H2O'), :) = z_vmr

            call calculate_gases_molar_mass()

            call calculate_altitude()
        end subroutine init_atmosphere

        subroutine init_chemistry()
            implicit none

            character(len=file_name_size) :: str_tmp
            character(len=file_name_size), dimension(:), allocatable :: labels, units
            integer :: i, j, i_pressure, ninput, id(1), indx, indx2
            doubleprecision :: log_pl(n_layers), pressure_factor, sum_tmp
            doubleprecision, dimension(:), allocatable :: api
            doubleprecision, dimension(:, :), allocatable :: columns

            write(*, '("Initializing chemistry...")')

            if(load_vmr_profiles) then
                write(*, '(1X, "Reading VMR profiles in file ''", A, "''")') &
                    trim(vmr_profiles_file)

                call read_data_file(vmr_profiles_file, columns, labels, units)
                i_pressure = 0
                ninput = size(labels)
                pressure_factor = 1d0

                allocate(api(size(columns(i, :))))

                log_pl = log(pressures_layers)

                do i = 1, size(columns(:, 1))
                    ! TODO native conversion from mbar to Pa
                    if (trim(labels(i)) == 'pressure') then
                        if (units(i) == 'Pa') then
                            pressure_factor = 1d-2
                        elseif (units(i) == 'mbar') then
                            pressure_factor = 1d0
                        else
                            write(*, '("Error: pressures must be in mbar or Pa, but are in ", A)') trim(units(i))
                            stop
                        end if

                        api(:) = log(columns(i, :) * pressure_factor)

                        i_pressure = i
                    end if
                end do

                if (i_pressure == 0) then
                    write(*, '("Error: label ''pressure'' not found")')
                    stop
                end if

                gases_vmr(:, :) = 0d0

                do ik = 1, n_gases
                    do i = 1, ninput
                        str_tmp = labels(i)
                        indx = index(trim(str_tmp), trim(gases_names(ik)))
                        indx2 = index(trim(str_tmp), 'cloud')

                        ! Handle the case where the label is e.g. 'volum_mixing_ratio_<species>'
                        if(indx > 0) then
                            if(len_trim(str_tmp) == len_trim(gases_names(ik))) then
                                str_tmp = trim(str_tmp(indx:))
                            else if(len_trim(str_tmp) > len_trim(gases_names(ik)) .and. indx > 1 .and. indx2 == 0) then
                                if(str_tmp(indx-1:indx-1) == '_') then
                                    str_tmp = trim(str_tmp(indx:))
                                else
                                    cycle
                                end if
                            else
                                cycle
                            end if
                        else
                            cycle
                        end if

                        ! Read the species VMR
                        if (trim(str_tmp) == trim(gases_names(ik))) then
                            do j = 1, n_layers
                                if(pressures_layers(j) < minval(columns(i_pressure, :)) * pressure_factor) then
                                    id = minloc(api)
                                    gases_vmr(ik, j) = columns(i, id(1))
                                else if (pressures_layers(j) <= maxval(columns(i_pressure, :))  * pressure_factor) then
                                    gases_vmr(ik, j) = exp(interp_ex_0d(log_pl(j), api, log(columns(i, :))))
                                else
                                    id = maxloc(api)
                                    gases_vmr(ik, j) = columns(i, id(1))
                                end if
                            end do

                            exit
                        end if
                    end do
                end do

                ! Ensure sum of VMR = 1
                do j = 1, n_layers
                    sum_tmp =  sum(gases_vmr(:, j))

                    do ik = 1, n_gases
                        gases_vmr(ik, j) = gases_vmr(ik, j) / sum_tmp
                    end do
                end do

                do ik = 1, n_species
                    species_vmr_layers(:, ik) = gases_vmr(gas_id(species_names(ik)), :)
                end do
            else
                do ik = 1, n_species
                    if(species_at_equilibrium(ik)) then
                        call calculate_chemistry(&
                            .True., &
                            gases_vmr, &
                            gas_element_vmr, p_c_condensates, vmr_sat_condensates, vmr_c_condensates, layer_condensates&
                        )

                        do i = 1, n_species
                            do j = 1, n_layers
                                if(species_at_equilibrium(i)) then
                                    species_vmr_layers(j, i) = gases_vmr(gas_id(species_names(i)), j)
                                end if
                            end do
                        end do

                        exit
                    end if
                end do

                do ik = 1, n_species
                    if(.not. species_at_equilibrium(ik)) then
                        call calculate_chemistry(&
                            .False., &
                            gases_vmr, &
                            gas_element_vmr, p_c_condensates, vmr_sat_condensates, vmr_c_condensates, layer_condensates&
                        )

                        do i = 1, n_species
                            do j = 1, n_layers
                                if (.not. species_at_equilibrium(i)) then
                                    species_vmr_layers(j, i) = gases_vmr(gas_id(species_names(i)), j)
                                end if
                            end do
                        end do

                        exit
                    end if
                end do
            end if
        end subroutine init_chemistry

        subroutine init_cloud()
            ! Possible cloud modes:
            ! cloud_mode = 'fixedRadius' (fixed radius)
            !            = 'fixedSedimentation' (fixed sedimentation_parameter)
            !            = 'fixedRadiusCondensation' (fixed radius by sedimentation_parameter at condensation level)
            !            = 'fixedRadiusTime' (fixed radius by condensation timescale)
            use chemistry, only: calculate_species_molar_mass
            use cloud, only: n_clouds, cloud_molar_mass, cloud_particle_radius

            implicit none

            character(len=file_name_size) :: str_tmp, str_tmp2
            character(len=file_name_size), dimension(:), allocatable :: labels, units
            logical :: cloud_vmr_found, cloud_particle_radius_found
            integer :: i, j, i_pressure, ninput, id(1)
            doubleprecision :: log_pl(n_layers), pressure_factor
            doubleprecision, dimension(:), allocatable :: api
            doubleprecision, dimension(:, :), allocatable :: columns

            write(*, '("Initializing clouds...")')

            allocate(&
                cloud_molar_mass(n_clouds), &
                layer_clouds(n_clouds), &
                cloud_vmr(n_clouds, n_layers), &
                tau_cloud(n_clouds, n_layers)&
            )

            cloud_molar_mass(:) = 0d0
            layer_clouds(:) = 0d0
            cloud_vmr(:, :) = 0d0
            tau_cloud(:, :) = 0d0

            if(n_clouds > 0) then
                do ic = 1, n_clouds
                    do i = 1, n_layers
                        cloud_particle_radius(ic, i) = cloud_particle_radius(ic, 1) ! initialization cloud radii
                    end do

                    cloud_molar_mass(ic) = calculate_species_molar_mass(cloud_names(ic)) * 1d3  ! kg.mol-1 to g.mol-1
                end do

                if(load_cloud_profiles) then
                    write(*, '(1X, "Reading cloud profiles in file ''", A, "''")') &
                        trim(vmr_profiles_file)

                    call read_data_file(vmr_profiles_file, columns, labels, units)

                    i_pressure = 0
                    ninput = size(labels)
                    pressure_factor = 1d0
                    allocate(api(size(columns(i, :))))

                    log_pl = log(pressures_layers)

                    do i = 1, size(columns(:, 1))
                        ! TODO native conversion from mbar to Pa
                        if (trim(labels(i)) == 'pressure') then
                            if (units(i) == 'Pa') then
                                pressure_factor = 1d-2
                            elseif (units(i) == 'mbar') then
                                pressure_factor = 1d0
                            else
                                write(*, '("Error: pressures must be in mbar or Pa, but are in ", A)') trim(units(i))
                                stop
                            end if

                            api(:) = log(columns(i, :) * pressure_factor)

                            i_pressure = i
                        end if
                    end do

                    if (i_pressure == 0) then
                        write(*, '("Error: label ''pressure'' not found")')
                        stop
                    end if

                    do ik = 1, n_clouds
                        str_tmp = 'cloud_vmr_' // trim(cloud_names(ik))
                        str_tmp2 = 'cloud_particle_radius_' // trim(cloud_names(ik))
                        cloud_vmr_found = .False.
                        cloud_particle_radius_found = .False.

                        do i = 1, ninput
                            if(trim(labels(i)) == trim(str_tmp)) then
                                if (units(i) /= 'None') then
                                    write(*, '("Error: VMR units must be ''None'', but are in ", A)') trim(units(i))
                                    stop
                                end if

                                do j = 1, n_layers
                                    if(pressures_layers(j) < minval(columns(i_pressure, :)) * pressure_factor) then
                                        id = minloc(api)
                                        cloud_vmr(ik, j) = columns(i, id(1))
                                    else if (pressures_layers(j) <= maxval(columns(i_pressure, :))  * pressure_factor) then
                                        cloud_vmr(ik, j) = exp(interp_ex_0d(log_pl(j), api, log(max(1d-30, columns(i, :)))))
                                    else
                                        id = maxloc(api)
                                        cloud_vmr(ik, j) = columns(i, id(1))
                                    end if
                                end do

                                cloud_vmr_found = .True.
                            else if(trim(labels(i)) == trim(str_tmp2)) then
                                if (units(i) /= 'm') then
                                    write(*, '("Error: cloud particle radii must be in m, but are in ", A)') &
                                        trim(units(i))
                                    stop
                                end if

                                do j = 1, n_layers
                                    if(pressures_layers(j) < minval(columns(i_pressure, :)) * pressure_factor) then
                                        id = minloc(api)
                                        cloud_particle_radius(ik, j) = columns(i, id(1))
                                    else if (pressures_layers(j) <= maxval(columns(i_pressure, :))  * pressure_factor) then
                                        cloud_particle_radius(ik, j) = &
                                            exp(interp_ex_0d(log_pl(j), api, log(max(1d-30, columns(i, :)))))
                                    else
                                        id = maxloc(api)
                                        cloud_particle_radius(ik, j) = columns(i, id(1))
                                    end if
                                end do

                                cloud_particle_radius_found = .True.
                            end if
                        end do

                        if (.not. cloud_vmr_found) then
                            write(*, '("Error: label ''", A, "'' not found")') &
                                trim(str_tmp)
                            stop
                        end if

                        if (.not. cloud_particle_radius_found) then
                            write(*, '("Error: label ''", A, "'' not found")') &
                                trim(str_tmp2)
                            stop
                        end if
                    end do
                else
                    call calculate_cloud_vmr()
                end if  ! if load_cloud_profiles

                call get_cloud_optical_constants()
            end if  ! (n_clouds > 0)
        end subroutine init_cloud

        subroutine init_exorem()
            implicit none

            retrieval_converged = .False.

            call read_exorem_input_parameters()

            ! Allocations
            allocate(&
                layer_condensates(n_condensates), &
                vmr_c_condensates(n_condensates)&
            )

            call load_themochemical_tables()
            call init_atmosphere()

            allocate(&
                cloud_radius_tmp(n_layers), &
                cloud_vmr_tmp(n_layers), &
                cloud_vmr_sat(n_layers), &
                grad(n_layers), &
                eddy_overshoot(n_layers), &
                vmr_sat_condensates(n_condensates, n_layers), &
                p_c_condensates(n_layers), &
                species_vmr_layers(n_layers, n_species), &
                v_sed(n_layers), &
                v_mixing(n_layers), &
                sum_t(n_layers)&
            )

            call reallocate_2Ddouble(cloud_particle_radius, n_clouds, n_layers)

            call read_k_coeff_h5(species_names, path_k_coefficients)
            call init_wavenumbers()
            call load_cia()

            p_k_species(:, :) = p_k_species(:, :) * 1d-2  ! Pa to mbar

            allocate(&
                tau(n_levels, n_wavenumbers, n_k_samples_max), &
                tau_clear(n_levels, n_wavenumbers, n_k_samples_max), &
                tau_rayleigh(n_levels, n_wavenumbers), &
                tau_rayleigh_clear(n_levels, n_wavenumbers) &
            )

            tau(:, :, :) = 0d0
            tau_clear(: ,: ,:) = 0d0
            tau_rayleigh(:, :) = 0d0
            tau_rayleigh_clear(:, :) = 0d0

            call init_species()
            call init_kzz()
            call init_chemistry()
            call init_rayleigh_scattering()
            call init_cloud()

            n_retrieved_levels = retrieval_level_top - retrieval_level_bottom + 1

            allocate(rad_noise(n_retrieved_levels))

            do i = 1, n_retrieved_levels
                rad_noise(i) = exp(log(retrieval_flux_error_top) * dble(i - 1) / dble(n_retrieved_levels - 1) + &
                    log(retrieval_flux_error_bottom) * (1d0 - dble(i - 1) / &
                    dble(n_retrieved_levels - 1))) * radiosity_internal_target(i + retrieval_level_bottom - 1)
            end do

            call get_stellar_spectrum()

            call display_infos()

            allocate(&
                dt_conv(n_levels), &
                matrix_s(n_levels, n_levels), &
                matrix_r(n_levels, n_retrieved_levels), &
                flux_conv(n_levels), &
                flux_clear(n_levels, n_wavenumbers), &
                matrix_sk(n_levels, n_retrieved_levels), matrix_ssk(n_retrieved_levels, n_retrieved_levels), &
                matrix_inv_diag(n_retrieved_levels), &
                rad_diff(n_retrieved_levels), &
                dt(n_levels), &
                matrix_t(n_levels, n_levels), &
                matrix_t_cloud(n_levels, n_levels), &
                radiosity_internal(n_levels), &
                radiosity_internal_cloud(n_levels), flux(n_levels, n_wavenumbers), &
                spectral_radiosity(n_levels, n_wavenumbers), &
                spectral_radiosity_clear(n_levels, n_wavenumbers), &
                spectral_radiosity_cloud(n_levels, n_wavenumbers), &
                flux_cloud(n_levels, n_wavenumbers), &
                matrix_inv(n_retrieved_levels, n_retrieved_levels)&
            )

            dt_conv(:) = 0d0
            matrix_s(:, :) = 0d0
            matrix_r(:, :) = 0d0
            flux_conv(:) = 0d0
            flux_clear(:, :) = 0d0
            matrix_sk(:, :) = 0d0
            matrix_ssk(:, :) = 0d0
            matrix_inv_diag(:) = 0d0
            rad_diff(:) = 0d0
            dt(:) = 0d0
            matrix_t(:, :) = 0d0
            matrix_t_cloud(:, :) = 0d0
            radiosity_internal(:) = 0d0
            radiosity_internal_cloud(:) = 0d0
            flux(:, :) = 0d0
            spectral_radiosity(:, :) = 0d0
            spectral_radiosity_clear(:, :) = 0d0
            spectral_radiosity_cloud(:, :) = 0d0
            flux_cloud(:, :) = 0d0
            matrix_inv(:, :) = 0d0

            chi2_0 = huge(0d0)
            chi2_1 = huge(0d0)

            call init_s_matrix()
        end subroutine init_exorem

        subroutine init_kzz()
            use atmosphere, only: eddy_diffusion_coefficient
            implicit none

            character(len=file_name_size), dimension(:), allocatable :: labels, units
            integer :: i, j, i_pressure, ninput, id(1), i_eddy
            doubleprecision :: log_pl(n_layers), pressure_factor, eddy_factor
            doubleprecision, dimension(:), allocatable :: api
            doubleprecision, dimension(:, :), allocatable :: columns

            write(*, '("Initializing eddy diffusion...")')

            allocate(radiosity_internal_target(n_levels))
            radiosity_internal_target(:) = cst_sigma * 1d3 * target_internal_temperature ** 4d0

            if(load_kzz_profile) then
                write(*, '(1X, "Reading Kzz profiles in file ''", A, "''")') &
                    trim(vmr_profiles_file)

                call read_data_file(vmr_profiles_file, columns, labels, units)

                i_pressure = 0
                i_eddy = 0
                ninput = size(labels)
                pressure_factor = 1d0
                eddy_factor = 1d0

                allocate(api(size(columns(i, :))))

                log_pl = log(pressures_layers)

                do i = 1, size(columns(:, 1))
                    ! TODO native conversion from mbar to Pa
                    if (trim(labels(i)) == 'pressure') then
                        if (units(i) == 'Pa') then
                            pressure_factor = 1d-2
                        elseif (units(i) == 'mbar') then
                            pressure_factor = 1d0
                        else
                            write(*, '("Error: pressures must be in mbar or Pa, but are in ", A)') trim(units(i))
                            stop
                        end if

                        api(:) = log(columns(i, :) * pressure_factor)

                        i_pressure = i
                    else if (trim(labels(i)) == 'eddy_diffusion_coefficient') then
                        if (units(i) == 'm2.s-1') then
                            eddy_factor = 1d2
                        else if (units(i) == 'cm2.s-1') then
                            eddy_factor = 1d0
                        else
                            write(*, '("Error: kzz must be in m2.s-1 or cm2.s-1, but are in ", A)') trim(units(i))
                            stop
                        end if

                        i_eddy = i
                    end if
                end do

                if (i_pressure == 0) then
                    write(*, '("Error: label ''pressure'' not found")')
                    stop
                end if

                if (i_eddy == 0) then
                    write(*, '("Error: label ''eddy_diffusion_coefficient'' not found")')
                    stop
                end if

                do j = 1, n_layers
                    if(pressures_layers(j) < minval(columns(i_pressure, :)) * pressure_factor) then
                        id = minloc(api)
                        eddy_diffusion_coefficient(j) = columns(i_eddy, id(1)) * eddy_factor
                    else if (pressures_layers(j) <= maxval(columns(i_pressure, :))  * pressure_factor) then
                        eddy_diffusion_coefficient(j) = &
                            exp(interp_ex_0d(log_pl(j), api, log(columns(i_eddy, :) * eddy_factor)))
                    else
                        id = maxloc(api)
                        eddy_diffusion_coefficient(j) = columns(i_eddy, id(1)) * eddy_factor
                    end if
                end do
            else
                write(*, '(1X, "Using eddy mode ''", A, "''")') &
                    trim(eddy_mode)

                do i = 1, n_layers - 1
                    eddy_diffusion_coefficient(i) =  eddy_diffusion_coefficient(1)
                    t_layer = sqrt(temperatures_layers(i) * temperatures_layers(i + 1))

                    cpr = c_p_level(i + 1) / cst_R + dcpr

                    gr = cpr * log(temperatures_layers(i + 1) / temperatures_layers(i)) / &
                        log(pressures_layers(i + 1) / pressures_layers(i))

                    if(trim(eddy_mode) == 'Ackerman') then
                        mixing_length = scale_height(i)*1d3 * max(0.1d0, gr)
                        eddy_diffusion_coefficient(i) = &
                            1d4 * scale_height(i)*1d3 / 3d0 * (mixing_length / scale_height(i)*1d-3)**(4d0 / 3d0) * &
                            (1d-3 * radiosity_internal_target(i) / cpr * &
                                scale_height(i)*1d3 * 1d-4 * gravities_layers(i) / &
                                pressures_layers(i))**(1d0 / 3d0)  ! (kzz in cm2.s-1)
                    else if (trim(eddy_mode) == 'constant') then
                        eddy_diffusion_coefficient(i) = eddy_diffusion_coefficient(1)
                    else
                        mixing_length = 1d0 * scale_height(i) * 1d3
                        eddy_diffusion_coefficient(i) = &
                            1d4 * scale_height(i)*1d3 / 3d0 * (mixing_length / scale_height(i) * 1d-3)**(4d0 / 3d0) * &
                            (1d-3 * 1d-2 * radiosity_internal_target(i) / cpr * scale_height(i) * 1d3 * 1d-4 * &
                            gravities_layers(i) / pressures_layers(i))**(1d0 / 3d0)  ! (kzz in cm2.s-1)
                    end if
                end do

                eddy_diffusion_coefficient(n_layers) = eddy_diffusion_coefficient(n_layers - 1)
            endif  ! (load_kzz_profile)
        end subroutine init_kzz

        subroutine init_rayleigh_scattering()
            implicit none

            integer :: i, j, n_no_index
            doubleprecision :: default_coeff

            write(*, '("Initializing Rayleigh scattering coefficients...")')

            n_no_index = 0

            allocate(rayleigh_scattering_coefficients(n_gases, n_wavenumbers))

            do i = 1, n_wavenumbers
                do j = 1, n_gases
                      rayleigh_scattering_coefficients(j, i) = &
                          rayleigh_scattering_coefficient(&
                              get_refractive_index(gases_names(j), wavenumbers(i)), &
                              wavenumbers(i)&
                          )
                end do
            end do

            default_coeff = rayleigh_scattering_coefficient(default_refractive_index, wavenumbers(1))

            do j = 1, n_gases
                if(&
                    rayleigh_scattering_coefficients(j, 1) >= default_coeff * (1d0 - 1d-12) .and. &
                    rayleigh_scattering_coefficients(j, 1) <= default_coeff * (1d0 + 1d-12)&
                ) then
                    n_no_index = n_no_index + 1
                end if
            end do

            write(*, '(1X, "Refractive indices of ", I0, " species calculated, &
                &default value (", F0.6, ") given to ", I0, " species")') &
                n_gases - n_no_index, default_refractive_index, n_no_index
        end subroutine init_rayleigh_scattering

        subroutine init_s_matrix()
            ! Calculate Matrix matrix_s * weight_apriori
            implicit none

            doubleprecision :: corrk, corri

            do i = 1, n_levels
                corri = smoothing_bottom * &
                    log(pressures(i) / pressures(n_levels)) / log(pressures(1) / pressures(n_levels)) + &
                    smoothing_top * &
                    (1d0 - log(pressures(i) / pressures(n_levels)) / log(pressures(1) / pressures(n_levels)))

                do k = 1, n_levels
                    corrk = &
                        smoothing_bottom * &
                        log(pressures(k) / pressures(n_levels)) / log(pressures(1) / pressures(n_levels)) + &
                        smoothing_top * &
                        (1d0 - log(pressures(k) / pressures(n_levels)) / log(pressures(1) / pressures(n_levels)))
                    corr = weight_apriori * exp(-0.5d0 * log(pressures(i) / pressures(k)) ** 2d0 / (corri * corrk))
                    matrix_s(i, k) = corr
                end do
            end do
        end subroutine init_s_matrix

        subroutine init_species()
            implicit none

            write(*, '("Initializing species VMR and metallicity...")')

            metallicity = sum(elemental_h_ratio(3:)) / sum(solar_h_ratio(3:))
        end subroutine init_species

        subroutine init_wavenumbers()
            implicit none

            integer :: i, index_closest(1)
            doubleprecision :: wvn_tmp(maxval(n_k_wavenumbers))

            write(*, '("Initializing wavenumbers...")')

            ! Check min
            index_closest = minloc(wavenumbers_k(1, :))

            wvn_tmp = wavenumbers_k(:, index_closest(1))
            index_closest = minloc(abs(wvn_tmp - wavenumber_min))

            if(wavenumber_min < wvn_tmp(index_closest(1)) * (1d0 - 1d-12) .or. &
               wavenumber_min > wvn_tmp(index_closest(1)) * (1d0 + 1d-12)) then
                write(*, '("Warning: set minimum wavenumber (", F0.3, ") not binned correctly &
                    &regarding of loaded k-tables bins centers (", F0.3, "), &
                    &changing wavenumber_min value...")') wavenumber_min, wvn_tmp(index_closest(1))

                wavenumber_min = wvn_tmp(index_closest(1))
            end if

            ! Check max
            if(mod(wavenumber_max - wavenumber_min, wavenumber_step) > 1d-12) then
                write(*, '("Warning: set maximum wavenumber (", F0.3, ") not binned correctly &
                    &regarding of set wavenumber step (", F0.3, "), &
                    &changing wavenumber_max value...")') wavenumber_max, wavenumber_step

                wavenumber_max = ceiling((wavenumber_max - wavenumber_min) / wavenumber_step) * wavenumber_step + &
                    wavenumber_min
            end if

            n_wavenumbers = int((wavenumber_max - wavenumber_min) / wavenumber_step) + 1

            allocate(wavenumbers(n_wavenumbers))

            do i = 1, n_wavenumbers
                wavenumbers(i) = wavenumber_min + wavenumber_step * dble(i - 1)
            end do

            allocate(spectral_radius(n_wavenumbers))

            spectral_radius(:) = 0d0

            write(*, '(1X, "Spectrum is calculated from ", F0.3, " to ", F0.3, " cm-1 (step = ", F0.3, "cm-1)")') &
                wavenumbers(1), wavenumbers(n_wavenumbers), wavenumber_step
        end subroutine init_wavenumbers

        subroutine load_cia()
            use math, only: interp
            use species, only: n_cia, cia_names

            implicit none

            integer :: i, j, l, n_temperatures_cia, n_wavenumbers_cia, wvn_min, wvn_max
            doubleprecision, dimension(1) :: t_tmp, cia_layers_tmp
            doubleprecision, dimension(:), allocatable :: wavenumbers_cia, temperatures_cia, cia_layers
            doubleprecision, dimension(:, :), allocatable :: cia, cia_tmp

            allocate(&
                cia_tmp(n_layers, n_wavenumbers), &
                h2_h2_cia(n_layers, n_wavenumbers), &
                h2_he_cia(n_layers, n_wavenumbers), &
                h2o_h2o_cia(n_layers, n_wavenumbers), &
                h2o_n2_cia(n_layers, n_wavenumbers) &
            )

            cia_tmp(:, :) = 0d0
            h2_h2_cia(:, :) = 0d0
            h2_he_cia(:, :) = 0d0
            h2o_h2o_cia(:, :) = 0d0
            h2o_n2_cia(:, :) = 0d0

            do i = 1, n_cia
                write(*, '("Loading collision induced absorption ''", A, "'' from file ''", A, "''")') &
                    trim(cia_names(i)), trim(cia_files(i))

                call read_cia_file(cia_files(i), wavenumbers_cia, temperatures_cia, cia)

                n_temperatures_cia = size(temperatures_cia)
                n_wavenumbers_cia = size(wavenumbers_cia)
                wvn_min = 1
                wvn_max = n_wavenumbers

                if(allocated(cia_layers)) then
                    deallocate(cia_layers)
                end if

                allocate(cia_layers(n_wavenumbers_cia))

                cia_layers = 0d0

                do l = 1, n_layers
                    ! Interpolation on temperatures
                    do j = 1, n_wavenumbers_cia
                        if(temperatures_layers(l) > temperatures_cia(n_temperatures_cia)) then
                            cia_layers(j) =cia(size(temperatures_cia), j)
                        else if (temperatures_layers(l) < temperatures_cia(1)) then
                            cia_layers(j) = cia(1, j)
                        else
                            t_tmp = temperatures_layers(l)
                            cia_layers_tmp = interp(t_tmp, temperatures_cia, cia(:, j))
                            cia_layers(j) = cia_layers_tmp(1)
                        end if
                    end do

                    ! Interpolation on wavenumbers
                    do j = 1, n_wavenumbers
                        if (wavenumbers(j) < wavenumbers_cia(1)) then
                            cia_tmp(l, j) = cia_layers(1)
                            wvn_min = j + 1
                        else
                            exit
                        end if
                    end do

                    do j = n_wavenumbers, 1, -1
                        if(wavenumbers(j) > wavenumbers_cia(n_wavenumbers_cia)) then
                            cia_tmp(l, j) = cia_layers(n_wavenumbers_cia)
                            wvn_max = j - 1
                        else
                            exit
                        end if
                    end do

                    ! Ensure we are within the wavenumber range of the CIA
                    if(wvn_min < n_wavenumbers .and. wvn_max > 1) then
                        cia_tmp(l, wvn_min:wvn_max) = interp(wavenumbers(wvn_min:wvn_max), wavenumbers_cia, cia_layers)
                    end if
                end do

                if (cia_names(i) == 'H2-H2') then
                    h2_h2_cia = cia_tmp
                elseif (cia_names(i) == 'H2-He') then
                    h2_he_cia = cia_tmp
                elseif (cia_names(i) == 'H2O-H2O') then
                    h2o_h2o_cia = cia_tmp
                elseif (cia_names(i) == 'H2O-N2') then
                    h2o_n2_cia = cia_tmp
                else
                    write(*, '("Error: CIA ''", A, "'' is not implemented")') trim(cia_names(i))
                    stop
                end if
            end do
        end subroutine load_cia

        subroutine get_outputs()
            implicit none

            doubleprecision :: cf(n_levels, n_levels)

            write(*, '("Preparing outputs...")')

            allocate(is_convective(n_levels))

            allocate(&
                tad(n_levels), errt(n_levels), errtb(n_levels), grada(n_levels), &
                spectral_radiosity_species(n_wavenumbers, n_species), &
                spectral_radiosity_clouds(n_wavenumbers), &
                spectral_radius_cia(n_wavenumbers), &
                spectral_radiosity_thermal(n_wavenumbers), &
                spectral_radius_derivative(n_levels, n_wavenumbers), &
                spectral_radius_derivative_clear(n_levels, n_wavenumbers), &
                spectral_radius_species(n_wavenumbers, n_species), &
                spectral_radiosity_cia(n_wavenumbers), &
                spectral_radius_clouds(n_wavenumbers), &
                spectral_radius_full_cover(n_wavenumbers), &
                spectral_radius_clear(n_wavenumbers) &
            )

            is_convective(:) = 0

            tad(:) = 0d0
            errt(:) = 0d0
            errtb(:) = 0d0
            grada(:) = 0d0
            spectral_radiosity_species(:, :) = 0d0
            spectral_radius_derivative(:, :) = 0d0
            spectral_radius_derivative_clear(: ,:) = 0d0
            spectral_radius_species(:, :) = 0d0
            spectral_radiosity_cia(:) = 0d0
            spectral_radius_cia(:) = 0d0
            spectral_radius_clouds(:) = 0d0
            spectral_radius_full_cover(:) = 0d0
            spectral_radius_clear(:) = 0d0
            spectral_radiosity_thermal(:) = 0d0

            ! Calculate covariance matrix
            do i = 1, n_levels
                errtb(i) = 0d0

                do j = 1, n_retrieved_levels
                    errtb(i) = errtb(i) + matrix_r(i, j) ** 2d0 * matrix_inv_diag(j)
                end do

                do k = 1, n_levels
                    cf(i, k) = matrix_s(i, k)

                    do j = 1, n_retrieved_levels
                        cf(i, k) = cf(i, k) - matrix_r(i, j) * matrix_sk(k, j)
                    end do
                end do
            end do

            do i = 1, n_levels
                errt(i) = sqrt(abs(cf(i, i)))
                errtb(i) = sqrt(abs(errtb(i)))
            end do

            ! Check superadiabatic lapse rate and 1 T-profile
            tad(n_levels) = temperatures(n_levels)
            is_convective(:) = 0

            do i = n_levels, 2, -1
                gr = log(temperatures(i - 1) / temperatures(i)) / log(pressures(i - 1) / pressures(i))
                t_layer = sqrt(temperatures(i - 1) * temperatures(i))

                call corr_adia(t_layer, &
                    gases_vmr(gas_id('H2'), min(i, n_layers)), gases_vmr(gas_id('H'), min(i, n_layers)), &
                    corr, dcpr)

                cpr = c_p_level(i) / cst_R + dcpr

                grada(i) = corr / cpr

                if(gr > grada(i)) then
                    tad(i - 1) = tad(i) * (pressures(i - 1) / pressures(i)) ** grada(i)
                    is_convective(i - 1) = 1
                else
                    tad(i - 1) = tad(i) * (pressures(i - 1) / pressures(i)) ** gr
                    is_convective(i - 1) = 0
                end if
            end do

            grada(1) = grada(2)

            if(output_transmission_spectra) then
                write(*, '("Without cloud:")')
                call calculate_transit_spectrum(&
                    tau_clear, tau_rayleigh_clear, n_k_samples(1), weights_k, spectral_radius_clear, &
                    spectral_radius_derivative_clear, &
                    .True. &
                )

                if(cloud_fraction >= 1d-12 .and. any_cloud_condensed) then
                    write(*, '("With clouds:")')
                    call calculate_transit_spectrum(&
                        tau, tau_rayleigh, n_k_samples(1), weights_k, spectral_radius_full_cover, &
                        spectral_radius_derivative, &
                        .True. &
                    )
                    spectral_radius = spectral_radius_full_cover
                else
                    spectral_radius_full_cover(:) = spectral_radius_clear
                    spectral_radius = spectral_radius_clear
                end if

                if(cloud_fraction >= 1d-12 .and. cloud_fraction <= 1d0 - 1d-12 .and. any_cloud_condensed) then
                    spectral_radius = &
                        (1d0 - cloud_fraction) * spectral_radius_clear + cloud_fraction * spectral_radius_full_cover
                    spectral_radius_derivative = &
                        (1d0 - cloud_fraction) * spectral_radius_derivative_clear + &
                        cloud_fraction * spectral_radius_derivative
                end if
            end if

            if (output_thermal_spectral_contribution) then
                call get_thermal_spectral_radiosity(spectral_radiosity_thermal(:))
            else
                spectral_radiosity_thermal(:) = -1d0
            end if

            if(n_species > 1 .and. output_species_spectral_contributions) then
                call get_spectral_radiosity_species(spectral_radiosity_species(:, :))
            else if(output_species_spectral_contributions) then
                spectral_radiosity_species(:, 1) = spectral_radiosity(n_levels, 1:n_wavenumbers)
                spectral_radius_species(:, 1) = spectral_radius
            else
                spectral_radiosity_species(:, :) = -1d0
                spectral_radius_species(:, :) = -1d0
            end if

            if(n_clouds > 0 .and. output_species_spectral_contributions) then
                call get_spectral_radiosity_clouds(spectral_radiosity_clouds(:))
            else
                spectral_radiosity_clouds(:) = -1d0
                spectral_radius_clouds(:) = -1d0
            end if

            if (output_cia_spectral_contribution) then
                call get_spectral_radiosity_cia(spectral_radiosity_cia(:))
            else
                spectral_radiosity_cia(:) = -1d0
            end if
        end subroutine get_outputs

        subroutine get_thermal_spectral_radiosity(spectral_radiosity_thermal)
            implicit none

            doubleprecision, intent(out) :: &
                spectral_radiosity_thermal(n_wavenumbers)

            doubleprecision :: &
                light_source_irradiance_null(n_wavenumbers), &
                taucl_dmp(n_clouds, n_layers), radcl_dmp(n_clouds, n_layers), fluxt_dmp(n_levels), &
                matrix_t_dmp(n_levels, n_levels), &
                flux_dmp(n_levels, n_wavenumbers), &
                flux_up_tmp(n_levels, n_wavenumbers)

            write(*, '("Calculating spectral contributions of thermal emissions")')

            light_source_irradiance_null(:) = 0d0
            spectral_radiosity_thermal(:) = 0d0
            radcl_dmp(1:n_clouds, :) = cloud_particle_radius(:, :)

            if (n_clouds == 0 .or. (n_clouds > 0 .and. cloud_fraction <= 1d0 - 1d-12)) then
                call calculate_radiative_transfer(&
                    gases_vmr(:, :), &
                    pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                    pressures, temperatures, &
                    light_source_irradiance_null(:), &
                    n_species, -1, &
                    wavenumber_min, wavenumber_step, &
                    0, cloud_vmr, taucl_dmp, cloud_particle_density, radcl_dmp, &
                    n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                    wavenumbers_k, n_k_samples, &
                    p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                    h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                    fluxt_dmp, matrix_t_dmp, flux_dmp, flux_up_tmp, wavenumbers &
                )

                spectral_radiosity_thermal(:) = flux_up_tmp(n_levels, 1:n_wavenumbers)
            end if

            if(n_clouds > 0) then
                call calculate_radiative_transfer(&
                    gases_vmr(:, :), &
                    pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                    pressures, temperatures, &
                    light_source_irradiance_null(:), &
                    n_species, -1, &
                    wavenumber_min, wavenumber_step, &
                    n_clouds, cloud_vmr, taucl_dmp, cloud_particle_density, radcl_dmp, &
                    n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                    wavenumbers_k, n_k_samples, &
                    p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                    h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                    fluxt_dmp, matrix_t_dmp, flux_dmp, flux_up_tmp, wavenumbers &
                )

                spectral_radiosity_thermal(:) = (1d0 - cloud_fraction) * spectral_radiosity_thermal(:) + &
                    cloud_fraction * flux_up_tmp(n_levels, 1:n_wavenumbers)
            end if
        end subroutine get_thermal_spectral_radiosity

        subroutine get_spectral_radiosity_species(spectral_radiosities)
            implicit none

            doubleprecision, intent(out) :: &
                spectral_radiosities(n_wavenumbers, n_species)

            integer :: &
                k
            doubleprecision :: &
                taucl_dmp(n_clouds, n_layers), radcl_dmp(n_clouds, n_layers), fluxt_dmp(n_levels), &
                matrix_t_dmp(n_levels, n_levels), &
                flux_dmp(n_levels, n_wavenumbers), &
                flux_up_tmp(n_levels, n_wavenumbers), spectral_radius_derivative_tmp(n_levels, n_wavenumbers)

            radcl_dmp(1:n_clouds, 1:n_layers) = cloud_particle_radius(:, :)

            do k = 1, n_species
                write(*, '("Calculating spectral contributions of species ", A, " (", I0, "/", I0, ")")') &
                    trim(species_names(k)), k, n_species

                call calculate_radiative_transfer(&
                    gases_vmr(:, :), &
                    pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                    pressures, temperatures, &
                    light_source_irradiance, &
                    n_species, k, &
                    wavenumber_min, wavenumber_step, &
                    0, cloud_vmr, taucl_dmp, &
                    cloud_particle_density, radcl_dmp, &
                    n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                    wavenumbers_k, n_k_samples, &
                    p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                    h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                    fluxt_dmp, matrix_t_dmp, flux_dmp, flux_up_tmp, wavenumbers &
                )

                spectral_radiosities(:, k) = flux_up_tmp(n_levels, 1:n_wavenumbers)

                if(output_transmission_spectra) then
                    call calculate_transit_spectrum(&
                        tau, tau_rayleigh, n_k_samples(1), weights_k, spectral_radius_species(:, k), &
                        spectral_radius_derivative_tmp, &
                        .False. &
                    )
                else
                    spectral_radius_derivative_tmp(:, :) = 0d0
                end if
            end do
        end subroutine get_spectral_radiosity_species

        subroutine get_spectral_radiosity_cia(spectral_radiosity_cia)
            implicit none

            doubleprecision, intent(out) :: &
                spectral_radiosity_cia(n_wavenumbers)

            doubleprecision :: &
                taucl_dmp(n_clouds, n_layers), radcl_dmp(n_clouds, n_layers), fluxt_dmp(n_levels), &
                matrix_t_dmp(n_levels, n_levels), &
                flux_dmp(n_levels, n_wavenumbers), &
                flux_up_tmp(n_levels, n_wavenumbers), spectral_radius_derivative_tmp(n_levels, n_wavenumbers)

            write(*, '("Calculating spectral contributions of CIA")')

            radcl_dmp(1:n_clouds, :) = cloud_particle_radius(:, :)

            call calculate_radiative_transfer(&
                gases_vmr(:, :), &
                pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                pressures, temperatures, &
                light_source_irradiance, &
                0, -1, &
                wavenumber_min, wavenumber_step, &
                0, cloud_vmr, taucl_dmp, cloud_particle_density, radcl_dmp, &
                n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                wavenumbers_k, n_k_samples, &
                p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                fluxt_dmp, matrix_t_dmp, flux_dmp, flux_up_tmp, wavenumbers &
            )

            spectral_radiosity_cia(:) = flux_up_tmp(n_levels, 1:n_wavenumbers)

            if(output_transmission_spectra) then
                call calculate_transit_spectrum(&
                    tau, tau_rayleigh, n_k_samples(1), weights_k, spectral_radius_cia, spectral_radius_derivative_tmp, &
                    .False. &
                )
            else
                spectral_radius_derivative_tmp(:, :) = 0d0
            end if
        end subroutine get_spectral_radiosity_cia

        subroutine get_spectral_radiosity_clouds(spectral_radiosity_clouds)
            implicit none

            doubleprecision, intent(out) :: &
                spectral_radiosity_clouds(n_wavenumbers)

            doubleprecision :: &
                taucl_dmp(n_clouds, n_layers), radcl_dmp(n_clouds, n_layers), fluxt_dmp(n_levels), &
                matrix_t_dmp(n_levels, n_levels), &
                flux_dmp(n_levels, n_wavenumbers), &
                flux_up_tmp(n_levels, n_wavenumbers), spectral_radius_derivative_tmp(n_levels, n_wavenumbers)

            write(*, '("Calculating spectral contributions of clouds")')

            radcl_dmp(1:n_clouds, :) = cloud_particle_radius(:, :)

            call calculate_radiative_transfer(&
                gases_vmr(:, :), &
                pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
                pressures, temperatures, &
                light_source_irradiance, &
                0, -1, &
                wavenumber_min, wavenumber_step, &
                n_clouds, cloud_vmr, taucl_dmp, cloud_particle_density, radcl_dmp, &
                n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
                wavenumbers_k, n_k_samples, &
                p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
                h2_h2_cia, h2_he_cia, h2o_n2_cia, h2o_h2o_cia, &
                fluxt_dmp, matrix_t_dmp, flux_dmp, flux_up_tmp, wavenumbers &
            )

            spectral_radiosity_clouds(:) = flux_up_tmp(n_levels, 1:n_wavenumbers)

            if(output_transmission_spectra) then
                call calculate_transit_spectrum(&
                    tau, tau_rayleigh, n_k_samples(1), weights_k, spectral_radius_clouds, &
                    spectral_radius_derivative_tmp, .False. &
                )
            else
                spectral_radius_derivative_tmp(:, :) = 0d0
            end if
        end subroutine get_spectral_radiosity_clouds

        subroutine get_stellar_spectrum()
            implicit none

            character(len=*), parameter :: teff_str = 'effective_temperature = '
            doubleprecision, parameter :: interpolation_step = 1d-1  ! (cm-1)

            character(len=1024) :: line
            integer :: file_unit, i, i_rad, i_wvl, j, n_filter, n_interp
            doubleprecision :: file_effective_temperature, sum_black_body

            character(len=file_name_size), dimension(:), allocatable :: labels, units
            doubleprecision, dimension(:), allocatable :: wavenumbers_tmp, irradiances_tmp, irradiance_conv_tmp, ones, &
                black_body_flux
            doubleprecision, dimension(:, :), allocatable :: columns

            allocate(light_source_irradiance(n_wavenumbers))

            if (.not. add_light_source .or. light_source_irradiation <= 0d0) then
                light_source_irradiance(:) = 0d0

                return
            end if

            ! Black body irradiance
            if(use_irradiation) then
                sum_black_body = planck_function(wavenumbers(1), light_source_effective_temperature) * wavenumbers(1)

                do i = 2, n_wavenumbers
                    sum_black_body = sum_black_body + &
                        planck_function(wavenumbers(i), light_source_effective_temperature) * &
                            (wavenumbers(i) - wavenumbers(i - 1))
                end do

                light_source_range = light_source_radius * (light_source_irradiation / (sum_black_body * pi)) ** (-0.5)
            end if

            do i = 1, n_wavenumbers
                light_source_irradiance(i) = spherical_black_body_spectral_radiance(&
                    wavenumbers(i), light_source_effective_temperature, light_source_radius, light_source_range, &
                    0.25d0 &
                ) * 1d3  ! W.m-2.sr-1/cm-1 to erg.s-1.cm-2.sr-1/cm-1
            end do

            if (light_source_spectrum_file == 'None') then
                return
            end if

            ! Read stellar spectrum
            write(*, '("Reading stellar spectrum in file ''", A, "''")') trim(light_source_spectrum_file)

            ! Find effective temperature
            open(newunit=file_unit, file=light_source_spectrum_file, status='old')
            read(file_unit, '(A)') line
            close(file_unit)

            i = index(line, teff_str)

            if (i == 0) then
                write(*, '("Error: unable to read the effective temperature of stellar spectrum file ''", A, "''", /, &
                    &"The stellar spectrum header should be :", /, /, &
                    &"# wavelength spectral_radiosity ! effective_temperature = xxxx K &
                    &(replace xxxx with the effective temperature of the spectrum)", /, &
                    &"# angstrom erg.s-1.cm-2.a-1", /, /, &
                    &"See the README for more details.")') &
                    trim(light_source_spectrum_file)
                stop
            end if

            i = i + len(teff_str)
            j = index(line(i:), ' ')
            j = i + j

            read(line(i:j), *) file_effective_temperature

            call read_data_file(light_source_spectrum_file, columns, labels, units)

            ! Find wavelength, then reverse and remove 0s to prepare for conversion from angstrom to cm-1
            i_wvl = 0

            do i = 1, size(columns(:, 1))
                if (labels(i) == 'wavelength') then
                    if (units(i) /= 'angstrom') then
                        write(*, '("Error: wavelengths must be in angstrom")')
                        stop
                    end if

                    do j = 1, size(columns(1, :))
                        if (columns(i, j) < tiny(0.)) then
                            columns(i, j) = tiny(0.)
                        end if
                    end do

                    columns(i, :) = columns(i, size(columns(i, :)):1:-1)

                    i_wvl = i
                end if
            end do

            if (i_wvl == 0) then
                write(*, '("Error: label ''wavelength'' not found")')
                stop
            end if

            ! Convert from erg.s-1.cm-2.a-1 to erg.s-1.cm-2.sr-1/cm-1
            i_rad = 0

            do i = 1, size(columns(:, 1))
                if (labels(i) == 'spectral_radiosity') then
                    if (units(i) /= 'erg.s-1.cm-2.a-1') then
                        write(*, '("Error: radiosities must be in erg.s-1.cm-2.a-1")')
                        stop
                    end if

                    columns(i, :) = columns(i, size(columns(i, :)):1:-1) * 1d8 * (columns(i_wvl, :) * 1d-8) ** 2d0 / pi
                    i_rad = i
                end if
            end do

            if (i_rad == 0) then
                write(*, '("Error: label ''spectral_radiosity'' not found")')
                stop
            end if

            ! Convert from angstrom to cm-1
            columns(i_wvl, :) = 1d0 / (columns(i_wvl, :) * 1d-8)  ! angstrom to cm-1

            ! Interpolation to obtain a regular grid
            n_filter = nint(wavenumber_step / interpolation_step)
            n_interp = nint((wavenumbers(n_wavenumbers) - wavenumbers(1)) / interpolation_step) + n_filter + 1
            allocate(wavenumbers_tmp(n_interp))
            allocate(irradiances_tmp(n_interp))

            do i = 1, n_interp
                wavenumbers_tmp(i) = wavenumbers(1) + (dble(i - 1)) * interpolation_step
            end do

            irradiances_tmp(:) = interp(wavenumbers_tmp(:), columns(i_wvl, :), columns(i_rad, :)) * &
                (light_source_radius / light_source_range) ** 2d0 * 0.25d0

            ! Sliding mean by convolution
            allocate(ones(n_filter), irradiance_conv_tmp(n_interp))

            ones(:) = 1d0 / n_filter
            irradiance_conv_tmp(:) = convolve(irradiances_tmp(:), ones(:))

            ! Get the sliding mean values at the right wavenumbers
            deallocate(irradiances_tmp)
            allocate(irradiances_tmp(n_wavenumbers), black_body_flux(n_wavenumbers))

            irradiances_tmp(:) = irradiance_conv_tmp(1:n_interp - n_filter:n_filter)

            sum_black_body = sum(light_source_irradiance(:))

            ! Convert to the right temperature
            do i = 1, n_wavenumbers
                black_body_flux(i) = spherical_black_body_spectral_radiance(&
                    wavenumbers(i), file_effective_temperature, light_source_radius, light_source_range, 0.25d0 &
                    ) * 1d3  ! W.m-2.sr-1/cm-1 to erg.s-1.cm-2.sr-1/cm-1
                light_source_irradiance(i) = irradiances_tmp(i) / black_body_flux(i) * light_source_irradiance(i)
            end do

            write(*, '(1X, "Stellar / black body irradiance = ", F0.3)') &
                sum(light_source_irradiance(:)) / sum_black_body

            write(*, '(1X, "Compensating...")')

            if(sum(light_source_irradiance(:)) / sum_black_body > 2d-2) then
                write(*, '(1X, "Warning: dispcrepancy between Stellar irradiance and black body irradiance is high. &
                    & Check the stellar spectrum or extend the wavenumber range.")')
            end if

            light_source_irradiance(:) = light_source_irradiance(:) * sum_black_body / sum(light_source_irradiance(:))
        end subroutine get_stellar_spectrum

        subroutine temperature_profile_retrieval()
            implicit none

            doubleprecision :: trace_kskt

            !  Calculate matrix_s * Kt matrix
            do i = 1, n_levels
                do j = 1, n_retrieved_levels
                    matrix_sk(i, j) = 0d0
                end do

                do k = 1, n_levels
                    do j = 1, n_retrieved_levels
                        matrix_sk(i, j) = matrix_sk(i, j) + matrix_s(i, k) * matrix_t(k, retrieval_level_bottom + j - 1)
                    end do
                end do
            end do

            ! Calculate K * matrix_s * Kt matrix
            do i = 1, n_retrieved_levels
                do j = 1, n_retrieved_levels
                    matrix_ssk(i, j) = 0d0

                    do k = 1, n_levels
                        matrix_ssk(i, j) = matrix_ssk(i, j) + matrix_t(k, retrieval_level_bottom + i - 1) * &
                            matrix_sk(k, j)
                    end do
                end do
            end do

            trace_kskt = 0d0

            do j = 1, n_retrieved_levels
                trace_kskt = trace_kskt + matrix_ssk(j, j)
            end do

            write(*, '(1X, "Trace of matrix KSKt:", ES12.4, /)') trace_kskt

            ! Calculate Matrix matrix_inv
            do j = 1, n_retrieved_levels
                matrix_inv(j, j) = rad_noise(j) ** 2d0
                matrix_inv_diag(j) = matrix_inv(j, j)
            end do

            do i = 1, n_retrieved_levels
                do j = 1, n_retrieved_levels
                    if(i /= j) then
                        matrix_inv(i, j) = 0d0
                    end if

                    matrix_inv(i, j) = matrix_inv(i, j) + matrix_ssk(i, j)
                end do
            end do

            ! Invert Matrix matrix_inv
            call matinv(matrix_inv(:, :), n_retrieved_levels)

            ! Calculate final matrices and variation of temperature
            do i = 1, n_levels
                do j = 1, n_retrieved_levels
                    matrix_r(i, j) = 0d0

                    do k = 1, n_retrieved_levels
                        matrix_r(i, j) = matrix_r(i, j) + matrix_sk(i, k) * matrix_inv(k, j)
                    end do
                end do
            end do

            do i = n_levels, 1, -1
                dt(i) = 0d0

                do j = 1, n_retrieved_levels
                    dt(i) = dt(i) + matrix_r(i, j) * rad_diff(j)
                end do

                temperatures(i) = temperatures(i) + dt(i)

                if (temperatures(i) < 0d0) then
                    write(*, '("lvl", T5, "p (Pa)", T15, "T (K)", T25, "dT")')

                    do j = n_levels, i, -1
                        write(*, '(I3, ES10.3, F10.3, F10.3)') &
                            j, pressures(j) * 1d2, temperatures(j), dt(j)
                    end do

                    write(*, '("Error: negative temperature in retrieval", /, &
                        &"You can try to (non-exhaustive list):", /, &
                        &"1. use a temperature profile a-priori closer to your target profile", /, &
                        &"2. relax the constraints on the retrieval flux error (retrieval_flux_error_bottom/top)", /, &
                        &"3. decrease the weight of a-priori (weight_apriori)", /, &
                        &"4. increase the iteration interval between thermochemical and/or cloud calculations", /, &
                        &"5. run a simulation with an intermediate set of parameter, and use it as input (see 1.)")')
                    stop
                end if
            end do

            ! Shorten the time in non adiabatic mode if the temperature profile has converged enough
            temperature_variation = maxval(abs(dt(:)) / temperatures(:))

            if(solution_deviation <= retrieval_tolerance .and. iter < n_non_adiabatic_iterations .and. &
                   temperature_variation <= retrieval_tolerance) then
                n_non_adiabatic_iterations = iter + 1
            else if(solution_deviation <= retrieval_tolerance .and. iter > n_non_adiabatic_iterations .and. &
                   temperature_variation <= retrieval_tolerance) then
                retrieval_converged = .True.
            end if

            ! Avoid thermal inversion radiosity_internal(n_levels)/radiosity_internal_target
            if(iter <= 10) then
                do i = 2, n_levels
                    if(temperatures(i) > 1.05d0 * temperatures(i - 1)) then
                        write(*, '("Warning: limiting temperature variation at level ", I0, ": ", &
                                    &F0.3, " -> ", F0.3, " K")') &
                              i, temperatures(i), 1.05d0 * temperatures(i - 1)
                    end if

                    temperatures(i) = min(temperatures(i), 1.05d0 * temperatures(i - 1))
                end do
            end if

            ! Update temperature in layers
            do j = 1, n_layers
                temperatures_layers(j) = sqrt(temperatures(j) * temperatures(j + 1))
            end do
        end subroutine temperature_profile_retrieval

        subroutine write_hdf5_output()
            use hdf5
            use h5_interface, only: write_h5_dataset_1d_dp, write_h5_dataset_2d_dp, write_h5_dataset_1d_int, &
                write_h5_dataset_0d_dp, write_h5_string, write_h5_dataset_0d_int
            use atmosphere, only: z
            use cloud, only: cloud_mode, sedimentation_parameter
            use species, only: n_cia, cia_names
            use target, only: target_mass, target_equatorial_radius, target_flattening

            implicit none

            character(len=*), parameter :: &
                group_level_name = 'levels', &
                group_layer_name = 'layers', &
                group_quality_name = 'run_quality', &
                group_spectra_name = 'spectra', &
                subgroup_cloud_name = 'clouds', &
                subgroup_emission_name = 'emission', &
                subgroup_flux_name = 'flux', &
                subgroup_transmission_name = 'transmission', &
                subgroup_vmr_name = 'volume_mixing_ratios', &
                supergroup_model_name = 'model_parameters', &
                supergroup_output_name = 'outputs'

            character(len=file_name_size) :: &
                str_tmp

            integer(HID_T) :: file_id, group_id, sub_group_id, sub_sub_group_id, super_group_id
            integer :: i, j, error

            doubleprecision :: c_p_layer_tmp(n_layers), lay_tmp(n_layers), rad_noise_tmp(n_levels)
            doubleprecision :: array_2d_dump(1, 1)

            array_2d_dump(:, :) = 0d0

            do i = 1, n_layers
                c_p_layer_tmp(i) = c_p_level(i + 1)
            end do

            write(*, '("Saving outputs in file ''", A, "''")') trim(hdf5_file_out)

            call h5open_f(error)
            call h5fcreate_f(hdf5_file_out, H5F_ACC_TRUNC_F, file_id, error)

            ! Outputs
            call h5gcreate_f(file_id, supergroup_output_name, super_group_id, error)
            call h5gopen_f(file_id, supergroup_output_name, super_group_id, error)

            ! Run quality
            call h5gcreate_f(super_group_id, group_quality_name, group_id, error)
            call h5gopen_f(super_group_id, group_quality_name, group_id, error)

            call write_h5_dataset_0d_dp(&
                hdf5_file_out, 'radiosity_actual_target_ratio', &
                radiosity_internal(n_levels) / radiosity_internal_target(n_levels), 'units', 'None', group_id &
            )
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, 'actual_internal_temperature', &
                sign((abs(radiosity_internal(n_levels)) / (cst_sigma * 1d3)) ** (1d0/4d0), &
                radiosity_internal(n_levels)), 'units', 'K', group_id &
            )
            call write_h5_dataset_0d_dp(hdf5_file_out, 'chi2_retrieval', chi2_0, 'units', 'None', group_id)
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, 'relative_delta_chi2_retrieval', (chi2_0 - chi2_1) / chi2_1, 'units', 'None', group_id &
            )
            call write_h5_dataset_1d_dp(hdf5_file_out, 'delta_temperature_layers', dt, 'units', 'K', group_id)

            ! Levels
            call h5gcreate_f(super_group_id, group_level_name, group_id, error)
            call h5gopen_f(super_group_id, group_level_name, group_id, error)

            call write_h5_dataset_1d_dp(hdf5_file_out, 'pressure', pressures * 1d2, 'units', 'Pa', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'temperature', temperatures, 'units', 'K', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'temperature_adiabatic', tad, 'units', 'K', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'temperature_uncertainty', errt, 'units', 'K', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'temperature_uncertainty_b', errtb, 'units', 'K', group_id)
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'delta_temperature_convection', dt_conv, 'units', 'K', group_id &
            )
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'radiosity_internal', radiosity_internal * 1d-3, 'units', 'W.m2', group_id &
            )
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'radiosity_convective', flux_conv * 1d-3, 'units', 'W.m2', group_id &
            )
            call write_h5_dataset_1d_int(hdf5_file_out, 'is_convective', is_convective, 'units', 'None', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'gradiant_temperature', grada, 'units', 'None', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'altitude', z * 1d3, 'units', 'm', group_id)
            call write_h5_dataset_2d_dp(hdf5_file_out, 'kernel_temperature', matrix_t, 'units', 'K', group_id)

            rad_noise_tmp(:) = 0d0
            do i = retrieval_level_bottom, retrieval_level_top
                rad_noise_tmp(i) = rad_noise(i - retrieval_level_bottom + 1)
            end do

            call write_h5_dataset_1d_dp(hdf5_file_out, 'radiosity_error', rad_noise_tmp, 'units', 'W.m-2', group_id)

            call h5gclose_f(group_id, error)

            ! Layers
            call h5gcreate_f(super_group_id, group_layer_name, group_id, error)
            call h5gopen_f(super_group_id, group_layer_name, group_id, error)

            call write_h5_dataset_1d_dp(hdf5_file_out, 'pressure', pressures_layers * 1d2, 'units', 'Pa', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'temperature', temperatures_layers, 'units', 'K', group_id)
            call write_h5_dataset_1d_dp(hdf5_file_out, 'gravity', gravities_layers * 1d-2, 'units', 'm.s-2', group_id)
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'molar_mass', molar_masses_layers(1:n_layers) * 1d-3, 'units', 'kg.mol-1', group_id &
            )
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'eddy_diffusion_coefficient', eddy_diffusion_coefficient, &
                'units', 'cm2.s-1', group_id &
            )
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'isobaric_molar_heat_capacity', c_p_layer_tmp, 'units', 'J.K-1.mol-1', group_id &
            )
            ! Layers/VMR
            call h5gcreate_f(group_id, subgroup_vmr_name, sub_group_id, error)
            call h5gopen_f(group_id, subgroup_vmr_name, sub_group_id, error)

            do i = 1, n_species
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, species_names(i), species_vmr_layers(:, i), &
                    'units', 'None', sub_group_id, 'absorbers' &
                )
            end do

            do j = 1, n_gases
                do i = 1, n_species
                    if(gases_names(j) == species_names(i)) then

                        exit
                    else if(i == n_species) then
                        lay_tmp = gases_vmr(j, :)

                        call write_h5_dataset_1d_dp(&
                            hdf5_file_out, gases_names(j), lay_tmp, &
                            'units', 'None', sub_group_id, 'gases' &
                        )
                    end if
                end do
            end do

            do i = 1, n_elements
                if(element_is_in_gases_list(i)) then
                    lay_tmp = gas_element_vmr(i, :)

                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, elements_symbol(i), lay_tmp, &
                        'units', 'None', sub_group_id, 'elements_gas_phase' &
                    )
                end if
            end do

            call h5gclose_f(sub_group_id, error)

            ! Layers/condensates
            call h5gcreate_f(group_id, 'condensates', sub_group_id, error)
            call h5gopen_f(group_id, 'condensates', sub_group_id, error)

            do i = 1, n_condensates
                lay_tmp = vmr_sat_condensates(i, :)

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, condensate_names(i), lay_tmp, &
                    'units', 'None', sub_group_id, 'volume_mixing_ratio_saturation' &
                )
                call write_h5_dataset_0d_dp(&
                    hdf5_file_out, condensate_names(i), p_c_condensates(i) * 1d5, &  ! bar to Pa
                    'units', 'Pa', sub_group_id, 'pressure_condensation' &
                )
                call write_h5_dataset_0d_dp(&
                    hdf5_file_out, condensate_names(i), vmr_c_condensates(i), &
                    'units', 'None', sub_group_id, 'volume_mixing_ratio_condensation' &
                )
            end do

            call h5gclose_f(sub_group_id, error)

            ! Layers/clouds
            call h5gcreate_f(group_id, subgroup_cloud_name, sub_group_id, error)
            call h5gopen_f(group_id, subgroup_cloud_name, sub_group_id, error)

            do i = 1, n_clouds
                lay_tmp = tau_cloud(i, :)

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, cloud_names(i), lay_tmp, &
                    'units', 'None', sub_group_id, 'opacity' &
                )

                lay_tmp = cloud_particle_radius(i, :)

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, cloud_names(i), lay_tmp, &
                    'units', 'm', sub_group_id, 'particle_radius' &
                )

                lay_tmp = cloud_vmr(i, :)

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, cloud_names(i), lay_tmp, &
                    'units', 'm', sub_group_id, 'volume_mixing_ratio' &
                )
            end do

            call h5gclose_f(sub_group_id, error)
            call h5gclose_f(group_id, error)

            ! Spectra
            call h5gcreate_f(super_group_id, group_spectra_name, group_id, error)
            call h5gopen_f(super_group_id, group_spectra_name, group_id, error)

            call write_h5_dataset_1d_dp(hdf5_file_out, 'wavenumber', wavenumbers, 'units', 'cm-1', group_id)

            ! Spectra/flux
            call h5gcreate_f(group_id, subgroup_flux_name, sub_group_id, error)
            call h5gopen_f(group_id, subgroup_flux_name, sub_group_id, error)

            if(output_fluxes) then
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'spectral_flux', &
                    flux(n_levels, :) * 1d-3, &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'spectral_flux_clear', &
                    flux_clear(n_levels, :) * 1d-3, &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_group_id &
                )

                if(n_clouds > 0) then
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        'spectral_flux_cloud', &
                        flux_cloud(n_levels, :) * 1d-3, &
                        'units', &
                        'W.m-2/cm-1', &
                        sub_group_id &
                    )
                else
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        'spectral_flux_cloud', &
                        [0d0], &
                        'units', &
                        'W.m-2/cm-1', &
                        sub_group_id &
                    )
                end if
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'spectral_flux', &
                    [0d0], &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'spectral_flux_clear', &
                    [0d0], &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'spectral_flux_cloud', &
                    [0d0], &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_group_id &
                )
            end if

            call h5gclose_f(sub_group_id, error)

            ! Spectra/emission
            call h5gcreate_f(group_id, subgroup_emission_name, sub_group_id, error)
            call h5gopen_f(group_id, subgroup_emission_name, sub_group_id, error)

            call write_h5_dataset_1d_dp(&
                hdf5_file_out, &
                'spectral_radiosity', &
                spectral_radiosity(n_levels, :) * 1d-3, &
                'units', &
                'W.m-2/cm-1', &
                sub_group_id &
            )

            call write_h5_dataset_1d_dp(&
                hdf5_file_out, &
                'spectral_radiosity_clear', &
                spectral_radiosity_clear(n_levels, :) * 1d-3, &
                'units', &
                'W.m-2/cm-1', &
                sub_group_id &
            )

            call write_h5_dataset_1d_dp(&
                hdf5_file_out, &
                'spectral_radiosity_cloud', &
                spectral_radiosity_cloud(n_levels, :) * 1d-3, &
                'units', &
                'W.m-2/cm-1', &
                sub_group_id &
            )

            call h5gcreate_f(sub_group_id, 'contributions', sub_sub_group_id, error)
            call h5gopen_f(sub_group_id, 'contributions', sub_sub_group_id, error)

            if(output_thermal_spectral_contribution) then
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'thermal', &
                    spectral_radiosity_thermal * 1d-3, &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_sub_group_id &
                )
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'thermal', &
                    [0d0], &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_sub_group_id &
                )
            end if

            if(output_cia_spectral_contribution) then
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'cia_rayleigh', &
                    spectral_radiosity_cia * 1d-3, &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_sub_group_id &
                )
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'cia_rayleigh', &
                    [0d0], &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_sub_group_id &
                )
            end if

            if(output_species_spectral_contributions) then
                if(n_clouds > 0) then
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        'clouds', &
                        spectral_radiosity_clouds * 1d-3, &
                        'units', &
                        'W.m-2/cm-1', &
                        sub_sub_group_id &
                    )
                else
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        'clouds', &
                        [0d0], &
                        'units', &
                        'W.m-2/cm-1', &
                        sub_sub_group_id &
                    )
                end if

                do i = 1, n_species
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        trim(species_names(i)), &
                        spectral_radiosity_species(:, i) * 1d-3, &
                        'units', &
                        'W.m-2/cm-1', &
                        sub_sub_group_id &
                    )
                end do
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'clouds', &
                    [0d0], &
                    'units', &
                    'W.m-2/cm-1', &
                    sub_sub_group_id &
                )

                do i = 1, n_species
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        trim(species_names(i)), &
                        [0d0], &
                        'units', &
                        'W.m-2/cm-1', &
                        sub_sub_group_id &
                    )
                end do
            end if

            call h5gclose_f(sub_sub_group_id, error)
            call h5gclose_f(sub_group_id, error)

            ! Spectra/transmission
            call h5gcreate_f(group_id, subgroup_transmission_name, sub_group_id, error)
            call h5gopen_f(group_id, subgroup_transmission_name, sub_group_id, error)

            if(output_transmission_spectra) then
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'transit_depth', &
                    (spectral_radius / light_source_radius) ** 2, &
                    'units', &
                    'None', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'transit_depth_full_cover', &
                    (spectral_radius_full_cover / light_source_radius) ** 2, &
                    'units', &
                    'None', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'transit_depth_clear', &
                    (spectral_radius_clear / light_source_radius) ** 2, &
                    'units', &
                    'None', &
                    sub_group_id &
                )

                call write_h5_dataset_2d_dp(&
                    hdf5_file_out, &
                    'derivative', &
                    (spectral_radius_derivative / light_source_radius) ** 2, &
                    'units', &
                    'None', &
                    sub_group_id &
                )
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'transit_depth', &
                    [0d0], &
                    'units', &
                    'None', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'transit_depth_full_cover', &
                    [0d0], &
                    'units', &
                    'None', &
                    sub_group_id &
                )

                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'transit_depth_clear', &
                    [0d0], &
                    'units', &
                    'None', &
                    sub_group_id &
                )

                call write_h5_dataset_2d_dp(&
                    hdf5_file_out, &
                    'derivative', &
                    array_2d_dump, &
                    'units', &
                    'None', &
                    sub_group_id &
                )
            end if

            call h5gcreate_f(sub_group_id, 'contributions', sub_sub_group_id, error)
            call h5gopen_f(sub_group_id, 'contributions', sub_sub_group_id, error)

            if(output_transmission_spectra .and. output_species_spectral_contributions .and. n_clouds > 0) then
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'clouds', &
                    (spectral_radius_clouds / light_source_radius) ** 2, &
                    'units', &
                    'None', &
                    sub_sub_group_id &
                )
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'clouds', &
                    [0d0], &
                    'units', &
                    'None', &
                    sub_sub_group_id &
                )
            end if

            if(output_cia_spectral_contribution .and. output_transmission_spectra) then
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'cia_rayleigh', &
                    (spectral_radius_cia / light_source_radius) ** 2, &
                    'units', &
                    'None', &
                    sub_sub_group_id &
                )
            else
                call write_h5_dataset_1d_dp(&
                    hdf5_file_out, &
                    'cia_rayleigh', &
                    [0d0], &
                    'units', &
                    'None', &
                    sub_sub_group_id &
                )
            end if

            if(output_species_spectral_contributions .and. output_transmission_spectra) then
                do i = 1, n_species
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        trim(species_names(i)), &
                        (spectral_radius_species(:, i) / light_source_radius) ** 2, &
                        'units', &
                        'None', &
                        sub_sub_group_id &
                    )
                end do
            else
                do i = 1, n_species
                    call write_h5_dataset_1d_dp(&
                        hdf5_file_out, &
                        trim(species_names(i)), &
                        [0d0], &
                        'units', &
                        'None', &
                        sub_sub_group_id &
                    )
                end do
            end if

            call h5gclose_f(sub_sub_group_id, error)
            call h5gclose_f(sub_group_id, error)
            call h5gclose_f(group_id, error)
            call h5gclose_f(super_group_id, error)

            ! Model
            call h5gcreate_f(file_id, supergroup_model_name, super_group_id, error)
            call h5gopen_f(file_id, supergroup_model_name, super_group_id, error)

            ! Target
            call h5gcreate_f(super_group_id, 'target', group_id, error)
            call h5gopen_f(super_group_id, 'target', group_id, error)

            call write_h5_dataset_0d_dp(hdf5_file_out, 'mass', target_mass, 'units', 'kg', group_id)
            call write_h5_dataset_0d_dp(hdf5_file_out, 'latitude', latitude, 'units', 'degree', group_id)
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, 'internal_temperature', target_internal_temperature, 'units', 'K', group_id &
            )
            call write_h5_dataset_0d_dp(hdf5_file_out, 'radius_1e5Pa', target_radius * 1d3, 'units', 'm', group_id)
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, &
                'equatorial_radius_1e5Pa', &
                target_equatorial_radius * 1d3, &
                'units', &
                'm', &
                group_id &
            )
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, &
                'polar_radius_1e5Pa', &
                target_equatorial_radius * (1d0 - target_flattening) * 1d3, &
                'units', &
                'm', &
                group_id &
            )

            call h5gclose_f(group_id, error)

            call h5gcreate_f(super_group_id, 'light_source', group_id, error)
            call h5gopen_f(super_group_id, 'light_source', group_id, error)

            call write_h5_dataset_0d_dp(hdf5_file_out, 'radius', light_source_radius, 'units', 'm', group_id)
            call write_h5_dataset_0d_dp(hdf5_file_out, 'range', light_source_range, 'units', 'm', group_id)
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, &
                'effective_temperature', &
                light_source_effective_temperature, &
                'units', &
                'K', &
                group_id &
            )
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, &
                'spectral_irradiance', &
                light_source_irradiance, &
                'units', &
                'W.m-2/cm-1', &
                group_id &
            )

            call h5gclose_f(group_id, error)

            call h5gcreate_f(super_group_id, 'atmosphere', group_id, error)
            call h5gopen_f(super_group_id, 'atmosphere', group_id, error)

            call write_h5_string(hdf5_file_out, 'eddy_mode', eddy_mode, group_id)

            call h5gclose_f(group_id, error)

            call h5gcreate_f(super_group_id, 'species', group_id, error)
            call h5gopen_f(super_group_id, 'species', group_id, error)

            do i = 1, n_elements
                if(element_is_in_gases_list(i)) then
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, &
                        elements_symbol(i), &
                        elemental_h_ratio(i), &
                        'units', &
                        'None', &
                        group_id, &
                        'elemental_abundances' &
                    )
                end if
            end do

            do i = 1, n_elements
                if(element_is_in_gases_list(i)) then
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, &
                        elements_symbol(i), &
                        solar_h_ratio(i), &
                        'units', &
                        'None', &
                        group_id, &
                        'solar_elemental_abundances' &
                    )
                end if
            end do

            call h5gcreate_f(group_id, 'absorber_is_at_equilibrium', sub_group_id, error)
            call h5gopen_f(group_id, 'absorber_is_at_equilibrium', sub_group_id, error)

            do i = 1, n_species
                write(str_tmp, *) species_at_equilibrium(i)
                call write_h5_string(hdf5_file_out, trim(species_names(i)), trim(str_tmp), sub_group_id)
            end do

            call h5gclose_f(sub_group_id, error)

            str_tmp = ''

            if(n_cia > 0) then
                write(str_tmp, *) trim(cia_names(1))

                if(n_cia > 1) then
                    do i = 2, n_cia
                        write(str_tmp, *) trim(str_tmp) // ', ' // trim(cia_names(i))
                    end do
                end if
            end if

            call write_h5_string(hdf5_file_out, 'collision_induced_absorptions', trim(str_tmp), group_id)

            call h5gclose_f(group_id, error)

            call h5gcreate_f(super_group_id, 'clouds', group_id, error)
            call h5gopen_f(super_group_id, 'clouds', group_id, error)

            call write_h5_string(hdf5_file_out, 'mode', cloud_mode, group_id)
            call write_h5_dataset_0d_dp(hdf5_file_out, 'fraction', cloud_fraction, 'units', 'None', group_id)

            if(cloud_mode == 'fixedRadius') then
                do i = 1, n_clouds
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, trim(cloud_names(i)), cloud_particle_radius(i, 1), 'units', 'm', &
                        group_id, 'particle_radius' &
                    )
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, trim(cloud_names(i)), 0d0, 'units', 'None', &
                        group_id, 'sedimentation_parameter' &
                    )
                end do
            else if(cloud_mode == 'fixedSedimentation' .or. cloud_mode == 'fixedRadiusCondensation') then
                do i = 1, n_clouds
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, trim(cloud_names(i)), 0d0, 'units', 'm', &
                        group_id, 'particle_radius' &
                    )
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, trim(cloud_names(i)), sedimentation_parameter(i), 'units', 'None', &
                        group_id, 'sedimentation_parameter' &
                    )
                end do
            else
                do i = 1, n_clouds
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, trim(cloud_names(i)), 0d0, 'units', 'm', &
                        group_id, 'particle_radius' &
                    )
                    call write_h5_dataset_0d_dp(&
                        hdf5_file_out, trim(cloud_names(i)), 0d0, 'units', 'None', &
                        group_id, 'sedimentation_parameter' &
                    )
                end do
            end if

            do i = 1, n_clouds
                call write_h5_dataset_0d_dp(&
                    hdf5_file_out, trim(cloud_names(i)), cloud_particle_density(i), 'units', 'kg.m-3', &
                    group_id, 'particle_density' &
                )
                call write_h5_dataset_0d_dp(&
                    hdf5_file_out, trim(cloud_names(i)), reference_wavenumber(i), 'units', 'cm-1', &
                    group_id, 'reference_wavenumber' &
                )
            end do

            call h5gclose_f(group_id, error)

            ! Retrieval
            call h5gcreate_f(super_group_id, 'retrieval', group_id, error)
            call h5gopen_f(super_group_id, 'retrieval', group_id, error)

            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'pressure_apriori', pressures_apriori * 1d2, 'units', 'Pa', group_id&
            )
            call write_h5_dataset_1d_dp(&
                hdf5_file_out, 'temperature_apriori', temperatures_apriori, 'units', 'K', group_id&
            )

            call write_h5_dataset_0d_int(&
                hdf5_file_out, 'level_bottom', retrieval_level_bottom, '', '', group_id&
            )
            call write_h5_dataset_0d_int(&
                hdf5_file_out, 'level_top', retrieval_level_top, '', '', group_id&
            )
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, 'flux_error_bottom', retrieval_flux_error_bottom, 'units', 'None', group_id&
            )
            call write_h5_dataset_0d_dp(&
                hdf5_file_out, 'flux_error_top', retrieval_flux_error_top, 'units', 'None', group_id&
            )
            call write_h5_dataset_0d_int(hdf5_file_out, 'n_iterations', n_iterations, '', '', group_id)
            call write_h5_dataset_0d_int(hdf5_file_out, 'n_burn_iterations', n_burn_iterations, '', '', group_id)
            call write_h5_dataset_0d_int(&
                hdf5_file_out, 'n_non_adiabatic_iterations', n_non_adiabatic_iterations, '', '', group_id&
            )
            call write_h5_dataset_0d_int(&
                hdf5_file_out, 'chemistry_iteration_interval', chemistry_iteration_interval, '', '', group_id&
            )
            call write_h5_dataset_0d_int(&
                hdf5_file_out, 'cloud_iteration_interval', cloud_iteration_interval, '', '', group_id&
            )
            call write_h5_dataset_0d_dp(hdf5_file_out, 'tolerance', retrieval_tolerance, '', '', group_id)
            call write_h5_dataset_0d_dp(hdf5_file_out, 'smoothing_bottom', smoothing_bottom, '', '', group_id)
            call write_h5_dataset_0d_dp(hdf5_file_out, 'smoothing_top', smoothing_top, '', '', group_id)
            call write_h5_dataset_0d_dp(hdf5_file_out, 'weight_apriori', weight_apriori, '', '', group_id)

            call h5gclose_f(group_id, error)
            call h5gclose_f(super_group_id, error)

            call h5close_f(error)
        end subroutine write_hdf5_output

        subroutine write_matrix_k()
            implicit none

            integer :: i, j, file_unit

            write(*, '("Saving matrix K in file ''", A,"''")') trim(kernel_file_out)

            open(newunit=file_unit, file=kernel_file_out, status='unknown')

            write(file_unit, '("# pressure kernel")')

            write(file_unit, '("# Pa K")')

            do i = 1, n_levels
                write(file_unit, *) pressures(i) * 1d2, (matrix_t(i, j), j = 1, n_levels)
            end do

            close(file_unit)
        end subroutine write_matrix_k

        subroutine write_outputs()
            implicit none

            if(output_full .and. output_species_spectral_contributions) then
                call write_matrix_k()

                call write_transmission_spectrum_derivative()
            endif

            call write_spectrum()

            ! Writing inverted temperature profile with error bar
            call write_temperature_profile()

            ! Printing the mole fractions in the atmospheric layers
            call write_vmr_profiles()
        end subroutine write_outputs

        subroutine write_spectrum()
            implicit none

            character(len=n_species * 64) :: species_list, unit_list
            integer :: i, k, file_unit

            species_list = 'spectral_radiosity_' // trim(species_names(1))
            unit_list = 'W.m-2/cm-1'

            if (n_species > 1) then
                do i = 2, n_species
                    species_list = &
                        trim(species_list) // ' ' // 'spectral_radiosity_' // trim(species_names(i))
                    unit_list = trim(unit_list) // ' ' // 'W.m-2/cm-1'
                end do
            end if

            do i = 1, n_species
                species_list = &
                    trim(species_list) // ' ' // 'transit_depth_' // trim(species_names(i))
                unit_list = trim(unit_list) // ' ' // 'None'
            end do

            write(*, '("Saving spectra in file ''", A,"''")') trim(spectrum_file_out)

            open(newunit=file_unit, file=spectrum_file_out, status='unknown')

            if(output_full .and. output_species_spectral_contributions) then
                write(file_unit, '("# wavenumber &
                    &spectral_flux &
                    &spectral_flux_clear &
                    &spectral_flux_full_cover &
                    &spectral_radiosity &
                    &spectral_radiosity_thermal &
                    &spectral_radiosity_cia_rayleigh ", &
                    &A, &
                    &" transit_depth_cia &
                    &transit_depth_clouds &
                    &transit_depth_clear &
                    &transit_depth_full_cover &
                    &transit_depth")') &
                trim(species_list)

                write(file_unit, '("# cm-1 &
                    &W.m-2/cm-1 &
                    &W.m-2/cm-1 &
                    &W.m-2/cm-1 &
                    &W.m-2/cm-1 &
                    &W.m-2/cm-1 &
                    &W.m-2/cm-1 ", &
                    &A, &
                    &" None &
                    &None &
                    &None &
                    &None &
                    &None")') &
                trim(unit_list)
            else
                write(file_unit, '("# wavenumber &
                    &spectral_flux &
                    &transit_depth")')

                write(file_unit, '("# cm-1 &
                    &W.m-2/cm-1 &
                    &None")')
            endif

            ! TODO native conversion from erg.s-1.cm-2/cm-1 to W.m-2/cm-1
            spectral_radiosity_species(:, :) = spectral_radiosity_species(:, :) * 1d-3
            spectral_radius_species(:, :) = (spectral_radius_species(:, :) / light_source_radius) ** 2

            if(output_full) then
                do i = 1, n_wavenumbers
                    write(file_unit, *) wavenumbers(i), &
                        flux(n_levels, i) * 1d-3, &
                        flux_clear(n_levels, i) * 1d-3, &
                        flux_cloud(n_levels, i) * 1d-3, &
                        spectral_radiosity(n_levels, i) * 1d-3, &
                        spectral_radiosity_thermal(i) * 1d-3, &
                        spectral_radiosity_cia(i) * 1d-3, &
                        (spectral_radiosity_species(i, k), k = 1, n_species), &
                        (spectral_radius_species(i, k), k = 1, n_species), &
                        (spectral_radius_cia(i) / light_source_radius) ** 2, &
                        (spectral_radius_clouds(i) / light_source_radius) ** 2, &
                        (spectral_radius_clear(i) / light_source_radius) ** 2, &
                        (spectral_radius_full_cover(i) / light_source_radius) ** 2, &
                        (spectral_radius(i) / light_source_radius) ** 2
                end do
            else
                do i = 1, n_wavenumbers
                    write(file_unit, *) wavenumbers(i), &
                        flux(n_levels, i) * 1d-3, &
                        (spectral_radius(i) / light_source_radius) ** 2
                end do
            end if

            close(file_unit)
        end subroutine write_spectrum

        subroutine write_temperature_profile()
            use atmosphere, only: z

            implicit none

            integer :: i, file_unit

            write(*, '("Saving temperature profile in file ''", A,"''")') trim(temperature_profile_file_out)

            open(newunit=file_unit, file=temperature_profile_file_out, status='unknown')

            write(file_unit, '("# pressure temperature &
                &temparature_adiabatic temperature_uncertainty temperature_uncertainty_b delta_temperature_convection &
                &radiosity_internal radiosity_convective is_convective grada &
                &altitude")')
            write(file_unit, '("# Pa K &
                &K K K K &
                &W.m-2 W.m-2 None None &
                &m", A)')

            do i = n_levels, 1, -1
                ! TODO native conversion from mbar to Pa and from erg.s-1.cm-2 to W.m-2
                write(file_unit, *) pressures(i) * 1d2, temperatures(i), tad(i), errt(i), errtb(i), dt_conv(i), &
                        radiosity_internal(i) * 1d-3, flux_conv(i) * 1d-3, is_convective(i), grada(i), &
                        z(i) * 1d3
            end do

            close(file_unit)
        end subroutine write_temperature_profile

        subroutine write_transmission_spectrum_derivative()
            implicit none

            integer :: i, file_unit

            write(*, '("Saving transmission spectrum contribution function in file ''", A,"''")') &
                trim(spectrum_derivative_file_out)

            open(newunit=file_unit, file=spectrum_derivative_file_out, status='unknown')

            write(file_unit, '("# wavenumber pressure derivative")')

            write(file_unit, '("# cm-1 Pa None")')

            write(file_unit, *) pressures(:) * 1d2

            spectral_radius_derivative(:, :) = (spectral_radius_derivative(:, :) / light_source_radius) ** 2

            do i = 1, n_wavenumbers
                write(file_unit, *) wavenumbers(i), (spectral_radius_derivative(j, i), j = 1, n_levels)
            end do

            close(file_unit)
        end subroutine write_transmission_spectrum_derivative

        subroutine write_vmr_profiles()
            implicit none

            integer :: vmr_out_size, vmr_out_size_used
            character(len=(n_gases + n_elements + n_condensates) * 64) :: species_list, unit_list
            character(len=n_clouds * 64 * 3) :: cloud_properties_list, unit_list_cloud
            integer :: i, j, k, file_unit, n_other_gases
            doubleprecision, dimension(:, :), allocatable :: vmr_out, gases_vmr_tmp

            vmr_out_size = n_gases + n_elements + n_condensates

            if(output_full) then
                vmr_out_size_used = vmr_out_size
            else
                vmr_out_size_used = n_species
            endif

            allocate(vmr_out(n_layers, vmr_out_size))

            vmr_out = 0d0

            ! Initialization
            species_list = 'volume_mixing_ratio_' // trim(species_names(1))
            unit_list = 'None'

            ! Line absorbers VMR
            if (n_species > 1) then
                do k = 2, n_species
                    species_list = &
                        trim(species_list) // ' ' // 'volume_mixing_ratio_' // trim(species_names(k))
                end do
            end if

            ! Other gases VMR
            if(output_full) then
                n_other_gases = n_gases - n_species
                allocate(gases_vmr_tmp(n_other_gases, n_layers))

                gases_vmr_tmp = 0d0

                if(n_other_gases > 0) then
                    i = 0

                    do k = 1, n_gases
                        do j = 1, n_species
                            if(gases_names(k) == species_names(j)) then

                                exit
                            else if(j == n_species) then
                                i = i + 1
                                species_list = &
                                    trim(species_list) // ' ' // 'other_volume_mixing_ratio_' // trim(gases_names(k))
                                gases_vmr_tmp(i, :) = gases_vmr(k, :)
                            end if
                        end do
                    end do
                end if

                ! Sum of gaseous element VMR
                do k = 1, n_elements
                    species_list = &
                        trim(species_list) // ' ' // 'sum_volume_mixing_ratio_' // trim(adjustl(elements_symbol(k)))
                end do

                ! Condensates saturation VMR
                do k = 1, n_condensates
                    species_list = &
                        trim(species_list) // ' ' // 'volume_mixing_ratio_saturation_' // &
                        trim(adjustl(condensate_names(k)))
                end do

                do k = 2, vmr_out_size
                    unit_list = trim(unit_list) // ' ' // 'None'
                end do
            else
                do k = 2, vmr_out_size_used
                    unit_list = trim(unit_list) // ' ' // 'None'
                end do
            end if !if(output_full)

            ! Clouds
            if (n_clouds > 0) then
                cloud_properties_list = 'cloud_opacity_' // trim(cloud_names(1))
                unit_list_cloud = 'None'

                if (n_clouds > 1) then
                    do k = 2, n_clouds
                        cloud_properties_list = &
                            trim(cloud_properties_list) // ' ' // 'cloud_opacity_' // trim(cloud_names(k))
                        unit_list_cloud = trim(unit_list_cloud) // ' ' // 'None'
                    end do

                    do k = 1, n_clouds
                        cloud_properties_list = &
                            trim(cloud_properties_list) // ' ' // 'cloud_particle_radius_' // trim(cloud_names(k))
                        unit_list_cloud = trim(unit_list_cloud) // ' ' // 'm'
                    end do

                    do k = 1, n_clouds
                        cloud_properties_list = &
                            trim(cloud_properties_list) // ' ' // 'cloud_vmr_' // trim(cloud_names(k))
                        unit_list_cloud = trim(unit_list_cloud) // ' ' // 'None'
                    end do
                else
                    cloud_properties_list = trim(cloud_properties_list) // ' ' // 'cloud_particle_radius_' // &
                        trim(cloud_names(1))
                    unit_list_cloud = trim(unit_list_cloud) // ' ' // 'm'
                    cloud_properties_list = trim(cloud_properties_list) // ' ' // 'cloud_vmr_' // trim(cloud_names(1))
                    unit_list_cloud = trim(unit_list_cloud) // ' ' // 'None'
                end if
            end if

            ! Save
            write(*, '("Saving VMR in file ''", A,"''")') &
                trim(vmr_file_out)

            open(newunit=file_unit, file=vmr_file_out, status='unknown')

            write(file_unit, '("# pressure temperature eddy_diffusion_coefficient ", A, 1X, A)') &
                trim(species_list), trim(cloud_properties_list)
            write(file_unit, '("# Pa K m2.s-1 ", A, 1X, A)') &
                trim(unit_list), trim(unit_list_cloud)

            vmr_out(:, 1:n_species) = species_vmr_layers(:, 1:n_species)

            if(output_full) then
                vmr_out(:, n_species + 1:n_gases) = transpose(gases_vmr_tmp)
                vmr_out(:, n_gases + 1:n_gases + n_elements) = transpose(gas_element_vmr)
                vmr_out(:, n_gases + n_elements + 1:vmr_out_size) = transpose(vmr_sat_condensates)
            end if

            ! TODO native conversion from mbar to Pa
            do j = n_layers, 1, -1
                write(file_unit, *) pressures_layers(j) * 1d2, temperatures_layers(j), &
                    eddy_diffusion_coefficient(j), &
                    (vmr_out(j, k), k = 1, vmr_out_size_used), &
                    (tau_cloud(k, j), k = 1, n_clouds), &
                    (cloud_particle_radius(k, j), k = 1, n_clouds), &
                    (cloud_vmr(k, j), k = 1, n_clouds)
            end do

            close(file_unit)
        end subroutine write_vmr_profiles
end program exorem
