module exorem_interface
    use files, only: file_name_size
    use interface, only: add_light_source, kernel_file_out, light_source_spectrum_file, &
        output_cia_spectral_contribution, output_files_extension, output_files_suffix, &
        output_species_spectral_contributions, output_thermal_spectral_contribution, output_full, &
        path_absorption_cross_sections, path_cia, path_clouds, path_data, path_k_coefficients, &
        path_light_source_spectra, path_lines, path_outputs, path_temperature_profile, &
        spectrum_derivative_file_out, spectrum_file_out, spectrum_file_prefix, &
        temperature_profile_file, temperature_profile_file_out, temperature_profile_file_prefix, vmr_file_out, &
        vmr_file_prefix, reallocate_1Ddouble, reallocate_2Ddouble, read_data_file, read_thermochemical_table, &
        load_kzz_profile, load_cloud_profiles
    use atmosphere, only: eddy_diffusion_coefficient, eddy_mode, h2_vmr, he_vmr, scale_height, metallicity, n_levels, &
        pressure_min, pressure_max, z_vmr, n_layers, gravities_layers, pressures, temperatures, &
        pressures_layers, temperatures_layers, molar_masses_layers, check_eddy_mode
    use chemistry, only: n_gases, n_condensates, n_thermochemical_data, gases_names, condensate_names, &
        condensates_delta_g, gases_c_p, gases_delta_g, temperatures_thermochemistry, &
        gases_delta_g_i, condensates_delta_g_i
    use cloud, only: n_clouds, cloud_mode, cloud_particle_radius, sedimentation_parameter, &
        cloud_particle_density, reference_wavenumber, cloud_names, cloud_fraction, &
        cloud_opacity_files, check_cloud_mode, supersaturation_parameter, sticking_efficiency
    use exorem_retrieval, only: retrieval_level_top, retrieval_level_bottom, retrieval_flux_error_top, &
        retrieval_flux_error_bottom, n_iterations, n_non_adiabatic_iterations, retrieval_tolerance, &
        smoothing_top, smoothing_bottom, weight_apriori, chemistry_iteration_interval, &
        cloud_iteration_interval, n_burn_iterations
    use light_source, only: light_source_radius, light_source_range, light_source_effective_temperature, &
        light_source_irradiation, incidence_angle, light_source_irradiance
    use math, only: deg2rad, convolve, interp, pi, prec_low, interp_ex_0d
    use physics, only: cst_G, cst_R, cst_sigma, n_elements, spherical_black_body_spectral_radiance
    use target, only: target_mass, target_equatorial_radius, target_radius, target_polar_radius, &
        target_gravity, target_flattening, latitude, target_internal_temperature, emission_angle, &
        target_equatorial_gravity
    use spectrometrics, only: wavenumber_max, wavenumber_min, wavenumber_step
    use species, only: allocate_species_primary_attributes, n_species, species_names, &
        species_at_equilibrium, n_cia, cia_names, cia_files, species_vmr_layers, elemental_h_ratio, elements_names

    implicit none

    character(len=*), parameter :: exorem_version = '2.2.0'

    logical :: &
        load_vmr_profiles, &
        output_fluxes, &
        output_hdf5, &
        output_transmission_spectra, &
        use_irradiation, &
        use_pressure_grid
        

    character(len=file_name_size) :: &
        hdf5_file_out, &
        vmr_profiles_file, &
        path_thermochemical_tables, &
        path_vmr_profiles

    save

    contains
        subroutine get_exorem_command_arguments(args)
            ! """
            ! Retrieve the Exo-REM arguments that were passed on the command line when it was invoked.
            ! :param args: array storing the arguments
            ! """
            implicit none

            character(len=file_name_size), dimension(:), intent(inout) :: args

            character(len=file_name_size) :: arg
            integer :: i, j, n_args

            j = 0
            n_args = size(args)

            args(:) = ''

            do i = 1, command_argument_count()
                call get_command_argument(i, arg)

                if(trim(arg) == '-v' .or. trim(arg) == '--version') then
                    write(*, '("Exo-REM ", A)') exorem_version

                    stop
                elseif(trim(arg) == '-h' .or. trim(arg) == '--help') then
                    call print_help()

                    stop
                else
                    j = j + 1
                end if

                if (len_trim(arg) == 0) then
                    if (j < n_args) then
                        write(*, '("Error: Exo-REM takes ", I0, " arguments but ", I0, " were given")') &
                            n_args, j
                        stop
                    end if

                    exit
                elseif (j > n_args) then
                    write(*, '("Error: Exo-REM takes ", I0, " arguments but ", I0, " were given")') &
                        n_args, j
                    stop
                end if

                if (j > 0) then
                    args(j) = arg
                end if
            end do

            contains
                subroutine print_help()
                    implicit none

                    character(len=*), parameter :: help_str = &
                        "Usage: ./exorem.exe [options] file..."//new_line('')//"&
                        &Example: ./exorem.exe ../inputs/example.nml"//new_line('')//"&
                        &Options: "//new_line('')//"&
                        &  --help         Display this information."//new_line('')//"&
                        &  --version      Display software version information."//new_line('')//"&
                        &"//new_line('')//"&
                        &For more information, read the README."//new_line('')//"&
                        &Gitlab of the software:"//new_line('')//"&
                        &<https://gitlab.obspm.fr/dblain/exorem>"

                    write(*, '(A)') help_str
                end subroutine print_help
        end subroutine get_exorem_command_arguments


        subroutine load_cloud_optical_constants(&
                file_sca, sig, radiusdyn, omeg, ep, gfactor)
            use files, only: file_name_size

            implicit none

            character(len=*), intent(in) :: file_sca

            double precision, allocatable, intent(out) :: &
                sig(:), omeg(:, :), ep(:, :), gfactor(:, :), radiusdyn(:)

            LOGICAL endwhile
            integer i
            integer :: nsize  ! optical properties (read in external ASCII files)
            integer :: nwvl ! Number of wavelengths in the domain (VIS or IR), read by master
            integer isize          ! Particle size index
            integer jfile, file_unit          ! ASCII file scan index

            character(len=file_name_size) scanline ! ASCII file scanning line

            doubleprecision, allocatable :: wvl(:)

            write(*, '("Reading cloud optical constants in file ''", A,"''")') trim(file_sca)

            !     1.2 Allocate the optical property table
            jfile = 1
            endwhile = .false.

            OPEN(newunit=file_unit, FILE = file_sca, status = 'old', form = 'formatted')

            read(file_unit, *)
            read(file_unit, *) nwvl
            read(file_unit, *)
            read(file_unit, *) nsize

            allocate(&
                sig(nwvl), &
                wvl(nwvl), &
                radiusdyn(nsize), &
                ep(nwvl, nsize), &
                gfactor(nwvl, nsize), &
                omeg(nwvl, nsize) &
            )

            do WHILE (.NOT.endwhile)
                READ(file_unit, *) scanline
                if ((scanline /= '#').and.(scanline /= ' ')) then
                    BACKSPACE(file_unit)
                    reading2_seq : SELECT CASE (jfile) ! ====================
                    CASE(1) reading2_seq      ! wvl -----------------------------
                        read(file_unit, *) wvl
                        jfile = jfile + 1
                    CASE(2) reading2_seq      ! radiusdyn -----------------------
                        read(file_unit, *) radiusdyn
                        jfile = jfile + 1
                    CASE(3) reading2_seq      ! ep ------------------------------
                        isize = 1
                        do WHILE (isize  <=  nsize)
                            READ(file_unit, *) scanline
                            if ((scanline /=  '#').and.(scanline /=  ' ')) then
                                BACKSPACE(file_unit)
                                read(file_unit, *) ep(:, isize)
                                isize = isize + 1
                            end if
                        end do
                        jfile = jfile + 1
                    CASE(4) reading2_seq      ! omeg ----------------------------
                        isize = 1
                        do WHILE (isize  <=  nsize)
                            READ(file_unit, *) scanline
                            if ((scanline /=  '#').and.(scanline /=  ' ')) then
                                BACKSPACE(file_unit)
                                read(file_unit, *) omeg(:, isize)
                                isize = isize + 1
                            end if
                        end do
                        jfile = jfile + 1
                    CASE(5) reading2_seq      ! gfactor -------------------------
                        isize = 1
                        do WHILE (isize  <=  nsize)
                            READ(file_unit, *) scanline
                            if ((scanline /=  '#').and.(scanline /=  ' ')) then
                                BACKSPACE(file_unit)
                                read(file_unit, *) gfactor(:, isize)
                                isize = isize + 1
                            end if
                        end do
                        jfile = jfile + 1
                        endwhile = .true.
                    CASE(6) reading2_seq
                        endwhile = .true.
                    CASE DEFAULT reading2_seq ! ----------------------------
                        WRITE(*, *) 'readoptprop: ', 'Error while loading optical properties.'
                        call ABORT
                    end SELECT reading2_seq   ! ==============================
                end if
            end do

            !     1.4 Close the file

            CLOSE(file_unit)

            ! Convert wavelengths (m) to wavenumbers (cm-1)
            do i = 1, nwvl
                sig(nwvl + 1 - i) = 1.0d-2 / wvl(i)
            end do
        end subroutine load_cloud_optical_constants


        subroutine load_temperature_profile_h5(file, pressure, temperature)
            use hdf5
            use h5_interface, only: load_h5_dataset_1d_dp

            implicit none

            integer :: error
            integer(HID_T) :: file_id, super_group_id, group_id

            character(len=*), intent(in) :: file
            doubleprecision, dimension(:), allocatable, intent(out) :: pressure, temperature

            call h5open_f(error)
            call h5fopen_f(file, H5F_ACC_RDONLY_F, file_id, error)
            call h5gopen_f(file_id, 'outputs', super_group_id, error)
            call h5gopen_f(super_group_id, 'levels', group_id, error)

            call load_h5_dataset_1d_dp(file, 'pressure', pressure, group_id)
            call load_h5_dataset_1d_dp(file, 'temperature', temperature, group_id)

            call h5gclose_f(group_id, error)
            call h5gclose_f(super_group_id, error)
            call h5fclose_f(file_id, error)
            call h5close_f(error)
        end subroutine load_temperature_profile_h5


        subroutine load_themochemical_tables()
            implicit none

            character(len=file_name_size), parameter :: path_gases = 'gases/', path_condensates = 'condensates/'
            character(len=file_name_size) :: file
            logical :: file_exists
            integer :: i, i_max, i_min, j, k, l
            doubleprecision, allocatable :: t_tmp(:), c_p_tmp(:), delta_g_tmp(:), arr_tmp(:)

            write(*, '("Loading thermochemical data...")')

            n_thermochemical_data = 67

            allocate(&
                temperatures_thermochemistry(n_thermochemical_data), &
                gases_c_p(n_gases, n_thermochemical_data), &
                gases_delta_g(n_gases, n_thermochemical_data), &
                condensates_delta_g(n_condensates, n_thermochemical_data), &
                gases_delta_g_i(n_gases), &
                condensates_delta_g_i(n_condensates) &
            )
            gases_delta_g_i(:) = 0d0
            condensates_delta_g_i(:) = 0d0

            do i = 1, 6
                temperatures_thermochemistry(i) = dble((i - 1) * 50)
            end do

            temperatures_thermochemistry(7) = 298.15d0

            do i = 8, 11
                temperatures_thermochemistry(i) = dble((i - 2) * 50)
            end do

            do i = 12, n_thermochemical_data
                temperatures_thermochemistry(i) = dble((i - 7) * 100)
            end do

            gases_c_p(:, :) = 0d0
            gases_delta_g(:, :) = 0d0
            condensates_delta_g(:, :) = 0d0

            ! Gases
            allocate(arr_tmp(n_gases))

            do i = 1, n_gases
                file = trim(path_thermochemical_tables) // &
                    trim(path_gases) // trim(gases_names(i)) // '.tct.dat'
                inquire(file=file, exist=file_exists)

                if(.not. file_exists) then
                    ! Exceptions
                    if(trim(gases_names(i)) == 'FeH') then
                        write(*, '("Info: file ''", A, "'' does not exists. &
                            &This is expected because the thermochemical data of this species are not used. &
                            &Assuming an isobaric molar heat capacity of ", ES11.4, " J.K-1.mol-1 &
                            &and a Gibb''s free energy of formation of 0 kJ.mol-1")') trim(file), 5d0/2d0 * cst_R

                            gases_c_p(i, :) = 5d0/2d0 * cst_R
                            gases_delta_g(i, :) = 0d0
                        cycle
                    else
                        write(*, '("Error: file ''", A, "'' does not exists.")') trim(file)

                        stop
                    end if
                end if

                call read_thermochemical_table(&
                    file, &
                    t_tmp, &
                    c_p_tmp, &
                    delta_g_tmp &
                )

                k = 0

                do j = 1, size(t_tmp)
                    i_max = 0
                    i_min = 0
                    k = k + 1

                    ! Check if temperature already exists in grid
                    if(t_tmp(j) < temperatures_thermochemistry(k) * (1d0 - prec_low) .or. &
                            t_tmp(j) > temperatures_thermochemistry(k) * (1d0 + prec_low)) then
                        do l = 1, n_thermochemical_data
                            if (j > 1) then  ! Find previous temperature in grid
                                 if (t_tmp(j - 1) >= temperatures_thermochemistry(l) * (1d0 - prec_low) .and. &
                                    t_tmp(j - 1) <= temperatures_thermochemistry(l) * (1d0 + prec_low)) then
                                        i_min = l
                                 else if (i_min == 0) then
                                     i_min = 1
                                 end if
                            else
                                i_min = 1
                            end if

                            if (t_tmp(j) >= temperatures_thermochemistry(l) * (1d0 - prec_low) .and. &
                                    t_tmp(j) <= temperatures_thermochemistry(l) * (1d0 + prec_low)) then
                                i_max = l

                                exit
                            else if (i_min > 0 .and. t_tmp(j) < temperatures_thermochemistry(l) * (1d0 - prec_low)) then
                                i_max = l

                                exit
                            end if
                        end do

                        if(i_min > 0 .and. i_max > 0) then
                            do l = i_min, i_max
                                gases_c_p(i, l) = interp_ex_0d(temperatures_thermochemistry(l), t_tmp, c_p_tmp)
                                gases_delta_g(i, l) = &
                                    interp_ex_0d(temperatures_thermochemistry(l), t_tmp, delta_g_tmp)
                            end do

                            k = i_max

                            cycle
                        end if
                    end if

                    ! Temperature was not found, add the new one to the grid
                    if(t_tmp(j) < temperatures_thermochemistry(k) * (1d0 - prec_low)) then
                        k = k - 1
                        cycle
                    else if(t_tmp(j) > temperatures_thermochemistry(k) * (1d0 + prec_low)) then
                        do l = k + 1, n_thermochemical_data
                            if (t_tmp(j) >= temperatures_thermochemistry(l) * (1d0 - prec_low) .and. &
                                    t_tmp(j) <= temperatures_thermochemistry(l) * (1d0 + prec_low)) then
                                gases_delta_g(i, l) = delta_g_tmp(j)
                                gases_c_p(i, l) = c_p_tmp(j)

                                k = l
                            end if
                        end do
                    else
                        gases_delta_g(i, k) = delta_g_tmp(j)
                        gases_c_p(i, k) = c_p_tmp(j)
                    end if
                end do

                if(k < n_thermochemical_data) then
                    do l = k, n_thermochemical_data
                        gases_c_p(i, l) = interp_ex_0d(temperatures_thermochemistry(l), t_tmp, c_p_tmp)
                        gases_delta_g(i, l) = &
                            interp_ex_0d(temperatures_thermochemistry(l), t_tmp, delta_g_tmp)
                    end do
                end if

                deallocate(&
                    t_tmp, &
                    c_p_tmp, &
                    delta_g_tmp &
                )
            end do

            ! Condensates
            deallocate(arr_tmp)
            allocate(arr_tmp(n_condensates))

            do i = 1, n_condensates
                file = trim(path_thermochemical_tables) // &
                    trim(path_condensates) // trim(condensate_names(i)) // '.tct.dat'
                inquire(file=file, exist=file_exists)

                if(.not. file_exists) then
                    ! Exceptions
                    if(trim(condensate_names(i)) == 'H2O' .or. &
                       trim(condensate_names(i)) == 'NH3' .or. &
                       trim(condensate_names(i)) == 'NH4SH' &
                        ) then
                        write(*, '("Info: file ''", A, "'' does not exists. &
                            &This is expected because the saturation pressures of this condensate are calculated &
                            &without using Gibb''s free energy of formation. &
                            &Assuming a Gibb''s free energy of formation of 0 kJ.mol-1")') trim(file)

                            condensates_delta_g(i, :) = 0d0
                        cycle
                    else
                        write(*, '("Error: file ''", A, "'' does not exists.")') trim(file)

                        stop
                    end if
                end if

                call read_thermochemical_table(&
                    file, &
                    t_tmp, &
                    c_p_tmp, &
                    delta_g_tmp &
                )

                k = 0

                do j = 1, size(t_tmp)
                    i_max = 0
                    i_min = 0
                    k = k + 1

                    ! Check if temperature already exists in grid
                    if(t_tmp(j) < temperatures_thermochemistry(k) * (1d0 - prec_low) .or. &
                            t_tmp(j) > temperatures_thermochemistry(k) * (1d0 + prec_low)) then
                        do l = 1, n_thermochemical_data
                            if (j > 1) then  ! Find previous temperature in grid
                                 if (t_tmp(j - 1) >= temperatures_thermochemistry(l) * (1d0 - prec_low) .and. &
                                    t_tmp(j - 1) <= temperatures_thermochemistry(l) * (1d0 + prec_low)) then
                                        i_min = l
                                 else if (i_min == 0) then
                                     i_min = 1
                                 end if
                            else
                                i_min = 1
                            end if

                            if (t_tmp(j) >= temperatures_thermochemistry(l) * (1d0 - prec_low) .and. &
                                    t_tmp(j) <= temperatures_thermochemistry(l) * (1d0 + prec_low)) then
                                i_max = l

                                exit
                            else if (i_min > 0 .and. t_tmp(j) < temperatures_thermochemistry(l) * (1d0 - prec_low)) then
                                i_max = l

                                exit
                            end if
                        end do

                        if(i_min > 0 .and. i_max > 0) then
                            do l = i_min, i_max
                                condensates_delta_g(i, l) = &
                                    interp_ex_0d(temperatures_thermochemistry(l), t_tmp, delta_g_tmp)
                            end do

                            k = i_max

                            cycle
                        end if
                    end if

                    ! Temperature was not found, add the new one to the grid
                    if(t_tmp(j) < temperatures_thermochemistry(k) * (1d0 - prec_low)) then
                        k = k - 1

                        cycle
                    else if(t_tmp(j) > temperatures_thermochemistry(k) * (1d0 + prec_low)) then
                        do l = k + 1, n_thermochemical_data
                            if (t_tmp(j) >= temperatures_thermochemistry(l) * (1d0 - prec_low) .and. &
                                    t_tmp(j) <= temperatures_thermochemistry(l) * (1d0 + prec_low)) then
                                condensates_delta_g(i, l) = delta_g_tmp(j)

                                k = l
                            end if
                        end do
                    else
                        condensates_delta_g(i, k) = delta_g_tmp(j)
                    end if
                end do

                if(k < n_thermochemical_data) then
                    do l = k, n_thermochemical_data
                        condensates_delta_g(i, l) = &
                            interp_ex_0d(temperatures_thermochemistry(l), t_tmp, delta_g_tmp)
                    end do
                end if

                deallocate(&
                    t_tmp, &
                    c_p_tmp, &
                    delta_g_tmp &
                )
            end do
        end subroutine load_themochemical_tables


        subroutine read_exorem_input_parameters()
            ! """
            ! Read the exorem input file.
            ! """
            use h5_interface, only: hdf5_files_extension

            implicit none

            integer, parameter :: &
                n_args = 1

            logical :: &
                use_atmospheric_metallicity, &
                use_flattening, &
                use_gravity, &
                use_light_source_spectrum, &
                use_metallicity, &
                use_elements_metallicity

            character(len=file_name_size) :: &
                input_file

            character(len=file_name_size), dimension(n_args) :: &
                args

            integer :: &
                file_unit, &
                i

            doubleprecision, dimension(n_elements) :: &
                elements_h_ratio, &
                elements_metallicity

            namelist/output_files/&
                spectrum_file_prefix, &
                temperature_profile_file_prefix, &
                vmr_file_prefix, &
                output_files_suffix

            namelist/target_parameters/&
                use_flattening, &
                use_gravity, &
                target_mass, &
                target_equatorial_gravity, &
                target_equatorial_radius, &
                target_polar_radius, &
                target_flattening, &
                latitude, &
                target_internal_temperature, &
                emission_angle

            namelist/light_source_parameters/&
                add_light_source, &
                use_irradiation, &
                use_light_source_spectrum, &
                light_source_radius, &
                light_source_range, &
                light_source_effective_temperature, &
                light_source_irradiation, &
                light_source_spectrum_file, &
                incidence_angle

            namelist/atmosphere_parameters/&
                use_metallicity, &
                use_pressure_grid, &
                h2_vmr, &
                he_vmr, &
                z_vmr, &
                metallicity, &
                n_levels, &
                n_species, &
                n_clouds, &
                n_cia, &
                eddy_mode, &
                eddy_diffusion_coefficient, &
                load_kzz_profile, &
                pressure_min, &
                pressure_max

            namelist/species_parameters/&
                use_elements_metallicity, &
                use_atmospheric_metallicity, &
                elements_names, &
                elements_h_ratio, &
                elements_metallicity, &
                species_names, &
                species_at_equilibrium, &
                cia_names, &
                load_vmr_profiles, &
                vmr_profiles_file

            namelist/spectrum_parameters/&
                wavenumber_min, &
                wavenumber_max, &
                wavenumber_step

            namelist/clouds_parameters/&
                cloud_mode, &
                cloud_fraction, &
                cloud_names, &
                cloud_particle_radius, &
                sedimentation_parameter, &
                supersaturation_parameter, &
                sticking_efficiency, &
                cloud_particle_density, &
                reference_wavenumber, &
                load_cloud_profiles

            namelist/retrieval_parameters/&
                temperature_profile_file, &
                retrieval_level_top, &
                retrieval_level_bottom, &
                retrieval_flux_error_top, &
                retrieval_flux_error_bottom, &
                n_iterations, &
                n_non_adiabatic_iterations, &
                chemistry_iteration_interval, &
                cloud_iteration_interval, &
                n_burn_iterations, &
                retrieval_tolerance, &
                smoothing_top, &
                smoothing_bottom, &
                weight_apriori

            namelist/options/&
                output_transmission_spectra, &
                output_species_spectral_contributions, &
                output_cia_spectral_contribution, &
                output_thermal_spectral_contribution, &
                output_fluxes, &
                output_hdf5, &
                output_full

            namelist/paths/&
                path_data, &
                path_cia, &
                path_clouds, &
                path_k_coefficients, &
                path_temperature_profile, &
                path_thermochemical_tables, &
                path_vmr_profiles, &
                path_light_source_spectra, &
                path_outputs

            call get_exorem_command_arguments(args)

            write(*, '("Exo-REM ", A, /, "____", /)') exorem_version

            input_file = trim(args(1))

            write(*, '("Reading parameters in file ''", A,"''")') trim(input_file)

            open(newunit=file_unit, file=input_file)

            elements_names(:) = ''
            allocate(eddy_diffusion_coefficient(1))

            read(file_unit, nml=output_files)
            read(file_unit, nml=target_parameters)

            read(file_unit, nml=light_source_parameters)

            read(file_unit, nml=atmosphere_parameters)

            call check_eddy_mode()

            allocate(cia_files(n_cia), cia_names(n_cia))
            allocate(&
                species_names(n_species), &
                species_at_equilibrium(n_species) &
            )
            allocate(&
                cloud_particle_radius(n_clouds, 1), &
                cloud_names(n_clouds), &
                cloud_opacity_files(n_clouds), &
                sedimentation_parameter(n_clouds), &
                supersaturation_parameter(n_clouds), &
                sticking_efficiency(n_clouds), &
                cloud_particle_density(n_clouds), &
                reference_wavenumber(n_clouds) &
            )

            read(file_unit, nml=species_parameters)

            call check_species()

            read(file_unit, nml=spectrum_parameters)

            if (n_clouds > 0) then
                read(file_unit, nml=clouds_parameters)

                call check_cloud_mode()
            end if

            read(file_unit, nml=retrieval_parameters)
            read(file_unit, nml=options)
            read(file_unit, nml=paths)

            close(file_unit)

            ! Files init
            if(output_hdf5)then
                hdf5_file_out = trim(path_outputs) // trim(output_files_suffix) // trim(hdf5_files_extension)
                kernel_file_out = trim(hdf5_file_out)
                spectrum_derivative_file_out = trim(hdf5_file_out)
                spectrum_file_out = trim(hdf5_file_out)
                temperature_profile_file_out = trim(hdf5_file_out)
                vmr_file_out = trim(hdf5_file_out)
            else
                kernel_file_out = trim(path_outputs) // &
                    trim('kernel') // '_' // trim(output_files_suffix) // trim(output_files_extension)
                spectrum_derivative_file_out = trim(path_outputs) // &
                    trim('d_spectrum') // '_' // trim(output_files_suffix) // trim(output_files_extension)
                spectrum_file_out = trim(path_outputs) // &
                    trim(spectrum_file_prefix) // '_' // trim(output_files_suffix) // trim(output_files_extension)
                temperature_profile_file_out = trim(path_outputs) // &
                    trim(temperature_profile_file_prefix) // '_' // trim(output_files_suffix) // &
                    trim(output_files_extension)
                vmr_file_out = trim(path_outputs) // &
                    trim(vmr_file_prefix) // '_' // trim(output_files_suffix) // trim(output_files_extension)
            end if

            temperature_profile_file = trim(path_temperature_profile) // trim(temperature_profile_file)

            vmr_profiles_file = trim(path_vmr_profiles) // trim(vmr_profiles_file)

            do i = 1, n_cia
                cia_files(i) = trim(path_cia) // trim(cia_names(i)) // '.cia.txt'
            end do

            do i = 1, n_clouds
                cloud_opacity_files(i) = trim(path_clouds) // trim(cloud_names(i)) // '.ocst.txt'
            end do

            ! Checks
            if(retrieval_level_top > n_levels) then
                write(*, '("Warning: highest retrieval level (", I0, ") is greater than &
                    &the number of atmospheric levels (", I0, "). Set retrieval_level_top = n_levels.")') &
                    retrieval_level_top, n_levels
                retrieval_level_top = n_levels
            end if

            if(retrieval_level_bottom < 2) then
                write(*, '("Warning: lowest retrieval level (", I0, ") is lower than &
                    &2. Set retrieval_level_bottom to 2.")') &
                    retrieval_level_bottom
                retrieval_level_bottom = 2
            end if

            if(n_non_adiabatic_iterations >= n_iterations .and. n_iterations > 0) then
                write(*, '("Warning: number of non-adiabatic iterations (", I0, ") is greater or equal to &
                    &the total number of iterations (", I0, "). Are you sure of what you are doing ?")') &
                    n_non_adiabatic_iterations, n_iterations
            end if

            ! Other inits
            if (.not. use_flattening) then
                target_flattening = (target_equatorial_radius - target_polar_radius) / target_equatorial_radius
            end if

            target_radius = target_equatorial_radius * (1d0 - target_flattening * sin(deg2rad(latitude)) ** 2d0)

            if (.not. use_gravity) then
                target_equatorial_gravity = target_mass * cst_G / target_equatorial_radius ** 2 * 1d2  ! m.s-2 to cm.s-2
            else
                target_equatorial_gravity = target_equatorial_gravity * 1d2  ! m.s-2 to cm.s-2
            end if

            target_gravity = target_equatorial_gravity * (target_equatorial_radius / target_radius) ** 2d0

            chemistry_iteration_interval = chemistry_iteration_interval + 1
            cloud_iteration_interval = cloud_iteration_interval + 1

            ! Unit conversion
            target_equatorial_radius = target_equatorial_radius * 1d-3  ! m to km
            target_radius = target_radius * 1d-3  ! m to km
            target_polar_radius = target_polar_radius * 1d-3  ! m to km
            pressure_min = pressure_min * 1d-2  ! Pa to mbar
            pressure_max = pressure_max * 1d-2  ! Pa to mbar

            if (add_light_source) then
                if(.not. use_irradiation) then
                    light_source_irradiation = cst_sigma * &
                        light_source_effective_temperature ** 4d0 * &
                        (light_source_radius / light_source_range) ** 2d0  ! (W.m-2)
                end if
            else
                light_source_irradiation = 0d0
            end if

            if (use_light_source_spectrum) then
                light_source_spectrum_file = trim(path_light_source_spectra) // trim(light_source_spectrum_file)
            else
                light_source_spectrum_file = 'None'
            end if

            call load_elemental_abundances()

            if (use_metallicity) then
                elemental_h_ratio(2:) = elemental_h_ratio(2:) * metallicity
                call get_solar_composition(elemental_h_ratio, h2_vmr, he_vmr, z_vmr)
            else
                call vmr2elemental_abundance()
                call get_solar_composition(elemental_h_ratio, h2_vmr, he_vmr, z_vmr)
            end if

            if (use_atmospheric_metallicity) then
                if (.not. use_metallicity) then
                    write(*, '("Using the following composition (VMR): &
                        &H2 = ", ES10.3, " He = ", ES10.3, " Z = ", ES10.3)') &
                        h2_vmr, he_vmr, z_vmr
                end if
            else if (use_elements_metallicity) then
                call metallicity2elemental_abundance()
            else
                call update_elemental_abundances()
            end if

            call get_solar_composition(elemental_h_ratio, h2_vmr, he_vmr, z_vmr)

            contains
                subroutine check_species
                    use chemistry, only: gases_names, n_gases, is_absorber

                    implicit none

                    integer :: i, ik

                    is_absorber = .False.

                    ! Check if input species is in database
                    do ik = 1, n_species
                        do i = 1, n_gases
                            if(trim(species_names(ik)) /= trim(gases_names(i))) then
                                if(i == n_gases) then
                                    write(*, '("Error: absorber ''", A, "'' is not implemented")') &
                                        trim(species_names(ik))
                                    stop
                                else
                                    is_absorber(i) = .True.
                                    cycle
                                end if
                            else
                                exit
                            end if
                        end do
                    end do
                end subroutine check_species

                subroutine load_elemental_abundances()
                    use species, only: solar_h_ratio

                    implicit none

                    character(len=file_name_size), dimension(:), allocatable :: &
                        labels, &
                        units

                    doubleprecision, dimension(:, :), allocatable :: &
                        columns

                    write(*, '("Loading standard elemental abundances in file ''", A, "''")') &
                        trim(trim(path_data) // 'solar_abundances.dat')

                    ! Read data file
                    call read_data_file(trim(path_data) // 'solar_abundances.dat', columns, labels, units)

                    ! Check file
                    if (labels(1) /= 'atomic_number') then
                        write(*, '("Error: file ''", A, "'': column 1 must have label ''atomic_number''")') &
                            trim(path_data) // 'solar_h_ratio.dat'
                        stop
                    end if

                     if (labels(2) /= 'elemental_abundance') then
                        write(*, '("Error: file ''", A, "'': column 2 must have label ''elemental_abundance''")') &
                            trim(path_data) // 'solar_h_ratio.dat'
                         stop
                    end if

                    ! Fill
                    elemental_h_ratio(int(columns(1, 1))) = columns(2, 1)  ! H abundance
                    elemental_h_ratio(2:) = log10(tiny(0d0)) + elemental_h_ratio(1)

                    ! Read lines
                    do i = 2, size(columns(1, :))
                        elemental_h_ratio(int(columns(1, i))) = columns(2, i)
                    end do

                    ! Calculate H ratio
                    elemental_h_ratio(:) = exp(elemental_h_ratio(:) * log(10d0)) / &
                        exp(elemental_h_ratio(1) * log(10d0))
                    solar_h_ratio(:) = elemental_h_ratio(:)
                end subroutine load_elemental_abundances

                subroutine get_solar_composition(standard_h_ratio, h2_vmr, he_vmr, z_vmr)
                    ! """
                    ! Get the ratio of H2, He and other elements from a given metallicity.
                    ! The elemental abundance of heavy elements was calculated as follows:
                    !   e_z = log10( sum_{X >= 3} exp(e_X * log(10)) )
                    ! Where X is the atomic number of the element, and e_X is given in Table 1 of Asplund et al. 2009.
                    ! Sources:
                    !   - Lodders 2010: https://arxiv.org/abs/1010.2746
                    ! :param h2_vmr: volume mixing ratio of H2
                    ! :param he_vmr: volume mixing ratio of He
                    ! :param z_vmr: volume mixing ratio of heavy elements
                    ! """
                    implicit none

                    doubleprecision, intent(in) :: standard_h_ratio(:)
                    doubleprecision, intent(out) :: h2_vmr, he_vmr, z_vmr

                    doubleprecision :: &
                        e_h, & ! elemental abundance of hydrogen in the solar photosphere
                        e_he, & ! elemental abundance of helium in the solar photosphere
                        e_z, & ! elemental abundance of heavy elements in the solar photosphere
                        solar_h2he_ratio

                    doubleprecision :: &
                        solar_h2z_ratio

                    e_h = standard_h_ratio(1)
                    e_he = standard_h_ratio(2)
                    e_z = 0d0

                    do i = 1, size(standard_h_ratio(3:))
                        e_z = e_z + standard_h_ratio(2 + i)
                    end do

                    if(e_he > 0d0) then
                        solar_h2he_ratio = e_h / 2d0 / e_he
                    else
                        solar_h2he_ratio = huge(0d0)
                    end if

                    if(e_z > 0d0) then
                        solar_h2z_ratio = e_h / 2d0 / e_z
                    else
                        solar_h2z_ratio = huge(0d0)
                    end if

                    h2_vmr = 1d0 / (1d0 + 1d0 / solar_h2he_ratio + 1d0 / solar_h2z_ratio)
                    he_vmr = 1d0 / (1d0 + solar_h2he_ratio + solar_h2he_ratio / solar_h2z_ratio)
                    z_vmr = 1d0 / (1d0 + solar_h2z_ratio + solar_h2z_ratio / solar_h2he_ratio)
                end subroutine get_solar_composition

                subroutine metallicity2elemental_abundance()
                    use species, only: solar_h_ratio
                    use physics, only: elements_symbol

                    implicit none

                    integer :: i, j

                    i = 0

                    do
                        i = i + 1

                        if (len(trim(elements_names(i))) == 0 .or. i > n_elements) then
                            exit
                        end if

                        do j = 1, n_elements
                            if (trim(elements_names(i)) == trim(elements_symbol(j))) then
                                elemental_h_ratio(j) = solar_h_ratio(j) * elements_metallicity(i)

                                exit
                            else if (j == n_elements) then
                                write(*, '("Error: element ''", A, "'' is not recognized")') elements_names(i)
                                stop
                            end if
                        end do
                    end do
                end subroutine metallicity2elemental_abundance

                subroutine vmr2elemental_abundance()
                    implicit none

                    integer :: i
                    doubleprecision :: z_1, z_0

                    elemental_h_ratio(2) = he_vmr / h2_vmr / 2d0
                    z_1 = z_vmr / h2_vmr / 2d0
                    z_0 = 0d0

                    do i = 1, size(elemental_h_ratio(3:))
                        z_0 = z_0 + elemental_h_ratio(2 + i)
                    end do

                    do i = 1, size(elemental_h_ratio(3:))
                        elemental_h_ratio(2 + i) = elemental_h_ratio(2 + i) * z_1 / z_0
                    end do
                end subroutine vmr2elemental_abundance

                subroutine update_elemental_abundances()
                    use physics, only: elements_symbol

                    implicit none

                    integer :: i, j

                    i = 0

                    do
                        i = i + 1

                        if (len(trim(elements_names(i))) == 0 .or. i > n_elements) then
                            exit
                        end if

                        do j = 1, n_elements
                            if (trim(elements_names(i)) == trim(elements_symbol(j))) then
                                elemental_h_ratio(j) = elements_h_ratio(i)

                                exit
                            else if (j == n_elements) then
                                write(*, '("Error: element ''", A, "'' is not recognized")') elements_names(i)
                                stop
                            end if
                        end do
                    end do

                end subroutine update_elemental_abundances
        end subroutine read_exorem_input_parameters
end module exorem_interface
