module chemistry
    ! """
    ! Contains useful chemical functions and constants.
    ! All the gibbs free energies of formation come from the JANAF database (https://janaf.nist.gov/) unless
    ! stated otherwise.
    ! Sources:
    !   - Gases and condensates selection:
    !       - Lodders 2010 https://arxiv.org/ftp/arxiv/papers/0910/0910.0811.pdf
    ! """
    use math, only: prec_high, prec_low, interp_ex, interp_ex_0d
    use physics, only: elements_symbol, n_elements, elements_molar_mass
    use species, only: species_name_size, n_species

    implicit none

    integer, parameter :: &
        n_condensates = 1,&!20, &
        n_gases = 4!43

    character(len=species_name_size), dimension(n_condensates), parameter :: &
        condensate_names = ['Fe']
        !condensate_names = [&
        !    'Al2O3  ', 'Ca     ', 'CaTiO3 ', 'Cr     ', 'Cr2O3  ', &
        !    'Fe     ', 'H2O    ', 'H3PO4  ', 'KCl    ', 'Mg2SiO4', &
        !    'MgSiO3 ', 'MnS    ', 'Na2S   ', 'NH3    ', 'NH4Cl  ', &
        !    'NH4SH  ', 'SiO2   ', 'TiN    ', 'VO     ', 'ZnS    ' &
        !]

    character(len=species_name_size), dimension(n_gases), parameter :: &
!        gases_names = [&
!            'Al     ', 'Ar     ', 'Ca     ', 'CH3    ', 'CH4    ', &
!            'CO     ', 'CO2    ', 'Cr     ', 'Fe     ', 'FeH    ', &
!            'H      ', 'H2     ', 'H2O    ', 'H2S    ', 'HCl    ', &
!            'HCN    ', 'He     ', 'K      ', 'KCl    ', 'Kr     ', &
!            'Mg     ', 'Mn     ', 'N2     ', 'Na     ', 'NaCl   ', &
!            'Ne     ', 'NH3    ', 'Ni     ', 'P      ', 'P2     ', &
!            'PH2    ', 'PH3    ', 'PO     ', 'SiH4   ', 'SiO    ', &
!            'Ti     ', 'TiO    ', 'TiO2   ', 'V      ', 'VO     ', &
!            'VO2    ', 'Xe     ', 'Zn     ' &
!        ]
            gases_names = [&
            'Fe     ', 'FeH    ', 'H      ', 'H2     ' &
        ]

    logical :: is_absorber(n_gases), is_condensed(n_condensates), is_quenched(n_gases), &
        element_is_in_gases_list(n_elements)

    integer :: n_thermochemical_data
    doubleprecision :: gases_molar_mass(n_gases), elements_in_gases(n_gases, n_elements)
    doubleprecision, allocatable :: condensates_delta_g(:, :), gases_c_p(:, :), gases_delta_g(:, :), &
        temperatures_thermochemistry(:), gases_delta_g_i(:), condensates_delta_g_i(:)

    save

    contains
        subroutine calculate_chemistry(&
            at_equilibrium, &
            gases_vmr, gas_element_abd, p_c_condensates, vmr_sat_condensates, vmr_c_condensates, layer_condensates &
        )
            ! """
            ! Calculate the thermochemical equilibirum.
            ! Given the chemical reaction aA + bB -> cC + dD, the chemical constant at equilibrium K_eq of this reaction
            ! is:
            !   K_eq = [C]**c * [D]**d / ([A]**a * [B]**b)
            ! where [X] = q_X is the volume mixing ratio of species X.
            ! We can also express this in term of partial pressure:
            !   K_p_eq = K_eq * p**(c + d - a - b) = exp(-Delta_G/RT)
            ! where p is the total pressure, T is the temperature, R is the molar gas constant and Delta_G is:
            !   Delta_G = c * g_C + d * g_D - a * g_A - b * g_B
            ! where g_X(T) is the standard Gibbs free energy of formation of species X.
            !
            ! Example: to calculate the vmr of NH3:
            !      N2 + 3H2 -> 2NH3
            !   => Delta_G = 2 * g_NH3 - 3 * g_H2 - g_N2
            !   => K_eq = exp(-(g_nh3 - 3 * g_h2 - g_n2) * 1e3 / (R * T)) / p**(2 - 3 - 1)
            !   => q_nh3**2 / (q_n2 * q_h2**3) = K_eq
            !   The factor 1e3 is to convert from kJ.mol-1 to J.mol-1.
            !   H2 and N2 are reference elements (g_h2 = g_n2 = 0), so we can write:
            !   K_eq = exp(-g_nh3 * 1e3 / (R * T)) / p**2 = q_nh3**2 / (q_n2 * q_h2**3)
            !   Moreover, if we assume that q_H2 does not vary, we can write:
            !      e_nh3 = K_eq * q_h2**3 = exp(-g_nh3) * 1e3 / (R * T)) * q_h2**3 / p**2
            !   => e_nh3 = q_nh3**2 / q_n2
            !
            ! Notes:
            ! - Layer/levels in model:
            !   ...
            !   p, T, g, k_zz, vmr    <- layer n + 1
            !   ---------plvl-------  <- level n + 1
            !   p, T, g, k_zz, vmr    <- layer n
            !   ---------plvl-------  <- level n
            !   ...
            !   p: pressures_layers, T: temperature, g: gravities_layers, k_zz: eddy diffusion coefficients,
            !   vmr: volume mixing ratio, plvl: level pressures_layers
            ! - given the reaction aA + bB -> sS
            !   if S is a solid, it does not contribute to the pressure anymore (p_S = 0), so K_eq can be written:
            !       K_eq = K_p_eq / p**(0 - a - b)
            ! - Al2O3, Ca CaTiO3 are a simplification from actual Ca and Al chemistry
            !   (see Lodders et al. 2002,
            !    http://solarsystem.wustl.edu/wp-content/uploads/reprints/2002/Lodders%202002%20ApJ.pdf)
            !   Ca is used only to remove Ca(g) at low temperatures. Ca(g) has probably already condensed into other
            !   species when Ca(cr,l) starts to become stable.
            ! - Fe-Ni alloys fomration is simulated by keeping constant the Ni(g)/Fe(g) ratio.
            ! - Quench levels for CH4/CO, NH3/N2 and CO2/CO calculated from
            !   Zahnle and Marley (2014): Eqs. 11-13, 32 and 44
            ! - If necessary, adiabatic extrpolation under the first level to determine the condensation levels of
            !   Mg2SiO4, MgSiO3, SiO2 and the quench levels
            !
            ! :param at_equilibrium: if True, the chemistry will be at equilibrium (no quenching)
            ! :return gases_vmr: VMR of the gases
            ! :return gas_element_abd: elemental abundances
            ! :return p_c_condensates: (mbar) pressure of condensation of condensates
            ! :return vmr_sat_condensates: saturation VMR of the condensates
            ! :return vmr_c_condensates: main condensing species VMR at condensation of the condensates
            ! :return layer_condensates: layer of condensation of the condensates
            ! """
            use atmosphere, only: scale_height, &
                pressures_layers, temperatures_layers, gravities_layers, eddy_diffusion_coefficient, pressures
            use species, only: elemental_h_ratio, solar_h_ratio
            use physics, only: cst_R

            implicit none

            logical, intent(in) :: at_equilibrium

            integer, intent(out), allocatable :: layer_condensates(:)

            doubleprecision, intent(out), allocatable :: &
                vmr_sat_condensates(:, :), vmr_c_condensates(:), p_c_condensates(:), &
                gas_element_abd(:, :), gases_vmr(:, :)

            integer, parameter :: i_max = 100

            character(len=*), parameter :: &
                condensation_fmt = '(1X, "Condensation of ", A, " in Layer ", I0, &
                        &": p = ", ES10.3, " bar, T = ", F0.3, " K, q = ", ES10.3)', &
                quench_fmt = '(1X, "Quench level ", A, ": p = ", ES10.3, " bar, T = ", F0.3, " K")'
            logical :: &
                co_ch4_quench, co_co2_quench, n2_nh3_quench, hcn_quench, &
                h2o_is_saturated

            integer :: i, ii, j, ip, ip_temp, nlay, gas_id_h2
            doubleprecision :: &
                alf, alf1, alf2, alpha, alpha1, &
                frac, &
                k_eq_ch3, k_eq_co, k_eq_h, k_eq_hcn, k_eq_co2, k_eq_tio, k_eq_tio2, &
                k_eq_kcl, k_eq_mg2sio4, k_eq_mgsio3, k_eq_sio2, k_eq_nh3, k_eq_nacl, k_eq_na2s, k_eq_nh4cl, &
                k_eq_kcl_solid, k_eq_sih4, k_eq_vo, k_eq_vo2, k_eq_vo_solid, &
                k_eq_catio3, k_eq_tin, &
                p, pc, pold, pbar, &
                t, tc, told, &
                qsath2o, &
                qh2oold, &
                pquenchc, pquenchco2, pquenchhcn, pquenchn, &
                tquenchc, tquenchco2, tquenchhcn, tquenchn, &
                qch4quenchhcn, qcoco2, qch4q, &
                tchemc, tchemc1, tchemco2, tchemco21, tchemhcn1, &
                tchemn1, tmix, tmix1, tmix11, tq1, tq2, x, &
                metallicity_c, metallicity_n, metallicity_o

            logical, allocatable :: &
                element_is_condensed(:)

            if (at_equilibrium) then
                write(*, '("Calculating thermochemistry at equilibirum...")')
            else
                write(*, '("Calculating non-equilibrium thermochemistry...")')
            end if

            call initialization()

            call init_non_equilibrium()

            !**		Loop on p-T layers (P and T in decreasing order)
            do ip = 1, nlay
                t = temperatures_layers(ip)
                p = pressures_layers(ip) * 1d-3
                pbar = pressures(ip) * 1d-3

                call interp_standard_gibbs_free_energy()

                ! Get the current gasesous elements abundance from the layer below
                if (ip > 1) then
                    gas_element_abd(:, ip) = gas_element_abd(:, ip - 1)
                    gases_vmr(:, ip) = gases_vmr(:, ip - 1)
                end if

                call calculate_time_constants()

                !**			H2 and H
                call calculate_h2_h_equilibrium()

                !**			H2O, CH4, CO and CO2
                !*			SiO and SiH4
                !call calculate_h2o_ch4_co_co2_sih4_sio_equilibirum()

                !**			NH3 & HCN
                !call calculate_nh3_n2_hcn_equilibrium()

                !                       Na & K
                !call calculate_cl_na_k_equilibrium()

                !**			Formation of NH4SH cloud
                !call nh4sh_condensation()

                ! Al2O3
                !call calculate_al_o_equilibrium()

                ! Cr
                !call calculate_cr_equilibrium()

                ! Fe Ni
                call calculate_fe_ni_co_equilibrium()

                ! TiO and VO
                !call calculate_ca_o_ti_v_equilibirum()

                ! Silicate clouds
                !call calculate_mg_si_o_equilibrium()

                ! MnS
                !call calculate_mn_s_equilibrium()

                ! ZnS
                !call calculate_zn_s_equilibrium()

                ! PH3 (no H3PO4 condensation)
                !call calculate_p_equilibrium()

                !call update_gases_vmr()

                !qh2oold = gases_vmr(gas_id('H2O'), ip)
            end do

            contains
                subroutine initialization()
                    implicit none

                    integer :: id

                    nlay = size(pressures_layers)
                    ip = 0
                    gas_id_h2 = gas_id('H2')

                    allocate(&
                        layer_condensates(n_condensates), &
                        vmr_sat_condensates(n_condensates, nlay), &
                        vmr_c_condensates(n_condensates), &
                        p_c_condensates(n_condensates), &
                        element_is_condensed(size(elemental_h_ratio)), &
                        gas_element_abd(size(elemental_h_ratio), nlay), &
                        gases_vmr(n_gases, nlay) &
                    )

                    layer_condensates(:) = -1
                    vmr_c_condensates(:) = 0d0
                    p_c_condensates(:) = 0d0
                    gas_element_abd(:, :) = tiny(0d0)
                    gases_vmr(:, :) = tiny(0d0)
                    vmr_sat_condensates(:, :) = 1d0

                    element_is_in_gases_list = .False.
                    is_condensed(:) = .False.
                    element_is_condensed(:) = .False.

                    gases_delta_g_i(:) = 0d0
                    condensates_delta_g_i(:) = 0d0

                    do i = 1, n_gases
                        do j = 1, n_elements
                            if (element_is_in_gases_list(j)) then
                                cycle
                            end if

                            id = count_element(trim(elements_symbol(j)), trim(gases_names(i)))

                            if (id > 0) then
                                element_is_in_gases_list(j) = .True.
                            end if
                        end do
                    end do

                    call init_gas_element_abd()

                    !qch4q = gas_element_abd(6, 1) * 0.45d0
                    !gases_vmr(gas_id('CO'), 1) = gas_element_abd(6, 1) * 0.45d0
                    !gases_vmr(gas_id('CO2'), 1) = gas_element_abd(6, 1) - (gases_vmr(gas_id('CO'), 1) + qch4q)
                    !gases_vmr(gas_id('NaCl'), 1) = min(gas_element_abd(11, 1),gas_element_abd(17, 1)) / 3d0
                    !gases_vmr(gas_id('HCl'), 1) = gas_element_abd(17, 1) - gases_vmr(gas_id('NaCl'), 1)

                    !qh2oold = gases_vmr(gas_id('H2O'), 1)

                    co_ch4_quench = .False.
                    co_co2_quench = .False.
                    n2_nh3_quench = .False.
                    hcn_quench = .False.

                    h2o_is_saturated = .False.

                    layer_condensates(:) = nlay + 1
                end subroutine initialization

                subroutine init_non_equilibrium()
                    implicit none

                    doubleprecision :: h

                    metallicity_c = elemental_h_ratio(6) / solar_h_ratio(6)
                    metallicity_n = elemental_h_ratio(7) / solar_h_ratio(7)
                    metallicity_o = elemental_h_ratio(8) / solar_h_ratio(8)

                    h = scale_height(1) * 1d5  ! km to cm
                    told = temperatures_layers(1) * 1.198d0
                    pold = 2d-3 * pressures_layers(1)
                    tq1 = 1.5d-6 * exp(4.2d4 / told) * (0.5 * (metallicity_c + metallicity_o))**(-0.70) / pold
                    tq2 = 4d1 * (0.5 * (metallicity_c + metallicity_o))**(-0.70) * exp(2.5d4 / told) / (pold * pold)

                    if (at_equilibrium) then
                        tmix = huge(0d0) / 1.4352d0
                    else
                        tmix = h ** 2d0 / eddy_diffusion_coefficient(1)
                    end if

                    tmix1 = 1.4352d0 * tmix
                    tmix11 = 1.4352d0 * tmix

                    tchemn1 = 1d-7 * exp(5.2d4 / told) / pold
                    tchemhcn1 = 1.5d-4 * exp(3.6d4 / told) / pold / (metallicity_n ** 0.35d0 * metallicity_c ** 0.35d0)
                    tchemco21 = 1.0d-10 * exp(3.8d4 / told) / sqrt(pold)
                    tchemc1 = 1d0 / (1d0 / tq1 + 1d0 / tq2)
                end subroutine init_non_equilibrium

                subroutine init_gas_element_abd()
                    ! """
                    ! Initialize the gaseous elements abundances.
                    ! Important: the abundance of element X is defined here as the sum of the gases VMR containing
                    ! element X. Consequently, the sum of the elemental abundances is not 1.
                    ! This subroutine relies heavily on approximations that are valid only in a restricted set of
                    ! abundances, hence it must be adapted for cases it was not build for.
                    ! Domain of validity:
                    !   - H-dominated atmospheres
                    !   - C, N and O are much more abundant than other elements (except H)
                    !   - H2O, CO and SiO are the major O-bearing species
                    ! Demonstration:
                    !   By definition:
                    !       sum(gases_vmr) = 1.     (1)
                    !       gas_element_abd_X = elemental_h_ratio * gas_element_abd_H       (2)
                    !   We set:
                    !       gas_element_abd_H = sum_i(n_i * gases_vmr_i),       (3)
                    !   were n_i is the number of H in gases_vmr_i.
                    !   We approximate the above by stating that some elements are contained only in one species.
                    !   We then calculate gas_element_abd_H using (1) and the approximation of (3), and proceed to
                    !   calculate the other gas_element_abd_X using (2).
                    ! """
                    implicit none

                    logical, dimension(n_gases) :: has_h
                    integer :: i, j, loc(1), n, n_unique_elements
                    doubleprecision :: gases_vmr_tmp(n_gases), sum_vmr_tmp_h, sum_vmr_tmp, sum_vmr_tmp_h_frac

                    ! Pre-initialize gases VMR
                    gases_vmr_tmp = 0d0
                    sum_vmr_tmp_h_frac = 0d0
                    has_H = .False.

                    do i = 1, n_gases
                        elements_in_gases(i, :) = count_all_elements(gases_names(i))
                        ! Get number of unique elements in species
                        n_unique_elements = 0

                        do j = 1, n_elements
                            if(elements_in_gases(i, j) > 0) then
                                n_unique_elements = n_unique_elements + 1
                            end if
                        end do

                        ! Handle species based on their number of unique elements
                        if(n_unique_elements == 1) then  ! 1-element species
                            loc = maxloc(elements_in_gases(i, 2:)) + 1

                            if (elements_in_gases(i, 1) > 0) then  ! pure H molecules are handled afterward
                                has_h(i) = .True.
                                cycle
                            else  ! other 1-element species
                                gases_vmr_tmp(gas_id(gases_names(i))) = &
                                    elemental_h_ratio(loc(1)) / elements_in_gases(i, loc(1))
                            end if
                        else if(n_unique_elements == 2)  then ! 2-element species
                            loc = maxloc(elements_in_gases(i, 2:)) + 1

                            if(elements_in_gases(i, 1) > 0) then  ! hydrides
                                has_h(i) = .True.
                                gases_vmr_tmp(gas_id(gases_names(i))) = &
                                    elemental_h_ratio(loc(1)) / elements_in_gases(i, loc(1))
                            else  ! other 2-element species: VMR is equal to the least abundant element
                                gases_vmr_tmp(gas_id(gases_names(i))) = huge(0d0)

                                do j = 1, n_elements
                                    if(elements_in_gases(i, j) > 0 .and. &
                                        elemental_h_ratio(j) < gases_vmr_tmp(gas_id(gases_names(i)))) then
                                        gases_vmr_tmp(gas_id(gases_names(i))) = &
                                            elemental_h_ratio(j) / elements_in_gases(i, j)
                                    end if
                                end do
                            end if
                        else  ! neglect complex molecules
                            gases_vmr_tmp(gas_id(gases_names(i))) = 0d0
                        end if
                    end do

                    ! Approximations
!                    gases_vmr_tmp(gas_id('CH3')) = 0d0  ! CO or CH4 are the only C-bearing species
!                    gases_vmr_tmp(gas_id('CO2')) = 0d0  ! CO or CH4 are the only C-bearing species
!                    gases_vmr_tmp(gas_id('FeH')) = 0d0  ! Fe is the only Fe-bearing species
!                    gases_vmr_tmp(gas_id('H')) = 0d0  ! H2 is the only H-only bearing species
!                    gases_vmr_tmp(gas_id('HCN')) = 0d0  ! no complex molecules
!                    gases_vmr_tmp(gas_id('KCl')) = 0d0  ! HCl is the only Cl-bearing species
!                    gases_vmr_tmp(gas_id('NH3')) = 0d0  ! N2 is the only N-bearing species
!                    gases_vmr_tmp(gas_id('NaCl')) = 0d0  ! HCl is the only Cl-bearing species
!                    gases_vmr_tmp(gas_id('P')) = 0d0  ! PH3 is the only P-bearing species
!                    gases_vmr_tmp(gas_id('P2')) = 0d0  ! PH3 is the only P-bearing species
!                    gases_vmr_tmp(gas_id('PH2')) = 0d0  ! PH3 is the only P-bearing species
!                    gases_vmr_tmp(gas_id('PO')) = 0d0  ! PH3 is the only P-bearing species
!                    gases_vmr_tmp(gas_id('SiH4')) = 0d0  ! SiO is the only Si-bearing species
!                    gases_vmr_tmp(gas_id('TiO')) = 0d0  ! Ti is the only Ti-bearing species
!                    gases_vmr_tmp(gas_id('TiO2')) = 0d0  ! Ti is the only Ti-bearing species
!                    gases_vmr_tmp(gas_id('VO')) = 0d0  ! V is the only V-bearing species
!                    gases_vmr_tmp(gas_id('VO2')) = 0d0  ! V is the only V-bearing species
!
!                    ! Handle C and O
!                    if(elemental_h_ratio(8) > elemental_h_ratio(6) + elemental_h_ratio(14)) then
!                        gases_vmr_tmp(gas_id('CH4')) = 0d0
!                        gases_vmr_tmp(gas_id('H2O')) = &
!                            elemental_h_ratio(8) - elemental_h_ratio(6) - elemental_h_ratio(14)
!                    else if (elemental_h_ratio(8) > elemental_h_ratio(14)) then
!                        gases_vmr_tmp(gas_id('CO')) = 0d0
!                        gases_vmr_tmp(gas_id('H2O')) = &
!                            elemental_h_ratio(8) - elemental_h_ratio(14)
!                    else
!                        write(* ,'("Error: chemistry: H2O initialized with a negative VMR; set C/H (CO) + Si/H (SiO) ratio&
!                            &are greater than O/H ratio (", ES10.3, " + ", ES10.3, " > ", ES10.3 ,")")')&
!                            elemental_h_ratio(6), elemental_h_ratio(14), elemental_h_ratio(8)
!                    end if

                    ! Get the sum of the VMR of all the H-bearing species and not H-bearing species
                    sum_vmr_tmp = 0d0
                    sum_vmr_tmp_h = 0d0
                    gases_vmr_tmp(gas_id('H2')) = 0d0

                    do i = 1, n_gases
                        if(.not. has_h(i)) then
                            sum_vmr_tmp = sum_vmr_tmp + gases_vmr_tmp(i)
                        else
                            sum_vmr_tmp_h = sum_vmr_tmp_h + gases_vmr_tmp(i)  ! sum of H-bearing molecules other than H2
                            n = count_element('H', gases_names(i))
                            sum_vmr_tmp_h_frac = (1d0 - n / 2d0) * gases_vmr_tmp(i)
                        end if
                    end do

                    ! Get H abundance
                    gas_element_abd(1, 1) = 1d0 / (0.5d0 + sum_vmr_tmp + sum_vmr_tmp_h_frac)

                    gases_vmr_tmp(gas_id('H2')) = 0.5d0 - sum_vmr_tmp_h

                    if(gases_vmr_tmp(gas_id('H2')) < 0d0) then
                        write(*, '("Error: chemistry: cannot handle atmospheres were the sum of the VMR of all &
                                &H-bearing molecules except H2 (", ES10.3,") is greater than the maximum possible &
                                &amount of H2 (", ES10.3, ")", /, &
                                &"Try to reduce the X/H ratio of elements such as O (H2O) or C (CH4), or reduce the &
                                &metallicity")') &
                                sum_vmr_tmp_h * gas_element_abd(1, 1), 0.5d0 * gas_element_abd(1, 1)
                        stop
                    end if

                    ! Calculate gases VMR
                    do i = 1, n_gases
                        gases_vmr(i, 1) = gases_vmr_tmp(i) * gas_element_abd(1, 1)
                    end do

                    call update_gas_element_abd()
                end subroutine init_gas_element_abd

                subroutine calculate_time_constants()
                    implicit none

                    doubleprecision :: gravity, h

                    gravity = gravities_layers(ip)
                    h = scale_height(ip) * 1d5  ! km to cm

                    if (at_equilibrium) then
                        tmix = huge(0d0) / 1.4352d0
                    else
                        tmix = h ** 2d0 / eddy_diffusion_coefficient(ip)
                    end if

                    tq1 = 1.5d-6 * exp(4.2d4 / t) * (0.5 * (metallicity_c + metallicity_o))**(-0.70) / p
                    tq2 = 4d1 * (0.5d0 * (metallicity_c + metallicity_o)) ** (-0.7d0) * exp(2.5d4 / t) / (p ** 2d0)
                    tchemC = 1d0 / (1d0 / tq1 + 1d0 / tq2)
                    tchemCO2 = 1d-10 * exp(3.8d4 / t) / sqrt(p)
                end subroutine calculate_time_constants

                subroutine calculate_h2_h_equilibrium()
                    ! """
                    ! Calculate the H2--H equilibrium.
                    ! All H-bearing species are taken into account.
                    ! Demonstration for vmr_H:
                    !       2 * H2 + H = sum_h    (1)
                    !
                    !       (H + 1/4 * k_eq_h)**2 = H**2 + 1/2 * k_eq_h + 1/16 * k_eq_h**2    (2)
                    !
                    !       H**2 / H2 = k_eq_h
                    !   =>  H**2 + 1/2 * k_eq_h = k_eq_h * sum_h / 2, from (1)
                    !   =>  (H + 1/4 * k_eq_h)**2 = k_eq_h * sum_h / 2 + 1/16 * k_eq_h**2, from (2)
                    !   =>  H = - 1/4 * k_eq_h + sqrt(k_eq_h * sum_h / 2 + 1/16 * k_eq_h**2)
                    ! """
                    implicit none

                    doubleprecision :: sum_h

                    ! H2 -> 2H
                    k_eq_h = k_eq(['H2', 'H '], [-1, 2])

                    sum_h = sum(elements_in_gases(:, 1) * gases_vmr(:, ip))
                    sum_h = sum_h - 2d0 * gases_vmr(gas_id('H2'), ip) - gases_vmr(gas_id('H'), ip)
                    sum_h = gas_element_abd(1, ip) - sum_h

                    if(sum_h < 0) then
                        sum_h = gas_element_abd(1, ip)
                    end if

                    gases_vmr(gas_id('H'), ip) = &
                        k_eq_h / 4d0 * (-1d0 + sqrt(1d0 + 8d0 / k_eq_h * sum_h))
                    print*, 'H1', gases_vmr(gas_id('H'), ip)
                    gases_vmr(gas_id('H'), ip) = &
                        -k_eq_h / 4d0 + sqrt(k_eq_h * sum_h / 2d0 + k_eq_h ** 2d0 / 16d0)
                     print*, 'H2', gases_vmr(gas_id('H'), ip), k_eq_h / 4d0 * (-1d0 + sqrt(1 + 8 / k_eq_h * sum_h))
                    gases_vmr(gas_id('H2'), ip) = gases_vmr(gas_id('H'), ip) ** 2d0 / k_eq_h
                end subroutine calculate_h2_h_equilibrium

                subroutine calculate_h2o_ch4_co_co2_sih4_sio_equilibirum()
                    implicit none

                    ! H2O + CH4 -> CO + 3H2
                    k_eq_co = k_eq(['H2O', 'CH4', 'CO ', 'H2 '], [-1, -1, 1, 3])

                    ! 2CH4 -> 2CH3 + H2
                    k_eq_ch3 = sqrt(k_eq(['CH4', 'CH3', 'H2 '], [-2, 2, 1]))

                    ! CO + H2O -> CO2 + H2
                    k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1])

                    ! SiH4 + H2O -> SiO + 3H2
                    k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3])

                    if (ip == 1) then
                        gases_vmr(gas_id('CH4'), ip) = qch4q / (1d0 + k_eq_ch3)  ! initialize qch4
                        gases_vmr(gas_id('CH3'), ip) = tiny(0d0)
                    end if

                    ! Update qsath2o
                    qsath2o = h2o_saturation_pressure(t) / p
                    call update_saturation_vmr('H2O', ip, qsath2o)

                    ! H2O condensation
                    if(gases_vmr(gas_id('H2O'), ip) > qsath2o) then
                        call h2o_condenstation()
                    end if

                    ! Equilibrium and quenching
                    if (tchemC <= tmix) then
                        ! Carbon chemistry time is lower than mixing time => thermochemical equilibrium
                        call osi(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                    else
                        if (.not. co_ch4_quench) then
                            call co_ch4_quenching()
                        end if

                        if(tchemCO2 <= tmix) then
                            if(co_ch4_quench) then
                                call osiqco(k_eq_co2, k_eq_sih4)
                            end if
                        else
                            if(.not. co_co2_quench) then
                                call co_co2_quenching()
                            end if

                            call osiqcoco2(k_eq_sih4)
                        end if
                    end if

                    call update_gases_vmr()

                    ! Update tchem
                    tchemC1 = tchemC
                    tchemCO21 = tchemCO2

                    ! Update
                    qh2oold = gases_vmr(gas_id('H2O'), ip)
                end subroutine calculate_h2o_ch4_co_co2_sih4_sio_equilibirum

                subroutine calculate_nh3_n2_hcn_equilibrium()
                    implicit none

                    doubleprecision :: tchemn, tchemhcn, ghcna_quench, gch4a_quench, gnh3_quench, qsat_nh3, sum_c

                    ! N2 + 3H2 -> 2NH3
                    k_eq_nh3 = k_eq(['N2 ', 'H2 ', 'NH3'], [-1, -3, 2])

                    ! 2CH4 + N2 -> 2HCN + 3H2
                    k_eq_hcn = k_eq(['CH4', 'N2 ', 'HCN', 'H2 '], [-2, -1, 2, 3]) * gases_vmr(gas_id('CH4'), ip) ** 2d0

                    tchemN = 1.0d-7 * exp(5.2d4 / t) / p

                    ! NH3 condensation
                    qsat_nh3 = nh3_saturation_pressure(t) / p

                    if(gases_vmr(gas_id('NH3'), ip) >= qsat_nh3) then
                        if (.not. is_condensed(condensate_id('NH3'))) then
                            call nh3_condenstation()
                        end if

                        call update_vmr('NH3', qsat_nh3)
                    end if

                    call update_saturation_vmr('NH3', ip, qsat_nh3)

                    ! N2/NH3
                    if(tchemN > tmix) then
                        if (.not. n2_nh3_quench) then
                            n2_nh3_quench = .True.

                            call get_pold_told(ip, pold, told)

                            x = log(tchemN1 / tmix1) / log(tchemN1 * tmix / (tchemN * tmix1))
                            tquenchN = told * (t / told)**x
                            pquenchN = pold * (p / pold)**x

                            write(*, quench_fmt) 'N2/NH3', pquenchN, tquenchN

                            gnh3_quench = &
                                interp_ex_0d(tquenchN, temperatures_thermochemistry, gases_delta_g(gas_id('NH3'), :))
                            k_eq_nh3 = &
                                exp(-2d0 * gnh3_quench * 1d3 / (cst_R * tquenchN)) * &
                                (gases_vmr(gas_id('H2'), ip)**3 * pquenchN * pquenchN)
                            tchemN = tmix
                        end if
                    end if

                    ! No else here so we can obtain the value of N2 and NH3 at the quench level and not below
                    if(tchemN <= tmix) then
                        if(.not. is_condensed(condensate_id('NH3'))) then
                            if(k_eq_nh3 < prec_high * gas_element_abd(7, ip)) then
                                gases_vmr(gas_id('N2'), ip) = 0.5d0 * gas_element_abd(7, ip)
                                gases_vmr(gas_id('NH3'), ip) = sqrt(k_eq_nh3 * gases_vmr(gas_id('N2'), ip))
                            else if(k_eq_nh3 > 1d6 * gas_element_abd(7, ip)) then
                                gases_vmr(gas_id('NH3'), ip) = gas_element_abd(7, ip)
                                gases_vmr(gas_id('N2'), ip) = gases_vmr(gas_id('NH3'), ip) ** 2 / k_eq_nh3
                            else
                                gases_vmr(gas_id('N2'), ip) = &
                                    0.5d0 * (gas_element_abd(7, ip) + 0.25d0 * k_eq_nh3 - &
                                    sqrt(0.25d0 * k_eq_nh3 * (0.25d0 * k_eq_nh3 + 2d0 * gas_element_abd(7, ip))))
                                gases_vmr(gas_id('NH3'), ip) = &
                                    gas_element_abd(7, ip) - 2d0 * gases_vmr(gas_id('N2'), ip)
                            end if
                        else
                            if(k_eq_nh3 < prec_high * gas_element_abd(7, ip)) then
                                gases_vmr(gas_id('N2'), ip) = 0.5d0 * gas_element_abd(7, ip)
                            else if(k_eq_nh3 > 1d6 * gas_element_abd(7, ip)) then
                                gases_vmr(gas_id('N2'), ip) = gases_vmr(gas_id('NH3'), ip) ** 2 / k_eq_nh3
                            else
                                gases_vmr(gas_id('N2'), ip) = &
                                    0.5d0 * (gas_element_abd(7, ip) + 0.25d0 * k_eq_nh3 - &
                                    sqrt(0.25d0 * k_eq_nh3 * (0.25d0 * k_eq_nh3 + 2d0 * gas_element_abd(7, ip))))
                            end if
                        end if
                    end if

                    tchemN1 = tchemN
                    tmix1 = tmix

                    ! HCN
                    tchemHCN = 1.5d-4 * exp(3.6d4 / t) / p / (metallicity_n**0.35 * metallicity_c**0.35)

                    if(tchemHCN > tmix .and. .not. hcn_quench) then
                        call get_pold_told(ip, pold, told)

                        x = log(tchemHCN1 / tmix11) / log(tchemHCN1 * tmix / &
                                (tchemHCN * tmix11))
                        tquenchHCN = told * (t / told)**x
                        pquenchHCN = pold * (p / pold)**x
                        qch4quenchHCN = gases_vmr(gas_id('CH4'), max(ip - 1, 1)) * &
                            (gases_vmr(gas_id('CH4'), ip) / gases_vmr(gas_id('CH4'), max(ip - 1, 1)))**x

                        write(*, quench_fmt) 'HCN', pquenchHCN, tquenchHCN

                        ghcna_quench = interp_ex_0d(tquenchHCN, temperatures_thermochemistry, &
                            gases_delta_g(gas_id('HCN'), :))
                        gch4a_quench = interp_ex_0d(tquenchHCN, temperatures_thermochemistry, &
                            gases_delta_g(gas_id('CH4'), :))

                        k_eq_hcn = &
                            exp(-2d0 * (ghcna_quench - gch4a_quench) * 1d3 / (cst_R * tquenchHCN)) &
                            * qch4quenchHCN * qch4quenchHCN / (gases_vmr(gas_id('H2'), ip)**3 * pquenchHCN * pquenchHCN)
                        tchemHCN = tmix
                        hcn_quench = .True.
                    end if

                    if(tchemHCN <= tmix) then
                        gases_vmr(gas_id('HCN'), ip) = sqrt(k_eq_hcn * gases_vmr(gas_id('N2'), ip))

                        sum_c = sum(elements_in_gases(:, 6) * gases_vmr(:, ip)) - gases_vmr(gas_id('HCN'), ip)
                        if(gases_vmr(gas_id('HCN'), ip) > 0.5d0 * sum_c) then  ! TODO better high HCN VMR handling
                            write(*, '("Warning: chemistry at level ", I0, " : HCN is the dominant C-bearing species. &
                                &This case is not handled by Exo-REM. &
                                &HCN VMR lowered from ", ES10.3, " to ", ES10.3, ". &
                                &**If you see this warning at the final iteration, your simulation is not valid.**")') &
                                ip, gases_vmr(gas_id('HCN'), ip), 1d-2 * sum_c
                            gases_vmr(gas_id('HCN'), ip) = 1d-2 * sum_c
                        end if
                    end if

                    tchemHCN1 = tchemHCN
                    tmix11 = tmix
                end subroutine calculate_nh3_n2_hcn_equilibrium

                subroutine calculate_cl_na_k_equilibrium()
                    implicit none

                    doubleprecision :: dq, dan, g(2, n_thermochemical_data), &
                        kcl_vmr_tmp, hcl_vmr_tmp, qhclsat, qnasat, qkclsat, &
                        qh2sold, qhclold, qkclold

                    ! Equilibrium constants
                    ! Na2S(s,l) + H2 -> 2Na + H2S
                    k_eq_na2s = exp((condensates_delta_g_i(condensate_id('Na2S')) - 2d0 * gases_delta_g_i(gas_id('Na')) - &
                        gases_delta_g_i(gas_id('H2S'))) * 1d3 / (cst_R * t)) * &
                        gases_vmr(gas_id('H2'), ip) / (p ** 2d0)
                    ! 2Na + 2HCl -> 2NaCl + H2
                    k_eq_nacl = sqrt(k_eq(['Na  ', 'HCl ', 'NaCl', 'H2  '], [-2, -2, 2, 1]))

                    ! 2K + 2HCl -> 2KCl + H2
                    k_eq_kcl = sqrt(k_eq(['K  ', 'HCl', 'KCl', 'H2 '], [-2, -2, 2, 1]))

                    ! KCl(s,l) -> KCl(g)
                    k_eq_kcl_solid = exp(-(gases_delta_g_i(gas_id('KCl')) &
                        - condensates_delta_g_i(condensate_id('KCl'))) * 1d3 / (cst_R * t)) / p
                    ! NH4Cl(s,l) -> NH3 + HCl
                    k_eq_nh4cl = exp(-&
                        (gases_delta_g_i(gas_id('NH3')) + &
                        gases_delta_g_i(gas_id('HCl')) - condensates_delta_g_i(condensate_id('NH4Cl'))) * &
                        1d3 / (cst_R * t)) / (p ** 2d0)

                    ! Initializations
                    if(ip > 1) then
                        qh2sold = gases_vmr(gas_id('H2S'), ip - 1)
                        qhclold = gases_vmr(gas_id('HCl'), ip - 1)
                        qkclold = gases_vmr(gas_id('KCl'), ip - 1)
                    else
                        qh2sold = gases_vmr(gas_id('H2S'), 1)
                        qhclold = gases_vmr(gas_id('HCl'), 1)
                        qkclold = gases_vmr(gas_id('KCl'), 1)
                    end if

                    qnasat = sqrt(k_eq_na2s / gases_vmr(gas_id('H2S'), ip))
                    qhclsat = 1d0

                    call update_saturation_vmr('Na2S', ip, qnasat)
                    call update_saturation_vmr('KCl', ip, k_eq_kcl_solid)

                    ! Na-S-Cl-K compunds gas phase equilibrium, check for condensations of KCl, Na2S and NH4Cl
                    ! Check for Na2S condensation
                    if(gases_vmr(gas_id('Na'), ip) <= qnasat) then
                        ! Gas phase equilibrium
                        gases_vmr(gas_id('Na'), ip) = &
                            gas_element_abd(11, ip) / &
                            (1d0 + k_eq_nacl * gases_vmr(gas_id('HCl'), ip))
                        gases_vmr(gas_id('K'), ip) = gas_element_abd(19, ip) / &
                            (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                        gases_vmr(gas_id('NaCl'), ip) = &
                            gases_vmr(gas_id('HCl'), ip) * gases_vmr(gas_id('Na'), ip) * k_eq_nacl
                        do i = 1, 10
                            if(gases_vmr(gas_id('NaCl'), ip) < gases_vmr(gas_id('HCl'), ip)) then
                                gases_vmr(gas_id('HCl'), ip) = gas_element_abd(17, ip) / &
                                    (1d0 + k_eq_nacl * gas_element_abd(11, ip) / &
                                     (1d0 + k_eq_nacl * gases_vmr(gas_id('HCl'), ip)) + &
                                     k_eq_kcl * gas_element_abd(19, ip) / &
                                     (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip)))
                                gases_vmr(gas_id('K'), ip) = gas_element_abd(19, ip) / &
                                    (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                                gases_vmr(gas_id('KCl'), ip) = &
                                    k_eq_kcl * gases_vmr(gas_id('K'), ip) * gases_vmr(gas_id('HCl'), ip)
                                gases_vmr(gas_id('Na'), ip) = &
                                    gas_element_abd(11, ip) / &
                                    (1d0 + k_eq_nacl * gases_vmr(gas_id('HCl'), ip))
                                gases_vmr(gas_id('NaCl'), ip) = &
                                    k_eq_nacl * gases_vmr(gas_id('Na'), ip) * gases_vmr(gas_id('HCl'), ip)
                            else
                                gases_vmr(gas_id('NaCl'), ip) = gas_element_abd(17, ip) / &
                                    (1d0 + (1d0 + k_eq_kcl * gases_vmr(gas_id('K'), ip)) / &
                                        (k_eq_nacl * gases_vmr(gas_id('Na'), ip)))
                                gases_vmr(gas_id('HCl'), ip) = &
                                    gas_element_abd(17, ip) - gases_vmr(gas_id('NaCl'), ip) * &
                                    (1d0 + k_eq_kcl * gases_vmr(gas_id('K'), ip) / &
                                    (k_eq_nacl * gases_vmr(gas_id('Na'), ip)))
                                gases_vmr(gas_id('Na'), ip) = &
                                    gas_element_abd(11, ip) / &
                                    (1d0 + k_eq_nacl * gases_vmr(gas_id('HCl'), ip))
                                gases_vmr(gas_id('K'), ip) = gas_element_abd(19, ip) / &
                                    (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                                gases_vmr(gas_id('KCl'), ip) = &
                                    k_eq_kcl * gases_vmr(gas_id('K'), ip) * gases_vmr(gas_id('HCl'), ip)
                            end if
                        end do
                    else  ! Na2S condensation
                        gases_vmr(gas_id('Na'), ip) = min(&
                            sqrt(k_eq_na2s / gases_vmr(gas_id('H2S'), ip)), &
                            gases_vmr(gas_id('Na'), max(ip - 1, 1))&
                        )

                        gases_vmr(gas_id('H2S'), ip) = gas_element_abd(16, ip) - 0.5d0 * &
                            (gases_vmr(gas_id('Na'), max(ip - 1, 1)) - gases_vmr(gas_id('Na'), ip))

                        ! Check for KCl condensation
                        qkclsat = k_eq_kcl_solid

                        if(gases_vmr(gas_id('KCl'), ip) <= qkclsat .and. .not. is_condensed(condensate_id('KCl'))) then
                            ! Gas phase equilibrium
                            if (k_eq_nacl * gases_vmr(gas_id('Na'), ip) > 1d0) then
                                do i = 1, 10
                                    gases_vmr(gas_id('Na'), ip) = &
                                        min(&
                                            sqrt(k_eq_na2s / gases_vmr(gas_id('H2S'), ip)), &
                                            gases_vmr(gas_id('Na'), max(ip - 1, 1))&
                                        )
                                    gases_vmr(gas_id('H2S'), ip) = &
                                        gas_element_abd(16, ip) - &
                                        0.5d0 * (gases_vmr(gas_id('Na'), max(ip - 1, 1)) - gases_vmr(gas_id('Na'), ip))
                                    gases_vmr(gas_id('HCl'), ip) = &
                                        gases_vmr(gas_id('NaCl'), ip) / (k_eq_nacl * gases_vmr(gas_id('Na'), ip))
                                    gases_vmr(gas_id('NaCl'), ip) = &
                                        gas_element_abd(17, ip) - gases_vmr(gas_id('HCl'), ip) - &
                                        gases_vmr(gas_id('KCl'), ip)
                                    gas_element_abd(11, ip) = &
                                        gases_vmr(gas_id('Na'), ip) + gases_vmr(gas_id('NaCl'), ip)
                                    gases_vmr(gas_id('KCl'), ip) = &
                                        gases_vmr(gas_id('HCl'), ip) * k_eq_kcl * gas_element_abd(19, ip) / &
                                        (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                                end do
                            else  ! cold atmospheres where [NaCl] > [HCl]
                                do i = 1, 10
                                    gases_vmr(gas_id('Na'), ip) = &
                                        min(&
                                            sqrt(k_eq_na2s / gases_vmr(gas_id('H2S'), ip)), &
                                            gases_vmr(gas_id('Na'), max(ip - 1, 1))&
                                        )
                                    gases_vmr(gas_id('H2S'), ip) = &
                                        gas_element_abd(16, ip) - &
                                        0.5d0 * (gases_vmr(gas_id('Na'), max(ip - 1, 1)) - gases_vmr(gas_id('Na'), ip))
                                    gases_vmr(gas_id('NaCl'), ip) = &
                                        k_eq_nacl * gases_vmr(gas_id('Na'), ip) * gases_vmr(gas_id('HCl'), ip)
                                    gases_vmr(gas_id('HCl'), ip) = &
                                        gas_element_abd(17, ip) - gases_vmr(gas_id('NaCl'), ip) - &
                                        gases_vmr(gas_id('KCl'), ip)
                                    gas_element_abd(11, ip) = &
                                        gases_vmr(gas_id('Na'), ip) + gases_vmr(gas_id('NaCl'), ip)
                                    gases_vmr(gas_id('KCl'), ip) = &
                                        gases_vmr(gas_id('HCl'), ip) * k_eq_kcl * gas_element_abd(19, ip) / &
                                        (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                                end do
                            end if

                            gases_vmr(gas_id('K'), ip) = &
                                gas_element_abd(19, ip) / (1d0 + k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                            qnasat = sqrt(k_eq_na2s / gases_vmr(gas_id('H2S'), ip))
                            call update_saturation_vmr('Na2S', ip, qnasat)
                        else
                            ! KCl condensation
                            if(gases_vmr(gas_id('KCl'), ip) > qkclsat .and. &
                                .not. is_condensed(condensate_id('KCl'))) then
                                kcl_vmr_tmp = gases_vmr(gas_id('KCl'), ip)

                                g(1, :) = condensates_delta_g(condensate_id('KCl'), :)
                                g(2, :) = gases_delta_g(gas_id('KCl'), :)

                                call simple_condensation(&
                                    'KCl', &
                                    gases_vmr(gas_id('KCl'), ip), &
                                    qkclold, &
                                    g, &
                                    [condensates_delta_g_i(condensate_id('KCl')), gases_delta_g_i(gas_id('KCl'))], &
                                    is_condensed(condensate_id('KCl'))&
                                )
                            end if

                            gases_vmr(gas_id('KCl'), ip) = min(k_eq_kcl_solid, qkclold)

                            ! Check for NH4Cl condensation
                            if(gases_vmr(gas_id('NH3'), ip) > tiny(0d0)) then
                                dq = 0.5d0 * (gases_vmr(gas_id('NH3'), ip) - qhclold)

                                if(k_eq_nh4cl < 1d-6 * dq ** 2d0 .and. dq > 0d0) then
                                    qhclsat = k_eq_nh4cl / (2d0 * dq)
                                else
                                    qhclsat = -dq + sqrt(k_eq_nh4cl + dq ** 2d0)
                                endif
                            else
                                qhclsat = 1d0
                            endif

                            if(gases_vmr(gas_id('HCl'), ip) <= qhclsat .and. &
                                .not. is_condensed(condensate_id('NH4Cl'))) then
                                ! Gas phase equilibrium
                                gases_vmr(gas_id('HCl'), ip) = qhclold

                                do i = 1, 10
                                    gases_vmr(gas_id('K'), ip) = &
                                        gases_vmr(gas_id('KCl'), ip) / &
                                        (k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                                    gases_vmr(gas_id('NaCl'), ip) = &
                                        (gas_element_abd(17, ip) - gas_element_abd(19, ip) + &
                                        gases_vmr(gas_id('K'), ip)) * &
                                        k_eq_nacl * gases_vmr(gas_id('Na'), ip) / &
                                            (1d0 + k_eq_nacl * gases_vmr(gas_id('Na'), ip))
                                    gases_vmr(gas_id('HCl'), ip) = &
                                        (gas_element_abd(17, ip) - &
                                         gas_element_abd(19, ip) + gases_vmr(gas_id('K'), ip)) / &
                                        (1d0 + k_eq_nacl * gases_vmr(gas_id('Na'), ip))
                                end do
                            else
                                ! NH4Cl condensation
                                hcl_vmr_tmp = gases_vmr(gas_id('HCl'), ip)

                                if(gases_vmr(gas_id('HCl'), ip) > qhclsat) then
                                    if (.not. is_condensed(condensate_id('NH4Cl'))) then
                                        write(*, condensation_fmt) &
                                            'NH4Cl', ip, p, t, gases_vmr(gas_id('HCl'), ip)

                                        call set_condensate_parameters('NH4Cl', ip, p, gases_vmr(gas_id('HCl'), ip))
                                    end if

                                    is_condensed(condensate_id('NH4Cl')) = .True.
                                end if

                                ! Gas phase equilibrium
                                gases_vmr(gas_id('HCl'), ip) = min(qhclsat, qhclold)

                                gases_vmr(gas_id('K'), ip) = &
                                    gases_vmr(gas_id('KCl'), ip) / &
                                    (k_eq_kcl * gases_vmr(gas_id('HCl'), ip))
                                gases_vmr(gas_id('NaCl'), ip) = &
                                    k_eq_nacl * gases_vmr(gas_id('Na'), ip) * gases_vmr(gas_id('HCl'), ip)

                                ! Update gas phase: NH4Cl
                                dan = &
                                    gas_element_abd(17, ip) - &
                                    (gases_vmr(gas_id('HCl'), ip) + gases_vmr(gas_id('NaCl'), ip) + &
                                    gases_vmr(gas_id('KCl'), ip)) - &
                                    gas_element_abd(19, ip) + gases_vmr(gas_id('K'), ip) + gases_vmr(gas_id('KCl'), ip)
                                gas_element_abd(1, ip) = gas_element_abd(1, ip) - 4d0 * dan
                                gas_element_abd(7, ip) = gas_element_abd(7, ip) - dan
                                gases_vmr(gas_id('NH3'), ip) = gases_vmr(gas_id('NH3'), ip) - dan
                            end if

                            ! Update gas phase: KCl
                            gas_element_abd(17, ip) = gases_vmr(gas_id('HCl'), ip) + gases_vmr(gas_id('NaCl'), ip) + &
                                    gases_vmr(gas_id('KCl'), ip)
                            gas_element_abd(19, ip) = gases_vmr(gas_id('K'), ip) + &
                                gases_vmr(gas_id('KCl'), ip)
                        end if

                        ! Update gas phase: Na2S
                        gas_element_abd(11, ip) = gases_vmr(gas_id('Na'), ip) + gases_vmr(gas_id('NaCl'), ip)
                        gas_element_abd(16, ip) = gas_element_abd(16, ip) - &
                                (qh2sold - gases_vmr(gas_id('H2S'), ip))
                    end if

                    call update_saturation_vmr('NH4Cl', ip, qhclsat)

                    ! Calculate Na2S pressure and temperature of condensation
                    if(gases_vmr(gas_id('Na'), max(ip - 1, 1)) > qnasat .and. .not. is_condensed(condensate_id('Na2S'))) then
                        call na2s_condensation(qnasat)
                    end if
                end subroutine calculate_cl_na_k_equilibrium

                subroutine calculate_ca_o_ti_v_equilibirum()
                    implicit none

                    doubleprecision :: ti_gas_vmr_tmp, &
                        r1, r2, r1v, r2v, &
                        k_eq_ca_solid, g(4, n_thermochemical_data), vo_gas_vmr_old

                    ! Ti + H2O -> TiO + H2
                    k_eq_tio = min(&
                        k_eq(['Ti ', 'H2O', 'TiO', 'H2 '], [-1, -1, 1, 1]), &
                        huge(0d0)&
                    )
                    ! Ti + 2H2O -> TiO2 + 2H2
                    k_eq_tio2 = min(&
                        k_eq(['Ti  ', 'H2O ', 'TiO2', 'H2  '], [-1, -2, 1, 2]), &
                        huge(0d0)&
                    )

                    ! V + H2O -> VO + H2
                    k_eq_vo = k_eq(['V  ', 'H2O', 'VO ', 'H2 '], [-1, -1, 1, 1])

                    ! V + 2H2O -> VO2 + 2H2
                    k_eq_vo2 = min(&
                        k_eq(['V  ', 'H2O', 'VO2', 'H2 '], [-1, -2, 1, 2]), &
                    huge(0d0)&
                    )

                    ! 2TiO + 2H2 + N2 -> 2TiN + 2H2O
                    k_eq_tin = exp(&
                        (gases_delta_g_i(gas_id('TiO')) &
                        - condensates_delta_g_i(condensate_id('TiN')) - gases_delta_g_i(gas_id('H2O'))) &
                        * 1d3 / (cst_R * t)) * &
                        gases_vmr(gas_id('H2'), ip) * p ** 1.5d0

                    ! VO(s) -> VO(g)
                    k_eq_vo_solid = exp(&
                        (condensates_delta_g_i(condensate_id('VO')) - gases_delta_g_i(gas_id('VO'))) &
                            * 1d3 / (cst_R * t)) / p

                    ! Ca + TiO + 2H2O -> CaTiO3(s,l) + 2H2
                    k_eq_catio3 = &
                        exp(&
                            (gases_delta_g_i(gas_id('Ca')) + gases_delta_g_i(gas_id('TiO')) + &
                            2d0 * gases_delta_g_i(gas_id('H2O')) - condensates_delta_g_i(condensate_id('CaTiO3'))) &
                            * 1d3 / (cst_R * t) &
                        ) / (gases_vmr(gas_id('H2'), ip) ** 2d0 / (p ** 2d0))
                    ! Ca(s) -> Ca(g)
                    k_eq_ca_solid = exp(-gases_delta_g_i(gas_id('Ca')) * 1d3 / (cst_R * t)) / p

                    gases_vmr(gas_id('Ti'), ip) = gas_element_abd(22, ip) / &
                        (1d0 + k_eq_tio * gases_vmr(gas_id('H2O'), ip) + &
                        k_eq_tio2 * gases_vmr(gas_id('H2O'), ip) ** 2)
                    gases_vmr(gas_id('TiO'), ip) = gas_element_abd(22, ip) / &
                        (1d0 + 1d0 / (k_eq_tio * gases_vmr(gas_id('H2O'), ip)) + &
                        (k_eq_tio2 * gases_vmr(gas_id('H2O'), ip) / k_eq_tio))
                    gases_vmr(gas_id('TiO2'), ip) = gas_element_abd(22, ip) / &
                        (1d0 + 1d0 / (k_eq_tio2 * gases_vmr(gas_id('H2O'), ip)) + &
                        k_eq_tio / (k_eq_tio2 * gases_vmr(gas_id('H2O'), ip)))

                    if(gases_vmr(gas_id('TiO'), ip) < tiny(0d0)) then
                        gases_vmr(gas_id('TiO'), ip) = tiny(0d0)
                    endif

                    r1 = gases_vmr(gas_id('Ti'), ip) / gases_vmr(gas_id('TiO'), ip)
                    r2 = gases_vmr(gas_id('TiO2'), ip) / gases_vmr(gas_id('TiO'), ip)

                    gases_vmr(gas_id('V'), ip) = &
                        gas_element_abd(23, ip) / &
                            (1d0 + k_eq_vo * gases_vmr(gas_id('H2O'), ip) + &
                            k_eq_vo2 * gases_vmr(gas_id('H2O'), ip) ** 2)
                    gases_vmr(gas_id('VO'), ip) = &
                        gas_element_abd(23, ip) / &
                            (1d0 + 1d0 / (k_eq_vo * gases_vmr(gas_id('H2O'), ip)) + &
                            (k_eq_vo2 * gases_vmr(gas_id('H2O'), ip) / k_eq_vo))
                    gases_vmr(gas_id('VO2'), ip) = &
                        gas_element_abd(23, ip) / &
                            (1d0 + 1d0 / (k_eq_vo2 * gases_vmr(gas_id('H2O'), ip)) + &
                            k_eq_vo / (k_eq_vo2 * gases_vmr(gas_id('H2O'), ip)))

                    if(gases_vmr(gas_id('VO'), ip) < tiny(0d0)) then
                        gases_vmr(gas_id('VO'), ip) = tiny(0d0)
                    endif

                    r1v = gases_vmr(gas_id('V'), ip) / gases_vmr(gas_id('VO'), ip)
                    r2v = gases_vmr(gas_id('VO2'), ip) / gases_vmr(gas_id('VO'), ip)

                    ! TiN condensation
                    if(gases_vmr(gas_id('TiO'), ip) > &
                        gases_vmr(gas_id('H2O'), ip) / (k_eq_tin * sqrt(gases_vmr(gas_id('N2'), ip))) &
                       .and. .not. is_condensed(condensate_id('CaTiO3'))) then
                        if (.not. is_condensed(condensate_id('TiN'))) then
                            is_condensed(condensate_id('TiN')) = .True.

                            write(*, condensation_fmt) 'TiN', ip, p, t, gases_vmr(gas_id('TiO'), ip)

                            call set_condensate_parameters('TiN', ip, p, gases_vmr(gas_id('TiO'), ip))
                        end if

                        gases_vmr(gas_id('TiO'), ip) = &
                            gases_vmr(gas_id('H2O'), ip) / (k_eq_tin * sqrt(gases_vmr(gas_id('N2'), ip)))
                        gases_vmr(gas_id('Ti'), ip) = gases_vmr(gas_id('TiO'), ip) * r1
                        gases_vmr(gas_id('TiO2'), ip) = gases_vmr(gas_id('TiO'), ip) * r2
                        gas_element_abd(22, ip) = gases_vmr(gas_id('TiO'), ip) * (1d0 + r1 + r2)
                    end if

                    call update_saturation_vmr('TiN', ip, gases_vmr(gas_id('TiO'), ip))

                    ! CaTiO3 condensation; dissolution of VO in CaTiO3
                    if(gas_element_abd(20, ip) * gases_vmr(gas_id('TiO'), ip) > 1d0 / &
                        (k_eq_catio3 * gases_vmr(gas_id('H2O'), ip) ** 2)) then
                         if (.not. is_condensed(condensate_id('CaTiO3'))) then
                            is_condensed(condensate_id('CaTiO3')) = .True.

                            write(*, condensation_fmt) 'CaTiO3', ip, p, t, gases_vmr(gas_id('Ca'), ip)

                            call set_condensate_parameters('CaTiO3', ip, p, gases_vmr(gas_id('Ca'), ip))
                        end if

                        do i = 1, 5
                            ti_gas_vmr_tmp = gases_vmr(gas_id('TiO'), ip) * (1d0 + r1 + r2)
                            gases_vmr(gas_id('TiO'), ip) = 1d0 / &
                                (k_eq_catio3 * gases_vmr(gas_id('H2O'), ip) ** 2 * gases_vmr(gas_id('Ca'), ip))
                            gases_vmr(gas_id('Ca'), ip) = gases_vmr(gas_id('Ca'), ip) - &
                                (ti_gas_vmr_tmp - gases_vmr(gas_id('TiO'), ip) * (1d0 + r1 + r2))
                        end do

                        gases_vmr(gas_id('Ti'), ip) = gases_vmr(gas_id('TiO'), ip) * r1
                        gases_vmr(gas_id('TiO2'), ip) = gases_vmr(gas_id('TiO'), ip) * r2
                        gas_element_abd(20, ip) = gases_vmr(gas_id('Ca'), ip)
                        gas_element_abd(22, ip) = gases_vmr(gas_id('TiO'), ip) * (1d0 + r1 + r2)

                        if(gases_vmr(gas_id('VO'), ip) < k_eq_vo_solid) then
                            do i = 1, 5
                                gases_vmr(gas_id('VO'), ip) = &
                                    gas_element_abd(23, max(ip - 1, 1)) * k_eq_vo_solid / &
                                    (gas_element_abd(22, max(ip - 1, 1)) - gas_element_abd(22, ip) + &
                                    gas_element_abd(23, max(ip - 1, 1)) + (1d0 + r1v + r2v) * &
                                    (k_eq_vo_solid - gases_vmr(gas_id('VO'), ip)))
                            end do

                            gases_vmr(gas_id('V'), ip) = gases_vmr(gas_id('VO'), ip) * r1v
                            gases_vmr(gas_id('VO2'), ip) = gases_vmr(gas_id('VO'), ip) * r2v
                            gas_element_abd(23, ip) = gases_vmr(gas_id('VO'), ip) * (1d0 + r1v + r2v)
                        end if
                    end if

                    call update_saturation_vmr('CaTiO3', ip, gases_vmr(gas_id('Ca'), ip))

                    ! Ca(s) condensation
                    ! TODO better Ca-Al chemistry, Ca(s) is considered only to remove Ca(g) at low temperatures
                    if(gases_vmr(gas_id('Ca'), ip) > k_eq_ca_solid) then
                        g(1, :) = condensates_delta_g(condensate_id('Ca'), :)
                        g(2, :) = gases_delta_g(gas_id('Ca'), :)
                        g(3:4, :) = 0d0

                        call simple_condensation(&
                            'Ca', &
                            gases_vmr(gas_id('Ca'), ip), &
                            gases_vmr(gas_id('Ca'), max(ip - 1, 1)), &
                            g, &
                            [condensates_delta_g_i(condensate_id('Ca')), gases_delta_g_i(gas_id('Ca'))], &
                            is_condensed(condensate_id('Ca'))&
                        )
                    end if

                    ! VO condensation
                    if(gases_vmr(gas_id('VO'), ip) > k_eq_vo_solid) then
                        g(1, :) = condensates_delta_g(condensate_id('VO'), :)
                        g(2, :) = gases_delta_g(gas_id('VO'), :)
                        g(3:4, :) = 0d0

                        vo_gas_vmr_old = gases_vmr(gas_id('VO'), ip)

                        call simple_condensation(&
                            'VO', gases_vmr(gas_id('VO'), ip), vo_gas_vmr_old, g, &
                            [condensates_delta_g_i(condensate_id('VO')), gases_delta_g_i(gas_id('VO'))], &
                            is_condensed(condensate_id('VO'))&
                        )

                        gases_vmr(gas_id('V'), ip) = gases_vmr(gas_id('VO'), ip) * r1v
                        gases_vmr(gas_id('VO2'), ip) = gases_vmr(gas_id('VO'), ip) * r2v
                        gas_element_abd(23, ip) = gases_vmr(gas_id('VO'), ip) * (1d0 + r1v + r2v)
                    end if

                    gases_vmr(gas_id('VO'), ip) = &
                        min(gases_vmr(gas_id('VO'), ip), gases_vmr(gas_id('TiO'), ip) * &
                        elemental_h_ratio(23) / elemental_h_ratio(22))
                end subroutine calculate_ca_o_ti_v_equilibirum

                subroutine calculate_mg_si_o_equilibrium()
                    implicit none

                    ! Mg2SiO4(s) + 3H2 -> 2Mg + SiO + 3H2O
                    k_eq_mg2sio4 = max(tiny(0d0), &
                        exp((condensates_delta_g_i(condensate_id('Mg2SiO4')) - &
                        2d0 * gases_delta_g_i(gas_id('Mg')) - gases_delta_g_i(gas_id('SiO')) - &
                        3d0 * gases_delta_g_i(gas_id('H2O'))) * &
                        1d3 / (cst_R * t)) * gases_vmr(gas_id('H2'), ip)**3 / (p**3))
                    ! MgSiO3(s) + 2H2 -> Mg + SiO + 2H2O
                    k_eq_mgsio3 = max(tiny(0d0), &
                        exp((condensates_delta_g_i(condensate_id('MgSiO3')) - &
                        gases_delta_g_i(gas_id('Mg')) - gases_delta_g_i(gas_id('SiO')) - &
                        2d0 * gases_delta_g_i(gas_id('H2O'))) * &
                        1d3 / (cst_R * t)) * gases_vmr(gas_id('H2'), ip)**2 / (p**2))
                    ! SiO2(s) + H2 -> SiO + H2O
                    k_eq_sio2 = max(tiny(0d0), &
                        exp((condensates_delta_g_i(condensate_id('SiO2')) - gases_delta_g_i(gas_id('SiO')) - &
                        gases_delta_g_i(gas_id('H2O'))) * &
                        1d3 / (cst_R * t)) * &
                        gases_vmr(gas_id('H2'), ip) / p)

                    ! Mg2SiO4 condensation
                    call mg2sio4_condensation()

                    ! MgSiO3 condensation
                    call mgsio3_condensation()

                    ! SiO2 condensation
                    call sio2_condensation()

                    call update_gases_vmr()

                    qh2oold = gases_vmr(gas_id('H2O'), ip)
                end subroutine calculate_mg_si_o_equilibrium

                subroutine calculate_fe_ni_co_equilibrium()
                    implicit none

                    integer :: ip_fe
                    doubleprecision :: gfea_old, gfeca_old, &
                        rfeh, pcfe, q0_fe, qsatfe, qsatfeold

                    ! 2Fe + H2 -> 2FeH
                    !**			FeH/Fe from Visscher et al. (2010, ApJ. 716, 1060-1075)
                    rfeh = 10d0**(-1.85d0 - 1.905d3 / t) * sqrt(p * gases_vmr(gas_id('H2'), ip))
                    gases_vmr(gas_id('Fe'), ip) = gases_vmr(gas_id('Fe'), max(ip - 1, 1)) / (1d0 + rfeh)

                    !qsatfe = exp((condensates_delta_g_i(condensate_id('Fe')) - &
                    !    gases_delta_g_i(gas_id('Fe'))) * 1d3 / (cst_R * t)) / p

!                    if(gases_vmr(gas_id('Fe'), ip) >= qsatfe .and. .not. is_condensed(condensate_id('Fe'))) then
!                        is_condensed(condensate_id('Fe')) = .True.
!
!                        call get_pold_told(ip, pold, told)
!
!                        gfea_old = interp_ex_0d(told, temperatures_thermochemistry, gases_delta_g(gas_id('Fe'), :))
!                        gfeca_old = interp_ex_0d(told, temperatures_thermochemistry, &
!                            condensates_delta_g(condensate_id('Fe'), :))
!
!                        qsatfeold = exp((gfeca_old - gfea_old) * 1d3 / (cst_R * told)) / pold
!
!                        frac = log(gases_vmr(gas_id('Fe'), ip) / qsatfeold) / log(qsatfe / qsatfeold)
!                        tc = 1d0 / ((1d0 - frac) / told + frac / t)
!                        pcfe = pold**(1d0 - frac) * p**frac
!                        q0_fe = gases_vmr(gas_id('Fe'), ip)
!
!                        if(pcfe <= pbar) then
!                            ip_fe = ip
!                        else
!                            ip_fe = ip - 1
!                        end if
!
!                        write(*, condensation_fmt) 'Fe', ip_fe, pcfe, tc, gases_vmr(gas_id('Fe'), ip)
!
!                        call set_condensate_parameters('Fe', ip_fe, pcfe, gases_vmr(gas_id('Fe'), ip))
!                    end if

                    !if(gases_vmr(gas_id('Fe'), ip) >= qsatfe) then
                        !call update_vmr('Fe', qsatfe)

                        !call update_saturation_vmr('Fe', ip, qsatfe)

                        ! Formation of Ni-Fe alloys, inclusion of Co
                        !gas_element_abd(27, ip) = &
                        !    gases_vmr(gas_id('Fe'), ip) * elemental_h_ratio(27) / elemental_h_ratio(26)
                        !gas_element_abd(28, ip) = &
                        !    gases_vmr(gas_id('Fe'), ip) * elemental_h_ratio(28) / elemental_h_ratio(26)
                        !gases_vmr(gas_id('Ni'), ip) = gas_element_abd(28, ip)

                        ! Updates
                        !gases_vmr(gas_id('FeH'), ip) = gases_vmr(gas_id('Fe'), ip) * rfeh

                        !call update_gases_vmr()
                    !else
                        gases_vmr(gas_id('FeH'), ip) = gases_vmr(gas_id('Fe'), ip) * rfeh
                    !end if
                    print*,'all',ip,gases_vmr(gas_id('FeH'), ip),gases_vmr(gas_id('Fe'), ip),gases_vmr(gas_id('H2'), ip),&
                    gases_vmr(gas_id('H'), ip)
                    print*,'Fe',ip,&
                        sqrt(gases_vmr(gas_id('FeH'), ip))/(sqrt(gases_vmr(gas_id('Fe'), ip)) * gases_vmr(gas_id('H2'), ip)),rfeh
                    print*,'Fe2',ip,&
                        gases_vmr(gas_id('FeH'), ip)/(gases_vmr(gas_id('Fe'), ip)),rfeh
                    print*,'H',ip,gases_vmr(gas_id('H2'), ip)/(sqrt(gases_vmr(gas_id('H'), ip))),k_eq(['H2', 'H '], [-1, 2])
                end subroutine calculate_fe_ni_co_equilibrium

                subroutine calculate_p_equilibrium()
                    implicit none

                    doubleprecision :: term, k_eq_h3po4, k_eq_p2, k_eq_ph2, k_eq_po, k_eq_ph3

                    ! 2PH3 -> 2P + 3H2
                    k_eq_ph3 = sqrt(k_eq(['PH3', 'P  ', 'H2 '], [-2, 2, 3]))

                    ! 2PH3 -> P2 + 3H2
                    k_eq_p2 = k_eq(['PH3', 'P2 ', 'H2 '], [-2, 1, 3])

                    ! 2PH3 -> 2PH2 + H2
                    k_eq_ph2 = sqrt(k_eq(['PH3', 'PH2', 'H2 '], [-2, 2, 1]))

                    ! 2PH3 + 2H2O -> 2PO + 5H2
                    k_eq_po = sqrt(k_eq(['PH3', 'H2O', 'PO ', 'H2 '], [-2, -2, 2, 5])) * gases_vmr(gas_id('H2O'), ip)

                    ! H3PO4 -> PH3 + 4H2O + 4H2
                    k_eq_h3po4 = exp((condensates_delta_g_i(condensate_id('H3PO4')) - gases_delta_g_i(gas_id('PH3')) &
                        - 4.d+0 * gases_delta_g_i(gas_id('H2O'))) * &
                        1.d+03 / (cst_R * t)) * &
                        gases_vmr(gas_id('H2'), ip) ** 4 / p

                    term = 1d0 + k_eq_ph2 + k_eq_ph3 + k_eq_po

                    if(term ** 2 > 1d6 * gas_element_abd(15, ip) * k_eq_p2) then
                        gases_vmr(gas_id('PH3'), ip) = gas_element_abd(15, ip) / term
                    else
                        gases_vmr(gas_id('PH3'), ip) = &
                            (-term + sqrt(term ** 2d0 + 8d0 * gas_element_abd(15, ip) * k_eq_p2)) / (4d0 * k_eq_p2)
                    end if

                    ! H3PO4 condensation (only at equilibirium !)
                    if(at_equilibrium .and. gases_vmr(gas_id('H2O'), ip) > 0d0) then
                        if (k_eq_h3po4 / gases_vmr(gas_id('H2O'), ip) ** 4 < gases_vmr(gas_id('PH3'), ip)) then
                            if (.not. is_condensed(condensate_id('H3PO4'))) then
                                is_condensed(condensate_id('NH4Cl')) = .True.

                                write(*, condensation_fmt) 'H3PO4', ip, p, t, &
                                    k_eq_h3po4 / gases_vmr(gas_id('H2O'), ip) ** 4
                            end if

                            gases_vmr(gas_id('PH3'), ip) = k_eq_h3po4 / gases_vmr(gas_id('H2O'), ip) ** 4
                            gas_element_abd(15, ip) = &
                                gases_vmr(gas_id('PH3'), ip) * (term + 2d0 * k_eq_p2 * gases_vmr(gas_id('PH3'), ip))
                        end if
                    end if

                    gases_vmr(gas_id('P'), ip) = k_eq_ph3 * gases_vmr(gas_id('PH3'), ip)
                    gases_vmr(gas_id('P2'), ip) = k_eq_p2 * gases_vmr(gas_id('PH3'), ip) ** 2d0
                    gases_vmr(gas_id('PH2'), ip) = k_eq_ph2 * gases_vmr(gas_id('PH3'), ip)
                    gases_vmr(gas_id('PO'), ip) = k_eq_po * gases_vmr(gas_id('PH3'), ip)
                end subroutine calculate_p_equilibrium

                subroutine calculate_al_o_equilibrium()
                    implicit none

                    doubleprecision :: al_vmr_old, g(4, n_thermochemical_data)

                    if (ip > 1) then
                        al_vmr_old = gas_element_abd(13, ip - 1)
                    else
                        al_vmr_old = gas_element_abd(13, 1)
                    end if

                    g(1, :) = condensates_delta_g(condensate_id('Al2O3'), :)
                    g(2, :) = 0d0
                    g(3, :) = gases_delta_g(gas_id('Al'), :)
                    g(4, :) = gases_delta_g(gas_id('H2O'), :)

                    !call al2o3_condensation()
                    call a_h2_to_c_d_condensation_reaction(&
                        'Al2O3', &
                        gases_vmr(gas_id('H2'), ip), gases_vmr(gas_id('Al'), ip), &
                        gases_vmr(gas_id('H2O'), ip), &
                        [0d0, gases_vmr(gas_id('H2'), ip), al_vmr_old, qh2oold], &
                        [1, 3, 2, 3], &
                        g, &
                        [condensates_delta_g_i(condensate_id('Al2O3')), 0d0, &
                        gases_delta_g_i(gas_id('Al')), gases_delta_g_i(gas_id('H2O'))], &
                        is_condensed(condensate_id('Al2O3')), &
                        .True. &
                    )

                    qh2oold = gases_vmr(gas_id('H2O'), ip)
                    call update_gas_element_abd()
                end subroutine calculate_al_o_equilibrium

                subroutine calculate_cr_equilibrium()
                    implicit none

                    doubleprecision :: g(2, n_thermochemical_data), g_cr2o3cr(4, n_thermochemical_data), &
                        cr_gas_vmr_old, k_eq_cr, &
                        k_eq_cr2o3

                    g(1, :) = condensates_delta_g(condensate_id('Cr'), :)
                    g(2, :) = gases_delta_g(gas_id('Cr'), :)

                    g_cr2o3cr(1, :) = condensates_delta_g(condensate_id('Cr2O3'), :)
                    g_cr2o3cr(2, :) = 0d0
                    g_cr2o3cr(3, :) = gases_delta_g(gas_id('Cr'), :)
                    g_cr2o3cr(4, :) = gases_delta_g(gas_id('H2O'), :)

                    ! Cr(s, l) -> Cr(g)
                    k_eq_cr = exp((condensates_delta_g_i(condensate_id('Cr')) - gases_delta_g_i(gas_id('Cr'))) &
                        * 1d3 / (cst_R * t)) / p

                    ! Cr2O3(s, l) + 3H2 -> 2Cr(g) + 3H2O
                    if((condensates_delta_g_i(condensate_id('Cr2O3')) - &
                        2d0 * gases_delta_g_i(gas_id('Cr')) - &
                        3d0 * gases_delta_g_i(gas_id('H2O'))) * &
                        1d3 / (cst_R * t) < log(tiny(0d0))) then
                        k_eq_cr2o3 = tiny(0d0)
                    else
                        k_eq_cr2o3 = max(&
                            exp(&
                                (condensates_delta_g_i(condensate_id('Cr2O3')) - 2d0 * gases_delta_g_i(gas_id('Cr')) &
                                - 3d0 * gases_delta_g_i(gas_id('H2O'))) &
                                * 1d3 / (cst_R * t) &
                            ) * gases_vmr(gas_id('H2'), ip) ** 3d0 * p ** (3d0 - 2d0 - 3d0), &
                            (1d0 + 2d-6) * tiny(0d0) &
                        )
                    end if

                    if (ip > 1) then
                        cr_gas_vmr_old = gas_element_abd(24, ip - 1)
                    else
                        cr_gas_vmr_old = gas_element_abd(24, ip)
                    end if

                    ! Reaction priority
                    if(gases_vmr(gas_id('Cr'), ip) / k_eq_cr >= &
                        gases_vmr(gas_id('Cr'), ip) ** 2 * gases_vmr(gas_id('H2O'), ip) ** 3 / k_eq_cr2o3) then
                        call simple_condensation(&
                            'Cr', gas_element_abd(24, ip), cr_gas_vmr_old, g, &
                            [condensates_delta_g_i(condensate_id('Cr')), gases_delta_g_i(gas_id('Cr'))], &
                            is_condensed(condensate_id('Cr'))&
                        )
                        call a_h2_to_c_d_condensation_reaction(&
                            'Cr2O3', &
                            gases_vmr(gas_id('H2'), ip), gases_vmr(gas_id('Cr'), ip), &
                            gases_vmr(gas_id('H2O'), ip), &
                            [0d0, gases_vmr(gas_id('H2'), ip), cr_gas_vmr_old, qh2oold], &
                            [1, 3, 2, 3], &
                            g_cr2o3cr, &
                            [condensates_delta_g_i(condensate_id('Cr2O3')), 0d0, &
                                gases_delta_g_i(gas_id('Cr')), gases_delta_g_i(gas_id('H2O'))], &
                            is_condensed(condensate_id('Cr2O3')), &
                            .True. &
                        )
                        call update_gas_element_abd()
                    else
                        call a_h2_to_c_d_condensation_reaction(&
                            'Cr2O3', &
                            gases_vmr(gas_id('H2'), ip), gases_vmr(gas_id('Cr'), ip), &
                            gases_vmr(gas_id('H2O'), ip), &
                            [0d0, gases_vmr(gas_id('H2'), ip), cr_gas_vmr_old, qh2oold], &
                            [1, 3, 2, 3], &
                            g_cr2o3cr, &
                            [condensates_delta_g_i(condensate_id('Cr2O3')), 0d0, &
                                gases_delta_g_i(gas_id('Cr')), gases_delta_g_i(gas_id('H2O'))], &
                            is_condensed(condensate_id('Cr2O3')), &
                            .True. &
                        )
                        call update_gas_element_abd()
                        call simple_condensation(&
                            'Cr', gas_element_abd(24, ip), cr_gas_vmr_old, g, &
                            [condensates_delta_g_i(condensate_id('Cr')), gases_delta_g_i(gas_id('Cr'))], &
                            is_condensed(condensate_id('Cr'))&
                        )
                    end if
                end subroutine calculate_cr_equilibrium

                subroutine calculate_mn_s_equilibrium()
                    implicit none

                    doubleprecision :: k_eq_mn_s, mn_vmr_old, g(4, n_thermochemical_data), &
                        h2s_vmr_tmp, qh2sold

                    ! Mn(s) -> Mn(g)
                    k_eq_mn_s = exp(-gases_delta_g_i(gas_id('Mn')) * 1d3 / (cst_R * t)) / p

                    h2s_vmr_tmp = gases_vmr(gas_id('H2S'), ip)
                    qh2sold = gases_vmr(gas_id('H2S'), max(ip - 1, 1))

                    if (ip > 1) then
                        mn_vmr_old = gas_element_abd(25, ip - 1)
                    else
                        mn_vmr_old = gas_element_abd(25, 1)
                    end if

                    g(1, :) = condensates_delta_g(condensate_id('MnS'), :)
                    g(2, :) = 0d0
                    g(3, :) = gases_delta_g(gas_id('Mn'), :)
                    g(4, :) = gases_delta_g(gas_id('H2S'), :)

                    call a_h2_to_c_d_condensation_reaction(&
                        'MnS', &
                        gases_vmr(gas_id('H2'), ip), gases_vmr(gas_id('Mn'), ip), gases_vmr(gas_id('H2S'), ip), &
                        [0d0, gases_vmr(gas_id('H2'), ip), mn_vmr_old, qh2sold], &
                        [1, 1, 1, 1], &
                        g, &
                        [condensates_delta_g_i(condensate_id('MnS')), 0d0, &
                            gases_delta_g_i(gas_id('Mn')), gases_delta_g_i(gas_id('H2S'))], &
                        is_condensed(condensate_id('MnS')), &
                        .False. &
                    )
                    gas_element_abd(16, ip) = gas_element_abd(16, ip) - &
                        (h2s_vmr_tmp - gases_vmr(gas_id('H2S'), ip))
                    call update_gas_element_abd()
                end subroutine calculate_mn_s_equilibrium

                subroutine calculate_zn_s_equilibrium()
                    implicit none

                    doubleprecision :: k_eq_zns_s, zn_vmr_old, g(4, n_thermochemical_data), &
                        h2s_vmr_tmp, qh2sold

                    ! ZnS + H2 -> Zn + H2S
                    k_eq_zns_s = exp(&
                        (condensates_delta_g_i(condensate_id('ZnS')) - gases_delta_g_i(gas_id('Zn')) - &
                        gases_delta_g_i(gas_id('H2S'))) &
                        * 1d3 / (cst_R * t)) * &
                        gases_vmr(gas_id('H2'), ip) / p

                    h2s_vmr_tmp = gases_vmr(gas_id('H2S'), ip)
                    qh2sold = gases_vmr(gas_id('H2S'), max(ip - 1, 1))

                    if (ip > 1) then
                        zn_vmr_old = gas_element_abd(30, ip - 1)
                    else
                        zn_vmr_old = gas_element_abd(30, 1)
                    end if

                    g(1, :) = condensates_delta_g(condensate_id('ZnS'), :)
                    g(2, :) = 0d0
                    g(3, :) = gases_delta_g(gas_id('Zn'), :)
                    g(4, :) = gases_delta_g(gas_id('H2S'), :)

                    call a_h2_to_c_d_condensation_reaction(&
                        'ZnS', &
                        gases_vmr(gas_id('H2'), ip), gases_vmr(gas_id('Zn'), ip), gases_vmr(gas_id('H2S'), ip), &
                        [0d0, gases_vmr(gas_id('H2'), ip), zn_vmr_old, qh2sold], &
                        [1, 1, 1, 1], &
                        g, &
                        [condensates_delta_g_i(condensate_id('ZnS')), 0d0, &
                            gases_delta_g_i(gas_id('Zn')), gases_delta_g_i(gas_id('H2S'))], &
                        is_condensed(condensate_id('ZnS')), &
                        .False. &
                    )

                    gas_element_abd(16, ip) = gas_element_abd(16, ip) - &
                        (h2s_vmr_tmp - gases_vmr(gas_id('H2S'), ip))
                    call update_gas_element_abd()
                end subroutine calculate_zn_s_equilibrium

                function k_eq(species, stoichiometric_coefficients, pressure, temperature)
                    ! """
                    ! Wrapper for equilibrium_constant_gases.
                    ! """
                    implicit none

                    character(len=*), dimension(:), intent(in) :: species
                    integer, dimension(:), intent(in) :: stoichiometric_coefficients
                    doubleprecision, optional :: pressure, temperature

                    integer :: i, n
                    integer, allocatable, dimension(:) :: species_id
                    doubleprecision, allocatable, dimension(:) :: delta_g_species
                    doubleprecision :: k_eq, h2_factor, pp, tt

                    n = size(species)
                    h2_factor = 1d0

                    allocate(species_id(n), delta_g_species(n))

                    if(present(pressure)) then
                        pp = pressure
                    else
                        pp = p
                    end if

                    if(present(temperature)) then
                        tt = temperature

                        do i = 1, n
                            species_id(i) = gas_id(trim(species(i)))
                            delta_g_species(i) = &
                                interp_ex_0d(&
                                    tt, temperatures_thermochemistry, &
                                    gases_delta_g(species_id(i), :) &
                                )
                        end do
                    else
                        tt = t

                        do i = 1, n
                            species_id(i) = gas_id(trim(species(i)))
                            delta_g_species(i) = gases_delta_g_i(species_id(i))
                        end do
                    end if

                    do i = 1, n
                        if(species_id(i) == gas_id_h2) then
                            h2_factor = gases_vmr(gas_id_h2, ip) ** stoichiometric_coefficients(i)
                        end if
                    end do

                    k_eq = equilibrium_constant_gases(stoichiometric_coefficients, delta_g_species, n, pp, tt) !/ &
                        !h2_factor

                    deallocate(species_id, delta_g_species)

                    return
                end function k_eq

                subroutine get_pold_told(ip, pold, told)
                    implicit none

                    integer, intent(in) :: ip
                    doubleprecision, intent(out) :: pold, told

                    if (ip == 1) then
                        told = temperatures_layers(ip) * 1.198d0
                        pold = 2d-3 * pressures_layers(ip)
                    else
                        told = temperatures_layers(ip - 1)
                        pold = 1d-3 * pressures_layers(ip - 1)
                    end if
                end subroutine get_pold_told

                subroutine h2o_condenstation()
                    implicit none

                    integer :: ip_h2o
                    doubleprecision :: h2o_vmr_saturation, tch2o, pch2o

                    ! Condensation level
                    if (.not. is_condensed(condensate_id('H2O'))) then
                        ! Status update
                        is_condensed(condensate_id('H2O')) = .True.

                        ! Interpolate to find true pressure/temperature of condensation
                        call get_pold_told(ip, pold, told)

                        h2o_vmr_saturation = h2o_saturation_pressure(told) / pold
                        frac = log(gases_vmr(gas_id('H2O'), ip) / h2o_vmr_saturation) / log(qsath2o / h2o_vmr_saturation)
                        tch2o = 1d0 / ((1d0 - frac) / told + frac / t)
                        pch2o = pold ** (1d0 - frac) * p ** frac

                        if(pch2o <= pbar) then
                            ip_h2o = ip
                        else
                            ip_h2o = ip - 1
                        end if

                        call set_condensate_parameters('H2O', ip_h2o, pch2o, gases_vmr(gas_id('H2O'), ip))

                        write(*, condensation_fmt) 'H2O', ip_h2o, pch2o, tch2o, h2o_vmr_saturation
                    end if

                    ! Update
                    h2o_is_saturated = .True.

                    call update_vmr('H2O', qsath2o)
                    call calculate_h2_h_equilibrium()

                    k_eq_co = k_eq(['H2O', 'CH4', 'CO ', 'H2 '], [-1, -1, 1, 3])
                    k_eq_ch3 = sqrt(k_eq(['CH4', 'CH3', 'H2 '], [-2, 2, 1]))
                    k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1])
                    k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3])
                end subroutine h2o_condenstation

                subroutine mgsio3_condensation()
                    implicit none

                    integer :: ip_mg, i
                    doubleprecision :: k_eq_mgsio3_c_lvl, k_eq_mgsio3_old, k_eq_mg2sio4_c_lvl, &
                        gh2oa_c, gmg2sio4a_c, gmga_c, gmgsio3a_c, gsioa_c, qsatmg, ao

                    qsatmg = min(&
                        gases_vmr(gas_id('Mg'), ip), &
                        (k_eq_mgsio3 / gases_vmr(gas_id('SiO'), ip) / gases_vmr(gas_id('H2O'), ip)**2)&
                    )
                    call update_saturation_vmr('MgSiO3', ip, qsatmg)

                    if((gases_vmr(gas_id('Mg'), ip) * gases_vmr(gas_id('SiO'), ip) * &
                        gases_vmr(gas_id('H2O'), ip)**2) > &
                        k_eq_mgsio3) then
                        if(.not. is_condensed(condensate_id('MgSiO3'))) then
                            is_condensed(condensate_id('MgSiO3')) = .True.

                            call get_pold_told(ip, pold, told)

                            gmgsio3a_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                condensates_delta_g(condensate_id('MgSiO3'), :))
                            gmga_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('Mg'), :))
                            gsioa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('SiO'), :))
                            gh2oa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('H2O'), :))

                            ! MgSiO3 + 2H2 -> Mg + SiO + 2H2O (at level below)
                            k_eq_mgsio3_old = max(&
                                (1d0 + 1d-6) * tiny(0d0), &
                                exp((gmgsio3a_c - gmga_c - gsioa_c - 2. * gh2oa_c) * &
                                1d3 / (cst_R * told)) * gases_vmr(gas_id('H2'), ip)**2 / (pold**2)&
                            )

                            ! Find precise T and p of condensation
                            frac = (log(gases_vmr(gas_id('Mg'), max(ip - 1, 1)) * &
                                    gases_vmr(gas_id('SiO'), max(ip - 1, 1)) * qh2oold**2) - &
                                    log(k_eq_mgsio3_old)) / &
                                    (log(k_eq_mgsio3) - log(k_eq_mgsio3_old))

                            if (frac < - t / (told - t)) then
                                frac = - t / (told - t) * (1d0 - 1d-3)
                            end if

                            tc = 1d0 / ((1d0 - frac) / told + frac / t)

                            if (frac > log(huge(0d0)) / log(p)) then
                                pc = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                            else
                                pc = pold ** (1d0 - frac) * p ** frac
                            end if

                            do i = 1, 5
                                gmg2sio4a_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    condensates_delta_g(condensate_id('Mg2SiO4'), :))
                                gmgsio3a_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    condensates_delta_g(condensate_id('MgSiO3'), :))
                                gmga_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    gases_delta_g(gas_id('Mg'), :))
                                gsioa_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    gases_delta_g(gas_id('SiO'), :))
                                gh2oa_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    gases_delta_g(gas_id('H2O'), :))

                                ! Mg2SiO4 + 3H2 -> 2Mg + SiO + 3H2O (at level of condensation)
                                k_eq_mg2sio4_c_lvl = max(&
                                    tiny(0d0), &
                                    exp((gmg2sio4a_c - 2d0 * gmga_c - gsioa_c - 3d0 * gh2oa_c) * &
                                    1d3 / (cst_R * tc)) * gases_vmr(gas_id('H2'), ip)**3 / (pc**3)&
                                )
                                ! MgSiO3 + 2H2 -> Mg + SiO + 2H2O (at level of condensation)
                                k_eq_mgsio3_c_lvl = max(&
                                    tiny(0d0), &
                                    exp((gmgsio3a_c - gmga_c - gsioa_c - 2d0 * gh2oa_c) * &
                                    1d3 / (cst_R * tc)) * gases_vmr(gas_id('H2'), ip)**2 / (pc**2)&
                                )

                                do ii = 1, 15
                                    alf = exp(&
                                        log(k_eq_mg2sio4_c_lvl) - &
                                        (2d0 * log(gases_vmr(gas_id('Mg'), ip)) + log(gases_vmr(gas_id('SiO'), ip)) + &
                                        3d0 * log(gases_vmr(gas_id('H2O'), ip)))&
                                    )

                                    if(abs(alf - 1d0) < 1d-8) then
                                        exit
                                    end if

                                    ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                    if(.not. co_co2_quench) then
                                        ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                    else
                                        ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                            gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                    end if

                                    ! Find which species from Mg, SiO or O will limitate the formation of Mg2SiO4
                                    if (alf < 1d0) then
                                        alpha = min(&
                                            0.5d0 * gases_vmr(gas_id('Mg'), ip) * (1d0 - sqrt(alf)), &
                                            gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                            ao / 3d0 * (1d0 - alf ** (1d0 / 3d0)) &
                                        )
                                    else
                                         alpha = max(&
                                            0.5d0 * gases_vmr(gas_id('Mg'), ip) * (1d0 - sqrt(alf)), &
                                            gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                            ao / 3d0 * (1d0 - alf ** (1d0 / 3d0)) &
                                        )
                                    end if

                                    call partition_o_mg_si(&
                                        ii, alf1, alf2, alpha1, alf, alpha, &
                                        gases_vmr(gas_id('Mg'), ip), &
                                        gas_element_abd(14, ip), gas_element_abd(8, ip), 1&
                                    )

                                    ! Update abundances
                                    if(gases_vmr(gas_id('Mg'), ip) - 2d0 * alpha > tiny(0d0)) then
                                        gases_vmr(gas_id('Mg'), ip) = &
                                            gases_vmr(gas_id('Mg'), ip) - 2d0 * alpha
                                    else
                                        gases_vmr(gas_id('Mg'), ip) = &
                                            gases_vmr(gas_id('Mg'), ip) * sqrt(alf)
                                    endif

                                    gas_element_abd(8, ip) = gas_element_abd(8, ip) - 3d0 * alpha
                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha

                                    ! Update O-C-Si species
                                    call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                                end do

                                frac = &
                                    (log(gases_vmr(gas_id('Mg'), ip) * &
                                     gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip)**2) - &
                                    log(k_eq_mgsio3_c_lvl)) / &
                                    (log(k_eq_mgsio3_old) - log(k_eq_mgsio3_c_lvl))

                                if (frac < - t / (told - t)) then
                                    frac = - t / (told - t) * (1d0 - 1d-3)
                                end if

                                tc = 1d0 / ((1d0 - frac) / tc + frac / told)

                                if (frac > log(huge(0d0)) / log(p)) then
                                    pc = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                                else
                                    pc = pold ** (1d0 - frac) * pc ** frac
                                end if
                            end do

                            if(pc <= pbar) then
                                ip_temp = ip
                            else
                                ip_temp = ip - 1
                            end if

                            write(*, condensation_fmt) 'MgSiO3', ip_temp, pc, tc, gases_vmr(gas_id('Mg'), ip)

                            call set_condensate_parameters('MgSiO3', ip_temp, pc, gases_vmr(gas_id('Mg'), ip))

                            ip_mg = min(ip_mg, ip_temp)

                            do ii = 1, 15
                                alf = k_eq_mgsio3 / &
                                    (gases_vmr(gas_id('Mg'), ip) * gases_vmr(gas_id('SiO'), ip) * &
                                    gases_vmr(gas_id('H2O'), ip) ** 2)

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                if(.not. co_co2_quench) then
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                else
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                        gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                end if

                                if(alf < 1d0) then
                                    alpha = min(&
                                        gases_vmr(gas_id('Mg'), ip) * (1d0 - alf), &
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao / 2d0 * (1d0 - sqrt(alf)))
                                else
                                    alpha = max(&
                                            gases_vmr(gas_id('Mg'), ip) * (1d0 - alf), &
                                            gas_element_abd(14, ip) * (1d0 - alf), &
                                            ao / 2d0 * (1d0 - sqrt(alf)))
                                end if

                                call partition_o_mg_si(&
                                    ii, alf1, alf2, alpha1, alf, alpha, &
                                    gases_vmr(gas_id('Mg'), ip), &
                                    gas_element_abd(14, ip), gas_element_abd(8, ip), 2&
                                )

                                if(gases_vmr(gas_id('Mg'), ip) - alpha > tiny(0d0)) then
                                    gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) - alpha
                                else
                                    gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) * alf
                                endif

                                gas_element_abd(8, ip) = gas_element_abd(8, ip) - 3d0 * alpha
                                gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha

                                call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                            end do
                        else
                            do ii = 1, 15
                                alf = k_eq_mgsio3 / (gases_vmr(gas_id('Mg'), ip) * &
                                    gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip)**2)

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                if(.not. co_co2_quench) then
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                else
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                        gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                end if

                                if(alf < 1d0) then
                                    alpha = min(&
                                        gases_vmr(gas_id('Mg'), ip) * (1d0 - alf), &
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao / 2d0 * (1d0 - sqrt(alf))&
                                    )
                                else
                                    alpha = max(&
                                        gases_vmr(gas_id('Mg'), ip) * (1d0 - alf), &
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao / 2d0 * (1d0 - sqrt(alf))&
                                    )
                                end if

                                call partition_o_mg_si(&
                                    ii, alf1, alf2, alpha1, alf, alpha, &
                                    gases_vmr(gas_id('Mg'), ip), &
                                    gas_element_abd(14, ip), gas_element_abd(8, ip), 2&
                                )

                                if(gases_vmr(gas_id('Mg'), ip) - alpha > tiny(0d0)) then
                                    gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) - alpha
                                else
                                    gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) * alf
                                endif

                                gas_element_abd(8, ip) = gas_element_abd(8, ip) - 3d0 * alpha
                                gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha

                                call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                            end do
                        end if
                    end if
                end subroutine mgsio3_condensation

                subroutine mg2sio4_condensation()
                    implicit none

                    integer :: ip_mg

                    doubleprecision :: k_eq_mg2sio4_old, &
                        gh2oa_c, gmg2sio4a_c, gsioa_c, gmga_c, q0_mg, qsatmg, pcmg2sio4, ao

                    qsatmg = (k_eq_mg2sio4 / gases_vmr(gas_id('SiO'), ip) / &
                        gases_vmr(gas_id('H2O'), ip)**3) ** 0.5
                    call update_saturation_vmr('Mg2SiO4', ip, qsatmg)

                    if((gases_vmr(gas_id('Mg'), ip)**2 * gases_vmr(gas_id('SiO'), ip) * &
                        gases_vmr(gas_id('H2O'), ip)**3) > &
                        k_eq_mg2sio4) then
                        if(.not. is_condensed(condensate_id('Mg2SiO4'))) then
                            is_condensed(condensate_id('Mg2SiO4')) = .True.

                            call get_pold_told(ip, pold, told)

                            gmg2sio4a_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                condensates_delta_g(condensate_id('Mg2SiO4'), :))
                            gmga_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('Mg'), :))
                            gsioa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('SiO'), :))
                            gh2oa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('H2O'), :))

                            k_eq_mg2sio4_old = &
                                    max(&
                                    exp((gmg2sio4a_c - 2d0 * gmga_c - gsioa_c - 3d0 * gh2oa_c) * 1d3 / &
                                    (cst_R * told)) * gases_vmr(gas_id('H2'), ip)**3 / (pold**3), &
                                    (1d0 + 1d-6) * tiny(0d0) &
                            )

                            if(k_eq_mg2sio4_old < tiny(0d0)) then
                                k_eq_mg2sio4_old = tiny(0d0)
                            end if

                            frac = (log(gases_vmr(gas_id('Mg'), max(ip - 1, 1))**2 * &
                                    gases_vmr(gas_id('SiO'), max(ip - 1, 1)) * qh2oold**3) - &
                                    log(k_eq_mg2sio4_old)) / &
                                    (log(k_eq_mg2sio4) - log(k_eq_mg2sio4_old))

                            if (frac < - t / (told - t)) then
                                frac = - t / (told - t) * (1d0 - 1d-3)
                            end if

                            tc = 1d0 / ((1d0 - frac) / told + frac / t)

                            if (frac > log(huge(0d0)) / log(p)) then
                                pcmg2sio4 = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                            else
                                pcmg2sio4 = pold ** (1d0 - frac) * p ** frac
                            end if

                            q0_mg = gases_vmr(gas_id('Mg'), ip)

                            if(pcmg2sio4 <= pbar) then
                                ip_mg = ip
                            else
                                ip_mg = ip - 1
                            end if

                            write(*, condensation_fmt) &
                                'Mg2SiO4', ip_mg, pcmg2sio4, tc, gases_vmr(gas_id('Mg'), ip)

                            call set_condensate_parameters('Mg2SiO4', ip_mg, pcmg2sio4, &
                                                           gases_vmr(gas_id('Mg'), ip))

                            do ii = 1, 15
                                alf = k_eq_mg2sio4 / &
                                    (gases_vmr(gas_id('Mg'), ip)**2 * gases_vmr(gas_id('SiO'), ip) * &
                                        gases_vmr(gas_id('H2O'), ip)**3)

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                if(.not. co_co2_quench) then
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                else
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                            gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                end if

                                if(alf < 1d0) then
                                    alpha = min(&
                                        0.5d0 * gases_vmr(gas_id('Mg'), ip) * (1d0 - sqrt(alf)), &
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao / 3d0 * (1d0 - alf ** (1d0 / 3d0)) &
                                    )
                                else
                                    alpha = max(&
                                        0.5d0 * gases_vmr(gas_id('Mg'), ip) * (1d0 - sqrt(alf)), &
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao / 3d0 * (1d0 - alf ** (1d0 / 3d0)) &
                                    )
                                end if

                                call partition_o_mg_si(&
                                    ii, alf1, alf2, alpha1, alf, alpha, &
                                    gases_vmr(gas_id('Mg'), ip), &
                                    gas_element_abd(14, ip), gas_element_abd(8, ip), 1&
                                )

                                if(gases_vmr(gas_id('Mg'), ip) - 2d0 * alpha > tiny(0d0)) then
                                    gases_vmr(gas_id('Mg'), ip) = &
                                        gases_vmr(gas_id('Mg'), ip) - 2d0 * alpha
                                else
                                    gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) * sqrt(alf)
                                endif

                                gas_element_abd(8, ip) = gas_element_abd(8, ip) - 4d0 * alpha
                                gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha

                                call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                            end do
                        else
                            if(.not. is_condensed(condensate_id('MgSiO3')) .and. &
                               .not. is_condensed(condensate_id('SiO2'))) then
                                do ii = 1, 15
                                    alf = k_eq_mg2sio4 / &
                                        (gases_vmr(gas_id('Mg'), ip)**2 * &
                                        gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip)**3)

                                    if(abs(alf - 1d0) < 1d-8) then
                                        exit
                                    end if

                                    ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                    if(.not. co_co2_quench) then
                                        ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                    else
                                        ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                            gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                    end if

                                    if(alf < 1d0) then
                                        alpha = min(&
                                            0.5d0 * gases_vmr(gas_id('Mg'), ip) * (1d0 - sqrt(alf)), &
                                            gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                            ao / 3d0 * (1d0 - alf**(1d0 / 3d0)) &
                                        )
                                    else
                                        alpha = max(&
                                            0.5d0 * gases_vmr(gas_id('Mg'), ip) * (1d0 - sqrt(alf)), &
                                            gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                            ao / 3d0 * (1d0 - alf**(1d0 / 3d0)) / 3d0 &
                                        )
                                    end if

                                    call partition_o_mg_si(ii, alf1, alf2, alpha1, alf, alpha, &
                                        gases_vmr(gas_id('Mg'), ip), &
                                        gas_element_abd(14, ip), gas_element_abd(8, ip), 1&
                                    )

                                    if(gases_vmr(gas_id('Mg'), ip) - 2d0 * alpha > tiny(0d0)) then
                                        gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) - 2d0 * alpha
                                    else
                                        gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) * sqrt(alf)
                                    endif

                                    gas_element_abd(8, ip) = gas_element_abd(8, ip) - 4d0 * alpha
                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha

                                    call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                                end do
                            end if
                        end if
                    end if
                end subroutine mg2sio4_condensation

                subroutine na2s_condensation(qnasat)
                    implicit none

                    doubleprecision, intent(in) :: qnasat

                    integer :: ip_tmp
                    doubleprecision :: k_eq_na2s_old, &
                        gh2sa_c, gna2sa_c, gnaa_c, pcna, q0_na2s, qnasatold

                    is_condensed(condensate_id('Na2S')) = .True.

                    call get_pold_told(ip, pold, told)

                    gna2sa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                        condensates_delta_g(condensate_id('Na2S'), :))
                    gh2sa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('H2S'), :))
                    gnaa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('Na'), :))

                    k_eq_na2s_old = &
                        exp((gna2sa_c - 2d0 * gnaa_c - gh2sa_c) * 1d3 / (cst_R * told)) * &
                        gases_vmr(gas_id('H2'), ip) / (pold ** 2d0)

                    qnasatold = sqrt(k_eq_na2s_old / gases_vmr(gas_id('H2S'), max(ip - 1, 1)))
                    frac = log(gases_vmr(gas_id('Na'), max(ip - 1, 1)) / qnasatold) / &
                        log(qnasat / qnasatold)

                    if (frac < - t / (told - t)) then
                            frac = - t / (told - t) * (1d0 - 1d-3)
                    end if

                    tc = 1d0 / ((1d0 - frac) / told + frac / t)
                    pcna = pold ** (1d0 - frac) * p ** frac

                    if(pcna <= pbar) then
                        ip_tmp = ip
                    else
                        ip_tmp = ip - 1
                    end if

                    write(*, condensation_fmt) 'Na2S', ip_tmp, pcna, tc, qnasatold

                    call set_condensate_parameters('Na2S', ip_tmp, pcna, qnasatold)

                    q0_na2s = gases_vmr(gas_id('Na'), ip)
                end subroutine na2s_condensation

                subroutine nh3_condenstation()
                    implicit none

                    integer :: ip_tmp
                    doubleprecision :: nh3_vmr_saturation, tc, pcnh3, qsatnh3

                    ! Status update
                    is_condensed(condensate_id('NH3')) = .True.

                    qsatnh3 = nh3_saturation_pressure(t) / p

                    ! Interpolate to find true pressure/temperature of condensation
                    call get_pold_told(ip, pold, told)

                    nh3_vmr_saturation = nh3_saturation_pressure(told) / pold
                    frac = log(gases_vmr(gas_id('NH3'), ip) / nh3_vmr_saturation) / &
                        log(qsatnh3/ nh3_vmr_saturation)

                    if (frac < - t / (told - t)) then
                            frac = - t / (told - t) * (1d0 - 1d-3)
                    end if

                    tc = 1d0 / ((1d0 - frac) / told + frac / t)
                    pcnh3 = pold ** (1d0 - frac) * p ** frac

                    if(pcnh3 <= pbar) then
                        ip_tmp = ip
                    else
                        ip_tmp = ip - 1
                    end if

                    write(*, condensation_fmt) 'NH3', ip_tmp, pcnh3, tc, nh3_vmr_saturation

                    call set_condensate_parameters('NH3', ip_tmp, pcnh3, gases_vmr(gas_id('NH3'), ip))
                end subroutine nh3_condenstation

                subroutine nh4sh_condensation()
                    implicit none

                    pc = nh4sh_saturation_pressure(t) / p ** 2

                    call update_saturation_vmr('NH4SH', ip, pc)

                    if(gases_vmr(gas_id('H2S'), ip) * gases_vmr(gas_id('NH3'), ip) > pc) then
                        if (.not. is_condensed(condensate_id('NH4SH'))) then
                            write(*, condensation_fmt) 'NH4SH', ip, p, t, gases_vmr(gas_id('H2S'), ip) * &
                                gases_vmr(gas_id('NH3'), ip)
                            is_condensed(condensate_id('NH4SH')) = .True.

                            call set_condensate_parameters('NH4SH', ip, pc * p ** 2, pc)
                        end if

                        alpha = 0.5d0 * (gases_vmr(gas_id('NH3'), ip) + gases_vmr(gas_id('H2S'), ip) - &
                            sqrt((gases_vmr(gas_id('NH3'), ip) + gases_vmr(gas_id('H2S'), ip)) ** 2 - &
                            4d0 * (gases_vmr(gas_id('NH3'), ip) * gases_vmr(gas_id('H2S'), ip) - pc)))
                        gases_vmr(gas_id('NH3'), ip) = gases_vmr(gas_id('NH3'), ip) - alpha
                        gases_vmr(gas_id('H2S'), ip) = gases_vmr(gas_id('H2S'), ip) - alpha

                        if (gases_vmr(gas_id('NH3'), ip) < gases_vmr(gas_id('H2S'), ip) * 1d-14) then
                            alpha = &
                                alpha + gases_vmr(gas_id('NH3'), ip) - pc / gases_vmr(gas_id('H2S'), ip)
                            gases_vmr(gas_id('NH3'), ip) = pc / gases_vmr(gas_id('H2S'), ip)
                        else if (gases_vmr(gas_id('H2S'), ip) < gases_vmr(gas_id('NH3'), ip) * 1d-14) then
                            alpha = alpha + gases_vmr(gas_id('H2S'), ip) - pc / &
                                gases_vmr(gas_id('NH3'), ip)
                            gases_vmr(gas_id('H2S'), ip) = pc / gases_vmr(gas_id('NH3'), ip)
                        end if

                        call update_gases_vmr()
                    end if
                end subroutine nh4sh_condensation

                subroutine sio2_condensation()
                    implicit none

                    integer :: i
                    doubleprecision :: k_eq_mgsio3_c_lvl, k_eq_sio2_old, k_eq_sio2_c_lvl, &
                        gh2oa_c, gsio2a_c, gsioa_c, gmga_c, gmgsio3a_c, ao

                    if(gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip) > k_eq_sio2) then
                        if(.not. is_condensed(condensate_id('SiO2'))) then
                            is_condensed(condensate_id('SiO2')) = .True.

                            call get_pold_told(ip, pold, told)

                            gsioa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('SiO'), :))
                            gsio2a_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                condensates_delta_g(condensate_id('SiO2'), :))
                            gh2oa_c = interp_ex_0d(told, temperatures_thermochemistry, &
                                gases_delta_g(gas_id('H2O'), :))

                            k_eq_sio2_old = exp((gsio2a_c - gsioa_c - gh2oa_c) * 1d3 / (cst_R * told)) * &
                                    gases_vmr(gas_id('H2'), ip) / pold

                            frac = log(gases_vmr(gas_id('SiO'), max(ip - 1, 1)) * qh2oold / k_eq_sio2_old) / &
                                log(k_eq_sio2 / k_eq_sio2_old)

                            if (frac < - t / (told - t)) then
                                frac = - t / (told - t) * (1d0 - 1d-3)
                            end if

                            tc = 1d0 / ((1d0 - frac) / told + frac / t)

                            if (frac > log(huge(0d0)) / log(p)) then
                                pc = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                            else
                                pc = pold ** (1d0 - frac) * p ** frac
                            end if

                            do i = 1, 5
                                gmgsio3a_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    condensates_delta_g(condensate_id('MgSiO3'), :))
                                gmga_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    gases_delta_g(gas_id('Mg'), :))
                                gsioa_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    gases_delta_g(gas_id('SiO'), :))
                                gsio2a_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    condensates_delta_g(condensate_id('SiO2'), :))
                                gh2oa_c = interp_ex_0d(tc, temperatures_thermochemistry, &
                                    gases_delta_g(gas_id('H2O'), :))

                                k_eq_mgsio3_c_lvl = exp((gmgsio3a_c - gmga_c - gsioa_c - 2. * gh2oa_c) * 1d3 / (cst_R * tc)) * &
                                        gases_vmr(gas_id('H2'), ip)**2 / (pc**2)
                                k_eq_sio2_c_lvl = exp((gsio2a_c - gsioa_c - gh2oa_c) * 1d3 / (cst_R * tc)) * &
                                        gases_vmr(gas_id('H2'), ip) / pc

                                do ii = 1, 15
                                    alf = k_eq_mgsio3 / &
                                        (gases_vmr(gas_id('Mg'), ip) * gases_vmr(gas_id('SiO'), ip) * &
                                        gases_vmr(gas_id('H2O'), ip)**2)

                                    if(abs(alf - 1d0) < 1d-8) then
                                        exit
                                    end if

                                    ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                    if(.not. co_co2_quench) then
                                        ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                    else
                                        ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                            gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                    end if

                                    if(alf < 1d0) then
                                        alpha = min(&
                                            gases_vmr(gas_id('Mg'), ip) * (1d0 - alf), &
                                            gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                            ao / 2d0 * (1d0 - sqrt(alf))&
                                        )
                                    else
                                        alpha = max(&
                                            gases_vmr(gas_id('Mg'), ip) * (1d0 - alf), &
                                            gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                            ao / 2d0 * (1d0 - sqrt(alf))&
                                        )
                                    end if

                                    call partition_o_mg_si(&
                                        ii, alf1, alf2, alpha1, alf, alpha, &
                                        gases_vmr(gas_id('Mg'), ip), &
                                        gas_element_abd(14, ip), gas_element_abd(8, ip), 2&
                                    )

                                    if(gases_vmr(gas_id('Mg'), ip) - alpha > tiny(0d0)) then
                                        gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) - alpha
                                    else
                                        gases_vmr(gas_id('Mg'), ip) = gases_vmr(gas_id('Mg'), ip) * alf
                                    endif

                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha
                                    gas_element_abd(8, ip) = gas_element_abd(8, ip) - 3d0 * alpha

                                    call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                                end do

                                frac = log(gases_vmr(gas_id('SiO'), ip) * &
                                    gases_vmr(gas_id('H2O'), ip) / k_eq_sio2_c_lvl) / log(k_eq_sio2 / k_eq_sio2_c_lvl)

                                if (frac < - t / (told - t)) then
                                    frac = - t / (told - t) * (1d0 - 1d-3)
                                end if

                                tc = 1d0 / ((1d0 - frac) / tc + frac / t)

                                if (frac > log(huge(0d0)) / log(p)) then
                                    pc = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                                else
                                    pc = pold ** (1d0 - frac) * pc ** frac
                                end if
                            end do

                            if(pc <= pbar) then
                                ip_temp = ip
                            else
                                ip_temp = ip - 1
                            end if

                            write(*, condensation_fmt) 'SiO2', ip_temp, pc, tc, gases_vmr(gas_id('SiO'), ip)

                            call set_condensate_parameters('SiO2', ip_temp, pc, gases_vmr(gas_id('SiO'), ip))

                            do ii = 1, 15
                                alf = k_eq_sio2 / (gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip))

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                ! Fix crash occurring if H2O is not the dominant O species and CO is quenched
                                if(.not. co_co2_quench) then
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)
                                else
                                    ao = gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip) - &
                                        gases_vmr(gas_id('CO'), ip) - 2d0 * gases_vmr(gas_id('CO2'), ip)
                                end if

                                if(alf < 1d0) then
                                    alpha = min(&
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao * (1d0 - alf)&
                                    )
                                else
                                    alpha = max(&
                                        gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        ao * (1d0 - alf)&
                                    )
                                end if

                                call partition_o_mg_si(&
                                    ii, alf1, alf2, alpha1, alf, alpha, &
                                    gases_vmr(gas_id('Mg'), ip), gas_element_abd(14, ip), gas_element_abd(8, ip), 3&
                                )

                                if(gas_element_abd(14, ip) - alpha > tiny(0d0)) then
                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha
                                else
                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) * alf
                                endif

                                gas_element_abd(8, ip) = gas_element_abd(8, ip) - 2d0 * alpha

                                call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                            end do
                        end if

                        if(gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip) >= k_eq_sio2 .and. &
                            .not. h2o_is_saturated) then
                            do ii = 1, 15
                                alf = k_eq_sio2 / (gases_vmr(gas_id('SiO'), ip) * gases_vmr(gas_id('H2O'), ip))

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                if(alf < 1d0) then
                                    alpha = min(gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        (gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)) * (1d0 - alf))
                                else
                                    alpha = max(gases_vmr(gas_id('SiO'), ip) * (1d0 - alf), &
                                        (gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)) * (1d0 - alf))
                                end if

                                call partition_o_mg_si(&
                                    ii, alf1, alf2, alpha1, alf, alpha, gases_vmr(gas_id('Mg'), ip), &
                                    gas_element_abd(14, ip), gas_element_abd(8, ip), 3&
                                )

                                if(gas_element_abd(14, ip) - alpha > tiny(0d0)) then
                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) - alpha
                                else
                                    gas_element_abd(14, ip) = gas_element_abd(14, ip) * alf
                                endif

                                gas_element_abd(8, ip) = gas_element_abd(8, ip) - 2d0 * alpha

                                call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                            end do
                        end if
                    end if

                    call update_saturation_vmr('SiO2', ip, gases_vmr(gas_id('SiO'), ip))
                end subroutine sio2_condensation

                subroutine co_ch4_quenching()
                    implicit none

                    doubleprecision :: gcoaq, gco2aq, gch4aq, gch3aq, gh2oaq, gsioaq, gsih4aq, eq, ech3q, eco2q, &
                        esioq

                    co_ch4_quench = .True.

                    ! Interpolate to find true pressure/temperature of quench
                    call get_pold_told(ip, pold, told)

                    x = log(tchemC1 / tmix1) / log(tchemC1 * tmix / (tchemC * tmix1))
                    tquenchC = told * (t / told)**x
                    pquenchC = pold * (p / pold)**x

                    write(*, quench_fmt) 'CO/CH4', pquenchC, tquenchC

                    ! Update parameters
                    gcoaq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('CO'), :))
                    gco2aq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('CO2'), :))
                    gch4aq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('CH4'), :))
                    gch3aq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('CH3'), :))
                    gh2oaq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('H2O'), :))
                    gsioaq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('SiO'), :))
                    gsih4aq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('SiH4'), :))
                    eq = exp(-(gcoaq - gh2oaq - gch4aq) * 1d3 / (cst_R * tquenchC)) / &
                            (gases_vmr(gas_id('H2'), ip)**3 * pquenchC * pquenchC)
                    ech3q = exp(-(gch3aq - gch4aq) * 1d3 / (cst_R * tquenchC)) / &
                            sqrt(gases_vmr(gas_id('H2'), ip) * pquenchC)
                    eco2q = exp(-(gco2aq - gcoaq - gh2oaq) * 1d3 / (cst_R * tquenchC)) / gases_vmr(gas_id('H2'), ip)
                    esioq = exp(-(gsioaq - gsih4aq - gh2oaq) * 1d3 / (cst_R * tquenchC)) / &
                            (gases_vmr(gas_id('H2'), ip)**3 * pquenchC * pquenchC)

                    call osi(eq, ech3q, eco2q, esioq, pquenchc, tquenchc)

                    qcoco2 = gases_vmr(gas_id('CO'), ip) + gases_vmr(gas_id('CO2'), ip)
                    qch4q = gases_vmr(gas_id('CH4'), ip) * (1d0 + ech3q)
                    gases_vmr(gas_id('CH3'), ip) = tiny(0d0)
                end subroutine co_ch4_quenching

                subroutine co_co2_quenching()
                    implicit none

                    doubleprecision :: gcoaq, gco2aq, gh2oaq, gsioaq, gsih4aq, eco2q, esioq

                    co_co2_quench = .True.

                    call get_pold_told(ip, pold, told)

                    x = log(tchemCO21 / tmix1) / &
                            log(tchemCO21 * tmix / (tchemCO2 * tmix1))
                    tquenchCO2 = told * (t / told)**x
                    pquenchCO2 = pold * (p / pold)**x

                    write(*, quench_fmt) 'CO/CO2', pquenchCO2, tquenchCO2

                    gcoaq = interp_ex_0d(tquenchCO2, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('CO'), :))
                    gco2aq = interp_ex_0d(tquenchCO2, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('CO2'), :))
                    gh2oaq = interp_ex_0d(tquenchCO2, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('H2O'), :))
                    gsioaq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('SiO'), :))
                    gsih4aq = interp_ex_0d(tquenchC, temperatures_thermochemistry, &
                        gases_delta_g(gas_id('SiH4'), :))
                    eco2q = exp(-(gco2aq - gcoaq - gh2oaq) * 1d3 / (cst_R * tquenchCO2)) / gases_vmr(gas_id('H2'), ip)
                    esioq = exp(-(gsioaq - gsih4aq - gh2oaq) * 1d3 / (cst_R * tquenchC)) / &
                            (gases_vmr(gas_id('H2'), ip)**3 * pquenchC * pquenchC)

                    call osiqco(eco2q, esioq, pquenchco2, tquenchco2)
                end subroutine co_co2_quenching

                subroutine osi(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4, pressure, temperature)
                    implicit none

                    doubleprecision, optional, intent(in) :: pressure, temperature
                    doubleprecision, intent(inout) :: k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4

                    integer :: i
                    doubleprecision :: d_sum, vmr_tmp, h2_vmr_tmp
                    doubleprecision :: pp, tt

                    if(present(pressure)) then
                        pp = pressure
                    else
                        pp = p
                    end if

                    if(present(temperature)) then
                        tt = temperature
                    else
                        tt = t
                    end if

                    if(.not. h2o_is_saturated) then
                        if(k_eq_co * &
                            (gas_element_abd(8, ip) - gas_element_abd(6, ip) - gas_element_abd(14, ip)) >= 1d0) then
                            if(k_eq_sih4 * (gas_element_abd(8, ip) - gas_element_abd(14, ip)) - &
                               k_eq_co * gas_element_abd(6, ip) >= 1d0) then
                                gases_vmr(gas_id('H2O'), ip) = &
                                    gas_element_abd(8, ip) - gas_element_abd(6, ip) - gas_element_abd(14, ip)

                                i = 0
                                d_sum = huge(0d0)

                                do while(d_sum > prec_high .and. i < i_max)
                                    i = i + 1
                                    vmr_tmp = gases_vmr(gas_id('H2O'), ip)
                                    h2_vmr_tmp = gases_vmr(gas_id('H2O'), ip)

                                    gases_vmr(gas_id('CO'), ip) = &
                                        gases_vmr(gas_id('H2O'), ip) * gas_element_abd(6, ip) * k_eq_co / &
                                        (1d0 + k_eq_ch3 + k_eq_co * gases_vmr(gas_id('H2O'), ip) * &
                                        (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))
                                    gases_vmr(gas_id('CO2'), ip) = gases_vmr(gas_id('CO'), ip) * &
                                        gases_vmr(gas_id('H2O'), ip) * k_eq_co2
                                    gases_vmr(gas_id('SiO'), ip) = &
                                        gases_vmr(gas_id('H2O'), ip) * gas_element_abd(14, ip) * k_eq_sih4 / &
                                        (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                                    gases_vmr(gas_id('SiH4'), ip) = &
                                         gases_vmr(gas_id('SiO'), ip) / (k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                                    gases_vmr(gas_id('H2O'), ip) = &
                                        gas_element_abd(8, ip) - sum(elements_in_gases(:, 8) * gases_vmr(:, ip)) + &
                                        gases_vmr(gas_id('H2O'), ip)

                                    ! Handle divergence at high metallicity
                                    if((d_sum < abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp) .and. i > 5) .or. &
                                        i > 30 .or. &
                                        gases_vmr(gas_id('H2O'), ip) <= 0d0 &
                                        ) then
                                        if(gases_vmr(gas_id('H2O'), ip) <= 0d0) then
                                           gases_vmr(gas_id('H2O'), ip) = &
                                                vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i)))
                                        else if (gases_vmr(gas_id('H2O'), ip) >= 1d0) then
                                           gases_vmr(gas_id('H2O'), ip) = &
                                                vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i)))
                                        else
                                            if(gases_vmr(gas_id('H2O'), ip) > vmr_tmp) then
                                                gases_vmr(gas_id('H2O'), ip) = &
                                                    vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                            else
                                                gases_vmr(gas_id('H2O'), ip) = &
                                                    vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                            end if
                                        end if
                                    end if

                                    call calculate_h2_h_equilibrium()

                                    ! Handle divergence at high metallicity
                                    if(gases_vmr(gas_id('H2'), ip) <= 0d0 .or. gases_vmr(gas_id('H2'), ip) >= 1d0) then
                                        if(gases_vmr(gas_id('H2'), ip) <= 0d0) then
                                           gases_vmr(gas_id('H2'), ip) = &
                                                h2_vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i)))
                                        else
                                           gases_vmr(gas_id('H2'), ip) = &
                                                h2_vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i)))
                                        end if
                                    end if

                                    k_eq_co = k_eq(['H2O', 'CH4', 'CO ', 'H2 '], [-1, -1, 1, 3], pp, tt)
                                    k_eq_ch3 = sqrt(k_eq(['CH4', 'CH3', 'H2 '], [-2, 2, 1], pp, tt))
                                    k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1], pp, tt)
                                    k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3], pp, tt)

                                    d_sum = abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp)
                                end do

                                gases_vmr(gas_id('CH4'), ip) = gases_vmr(gas_id('CO'), ip) / &
                                    (k_eq_co * gases_vmr(gas_id('H2O'), ip))

                                call calculate_h2_h_equilibrium()
                            else
                                gases_vmr(gas_id('H2O'), ip) = (gas_element_abd(8, ip) - gas_element_abd(6, ip)) / &
                                    (1d0 + k_eq_sih4 * gas_element_abd(14, ip))

                                i = 0
                                d_sum = huge(0d0)

                                do while(d_sum > prec_high .and. i < i_max)
                                    i = i + 1
                                    vmr_tmp = gases_vmr(gas_id('H2O'), ip)

                                    gases_vmr(gas_id('SiH4'), ip) = &
                                        gas_element_abd(14, ip) / (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                                    gases_vmr(gas_id('CO'), ip) = &
                                        gases_vmr(gas_id('H2O'), ip) * gas_element_abd(6, ip) * k_eq_co / &
                                        (1d0 + k_eq_ch3 + k_eq_co * gases_vmr(gas_id('H2O'), ip) * &
                                        (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))
                                    gases_vmr(gas_id('CO2'), ip) = &
                                        gases_vmr(gas_id('CO'), ip) * gases_vmr(gas_id('H2O'), ip) * k_eq_co2
                                    gases_vmr(gas_id('H2O'), ip) = &
                                        (gas_element_abd(8, ip) - &
                                        gases_vmr(gas_id('CO'), ip) * &
                                        (1d0 + 2d0 * k_eq_co2 * gases_vmr(gas_id('H2O'), ip))) / &
                                        (1d0 + k_eq_sih4 * gases_vmr(gas_id('SiH4'), ip))

                                    ! Handle divergence at high metallicity
                                    if((d_sum < abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp) .and. i > 5) .or. &
                                        i > 30) then
                                        if(gases_vmr(gas_id('H2O'), ip) > vmr_tmp) then
                                            gases_vmr(gas_id('H2O'), ip) = &
                                                vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                        else
                                            gases_vmr(gas_id('H2O'), ip) = &
                                                vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                        end if
                                    end if

                                    call calculate_h2_h_equilibrium()

                                    k_eq_co = k_eq(['H2O', 'CH4', 'CO ', 'H2 '], [-1, -1, 1, 3], pp, tt)
                                    k_eq_ch3 = sqrt(k_eq(['CH4', 'CH3', 'H2 '], [-2, 2, 1], pp, tt))
                                    k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1], pp, tt)
                                    k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3], pp, tt)

                                    d_sum = abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp)
                                end do

                                gases_vmr(gas_id('SiO'), ip) = &
                                    gases_vmr(gas_id('H2O'), ip) * k_eq_sih4 * gases_vmr(gas_id('SiH4'), ip)
                                gases_vmr(gas_id('CH4'), ip) = gases_vmr(gas_id('CO'), ip) / &
                                    (k_eq_co * gases_vmr(gas_id('H2O'), ip))

                                call calculate_h2_h_equilibrium()
                            end if
                        else
                            if(k_eq_sih4 * (gas_element_abd(8, ip) - gas_element_abd(14, ip)) - &
                                k_eq_co * gas_element_abd(6, ip) >= 1d0) then
                                gases_vmr(gas_id('H2O'), ip) = (gas_element_abd(8, ip) - gas_element_abd(14, ip)) / &
                                    (1d0 + k_eq_co * gas_element_abd(6, ip))

                                i = 0
                                d_sum = huge(0d0)

                                do while(d_sum > prec_high .and. i < i_max)
                                    i = i + 1
                                    vmr_tmp = gases_vmr(gas_id('H2'), ip)

                                    gases_vmr(gas_id('CH4'), ip) = gas_element_abd(6, ip) / &
                                        (1d0 + k_eq_ch3 + &
                                        k_eq_co * gases_vmr(gas_id('H2O'), ip) * &
                                        (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))
                                    gases_vmr(gas_id('CH3'), ip) = gases_vmr(gas_id('CH4'), ip) * k_eq_ch3
                                    gases_vmr(gas_id('SiO'), ip) = &
                                        gases_vmr(gas_id('H2O'), ip) * gas_element_abd(14, ip) * k_eq_sih4 / &
                                        (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                                    gases_vmr(gas_id('SiH4'), ip) = &
                                         gases_vmr(gas_id('SiO'), ip) / (k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                                    gases_vmr(gas_id('H2O'), ip) = &
                                        (gas_element_abd(8, ip) - gases_vmr(gas_id('SiO'), ip)) / &
                                        (1d0 + k_eq_co * gases_vmr(gas_id('CH4'), ip) * &
                                        (1d0 + 2d0 * k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))

                                    call calculate_h2_h_equilibrium()

                                    ! Handle divergence at high metallicity
                                    if((d_sum < abs(gases_vmr(gas_id('H2'), ip) - vmr_tmp) .and. i > 5) .or. &
                                            i > 30 .or. &
                                            gases_vmr(gas_id('H2'), ip) <= 0d0 .or. &
                                            gases_vmr(gas_id('H2'), ip) >= 1d0 &
                                        ) then
                                        if(gases_vmr(gas_id('H2'), ip) <= 0d0) then
                                           gases_vmr(gas_id('H2'), ip) = &
                                                vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i)))
                                        else if (gases_vmr(gas_id('H2'), ip) >= 1d0) then
                                           gases_vmr(gas_id('H2'), ip) = &
                                                vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i)))
                                        else
                                            if(gases_vmr(gas_id('H2'), ip) > vmr_tmp) then
                                                gases_vmr(gas_id('H2'), ip) = &
                                                    vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                            else
                                                gases_vmr(gas_id('H2'), ip) = &
                                                    vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                            end if
                                        end if
                                    end if

                                    k_eq_co = k_eq(['H2O', 'CH4', 'CO ', 'H2 '], [-1, -1, 1, 3], pp, tt)
                                    k_eq_ch3 = sqrt(k_eq(['CH4', 'CH3', 'H2 '], [-2, 2, 1], pp, tt))
                                    k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1], pp, tt)
                                    k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3], pp, tt)

                                    d_sum = abs(gases_vmr(gas_id('H2'), ip) - vmr_tmp)
                                end do

                                gases_vmr(gas_id('CO'), ip) = &
                                    gases_vmr(gas_id('H2O'), ip) * k_eq_co * gases_vmr(gas_id('CH4'), ip)
                                gases_vmr(gas_id('CO2'), ip) = &
                                    gases_vmr(gas_id('CO'), ip) * gases_vmr(gas_id('H2O'), ip) * k_eq_co2

                                call calculate_h2_h_equilibrium()
                            else
                                gases_vmr(gas_id('H2O'), ip) = &
                                    gas_element_abd(8, ip) / (1d0 + k_eq_co * gas_element_abd(6, ip) + &
                                    k_eq_sih4 * gas_element_abd(14, ip))

                                i = 0
                                d_sum = huge(0d0)

                                do while(d_sum > prec_high .and. i < i_max)
                                    i = i + 1
                                    vmr_tmp = gases_vmr(gas_id('H2O'), ip)

                                    gases_vmr(gas_id('SiH4'), ip) = &
                                        gas_element_abd(14, ip) / (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                                    gases_vmr(gas_id('CH4'), ip) = gas_element_abd(6, ip) / &
                                        (1d0 + k_eq_ch3 + k_eq_co * &
                                        gases_vmr(gas_id('H2O'), ip) * (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))
                                    gases_vmr(gas_id('CH3'), ip) = k_eq_ch3 * gases_vmr(gas_id('CH4'), ip)
                                    gases_vmr(gas_id('H2O'), ip) = gas_element_abd(8, ip) / &
                                        (1d0 + k_eq_co * gases_vmr(gas_id('CH4'), ip) * &
                                        (1d0 + 2d0 * k_eq_co2 * gases_vmr(gas_id('H2O'), ip)) + &
                                        k_eq_sih4 * gases_vmr(gas_id('SiH4'), ip))

                                    ! Handle divergence at high metallicity
                                    if((d_sum < abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp) .and. i > 5) .or. &
                                        i > 30) then
                                        if(gases_vmr(gas_id('H2O'), ip) > vmr_tmp) then
                                            gases_vmr(gas_id('H2O'), ip) = &
                                                vmr_tmp * (1d0 + exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                        else
                                            gases_vmr(gas_id('H2O'), ip) = &
                                                vmr_tmp * (1d0 - exp(log(prec_low) / dble(i_max) * dble(i - 5)))
                                        end if
                                    end if

                                    call calculate_h2_h_equilibrium()

                                    k_eq_co = k_eq(['H2O', 'CH4', 'CO ', 'H2 '], [-1, -1, 1, 3], pp, tt)
                                    k_eq_ch3 = sqrt(k_eq(['CH4', 'CH3', 'H2 '], [-2, 2, 1], pp, tt))
                                    k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1], pp, tt)
                                    k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3], pp, tt)

                                    d_sum = abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp)
                                end do

                                gases_vmr(gas_id('CO'), ip) = &
                                    gases_vmr(gas_id('H2O'), ip) * k_eq_co * gases_vmr(gas_id('CH4'), ip)
                                gases_vmr(gas_id('CO2'), ip) = &
                                    gases_vmr(gas_id('CO'), ip) * gases_vmr(gas_id('H2O'), ip) * k_eq_co2
                                gases_vmr(gas_id('SiO'), ip) = &
                                    gases_vmr(gas_id('H2O'), ip) * k_eq_sih4 * gases_vmr(gas_id('SiH4'), ip)

                                ! If H2O VMR < 0, sacrifice strict O conservation
                                if(gases_vmr(gas_id('H2O'), ip) < 0d0) then
                                    gases_vmr(gas_id('H2O'), ip) =  gas_element_abd(8, ip) / &
                                        (1d0 + k_eq_co * gases_vmr(gas_id('CH4'), ip) * &
                                        (1d0 + 2d0 * k_eq_co2 * gases_vmr(gas_id('H2O'), ip)) + &
                                        k_eq_sih4 * gases_vmr(gas_id('SiH4'), ip))
                                end if

                                call calculate_h2_h_equilibrium()
                            end if
                        end if
                    else
                        gases_vmr(gas_id('CO'), ip) = &
                            gases_vmr(gas_id('H2O'), ip) * gas_element_abd(6, ip) * k_eq_co / &
                            (1d0 + k_eq_ch3 + k_eq_co * gases_vmr(gas_id('H2O'), ip) * &
                            (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))
                        gases_vmr(gas_id('CO2'), ip) = gases_vmr(gas_id('CO'), ip) * &
                            gases_vmr(gas_id('H2O'), ip) * k_eq_co2
                        gases_vmr(gas_id('SiO'), ip) = &
                            gases_vmr(gas_id('H2O'), ip) * gas_element_abd(14, ip) * k_eq_sih4 / &
                            (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                    end if
                end subroutine osi

                subroutine osiqco(k_eq_co2, k_eq_sih4, pressure, temperature)
                    implicit none

                    doubleprecision, optional, intent(in) :: pressure, temperature
                    doubleprecision, intent(inout) :: k_eq_co2, k_eq_sih4

                    integer :: i
                    doubleprecision :: vmr_tmp, d_sum
                    doubleprecision :: pp, tt

                    if(present(pressure)) then
                        pp = pressure
                    else
                        pp = p
                    end if

                    if(present(temperature)) then
                        tt = temperature
                    else
                        tt = t
                    end if

                    if(.not. h2o_is_saturated) then
                        if(k_eq_sih4 * (gas_element_abd(14, ip) - gas_element_abd(8, ip) + qcoco2) <= 1d0) then
                            gases_vmr(gas_id('H2O'), ip) = gas_element_abd(8, ip) - qcoco2
                        else
                            gases_vmr(gas_id('H2O'), ip) = &
                                (gas_element_abd(8, ip) - qcoco2) / &
                                (k_eq_sih4 * (gas_element_abd(14, ip) - gas_element_abd(8, ip) + qcoco2))
                        end if

                        i = 0
                        d_sum = huge(0d0)

                        do while(d_sum > prec_high .and. i < i_max)
                            i = i + 1
                            vmr_tmp = gases_vmr(gas_id('H2O'), ip)

                            gases_vmr(gas_id('H2O'), ip) = &
                                (gas_element_abd(8, ip) - qcoco2) / &
                                (1d0 + k_eq_sih4 * gas_element_abd(14, ip) / &
                                (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip)) + &
                                k_eq_co2 * qcoco2 / (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))

                            call calculate_h2_h_equilibrium()

                            k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3], pp, tt)
                            k_eq_co2 = k_eq(['CO ', 'H2O', 'CO2', 'H2 '], [-1, -1, 1, 1], pp, tt)

                            d_sum = abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp)
                        end do
                    end if

                    gases_vmr(gas_id('CO'), ip) = qcoco2 / &
                        (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip))
                    gases_vmr(gas_id('CO2'), ip) = gases_vmr(gas_id('CO'), ip) * &
                        gases_vmr(gas_id('H2O'), ip) * k_eq_co2

                    if(gases_vmr(gas_id('H2O'), ip) < 0d0) then
                        ! If H2O VMR < 0, sacrifice strict O conservation
                        if(gases_vmr(gas_id('H2O'), ip) < 0d0) then
                            gases_vmr(gas_id('H2O'), ip) = (gas_element_abd(8, ip) - &
                                (gases_vmr(gas_id('CO'), ip) + gases_vmr(gas_id('CO2'), ip))) / &
                                (1d0 + k_eq_sih4 * gas_element_abd(14, ip) / &
                                (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip)) + &
                                k_eq_co2 * (gases_vmr(gas_id('CO'), ip) + gases_vmr(gas_id('CO2'), ip)) / &
                                (1d0 + k_eq_co2 * gases_vmr(gas_id('H2O'), ip)))

                            if(gases_vmr(gas_id('H2O'), ip) < 0d0) then
                                gases_vmr(gas_id('H2O'), ip) = gas_element_abd(8, ip) / &
                                    (1d0 + k_eq_co * gases_vmr(gas_id('CH4'), ip) * &
                                    (1d0 + 2d0 * k_eq_co2 * gases_vmr(gas_id('H2O'), ip)) + &
                                    k_eq_sih4 * gases_vmr(gas_id('SiH4'), ip))
                            end if
                        end if
                    end if

                    gases_vmr(gas_id('SiO'), ip) = &
                        gases_vmr(gas_id('H2O'), ip) * gas_element_abd(14, ip) * k_eq_sih4 / &
                            (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                    gases_vmr(gas_id('SiH4'), ip) = &
                         gases_vmr(gas_id('SiO'), ip) / (k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))

                    call calculate_h2_h_equilibrium()
                end subroutine osiqco

                subroutine osiqcoco2(k_eq_sih4, pressure, temperature)
                    implicit none

                    doubleprecision, optional, intent(in) :: pressure, temperature
                    doubleprecision, intent(inout) :: k_eq_sih4

                    integer :: i
                    doubleprecision :: vmr_tmp, d_sum
                    doubleprecision :: pp, tt

                    if(present(pressure)) then
                        pp = pressure
                    else
                        pp = p
                    end if

                    if(present(temperature)) then
                        tt = temperature
                    else
                        tt = t
                    end if

                    if(.not. h2o_is_saturated) then
                        if(k_eq_sih4 * (gas_element_abd(14, ip) - gas_element_abd(8, ip) + &
                            gases_vmr(gas_id('CO'), ip) + 2d0 * gases_vmr(gas_id('CO2'), ip)) <= 1d0) then
                            gases_vmr(gas_id('H2O'), ip) = &
                                gas_element_abd(8, ip) - &
                                gases_vmr(gas_id('CO'), ip) - &
                                2d0 * gases_vmr(gas_id('CO2'), ip)
                        else
                            gases_vmr(gas_id('H2O'), ip) = &
                                (gas_element_abd(8, ip) - &
                                gases_vmr(gas_id('CO'), ip) - &
                                2d0 * gases_vmr(gas_id('CO2'), ip)) / &
                                   (k_eq_sih4 * (gas_element_abd(14, ip) - gas_element_abd(8, ip) + &
                                    gases_vmr(gas_id('CO'), ip) + 2d0 * gases_vmr(gas_id('CO2'), ip)))
                        end if

                        i = 0
                        d_sum = huge(0d0)

                        do while(d_sum > prec_high .and. i < i_max)
                            i = i + 1
                            vmr_tmp = gases_vmr(gas_id('H2O'), ip)

                            gases_vmr(gas_id('H2O'), ip) = &
                                (gas_element_abd(8, ip) - &
                                    gases_vmr(gas_id('CO'), ip) - &
                                    2d0 * gases_vmr(gas_id('CO2'), ip)) / &
                                (1d0 + k_eq_sih4 * gas_element_abd(14, ip) / &
                                (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip)))

                            call calculate_h2_h_equilibrium()

                            k_eq_sih4 = k_eq(['SiH4', 'H2O ', 'SiO ', 'H2  '], [-1, -1, 1, 3], pp, tt)

                            d_sum = abs(gases_vmr(gas_id('H2O'), ip) - vmr_tmp)
                        end do
                    end if

                    gases_vmr(gas_id('SiO'), ip) = &
                        gases_vmr(gas_id('H2O'), ip) * gas_element_abd(14, ip) * k_eq_sih4 / &
                        (1d0 + k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))
                    gases_vmr(gas_id('SiH4'), ip) = &
                         gases_vmr(gas_id('SiO'), ip) / (k_eq_sih4 * gases_vmr(gas_id('H2O'), ip))

                    call calculate_h2_h_equilibrium()

                    gas_element_abd(8, ip) = sum(gases_vmr(:, ip) * elements_in_gases(:, 8))
                end subroutine osiqcoco2

                subroutine update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                    implicit none

                    doubleprecision, intent(inout) :: k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4

                    if(.not. co_ch4_quench) then
                        call osi(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)
                    else
                        if(.not. co_co2_quench) then
                            call osiqco(k_eq_co2, k_eq_sih4)
                        else
                            call osiqcoco2(k_eq_sih4)
                        end if
                    end if
                end subroutine update_c_o_si

                subroutine update_gas_element_abd()
                    ! """
                    ! """
                    implicit none

                    integer :: ip_tmp

                    ip_tmp = max(ip, 1)

                    do j = 1, n_elements
                        if (element_is_in_gases_list(j)) then
                            gas_element_abd(j, ip_tmp) = sum(elements_in_gases(:, j) * gases_vmr(:, ip_tmp))
                        else
                            gas_element_abd(j, ip_tmp) = 0d0
                        end if
                    end do
                end subroutine update_gas_element_abd

                subroutine update_vmr(gas_name, new_vmr)
                    implicit none

                    character(len=*), intent(in) :: gas_name
                    doubleprecision, intent(in) :: new_vmr
                    doubleprecision, dimension(n_gases) :: gases_vmr_layer

                    integer, parameter :: i_max = 100

                    integer :: i, id
                    doubleprecision :: &
                        a, gases_vmr_tmp(n_gases), sum_gases_vmr_tmp, d_sum

                    id = gas_id(gas_name)
                    gases_vmr_layer(:) = gases_vmr(:, ip)

                    a = 1d0 / (sum(gases_vmr_layer(:id-1)) + new_vmr + sum(gases_vmr_layer(id+1:)))

                    gases_vmr_tmp(:id-1) = gases_vmr_layer(:id-1) * a
                    gases_vmr_tmp(id) = new_vmr
                    gases_vmr_tmp(id+1:) = gases_vmr_layer(id+1:) * a

                    d_sum = huge(0d0)
                    i = 0

                    do while(d_sum > prec_low .and. i < i_max)
                        i = i + 1
                        sum_gases_vmr_tmp = sum(gases_vmr_tmp)

                        a = 1d0 / sum_gases_vmr_tmp

                        gases_vmr_tmp(:id-1) = gases_vmr_tmp(:id-1) * a
                        gases_vmr_tmp(id+1:) = gases_vmr_tmp(id+1:) * a

                        d_sum = abs(sum(gases_vmr_tmp) - sum_gases_vmr_tmp)
                    end do

                    gases_vmr(:, ip) = gases_vmr_tmp(:)

                    call update_gas_element_abd()
                end subroutine update_vmr

                subroutine update_gases_vmr()
                    implicit none

                    integer :: i
                    doubleprecision :: sum_tmp

                    sum_tmp = sum(gases_vmr(:, ip))

                    do i = 1, n_gases
                        gases_vmr(i, ip) = gases_vmr(i, ip) / sum_tmp
                    end do

                    call update_gas_element_abd()
                end subroutine update_gases_vmr

                subroutine interp_standard_gibbs_free_energy()
                    implicit none

                    integer :: i

                    do i = 1, n_gases
                        gases_delta_g_i(i) = &
                            interp_ex_0d(t, temperatures_thermochemistry, gases_delta_g(i, :))
                    end do

                    do i = 1, n_condensates
                        condensates_delta_g_i(i) = &
                            interp_ex_0d(t, temperatures_thermochemistry, condensates_delta_g(i, :))
                    end do
                end subroutine interp_standard_gibbs_free_energy

                subroutine update_saturation_vmr(condensate_name, ip, saturation_vmr)
                    ! """
                    ! Update the saturation VMR
                    ! """
                    implicit none

                    character(len=*), intent(in) :: condensate_name
                    integer, intent(in) :: ip
                    doubleprecision, intent(in) :: saturation_vmr

                    do i = 1, n_condensates
                        if (trim(condensate_name) == trim(condensate_names(i))) then
                            vmr_sat_condensates(i, ip) = saturation_vmr

                            return
                        end if
                    end do

                    write(*, '("Error: chemistry: condensate ''", A, "'' not recognized")') trim(condensate_name)
                    stop
                end subroutine update_saturation_vmr

                subroutine set_condensate_parameters(&
                        condensate_name, layer_condensation, pressure_condensation, vmr_condensation &
                    )
                    implicit none

                    character(len=*), intent(in) :: condensate_name
                    integer, intent(in) :: layer_condensation
                    doubleprecision, intent(in) :: pressure_condensation, vmr_condensation

                    integer :: i

                    do i = 1, n_condensates
                        if(trim(condensate_name) == trim(condensate_names(i))) then
                            layer_condensates(i) = layer_condensation
                            p_c_condensates(i) = pressure_condensation
                            vmr_c_condensates(i) = vmr_condensation

                            return
                        end if
                    end do

                    write(*, '("Error: chemistry: condensate ''", A, "'' not recognized")') trim(condensate_name)
                    stop
                end subroutine set_condensate_parameters

                subroutine simple_condensation(&
                    condensing_species, vmr_gas, vmr_gas_old, g, ga, species_is_condensed &
                )
                    implicit none

                    character(len=*), intent(in) :: condensing_species
                    doubleprecision, intent(in):: vmr_gas_old
                    doubleprecision, intent(in), dimension(2) :: ga
                    doubleprecision, intent(in), dimension(2, n_thermochemical_data) :: g

                    logical, intent(inout) :: species_is_condensed
                    doubleprecision, intent(inout):: vmr_gas

                    integer :: i, ip_tmp
                    doubleprecision :: g_c(2), k_eq_lvl, k_eq_old

                    ! A(s,l) -> A(g)
                    k_eq_lvl = exp((ga(1) - ga(2)) * 1d3 / (cst_R * t)) / p

                    if (vmr_gas > k_eq_lvl) then
                        if (.not. species_is_condensed) then
                            species_is_condensed = .True.

                            call get_pold_told(ip, pold, told)

                            do i = 1, 2
                                g_c(i) = interp_ex_0d(told, temperatures_thermochemistry, g(i, :))
                            end do

                            k_eq_old = exp((g_c(1) - g_c(2)) * 1d3 / (cst_R * told)) / pold

                            frac = log(vmr_gas_old / k_eq_old) / log(k_eq_lvl / k_eq_old)

                            if (frac < - t / (told - t)) then
                                frac = - t / (told - t) * (1d0 - 1d-3)
                            end if

                            tc = 1d0 / ((1d0 - frac) / told + frac / t)

                            if (frac > log(huge(0d0)) / log(p)) then
                                pc = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                            else
                                pc = pold ** (1d0 - frac) * p ** frac
                            end if

                            if(pc <= pbar) then
                                ip_tmp = ip
                            else
                                ip_tmp = ip - 1
                            end if

                            write(*, condensation_fmt) trim(condensing_species), ip_tmp, pc, tc, vmr_gas

                            call set_condensate_parameters(trim(condensing_species), ip_tmp, pc, vmr_gas)
                        end if

                        call update_vmr(condensing_species, k_eq_lvl)
                    end if

                    call update_saturation_vmr(trim(condensing_species), ip, vmr_gas)
                end subroutine simple_condensation

                subroutine a_h2_to_c_d_condensation_reaction(&
                    condensing_species, h2_vmr, vmr_3, vmr_4, vmr_old, nu, g, ga, a_is_condensed, product_d_is_h2o &
                )
                    ! """
                    ! Check if for the formation of a condensate, calculate the pressure and temperature of condensation
                    ! and update VMR of the reactants accordingly.
                    ! Accurate only in hydrogen-dominated atmospheres.
                    ! Reaction:
                    !   nu_1 A(s,l) + nu_2 H2 -> nu_3 C + nu_4 D
                    ! The first species (A) is the condensate, the 2nd species must be H2.
                    ! Reaction is considered from left to right only for numerical convenience.
                    ! :param condensing_species: name of the condensing species A(s)
                    ! :param h2_vmr: volume mixing ratio of H2
                    ! :param vmr_3: volume mixing ratio of species C
                    ! :param vmr_4: volume mixing ratio of species D
                    ! :param vmr_old: volume mixing ratio of the species at level below the current one (ip -1)
                    ! :param nu: stoechiometric coefficients of the species
                    ! :param g: temperature-dependent standard Gibb's free energy of the species
                    ! :param ga: standard Gibb's free energy of the species at the temperature of the current level
                    ! :param a_is_condensed: if True, species A already condensed in a previous level
                    ! :param product_d_is_h2o: if True, update C, O and Si abundance based on product D parameters
                    ! """
                    implicit none

                    logical, intent(in) :: product_d_is_h2o
                    character(len=*), intent(in) :: condensing_species
                    integer, intent(in), dimension(4) :: nu
                    doubleprecision, intent(in) :: h2_vmr
                    doubleprecision, intent(in), dimension(4) :: vmr_old, ga
                    doubleprecision, intent(in), dimension(4, n_thermochemical_data) :: g

                    logical, intent(inout) :: a_is_condensed
                    doubleprecision, intent(inout) :: vmr_3, vmr_4

                    integer :: i, ii, ip_temp
                    doubleprecision :: k_eq_lvl, k_eq_old, k_eq_c, g_c(4), vmr(4)

                    ! nu_1 A(s) + nu_2 H2 -> nu_3 C + nu_4 D
                    if((nu(1) * ga(1) - nu(3) * ga(3) - nu(4) * ga(4)) * 1d3 / (cst_R * t) < log(tiny(0d0))) then
                        k_eq_lvl = tiny(0d0)
                    else
                        k_eq_lvl = max(&
                            exp(&
                                (nu(1) * ga(1) - nu(3) * ga(3) - nu(4) * ga(4)) &
                                * 1d3 / (cst_R * t) &
                            ) * h2_vmr ** nu(2) * p ** (nu(2) - nu(3) - nu(4)), &
                            (1d0 + 2d-6) * tiny(0d0) &
                        )
                    end if

                    vmr = [0d0, h2_vmr, vmr_3, vmr_4]

                    if (vmr(3) ** nu(3) * vmr(4) ** nu(4) > k_eq_lvl) then
                        if (.not. a_is_condensed) then
                            a_is_condensed = .True.

                            call get_pold_told(ip, pold, told)

                            do i = 1, 4
                                g_c(i) = interp_ex_0d(told, temperatures_thermochemistry, g(i, :))
                            end do

                            ! Reaction at level below
                            k_eq_old = max(&
                                exp(&
                                    (nu(1) * g_c(1) - nu(3) * g_c(3) - nu(4) * g_c(4)) &
                                    * 1d3 / (cst_R * told) &
                                ) * h2_vmr ** nu(2) * pold ** (nu(2) - nu(3) - nu(4)), &
                                (1d0 + 1d-6) * tiny(0d0) &
                            )

                            ! Find precise T and p of condensation
                            frac = (log(vmr_old(3) ** nu(3) * vmr_old(4) ** nu(4)) - &
                                    log(k_eq_old)) / (log(k_eq_lvl) - log(k_eq_old))

                            ! Ensure 0 < tc < inf
                            if (frac < - t / (told - t)) then
                                frac = - t / (told - t) * (1d0 - 1d-3)
                            end if

                            tc = 1d0 / ((1d0 - frac) / told + frac / t)

                            if (frac > log(huge(0d0)) / log(p)) then
                                pc = exp(log(pold) * (1d0 - frac) + log(p) * frac)
                            else
                                pc = pold ** (1d0 - frac) * p ** frac
                            end if

                            do ii = 1, 15
                                do i = 1, 4
                                    g_c(i) = interp_ex_0d(tc, temperatures_thermochemistry, g(i, :))
                                end do

                                ! Reaction at level of condensation
                                k_eq_c = max(&
                                    exp(&
                                        (nu(1) * g_c(1) - nu(3) * g_c(3) - nu(4) * g_c(4)) &
                                        * 1d3 / (cst_R * tc) &
                                    ) * h2_vmr ** nu(2) * pc ** (nu(2) - nu(3) - nu(4)), &
                                    tiny(0d0) &
                                )

                                alf = k_eq_c / (vmr(3) ** nu(3) * vmr(4) ** nu(4))

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                call update_c_d_vmr(vmr(3), vmr(4), nu(3), nu(4), alf, product_d_is_h2o)

                                frac = (log(vmr(3) ** nu(3) * vmr(4) ** nu(4)) - log(k_eq_c)) / &
                                    (log(k_eq_old) - log(k_eq_c))
                                tc = 1d0 / ((1d0 - frac) / tc + frac / told)
                                pc = exp((1d0 - frac) * log(pc) + frac * log(pold))
                            end do

                            if(pc <= pbar) then
                                ip_temp = ip
                            else
                                ip_temp = ip - 1
                            end if

                            write(*, condensation_fmt) trim(condensing_species), ip_temp, pc, tc, vmr(3)

                            call set_condensate_parameters(trim(condensing_species), ip_temp, pc, vmr(3))

                            do ii = 1, 15
                                alf = k_eq_lvl / (vmr(3) ** nu(3) * vmr(4) ** nu(4))

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                call update_c_d_vmr(vmr(3), vmr(4), nu(3), nu(4), alf, product_d_is_h2o)
                            end do
                        else
                            do ii = 1, 15
                                alf = k_eq_lvl / (vmr(3) ** nu(3) * vmr(4) ** nu(4))

                                if(abs(alf - 1d0) < 1d-8) then
                                    exit
                                end if

                                call update_c_d_vmr(vmr(3), vmr(4), nu(3), nu(4), alf, product_d_is_h2o)
                            end do
                        end if
                    end if

                    vmr_3 = vmr(3)
                    vmr_4 = vmr(4)

                    call update_saturation_vmr(trim(condensing_species), ip, vmr_3)
                end subroutine a_h2_to_c_d_condensation_reaction

                subroutine update_c_d_vmr(vmr_3, vmr_4, nu_3, nu_4, alf, product_d_is_h2o)
                    implicit none

                    logical, intent(in) :: product_d_is_h2o
                    integer, intent(in) :: nu_3, nu_4
                    doubleprecision, intent(in) :: alf
                    doubleprecision, intent(inout) :: vmr_3, vmr_4

                    doubleprecision :: vmr_3_tmp, vmr_4_tmp, vmr_4_tmp_2

                    ! Find which species limitate the formation of the refactory species
                    vmr_3_tmp = vmr_3
                    vmr_4_tmp_2 = vmr_4

                    if (product_d_is_h2o) then
                        vmr_4_tmp = gas_element_abd(8, ip)
                    else
                        vmr_4_tmp = vmr_4
                    end if

                    if (alf < 1d0) then
                        alpha = min(&
                            vmr_3 * (1d0 - alf ** (1d0 / nu_3)) / nu_3, &
                            vmr_4_tmp * (1d0 - alf ** (1d0 / nu_4)) / nu_4&
                        )
                    else
                        alpha = max(&
                            vmr_3 * (1d0 - alf ** (1d0 / nu_3)) / nu_3, &
                            vmr_4_tmp * (1d0 - alf ** (1d0 / nu_4)) / nu_4&
                        )
                    end if

                    ! Update abundances
                    if (vmr_3 - nu_3 * alpha > tiny(0d0)) then
                        vmr_3 = vmr_3 - nu_3 * alpha
                    else
                        vmr_3 = vmr_3 * alf ** (1d0 / nu_3)
                    end if

                    if (vmr_4_tmp - nu_4 * alpha > tiny(0d0)) then
                        vmr_4_tmp = vmr_4_tmp - nu_4 * alpha
                    else
                        vmr_4_tmp = vmr_4_tmp * alf ** (1d0 / nu_4)
                    end if

                    if (product_d_is_h2o) then
                        gas_element_abd(8, ip) = vmr_4_tmp

                        call update_c_o_si(k_eq_co, k_eq_ch3, k_eq_co2, k_eq_sih4)

                        !alpha = (vmr_4_tmp_2 - gases_vmr(gas_id('H2O'), ip)) / nu_4

                        !vmr_4 = gases_vmr(gas_id('H2O'), ip)
                    else
                        vmr_4 = vmr_4_tmp
                    end if
                end subroutine update_c_d_vmr
        end subroutine calculate_chemistry


        function count_element(symbol, molecule_formula) result(n_element)
            ! """
            ! Count the number of atoms of a given element in a molecule.
            ! The molecule formula must be of format XYaZb where X, Y and Z are elements symbols and a and b are the
            ! number of the element on the left of the number.
            ! Formula including isotopes such as '15N' may give wrong results.
            ! Examples:
            !   print *, count_element('C', 'CH4')  ! output 1
            !   print *, count_element('Al', 'Al2O3')  ! output 2
            !   print *, count_element('H', 'NH4SH')  ! output 5
            !   print *, count_element('S', 'SiS')  ! output 1
            !   print *, count_element('13C', '13CH4')  ! output 1 (right)
            !   print *, count_element('13C', '13C18O')  ! output 18 (wrong)
            !   print *, count_element('N', '14N15N')  ! output 16 (wrong)
            ! :param symbol: symbol of the element
            ! :param molecule formula: formula of the molecule
            ! """
            implicit none

            character(len=*), intent(in) :: symbol
            character(len=*), intent(in) :: molecule_formula

            character(len=10), parameter :: str_num = '0123456789'  ! string containing all digits
            character(len=26), parameter :: str_low = 'abcdefghijklmnopqrstuvwxyz'  ! string containing lower case letters

            integer :: i, i_sym, i_num, n_element, size_symbol, size_formula, add_num

            ! Initialization
            size_symbol = len_trim(symbol)
            size_formula = len_trim(molecule_formula)
            n_element = 0
            i_sym = 1

            ! Search in the molecule formula
            do
                i = index(molecule_formula(i_sym:), symbol)

                if(i == 0) then  ! element not found in formula
                    return
                end if

                ! Search for number at the right of the element symbol
                i_sym = i_sym + i + size_symbol - 1
                i = 0

                do while(i_sym + i <= size_formula)
                    i_num = index(str_num, molecule_formula(i_sym + i:i_sym + i))

                    if(i_num > 0) then  ! a digit was found
                        i = i + 1  ! move right into the formula string
                    else  ! no digits were found
                        i_num = index(str_low, molecule_formula(i_sym + i:i_sym + i))

                        if(i_num > 0) then  ! next character is lower case: wrong element
                            i = -1
                        end if

                        exit
                    end if

                end do

                if (i == -1) then  ! false positive
                    add_num = 0
                else if (i == 0) then
                    add_num = 1
                else
                    i = i - 1
                    read(molecule_formula(i_sym:i_sym + i), *) add_num  ! convert str to integer
                end if

                n_element = n_element + add_num  ! add and loop to search for another occurence of the symbol
                i_sym = i_sym + 1
            end do

            return
        end function count_element


        function count_all_elements(molecule_formula) result(n_elements_arr)
            implicit none

            character(len=*), intent(in) :: molecule_formula

            integer :: i, n_elements_arr(n_elements)

            n_elements_arr = 0

            do i = 1, n_elements
                n_elements_arr(i) = count_element(trim(elements_symbol(i)), trim(molecule_formula))
            end do

            return
        end function count_all_elements


        subroutine calculate_gases_molar_mass()
            implicit none

            integer :: i, j

            do i = 1, n_gases
                elements_in_gases(i, :) = count_all_elements(gases_names(i))
                gases_molar_mass(i) = 0d0

                do j = 1, n_elements
                    gases_molar_mass(i) = gases_molar_mass(i) + elements_in_gases(i, j) * elements_molar_mass(j)
                end do
            end do
        end subroutine calculate_gases_molar_mass


        function calculate_species_molar_mass(species_name) result(molar_mass)
            ! """
            ! Calculate the species molar mass from its name/formula.
            ! Example: 'NH4SH' -> mu_NH4SH = 1 * mu_N + 5 * mu_H + 1 * mu_S
            ! :param species_name: species name
            ! :return molar_mass: (kg.mol-1) the molar mass of the species
            ! """
            implicit none

            character(len=species_name_size), intent(in) :: species_name
            doubleprecision :: molar_mass

            molar_mass = sum(count_all_elements(species_name) * elements_molar_mass(:))

            return
        end function calculate_species_molar_mass


        function condensate_id(condensate_name) result(idx)
            implicit none

            character(len=*), intent(in) :: condensate_name

            integer :: i, idx

            do i = 1, n_condensates
                if(trim(condensate_name) == trim(condensate_names(i))) then
                    idx = i

                    return
                end if
            end do

            write(*, '("Error: chemistry: condensate ''", A, "'' not implemented")') trim(condensate_name)
            stop
        end function condensate_id


        function equilibrium_constant_gases(stoichiometric_coefficients, delta_g_species, &
                n_species_reaction, &
                pressure, temperature) result(k_eq)
            ! """
            ! Get the equilibrium constant of a chemical reaction involving only gases.
            ! Given the reaction:
            !   aA + bB + ... -> cC + dD + ...,
            ! we have:
            !   K_eq = [C]**c * [D]**d * ... / ([A]**a * [B]**b * ...)
            ! and:
            !   K_eq = exp(-Delta_G / (R*T)) / p (c + d + ... - a - b - ...)
            ! with:
            !   DeltaG = c * g_C + d * g_D + ... - a * g_A - b * g_B
            ! :param stoichiometric_coefficients: stoichiometric coeffs of each species (+ for prods., - for reacts.)
            ! :param delta_g_species: delta G of each involved species at the temperature of the reaction
            ! :param n_species_reaction: number of species in the reaction
            ! :param p: (bar) pressure
            ! :param t: (K) pressure
            ! :return k_eq: the equilibrium constant of the reaction
            ! """
            use physics, only: cst_R

            implicit none

            integer, intent(in) :: n_species_reaction

            integer, dimension(n_species_reaction), intent(in) :: stoichiometric_coefficients
            doubleprecision, dimension(n_species_reaction), intent(in) :: delta_g_species
            doubleprecision, intent(in) :: pressure, temperature

            integer :: i, p_power
            doubleprecision :: k_eq, delta_g_reaction

            delta_g_reaction = 0d0
            p_power = 0

            do i = 1, n_species_reaction
                delta_g_reaction = delta_g_reaction + &
                    stoichiometric_coefficients(i) * delta_g_species(i) * 1d3  ! kJ to J
                p_power = p_power - stoichiometric_coefficients(i)
            end do

            k_eq = exp(-delta_g_reaction / (cst_R * temperature)) * pressure ** p_power

            return
        end function equilibrium_constant_gases


        function gas_id(gas_name) result(idx)
            implicit none

            character(len=*), intent(in) :: gas_name

            integer :: i, idx

            do i = 1, n_gases
                if(trim(gas_name) == trim(gases_names(i))) then
                    idx = i

                    return
                end if
            end do

            write(*, '("Error: chemistry: gas ''", A, "'' not implemented")') trim(gas_name)
            error stop
        end function gas_id


        subroutine partition_o_mg_si(ii, alf1, alf2, alpha1, alf, alpha, qmg, asi, ao, ic)
            implicit none

            integer, intent(in) :: ii, ic
            doubleprecision, intent(in) :: alf, qmg, asi, ao
            doubleprecision, intent(inout) :: alf1, alf2, alpha1, alpha

            doubleprecision :: x

            if(ii <= 2) then
                alf1 = alf
                alpha1 = alpha
            else
                alf2 = alf

                if (alf1 > alf2 * (1d0 - prec_high) .and. alf1 < alf2 * (1d0 + prec_high)) then
                    x = huge(0.)
                else
                    x = -alpha1 * log(alf2) / log(alf2 / alf1)
                end if

                if(ic == 1) then
                    alpha = &
                        min(x, 0.4 * qmg, 0.8 * asi, 0.2 * ao)
                else if(ic == 2) then
                    alpha = &
                        min(x, 0.8 * qmg, 0.8 * asi, 0.2667 * ao)
                else
                    alpha = min(x, 0.8 * asi, 0.4 * ao)
                end if

                alf1 = alf2
                alpha1 = alpha
            end if
        end subroutine partition_o_mg_si


        ! Saturation pressures
        double precision function h2o_saturation_pressure(temperature)
            ! """
            ! Calculate the H2O pressure of saturation  at a given temperature.
            ! Sources:
            !   - Fray & Schmitt 2009 (Ice-I deposition)
            !   - Wagner and Pruss 1993 (condensation)
            !   - Lin et al. 2004 (Ice-VII deposition)
            ! :param temperature: (K) temperature
            ! """
            implicit none

            doubleprecision, intent(in) :: &
                temperature  ! (K) temperature

            double precision, parameter :: &
                temperature_triple_point = 273.16d0, &  ! (K) temperature of H2O triple point (F&S 2009)
                pressure_triple_point = 6.11657d-3, & ! (bar) pressure of H2O triple point (Fray & Schmitt 2009)
                temperature_critical_point = 647.096d0, &  ! (K) temperature of H2O critical point (IAPWS 2011)
                pressure_critical_point = 220.64d0, &  ! (bar) pressure of H2O critical point (IAPWS 2011)
                temperature_ice7_triple_point = 355, &  ! (K) temperature of the liquid-ice6-ice7 H2O triple point
                pressure_ice7_triple_point = 2.17d4  ! (bar) pressure of the liquid-ice6-ice7 H2O triple point

            doubleprecision, parameter :: &
                pressure_c = 0.85d4, &  ! (bar) fitting paremeter pressure (Lin et al. 2004)
                alpha = 3.47  ! fitting parameter (Lin et al. 2004)

            double precision, parameter, dimension(6) :: &
                a = [-7.85951783D0, 1.84408259D0, -11.7866497D0, 22.6807411D0, -15.9618719D0, 1.80122502D0], &
                tau_pow = [1D0, 1.5D0, 3D0, 3.5D0, 4D0, 7.5D0]
                ! Coefficients from Wagner and Pruss 1993 (IAPWS 2011)
            double precision, parameter, dimension(7) :: &
                e = [20.996967D0, 3.724375D0, -13.920548D0, 29.698877D0, -40.197239D0, 29.788048D0, -9.130510D0]
                ! Coefficients from Feistel and Wagner 2007

            integer :: &
                i  ! index
            double precision :: &
                eta, &  ! intermediate factor
                tau     ! intermediate factor

            if (temperature <= temperature_triple_point) then
                ! Gas <-> Solid (ice-I) profile (Feistel and Wagner 2007)
                eta = 0D0

                do i = 1, size(e)
                    eta = eta + e(i) * (temperature / temperature_triple_point) ** (i - 1)
                end do

                h2o_saturation_pressure = pressure_triple_point * &
                    exp(3D0 / 2D0 * log(temperature / temperature_triple_point) + &
                        (1D0 - temperature_triple_point / temperature) * eta)
            elseif (temperature <= temperature_critical_point) then
                ! Gas <-> Liquid profile (Wagner and Pruss 1993)
                ! This formula misses the triple point by less than 0.08 percent
                tau = 1D0 - temperature / temperature_critical_point
                eta = 0D0

                do i = 1, size(a)
                    eta = eta + a(i) * tau ** tau_pow(i)
                end do

                h2o_saturation_pressure = pressure_critical_point * &
                    exp(temperature_critical_point / temperature * eta)
            else
                ! Gas <-> Solid (ice-VII) profile (Lin et al. 2004)
                h2o_saturation_pressure = &
                    pressure_c * ((temperature / temperature_ice7_triple_point) ** alpha - 1d0) + &
                    pressure_ice7_triple_point
            end if
        end function h2o_saturation_pressure


        double precision function nh3_saturation_pressure(temperature)
            ! """
            ! Calculate the NH3 pressure of saturation at a given temperature.
            ! Sources: Fray & Schmitt 2009, Lide 2006 et al. 2006.
            ! :param temperature: (K) temperature
            ! """
            use math, only: interp

            implicit none

            double precision, parameter :: &
                temperature_triple_point = 195.41D0, &  ! (K) temperature of NH3 triple point (F&S 2009)
                pressure_triple_point = 6.09D-2, & ! (bar) pressure of NH3 triple point (Fray & Schmitt 2009)
                temperature_critical_point = 405.5D0, &  ! (K) temperature of NH3 critical point (Lide 2006)
                pressure_critical_point = 113.5D0  ! (bar) pressure of NH3 critical point (Lide 2006)

            double precision, parameter, dimension(5) :: &
                a = [1.596E1, -3.537E3, -3.310E4, 1.742E6, -2.995E7]
            double precision, parameter, dimension(23) :: &
                saturation_pressures_ref = [&
                    pressure_triple_point, &
                    8.7D0 * 1D-2, &
                    12.6D0 * 1D-2, &
                    17.9D0 * 1D-2, &
                    24.9D0 * 1D-2, &
                    34.1D0 * 1D-2, &
                    45.9D0 * 1D-2, &
                    60.8D0 * 1D-2, &
                    79.6D0 * 1D-2, &
                    103D0 * 1D-2, &
                    131D0 * 1D-2, &
                    165D0 * 1D-2, &
                    207D0 * 1D-2, &
                    256D0 * 1D-2, &
                    313D0 * 1D-2, &
                    381D0 * 1D-2, &
                    460D0 * 1D-2, &
                    552D0 * 1D-2, &
                    655D0 * 1D-2, &
                    774D0 * 1D-2, &
                    909D0 * 1D-2, &
                    1062D0 * 1D-2, &
                    pressure_critical_point &
                    ], &
                temperatures_ref = [&
                    temperature_triple_point, &
                    200D0, &
                    205D0, &
                    210D0, &
                    215D0, &
                    220D0, &
                    225D0, &
                    230D0, &
                    235D0, &
                    240D0, &
                    245D0, &
                    250D0, &
                    255D0, &
                    260D0, &
                    265D0, &
                    270D0, &
                    275D0, &
                    280D0, &
                    285D0, &
                    290D0, &
                    295D0, &
                    300D0, &
                    temperature_critical_point &
                    ]
                ! Data from The CRC Handbook of Chemistry and Physics (Lide 2006)
            doubleprecision, intent(in) :: &
                temperature  ! (K) temperature

            integer :: &
                i  ! index

            double precision, dimension(1) :: &
                pressure_interp

            if (temperature <= temperature_triple_point) then
                ! Coefficients from Fray and Schmitt 2009
                ! Not valid below 15 K, but extrapolated anyway
                nh3_saturation_pressure = 0D0

                do i = 1, size(a)
                    nh3_saturation_pressure = nh3_saturation_pressure + a(i) / temperature ** (i - 1)
                end do

                nh3_saturation_pressure = exp(nh3_saturation_pressure)
            elseif (temperature <= temperature_critical_point) then
                pressure_interp = interp(log([temperature]), log(temperatures_ref), log(saturation_pressures_ref))
                nh3_saturation_pressure = exp(pressure_interp(1))
            else
                nh3_saturation_pressure = 1d10
            end if
        end function nh3_saturation_pressure


        doubleprecision function nh4sh_saturation_pressure(temperature)
            ! """
            ! Calculate the NH4SH pressure of saturation at a given temperature.
            ! Source: D. R. Stull, 1947 (http://terpconnect.umd.edu/~nsw/chbe301/chap02.pdf, Table 2-7, "NH4HS")
            ! :param temperature: (K) temperature
            ! """
            implicit none

            doubleprecision :: temperature

            nh4sh_saturation_pressure = 1d1 ** (1.4581d1 - 4.6363d3 / temperature) * 1.01325d0  ! atm to bar
        end function nh4sh_saturation_pressure
end module chemistry
