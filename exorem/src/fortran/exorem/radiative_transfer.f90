module radiative_transfer
    implicit none

    save

    contains
        subroutine calculate_radiative_transfer(&
            gases_vmr, &
            pressures_layers, temperatures_layers, gravities_layers, species_vmr_layers, &
            pressures, temperatures, &
            light_source_irradiance, &
            n_species, i_single_species, &
            wavenumber_min, wavenumber_step,&
            n_clouds, cloud_vmr, tau_cloud, cloud_particle_density, cloud_particle_radius, &
            n_k_pressures, n_k_temperatures, n_k_wavenumbers, &
            wavenumbers_k, ng, &
            p_k_species, t_k_species, weights_k, samples_k, kcoeff_species, &
            h2_h2_cia, h2_he_cia, h2o_n2_collision_induced_absorption, h2o_h2o_collision_induced_absorption, &
            radiosity_internal, matrix_t, flux, spectral_radiosity, wavenumbers &
        )
            ! """
            ! Radiative transfer code for giant planets.
            ! """
            use atmosphere, only: n_levels, n_layers, tau, tau_rayleigh, scale_height
            use chemistry, only: n_gases, gas_id
            use cloud, only: asymetry_factor, single_scattering_albedo, &
                q_ext, q_ext_ref, q_scat, update_cloud_optical_parameters
            use exorem_interface, only: load_cloud_optical_constants
            use k_coefficients_interface_h5, only: n_k_samples_max, n_k_pressures_max, n_k_temperatures_max, &
                n_k_wavenumbers_max
            use math, only: pi, interp_ex_0d
            use physics, only: cst_p0, cst_t0, cst_n0, spherical_black_body_spectral_radiance
            use species, only: rayleigh_scattering_coefficients
            use spectrometrics, only: n_wavenumbers

            implicit none

            integer, intent(in) :: n_species, n_clouds, &
                n_k_pressures(n_species), n_k_temperatures(n_species), n_k_wavenumbers(n_species), ng(n_species), &
                i_single_species
            doubleprecision, intent(in) :: &
                gases_vmr(n_gases, n_layers), &
                pressures_layers(n_layers), temperatures_layers(n_layers), gravities_layers(n_layers), &
                species_vmr_layers(n_layers, n_species), &
                pressures(n_levels), temperatures(n_levels), &
                light_source_irradiance(n_wavenumbers), &
                wavenumber_min, wavenumber_step, cloud_vmr(n_clouds, n_layers), &
                cloud_particle_density(n_clouds), wavenumbers_k(n_k_wavenumbers_max, n_species), &
                p_k_species(n_k_pressures_max, n_species), &
                t_k_species(n_k_temperatures_max, n_k_pressures_max, n_species), &
                weights_k(n_k_samples_max), &
                samples_k(n_k_samples_max), &
                kcoeff_species(n_k_samples_max, n_k_wavenumbers_max, n_k_temperatures_max, n_k_pressures_max, n_species), &
                wavenumbers(n_wavenumbers)
            doubleprecision, dimension(n_layers, n_wavenumbers), intent(in) :: &
                h2_h2_cia, h2_he_cia, h2o_n2_collision_induced_absorption, h2o_h2o_collision_induced_absorption

            doubleprecision, intent(out) :: tau_cloud(n_clouds, n_layers), &
                cloud_particle_radius(n_clouds, n_layers), radiosity_internal(n_levels), &
                matrix_t(n_levels, n_levels), flux(n_levels, n_wavenumbers), spectral_radiosity(n_levels, n_wavenumbers)

            integer :: i, i_min, ic, ig, ig1, ig2, ij, ik, indg, ipk1, ipk2, itk1, itk2, iw, j, jj, &
                tab_i(n_k_pressures_max), tab_j(n_k_temperatures_max)
            doubleprecision :: matrix_t2(n_levels, n_levels), qh2(n_layers), qhe(n_layers), qh2o(n_layers)
            doubleprecision :: pkd(n_k_pressures_max), tkd(n_k_temperatures_max)
            doubleprecision :: dtauc(n_wavenumbers), &
                    dtauk(n_k_samples_max, n_wavenumbers), dtau(n_k_samples_max, n_wavenumbers), &
                    dtausum(n_k_samples_max * n_k_samples_max), &
                    flux_scattering(n_levels, n_wavenumbers), fluxt_scattering(n_levels), &
                    flux_scattering_up(n_levels, n_wavenumbers), flux_scattering_down(n_levels, n_wavenumbers), &
                    fluxt_scattering_up(n_levels), fluxt_scattering_down(n_levels), &
                    df_scattering(n_levels, n_levels)
            doubleprecision :: pl_lev(n_levels, n_wavenumbers), dpl_lev(n_levels, n_wavenumbers), &
                    pl_lay(n_levels, n_wavenumbers), dpl_lay(n_levels, n_wavenumbers), tl2(n_levels), pl_lev2(n_levels)
            doubleprecision :: &
                    wmix(n_k_samples_max * n_k_samples_max), &
                    gfactor_scattering(n_layers), omeg_scattering(n_layers), dtau_scattering(n_layers), &
                    dtau_rayleigh(n_layers)
            doubleprecision :: amedg, amedk, apj, cloudj, cmam(n_layers), fac_cont, fipk1, fitk1, h0(n_layers), &
                pj, qlj, tab_pk, tab_tk, tab_tk1, tab_tk2, taucl_scat_tot, tj, fracg

            ! Initialisation
            if (n_clouds <= 0) then
                write(*, '("Calculating radiative transfer without clouds...")')
            else
                write(*, '("Calculating radiative transfer with clouds...")')
            end if

            qh2(:) = gases_vmr(gas_id('H2'), :)
            qhe(:) = gases_vmr(gas_id('He'), :)
            qh2o(:) = gases_vmr(gas_id('H2O'), :)

            do i = 1, n_k_pressures_max
                tab_i(i) = i
            end do

            do i = 1, n_k_temperatures_max
                tab_j(i) = i
            end do

            if(n_k_samples_max > 1) then  ! k-coefficients have more than one sample
                do ig = 1, n_k_samples_max
                    if(samples_k(ig) > 0.5d0) then
                        exit
                    end if
                end do

                indg = ig - 1
                fracg = (samples_k(ig) - 0.5d0) / (samples_k(ig) - samples_k(ig - 1))
            else  ! k-coefficients have one sample (variables will not be used)
                indg = 0
                fracg = 0d0
            end if

            ! Update cloud optical parameters
            if(n_clouds > 0) then
                call update_cloud_optical_parameters()
            end if

            ! Calculation of the optical depths in the layers
            tau(:, :, :) = 0d0

            do j = n_layers, 1, -1
                tj = temperatures_layers(j)
                pj = pressures_layers(j)
                apj = log(pressures_layers(j))

                h0(j) = 1d5 * scale_height(j) * cst_t0 / tj  ! km to cm
                cmam(j) = cst_n0 * 1d-6 * &
                    (pressures(j) - pressures(j + 1)) / (cst_p0 * 1d-2)  ! N_Loschmidt (cm-3), p_Loschmidt (mbar)

                ! CIA contribution to opacity
                fac_cont = (cst_t0 / tj) * h0(j) * pj * &
                    (pressures(j) - pressures(j + 1)) / ((cst_p0 * 1d-2) ** 2d0)

                do i = 1, n_wavenumbers
                    dtauc(i) = fac_cont *&
                        (qh2(j) * (qh2(j) * h2_h2_cia(j, i) + qhe(j) * h2_he_cia(j, i)) + &
                         qh2o(j) * (qh2o(j) * h2o_h2o_collision_induced_absorption(j, i) + &
                                    (1 - qh2o(j)) * h2o_n2_collision_induced_absorption(j, i)) &
                        )
                end do

                ! Cloud opacity
                if(n_clouds > 0) then
                    do ik = 1, n_wavenumbers
                        cloudj = 0d0

                        do ic = 1, n_clouds
                            cloudj = cloudj + cloud_vmr(ic, j) / &
                                (4d0 / 3d0 * cloud_particle_density(ic) * cloud_particle_radius(ic, j) * &
                                gravities_layers(j) * 1d-2) * &
                                (pressures(j) - pressures(j + 1)) * 1d2 * q_ext(ic, ik, j)
                            cloudj = max(0d0, cloudj)
                        end do

                        dtauc(ik) = dtauc(ik) + cloudj
                    end do
                end if

                ! Iteration over the absorbers
                dtau(:, :) = 0d0

                do ik = 1, n_species
                    ! Cycle if the contribution of only 1 species is asked
                    if(ik /= i_single_species .and. i_single_species > 0) then
                        cycle
                    end if

                    ! Initialise k-coefficient parameters
                    qlj = species_vmr_layers(j, ik)

                    do ij = 1, n_k_pressures(ik)
                        pkd(ij) = log(p_k_species(ij, ik))
                    end do

                    tab_pk = interp_ex_0d(apj, pkd, dble(tab_i))

                    if(tab_pk <= 1d0) then
                        ipk1 = 1
                        fipk1 = 0d0
                        ipk2 = ipk1
                    else
                        if(tab_pk < n_k_pressures(ik)) then
                            ipk1 = idint(tab_pk)
                            fipk1 = 1d0 - tab_pk + dble(ipk1)
                            ipk2 = ipk1 + 1
                        else
                            ipk1 = n_k_pressures(ik)
                            fipk1 = 1d0
                            ipk2 = ipk1
                        end if
                    end if

                    do ij = 1, n_k_temperatures(ik)
                        tkd(ij) = log(t_k_species(ij, ipk1, ik))
                    end do

                    tab_tk1 = interp_ex_0d(log(tj), tkd, dble(tab_j))

                    do ij = 1, n_k_temperatures(ik)
                        tkd(ij) = log(t_k_species(ij, ipk2, ik))
                    end do

                    tab_tk2 = interp_ex_0d(log(tj), tkd, dble(tab_j))
                    tab_tk = fipk1 * tab_tk1 + (1d0 - fipk1) * tab_tk2

                    if(tab_tk <= 1d0) then
                        itk1 = 1
                        fitk1 = 0d0
                        itk2 = itk1
                    else
                        if(tab_tk < n_k_temperatures(ik)) then
                            itk1 = idint(tab_tk)
                            fitk1 = 1d0 - tab_tk + dble(itk1)
                            itk2 = itk1 + 1
                        else
                            itk1 = n_k_temperatures(ik)
                            fitk1 = 1d0
                            itk2 = itk1
                        end if
                    end if

                    ! Find the closest k-coefficient wavenumber to the minimum wavenumber
                    ! wavenumbers_k must be ordered
                    iw = 1

                    do
                        if(iw > n_k_wavenumbers(ik)) then
                            exit
                        end if

                        if(abs(wavenumbers_k(iw + 1, ik) - wavenumber_min) < &
                                abs(wavenumbers_k(iw, ik) - wavenumber_min)) then
                            iw = iw + 1
                        else
                            exit
                        end if
                    end do

                    i_min = 1

                    ! Find the closest wavenumber to the minimum k-coefficient wavenumber
                    if(iw == 1) then
                        do i_min = 1, n_wavenumbers
                            if(abs(wavenumbers_k(1, ik) - (wavenumber_min + wavenumber_step * dble(i_min))) > &
                               abs(wavenumbers_k(1, ik) - (wavenumber_min + wavenumber_step * dble(i_min - 1)))) then
                                exit
                            end if
                        end do
                    end if

                    do i = i_min, n_wavenumbers
                        ! Find the closest k-coefficient wavenumber to the current wavenumber
                        ! wavenumbers_k must be ordered
                        do
                            if(iw == n_k_wavenumbers(ik)) then
                                exit
                            end if

                            if(abs(wavenumbers_k(iw + 1, ik) - (wavenumber_min + wavenumber_step * dble(i - 1))) < &
                                    abs(wavenumbers_k(iw, ik) - (wavenumber_min + wavenumber_step * dble(i - 1)))) then
                                iw = iw + 1
                            else
                                exit
                            end if
                        end do

                        ! Exit if current wavenumber is greater than the maximum k-coefficient wavenumber
                        if(wavenumber_min + wavenumber_step * dble(i - 1) >= &
                                wavenumbers_k(n_k_wavenumbers(ik), ik) * (1d0 + 1d-6)) then
                            exit
                        end if

                        ! Species contribution
                        do ig = 1, ng(ik)
                            if(kcoeff_species(ig, iw, itk1, ipk1, ik) <= 1d-40.or.&
                                    kcoeff_species(ig, iw, itk2, ipk1, ik) <= 1d-40.or.&
                                    kcoeff_species(ig, iw, itk1, ipk2, ik) <= 1d-40.or.&
                                    kcoeff_species(ig, iw, itk2, ipk2, ik) <= 1d-40) then
                                dtauk(ig, i) = (fipk1 * (fitk1 * kcoeff_species(ig, iw, itk1, ipk1, ik) + &
                                        (1d0 - fitk1) * kcoeff_species(ig, iw, itk2, ipk1, ik)) + &
                                        (1d0 - fipk1) * (fitk1 * kcoeff_species(ig, iw, itk1, ipk2, ik) + &
                                        (1d0 - fitk1) * kcoeff_species(ig, iw, itk2, ipk2, ik))) * qlj * cmam(j) * h0(j)
                            else
                                dtauk(ig, i) = exp(fipk1 * (fitk1 * log(kcoeff_species(ig, iw, itk1, ipk1, ik)) + &
                                        (1d0 - fitk1) * log(kcoeff_species(ig, iw, itk2, ipk1, ik))) + &
                                        (1d0 - fipk1) * (fitk1 * log(kcoeff_species(ig, iw, itk1, ipk2, ik)) + &
                                        (1d0 - fitk1) * log(kcoeff_species(ig, iw, itk2, ipk2, ik)))) * &
                                        qlj * cmam(j) * h0(j)
                            end if
                        end do

                        ! Contribution of 2 or more species (assuming random line distribution between the 2 species)
                        if(ik == 1) then
                            do ig = 1, ng(1)
                                dtau(ig, i) = dtauk(ig, i)
                            end do
                        else if(n_k_samples_max > 1) then  ! k-coefficients have more than one sample
                            if(dtau(indg, i) < 1d-40 .or. dtau(indg + 1, i) < 1d-40) then
                                amedg = fracg * dtau(indg, i) + (1d0 - fracg) * dtau(indg + 1, i)
                            else
                                amedg = exp(fracg * log(dtau(indg, i)) + &
                                        (1d0 - fracg) * log(dtau(indg + 1, i)))
                            end if

                            if(dtauk(indg, i) < 1d-40.or.dtauk(indg + 1, i) < 1d-40) then
                                amedk = fracg * dtauk(indg, i) + (1d0 - fracg) * dtauk(indg + 1, i)
                            else
                                amedk = exp(fracg * log(dtauk(indg, i)) + &
                                        (1d0 - fracg) * log(dtauk(indg + 1, i)))
                            end if

                            if(amedg <= amedk * 1d-2 .and. dtau(ng(1), i) <= 1d-2 * dtauk(ng(1), i)) then
                                do ig = 1, ng(ik)
                                    dtau(ig, i) = dtauk(ig, i) + amedg
                                end do
                            else
                                if(amedk <= amedg * 1d-2 .and. dtauk(ng(1), i) <= 1d-2 * dtau(ng(1), i)) then
                                    do ig = 1, ng(ik)
                                        dtau(ig, i) = dtau(ig, i) + amedk
                                    end do
                                else
                                    ig = 0

                                    do ig1 = 1, ng(ik)
                                        do ig2 = 1, ng(ik)
                                            ig = ig + 1
                                            wmix(ig) = weights_k(ig1) * weights_k(ig2)
                                            dtausum(ig) = dtau(ig1, i) + dtauk(ig2, i)

                                            if(dtausum(ig) < 1d-40) then
                                                dtausum(ig) = log(1d-40)
                                            else
                                                dtausum(ig) = log(dtausum(ig))
                                            end if
                                        end do
                                    end do

                                    call sort_two_arrays(dtausum, wmix, ng(ik) * ng(ik))

                                    do ig = 2, ng(ik) * ng(ik)
                                        wmix(ig) = wmix(ig) + wmix(ig - 1)
                                    end do

                                    do ig = 1, ng(ik)
                                        dtau(ig, i) = exp(interp_ex_0d(samples_k(ig), wmix, dtausum))
                                    end do
                                end if
                            end if
                        else  ! k-coefficients have one sample
                            dtau(1, i) = dtau(1, i) + dtauk(1, i)
                        end if
                    end do
                end do

                ! Total opacity
                do i = 1, n_wavenumbers
                    do ig = 1, n_k_samples_max
                        tau(j, i, ig) = tau(j + 1, i, ig) + dtau(ig, i) + dtauc(i)
                    end do
                end do
            end do

            ! Calculation of the Planck function (erg s-1 cm-2 sr-1/cm-1) and derivative (erg s-1 cm-2 sr-1/cm-1/K)
            call planck(wavenumbers, temperatures, pl_lev, dpl_lev)

            do j = 2, n_levels
                tl2(j) = temperatures_layers(j - 1)
            end do

            tl2(1) = temperatures(1)

            call planck(wavenumbers, tl2, pl_lay, dpl_lay)

            ! Calculation of fluxes with scattering
            omeg_scattering(:) = 0d0
            gfactor_scattering(:) = 0d0
            fluxt_scattering(:) = 0d0
            fluxt_scattering_up(:) = 0d0
            fluxt_scattering_down(:) = 0d0
            dtau_scattering(:) = 0d0
            dtau_rayleigh(:) = 0d0
            matrix_t2(:, :) = 0d0
            df_scattering(:, :) = 0d0
            flux_scattering(:, :) = 0d0
            spectral_radiosity(:, :) = 0d0
            flux_scattering_up(:, :) = 0d0
            flux_scattering_down(:, :) = 0d0

            do i = 1, n_wavenumbers
                do ig = 1, n_k_samples_max
                    do j = n_layers, 1, -1
                        dtau_scattering(n_levels - j) = tau(j, i, ig) - tau(j + 1, i, ig)

                        dtau_rayleigh(n_levels - j) = sum(gases_vmr(:, j) * rayleigh_scattering_coefficients(:, i))
                        dtau_rayleigh(n_levels - j) = dtau_rayleigh(n_levels - j) * cmam(j) * h0(j)

                        tau_rayleigh(j, i) = tau_rayleigh(j + 1, i) + dtau_rayleigh(n_levels - j)

                        dtau_scattering(n_levels - j) = dtau_scattering(n_levels - j) + dtau_rayleigh(n_levels - j)
                        omeg_scattering(n_levels - j) = dtau_rayleigh(n_levels - j)
                        gfactor_scattering(n_levels - j) = 0d0

                        ! Cloud contribution
                        if(n_clouds > 0) then
                            taucl_scat_tot = tiny(0.) + dtau_rayleigh(n_levels - j)

                            do ic = 1, n_clouds
                                tau_cloud(ic, j) = &
                                    max(&
                                        cloud_vmr(ic, j) / &
                                            (4d0 / 3d0 * cloud_particle_density(ic) * &
                                             cloud_particle_radius(ic, j) * gravities_layers(j) * 1d-2) * &
                                            (pressures(j) - pressures(j + 1)) * 1d2, &
                                        0d0 &
                                    )

                                taucl_scat_tot = &
                                    taucl_scat_tot + &
                                    tau_cloud(ic, j) * q_scat(ic, i, j)
                                omeg_scattering(n_levels - j) = &
                                    omeg_scattering(n_levels - j) + &
                                    single_scattering_albedo(ic, i, j) * tau_cloud(ic, j) * q_ext(ic, i, j)
                                gfactor_scattering(n_levels - j) = &
                                    gfactor_scattering(n_levels - j) + &
                                    asymetry_factor(ic, i, j) * tau_cloud(ic, j) * q_scat(ic, i, j)
                            end do

                            ! Reference cloud opacity (for diagnostic)
                            do ic = 1, n_clouds
                                tau_cloud(ic, j) = tau_cloud(ic, j) * q_ext_ref(ic, j)
                            end do

                            gfactor_scattering(n_levels - j) = gfactor_scattering(n_levels - j) / taucl_scat_tot
                        end if

                        omeg_scattering(n_levels - j) = omeg_scattering(n_levels - j) / dtau_scattering(n_levels - j)
                        pl_lev2(n_levels + 1 - j) = pl_lev(j, i)
                    end do  ! layers

                    ! Note: in calculate_two_stream_fluxes, level 1 is the top level, not the bottom one
                    flux_scattering_up(n_levels, i) = 0d0
                    flux_scattering_down(n_levels, i) = 0d0
                    flux_scattering_down(1, i) = light_source_irradiance(i)
                    pl_lev2(1) = pl_lev(n_levels, i)

                    call calculate_two_stream_fluxes(n_layers, pl_lev2, dtau_scattering, omeg_scattering, &
                        gfactor_scattering, flux_scattering_up(:, i), flux_scattering_down(:, i), df_scattering)

                    do j = 1, n_levels
                        do jj = 1, n_levels
                            matrix_t2(jj, j) = matrix_t2(jj, j) + weights_k(ig) * &
                                df_scattering(n_levels + 1 - j, n_levels + 1 - jj) * &
                                dpl_lev(jj, i) * pi * wavenumber_step
                        end do

                        flux_scattering(j, i) = flux_scattering(j, i) + &
                            (flux_scattering_up(n_levels + 1 - j, i) - flux_scattering_down(n_levels + 1 - j, i)) * &
                            weights_k(ig) * pi
                        spectral_radiosity(j, i) = spectral_radiosity(j, i) + &
                            (flux_scattering_up(n_levels + 1 - j, i) - flux_scattering_down(n_levels + 1 - j, i) + &
                            light_source_irradiance(i)) * weights_k(ig) * pi
                        fluxt_scattering(j) = fluxt_scattering(j) + (flux_scattering_up(n_levels + 1 - j, i) - &
                            flux_scattering_down(n_levels + 1 - j, i)) * weights_k(ig) * pi * wavenumber_step
                        fluxt_scattering_up(j) = fluxt_scattering_up(j) + &
                            flux_scattering_up(n_levels + 1 - j, i) * wavenumber_step * weights_k(ig) * pi
                        fluxt_scattering_down(j) = fluxt_scattering_down(j) + &
                            flux_scattering_down(n_levels + 1 - j, i) * wavenumber_step * weights_k(ig) * pi
                    end do
                end do ! g-vector
            end do ! wavenumbers

            flux = flux_scattering
            radiosity_internal = fluxt_scattering
            matrix_t = matrix_t2
        end subroutine calculate_radiative_transfer


        subroutine calculate_two_stream_fluxes(n_layers, planck_function, dtau, single_scattering_albedos, &
            asymmetry_parameters, flux_upward, flux_downward, d_kernel)
            ! """
            ! Calculate the upward, downward and net thermal flux in an inhomogeneous absorbing, scattering atmosphere.
            ! The two_stream approximation (quadrature with cos of average angle = amu1) is used to find the diffuse
            ! reflectivity and transmissivity and the total upward and downward fluxes for each of the n_layers 
            ! homogeneous layers.
            ! The adding method is then used to combine these layers.
            ! If any layer is thicker than dtau = *emax1*, it is assumed to be semi-infinite. Layers thicker than
            ! dtau = *emax2*, are treated as infinite layers.
            !
            ! Note: to account for a diffuse flux at the top of the atmosphere, the user must set flux_downward(1) equal
            ! to that value. For no downward diffuse flux at the top of the atmosphere, the user must initialize 
            ! flux_downward(1) to zero in the calling program.
            !
            ! Source: unknown.
            !
            ! :param n_layers: number of homogeneous model layers
            ! :param planck_function: planck function at each level from top of the atmosphere (l=1) to the surface 
            !   (l=n_layers+1)
            ! :param dtau: array of normal incidence optical depths in each homogeneous model layer (n_layers values)
            ! :param single_scattering_albedos: array of single scattering albedos for each homogeneous model layer 
            !   (n_layers values)
            ! :param asymmetry_parameters: array of asymmetry parameters for each homogeneous model layer 
            !   (n_layers values)
            ! :return flux_upward: upward flux at n_layers+1 layer boundries 
            !   (flux_upward(l) is the upward flux at the top of layer l)
            ! :return flux_downward: downward flux at n_layers+1 layer boundries
            !   (flux_downward(l) is the downward flux at the top of layer l)
            ! :return d_kernel: kernel of upward flux minus downard flux
            ! """
            use atmosphere, only : n_levels

            implicit none

            integer, intent(in) :: n_layers
            doubleprecision, intent(in) :: dtau(n_levels - 1), asymmetry_parameters(n_levels - 1), &
                planck_function(n_levels)

            doubleprecision, intent(inout) :: single_scattering_albedos(n_levels - 1)

            doubleprecision, intent(out) :: flux_upward(n_levels), flux_downward(n_levels), d_kernel(n_levels, n_levels)

            doubleprecision, parameter :: prec = 1d-10, emax1 = 8d0, emax2 = 24d0, amu1 = 2d0 / 3d0
            doubleprecision :: planck_function_tmp(n_levels), om_p(n_levels - 1), dtau_p(n_levels - 1), ru(n_levels), &
                dd(n_levels), dflux(n_levels - 1), g1(n_levels - 1), g2(n_levels - 1), &
                du(n_levels - 1), ufl(n_levels), dfl(n_levels), uflux(n_levels), ekt(n_levels - 1), &
                sk(n_levels - 1), rl(n_levels), temperatures_layers(n_levels), rs(n_levels)

            integer :: j, l, np2
            doubleprecision :: skt, alb, denom, e2ktm, emis, downward_flux_top

            downward_flux_top = flux_downward(1)

            np2 = n_layers + 2

            ! Scale the optical depths, single scattering albedos and the scattering asymmetry factors for use in the
            ! two-stream approximation. Initialize other quantities. Use wiscombe's trick to subtract a small value from
            ! the single scattering for the case of a conservative atmosphere.
            do l = 1, n_layers
                if(1d0 - single_scattering_albedos(l) < PREC) then
                    single_scattering_albedos(l) = 1d0 - PREC
                end if

                dtau_p(l) = (1d0 - single_scattering_albedos(l) * asymmetry_parameters(l)) * dtau(l)
                om_p(l) = (1d0 - asymmetry_parameters(l)) * single_scattering_albedos(l) &
                    / (1d0 - single_scattering_albedos(l) * asymmetry_parameters(l))
                g1(l) = (1.D+00 - 0.5D+00 * om_p(l)) / amu1
                g2(l) = 0.5D+00 * om_p(l) / amu1
                sk(l) = DSQRT(g1(l) * g1(l) - g2(l) * g2(l))
            end do
            
            ! Diffuse transmittance and reflectance of each homogeneous layer the equations for rl and 
            ! temperatures_layers were derived by solving the homogeneous part of the equation of transfer 
            ! (ie. no planck_function term).
            do l = 1, n_layers
                skt = sk(l) * dtau_p(l)

                if(skt  > emax2) then
                    rl(l) = g2(l) / (g1(l) + sk(l))
                    temperatures_layers(l) = 0d0
                else
                    ekt(l) = exp(skt)

                    if(skt > emax1) then
                        ! Semi-infinite layers
                        rl(l) = g2(l) / (g1(l) + sk(l))
                        temperatures_layers(l) = 2.0D0 * sk(l) / (ekt(l) * (g1(l) + sk(l)))
                    else
                        e2ktm = ekt(l) * ekt(l) - 1d0
                        denom = g1(l) * e2ktm + sk(l) * (e2ktm + 2d0)

                        rl(l) = g2(l) * e2ktm / denom
                        temperatures_layers(l) = 2d0 * sk(l) * ekt(l) / denom
                    end if
                end if
            end do

            ! Set the "reflectivity", "transmissivity" and "emissivity" of the "surface" assuming semi-infinite layer
            ! with same om_p as layer n_layers. For "emissivity", assume same db/dtau as layer n_layers.
            alb = g2(n_layers) / (g1(n_layers) + sk(n_layers))
            rl(n_levels) = (1d0 - alb)
            temperatures_layers(n_levels) = 0d0
            emis = 1d0 - alb + (1d0 + alb) * amu1 * (1d0 - planck_function(n_levels - 1) / planck_function(n_levels)) &
                / dtau_p(n_layers)

            ! Use adding method to find the reflectance and transmittance of combined layers. Add downward from the top
            ! and upward from the bottom at the same time.
            rs(1) = rl(1)
            ru(n_levels) = alb

            do l = 1, n_layers
                dd(l) = 1d0 / (1d0 - rs(l) * rl(l + 1))
                rs(l + 1) = rl(l + 1) + temperatures_layers(l + 1) * temperatures_layers(l + 1) * rs(l) * dd(l)
                du(n_levels - l) = 1d0 / (1d0 - rl(n_levels - l) * ru(np2 - l))
                ru(n_levels - l) = rl(n_levels - l) + temperatures_layers(n_levels - l) &
                    * temperatures_layers(n_levels - l) * ru(np2 - l) * du(n_levels - l)
            end do

            ! Compute the upward and downward flux for each homogeneous layer.
            ! Loop over j for kernel
            do j = 1, n_levels + 1
                dfl(n_levels) = 0d0

                if(j <= n_levels) then
                    flux_downward(1) = 0d0

                    do l = 1, n_levels
                        d_kernel(l, j) = 0d0
                        planck_function_tmp(l) = 0d0
                    end do

                    planck_function_tmp(j) = 1d0
                else
                    flux_downward(1) = downward_flux_top

                    do l = 1, n_levels
                        planck_function_tmp(l) = planck_function(l)
                    end do
                end if

                ufl(n_levels) = emis * planck_function_tmp(n_levels)

                do l = 1, n_layers
                    if(planck_function_tmp(l) < prec .and. planck_function_tmp(l+1) < prec) then  ! no need to calculate
                        ufl(l) = 0d0
                        dfl(l) = 0d0
                    else
                        call calculate_up_down_fluxes(&
                            planck_function_tmp(l), planck_function_tmp(l + 1), &
                            single_scattering_albedos(l), asymmetry_parameters(l), dtau(l), &
                            ufl(l), dfl(l) &
                        )
                    end if
                end do

                ! Use adding method to find upward and downward fluxes for combined layers. Start at top
                dflux(1) = dfl(1) + temperatures_layers(1) * flux_downward(1)

                do l = 1, n_layers - 1
                    dflux(l + 1) = temperatures_layers(l + 1) * &
                        (rs(l) * (rl(l + 1) * dflux(l) + ufl(l + 1)) * dd(l) + dflux(l)) + dfl(l + 1)
                end do

                flux_downward(n_levels) = (dflux(n_layers) + rs(n_layers) * emis * planck_function_tmp(n_levels)) &
                    / (1d0 - rs(n_layers) * alb)

                ! Use adding method to find upward and downward fluxes for combined layers. Start at bottom.
                uflux(n_levels) = ufl(n_levels)

                do l = 1, n_layers
                    uflux(n_levels - l) = temperatures_layers(n_levels - l) * (uflux(np2 - l) + &
                            ru(np2 - l) * dfl(n_levels - l)) * du(n_levels - l) + &
                            ufl(n_levels - l)
                end do

                ! Find the total upward and downward fluxes at interfaces between inhomogeneous layers.
                flux_upward(1) = uflux(1) + ru(1) * flux_downward(1)
                flux_upward(n_levels) = alb * flux_downward(n_levels) + emis * planck_function_tmp(n_levels)

                do l = 1, n_layers - 1
                    flux_upward(n_levels - l) = (uflux(n_levels - l) + ru(n_levels - l) * &
                        dflux(n_layers - l)) / (1d0 - ru(n_levels - l) * &
                        rs(n_layers - l))
                end do

                do l = 1, n_layers - 1
                    flux_downward(l + 1) = dflux(l) + rs(l) * flux_upward(l + 1)
                end do

                if(j <= n_levels) then
                    do l = 1, n_levels
                        d_kernel(l, j) = flux_upward(l) - flux_downward(l)
                    end do
                end if
            end do  ! j = 1, n_levels + 1

            return
        end subroutine calculate_two_stream_fluxes


        subroutine calculate_up_down_fluxes(emission_1, emission_2, single_scattering_albedo, &
            asymmetry_parameter, optical_depth, upward_flux, downward_flux)
            ! """
            ! Use the two-stream routine (quadrature with cos of average angle = amu1) to find the upward and downward
            ! thermal fluxes emitted from a homogeneous layer.
            !
            ! Authors: David Paige and David Crisp
            !
            ! :param btatm1: atmospheric emission at level l (arbitrary units)
            ! :param btatm2: atmospheric emission at level l+1 (arbitrary units)
            ! :param single_scattering_albedo: infrared single scattering albedo
            ! :param asymmetry_parameter: infrared asymmetry parameter
            ! :param optical_depth: optical depth (extinction = absorption + scattering)
            ! :return upward_flux: upward flux at top of atm (same units as btatm1 or 2)
            ! :return downward_flux: downward flux at surface
            ! """
            implicit none

            doubleprecision, intent(in) :: emission_1, emission_2, single_scattering_albedo, asymmetry_parameter, &
                optical_depth
            doubleprecision, intent(out) :: upward_flux, downward_flux

            doubleprecision, parameter :: amu1 = 2d0 / 3d0  ! 1d0/sqrt(3d0)

            doubleprecision :: c1, c2, emkt, epkt, v1, v2, v3, v4, v5, v6, dair, cap, opttp, omttp, dtauir, db

            dair = single_scattering_albedo * (1d0 - asymmetry_parameter) &
                / (1d0 - asymmetry_parameter * single_scattering_albedo)
            cap = sqrt(1d0 - dair) / amu1
            opttp = sqrt(1d0 - 0.5d0 * dair + sqrt(1d0 - dair))
            omttp = sqrt(1d0 - 0.5d0 * dair - sqrt(1d0 - dair))
            dtauir = (1d0 - asymmetry_parameter * single_scattering_albedo) * optical_depth
            db = (emission_2 - emission_1) * amu1 / dtauir

            if (cap * dtauir < 24d0) then
                ! The layer thickness is finite.
                epkt = exp(+cap * dtauir)
                emkt = 1d0 / epkt

                ! Set top B.C.
                v1 = opttp
                v2 = omttp
                v3 = -(emission_1 - db)

                ! Set lower B.C.
                v4 = emkt * omttp
                v5 = epkt * opttp
                v6 = -(emission_2 + db)

                ! Solve system of equations.
                c1 = (v3 * v5 - v6 * v2) / (v1 * v5 - v4 * v2)
                c2 = (v1 * v6 - v3 * v4) / (v1 * v5 - v4 * v2)

                upward_flux = c1 * omttp + c2 * opttp + emission_1 + db
                downward_flux = c1 * emkt * opttp + c2 * epkt * omttp + emission_2 - db
            else
                ! Assume layer is semi-infinite.
                upward_flux = emission_1 * (1d0 - omttp / opttp) + db * (1d0 + omttp / opttp)
                downward_flux = emission_2 * (1d0 - omttp / opttp) - db * (1d0 + omttp / opttp)
            end if

            return
        end subroutine calculate_up_down_fluxes


        subroutine planck(wavenumbers, t, pressure_level, dpl)
            use atmosphere, only: n_levels
            use physics, only: cst_c, cst_h, cst_k
            use spectrometrics, only: n_wavenumbers

            implicit none

            doubleprecision, parameter :: &
                hck = cst_h * cst_c * 1d2 / cst_k, &
                hc2 = 2d0 * cst_h * (cst_c * 1d2) ** 2d0

            doubleprecision, intent(in) :: wavenumbers(n_wavenumbers), t(n_levels)
            doubleprecision, intent(out) :: pressure_level(n_levels, n_wavenumbers), dpl(n_levels, n_wavenumbers)

            integer :: i, j
            doubleprecision :: hckt_nu

            do j = 1, n_levels
                do i = 1, n_wavenumbers
                    hckt_nu = hck * wavenumbers(i) / t(j)

                    if(hckt_nu > log(huge(0.))) then
                        pressure_level(j, i) = tiny(0.)
                        dpl(j, i) = tiny(0.)
                    else
                        pressure_level(j, i) = hc2 * wavenumbers(i) ** 3d0 / &
                            (exp(hck * wavenumbers(i) / t(j)) - 1d0) * 1d7  ! j to erg
                        dpl(j, i) = &
                            hc2 * wavenumbers(i) ** 3d0 * &
                            (hck * wavenumbers(i) / (t(j) ** 2d0)) * exp(hck * wavenumbers(i) /  t(j)) / &
                            (exp(hck * wavenumbers(i) / t(j)) - 1d0) ** 2d0 * 1d7  ! j to erg
                    end if
                end do
            end do
        end subroutine planck


        subroutine sort_two_arrays(main_array, linked_array, n)
            ! """
            ! Sorts a main array into ascending numerical order while making the corresponding rearrangements to the
            ! linked array.
            ! Example:
            !   x = [0.33, 1.0, 0.1, 5.0]  ! "main" array
            !   y = [0.32, 0.84, 0.1, -0.96]  ! "linked" array
            !   call sort_two_arrays(x, y, 4)
            !   ! x = [0.1, 0.33, 1.0, 5.0]
            !   ! y = [0.1, 0.32, 0.84, -0.96]
            ! """
            use math, only: quicksort_index, quicksort

            implicit none

            integer, intent(in) :: n
            integer :: ordered_indices(n)
            doubleprecision, intent(inout) :: main_array(n), linked_array(n)

            integer :: i
            doubleprecision :: array_tmp(n)

            call quicksort_index(main_array, ordered_indices)

            array_tmp(:) = linked_array(:)

            do i = 1, n
                linked_array(i) = array_tmp(ordered_indices(i))
            end do
        end subroutine sort_two_arrays
end module radiative_transfer