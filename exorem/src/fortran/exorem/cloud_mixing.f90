module cloud_mixing
    implicit none

    contains
        subroutine calculate_cloud_mixing(nlay, p, t, pl, tl, gl, ml, layer_clouds, pbot, &
            qsat_cloud, q_cloud, q0, kzz, cloud_mode, eddy_mode, fsed, radius, rho_c, vsed, vmixing)
            use math, only: pi
            use physics, only: cst_R
            use atmosphere, only: n_levels, n_layers

            implicit none

            doubleprecision, parameter :: avocado_molrad2 = 12032.12883d0, anueff = 0.3d0, tinyiest = 2d0 * tiny(0d0)

            character(len=*), intent(in) :: eddy_mode, cloud_mode
            integer, intent(in) :: nlay, layer_clouds
            doubleprecision, intent(in) :: p(n_levels), t(n_levels), pl(n_layers), tl(n_layers), gl(n_layers), &
                ml(n_layers), pbot, qsat_cloud(n_layers),q0, kzz(n_layers), fsed, rho_c !kzz in cm^2.s
            doubleprecision, intent(out) ::  q_cloud(n_layers), radius(n_layers), vsed(n_layers), vmixing(n_layers)

            double precision :: pl2(nlay), q_vap(n_layers), dq_cond(n_layers)
            double precision :: var_a, var_b, H2, H3

            integer :: i
            double precision a, b, sg, visc, Hscale(nlay), rho(nlay), radius_sed(nlay), ratio, dz, &
                c

            logical variation_constante

            write(*, '("Calculating cloud mixing...")')

            sg = (log(1d0 + anueff))**0.5 !(Hansen 1974)

            variation_constante = .True.

            !    Sedimentation velocity with a Cunnigham slip factor
            q_cloud(:) = 0d0
            q_vap(:)=q0
            pl2(:) = pl(1:nlay) * 100d0
            rho(:) = p(1:nlay) * 100d0 / (cst_R * t(1:nlay) / ml(1:nlay) * 1000d0)
            Hscale(:) = 1d5 * (cst_R * tl(1:nlay) / ml(1:nlay) / gl(1:nlay))
            ! conversion effective radius -> geometric radius -> sedimentation radius (formula from Ackerman & Marley 2001 and Hansen 1974)
            vsed(:) = 0d0

            do i = 1, nlay
                visc = 2.0123d-7 * tl(i) ** (2d0 / 3d0) ! H2
                a = sqrt(2d0) / 2d0 * cst_R / (4d0 * pi * avocado_molrad2)
                b = (2d0 / 9d0) * (rho_c - rho(i)) * gl(i) * 1.0d-2 / visc
                ! conversion effective radius -> geometric radius -> sedimentation radius (formula from Ackerman & Marley 2001 and Hansen 1974)

                if((trim(eddy_mode) == 'infinity') .and. (cloud_mode == 'fixedRadius')) then
                    vsed(i) = 0d0
                else if(radius(i) > 0) then
                    radius_sed(i) = radius(i) * exp((1.4d0 + 1d0) / 2d0 * sg ** 2d0)
                    vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + 4d0 / 3d0 * (a * tl(i) / pl2(i)) / radius_sed(i))
                end if

                vmixing(i) = 1d-4 * kzz(i) / Hscale(i)

                if(cloud_mode == 'fixedSedimentation') then
                    vsed(i) = vmixing(i) * fsed
                    c = 4d0 / 3d0 * (a * tl(i) / pl2(i))
                    radius_sed(i) = 0.5d0 * (-c + (c**2 + 4d0 * fsed * vmixing(i) / b)**0.5)
                    !          radius(i)=radius_sed(i)*fsed**(1.0/1.4)*exp(-(1.4+1)/2.0*sg**2)
                    radius(i) = radius_sed(i) * exp(-(1.4d0 + 1d0) / 2d0 * sg**2)
                    radius(i) = max(radius(i), 1.0d-8)
                elseif(cloud_mode == 'fixedRadiusCondensation') then
                    c = 4d0 / 3d0 * (a * tl(i) / pl2(i))

                    if((i > layer_clouds + 2)) then
                        radius_sed(i) = radius_sed(layer_clouds + 2)
                        vsed(i) = b * radius_sed(i) * &
                            radius_sed(i) * (1d0 + 4d0 / 3d0 * (a * tl(i) / pl2(i)) / radius_sed(i))
                    else
                        vsed(i) = vmixing(i) * fsed
                        radius_sed(i) = 0.5d0 * (-c + (c**2d0 + 4d0 * fsed * vmixing(i) / b)**0.5)
                    end if
                    !          radius(i)=radius_sed(i)*fsed**(1.0/1.4)*exp(-(1.4+1)/2.0*sg**2)
                    radius(i) = radius_sed(i) * exp(-(1.4d0 + 1d0) / 2d0 * sg**2)
                    radius(i) = max(radius(i), 1.0d-8)
                end if
            end do

            if((layer_clouds > 1) .and. (layer_clouds < nlay)) then
                do i = layer_clouds, nlay - 1
                    dz = (pl(i - 1) - pl(i)) * 100.0 * 100.0 / rho(i) / gl(i)
                    dq_cond(i)=  min(qsat_cloud(i - 1),q_vap(i - 1)) - qsat_cloud(i)
                    if((cloud_mode == 'fixedRadius') .or. (cloud_mode == 'fixedRadiusCondensation')) then
                        q_cloud(i) = (dq_cond(i) + &
                                      q_cloud(i - 1) * (1d0 - 0.5d0 * vsed(i - 1) / (1d-4 * kzz(i - 1)) * dz) &
                                     ) / (1d0 + 0.5d0 * vsed(i) / (1d-4 * kzz(i)) * dz)
                        
                        if(variation_constante) then
                            var_a = -0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) + vsed(i) / kzz(i)) * dz

                            if(qsat_cloud(i) > qsat_cloud(i - 1) * (1d0 - 1d-12) .and. &
                               qsat_cloud(i) < qsat_cloud(i - 1) * (1d0 + 1d-12)) then
                                H2 = dz * sign(1d10, qsat_cloud(i) - qsat_cloud(i - 1))
                            else
                                H2 = dz / log(qsat_cloud(i) / qsat_cloud(i - 1))
                            end if

                            H3 = 1 / (1 / H2 + 0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) + vsed(i) / kzz(i)))

                            if (qsat_cloud(i) > 1 .and. q_cloud(i - 1) < tinyiest * (1d0 + 1d-12)) then
                                q_cloud(i) = 0d0  ! fix isolated cloud formation far above the condensation level
                            else if(dz / H3 > log(huge(0d0))) then  ! overflow, use log and simplified expression
                                var_b = log(abs(qsat_cloud(i - 1) / H2 * H3)) + dz / H3
                                var_b = max(0d0,min(var_b,q_vap(i - 1)- qsat_cloud(i)))
                                q_cloud(i) = sign(exp(var_a + var_b), -qsat_cloud(i - 1) / H2 * H3)
                            else  ! normal case
                                var_b = -qsat_cloud(i - 1) / H2 * H3 * (exp(dz / H3) - 1)
                                var_b = min(var_b,q_vap(i - 1)- qsat_cloud(i))
                                q_cloud(i) = exp(var_a) * (q_cloud(i - 1) + var_b)
                            end if
                        end if
                    elseif(cloud_mode == 'fixedSedimentation') then
                        
                        q_cloud(i) = (dq_cond(i) + q_cloud(i - 1) * &
                                (1d0 - 0.5d0 * fsed / Hscale(i - 1) * dz)) / &
                                (1d0 + 0.5d0 * fsed / Hscale(i) * dz)
                        if(variation_constante) then
                            var_a = exp(-0.5 * (fsed / Hscale(i - 1) + fsed / Hscale(i)) * dz)

                            if(qsat_cloud(i) > qsat_cloud(i - 1) * (1d0 - 1d-12) .and. &
                               qsat_cloud(i) < qsat_cloud(i - 1) * (1d0 + 1d-12)) then
                                H2 = dz * sign(1d10, qsat_cloud(i) - qsat_cloud(i - 1))
                            else
                                H2 = dz / log(qsat_cloud(i) / qsat_cloud(i - 1))
                            end if
                            H3 = 1 / (1d0 / H2 + 0.5d0 * (fsed / Hscale(i - 1) + fsed / Hscale(i)))
                            var_b = -qsat_cloud(i - 1) / H2 * H3 * (exp(dz / H3) - 1)
                            var_b = min(var_b,q_vap(i - 1)- qsat_cloud(i))
                            q_cloud(i) = var_a * (q_cloud(i - 1) + var_b)
                        end if
                    else
                        q_cloud(i) = 0d0
                    end if

                    q_cloud(i) = max(tinyiest, q_cloud(i))
                    q_cloud(i) = min(q_cloud(i), q0)
                    q_cloud(i) = min(q_cloud(i), qsat_cloud(1))
                    q_vap(i)   = min(qsat_cloud(i),q_cloud(i-1)+q_vap(i-1))
                end do
            elseif(layer_clouds <= 1) then
                if((cloud_mode == 'fixedSedimentation') .or. (cloud_mode == 'fixedRadiusCondensation')) then
                    q_cloud(1) = (q0 - qsat_cloud(1)) * (p(1) / pbot)**(fsed)
                elseif(cloud_mode == 'fixedRadiusCondensation') then
                    q_cloud(1) = (q0 - qsat_cloud(1)) * (p(1) / pbot)**(vsed(1) * Hscale(1) / (1.0e-4 * kzz(1)))
                else
                    q_cloud(1) = 0d0
                end if

                q_cloud(1) = max(0d0, min(q0 - qsat_cloud(1), q_cloud(1)))
                q_vap(1) = qsat_cloud(1)
                do i = 2, nlay - 1
                    dz = (pl(i - 1) - pl(i)) * 100d0 * 100d0 / rho(i) / gl(i)
                    dq_cond(i)= max(0d0, min(qsat_cloud(i - 1),q_vap(i - 1)) - qsat_cloud(i))
                    if((cloud_mode == 'fixedRadius').or.(cloud_mode == 'fixedRadiusCondensation')) then
                        q_cloud(i) = (dq_cond(i) + q_cloud(i - 1) * (1d0 - 0.5d0 * vsed(i - 1) / &
                                (1d-4 * kzz(i - 1)) * dz)) / &
                                (1d0 + 0.5d0 * vsed(i) / (1d-4 * kzz(i)) * dz)

                        if(variation_constante) then
                            var_a = exp(-0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) + vsed(i) / kzz(i)) * dz)

                            if(qsat_cloud(i) > qsat_cloud(i - 1) * (1d0 - 1d-12) .and. &
                               qsat_cloud(i) < qsat_cloud(i - 1) * (1d0 + 1d-12)) then
                                H2 = dz * sign(1d10, qsat_cloud(i) - qsat_cloud(i - 1))
                            else
                                H2 = dz / log(qsat_cloud(i) / qsat_cloud(i - 1))
                            end if

                            H3 = 1d0 / (1d0 / H2 + 0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) + vsed(i) / kzz(i)))

                            if(dz / H3 > log(huge(0d0))) then
                                var_b = -huge(0d0)
                            else
                                var_b = -qsat_cloud(i - 1) / H2 * H3 * (exp(dz / H3) - 1)
                                var_b = min(var_b,q_vap(i - 1)- qsat_cloud(i))
                            end if

                            q_cloud(i) = var_a * (q_cloud(i - 1) + var_b)
                        end if
                    else if(cloud_mode == 'fixedSedimentation') then
                        q_cloud(i) = (dq_cond(i) + q_cloud(i - 1) * &
                                (1d0 - 0.5d0 * fsed / Hscale(i - 1) * dz)) / &
                                (1d0 + 0.5d0 * fsed / Hscale(i) * dz)
                        if(variation_constante) then
                            var_a = exp(-0.5d0 * (fsed / Hscale(i - 1) + fsed / Hscale(i)) * dz)

                            if(qsat_cloud(i) > qsat_cloud(i - 1) * (1d0 - 1d-12) .and. &
                               qsat_cloud(i) < qsat_cloud(i - 1) * (1d0 + 1d-12)) then
                                H2 = dz * sign(1d10, qsat_cloud(i) - qsat_cloud(i - 1))
                            else
                                H2 = dz / log(qsat_cloud(i) / qsat_cloud(i - 1))
                            end if
                            H3 = 1d0 / (1d0 / H2 + 0.5d0 * (fsed / Hscale(i - 1) + fsed / Hscale(i)))
                            var_b = -qsat_cloud(i - 1) / H2 * H3 * (exp(dz / H3) - 1)
                            var_b = min(var_b,q_vap(i - 1)- qsat_cloud(i))
                            q_cloud(i) = var_a * (q_cloud(i - 1) + var_b)
                        end if
                    else
                        q_cloud(i) = 0d0
                    end if

                    q_cloud(i) = max(tinyiest, q_cloud(i))
                    q_cloud(i) = min(q_cloud(i), q0)
                    q_vap(i)   = min(qsat_cloud(i),q_cloud(i-1)+q_vap(i-1))
                end do

                q_cloud(1) = q_cloud(2)
            end if !((layer_clouds > 0).and.(layer_clouds < nlay))

            do i = 2, nlay - 1
                q_cloud(i) = min(q_cloud(i), qsat_cloud(i - 1) + q_cloud(i - 1))
            end do

            ! Cloud VMR at condensation level
            if((layer_clouds >= 2) .and. (layer_clouds < nlay)) then
                ratio = (pbot - p(layer_clouds + 1)) / (p(layer_clouds) - p(layer_clouds + 1))
                ratio = max(min(1d0, ratio), 0d0)

                if (q_cloud(layer_clouds + 1) < tinyiest * (1d0 + 1d-12)) then  ! form cloud even if smthng went wrong above
                    q_cloud(layer_clouds) = q_cloud(layer_clouds)
                else if (ratio > tiny(0d0) / q_cloud(layer_clouds + 1)) then
                    q_cloud(layer_clouds) = 0.5d0 * q_cloud(layer_clouds + 1) * ratio
                else
                    q_cloud(layer_clouds) = 0.5d0 * q_cloud(layer_clouds + 1)
                end if
            elseif(layer_clouds == 1) then
                q_cloud(layer_clouds) = 0.5d0 * q_cloud(layer_clouds + 1)
            end if
        end subroutine calculate_cloud_mixing


        subroutine calculate_cloud_mixing2(nlay, p, t, &
            pl, tl, gl, ml, &
            layer_clouds, pbot, qsat_cloud, &
            q_cloud, q0, kzz, radius, rho_c, vsed, vmixing, stick_ef0, smax)
        use math, only: pi
        use physics, only: cst_N_A, cst_R
        use atmosphere, only: n_levels, n_layers

        implicit none

        doubleprecision, parameter :: &
            amolrad = 1.4135d-10, avocado_molrad2 = 12032.12883d0, &
            anueff = 0.3d0, radius_sed_min = 1.37d-7, radius_sed_max = 1.37d-4 ,&
            f = 2d0, alpha = 1d0, tiniest = tiny(0.)

        integer, intent(in) :: nlay, layer_clouds
        doubleprecision, intent(in) :: p(n_levels), t(n_levels), &
            pl(n_layers), tl(n_layers), gl(n_layers), ml(n_layers), &
            pbot, qsat_cloud(n_layers), q0, kzz(n_layers), rho_c, smax,stick_ef0
        doubleprecision, intent(inout) :: radius(n_layers)
        doubleprecision, intent(out) :: q_cloud(n_layers), vsed(n_layers), vmixing(n_layers)

        double precision :: pl2(nlay), q_vap(nlay)
        double precision :: tau_mixing(nlay), tau_cond(nlay), tau_sed(nlay), tau_sed_mixing(nlay), rho_s(nlay)
        double precision :: tau_coal(nlay), tau_coal_cond(nlay), tau_sed_cond(nlay)
        double precision :: var_a, var_b, H2, H3, aKn, r0, fnewton, dfnewton, stick_ef

        integer :: i, ii, k
        double precision :: a, b, sg, visc, vsed_cond(nlay), Hscale(nlay), rho(nlay), radius_sed(nlay), &
            ratio, cc, dz, bb, aa, var_a2

        logical variation_constante, coalescence

        write(*, '("Calculating cloud mixing (alt)...")')

        sg = (log(1d0 + anueff))**0.5 ! (Hansen 1974)

        variation_constante = .true.
        coalescence = .true.

        stick_ef = min(1d0,stick_ef0)
        stick_ef = max(1d-6,stick_ef)

        ! Sedimentation velocity with a Cunnigham slip factor
        q_cloud(:) = 0d0
        q_vap(:)=q0
        pl2(:) = pl(1:nlay) * 100d0
        rho(:) = p(1:nlay) * 100d0 / (cst_R * t(1:nlay) / ml(1:nlay) * 1000d0)
        Hscale(:) = 1d5 * (cst_R * tl(1:nlay) / ml(1:nlay) / gl(1:nlay))
        ! Conversion effective radius -> geometric radius -> sedimentation radius (formula from Ackerman & Marley 2001 and Hansen 1974)
        radius_sed(:) = radius(1:nlay) * exp((1.4d0 + 1d0) / 2d0 * sg ** 2d0)
        vsed(:) = 0d0
        tau_mixing(:) = 1d4 * Hscale(:)**2 / kzz(1:nlay)
        rho_s(:) = rho(:) * qsat_cloud(1:nlay)
        vmixing(:) = 0d0
        vmixing(1:nlay) = 1d-4 * kzz(1:nlay) / Hscale(:)

        do i = 1, nlay
            visc = 2.0123d-7 * tl(i)**0.66 ! H2
            a = sqrt(2d0) / 2d0 * cst_R / (4.0 * pi * avocado_molrad2)
            b = (2d0 / 9d0) * (rho_c - rho(i)) * gl(i) * 1d-2 / visc
            vsed(i) = b * radius_sed(i) ** 2d0 * (1d0 + (4d0 / 3d0) * (a * tl(i) / pl2(i)) / radius_sed(i))
        end do

        do ii = 1, 4
            do i = 1, nlay
                visc = 2.0123d-7 * tl(i) ** (2d0 / 3d0) ! H2
                a = sqrt(2d0) / 2d0 * cst_R / (4.0 * pi * avocado_molrad2)
                b = (2d0 / 9d0) * (rho_c - rho(i)) * gl(i) * 1.0d-2 / visc
                aa = b
                bb = a * b * (4d0 / 3d0) * tl(i) / pl2(i)
                radius_sed(i) = (tau_mixing(i) / rho(i) * (2d0 * f * visc * rho_s(i) * smax / rho_c))**0.5

                if((i == 1) .and. (layer_clouds < 1)) radius_sed(i) = (tau_mixing(i) * &
                        (2d0 * f * visc * (qsat_cloud(i) * q0)**0.5 * smax / rho_c))**0.5
                radius_sed(i) = max(radius_sed(i), radius_sed_min)
                radius_sed(i) = min(radius_sed(i), radius_sed_max)
                radius(i) = radius_sed(i) / exp((1.4d0 + 1d0) / 2d0 * sg**2)

                vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * (a * tl(i) / pl2(i)) / radius_sed(i))
                aKn = cst_R / cst_N_A * tl(i) / (2d0**0.5 * pi * amolrad**2 * pl2(i)) / radius_sed(i)

                if(aKn >= alpha) then
                    radius_sed(i) = tau_mixing(i) * (3d0 * alpha * f * rho_s(i) * smax / 2d0 / rho_c) * &
                        (2d0 * cst_R * tl(i) / pi / ml(i) * 1d3)**0.5

                    if((i == 1) .and. (layer_clouds < 1)) radius_sed(i) = tau_mixing(i) * &
                            (3d0 * alpha * f * (qsat_cloud(i) * q0)**0.5 * rho(i) * smax / 2d0 / rho_c) * &
                            (2d0 * cst_R * tl(i) / pi / ml(i) * 1d3)**0.5

                    radius_sed(i) = max(radius_sed(i), radius_sed_min)
                    radius_sed(i) = min(radius_sed(i), radius_sed_max)

                    vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                        (a * tl(i) / pl2(i)) / radius_sed(i))
                    tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                    tau_cond(i) = tau_mixing(i)
                    tau_sed(i) = Hscale(i) / vsed(i)

                    if(tau_sed(i) < tau_cond(i)) then
                        r0 = radius_sed(i)

                        do k = 1, 100
                            fnewton = tau_cond(i) / r0 * aa * radius_sed(i)**3 + tau_cond(i) / r0 * &
                                bb * radius_sed(i)**2 - Hscale(i)
                            dfnewton = 3 * tau_cond(i) / r0 * aa * radius_sed(i)**2 + 2d0 * tau_cond(i) / r0 * &
                                bb * radius_sed(i)
                            radius_sed(i) = radius_sed(i) - fnewton / dfnewton
                            if(abs(fnewton / dfnewton / radius_sed(i)) <= 1.0d-3) exit
                        end do

                        tau_cond(i) = tau_cond(i) / r0 * radius_sed(i)
                        vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                            (a * tl(i) / pl2(i)) / radius_sed(i))
                        tau_sed(i) = Hscale(i) / vsed(i)
                        tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                    end if

                    tau_coal(i) = 4 * radius_sed(i) * rho_c / (3d0 * stick_ef * alpha * vsed(i) * rho(i) * &
                        max(tiniest, q_cloud(i)))
                    if(coalescence.and.(tau_coal(i) < tau_cond(i))) then
                        r0 = radius_sed(i)

                        do k = 1, 100
                            fnewton = tau_cond(i) / r0 * radius_sed(i) * (aa * radius_sed(i)**2 + bb * radius_sed(i)) - &
                                tau_coal(i) * vsed(i) * radius_sed(i) / r0
                            dfnewton = 3d0 * tau_cond(i) / r0 * aa * radius_sed(i)**2 + 2 * tau_cond(i) / r0 * bb * &
                                radius_sed(i) - tau_coal(i) * vsed(i) / r0
                            radius_sed(i) = radius_sed(i) - fnewton / dfnewton
                            if(abs(fnewton / dfnewton / radius_sed(i)) <= 1d-3) exit
                        end do

                        tau_cond(i) = tau_cond(i) / r0 * radius_sed(i)
                        vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                            (a * tl(i) / pl2(i)) / radius_sed(i))
                        tau_sed(i) = Hscale(i) / vsed(i)
                        tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                        tau_coal(i) = 4 * radius_sed(i) * rho_c / &
                            (3d0 * alpha * vsed(i) * rho(i) * max(tiniest, q_cloud(i)))
                    end if
                else
                    tau_cond(i) = tau_mixing(i)
                    tau_sed(i) = Hscale(i) / vsed(i)

                    if(tau_sed(i) < tau_cond(i)) then
                        r0 = radius_sed(i)
                        do k = 1, 100
                            fnewton = tau_cond(i) / r0**2 * aa * radius_sed(i)**4 + tau_cond(i) / r0**2 * bb * &
                                radius_sed(i)**3 - Hscale(i)
                            dfnewton = 4 * tau_cond(i) / r0**2 * aa * radius_sed(i)**3 + &
                                3d0 * tau_cond(i) / r0**2 * bb * radius_sed(i)**2
                            radius_sed(i) = radius_sed(i) - fnewton / dfnewton
                            if(abs(fnewton / dfnewton / radius_sed(i)) <= 1d-3) exit
                        end do

                        tau_cond(i) = tau_cond(i) / r0**2 * radius_sed(i)**2
                        vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                            (a * tl(i) / pl2(i)) / radius_sed(i))
                        tau_sed(i) = Hscale(i) / vsed(i)
                        tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                    end if
                end if

                tau_coal(i) = 4d0 * radius_sed(i) * rho_c / (3d0 * stick_ef * alpha * vsed(i) * rho(i) * &
                    max(tiniest, q_cloud(i)))
                if(coalescence.and.(tau_coal(i) < tau_cond(i))) then
                    r0 = radius_sed(i)
                    do k = 1, 100
                        fnewton = tau_cond(i) / r0**2 * radius_sed(i)**2 * (aa * radius_sed(i)**2 + &
                            bb * radius_sed(i)) - tau_coal(i) * vsed(i) * radius_sed(i) / r0
                        dfnewton = 4 * tau_cond(i) / r0**2 * aa * radius_sed(i)**3 + 3d0 * tau_cond(i) / r0**2 * &
                            bb * radius_sed(i)**2 - tau_coal(i) * vsed(i) / r0
                        radius_sed(i) = radius_sed(i) - fnewton / dfnewton
                        if(abs(fnewton / dfnewton / radius_sed(i)) <= 1d-3) exit
                    end do
                    tau_cond(i) = tau_cond(i) / r0**2 * radius_sed(i)**2
                    vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                        (a * tl(i) / pl2(i)) / radius_sed(i))
                    tau_sed(i) = Hscale(i) / vsed(i)
                    tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                    tau_coal(i) = 4 * radius_sed(i) * rho_c / &
                        (3d0 * alpha * vsed(i) * rho(i) * max(tiniest, q_cloud(i)))
                end if

                tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                radius(i) = radius_sed(i) / exp((1.4d0 + 1d0) / 2d0 * sg**2)

                vsed_cond(i) = vsed(i)
                tau_sed_cond(i) = tau_sed(i)
                tau_coal_cond(i) = tau_coal(i)
            end do !i=layer_clouds, nlay

            if((layer_clouds > 1) .and. (layer_clouds < nlay)) then
                do i = layer_clouds + 1, nlay - 1
                    visc = 2.0123d-7 * tl(i)**(2d0 / 3d0) ! H2
                    b = (2d0 / 9d0) * (rho_c - rho(i)) * gl(i) * 1d-2 / visc

                    dz = (pl(i - 1) - pl(i)) * 100d0 * 100d0 / rho(i) / gl(i)
                    var_a = exp(-0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) + vsed(i - 1) / kzz(i)) * dz)
                    var_a2 = exp(-0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) + vsed_cond(i) / kzz(i)) * dz)

                    if(coalescence) then
                        var_a = exp(-0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) * &
                            (1 + tau_sed(i - 1) / tau_coal(i - 1)) + vsed(i - 1) / kzz(i) * &
                            (1 + tau_sed(i - 1) / tau_coal(i - 1))) * dz)
                        var_a2 = exp(-0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) * (1 + tau_sed_cond(i - 1) / &
                            tau_coal_cond(i - 1)) + vsed_cond(i) / kzz(i) * &
                            (1 + tau_sed_cond(i) / tau_coal_cond(i))) * dz)
                    end if

                    aa = b
                    bb = a * b * (4d0 / 3d0) * tl(i) / pl2(i)
                    if(qsat_cloud(i) > qsat_cloud(i - 1) * (1d0 - 1d-12) .and. &
                       qsat_cloud(i) < qsat_cloud(i - 1) * (1d0 + 1d-12)) then
                        H2 = dz * sign(1d10, (qsat_cloud(i) - qsat_cloud(i - 1)))
                    else
                        H2 = dz / log(qsat_cloud(i) / qsat_cloud(i - 1))
                    end if
                    H3 = 1 / (1 / H2 + 0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) + vsed_cond(i) / kzz(i)))
                    if(coalescence) H3 = 1 / (1 / H2 + 0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) * &
                            (1 + tau_sed_cond(i - 1) / tau_coal_cond(i - 1)) + vsed_cond(i) / kzz(i) * &
                            (1 + tau_sed_cond(i) / tau_coal_cond(i))))
                    var_b = -qsat_cloud(i - 1) / H2 * H3 * (exp(dz / H3) - 1)
                    var_b = min(var_b, q_vap(i - 1) - qsat_cloud(i))
                    q_cloud(i) = var_a * q_cloud(i - 1) + var_a2 * var_b

                    cc = b * (var_a2 * var_b * radius_sed(i)**2 + var_a * q_cloud(i - 1) * radius_sed(i - 1)**2) + &
                        bb * (var_a2 * var_b * radius_sed(i) + var_a * q_cloud(i - 1) * radius_sed(i - 1))
                    cc = -cc / (var_a2 * var_b + var_a * q_cloud(i - 1))
                    radius_sed(i) = ((bb**2 - 4d0 * aa * cc)**0.5d0 - bb) / 2d0 / aa

                    radius_sed(i) = max(radius_sed(i), radius_sed_min)
                    radius_sed(i) = min(radius_sed(i), radius_sed_max)
                    radius(i) = radius_sed(i) / exp((1.4d0 + 1d0) / 2d0 * sg**2)
                    radius(i) = max(radius(i), radius_sed_min / exp((1.4d0 + 1d0) / 2d0 * sg**2))
                    radius(i) = min(radius(i), radius_sed_max / exp((1.4d0 + 1d0) / 2d0 * sg**2))

                    vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                        (a * tl(i) / pl2(i)) / radius_sed(i))
                    tau_sed_mixing(i) = Hscale(i) / max(1d-7, abs(vsed(i) - 1d-4 * kzz(i) / Hscale(i)))
                    tau_coal(i) = 4 * radius_sed(i) * rho_c / (3d0 * alpha * vsed(i) * rho(i) * &
                        max(tiniest, q_cloud(i)))

                    q_cloud(i) = max(1d-30, q_cloud(i))
                    q_cloud(i) = min(q_cloud(i), q0)
                    q_cloud(i) = min(q_cloud(i), qsat_cloud(1))
                    q_vap(i)   = min(qsat_cloud(i),q_cloud(i-1)+q_vap(i-1))
                end do
            elseif(layer_clouds <= 1) then
                q_cloud(1) = (q0 - qsat_cloud(1)) * (p(1) / pbot)**(4.0)
                q_cloud(1) = max(0d0, min(q0 - qsat_cloud(1), q_cloud(1)))

                do i = 2, nlay - 1
                    visc = 2.0123d-7 * tl(i)**(2d0 / 3d0) ! H2
                    b = (2d0 / 9d0) * (rho_c - rho(i)) * gl(i) * 1d-2 / visc
                    aa = b
                    bb = a * b * (4d0 / 3d0) * tl(i) / pl2(i)

                    dz = (pl(i - 1) - pl(i)) * 100d0 * 100d0 / rho(i) / gl(i)
                    var_a = exp(-0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) + vsed(i - 1) / kzz(i)) * dz)
                    var_a2 = exp(-0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) + vsed_cond(i) / kzz(i)) * dz)
                    if(coalescence) then
                        var_a = exp(-0.5d0 / 1d-4 * (vsed(i - 1) / kzz(i - 1) * (1 + tau_sed(i - 1) / &
                                tau_coal(i - 1)) + vsed(i - 1) / kzz(i) * (1 + tau_sed(i - 1) / tau_coal(i - 1))) * dz)
                        var_a2 = exp(-0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) * (1 + tau_sed_cond(i - 1) / &
                            tau_coal_cond(i - 1)) + vsed_cond(i) / kzz(i) * &
                            (1 + tau_sed_cond(i) / tau_coal_cond(i))) * dz)
                    end if

                    if(qsat_cloud(i) > qsat_cloud(i - 1) * (1d0 - 1d-12) .and. &
                       qsat_cloud(i) < qsat_cloud(i - 1) * (1d0 + 1d-12)) then
                        H2 = dz * sign(1d10, (qsat_cloud(i) - qsat_cloud(i - 1)))
                    else
                        H2 = dz / log(qsat_cloud(i) / qsat_cloud(i - 1))
                    end if

                    H3 = 1d0 / (1d0 / H2 + 0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) + vsed_cond(i) / kzz(i)))

                    if(coalescence) H3 = 1 / (1 / H2 + 0.5d0 / 1d-4 * (vsed_cond(i - 1) / kzz(i - 1) * &
                        (1 + tau_sed_cond(i - 1) / tau_coal_cond(i - 1)) + vsed_cond(i) / kzz(i) * &
                        (1 + tau_sed_cond(i) / tau_coal_cond(i))))
                    var_b = -qsat_cloud(i - 1) / H2 * H3 * (exp(dz / H3) - 1)
                    var_b = min(var_b,q_vap(i - 1)- qsat_cloud(i))
                    q_cloud(i) = var_a * q_cloud(i - 1) + var_a2 * var_b

                    cc = aa * (var_a2 * var_b * radius_sed(i)**2 + var_a * q_cloud(i - 1) * radius_sed(i - 1)**2) + &
                        bb * (var_a2 * var_b * radius_sed(i) + var_a * q_cloud(i - 1) * radius_sed(i - 1))
                    cc = -cc / (var_a2 * var_b + var_a * q_cloud(i - 1))
                    radius_sed(i) = ((bb**2 - 4d0 * aa * cc)**0.5 - bb) / 2d0 / aa

                    radius_sed(i) = max(radius_sed(i), radius_sed_min)
                    radius_sed(i) = min(radius_sed(i), radius_sed_max)
                    radius(i) = radius_sed(i) / exp((1.4d0 + 1d0) / 2d0 * sg**2)
                    radius(i) = max(radius(i), radius_sed_min / exp((1.4d0 + 1d0) / 2d0 * sg**2))
                    radius(i) = min(radius(i), radius_sed_max / exp((1.4d0 + 1d0) / 2d0 * sg**2))

                    vsed(i) = b * radius_sed(i) * radius_sed(i) * (1d0 + (4d0 / 3d0) * &
                        (a * tl(i) / pl2(i)) / radius_sed(i))

                    tau_coal(i) = 4d0 * radius_sed(i) * rho_c / (3d0 * stick_ef *alpha * vsed(i) * rho(i) * &
                        max(tiniest, q_cloud(i)))

                    q_cloud(i) = max(tiniest, q_cloud(i))
                    q_cloud(i) = min(q_cloud(i), q0)
                    q_vap(i)   = min(qsat_cloud(i),q_cloud(i-1)+q_vap(i-1))
                end do

                q_cloud(1) = q_cloud(2)
            end if !((layer_clouds > 0).and.(layer_clouds < nlay))

            do i = 2, nlay - 1
                q_cloud(i) = min(q_cloud(i), q_vap(i - 1) + q_cloud(i - 1))
            end do

            if((layer_clouds >= 2).and.(layer_clouds < nlay)) then
                ratio = (pbot - p(layer_clouds + 1)) / (p(layer_clouds) - p(layer_clouds + 1))
                ratio = max(min(1d0, ratio), 0d0)
                q_cloud(layer_clouds) = 0.5d0 * q_cloud(layer_clouds + 1) * ratio
            elseif(layer_clouds == 1) then
                q_cloud(layer_clouds) = 0.5d0 * q_cloud(layer_clouds + 1)
            end if
        end do !ii=1,10
    end subroutine calculate_cloud_mixing2
end module cloud_mixing
