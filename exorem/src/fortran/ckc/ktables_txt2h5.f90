program ktables_txt2h5
    ! """
    ! Convert Exo-REM k-coefficients txt files into HDF5 files.
    ! """
    use interface, only: file_name_size
    use k_coefficients_interface, only: read_k_coeff, n_k_wavenumbers_max, n_k_wavenumbers, n_k_pressures, &
        n_k_temperatures, n_k_samples_max, wavenumbers_k_min, wavenumbers_k_step, kcoeff_species, &
        p_k_species, t_k_species, samples_k, weights_k
    use k_coefficients_interface_h5, only: write_k_coeff_h5

    implicit none

    character(len=*), parameter :: txt2h5_version = '1.0.0'
    integer, parameter :: n_args = 2, n_opt_args = 1
    character(len=file_name_size), dimension(n_args) :: args
    character(len=file_name_size), dimension(n_opt_args) :: opt_args
    character(len=file_name_size), allocatable :: species_name(:)
    character(len=file_name_size) ::  path_txt, str_tmp
    integer :: i, j, num_elements
    doubleprecision, allocatable :: wavenumbers_k(:)

    call get_txt2h5_command_arguments(args, opt_args)

    str_tmp = trim(args(1))

    num_elements = 1

    do i = 1, len_trim(str_tmp)
        if(str_tmp(i:i) == ' ') then
            num_elements = num_elements + 1
        end if
    end do

    allocate(species_name(num_elements))

    read(str_tmp, *) species_name

    path_txt = trim(args(2))

    call read_k_coeff(species_name, path_txt)

    allocate(wavenumbers_k(n_k_wavenumbers_max))

    do i = 1, num_elements
        do j = 1, n_k_wavenumbers_max
            wavenumbers_k(j) = wavenumbers_k_min(i) + wavenumbers_k_step(i) * dble(j - 1)
        end do

        call write_k_coeff_h5(&
            species_name(i), &
            wavenumbers_k(1), &
            wavenumbers_k(n_k_wavenumbers(i)), &
            wavenumbers_k, &
            n_k_wavenumbers(i), &
            p_k_species(1:n_k_pressures(i), i), &
            n_k_pressures(i), &
            t_k_species(1:n_k_temperatures(i), 1:n_k_pressures(i), i), &
            n_k_temperatures(i), &
            samples_k, &
            weights_k, &
            kcoeff_species(1:n_k_samples_max, 1:n_k_wavenumbers(i), 1:n_k_temperatures(i), 1:n_k_pressures(i), i), &
            n_k_samples_max, &
            path_txt, &
            .False., &
            opt_args(1) &
        )
    end do

    contains
        subroutine get_txt2h5_command_arguments(args, opt_args)
            ! """
            ! Retrieve the txt2h5 arguments that were passed on the command line when it was invoked.
            ! :param args: array storing the arguments
            ! """
            implicit none

            character(len=file_name_size), dimension(:), intent(inout) :: args, opt_args

            character(len=file_name_size) :: arg
            integer :: i, j, n_args, n_opt_args

            j = 0
            n_args = size(args)
            n_opt_args = size(opt_args)

            args(:) = ''
            opt_args(:) = ''

            do i = 1, command_argument_count()
                call get_command_argument(i, arg)

                if(trim(arg) == '-v' .or. trim(arg) == '--version') then
                    write(*, '("txt2h5 ", A)') txt2h5_version

                    stop
                elseif(trim(arg) == '-h' .or. trim(arg) == '--help') then
                    call print_help()

                    stop
                else
                    j = j + 1
                end if

                if (len_trim(arg) == 0) then
                    if (j < n_args) then
                        write(*, '("Error: txt2h5 takes ", I0, " arguments but ", I0, " were given")') &
                            n_args, j
                        stop
                    end if

                    exit
                end if

                if(j > n_args) then
                    opt_args(j - n_args) = trim(arg)

                    if (j > n_args + n_opt_args) then
                        write(*, '("Error: txt2h5 takes ", I0, " arguments but ", I0, " were given")') &
                            n_args + n_opt_args, j

                        stop
                    end if
                else if (j > 0) then
                    args(j) = trim(arg)
                end if
            end do
        end subroutine get_txt2h5_command_arguments

        subroutine print_help()
            implicit none

            character(len=*), parameter :: help_str = &
                "Usage: ./txt2h5.exe [options] 'list of species' path/to/k_coeffecients/ ['additional info']"&
                //new_line('')//"&
                &Example: ./txt2h5.exe 'CH4 CO H2O NH3' ../data/k_coefficients_tables/R50/"//new_line('')//"&
                &Options: "//new_line('')//"&
                &  --help         Display this information."//new_line('')//"&
                &  --version      Display software version information."//new_line('')//"&
                &"//new_line('')//"&
                &Converted ASCII files must be in the ktable.exorem.txt output format"//new_line('')//"&
                &Gitlab of the software:"//new_line('')//"&
                &<https://gitlab.obspm.fr/dblain/exorem>"

            write(*, '(A)') help_str
        end subroutine print_help
end program ktables_txt2h5
