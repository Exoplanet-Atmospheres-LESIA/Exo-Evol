program ckc
    ! """
    ! Calculate k coefficients based on absorption cross sections and write the results.
    ! """
    use math, only: pi, quicksort_index, search_sorted
    use k_correlation, only: g_vec, gtype, k_vec, nu_0_list, num_nu_0, num_t, num_vec, resolution, t_latt, w_vec, &
        allocate_k_correlation_parameters, compute_distribution_function, calculate_gauss_legendre_quadrature, &
        interpolate_k_vec
    use thermodynamics, only: n_levels, size_thermospace, pressure_space, temperature_space
    use species, only: species_names
    use spectrometrics, only: wavenumber_max, wavenumber_min, min_total_interval_size
    use interface, only: read_absorption_cross_sections_files, path_outputs, absorption_cross_section_file_prefix, &
        reallocate_1Ddouble, reallocate_1Dinteger, &
        path_absorption_cross_sections, path_data, path_k_coefficients, read_input_parameters
    use k_coefficients_interface, only: write_k_coeff

    implicit none
    
    character(len=*), parameter :: ckc_version = '8.2.1'

    call get_ckc_command_arguments()
    
    write(6, '("CKC ", A, /)') ckc_version

    call init_k_correlation()

    call calculate_k_coefficients()

    call write_k_coeff(&
        species_names(1), wavenumber_min, wavenumber_max, nu_0_list(:), num_nu_0, &
        pressure_space(:), n_levels, t_latt(:, :), size_thermospace, &
        g_vec(:), w_vec(:), k_vec(:, :, :, :), num_vec, path_k_coefficients &
    )

    write(6, '("CKC: done !", /)')

    contains
        subroutine calculate_k_coefficients()
            ! """
            ! Calculate the k coefficients.
            ! """
            implicit none

            integer :: &
                i, &                ! index
                i_0, &              ! indices of nearest_nu_min et al
                i_max, &            ! indices of nearest_nu_min et al
                i_min, &            ! indices of nearest_nu_min et al
                i_t, &              ! index
                j, &                ! index
                l, &                ! index
                num_g, &            ! num_g<>num_k when some k-values are equal
                num_k, &            ! Number of extracted monochromatic-k data
                num_phi             ! number of Discrete Instrument Function values

            doubleprecision :: &
                nearest_nu_0, &     ! wavenumber in k-Data Base which is nearest to the requested wavnumber
                nu_min, &           ! [nu_min,nu_max] = Wave Number Interval
                nu_max, &           ! [nu_min,nu_max] = Wave Number Interval
                wvn_min_cross_sections

            integer, dimension(:), allocatable :: &
                indx                ! Index array used by sort algorithm

            doubleprecision, dimension(:), allocatable :: &
                absorption_cross_sections, &
                g, &                ! (g,kg) = Full Distribution
                k_coefficients, &   ! k coefficients
                kg, &               ! kg(i) = k(indx(i)) if all k-values unequal
                nu, &               ! (cm-1) wavenumbers
                phi_dnu             ! Stieltjes Integrator

            call get_g_vector()

            call display_parameters()

            wvn_min_cross_sections = mod(wavenumber_min, min_total_interval_size)

            ! Main loop
            do j = 1, n_levels
                num_t(j) = 0

                do i_t = 1, size_thermospace
                    ! Read absorption data file
                    num_t(j) = num_t(j) + 1

                    call read_absorption_cross_sections_files(&
                        species_names(1), wvn_min_cross_sections, wavenumber_max, resolution, temperature_space(i_t), &
                        pressure_space(j), &
                        nu, absorption_cross_sections &
                    )

                    t_latt(num_t(j), j) = temperature_space(i_t)

                    write (*, '("Calculating for temperature = ", F0.4, " K", /, &
                                &16X, "pressure    = ", ES10.4, " Pa...", /)') &
                        temperature_space(i_t), pressure_space(j)

                    do l = 1, num_nu_0
                        ! Select nearest wavenumber
                        ! Note: The wave number associated with the created k-distribution file will be the wave number,
                        ! nearest_nu_0, in the monochromatic k-data base, which is closest to the requested value, nu_0.
                        ! The computed Instrument Function will be centered exactly at nearest_nu_0.
                        i_0 = search_sorted(nu, nu_0_list(l))
                        nearest_nu_0 = nu(i_0)

                        ! Spectral limits of instrument function
                        nu_min = nearest_nu_0 - 0.5 * resolution
                        nu_max = nearest_nu_0 + 0.5 * resolution

                        i_min = search_sorted(nu, nu_min)
                        i_max = search_sorted(nu, nu_max)

                        num_phi = i_max - i_min + 1

                        if(allocated(indx)) then
                            call reallocate_1Dinteger(indx, num_phi)
                            call reallocate_1Ddouble(g, num_phi)
                            call reallocate_1Ddouble(kg, num_phi)
                            call reallocate_1Ddouble(k_coefficients, num_phi)
                            call reallocate_1Ddouble(phi_dnu, num_phi)
                        else
                            allocate(indx(num_phi))
                            allocate(g(num_phi))
                            allocate(kg(num_phi))
                            allocate(k_coefficients(num_phi))
                            allocate(phi_dnu(num_phi))
                        end if

                        num_k = 0

                        do i = i_min, i_max
                            num_k = num_k + 1
                            k_coefficients(num_k) = absorption_cross_sections(i)
                        end do

                        ! Apply instrument function (2nd conjecture)
                        ! Wavenumber step is assumed to be constant within wide spectral intervals, and to vary sharply
                        ! between these intervals
                        do i = 1, num_phi - 1
                            phi_dnu(i) = (nu(i_min + i) - nu(i_min + i - 1))
                        end do !  Stieltjes Integrator

                        phi_dnu(num_phi) = phi_dnu(num_phi - 1)

                        ! Get ascending indices for k
                        call quicksort_index(k_coefficients, indx)

                        ! Compute k-distribution
                        call compute_distribution_function(k_coefficients, num_k, indx, phi_dnu, g, kg, num_g)

                        ! Interpolate k_vec from kg at abscissas g_vec
                        call interpolate_k_vec(g, kg, num_g, g_vec, num_vec, k_vec(l, j, i_t, :))
                    end do ! wavenumber loop
                end do ! temperature Loop
            end do ! pressure Loop
        end subroutine calculate_k_coefficients

        subroutine display_parameters()
            ! """
            ! Display some useful informations.
            ! """
            implicit none

            integer :: &
                i   ! index

            ! Display parameters chosen and program constraints
            write(6, &
                '(/, "Number of parameters chosen, and maximum number allowed:", /,&
                &1X, "Wavenumbers: num_nu_0 = ", I0, /&
                &1X, "Points/Distribution:  num_vec = ", I0)') &
                num_nu_0, num_vec

            ! Display kg-lattice attributes
            write(6, &
                '(/, "kg-lattice attributes:", /,&
                &1X, "Instrument Function resolution: ", F0.3, " cm-1 (Spectral Width Between Inflection Points)")') &
                resolution

            ! Display decimating/quadrature values of g
            write(6, &
                '(/, I0, " quadrature values of g are used to decimate full k-Distributions.", /, &
                &"The decimated Distributions are written to the k-Distribution Lattice File.", /, &
                &"The associated quadrature weights w  are also listed:", /, &
                &6X, "g", 12X,"w")') &
                num_vec
            write(6, '(I5, F12.8, F12.8)') (i, g_vec(i), w_vec(i), i = 1, num_vec)

            write(6, '(/, I0, " distribution lattice files will be saved in directory ''", A, &
                &"''", /, "for central wavenumbers nu_0 between ", F0.2, " and ", F0.2, " cm-1")') &
                num_nu_0, trim(path_k_coefficients), nu_0_list(1), nu_0_list(num_nu_0)
        end subroutine display_parameters

        subroutine get_g_vector()
            ! """
            ! Get the g and w vectors.
            ! - One g-vector, g_vec, is used for all (g, k) distributions at all (nu, p, t) Lattice Sites in the
            !   Distribution Data Base for a given Instrument "MODE".
            ! - The components (g[i], w[i]) of (g_vec, w_vec) are Quadrature Rule (abscissa[i], weight[i]) for
            !   approximating integration on the unit interval [0, 1]
            ! """
            implicit none

            integer :: &
                i, &
                j, &
                num_vec_1, &
                num_vec_2

            if (trim(gtype) == 'Gauss-Legendre') then
                call calculate_gauss_legendre_quadrature(0d0, 1d0, num_vec, g_vec, w_vec)
            else if (trim(gtype) == 'Gauss-Legendre2') then
                ! Gauss-Legendre quadrature from 0 to 0.95 and from 0.95 to 1.
                num_vec_1 = num_vec / 2
                num_vec_2 = num_vec - num_vec_1

                call calculate_gauss_legendre_quadrature(0.95d0, 1d0, num_vec_2, g_vec, w_vec)

                do i = 1, num_vec_2
                    g_vec(i + num_vec_1) = g_vec(i)
                    w_vec(i + num_vec_1) = w_vec(i)
                end do

                call calculate_gauss_legendre_quadrature(0d0, 0.95d0, num_vec_1, g_vec, w_vec)
            else if(trim(gtype) == 'Gauss-Chebyshev')  then
                do i = 1, num_vec
                    j = num_vec + 1 - i
                    g_vec(i) = cos(pi / 2d0 * float(2 * j - 1) / float(2 * num_vec + 1))**2
                    w_vec(i) = 2 * pi * sqrt(g_vec(i) * (1d0 - g_vec(i))) / &
                               dble(2 * num_vec + 1)
                end do
            else
                write(6, &
                    '("Error: integration type ''", A, "'' not recognized", /, &
                    &"Integration types available: Gauss-Legendre|Gauss-Legendre2|Gauss-Chebyshev")') trim(gtype)
                stop
            end if

            ! Safety check
            if(sum(w_vec) <= 1d0 - 1d-12 .or. sum(w_vec) >= 1d0 + 1d-12) then
                write(6, '("Error: the sum of the quadrature weights of the g-vector must be exactly 1, but is ", &
                    &F0.12)') &
                    sum(w_vec)
                stop
            end if
        end subroutine

        subroutine init_k_correlation()
            ! """
            ! """
            use k_correlation, only: allocate_k_correlation_parameters, resolution, nu_0_list, num_nu_0
            use spectrometrics, only: wavenumber_min, wavenumber_max

            implicit none

            integer :: &
                i

            call read_input_parameters()

            ! Initialise wavenumbers for k-correlation
            if (wavenumber_min > 0d0 .and. resolution > 0d0) then
                i = 1
                allocate(nu_0_list(1))
                nu_0_list(1) = wavenumber_min + resolution / 2d0

                do while(nu_0_list(i) < wavenumber_max - resolution / 2d0)
                    i = i + 1

                    if(i > size(nu_0_list)) then
                        call reallocate_1Ddouble(nu_0_list, (i - 1) * 2)
                    end if

                    nu_0_list(i) = wavenumber_min + resolution * (dble(i - 1) + 0.5d0)
                end do

                num_nu_0 = i
            else
                write(6, '("Error: unable to create spectral array with minimum wavenumber lower than 0 (", F0.3, &
                    &" cm-1) or wavenumber step lower than 0 (", F0.3, " cm-1)", /, &
                    &"Ensure both of these parameters are greater than 0")') &
                    wavenumber_min, resolution
                stop
            end if

            call allocate_k_correlation_parameters()
        end subroutine init_k_correlation
    
        subroutine get_ckc_command_arguments()
            ! """
            ! Retrieve the CKC arguments that were passed on the command line when it was invoked.
            ! """
            implicit none

            character(len=255) :: arg
            integer :: i, j, n_args, n_opt_args

            j = 0
            n_args = 0
            n_opt_args = 0

            do i = 1, command_argument_count()
                call get_command_argument(i, arg)

                if(trim(arg) == '-v' .or. trim(arg) == '--version') then
                    write(*, '("CKC ", A)') ckc_version

                    stop
                elseif(trim(arg) == '-h' .or. trim(arg) == '--help') then
                    call print_help()

                    stop
                else
                    j = j + 1
                end if

                if (len_trim(arg) == 0) then
                    if (j < n_args) then
                        write(*, '("Error: txt2h5 takes ", I0, " arguments but ", I0, " were given")') &
                            n_args, j
                        stop
                    end if

                    exit
                end if

                if(j > n_args) then
                    write(*, '("Error: ckc takes ", I0, " arguments but ", I0, " were given")') &
                        n_args + n_opt_args, j

                    stop
                end if
            end do
        end subroutine get_ckc_command_arguments

        subroutine print_help()
            implicit none

            character(len=*), parameter :: help_str = &
                "Usage: ./ckc.exe [options]"&
                //new_line('')//"&
                &Options: "//new_line('')//"&
                &  --help         Display this information."//new_line('')//"&
                &  --version      Display software version information."//new_line('')//"&
                &"//new_line('')//"&
                &Input parameters must be in the file '../inputs/input_parameters.nml'."//new_line('')//"&
                &A working example of this file is available in the Gitlab of the software."//new_line('')//"&
                &Gitlab of the software:"//new_line('')//"&
                &<https://gitlab.obspm.fr/dblain/exorem>"

            write(*, '(A)') help_str
        end subroutine print_help
end program ckc