! """
! Contains modules representing pseudo-objects.
! """
module files
    ! """
    ! """
    implicit none

    integer, parameter :: &
        file_name_size = 255  ! maximum number of characters in a file name
end module files


module atmosphere
    ! """
    ! """
    use files, only: file_name_size

    implicit none

    character(len=file_name_size) :: &
        eddy_mode  ! eddy calculations:
                   !    'constant': constant across the pressure grid
                   !    'Ackerman': from Ackerman & Marley
                   !    'AckermanConvective': same as Ackerman, but takes convection into account
                   !    'infinity': same as AckermanConvective, but is set to infinity for clouds if r is fixed

    integer :: &
        n_layers, &
        n_levels

    doubleprecision :: &
        h2_vmr, &
        he_vmr, &
        metallicity, &
        pressure_min, &
        pressure_max, &
        z_vmr

    doubleprecision, dimension(:), allocatable :: &
        eddy_diffusion_coefficient, &
        gravities_layers, &
        scale_height, &
        pressures, &
        temperatures, &
        pressures_layers, &
        temperatures_layers, &
        molar_masses_layers, &
        z

    doubleprecision, dimension(:, :), allocatable :: &
        tau_rayleigh

    doubleprecision, dimension(:, :, :), allocatable :: &
        tau

    save

    contains
        subroutine check_eddy_mode()
            ! """
            ! """
            implicit none

            integer, parameter :: n_modes = 4

            character(len=file_name_size), dimension(n_modes), parameter :: &
                possible_modes = [&
                    'constant          ', &
                    'Ackerman          ', &
                    'AckermanConvective', &
                    'infinity          ' &
                ]

            logical :: mode_exists
            integer :: i

            mode_exists = .False.

            do i = 1, n_modes
                if (trim(eddy_mode) == trim(possible_modes(i))) then
                    mode_exists = .True.

                    exit
                end if
            end do

            if (.not. mode_exists) then
                write(*, '("Error: eddy mode ''", A, "'' not implemented, possible eddy modes are:")') &
                    trim(eddy_mode)

                do i = 1, n_modes
                    write(*, '(1X, A)') trim(possible_modes(i))
                end do

                stop
            end if
        end subroutine check_eddy_mode
end module atmosphere


module exorem_retrieval
    ! """
    ! """
    implicit none

    integer :: &
        chemistry_iteration_interval, &  ! number of iterations between 2 calls the of the thermochemical equilibrium
        cloud_iteration_interval, &  ! number of iterations between 2 calls of the cloud physics
        n_burn_iterations, &  ! number of iterations where all the physics is calculated at every iterations
        n_iterations, &
        n_non_adiabatic_iterations, &
        retrieval_level_top, &
        retrieval_level_bottom

    doubleprecision :: &
        retrieval_flux_error_top, &
        retrieval_flux_error_bottom, &
        retrieval_tolerance, &
        smoothing_top, &
        smoothing_bottom, &
        weight_apriori
end module exorem_retrieval


module light_source
    ! """
    ! """
    implicit none

    doubleprecision :: &
        light_source_radius, &                  ! (m) radius of the light source
        light_source_range, &                   ! (m) distance between the target and the light source
        light_source_effective_temperature, &   ! (K) light src effective temperature
        light_source_irradiation, &             ! (W.m-2) light source irradiation
        incidence_angle                         ! (deg) incident light relative to the normal of the atmosphere TODO [low][future] unused yet

    doubleprecision, dimension(:), allocatable :: &
        light_source_irradiance

    save
end module light_source


module species
    ! """
    ! Contains the species parameters.
    ! """
    use files, only: file_name_size
    use physics, only: n_elements

    implicit none

    integer, parameter :: &
        element_symbol_size = 4, &
        species_name_size = 32

    character(len=element_symbol_size), dimension(n_elements) :: &
        elements_names

    character(len=species_name_size), dimension(:), allocatable :: &
        line_shape, &                                    ! shape of the lines of the species
        species_names, &                                 ! names of the species
        cia_names                                        ! names of the cia

    character(len=file_name_size), dimension(:), allocatable :: &
        cia_files, &                                    ! files containing the collision induced absorptions
        lines_files                                     ! files containing the lines of the species

    integer :: &
        n_broadenings_max = 0, &                        ! maximum number of broadenings
        n_cia = 0, &                                    ! number of collision induced absorptions to take into account
        n_electronic_states_max = 0, &                  ! maximum number of electronic states
        n_species, &                                    ! number of species
        n_vibrational_modes_max = 0                     ! maximum number of vibrationla modes

    logical, dimension(:), allocatable :: &
        species_at_equilibrium

    integer, dimension(:), allocatable :: &
        n_broadenings_species, &                        ! number of braodenings of each species
        n_electronic_states_species, &                  ! number of electronic states of each species
        n_vibrational_modes_species                     ! number of vibrational modes of each species

    integer, dimension(:, :), allocatable :: &
        electronic_states_degeneracies, &               ! electronic states degeneracies
        vibrational_modes_degeneracies                  ! vibrational modes degeneracies

    doubleprecision, dimension(n_elements) :: &
        elemental_abundances, &
        elemental_h_ratio, &
        solar_h_ratio

    doubleprecision, dimension(:), allocatable :: &
        cutoffs, &                                      ! (cm-1) sublorentzian cutoff of the species
        intensities_min, &                              ! (cm-1/(molecules.cm-2)) minimum line intensity to keep
        molar_masses, &                                 ! (kg.mol-1) molar masses of the species
        rotational_partition_exponents, &               ! rotational partition temperature exponent of the species
        species_metallicity, &                          ! metallicity (times solar) of the species
        species_vmr, &                                  ! Volume Mixing Ratio of the species
        temperature_intensities_min                     ! (K) reference temperature of the minimum line intensity

    doubleprecision, dimension(:, :), allocatable :: &
        electronic_states_wavenumbers, &                ! (cm-1) electronic states wavenumbers
        rayleigh_scattering_coefficients, &             ! (cm2) Rayleigh scattering coefficients of the species
        species_broadenings, &                          ! (cm.1.atm-1) broadenings of each species
        species_broadening_temperature_coefficients, &  ! temperature dependance coefficient of the broadenings
        species_vmr_layers, &                           ! Volume Mixing Ratio of the species at given layers
        sublimation_profiles_coefficients, &            ! sublimation profile coefficients (Fray & Schmitt 2009)
        vibrational_modes_wavenumbers                   ! (cm-1) vibrational modes wavenumbers

    save

    contains
        subroutine allocate_species_primary_attributes()
            ! """
            ! Allocate species attributes that do not depend of other species.
            ! """
            implicit none

            allocate(&
                cutoffs(n_species), &
                intensities_min(n_species), &
                lines_files(n_species), &
                line_shape(n_species), &
                molar_masses(n_species), &
                n_broadenings_species(n_species), &
                n_electronic_states_species(n_species), &
                n_vibrational_modes_species(n_species), &
                rotational_partition_exponents(n_species), &
                species_at_equilibrium(n_species), &
                species_names(n_species), &
                species_vmr(n_species), &
                temperature_intensities_min(n_species) &
            )

            cutoffs(:) = 0d0
            intensities_min(:) = 0d0
            lines_files(:) = ''
            line_shape(:) = ''
            molar_masses(:) = 0d0
            n_broadenings_species(:) = 0
            n_electronic_states_species(:) = 0
            n_vibrational_modes_species(:) = 0
            rotational_partition_exponents(:) = 0d0
            species_at_equilibrium(:) = .True.
            species_names(:) = ''
            species_vmr(:) = 0d0
            temperature_intensities_min(:) = 0d0
        end subroutine allocate_species_primary_attributes

        subroutine allocate_species_secondary_attributes()
            ! """
            ! Allocate species attributes that depends of all the species.
            ! """
            implicit none

            n_broadenings_max = maxval(n_broadenings_species)
            n_electronic_states_max = maxval(n_electronic_states_species)
            n_vibrational_modes_max = maxval(n_vibrational_modes_species)

            allocate(&
                electronic_states_degeneracies(n_species, n_electronic_states_max), &
                electronic_states_wavenumbers(n_species, n_electronic_states_max), &
                species_broadenings(n_species, n_broadenings_max), &
                species_broadening_temperature_coefficients(n_species, n_broadenings_max), &
                vibrational_modes_degeneracies(n_species, n_vibrational_modes_max), &
                vibrational_modes_wavenumbers(n_species, n_vibrational_modes_max) &
            )

            electronic_states_degeneracies(:, :) = 0
            electronic_states_wavenumbers(:, :) = 0d0
            species_broadenings(:, :) = 0d0
            species_broadening_temperature_coefficients(:, :) = 0d0
            vibrational_modes_degeneracies(:, :) = 0
            vibrational_modes_wavenumbers(:, :) = 0d0
        end subroutine allocate_species_secondary_attributes

        function get_ph3_absorption_cross_section_correction_factor(temperature) result(correction_factor)
            ! """
            ! Get PH3 absorption correction factor due to the lack of completness of Sousa-Silva et al. line list.
            ! Source: Sousa-Silva et al. 2014, doi:10.1093/mnras/stu2246
            ! """
            implicit none

            doubleprecision, intent(in) :: temperature

            doubleprecision, dimension(6), parameter :: &
                temperatures_ref = (/1014d0, 1146d0, 1500d0, 1797d0, 2000d0, 2500d0/), &
                line_list_completnesses = (/1d0 - 1d-15, 0.99d0, 0.91d0, 0.80d0, 0.70d0, 0.50d0/)

            integer :: &
                i

            doubleprecision :: &
                a, &
                b, &
                correction_factor

            if(temperature <= temperatures_ref(1)) then
                correction_factor = 1d0
            else
                do i = 1, size(temperatures_ref) - 1
                    if(temperature > temperatures_ref(i)) then
                        call get_ab(&
                            temperatures_ref(i), &
                            temperatures_ref(i + 1), &
                            line_list_completnesses(i), &
                            line_list_completnesses(i + 1), &
                            a, &
                            b &
                        )

                        correction_factor = 1d0 - exp(-a / (temperature - b))

                        write(*, '("Warning: applying a correction factor of ", F0.3, &
                            &" to PH3 lines intensity, according Sousa-Silva et al. 2014", /, &
                            &"Ensure that the PH3 line list you are using come from this source")') &
                            1d0 / correction_factor

                        return
                    end if
                end do
            end if

            contains
                subroutine get_ab(x0, x1, y0, y1, a, b)
                    ! """
                    ! Find parameters a and b of function f(x) = 1 - exp(-a / (x - b)) given f(x0) and f(x1).
                    ! :param x0: lower bound
                    ! :param x1: greater bound
                    ! :param y0: f(x0)
                    ! :param y1: f(x1)
                    ! :return a: parameter a of f(x)
                    ! :return b: parameter b of f(x)
                    ! """
                    implicit none

                    doubleprecision, intent(in) :: x0, x1, y0, y1
                    doubleprecision, intent(out) :: a, b

                    b = (log(1d0 - y0) / log(1d0 - y1) * x0 - x1) / (log(1d0 - y0) / log(1d0 - y1) - 1d0)
                    a = -log(1d0 - y1) * (x1 - b)
                end subroutine get_ab
        end function get_ph3_absorption_cross_section_correction_factor
end module species


module spectrometrics
    ! """
    ! Contains the spectromietric parameters.
    ! """
    implicit none

    integer :: &
        n_wavenumbers

    doubleprecision :: &
        wavenumber_max, &               ! (cm-1) spectral interval upper bound
        wavenumber_min, &               ! (cm-1) spectral interval lower bound
        wavenumber_step, &              ! (cm-1) maximal wavenumber step
        doppler_deviation_tolerance, &  ! maximum deviation of the Doppler width between the borders of a sub-interval
        min_total_interval_size         ! (cm-1) minimum size of a parallelised spectral interval

    doubleprecision, dimension(:), allocatable :: &
        spectral_radius, &
        wavenumbers

    save
end module spectrometrics


module cloud
    ! """
    ! """
    use files, only: file_name_size
    use species, only: species_name_size

    implicit none

    character(len=file_name_size) :: &
        cloud_mode  ! cloud mode:
                    !    'fixedRadius': cloud particle radius is fixed
                    !    'fixedSedimentation': cloud sedimentation parameter is fixed
                    !    'fixedRadiusCondensation': cloud particle radius fixed by f_sed at condensation level
                    !    'fixedRadiusTime': cloud particle radius fixed by condensation timescale

    character(len=file_name_size), dimension(:), allocatable :: &
        cloud_opacity_files

    character(len=species_name_size), dimension(:), allocatable :: &
        cloud_names

    integer :: &
        n_clouds

    doubleprecision :: &
        cloud_fraction

    integer, dimension(:), allocatable :: &
        n_cloud_particle_radii

    doubleprecision, dimension(:), allocatable :: &
        sedimentation_parameter, &
        supersaturation_parameter, &
        sticking_efficiency, &
        cloud_particle_density, &
        cloud_molar_mass, &
        reference_wavenumber

    doubleprecision, dimension(:, :), allocatable :: &
        cloud_particle_radius, &
        q_ext_ref  ! [for diagnostic] extinction coefficient at reference wavenumner

    doubleprecision, dimension(:, :, :), allocatable :: &
        cloud_particle_radius_data, &
        qext, &
        gfactor, &
        omeg, &
        qscat, &
        qabs

    doubleprecision, dimension(:, :, :), allocatable :: &
        asymetry_factor, &
        single_scattering_albedo, &
        q_scat, &
        q_ext

    save

    contains
        subroutine check_cloud_mode()
        ! """
        ! """
        implicit none

        integer, parameter :: n_modes = 4

        character(len=file_name_size), dimension(n_modes), parameter :: &
            possible_modes = [&
                'fixedRadius            ', &
                'fixedSedimentation     ', &
                'fixedRadiusCondensation', &
                'fixedRadiusTime        ' &
            ]

        logical :: mode_exists
        integer :: i

        mode_exists = .False.

        do i = 1, n_modes
            if (trim(cloud_mode) == trim(possible_modes(i))) then
                mode_exists = .True.

                exit
            end if
        end do

        if (.not. mode_exists) then
            write(*, '("Error: cloud mode ''", A, "'' not implemented, possible cloud modes are:")') &
                trim(cloud_mode)

            do i = 1, n_modes
                write(*, '(1X, A)') trim(possible_modes(i))
            end do

            stop
        end if
    end subroutine check_cloud_mode

        subroutine update_cloud_optical_parameters()
            ! """
            ! Update the cloud optical parameters according to the calculated radius.
            ! """
            use math, only: interp_ex_0d
            use atmosphere, only: n_layers
            use spectrometrics, only: n_wavenumbers, wavenumbers

            implicit none

            integer :: i, j, k

            do i = 1, n_clouds
                do j = 1, n_layers
                    do k = 1, n_wavenumbers
                        q_ext(i, k, j) = &
                            max(interp_ex_0d(&
                                    cloud_particle_radius(i, j), &
                                    cloud_particle_radius_data(i, :n_cloud_particle_radii(i), k), &
                                    qext(i, :n_cloud_particle_radii(i), k)&
                                ), 0d0)
                        q_scat(i, k, j) = &
                            max(interp_ex_0d(&
                                    cloud_particle_radius(i, j), &
                                    cloud_particle_radius_data(i, :n_cloud_particle_radii(i), k), &
                                    qscat(i, :n_cloud_particle_radii(i), k)&
                                ), 0d0)
                        single_scattering_albedo(i, k, j) = &
                            min(max(interp_ex_0d(&
                                        cloud_particle_radius(i, j), &
                                        cloud_particle_radius_data(i, :n_cloud_particle_radii(i), k), &
                                        omeg(i, :n_cloud_particle_radii(i), k)&
                                    ), 0d0), 1d0)
                        asymetry_factor(i, k, j) = &
                            min(max(interp_ex_0d(&
                                        cloud_particle_radius(i, j), &
                                        cloud_particle_radius_data(i, :n_cloud_particle_radii(i), k), &
                                        gfactor(i, :n_cloud_particle_radii(i), k)&
                                    ), -1d0), 1d0)
                    end do

                    q_ext_ref(i, j) = interp_ex_0d(reference_wavenumber(i), wavenumbers, q_ext(i, :, j))
                end do
            end do
        end subroutine update_cloud_optical_parameters
end module cloud


module target
    implicit none

    doubleprecision :: &
        emission_angle, &                   ! (deg) emission angle ! TODO [low][future] unused yet
        latitude, &                         ! (deg) latitude of observation on the target
        target_internal_temperature, &      ! (K) internal temperature of the target
        target_flattening, &                ! flattening of the target
        target_gravity, &                   ! (m.s-2) surface gravity of the target
        target_equatorial_radius, &         ! (m) equatorial radius of the target
        target_equatorial_gravity, &
        target_mass, &                      ! (kg) mass of the target
        target_polar_radius, &              ! (m) polar radius of the target
        target_radius                       ! (m) radius of the target

end module target


module thermodynamics
    ! """
    ! Contain the thermodynamic parameters.
    ! """
    implicit none

    integer :: &
        n_levels, &         ! number of atmospheric levels
        size_thermospace    ! always =1 if profile mode is activated, number of temperatures otherwise

    doubleprecision, dimension(:), allocatable :: &
        pressure_space, &   ! (Pa) pressures where to calculate the absorption cross sections
        temperature_space   ! (K) temperatures where to calculate the absorption cross sections

    save
    
    contains
        subroutine allocate_thermodynamics_attributes()
            ! """
            ! Allocate the thermodynamic attributes.
            ! """
            implicit none

            allocate(&
                pressure_space(n_levels), &
                temperature_space(size_thermospace) &
            )

            pressure_space(:) = 0d0
            temperature_space(:) = 0d0
        end subroutine allocate_thermodynamics_attributes
end module thermodynamics
