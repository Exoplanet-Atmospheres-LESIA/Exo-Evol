module physics
    ! """
    ! Contains useful physical functions and constants.
    ! Sources:
    !   - CODATA 2018: https://physics.nist.gov/cgi-bin/cuu/Category?view=pdf&All+values.x=69&All+values.y=14
    ! """
    implicit none

    integer, parameter :: &
        n_elements = 118            ! number of (relevant) elements

    doubleprecision, parameter :: &
        cst_pi = 3.141592653589793d0, & ! pi

        cst_c = 2.99792458d8 , &    ! (m.s-1) speed of light in vacuum (CODATA 2018, exact)
        cst_h = 6.62607015d-34, &   ! (J.s) Planck constant (CODATA 2018, exact)
        cst_k = 1.380649d-23, &     ! (J.K-1) Boltzmann constant (CODATA 2018, exact)
        cst_N_A = 6.02214076d23, &  ! (mol-1) Avogadro constant (CODATA 2018, exact)
        cst_p0 = 1.01325d5, &       ! (Pa) 1 atm (CODATA 2018, also reference pressure for GEISA 2015, exact)

        cst_t0 = 273.15d0, &        ! (K) Loschmidt reference temperature: 0 degree Celsius
        cst_t_ref = 296d0, &        ! (K) GEISA 2015 database reference temperature

        cst_G = 6.67430d-11, &      ! (m3.kg-1.s-2) newtonian constant of gravitation (CODATA 2018, +/- 2.2e-5)

        cst_hbar = &                ! (J.s) reduced Planck constant (CODATA 2018, exact)
            cst_h / (2d0 * cst_pi), &
        cst_n0 = &                  ! (m-3) Loschmidt constant at 273.15 K and 1 bar (CODATA 2018, exact)
            cst_p0 / (cst_k * cst_t0), &
        cst_R = &                   ! (J.mol-1.K-1) molar gas constant (CODATA 2018, exact)
            cst_N_A * cst_k, &
        cst_sigma = &               ! (W.m2.K-4) Stefan-Boltzmann constant (CODATA 2018, exact)
            (cst_pi ** 2d0 / 60d0) * cst_k ** 4d0 / (cst_hbar ** 3d0 * cst_c ** 2d0)

    character(len=2), dimension(n_elements) :: &
        elements_symbol = [ &       ! elements symbols ordered by atomic number from 1 to 118
            'H ', 'He', &
            'Li', 'Be', 'B ', 'C ', 'N ', 'O ', 'F ', 'Ne', &
            'Na', 'Mg', 'Al', 'Si', 'P ', 'S ', 'Cl', 'Ar', &
            'K ', 'Ca', 'Sc', 'Ti', 'V ', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', &
            'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',&
            'Rb', 'Sr', 'Y ', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', &
            'Cd', 'In', 'Sn', 'Sb', 'Te', 'I ', 'Xe',&
            'Cs', 'Ba', 'La', &
            'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', &
            'Hf', 'Ta', 'W ', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At', 'Rn', &
            'Fr', 'Ra', 'Ac', &
            'Th', 'Pa', 'U ', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr', &
            'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og' &
    ]

    doubleprecision, dimension(n_elements) :: &
        elements_molar_mass = [ &       ! elements symbols ordered by atomic number from 1 to 118
            1.00794d-3, 4.002602d-3, &
            6.941d-3, 9.012182d-3, 10.811d-3, 12.0107d-3, 14.0067d-3, 15.9994d-3, 18.9984032d-3, 20.1797d-3, &
            22.98976928d-3, 24.3050d-3, 26.9815386d-3, 28.0855d-3, 30.973762d-3, 32.065d-3, 35.453d-3, 39.948d-3, &
            39.0983d-3, 40.078d-3, 44.955912d-3, 47.867d-3, 50.9415d-3, 51.9961d-3, 54.938045d-3, 55.845d-3, &
            58.933195d-3, 58.6934d-3, 63.546d-3, &
            65.409d-3, 69.723d-3, 72.64d-3, 74.92160d-3, 78.96d-3, 79.904d-3, 83.798d-3, &
            85.4678d-3, 87.62d-3, 88.90585d-3, 91.224d-3, 92.90638d-3, 95.94d-3, 98d-3, 101.07d-3,  102.90550d-3, &
            106.42d-3, 107.8682d-3, &
            112.411d-3, 114.818d-3, 118.710d-3, 121.760d-3, 127.60d-3, 126.90447d-3, 131.293d-3, &
            132.9054519d-3, 137.327d-3, 138.90547d-3, &
            140.116d-3, 140.90765d-3, 144.242d-3, 145d-3, 150.36d-3, 151.964d-3, 157.25d-3, 158.92535d-3, 162.500d-3, &
            164.93032d-3, &
            167.259d-3, 168.93421d-3, 173.04d-3, 174.967d-3, &
            178.49d-3, 180.94788d-3, 183.84d-3, 186.207d-3, 190.23d-3, 192.217d-3, 195.084d-3, 196.966569d-3, &
            200.59d-3, 204.3833d-3, 207.2d-3, 208.98040d-3, 210d-3, 210d-3, 222d-3, &
            223d-3, 226d-3, 227d-3, &
            232.03806d-3, 231.03588d-3, 238.02891d-3, &  ! uranium, below are purely unstable/synthetic elements
            237d-3, 244d-3, 243d-3, 247d-3, 247d-3, 252d-3, 252d-3, 257d-3, 258d-3, 259d-3, 266d-3, &
            267d-3, 268d-3, 269d-3, 278d-3, 269d-3, 282d-3, 281d-3, 286d-3, 285d-3, 286d-3, &
            290d-3, 290d-3, 293d-3, 294d-3, 295d-3 &
    ]   ! (kg.mol-1) Source: Weiser 2005 (http://publications.iupac.org/pac/2006/pdf/7811x2051.pdf), for unstable
        ! elements, the most stable (naturally occuring if applicable) isotope was used

    save

    contains
        function planck_function(wavenumber, temperature) result(spectral_radiance)
            ! """
            ! Calculate the Planck function.
            ! The facor 1D2 is for the conversion from m.s-1 to cm.s-1. The factor 1D4 is for the conversion from
            ! W.cm-2.sr-1/cm-1 to W.m-2.sr-1/cm-1.
            ! :param wavenumber: (cm-1) wavenumber
            ! :param temperature: (K) temperature
            ! :return spectral_radiance: (W.m-2.sr-1/cm-1) the spectral radiance
            ! """
            implicit none

            double precision, intent(in) :: temperature, wavenumber
            double precision :: spectral_radiance  ! (W.m-2.sr-1/cm-1) spectral radiance of a black body

            spectral_radiance = 2D0 * cst_h * (cst_c * 1D2)**2 * wavenumber**3 / &
                (exp(cst_h * (cst_c * 1D2) * wavenumber / (cst_k * temperature)) - 1D0) * 1D4

            return
        end function planck_function


        function spherical_black_body_spectral_radiance(&
                wavenumber, temperature, radius, distance, redistribution_factor&
            ) result(spectral_radiance)
            ! """
            ! Calculate the Planck function.
            ! The facor 1D2 is for the conversion from m.s-1 to cm.s-1. The factor 1D4 is for the conversion from
            ! W.cm-2.sr-1/cm-1 to W.m-2.sr-1/cm-1.
            ! :param wavenumber: (cm-1) wavenumber
            ! :param temperature: (K) temperature
            ! :param radius: (m) radius of the spherical black body
            ! :param distance: (m) distance from the spherical black body
            ! :param redistribution_factor: redistribution factor to take heat redistribution into account
            ! """
            implicit none

            double precision, intent(in) :: distance, redistribution_factor, radius, temperature, wavenumber
            double precision :: spectral_radiance  ! (W.m-2.sr-1/cm-1) spectral radiance of a black body

            spectral_radiance = &
                planck_function(wavenumber, temperature) * (radius / distance) ** 2d0 * redistribution_factor

            return
        end function spherical_black_body_spectral_radiance
end module physics