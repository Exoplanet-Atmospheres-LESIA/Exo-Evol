module interface
    ! """
    ! Contains all the parameters, functions and subroutines to manage the interface between the code and the data.
    ! """
    use files, only: file_name_size
    use math, only: reallocate_1Ddouble, reallocate_1Dinteger, reallocate_2Ddouble, reallocate_2Dinteger

    implicit none

    character(len=*), parameter :: &
        input_file = '../inputs/input_parameters.nml', &! contains all the input parameters
        output_files_extension = '.dat'                 ! output files extension

    logical :: &
        add_light_source, &                             ! if True, add the light source
        output_cia_spectral_contribution, &             ! if True, output the CIA spectral contribution
        output_species_spectral_contributions, &        ! if True, output the absorbers spectral contribution
        output_species_by_species, &                    ! if True, output one file per species
        output_thermal_spectral_contribution, &         ! if True, output the thermal spectral contribution
        use_profile_mode, &                             ! if True, pressure and temperature spaces are read as p(T)
        output_full, &                                  ! if True, output other gases, sum of gases and saturation pressures               
        load_kzz_profile, &                             ! if True, use initial Kzz profile
        load_cloud_profiles                             ! if True, use initial cloud profile


    character(len=file_name_size) :: &
        absorption_cross_section_file_prefix, &         ! prefix of the absorption cross section files
        light_source_spectrum_file, &
        kernel_file_out, &
        spectrum_derivative_file_out, &
        spectrum_file_out, &                            ! output spectrum file
        temperature_profile_file_out, &
        vmr_file_out, &
        spectrum_file_prefix, &                         ! prefix of the spectrum files
        temperature_profile_file_prefix, &              ! prefix of the temperature profile files
        temperature_profile_file, &                     ! input temperature profile file
        output_files_suffix, &                          ! suffix of the output files
        vmr_file_prefix, &                              ! prefix of the volume mixing ratio files
        vmr_profiles_file, &                            ! input vmr profile file
        voigt_data_file, &                              ! name of the Voigt data file
        path_absorption_cross_sections, &               ! path to the absorption cross sections files
        path_cia, &                                     ! path to collision induced absorption files
        path_clouds, &                                  ! path to clouds parameters
        path_data, &                                    ! path to data
        path_k_coefficients, &                          ! path to the k distribution files
        path_lines, &                                   ! path to lines data
        path_temperature_profile, &                     ! path to the input temperature profile file
        path_vmr_profiles, &                            ! path to the vmr profile file
        path_species_data, &                            ! path to species data
        path_light_source_spectra, &                    ! path to the spectrum of the light source
        path_outputs                                    ! path to the output directory

    doubleprecision :: &
        time_begin, &                                   ! (s) store the time of the beginning of an execution
        time_end                                        ! (s) store the time of the end of an execution

    save

    contains
        function count_lines(file) result(n_lines)
            ! """
            ! Count the number of lines in a file.
            ! :param file: file to read
            ! :return n_lines: the number of lines in the file
            ! """
            implicit none

            character(len=*), intent(in) :: file

            character(len=1) :: &
                dump  ! dump value used to read the lines of the file

            integer :: &
                file_unit, &
                io, &   ! store the io status of the file
                n_lines ! number of lines in the file

            open(newunit=file_unit, file=file, status='old')

            n_lines = 0

            do
                read(file_unit, '(A1)', iostat=io) dump

                if(io < 0) then
                    exit
                end if

                n_lines = n_lines + 1
            end do

            close(file_unit)

            return
        end function count_lines


        function count_substring(string, substring) result(c)
            implicit none

            character(len=*), intent(in) :: string, substring
            integer :: c, p, posn

            c = 0

            if(len(substring) == 0) then
                return
            end if

            p = 1

            do
                posn = index(string(p:), substring)

                if(posn == 0) then
                    return
                end if

                c = c + 1
                p = p + posn + len(substring)
            end do
        end function count_substring


        subroutine get_rey_ch4_lines_file(&
            temperature, wavenumber_max, &
            rey_ch4_lines_file, rey_ch4_superlines_file, t_ref&
        )
            ! """
            ! Get the names of Rey et al. 2017 CH4 "hybrid" files (http://theorets.tsu.ru/molecules.ch4.hybrid).
            ! The file names depends on the temperature.
            ! Return an error if the temperature is above the maximum temperature of the files, or if the maximum
            ! wavenumber is above the maximum wavenumber in the files.
            ! :param temperature: (K) temperature of the simulated atmospheric level
            ! :param wavenumber_max: (cm-1) maximum wavenumber of the simulated spectral interval
            ! :return rey_ch4_lines_file: the name of the CH4 lines file corresponding to the given temperature
            ! :return rey_ch4_superlines_file: the name of the corresponding CH4 superlines file
            ! :return t_ref: (K) the reference temperature of the CH4 lines file
            ! """
            implicit none

            doubleprecision, intent(in) :: temperature, wavenumber_max

            character(len=file_name_size), intent(out) :: rey_ch4_lines_file, rey_ch4_superlines_file
            doubleprecision, intent(out) :: t_ref

            character(len=*), parameter :: &
                ch4_lines_directory = 'CH4', &                    ! sub directory containing the CH4 lines
                ch4_strong_lines_prefix = 'Pred_12CH4_T', &       ! file name prefix of the CH4 strong lines
                ch4_strong_lines_suffix = '_StrongLines.pred', &  ! file name suffix of the CH4 strong lines
                ch4_weak_lines_prefix = 'Superlines_12CH4_', &    ! file name prefix of the CH4 (weak) superlines
                ch4_weak_lines_suffix = '_WeakLines_QC.pred'      ! file name suffix of the CH4 (weak) superlines

            logical :: &
                file_exists = .False.                             ! store if the searched file exists

            integer, parameter :: &
                temperature_increment = 20, &                     ! (K) CH4 lines files reference temperature increment
                temperature_line_max = 3000, &                    ! (K) maximum reference temperature of the CH4 files
                wavenumber_increment = 100, &                     ! (cm-1) wavenumber increment of the CH4 files
                wavenumber_line_max = 13400, &                    ! (cm-1) maximum wavenumber of the CH4 files
                wavenumber_line_min = 0                           ! (cm-1) minimum wavenumber of the CH4 files

            integer :: &
                temperature_file, &                               ! (K) reference temperature of a CH4 file
                wavenumber_file                                   ! (cm-1) maximum wavenumber of a CH4 file

            call check_inputs()

            rey_ch4_lines_file = get_rey_lines_file()

            t_ref = temperature_file

            rey_ch4_superlines_file = get_rey_superlines_file()

            contains
                subroutine check_inputs()
                    ! """
                    ! Check the subroutine inputs.
                    ! """
                    implicit none

                    if(temperature > dble(temperature_line_max)) then
                        write(*, '("Error: desired temperature (", F6.0, " K) &
                            &is above maximum temperature available in lines data (", I4, " K)")') &
                            temperature, temperature_line_max

                        stop
                    end if

                    if(wavenumber_max > wavenumber_line_max) then
                        write(*, '("Error: wavenumber interval upper bound (", F7.0, " cm-1) &
                            &is above maximum wavenumber available in lines data (", I5, " cm-1)")') &
                            wavenumber_max, wavenumber_line_max

                        stop
                    end if

                    if(wavenumber_max > wavenumber_line_max) then
                        write(*, '("Error: wavenumber interval upper bound (", F7.0, " cm-1) &
                            &is above maximum wavenumber available in lines data (", I5, " cm-1)")') &
                            wavenumber_max, wavenumber_line_max

                        stop
                    end if
                end subroutine check_inputs

                function get_rey_lines_file() result(lines_file)
                    ! """
                    ! Get the CH4 strong lines file.
                    ! :return lines_file: the CH4 strong lines file
                    ! """
                    implicit none

                    character(len=file_name_size) :: lines_file

                    temperature_file = ceiling(temperature / dble(temperature_increment)) * temperature_increment
                    wavenumber_file = wavenumber_line_max

                    do while(temperature_file <= temperature_line_max)
                        do while(wavenumber_file > wavenumber_line_min)
                            write(lines_file, '(A, I0, "_", I0, "_", I0, A)') &
                                trim(path_lines) // trim(ch4_lines_directory) // '/' // trim(ch4_strong_lines_prefix), &
                                temperature_file, wavenumber_line_min, wavenumber_file, &
                                trim(ch4_strong_lines_suffix)

                            inquire(file=lines_file, exist=file_exists)

                            if(file_exists) then
                                exit
                            else
                                wavenumber_file = wavenumber_file - wavenumber_increment
                            end if
                        end do

                        if(file_exists) then
                            exit
                        else
                            temperature_file = temperature_file + temperature_increment
                            wavenumber_file = wavenumber_line_max
                        end if
                    end do
                end function get_rey_lines_file

                function get_rey_superlines_file() result(lines_file)
                    ! """
                    ! Get the CH4 (weak) superlines file.
                    ! :return lines_file: the CH4 superlines file
                    ! """
                    implicit none

                    character(len=file_name_size) :: lines_file

                    temperature_file = nint(temperature / dble(temperature_increment)) * temperature_increment
                    wavenumber_file = wavenumber_line_max

                    do while(temperature_file <= temperature_line_max)
                        do while(wavenumber_file > wavenumber_line_min)
                            write(lines_file, '(A, I0, "K_", I0, "_", I0, A)') &
                                trim(path_lines) // trim(ch4_lines_directory) // '/' // trim(ch4_weak_lines_prefix), &
                                temperature_file, wavenumber_line_min, wavenumber_file, &
                                trim(ch4_weak_lines_suffix)

                            inquire(file=lines_file, exist=file_exists)

                            if(file_exists) then
                                exit
                            else
                                wavenumber_file = wavenumber_file - wavenumber_increment
                            end if
                        end do

                        if(file_exists) then
                            exit
                        else
                            temperature_file = temperature_file + temperature_increment
                            wavenumber_file = wavenumber_line_max
                        end if
                    end do
                end function get_rey_superlines_file
        end subroutine get_rey_ch4_lines_file


        subroutine read_absorption_cross_sections_files(&
            species_name, wavenumber_min, wavenumber_max, resolution, temperature, pressure, &
            wavenumbers, absorption_cross_sections &
        )
            ! """
            ! Read the calculated absorption cross sections of a species.
            ! :param species_name: name of the species
            ! :param wavenumber_min: (cm-1) minimum wavenumber of the spectral interval
            ! :param wavenumber_max: (cm-1) maximum wavenumber of the spectral interval
            ! :param resolution: (cm-1) tolerance toward wavenumber_max when reading multiple AXS files
            ! :param temperature: (K) temperature of calculation
            ! :param pressure: (Pa) pressure of calculation
            ! :return wavenumbers: (cm-1) wavenumbers of the absorption cross sections
            ! :return absorption_cross_sections: (cm2) absorption cross sections calculated at the given T and p
            ! """
            use species, only: species_name_size
            use spectrometrics, only: min_total_interval_size

            implicit none

            character(len=species_name_size), intent(in) :: species_name
            doubleprecision, intent(in) :: wavenumber_min, wavenumber_max, temperature, resolution, pressure

            doubleprecision, dimension(:), allocatable, intent(out) :: absorption_cross_sections, wavenumbers

            logical :: &
                file_exists

            character(len=species_name_size):: &
                species_name_file

            character(len=file_name_size) :: &
                sigma_file  ! file where to write the absorption cross sections

            integer :: &
                i, &        ! index
                i0, &
                io, &
                file_unit, &   ! unit of the opened file
                n_wavenumbers, &
                n_wavenumbers_subinterval

            doubleprecision :: &
                pressure_file, &
                temperature_file, &
                wvn_max, &
                wvn_min, &
                wvn_max_subinterval, &
                wvn_min_subinterval, &
                wavenumber_step

            n_wavenumbers = 0

            write(sigma_file, '(A, "_", ES9.3, "-", ES9.3, "cm-1_", ES9.3, "K_", ES9.3, "Pa")') &
                  trim(species_name), wavenumber_min, wavenumber_max, &  ! TODO [low] improve species name writting ?
                  temperature, pressure
            sigma_file = trim(path_absorption_cross_sections) // trim(absorption_cross_section_file_prefix) // &
                '_' // trim(sigma_file) // '.dat'

            inquire(file=trim(sigma_file), number=file_unit, exist=file_exists)

            if(.not. file_exists) then
                call read_multiple_axs_files()
            else
                call read_1_axs_file()
            end if

            contains
                subroutine check_file()
                    ! """
                    ! """
                    implicit none

                    if(trim(species_name_file) /= trim(species_name)) then
                        write(&
                            *, &
                            '("Error: species name in file ''", A, "'' (''", A, "'')&
                            &does not corresponds to requested species (''", A, "'')")' &
                        ) sigma_file, species_name_file, species_name

                        stop
                    end if

                    if(pressure_file < pressure - tiny(0.) .or. pressure_file > pressure + tiny(0.)) then
                        write(&
                            *, &
                            '("Error: pressure in file ''", A, "'' (", ES10.4, ")&
                            &does not corresponds to requested pressure (", ES10.4, ")")' &
                        ) sigma_file, pressure_file, pressure

                        stop
                    end if

                    if(temperature_file < temperature - tiny(0.) .or. temperature_file > temperature + tiny(0.)) then
                        write(&
                            *, &
                            '("Error: temperature in file ''", A, "'' (", ES10.4, ") &
                            &does not corresponds to requested temperature (", ES10.4, ")")' &
                        ) sigma_file, pressure_file, pressure

                        stop
                    end if

                    if(wvn_min_subinterval < wavenumber_min) then
                        write(&
                            *, &
                            '("Error: minimal wavenumber in file ''", A, "'' (", ES20.14, ") &
                            &is below minimal requested wavenumber (", ES20.14, ")")' &
                        ) sigma_file, wvn_min_subinterval, wavenumber_min

                        stop
                    end if

                    if(wvn_max_subinterval > wavenumber_max + resolution) then
                        write(&
                            *, &
                            '("Error: maximal wavenumber in file ''", A, "'' (", ES20.14, ") &
                            &does not corresponds to requested wavenumber (", ES20.14, ")")' &
                        ) trim(sigma_file), wvn_max_subinterval, wavenumber_max

                        stop
                    end if
                end subroutine check_file

                subroutine read_1_axs_file()
                    ! """
                    ! """
                    implicit none

                    if(file_unit == -1) then  ! file is not open yet
                        open(&
                            newunit=file_unit, &
                            file=trim(sigma_file), &
                            form='formatted' &
                        )

                        write(&
                            *, &
                            '(1X, "Reading ", A, " absorption cross sections in file ''", A,"''")' &
                        ) &
                            trim(species_name), &
                            trim(sigma_file)
                    end if

                    do
                        read(&
                            file_unit, &
                            '(A32, ES20.14, ES20.14, I8, ES10.4, ES10.4)', &
                            iostat=io &
                        )   species_name_file, wvn_min_subinterval, wvn_max_subinterval, n_wavenumbers_subinterval, &
                            temperature_file, pressure_file

                        if(io < 0) then
                            exit
                        end if

                        call check_file()

                        i0 = n_wavenumbers
                        n_wavenumbers = n_wavenumbers + n_wavenumbers_subinterval
                        wavenumber_step = &
                            (wvn_max_subinterval - wvn_min_subinterval) / dble(n_wavenumbers_subinterval - 1)

                        if(.not. allocated(absorption_cross_sections)) then
                            allocate(absorption_cross_sections(n_wavenumbers))
                            allocate(wavenumbers(n_wavenumbers))
                        else
                            call reallocate_1Ddouble(absorption_cross_sections, n_wavenumbers)
                            call reallocate_1Ddouble(wavenumbers, n_wavenumbers)
                        end if

                        do i = 1, n_wavenumbers_subinterval
                            read(file_unit, '(ES10.4)' &
                                 ) absorption_cross_sections(i0 + i)
                            wavenumbers(i0 + i) = wvn_min_subinterval + wavenumber_step * dble(i - 1)
                        end do
                    end do

                    close(file_unit)
                end subroutine read_1_axs_file

                subroutine read_multiple_axs_files()
                    ! """
                    ! """
                    implicit none

                    integer :: &
                        n_files_read, &
                        n_sub_interval

                    n_files_read = 0
                    wvn_min = wavenumber_min
                    wvn_max = wvn_min

                    do while(wvn_max < wavenumber_max + resolution)
                        wvn_max = wvn_max + min_total_interval_size

                        if(wvn_max > wavenumber_max + resolution) then
                            wvn_max = wavenumber_max + resolution
                        end if

                        write(sigma_file, '(A, "_", ES9.3, "-", ES9.3, "cm-1_", ES9.3, "K_", ES9.3, "Pa")') &
                              trim(species_name), wvn_min, wvn_max, &
                              temperature, pressure
                        sigma_file = trim(path_absorption_cross_sections) // &
                            trim(absorption_cross_section_file_prefix) // '_' // trim(sigma_file) // '.dat'

                        inquire(file=trim(sigma_file), number=file_unit, exist=file_exists)

                        if(.not. file_exists) then
                            if(wvn_max >= (wavenumber_max + resolution) * (1 - 1d-12) .and. &
                               wvn_max <= (wavenumber_max + resolution) * (1 + 1d-12) .and. &
                               wvn_min >= (wavenumber_max + resolution) - 2 * min_total_interval_size) then
                                write(*, '("Error: file ''", A, "'' does not exists")') trim(sigma_file)
                                stop
                            else if (wvn_max >= (wavenumber_max + resolution) * (1 - 1d-12) .and. &
                                     wvn_max <= (wavenumber_max + resolution) * (1 + 1d-12)) then
                                wvn_min = wvn_min + min_total_interval_size
                                wvn_max = wvn_min

                                cycle
                            else
                                cycle
                            end if
                        else
                            wvn_min = wvn_max
                            n_files_read = n_files_read + 1

                            if (wvn_max >= wavenumber_max * (1 - 1d-12) .and. &
                                wvn_max <= (wavenumber_max + resolution) * (1 + 1d-12)) then
                                wvn_max = huge(0d0)  ! ensure loop exit
                            end if
                        end if

                        if(file_unit == -1) then  ! file is not open yet
                            open(&
                                newunit=file_unit, &
                                file=trim(sigma_file), &
                                form='formatted' &
                            )

                            write(&
                                *, &
                                '(1X, "Reading ", A, " absorption cross sections in file ''", A,"''")' &
                            ) &
                                trim(species_name), &
                                trim(sigma_file)
                        end if

                        n_sub_interval = 0

                        do
                            n_sub_interval = n_sub_interval + 1

                            read(&
                                file_unit, &
                                '(A32, ES20.14, ES20.14, I8, ES10.4, ES10.4)', &
                                iostat=io &
                            )   species_name_file, wvn_min_subinterval, wvn_max_subinterval, &
                                n_wavenumbers_subinterval, temperature_file, pressure_file

                            if(io < 0) then
                                exit
                            end if

                            call check_file()

                            i0 = n_wavenumbers

                            wavenumber_step = &
                                (wvn_max_subinterval - wvn_min_subinterval) / dble(n_wavenumbers_subinterval - 1)

                            if(n_files_read > 1 .and. n_sub_interval == 1) then
                                n_wavenumbers_subinterval = n_wavenumbers_subinterval - 1
                                wvn_min_subinterval = wvn_min_subinterval + wavenumber_step
                                read(file_unit, *)  ! pass first value
                            end if

                            n_wavenumbers = n_wavenumbers + n_wavenumbers_subinterval

                            if(.not. allocated(absorption_cross_sections)) then
                                allocate(absorption_cross_sections(n_wavenumbers))
                                allocate(wavenumbers(n_wavenumbers))
                            else
                                call reallocate_1Ddouble(absorption_cross_sections, n_wavenumbers)
                                call reallocate_1Ddouble(wavenumbers, n_wavenumbers)
                            end if

                            do i = 1, n_wavenumbers_subinterval
                                read(file_unit, '(ES10.4)' &
                                     ) absorption_cross_sections(i0 + i)
                                wavenumbers(i0 + i) = wvn_min_subinterval + wavenumber_step * dble(i - 1)
                            end do
                        end do

                        close(file_unit)
                    end do
                end subroutine read_multiple_axs_files
        end subroutine read_absorption_cross_sections_files


        subroutine read_cia_file(file_spec, wavenumbers, temperatures, cia)
            ! """
            ! """
            implicit none

            character(len=*), intent(in) :: file_spec
            doubleprecision, allocatable, intent(out) :: wavenumbers(:), temperatures(:), cia(:, :)

            integer :: i, j, file_unit, n_wavenumbers, n_temperatures

            open(newunit=file_unit, file=file_spec, status='old')
            read(file_unit, *) n_temperatures, n_wavenumbers

            allocate(&
                cia(n_temperatures, n_wavenumbers), &
                temperatures(n_temperatures), &
                wavenumbers(n_wavenumbers) &
            )

            do i = 1, n_temperatures
                read(file_unit, *) temperatures(i)
            end do

            do j = 1, n_wavenumbers
                read(file_unit, *) wavenumbers(j), (cia(i, j), i = 1, n_temperatures)
            end do

            close(file_unit)
        end subroutine read_cia_file


        subroutine read_data_file(file, columns, labels, units)
            ! """
            ! Read the data files used in the program.
            ! Data files can be any file with two columns -representing a function f(x)- and a header.
            ! :param file: the file to read
            ! :return columns: the columns of the file
            ! :return labels: the lables defining the columns
            ! :return units: units of the columns
            ! """
            implicit none

            character(len=*), intent(in) :: file
            character(len=*), dimension(:), allocatable, intent(out) :: labels, units
            doubleprecision, dimension(:, :), allocatable, intent(out) :: columns

            character(len=2), parameter :: &
                py_comment_starter = '# '  ! python comment starter (used by default in numpy loadtxt)

            character(len=4*file_name_size) :: &
                str_tmp

            integer :: &
                file_unit, &
                i, &
                index_comment, &
                io, &
                l, &
                n_cols, &
                size_array

            open(newunit=file_unit, file=file, status='old')

            read(file_unit, '(A)') str_tmp
            ! Check for numpy.savetxt generated files (header delimited with #)
            i = index(str_tmp, py_comment_starter)

            if (i > 0) then
                i = i + len(py_comment_starter)
                str_tmp = str_tmp(i:)
            end if

            ! Check for optional one-value keys (e.g. effective_temperature = 3500)
            index_comment = index(str_tmp, ' !')

            if (index_comment == 0) then
                index_comment = len(trim(str_tmp))
            end if

            ! Count the number of whitespaces, the number of columns is this number + 1
            n_cols = count_substring(trim(str_tmp(:index_comment)), ' ')
            n_cols = n_cols + 1

            ! Allocations
            allocate(&
                labels(n_cols), &
                units(n_cols) &
            )

            ! Read labels
            read(str_tmp, *) labels

            ! Read units
            read(file_unit, '(A)') str_tmp

            if (i > 0) then
                str_tmp = str_tmp(i:)
            end if

            read(str_tmp, *) units

            ! Read data
            allocate(columns(n_cols, 1))
            size_array = 1
            l = 0

            do
                l = l + 1

                if(l > size_array) then
                    call reallocate_2Ddouble(columns, n_cols, size_array * 2)
                    size_array = size(columns(1, :))
                end if

                read(file_unit, *, iostat=io) columns(:, l)

                if (io < 0) then
                    l = l - 1

                    exit
                end if
            end do

            call reallocate_2Ddouble(columns, n_cols, l)

            close(file_unit)
        end subroutine read_data_file


        subroutine read_input_parameters()
            ! """
            ! Read the input file.
            ! """
            use k_correlation, only: gtype, num_vec, resolution
            use spectrometrics, only: wavenumber_max, wavenumber_min, wavenumber_step, doppler_deviation_tolerance, &
                min_total_interval_size
            use thermodynamics, only: n_levels, size_thermospace, pressure_space, temperature_space, &
                allocate_thermodynamics_attributes
            use species, only: n_species, species_names, species_vmr, allocate_species_primary_attributes

            implicit none

            integer :: file_unit

            namelist/output_files/&
                absorption_cross_section_file_prefix
            namelist/spectral_space/&
                wavenumber_min, &
                wavenumber_max, &
                wavenumber_step, &
                doppler_deviation_tolerance, &
                min_total_interval_size
            namelist/k_distribution/&
                gtype, &
                num_vec, &
                resolution
            namelist/atmosphere_parameters/&
                n_levels, &
                size_thermospace, &
                n_species
            namelist/thermodynamic_space/&
                pressure_space, &
                temperature_space
            namelist/species_data/&
                species_names, &
                species_vmr
            namelist/options/&
                output_species_by_species, &
                use_profile_mode
            namelist/paths/&
                path_absorption_cross_sections, &
                path_data, &
                path_k_coefficients, &
                path_lines, &
                path_species_data, &
                path_outputs

            open(newunit=file_unit, file=input_file)
            read(file_unit, nml=output_files)
            read(file_unit, nml=spectral_space)
            read(file_unit, nml=k_distribution)

            read(file_unit, nml=atmosphere_parameters)

            read(file_unit, nml=options)

            call allocate_species_primary_attributes()

            if(use_profile_mode) then
                size_thermospace = n_levels  ! pressures and temperatures are read as p(T)
            end if

            call allocate_thermodynamics_attributes()

            if(use_profile_mode) then
                size_thermospace = 1  ! one temperature profile
            end if

            read(file_unit, nml=thermodynamic_space)
            read(file_unit, nml=species_data)

            read(file_unit, nml=paths)

            close(file_unit)
        end subroutine read_input_parameters


        subroutine read_lines_files(&
            lines_file, wavenumber_min, wavenumber_max, &
            intensity_min, temperature_intensity_min, rotational_partition_exponent, &
            n_broadenings, broadenings_borders, &
            lines_wavenumber, lines_intensity, lines_e_low, i_broadening_lines, n_lines&
        )
            ! """
            ! Read a lines file in GEISA 2015 format (http://cds-espri.ipsl.fr/cgi-bin/geisa/geisa2015_format)
            ! and return its useful data.
            ! :param lines_file: lines file to read
            ! :param wavenumber_min: (cm-1) minimum line wavenumber to keep
            ! :param wavenumber_max: (cm-1) maximum line wavenumber to keep
            ! :param intensity_min: (cm-1/(molecules.cm-2)) minimum line intensity to keep
            ! :param temperature_intensity_min: (K) reference temperature of the minimum intensity
            ! :param rotational_partition_exponent: rotational partition exponent of the species
            ! :param n_broadenings: number of braodening coefficients associated to the species
            ! :param broadening_borders: (cm-1.atm-1) borders of the broadening coefficients blocks
            ! :return lines_wavenumber: (cm-1) wavenumbers of the retained lines
            ! :return lines_intensity: (cm-1/(molecules.cm-2)) intensities of the retained lines
            ! :return lines_e_low: (cm-1) energy of the lower transition level of the retained lines
            ! :return i_broadening_lines: index of the broadening block of the retained lines
            ! :return n_lines: number of retained lines
            ! """
            use physics, only: cst_t_ref, cst_c, cst_h, cst_k

            implicit none

            character(len=*), intent(in) :: lines_file
            integer, intent(in) :: n_broadenings
            doubleprecision, intent(in) :: wavenumber_min, wavenumber_max, &
                intensity_min, temperature_intensity_min, rotational_partition_exponent
            doubleprecision, dimension(:), intent(in) :: broadenings_borders

            integer, intent(out) :: n_lines
            integer, dimension(:), allocatable, intent(out) :: i_broadening_lines
            doubleprecision, dimension(:), allocatable, intent(out) ::  lines_wavenumber, lines_intensity, lines_e_low

            integer :: &
                file_unit, &
                i, &                        ! index
                io, &                       ! store the io status of the file
                l, &                        ! index
                size_array                  ! size of the arrays

            doubleprecision :: &
                broadening, &               ! (cm-1.atm-1) broadening pressure HWHM at 296 K
                e_low, &                    ! (cm-1) energy of the lower transition level
                intensity, &                ! (cm-1/(molecule.cm-2) intensity of the line at 296K
                intensity_min_test_ref, &   ! (cm-1/(molecule.cm-2) minimum intensity of the line at the given ref. T
                intensity_test_ref, &       ! (cm-1/(molecule.cm-2) intensity of the line at the given reference T
                hckt_test_ref, &            ! hc/k * (1/T - 1/T_ref)
                wvn_line                    ! (cm-1) wavenumber of the line

            intensity_min_test_ref = intensity_min / &
                (cst_t_ref / temperature_intensity_min) ** rotational_partition_exponent
            hckt_test_ref = cst_h * cst_c * 1d2 / cst_k * (1d0 / temperature_intensity_min - 1d0 / cst_t_ref)

            allocate(&
                i_broadening_lines(1), &
                lines_wavenumber(1), &
                lines_intensity(1), &
                lines_e_low(1) &
            )

            i_broadening_lines(1) = 0
            lines_wavenumber(1) = 0d0
            lines_intensity(1) = 0d0
            lines_e_low(1) = 0d0

            size_array = 1

            open(newunit=file_unit, file=lines_file, status='old')

            l = 0

            do
                read(file_unit, '(F12.6, 1PD11.4, 0PF6.4, F10.4)', iostat=io) wvn_line, intensity, broadening, e_low

                if(wvn_line > wavenumber_max .or. io < 0) then
                    exit
                end if

                if(wvn_line < wavenumber_min .or. io < 0) then
                    cycle
                end if

                intensity_test_ref = intensity * exp(-hckt_test_ref * e_low)

                if(intensity_test_ref < intensity_min_test_ref .and. intensity_test_ref < intensity_min) then
                    cycle
                end if

                l = l + 1

                if(l > size_array) then
                    size_array = size(lines_wavenumber)
                    call reallocate_1Ddouble(lines_wavenumber, size_array * 2)
                    call reallocate_1Ddouble(lines_intensity, size_array * 2)
                    call reallocate_1Ddouble(lines_e_low, size_array * 2)
                    call reallocate_1Dinteger(i_broadening_lines, size_array * 2)
                end if

                lines_wavenumber(l) = wvn_line
                lines_intensity(l) = intensity
                lines_e_low(l) = e_low

                if(n_broadenings == 1) then
                    i_broadening_lines(l) = 1
                else
                    do i = 1, n_broadenings
                        if(broadening <= broadenings_borders(i)) then
                            i_broadening_lines(l) = i

                            exit
                        elseif(i == n_broadenings) then
                            i_broadening_lines(l) = n_broadenings
                        end if
                    end do
                end if
            end do

            close(file_unit)

            if(l > 0) then
                n_lines = l
            else
                write(*, '("Warning: no lines found between ", F0.3, " and ", F0.3, " cm-1 in file ''", A, "''")') &
                    wavenumber_min, wavenumber_max, trim(lines_file)

                n_lines = 1
            end if

            call reallocate_1Ddouble(lines_wavenumber, n_lines)
            call reallocate_1Ddouble(lines_intensity, n_lines)
            call reallocate_1Ddouble(lines_e_low, n_lines)
            call reallocate_1Dinteger(i_broadening_lines, n_lines)
        end subroutine read_lines_files


        subroutine read_lines_files_ch4(&
            lines_file, superlines_file, wavenumber_min, wavenumber_max, &
            n_broadenings, broadenings_borders, &
            lines_wavenumber, lines_intensity, lines_e_low, i_broadening_lines, i_broadening_superlines, &
            n_lines, &
            wavenumbers_superlines, intensities_superlines, n_superlines)
            ! """
            ! Read a lines file in Rey et al. 2017 "hybrid" format (http://theorets.tsu.ru/molecules.ch4.hybrid).
            ! and return its useful data.
            ! :param lines_file: lines file to read
            ! :param superlines_file: superlines file to read
            ! :param wavenumber_min: (cm-1) minimum line wavenumber to keep
            ! :param wavenumber_max: (cm-1) maximum line wavenumber to keep
            ! :param n_broadenings: number of braodening coefficients associated to the species
            ! :param broadening_borders: (cm-1.atm-1) borders of the broadening coefficients blocks
            ! :return lines_wavenumber: (cm-1) wavenumbers of the retained lines
            ! :return lines_intensity: (cm-1/(molecules.cm-2)) intensities of the retained lines
            ! :return lines_e_low: (cm-1) energy of the lower transition level of the retained lines
            ! :return i_broadening_lines: index of the broadening block of the retained lines
            ! :return n_lines: number of retained lines
            ! :return wavenumbers_superlines: (cm-1) wavenumbers of the retained superlines
            ! :return intensities_superlines: (cm-1/(molecules.cm-2)) intensities of the retained superlines
            ! :return n_superlines: number of retained superlines
            ! """
            implicit none

            character(len=*), intent(in) :: lines_file, superlines_file
            integer, intent(in) :: n_broadenings
            doubleprecision, intent(in) :: wavenumber_min, wavenumber_max
            doubleprecision, dimension(:), intent(in) :: broadenings_borders

            integer, intent(out) :: n_lines, n_superlines
            integer, dimension(:), allocatable, intent(out) :: i_broadening_lines, i_broadening_superlines
            doubleprecision, dimension(:), allocatable, intent(out) :: lines_wavenumber, lines_intensity, lines_e_low, &
                wavenumbers_superlines, intensities_superlines

            doubleprecision, parameter :: &
                broadening = 0.065d0  ! (cm-1.atm-1) approximatly the average broadening of the CH4 lines

            integer :: &
                file_unit, &
                i, &                    ! index
                io, &                   ! store the io status of the file
                l, &                    ! index
                size_array              ! size of the arrays

            doubleprecision :: &
                e_low, &                ! (cm-1) energy of the lower transition level of the line
                intensity, &            ! (cm-1/(molecules.cm-2)) intensity of the line
                wavenumber              ! (cm-1) wavenumber of the line

            allocate(&
                lines_wavenumber(1), &
                lines_intensity(1), &
                lines_e_low(1), &
                i_broadening_lines(1), &
                i_broadening_superlines(1), &
                wavenumbers_superlines(1), &
                intensities_superlines(1) &
            )

            i_broadening_lines(1) = 0
            i_broadening_superlines(1) = 0
            lines_wavenumber(1) = 0d0
            lines_intensity(1) = 0d0
            lines_e_low(1) = 0d0
            wavenumbers_superlines(1) = 0d0
            intensities_superlines(1) = 0d0

            size_array = 1

            ! CH4 lines (Rey et al.2017)
            open(newunit=file_unit, file=lines_file, status='old')

            l = 0

            do
                read(file_unit, *, iostat=io) wavenumber, intensity, e_low

                if(wavenumber > wavenumber_max .or. io < 0) then
                    exit
                end if

                if(wavenumber < wavenumber_min) then
                    cycle
                end if

                l = l + 1

                if(l > size_array) then
                    size_array = size(lines_wavenumber)
                    call reallocate_1Ddouble(lines_wavenumber, size_array*2)
                    call reallocate_1Ddouble(lines_intensity, size_array*2)
                    call reallocate_1Ddouble(lines_e_low, size_array*2)
                    call reallocate_1Dinteger(i_broadening_lines, size_array*2)
                end if

                lines_wavenumber(l) = wavenumber
                lines_intensity(l) = intensity
                lines_e_low(l) = e_low

                if(n_broadenings == 1) then
                    i_broadening_lines(l) = 1
                else
                    do i = 1, n_broadenings
                        if(broadening <= broadenings_borders(i)) then
                            i_broadening_lines(l) = i

                            exit
                        elseif(i == n_broadenings) then
                            i_broadening_lines(l) = n_broadenings
                        end if
                    end do
                end if
            end do

            close(file_unit)

            if(l > 0) then
                n_lines = l
            else
                write(*, '("Warning: no lines found between ", F0.3, " and ", F0.3, " cm-1 in file ''", A, "''")') &
                    wavenumber_min, wavenumber_max, trim(lines_file)

                n_lines = 1
            end if

            call reallocate_1Ddouble(lines_wavenumber, n_lines)
            call reallocate_1Ddouble(lines_intensity, n_lines)
            call reallocate_1Ddouble(lines_e_low, n_lines)
            call reallocate_1Dinteger(i_broadening_lines, n_lines)

            ! CH4 Super lines (Rey et al.2017)
            size_array = 1

            l = 0

            open(newunit=file_unit, file=superlines_file, status='old')

            do
                read(file_unit, *, iostat=io) wavenumber, intensity

                if(wavenumber > wavenumber_max .or. io < 0) then
                    exit
                end if

                if(wavenumber < wavenumber_min) then
                    cycle
                end if

                l = l + 1

                if(l > size_array) then
                    size_array = size(wavenumbers_superlines)
                    call reallocate_1Ddouble(wavenumbers_superlines, size_array*2)
                    call reallocate_1Ddouble(intensities_superlines, size_array*2)
                    call reallocate_1Dinteger(i_broadening_superlines, size_array*2)
                end if

                wavenumbers_superlines(l) = wavenumber
                intensities_superlines(l) = intensity

                if(n_broadenings == 1) then
                    i_broadening_superlines(l) = 1

                    cycle
                else
                    do i = 1, n_broadenings
                        if(broadening <= broadenings_borders(i)) then
                            i_broadening_superlines(l) = i

                            exit
                        elseif(i == n_broadenings) then
                            i_broadening_lines(l) = n_broadenings
                        end if

                        cycle
                    end do
                end if
            end do

            close(file_unit)

            if(l > 0) then
                n_superlines = l
            else
                write(*, '("Warning: no lines found between ", F0.3, " and ", F0.3, " cm-1 in file ''", A, "''")') &
                    wavenumber_min, wavenumber_max, trim(superlines_file)

                n_superlines = 1
            end if

            call reallocate_1Ddouble(wavenumbers_superlines, n_superlines)
            call reallocate_1Ddouble(intensities_superlines, n_superlines)
            call reallocate_1Dinteger(i_broadening_superlines, n_superlines)
        end subroutine read_lines_files_ch4


        subroutine read_species_data(&
            file, &
            name, molar_mass, line_shape, rotational_partition_exponent, &
            n_vibrational_modes, n_electronic_states, n_broadenings, cutoff, intensity_min, temperature_intensity_min&
        )
            ! """
            ! Read a species parameter file.
            ! This file is a name list that contains the species needed physical properties.
            ! :param file: file to read
            ! :return name: name of the species
            ! :return molar_mass: (kg.mol-1) molar mass of the species
            ! :return line_shape: shape of the lines of the species
            ! :return rotational_partition_exp: rotational partition exponent of the species
            ! :return cutoff: (cm-1) spectral distance from the center of the lines within the voigt profile is calc.
            ! :return n_vibrational_band: number of vibrational bands
            ! :return vibrational_bands_wavenumber: (cm-1) vibrational bands wavenumbers
            ! :return vibrational_bands_degeneracy: vibrational bands degenaracies
            ! """
            implicit none

            character(len=*), intent(in) :: file

            character(len=*), intent(out) :: name, line_shape
            integer, intent(out) :: n_broadenings, n_electronic_states, n_vibrational_modes
            doubleprecision, intent(out) :: molar_mass, rotational_partition_exponent, cutoff, &
                intensity_min, temperature_intensity_min

            integer :: file_unit

            namelist/species_data/&
                name, &
                molar_mass, &
                rotational_partition_exponent, &
                n_vibrational_modes, &
                n_electronic_states, &
                n_broadenings, &
                line_shape, &
                cutoff, &
                intensity_min, &
                temperature_intensity_min

            open(newunit=file_unit, file=file)
            read(file_unit, nml=species_data)
            close(file_unit)
        end subroutine read_species_data


        subroutine read_species_broadening_blocks(&
            file, n_broadenings, &
            broadenings, broadening_temperature_coefficients &
        )
            ! """
            ! Read the broadening blocks of a species.
            ! In order to speed up the calculations, CAXS attributes to each species a set of pre-determined lines
            ! broadenings values. This way, only a few Voigt profiles are calculated, instead of one for each lines.
            ! :param file: file to read
            ! :param n_broadenings: number of broadening blocks to read
            ! :return broadenings: (cm-1.atm-1) boradenings of the species
            ! :return broadening_temperature_coefficients: temerature dependance coefficients of the broadenings
            ! """
            implicit none

            character(len=*), intent(in) :: file
            integer, intent(in) :: n_broadenings

            doubleprecision, dimension(n_broadenings), intent(out) :: broadenings, &
                broadening_temperature_coefficients

            integer :: file_unit

            namelist/broadenings_blocks/&
                broadenings, &
                broadening_temperature_coefficients

            open(newunit=file_unit, file=file)
            read(file_unit, nml=broadenings_blocks)
            close(file_unit)
        end subroutine read_species_broadening_blocks


        subroutine read_species_electronic_states(&
            file, n_electronic_states, &
            electronic_states_wavenumber, electronic_states_degeneracy &
        )
            ! """
            ! Read the electronic states of a species.
            ! :param file: file to read
            ! :param n_electronic_states: number of electronic states to read
            ! :return electronic_states_wavenumber: (cm-1) wavenumbers of the electronic states
            ! :return electronic_states_degeneracy: degeneracies of the electronic states
            ! """
            implicit none

            character(len=*), intent(in) :: file
            integer, intent(in) :: n_electronic_states

            integer, dimension(n_electronic_states), intent(out) :: electronic_states_degeneracy
            doubleprecision, dimension(n_electronic_states), intent(out) :: electronic_states_wavenumber

            integer :: file_unit

            namelist/electronic_states/&
                electronic_states_wavenumber, &
                electronic_states_degeneracy

            open(newunit=file_unit, file=file)
            read(file_unit, nml=electronic_states)
            close(file_unit)
        end subroutine read_species_electronic_states


        subroutine read_species_vibrational_modes(&
            file, n_vibrational_modes, &
            vibrational_modes_wavenumber, vibrational_modes_degeneracy &
        )
            ! """
            ! Read the vibrational modes of a species.
            ! :param file: file to read
            ! :param n_vibrational_modes: number of vibrational modes to read
            ! :return vibrational_modes_wavenumber: (cm-1) wavenumbers of the vibrational modes
            ! :return vibrational_modes_degeneracy: degeneracies of the vibrational modes
            ! """
            implicit none

            character(len=*), intent(in) :: file
            integer, intent(in) :: n_vibrational_modes

            integer, dimension(n_vibrational_modes), intent(out) :: vibrational_modes_degeneracy
            doubleprecision, dimension(n_vibrational_modes), intent(out) :: vibrational_modes_wavenumber

            integer :: file_unit

            namelist/vibrational_modes/&
                vibrational_modes_wavenumber, &
                vibrational_modes_degeneracy

            open(newunit=file_unit, file=file)
            read(file_unit, nml=vibrational_modes)
            close(file_unit)
        end subroutine read_species_vibrational_modes


        subroutine read_thermochemical_table(&
            file, temperature, isobaric_molar_heat_capacity, gibbs_free_energy_formation &
        )
            ! """
            ! Read a thermochemical table.
            ! :param file: file to read
            ! :return temperature: (K) temperature grid of the table
            ! :return isobaric_molar_heat_capacity: (J.K-1.mol-1) isobaric molar heat capacity of the species
            ! :return gibbs_free_energy_formation: (kJ.mol-1) Gibb's free energy of formation of the species
            ! """
            use physics, only: cst_R

            implicit none

            character(len=*), intent(in) :: file

            doubleprecision, dimension(:), allocatable, intent(out) :: temperature, isobaric_molar_heat_capacity, &
                gibbs_free_energy_formation

            integer, parameter :: n_parameters = 3
            doubleprecision, parameter :: default_isobaric_molar_heat_capacity = 5d0/2d0 * cst_R
            character(len=file_name_size), dimension(n_parameters), parameter :: &
                expected_labels = [&
                    'temperature                 ', &
                    'isobaric_molar_heat_capacity', &
                    'gibbs_free_energy_formation ' &
                ], &
                expected_units = [&
                    'K          ', &
                    'J.K-1.mol-1', &
                    'kJ.mol-1   ' &
                ]
            character(len=file_name_size), dimension(:), allocatable :: labels, units
            integer :: i, j, n_cols, n_temperatures
            integer, dimension(:), allocatable :: id_cols
            doubleprecision, dimension(:, :), allocatable :: columns

            write(*, '(1X, "Reading thermochemical table in file ''", A, "''")') &
                trim(file)

            call read_data_file(file, columns, labels, units)

            n_cols = size(columns(:, 1))
            n_temperatures = size(columns(1, :))

            allocate(&
                id_cols(n_cols), &
                temperature(n_temperatures), &
                isobaric_molar_heat_capacity(n_temperatures), &
                gibbs_free_energy_formation(n_temperatures) &
            )

            id_cols(:) = 0
            temperature(:) = 0d0
            isobaric_molar_heat_capacity(:) = 0d0
            gibbs_free_energy_formation(:) = 0d0

            ! Check units
            do i = 1, n_cols
                do j = 1, n_parameters
                    if (trim(labels(i)) == trim(expected_labels(j))) then
                        if (trim(units(i)) /= trim(expected_units(j))) then
                            write(*, '("Error: ", A, " must be in ", A, ", but are in ", A)') &
                                trim(labels(i)), trim(expected_units(j)), trim(units(i))
                            stop
                        end if
    
                        id_cols(j) = i
                    end if
                end do
            end do

            ! Check labels
            do j = 1, n_parameters
                if (id_cols(j) == 0) then
                    if (trim(expected_labels(j)) /= 'isobaric_molar_heat_capacity') then
                        write(*, '("Error: label ''", A, "'' not found")') trim(expected_labels(j))
                        stop
                    else
                        write(*, '("Warning: label ''", A, "'' not found, &
                            &assuming a molar heat capacity of ", ES11.4)') &
                            trim(expected_labels(j)), default_isobaric_molar_heat_capacity

                        isobaric_molar_heat_capacity(:) = default_isobaric_molar_heat_capacity
                    end if
                else
                    if(trim(expected_labels(j)) == 'temperature') then
                        temperature(:) = columns(id_cols(j), :)
                    else if(trim(expected_labels(j)) == 'isobaric_molar_heat_capacity') then
                        isobaric_molar_heat_capacity(:) = columns(id_cols(j), :)
                    else if(trim(expected_labels(j)) == 'gibbs_free_energy_formation') then
                        gibbs_free_energy_formation(:) = columns(id_cols(j), :)
                    end if
                end if
            end do
        end subroutine read_thermochemical_table


        subroutine read_voigt_file(file, voigt_data)
            ! """
            ! Read the file containing the data used to calculate the Voigt profile.
            ! :param file: file to read
            ! :return voigt_data: data used to calculate the Voigt profile.
            ! """
            implicit none

            character(len=*), intent(in) :: file
            doubleprecision , intent(out):: voigt_data(400, 100)

            integer :: file_unit

            open(newunit=file_unit, file=file, status='old', form='unformatted')  ! improve file input
            read(file_unit) voigt_data(:, :)
            close(file_unit)
        end subroutine read_voigt_file

        subroutine write_absorption_cross_sections_files(&
            species_name, wvn_min_subinterval, wvn_max_subinterval, n_wavenumbers, &
            wavenumber_min, wavenumber_max, temperature, pressure, absorption_cross_sections &
        )
            ! """
            ! Write the calculated absorption cross sections of a species.
            ! :param species_name: name of the species
            ! :param wvn_min_subinterval: (cm-1) minimum wavenumber of the current spectral sub-interval
            ! :param wvn_max_subinterval: (cm-1) maximum wavenumber of the current spectral sub-interval
            ! :param n_wavenumbers: number of wavenumbers in the current spectral sub-interval
            ! :param wavenumber_min: (cm-1) minimum wavenumber of the spectral interval
            ! :param wavenumber_max: (cm-1) maximum wavenumber of the spectral interval
            ! :param temperature: (K) temperature of calculation
            ! :param pressure: (Pa) pressure of calculation
            ! :param absorption_cross_sections: (cm2) absorption cross sections calculated at the given T and p
            ! """
            use species, only: species_name_size

            implicit none

            character(len=species_name_size), intent(in) :: species_name
            integer, intent(in) :: n_wavenumbers
            doubleprecision, intent(in) :: wvn_min_subinterval, wvn_max_subinterval, wavenumber_min, wavenumber_max, &
                temperature, pressure, absorption_cross_sections(n_wavenumbers)

            character(len=file_name_size) :: &
                sigma_file  ! file where to write the absorption cross sections

            integer :: &
                i, &        ! index
                file_unit   ! unit of the opened file

            write(sigma_file, '(A, "_", ES9.3, "-", ES9.3, "cm-1_", ES9.3, "K_", ES9.3, "Pa")') &
                  trim(species_name), wavenumber_min, wavenumber_max, &  ! TODO [low] improve species name writting ?
                  temperature, pressure
            sigma_file = trim(path_absorption_cross_sections) // trim(absorption_cross_section_file_prefix) // &
                '_' // trim(sigma_file) // '.dat'

            inquire(file=trim(sigma_file), number=file_unit)

            if(file_unit == -1) then  ! file is not open yet
                open(&
                    newunit=file_unit, &
                    file=trim(sigma_file), &
                    form='formatted' &
                )
            end if

            write(&
                file_unit, &
                '(A32, ES20.14, ES20.14, I8, ES10.4, ES10.4)' &
            )   species_name, wvn_min_subinterval, wvn_max_subinterval, n_wavenumbers, temperature, pressure

            do i = 1, n_wavenumbers
                write(file_unit, '(ES10.4)' &
                     ) absorption_cross_sections(i)
            end do

            if(wvn_max_subinterval >= wavenumber_max - tiny(0.) .and. &
                wvn_max_subinterval <= wavenumber_max + tiny(0.)) then
                close(file_unit)
            end if
        end subroutine write_absorption_cross_sections_files
end module interface
