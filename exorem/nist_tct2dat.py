"""
Convert NIST-JANAF thermochemical tables into Exo-REM tct.dat files.

Run this script as:
python nist_tct2dat.py path/to/thermochemical/tables/file
for example, to convert all files in the gases directory,
python nist_tct2dat.py ./data/thermochemical_tables/gases/*
"""
import argparse
import glob
from warnings import warn

import numpy as np

# Arguments definition
parser = argparse.ArgumentParser(
    description='Convert NIST-JANAF thermochemical tables in a folder into Exo-REM tct.dat files.'
)

parser.add_argument(
    'directory',
    default='./data/thermochemical_tables/gases/',
    help='suffix of the files'
)


def nist_tct2dat(file):
    """
    Convert NIST-JANAF thermochemical tables into Exo-REM tct.dat files.
    Source: NIST-JANAF: https://www.nist.gov/system/files/documents/srd/JPCRD302001475p.pdf
    :param file: the file to be converted
    """

    with open(file, 'r') as f:
        species_info = f.readline()

        if 'temperature' in species_info:
            warn(f"file '{file}' is already converted")
            return

        header = f.readline()
        header_keys = header.split('\t')

        data = np.zeros((1, len(header_keys)))

        i = -1

        for line in f:
            line = line.strip()
            cols = list(line.split('\t'))

            for j, col in enumerate(cols):
                try:
                    cols[j] = float(col)
                except ValueError:
                    if cols[j] == 'INFINITE':
                        cols[j] = np.inf
                    elif 'FUGACITY' in cols[j] or '<-->' in cols[j] or 'LAMBDA' in cols[j] or cols[j] == '0. 0. 0.':
                        cols[j] = 0.0
                        cols = cols + [0.0, 0.0]
                    elif 'TRANSITION' in cols[j]:
                        cols[0] += 1e-3
                        cols[j] = 0.0
                        cols = cols + [0.0, 0.0]

            i += 1

            if i > 0:
                data = np.vstack((data, np.zeros(len(header_keys))))

            data[i, :] = cols[:]

    header_keys = header.split('\t')
    units = [''] * len(header_keys)

    for i, key in enumerate(header_keys):
        key = key.replace('\n', '')

        if key == 'T(K)':
            header_keys[i] = 'temperature'
            units[i] = 'K'
        elif key == 'Cp':
            header_keys[i] = 'isobaric_molar_heat_capacity'
            units[i] = 'J.K-1.mol-1'
        elif key == 'S':
            header_keys[i] = 'standard_molar_entropy'
            units[i] = 'J.K-1.mol-1'
        elif key == '-[G-H(Tr)]/T':
            header_keys[i] = 'gibbs_energy_function'
            units[i] = 'J.K-1.mol-1'
        elif key == 'H-H(Tr)':
            header_keys[i] = 'enthalpy_difference'
            units[i] = 'kJ.mol-1'
        elif key == 'delta-f H':
            header_keys[i] = 'enthalpy_formation'
            units[i] = 'kJ.mol-1'
        elif key == 'delta-f G':
            header_keys[i] = 'gibbs_free_energy_formation'
            units[i] = 'kJ.mol-1'
        elif key == 'log Kf':
            header_keys[i] = 'equilibrium_constant_formation'
            units[i] = 'None'
        else:
            raise KeyError(f"column label '{key}' is not recognized")

    data_dict = {}

    for i, key in enumerate(header_keys):
        data_dict[key] = data[:, i]

    data_dict['units'] = units

    with open(file, 'w') as f:
        header = ' '.join(header_keys)
        header += f' ! {species_info}, p_ref = 0.1 MPa, t_ref = 298.15 K, source: https://janaf.nist.gov/'
        header = header.replace('\n', '').replace('\t', ' ')
        units = ' '.join(units)

        f.write(header + '\n')
        f.write(units)

        data_shape = np.shape(data)

        for i in range(data_shape[0]):
            f.write('\n')

            for j in range(data_shape[1]):
                f.write(str(data[i, j]) + ' ')

        f.write('\n')

    return data_dict


def nist_tct2dat_all(directory):
    files = glob.glob(directory)

    for file in files:
        print(file)
        nist_tct2dat(file)


if __name__ == '__main__':
    args = parser.parse_args()

    nist_tct2dat_all(args.directory)
