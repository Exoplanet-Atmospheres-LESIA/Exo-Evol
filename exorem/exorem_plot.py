"""
Plot the most useful Exo-REM figures.

Run this script as:
python exorem_plot.py file_suffix --path_inputs <path/inputs/> --path_outputs <path/outputs/> --format <format>
for example, to read HDF5 outputs (default) and generate png figures instead of pdf
python exorem_plot.py ./outputs/exorem/example_R50.h5 --format png

To read .dat files, run for example:
python exorem_plot.py example_R50 --input_extension dat
"""
import argparse

from src.python.plot_figures import plot_all
from src.python.dat.plot_figures_dat import plot_all_dat

# Arguments definition
parser = argparse.ArgumentParser(
    description='Plot the most useful Exo-REM figures.'
)

parser.add_argument(
    'file_suffix',
    help='suffix of the files'
)

parser.add_argument(
    '--path_inputs',
    default='outputs/exorem/',
    help='figures directory'
)

parser.add_argument(
    '--path_outputs',
    default='outputs/figures/',
    help='figures directory'
)

parser.add_argument(
    '--format',
    default='pdf',
    help='format of the figures'
)

parser.add_argument(
    '--inputs_extension',
    default='h5',
    help='extension of the input data files'
)


if __name__ == '__main__':
    args = parser.parse_args()

    if args.inputs_extension == 'h5' or args.inputs_extension == 'hdf5':
        path_inputs, file = args.file_suffix.rsplit('/', 1)

        plot_all(
            file_suffix=file,
            path_outputs=args.path_outputs,
            path_inputs=path_inputs + '/',
            image_format=args.format
        )
    else:
        plot_all_dat(
            file_suffix=args.file_suffix + '.' + args.inputs_extension,
            path_outputs=args.path_outputs,
            path_inputs=args.path_inputs,
            image_format=args.format
        )
