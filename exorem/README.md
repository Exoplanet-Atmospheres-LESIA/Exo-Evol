# Exo-REM
## Synopsis
Exo-REM is a 1D radiative-equilibrium model first developed for the simulation of young gas giants far from their star and brown dwarfs. Fluxes are calculated using the two-stream approximation assuming hemispheric closure. The radiative-convective equilibrium is solved assuming that the net flux (radiative + convective) is conservative. The conservation of flux over the pressure grid is solved iteratively using a constrained linear inversion method. Rayleigh scattering as well as absorption and scattering by clouds (calculated from extinction coefficient, single scattering albedo, and asymmetry factor interpolated from precomputed tables for a set of wavelengths and particle radii) are also taken into account.

Exo-REM contains also four side-programs used to generate the k-coefficients files:
- CAXS (Calculate Absorption Cross Sections), capable of calculating the absorption cross 
sections of any species over a wide range of wavenumbers. Individual species can be combined together (for example 
using isotopic ratios). The cross sections can be calculated either using a pressure-temperature profile or a grid of
pressures and temperatures.
- CKC (Calculate K Coefficients), able to calculate the k-coefficients of a species using the CAXS outputs.
- txt2h5, to convert CKC ASCII k-tables into HDF5-formatted k-tables. Using [Exo_k](http://perso.astrophy.u-bordeaux.fr/~jleconte/exo_k-doc/index.html#) instead is recommended.
- optprogen, to generate clouds optical constants (code developed by F. Montmessin and J.-B. Madeleine at LATMOS and LMD)

N.B. For CAXS, the methane (CH4) species, lines data from 
[Rey et al. 2017](http://theorets.tsu.ru/molecules.ch4.hybrid) can be used.

## Motivation
Exo-REM is meant to be easy to use and parametrize, and its outputs easily transportable. 
It is written in Fortran, and should be easy to understand, maintain and modify.

## Installation
### Linux
1. Install the HDF5 libraries:
    ```bash
    sudo apt-get install libhdf5-dev
    ```
	If you intend to use *CAXS*, install the latest version of [OpenMPI](https://www.open-mpi.org/) as well
(support of `mpif08` is mandatory).
2. Download the tar.gz archive in the _dist_ directory.
3. Extract its content anywhere you want executing:
    ```bash
    tar xzvf exorem.tar.gz
    ```
4. `cd` yourself into the extracted _dist/exorem_ directory, then compile the code executing:
    ```bash
    make exorem
    ```
5. Download the R50 k-tables (_R50.tar.xz_) available [here](https://lesia.obspm.fr/exorem/ktables/default/xz/).
6. Extract its content in the _data/k_coefficients_tables/_ directory by executing:
    ```bash
    tar xJvf R50.tar.xz
    ```
7. Test the installation executing:
    ```bash
	cd bin
    ./exorem.exe --version
    ```
	Then:
	```bash
    ./exorem.exe ../inputs/example.nml
    ```
 
Notes: 
- you can compile all the programs coming with *Exo-REM* executing:
	```bash
	make
	```
- you can compile the debug version (slower, but more informative on error) of *Exo-REM* executing:
	```bash
	make exorem_debug
	```
	or, for all the programs:
	```bash
	make debug
	```

### Windows 10
1. Install your favourite Linux distribution on your computer following [these instructions](
https://docs.microsoft.com/en-gb/windows/wsl/install-win10).
2. Launch the Linux application.
3. Execute inside the Linux terminal:
    ```bash
    apt-get update -y
    apt-get upgrade -y
    apt-get install -y build-essential
    ```
3. Follow the installation steps for Linux.

Note: to access to a Windows directory from your Linux application, use `/mnt`.
For example to `cd` into _C:\Users_ execute `cd /mnt/c/Users`.

## Usage
### Exo-REM
#### Setup
1. **CIA**: verify if the collision-induced absorption (CIA) files are inside the _data/cia_ directory. If you want to use other CIA files, respect the same format and use "CIAname.cia.txt" as file name (e.g. "H2-He.cia.txt").
2. **Clouds**: if you intend to use clouds, verify if the cloud optical constants (OCST) files are inside the _data/cloud_optical_constants_ directory. If you want to use other OCST files, respect the same format and use "cloudName.ocst.txt" as file name (e.g. "H2O.ocst.txt").
3. **k-coefficients**: you can download compressed *Exo-REM* k-tables [here](https://lesia.obspm.fr/exorem/). Put k-coefficients of desired absorbers inside the _data/k_coefficients_tables_ directory. You can also use directly *CKC* outputs converted into HDF5 using [*Exo_k*](http://perso.astrophy.u-bordeaux.fr/~jleconte/exo_k-doc/index.html#) (recommended) or *txt2h5*, or e.g. *[petitRADTRANS](https://petitradtrans.readthedocs.io/en/latest/content/available_opacities.html)* k-tables. If your absorber name is "absorberName", the lines file needs to be named "absorberName.ktable.exorem.h5" (e.g. H2O.ktable.exorem.h5).
4. **Stellar spectra**: if you intend to use stellar spectra, you can download them e.g. [here](http://svo2.cab.inta-csic.es/theory/newov2/index.php?models=bt-settl). You **must** modify them in order to respect the *Exo-REM* data format (see section data format).
5. **Thermochemical tables**: verify if all the condensates and gases thermochemical tables are in the _data/thermochemical_tables_ directory. If you want to add more species to the chemical model, respect the same format and use "speciesName.tct.dat" as file name (e.g. "H2O.tct.dat").
6. **A-priori temperature profile**: put a temperature profile as a priori inside the _inputs/atmospheres/temperature_profiles_ directory (by default). The *Exo-REM* data format must be respected. **You can also use *Exo-REM* HDF5 outputs**. You should have received an example of such a file with your *Exo-REM* distribution.
7. **Input file**: copy and edit the file _inputs/example.nml_ to suit your needs. **Be careful:** 
    - `n_cia` must match the number of values of `cia_names`;
    - `n_species` must match the number of values of `species_names` **and** `species_at_equilibrium`;
    - `n_clouds` must match the number of values of all the cloud parameters **except** `cloud_mode` and `cloud_fraction`;
    - `species_names` and `cia_names` values must be the names used for the _data/cia_ and _data/k_coefficients_tables_ files.

#### Running
1. Open a terminal.
2. `cd` yourself into the Exo-REM _bin_ directory.
3. Launch the calculations by executing:
    ```bash
    ./exorem.exe path/to/input_file_name.nml
    ```
    Example: 
    ```bash
    ./exorem.exe ../inputs/betapicb.nml
    ```
4. By default, at the end of the calculations, the results are stored in the _outputs/exorem_ directory.

**Disclaimer:** *Exo-REM* is a software that can be tricky to master. Tinkering may be necessary in order to
obtain decent results. A cookbook is available in the wiki of the software 
[here](https://gitlab.obspm.fr/dblain/exorem/-/wikis/Advice-and-troubleshooting).

#### Plotting
1. Open a terminal.
2. `cd` yourself into the Exo-REM main directory.
3. Plot the results by executing:
	```bash
    python exorem_plot path/to/file --path_outputs path/to/figures --format figures_format --inputs_extension extension
    ```
    Example, by default:
    ```bash
    python exorem_plot ./outputs/exorem/betapicb.h5
    ```
    Or, to generate png figures (instead of pdf) in directory _figs_ from .dat outputs:
    ```bash
    python exorem_plot betapicb --path_inputs ./outputs/exorem/ --path_outputs figs --inputs_extension dat --format png
    ```

Notes: more plotting options are available in the _src/python/plot_figures.py_ module.

### CAXS
#### Setup
1. Put [GEISA 2015](http://cds-espri.ipsl.fr/cgi-bin/geisa/geisa2015_format) - formatted lines data of the species of your choice inside the _data/lines_ directory. These data needs to contains **at least** GEISA parameters A to D. If your species name is "speciesName", the lines file needs to be named "speciesName.geisa.txt" (e.g. "H2O.geisa.txt").
2. Copy the file _data/species/H2O.nml_, rename the copy as _speciesName.nml_ and edit it to suit your needs.
3. Edit the file _inputs/input_parameters.nml_ to suit your needs. **Be careful:** 
    - `n_levels` must match the number of values of `pressure_space`;
    - if `use_profile_mode` is `True`, `n_levels` must also match the number of values of `temperature_space`;
    - if `use_profile_mode` is `False`, `size_thermospace` must match the number of values of `temperature_space`;
    - `n_species` must match the number of values of `species_names` and `species_vmr`;
    - `species_names` values must be the names used for the _data/lines_ and _data/species_ files.

#### Setup for Rey et al. 2017 CH4 lines
1. Get Rey et al. CH4 lines from [here](http://theorets.tsu.ru/molecules.ch4.hybrid) (both strong lines and weak lines).
2. Put the lines files inside the _data/lines/CH4_ directory (`mkdir` it if it doesn't exists yet).
3. Follow the "setup" steps above from step 2. **Be sure to exactly write "CH4" for the _data/species_ file name and for the species name**.

#### Running
1. Open a terminal.
2. `cd` yourself into the Exo-REM _bin_ directory.
3. Launch the calculations by executing (replace `X` by the number of processes you want to use):
    ```bash
    mpiexec -np X caxs.exe
    ```
    Alternatively, if you want to use only 1 process, you can execute (you still need to have OpenMPI installed):
    ```bash
    ./caxs.exe
    ```
4. By default, at the end of the calculations, the results are stored in the _outputs_ directory.

### CKC
After running *CAXS*, you can run *CKC* in order to obtain k-tables:
```bash
./ckc.exe
```
*CKC* uses the same input file than *CAXS*.

### txt2h5
Check also [Exo_k](http://perso.astrophy.u-bordeaux.fr/~jleconte/exo_k-doc/index.html#).
After running *CKC*, you can convert the *CKC* ASCII outputs into HDF5 files by running *txt2h5*:
```bash
./txt2h5.exe 'list of species' path/to/ktable/species.ktable.exorem.txt ['additional info']
```
Example:
```bash
./txt2h5.exe 'CH4 H2O' ../data/k_coefficients_tables/txt/ 'line list DOI'
```

### optpropgen (code developed by F. Montmessin and J.-B. Madeleine at LATMOS/LMD)
Run using:
```bash
./optpropgen.exe path/to/refractive_index_file.dat path/to/output_file.ocst.txt [min_radius max_radius]
```
Example, to calculate for radii between 1e-6 and 1e-5 m (default is 1e-7 and 1e-4 m):
```bash
./optpropgen.exe ../data/cloud_optical_constants/relative_refractive_indices/optind_ice.dat ../data/cloud_optical_constants/ice.ocst.txt 1e-6 1e-5
```

## Data format
Most of the files in *Exo-REM* uses the same data format:
- The data are organized in columns.
- The first line contains the labels of the columns, separated by whitespaces or tabs.
	This line can also contains additional information or comments by adding a "!" after all the labels.
	This can be used for example in stellar spectra to add the effective temperature of the spectrum.
- The second line contains the units of the columns, separated by whitespaces or tabs.
- The following lines contains the actual data, the columns are separated by whitespaces or tabs.

Example for a stellar spectrum:
```dat
# wavelength spectral_radiosity ! effective_temperature = 3500 K <additional info>
# angstrom erg.s-1.cm-2.a-1
...
   13012.831 5.018110e+05	
   13013.030 5.098700e+05
...
```

### Convert NIST-JANAF thermochemical tables into Exo-REM data files
1. Download NIST-JANAF thermochemical tables [here](https://janaf.nist.gov/).
2. Put them into the _./data/thermochemical_tables/gases/_ or ./data/thermochemical_tables/condensates/ directory (by default).
3. Open a terminal.
4. `cd` yourself into the Exo-REM main directory.
5. Convert the files:
	```bash
    python nist_tct2dat.py path/to/thermochemical/tables/
    ```
    Example:
    ```bash
    python nist_tct2dat.py ./data/thermochemical_tables/gases/
    ```
    
## Wiki
The wiki of the software can be found [here](https://gitlab.obspm.fr/dblain/exorem/wikis/home).

## References
1. [Baudino et al. 2015](https://doi.org/10.1051/0004-6361/201526332)
2. [Baudino et al. 2017](https://doi.org/10.3847/1538-4357/aa95be)
3. [Charnay et al. 2018](https://doi.org/10.3847/1538-4357/aaac7d)
4. [Blain et al. 2020](https://doi.org/10.1051/0004-6361/202039072)
