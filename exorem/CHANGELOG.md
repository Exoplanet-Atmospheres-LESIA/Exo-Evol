# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com)
and this project adheres to [Semantic Versioning](http://semver.org).


## [2.2.0] - 2021-06-10
### Added
- Options to output transmission spectra, and fluxes.
- Output of the cloud contribution to emission spectra.
- Output of clear and full coverage of spectral radiosity.
- Output of run diagnostic values (chi2, delta chi2, delta T, actual T_int, ratio between actual and targeted radiosity).
- (Python) cloud coverage argument in function plot_emission_spectrum().
- (Python) useful functions to convert between units.

### Changed
- (HDF5 only) unwanted outputs are filled with one zero per dimension, to save storage memory.
- (Python) more consistent function names and arguments order.

## [2.1.3] - 2021-05-08
### Changed
- Module transfer renamed radiative_transfer.
- Clearer subroutine names in module radiative_transfer.
- Clearer variable names in module radiative_transfer.
- Code clean-up.

### Fixed
- Huge performance issue due to unnecessary call of a function. Calculation time is reduced by a factor ~2 with 81 atmospheric levels.
- Interpolating function not optimal (small performance increase).
- Sorting function not optimal (tiny performance increase).

## [2.1.2] - 2021-05-05
### Fixed
- Low (< 0.1 cm-1) k-coefficients wavenumber step not always re-calculated correctly.
- Insufficient precision for wavenumbers when writing txt k-coefficients.
- Division by 0 occurring when initial atmospheric composition contains no He or no metals.

## [2.1.1] - 2021-04-30
### Fixed
- Clouds VMR calculated before being loaded.

## [2.1.0] - 2021-04-29
### Added
- Option to load a Kzz profile.
- Option to load a cloud profile.
- Cloud supersaturation parameter as input.
- Cloud sticking efficiency as input.
- Option to output only a limited number of parameters (only using .dat outputs).

### Fixed
- Loading of some VMR not working properly.

## [2.0.1] - 2021-04-15
### Fixed
- Opacities not calculated correctly when using multiple k-coefficients with only one sample point.
- Out of bounds array occurring when calculating synthetic spectra outside of CIA wavenumber range.
- Total transmission spectrum cannot be excluded in `plot_contribution_transmission_spectra()`.

## [2.0.0] - 2021-01-19
### Added
- Cloud module.
- Condensation of Al2O3, Cr, Cr2O3, MnS, ZnS and NH3.
- Eddy diffusion coefficient mode as input.
- Python scripts to read outputs and make figures.
- Option to output in HDF5 format.
- Output of the transmission spectrum (with and without clouds) and of its derivative.
- Output of elements total VMR.
- Output of all gaseous species VMR (not only of the absorbers).
- Output of matrix K.
- Option to load VMR profiles.
- Option to load pressure grid from temperature profile file.
- Option to use irradiation instead of target-light source range.
- Option to stop the code after reaching a given convergence tolerance.
- Option to call the chemistry every n-intrations.
- Option to call the cloud physics every n-intrations.
- Option to calculate downward flux from stellar spectrum.
- Option to calculate the metallicity of a planet based on solar composition.
- Options to output the species and/or cia spectral contributions.
- Possibility to read the temperature profile from HDF5 Exo-REM outputs.
- Command line options `--help` and `--version` for all programs.
- Display of the mean molar mass at iteration 0.
- Module `chemistry` used to store chemical parameters.
- Module `optics` used to store optical functions.
- Module `atmosphere` in `objects.f90`, used to store atmospheric parameters.
- Module `cloud` in `objects.f90`, used to store clouds parameters.
- Module `exorem_retrieval` in `objects.f90`, used to retrieval parameters.
- Module `light_source` in `objects.f90`, used to light source (star) parameters.
- Module `target` in `objects.f90`, used to store target (planet) parameters.
- Module `utils` in `utils.f90`, used to store utilitary functions.
- Module `exorem_interface` in `exorem_interface.f90`, used to manage the interface specific to *Exo-REM*.
- Module `k_coefficients_interface`, used to manage the interface specific to *CKC* (can be used independently).
- More attributes to species and target.
- K-coefficients archives.
- Support for HDF5-formatted k-coefficients.
- New program `ktables_txt2hdf5` to convert txt-formatted k-coefficients into HDF5 files.
- New program `optpropgen` to generate cloud optical properties.
- CAXS: pure Voigt profile option for species.

### Changed
- The command to run *Exo-REM* is now `./exorem.exe path/to/input_file` instead of `./exorem.exe < path/to/input_file`.
- Input files format from text to Namelist.
- Most data files extensions.
- *k*-tables units must now be conform to ExoMol *k*-tables units.
- Convection is now calculated with more precision
- Chemistry is now based explicitly on elemental abundance.
- Rayleigh scattering is now calculated from the wavelength-dependent refractive indices of individual species.
- Heat capacity is now read from files and is calculated taking all the gaseous species into account.
- Gibb's free energies are now read from files instead of hardcoded.
- Eddy diffusion coefficient is now an input.
- Removed limitations in number of levels, wavenumbers.
- CIA can be added and removed from the input file.
- *Exo-REM* makefile is now common with *CAXS* and *CKC* makefile.
- Module `physical_constants` renamed `physics`.
- *Exo-REM* now uses the `interface`, `physics` and `math` modules common to *CAXS* and *CKC*.
- Constant `cst_p0` now in Pa instead of bar.
- Chemistry modules merged into one.
- All subroutines are now inside modules.
- Rework of terminal outputs.
- All arrays are now dynamically allocated.
- Hard array sizes are now shared instead of redefined in every files.
- Some variables have now clearer names.
- Terminal output improvements.
- Code clean-up.
- CAXS: line shape of species is now a string defined in the species data file.
- CKC: no more limited to have a wavenumber range exactly corresponding to cross-section files range.

### Removed
- CAXS: saturation profiles parameters the species data file, as they were not used and are replaced by the 
thermochemical table files.

### Fixed
- Cloud condensing multiple times.
- Chemistry not behaving properly at high metallicities.
- Layers molar mass not accurate at high metallicities.
- Quenched VMR not accurately calculated.
- Height scale in chemistry not accurate at high metallicities.
- H2, He and H2O CIA not taking into account variation of VMR with layers.
- Conservation of oxygen element not respected in thermochemical calculations.
- Some cloud causing NaN when condensing below the grid.
- Some cloud not condensing at the right pressure.
- Some cloud mixing mode not working properly.
- Cloud not forming correctly when eddy diffusion coefficient is low.
- Adiabatic correction underestimated on highly irradiated planets.
- Correlated-k mixing method leading to a slight underestimate of the absorption on certain cases.
- Fix metallicity not being updated properly in some cases.
- Better fixes for the crashes due to chemistry.
- Altitude calculated only at initialization despite the temperature changing.
- A NaN-inducing bug caused by variable `ng` not initialised in function `chem_noneq`.
- A NaN-inducing bug caused by the chemistry of NH4SH in function `chem_noneq`.
- A rare NaN-inducing bug caused by variable `qh2` not being initialised in function `read_k_coeff`.
- A rare NaN-inducing bug caused by variable `qh2o` not being initialised in function `chem_noneq`.
- A rare NaN-inducing bug caused by variable `qch4` not being initialised in function `chem_noneq`.
- Cloud radiative transfer never calculated.
- Physical variables calculated at multiple places, with small variations and different names.
- Some numbers not in double precision.
- Cloud optical constants reloaded at each iteration.
- CKC: negative k-coefficients in some rare cases. 

## [1.5.0] - 2019-12-19
### Changed
- All outputs have now the same format: the two first lines are reserved respectively for the columns label and unit.
- The code lines used to write all the output files are now in separate subroutines.

## [1.4.0] - 2019-12-19
### Added 
- H2O continuum (self and foreign).

### Changed
- CIA files must now be in ASCII, self and foreign CIA must be separated.
- Better H2O saturation pressure calculation with the addition of the gas-liquid condensation.

### Fixed
- Quenching of CO2 not taking H2 VMR into account.
- Index error when calculating the CIA contribution spectrum. 

## [1.3.0] - 2019-12-17
### Added
- Output of the contribution spectra of each absorbers and of the CIA contribution spectrum.

### Changed
- The two first lines of Exo-REM spectrum file output are now reserved respectively for the columns label and unit.
- File `transac_AD.f90` renamed `exorem.f90`.
- The code lines used to write the spectrum files are now in a separate subroutine.

### Fixed
- Division by zero still happening in equilibrium calculations.

## [1.2.4] - 2019-12-11
### Changed
- Code clean-up.

### Fixed
- All implicits variables are now explicit.

### Removed
- Unused function `detau`.
- Unused arguments in functions `cloud_mixing` and `cloud_mixing2`.

## [1.2.3] - 2019-12-10
### Changed
- Saturation profile functions of H2O and NH4SH are now inside module `physics`.
- Matrix inversion changed from Gauss-Jordan to LU (Doolittle), leading to minor speed improvement.
- Increased `nfreq_max` to 4000.
- (Minor) no more deprecated character length declarations.
- (Minor) no more obsolete in-line function declarations.
- Code clean-up.

### Fixed
- Some unused variables.
- (Minor) some type mismatches.
- (Minor) all float direct comparison.

### Removed
- Unused function `eint`.

## [1.2.2] - 2019-12-06
### Changed
- Better Makefile.
- Quick reorganisation of transac_AD.
- Replaced deprecated built-in function `dabs` by `abs`.
- Code clean-up.

## [1.2.1] - 2019-12-02
### Changed
- Removed unused variables.
- Removed some unused dummy arguments.
- Replaced some deprecated built-in functions.
- Other code clean-up.

### Fixed
- Some floating point exception that could occur when the chemistry takes place below the grid.

## [1.2.0] - 2019-11-28
### Added
- Redistribution factor in spherical black body radiance calculation.
- Output of the upward flux.

### Changed
- Code cleanup.

### Fixed
- Division by zero happening sometimes in equilibrium calculations.
- Division by zero happening sometimes at high wavenumbers low temperatures.

## [1.1.2] - 2019-11-22
### Changed
- Code cleanup.

### Fixed
- Downward flux not calculated properly when adding stellar insolation.

## [1.1.1] - 2019-11-21
### Changed
- Code cleanup.

## [1.1.0] - 2019-11-21
### Added
- Stellar insolation in ExoRem.

## [1.0.1] - 2019-10-30
### Changed
- Code cleanup.

## [1.0.0] - 2019-10-29
### Added
- ExoRem source code translated to fortran90.

# Pre Exo-REM changelog 
## [3.4.0] - 2019-10-23
### Added
- Python script to merge the absorption cross section files.

### Changed
- Lowered interpolation tolerance of the k-vector.

## [3.3.0] - 2019-10-17
### Added
- Function `search_sorted` to find the index of the closest value to another one inside a sorted array.

### Changed
- Subroutine `read_absorption_cross_sections_files` now output the wavenumber array instead of the number of 
wavenumbers.

### Fixed
- Changes in wavenumber step between absorption cross sections files not taken into account.
- Upper bound of correlated-k intervals not calculated properly.
- Segmentation fault occurring sometimes in subroutine `quicksort_index`.

## [3.2.2] - 2019-09-25
### Fixed
- Low-intensity lines not pre-filtered during lines file reading.

## [3.2.1] - 2019-09-23
### Fixed
- Segmentation fault occurring sometimes when saving lines parameters of multiple species, due to the array storing 
the number of lines by species initialised with random values instead of zeros.

## [3.2.0] - 2019-09-11
### Added
- Calculation of the line shape of Sodium and Potassium.

## [3.1.2] - 2019-09-09
### Changed
- Processes info file name format now display the species name and the wavenumber range instead of the date.
- Program `CAXS` no longer display in which files it saves the absorption cross sections, in order to make a cleaner and
lighter terminal output.

## [3.1.1] - 2019-09-06
### Changed
- Absorption cross section files calculated on multiple processes on a large wavenumber range were very heavy due to the
code forcing all processes to have the same wavenumber step. The wavenumber step is now calculated based on the minimal
wavenumber of each process, complicating the calculation of the k-coefficients but lowering memory usage and calculation
time.

## [3.1.0] - 2019-09-03
### Added
- Correction factor for Sousa-Silva et al. 2015 PH3 line list.
- Output file containing the time-to-complete and the spectral interval of each processes.
- Some comments.

### Changed
- All the processes have the same wavenumber steps.
- Subroutine `gauleg` renamed `calculate_gauss_legendre_quadrature`.
- Reference to "ExoMol" in k-coefficients file name removed.
- Code cleanup.

### Fixed
- Wavenumber max of last sub-interval sometimes being 1 step greater than the requested maximum wavenumber.

## [3.0.0] - 2019-08-29
### Added
- Full integration of correlated-k calculation.
- Path options for absorption cross sections and k-correlated files.
- Input parameters for correlated-k calculations.
- More compilation options in the *Makefile*.

### Changed
- All lines files are now read only once.

### Fixed
- Absorption cross sections of CH4 strong lines in Rey et al. format added two times to the total absorption cross 
section.
- Infinite loop when trying to read lines files where the maximum wavenumber is below the lower bound of the spectral
interval.
- Typos in terminal display.

### Removed
- Input parameter `k_distribution_file_prefix`.

## [2.0.0] - 2019-08-23
### Added
- Input parameter `k_distribution_file_prefix`.

### Changed
- Input parameter `output_file` renamed `absorption_cross_section_file_prefix`.
- Code cleanup.

### Fixed
- Subroutines `quicksort` sorting arrays in decreasing order instead of ascending order.

## [1.2.1] - 2019-08-22
### Fixed
- When combining multiple species, all of them have the line parameters of the last species from the second spectral 
sub-interval.
- When combining multiple species, CH4 from Rey et al. have the reference temperature of the last species from the
second spectral sub-interval.

## [1.2.0] - 2019-08-21
### Added
- Subroutine to read the absorption coefficient files.

### Removed
- References to mpi in module `interface`.

## [1.1.0] - 2019-08-14
### Added
- Program to calculate the k-correlated coefficients.

### Changed
- CAXS-related source files moved into a _caxs_ sub-folder.

## [1.0.4] - 2019-08-12
### Fixed
- Crash when reading Rey et al. CH4 lines at maximum temperature.

## [1.0.3] - 2019-08-09
### Fixed
- Lines parameters not updated properly in runs with multiple species and multiple thermodynamic spaces.
- Temperatures not allocated properly when not using profile mode.
- Superlines parameters not allocated properly.

## [1.0.2] - 2019-08-09
### Fixed
- Wavenumbers sharing between processes not behaving properly in some cases.

## [1.0.1] - 2019-08-09
### Changed
- Removed a useless variable.

### Fixed
- Crash when writing absorption cross sections of multiple species.

## [1.0.0] - 2019-08-06
### Added
- Doppler deviation tolerance as input parameter.
- Minimum spectral interval size, for parallelisation.
- Option to use the parameters `pressure_space` and `temperature_space` either as a temperature profile (p(T)) or as 
independent parameters (p(i), T(j)).
- Option to output absorptions species by species instead of the absorptions taking into account all the species at 
once.
- Display of the completion time of each processes.

### Changed
- Input file namelists order.
- Pressure input is now in Pa instead of mbar.
- Main program renamed `CAXS`.
- Module `transfer` moved into the _fortran_ directory.
- Wavenumbers are now shared among processes following a logarithmic rule instead of a linear rule.
- Lines are now rejected only if their intensity is below the minimum intensity and the minimum tested intensity.
- Changed the order of the parameters of subroutine `read_lines_files_ch4`.
- Simplified the `tau_lines` functions and replaced them by a single, clearer `get_line_absorption_cross_sections` function.
- Result writing is now done in a separate subroutine in module `interface`.
- Faster lines file reading.
- More detailed terminal output.
- Improved terminal output.
- Removed one remaining archaism.
- Cleaner variable declarations.

### Fixed
- Spectral sub-intervals recovering themselves.
- Infinite loop when the minimum wavenumber of a sub-interval were above the maximum wavenumber.
- Incorrect minimal wavenumber when reading lines file.
- Missing docstrings.

## [0.6.0] - 2019-07-23
### Added
- Subroutine `get_rey_ch4_lines_file` to automatically get Rey et al. CH4 lines and superlines files.

### Changed
- Lines files format (except for CH4) updated to GEISA 2015 format.
- CH4 data (except lines and superlines) are now read as other species data.
- Changed some variables names to clearer ones in subroutine `read_lunes_files_ch4`.

### Fixed
- Last superlines not broadened if the number of superlines is higher than the number of lines.
- CH4 lines and superlines not read correctly.
- Dimensions of some species data incorrectly passed.

## [0.5.0] - 2019-07-19
### Added
- Outputs of the calculated absorption cross sections.
- Module `mpi_utils` to store useful Open Mpi functions.

### Changed
- Parallelisation on the wavenumbers using OpenMPI.
- Use of the COdata 2018 physical constants.
- Most terminal outputs commented due to the parallelisation.

### Fixed
- Conditions in subroutine `tau_lines1` not treated correctly.
- Parameter `f_voigt` of subroutines `tau_lines1`, `*2` and `*3` incorrectly declared as an output.
- Parameter `broadening_borders` of subroutines `read_lines_files` and `*_ch4` incorrectly declared as an output.
- Some values of function `voigt_from_data` written as floats instead of doubles.

### Removed
- Output of the mean absorption cross section.

### Important note
The modification of hc/k due to the use of COdata 2018 constants causes **relative** differences up to ~ 1E-3 
between the absorption cross sections of PH3 calculated by the previous versions and the one calculated by this version. 
The peak relative difference value was measured at 800 K and 100 bar (the highest temperature and pressure considered 
during our tests). This can be compared with the numeric relative noise value of 1E-4 due to the output format.

## [0.4.1] - 2019-07-15
### Fixed
- Incorrect character length for species names.
- Bad version display.
- Uppercase variables.

## [0.4.0] - 2019-07-15
### Added
- Module `transfer` to store transfer subroutines.
- Module `math` to store useful mathematical functions.
- Module `physical_constants` to store useful constants.
- Function `read_voigt_file` to read the Voigt data file (separated from subroutine `transfer`).
- Function `count_lines` to count the number of lines in a file.
- Variable `lines_files` to module `species`.
- More terminal messages.
- The makefile of the code.
- Paths to data as inputs.
- Output file name as input.

### Changed
- Reorganisation of the code interface.
- Reorganisation of the code physics.
- Molar masses are now in kg.mol-1 instead of g.mol-1.
- Run parameters are read from a Fortran Namelist instead of the ASCII file _transac.dat_.
- Species data are read from a Fortran Namelist rather than inside the _transac.dat_ file.
- No more intermediate temperature profile outputs.
- All arrays are now dynamically allocated.
- Run parameters are no more read inside subroutine `transfer`.
- Rework of subroutine `det_step`, renamed `get_synthetic_spectrum_wavenumber_step` and moved into module `transfer`.
- Subroutine `transfer` renamed `calculate_species_absorption_cross_sections` and moved into module `transfer`.
- Subroutine `read_lines_files_sp5` renamed `read_lines_files_ch4`.
- Subroutines `read_lines_files` and `read_lines_files` moved into module `interface`.
- Subroutines `tau_lines1`, `*2` and `*3` moved into module `transfer`.
- Lots of variables have now much clearer names.
- Removed file _global_variable.f90_ and moved its content into module `interface`.
- Source file _transfer_khi.f90_ renamed _transfer.f90_.
- Date is now displayed at the start of the program.
- Code is now in Fortran2018 standard.
- Removed some remaining archaisms.

### Removed
- Subroutine `transac`.

## [0.3.1] - 2019-07-08
### Fixed
- Test code in _transac.f90_ removed.

## [0.3.0] - 2019-07-08
### Changed
- Reorganised code structure.

## [0.2.0] - 2019-07-05
### Changed
- Function `tina` moved into program `transac`.
- Function `voigt` moved into subroutine `transfer_khi`.
- Removed all unused variables.
- Removed all implicit definitions.
- Removed all `goto`.
- Removed all labels.
- Removed most (all ?) archaisms.

### Fixed
- Remaining out of bound array.
- One variable defined as `doubleprecision` instead of `integer`.

### Removed
- Useless files _k.f90_ and _cre_v.f90_

## [0.1.0] - 2019-07-05
### Added
- Shell script _clean_ to easily clean the _k_correlated_ directory.
- Explicit output writing in a file to subroutine `transfer_khi`.

### Changed
- Code is now in Fortran2008 standard (some obsolecences are still here).

### Fixed
- Program `transac` not reading the "transac" file.
- Variable `corps` not passed correctly from program `transac` to subroutine `transfer_khi`.
- Out of bound arrays.

## [0.0.0] - 2019-06-30
Legacy program by Bruno Bézard and Jean-Loup Baudino.
