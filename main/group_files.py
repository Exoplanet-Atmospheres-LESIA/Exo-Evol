#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 18 11:50:52 2023

@author: cwilkinson
"""
import sys
sys.path.insert(0, '..')

import pandas as pd
import numpy as np
import pyarrow.feather as feather
import glob
import matplotlib.pyplot as plt
from python_modules.plotting_tools_module import PlotRetrieval
from python_modules.instrument_module import read_filter_docs, photo_columns, load_filter, apply_filter
from python_modules.interface_module import load_spectrum, get_closest_interpolator, load_pkl
from python_modules.grid_data_processing_module import ProcessGrid
from python_modules.quality_module import QualityControl
from python_modules.regression_module import MLmodels
from python_modules.physics_module import wavenumber_to_wavelength
import gc

n_files = 24

#for ii in range(1000):
grid_tools = ProcessGrid('*dec2024/*_500.pkl', n_files)
#grid_tools.clear_files()
grid_tools.check_folders()
print('Checked folders')
grid_tools.load_files(-1)
print('Loaded files')
grid_tools.add_columns()
print('Added columns')
grid_tools.save_files()
print('Saved files')
del grid_tools
gc.collect()

QC = QualityControl(n_estimators=5000)
QC.prepare_data()
QC.train_model() 
del QC
gc.collect()

