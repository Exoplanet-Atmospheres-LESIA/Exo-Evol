import sys
sys.path.insert(0, '..')

from python_modules.ML_module import *
from python_modules.interface_module import *
from python_modules.grid_data_processing_module import *

from concurrent.futures import ThreadPoolExecutor, as_completed
from itertools import product

def main() :
    
    run_load_grid = True
    
    if run_load_grid :
        df = load_grid('./../../data/current_exoevol_grid/ker/','cloudy',EOS='ker',Mass=[0,40],Met=[-2,2],
                                    Core=[0,40],T_irr=[0,2000],T_int=[0,2000],clouds=True,equilibrium=False,jobs=1) 
        df["Met"] = np.log10(df['Met'])
        print(len(df))

        replace_none_nan(df, ['radiosity_up', 'depth_up', 'wavenumber_up'])
        df = df[df['radiosity_up'].notna()]
        df = df[df['depth_up'].notna()]
        df = df[df['wavenumber_up'].notna()]
        df = df[df['/outputs/spectra/transmission/contributions/CH4'].notna()]

        df['radiosity_up_'] = convert_radiosity(df['radiosity_up'], df['wavenumber_up'])
        df['depth_up_'] = convert_transit(depth=df['depth_up'], old_radius=df['/model_parameters/target/radius_1e5Pa'],
                                            computed_radius=df['Radius_Jup'],
                                            light_source_radius=df['/model_parameters/light_source/radius'])
        df['wavelength_up'] = wavenumber_to_wavelength(df['wavenumber_up'])
        df['wavelength'] = wavenumber_to_wavelength(df['/outputs/spectra/wavenumber'])
        wavelength = df['wavelength'].iloc[0]
        
        print('Grid loaded')
    
    photometric_points = ['MIRI F1065C','MIRI F1140C','MIRI F1550C']
    
    output_path = './../../data/ML_model/data'
    
    make_models = True
    
    if make_models :
        # Run functions in parallel using ThreadPoolExecutor
        with ThreadPoolExecutor(max_workers=1) as executor:
            futures = {
                executor.submit(S_evol_ML, df, output_path): "S_evol_ML",
                executor.submit(radius_ML, df, output_path): "radius_ML",
                executor.submit(S_ML, df, output_path): "S_ML"
            } 
            
            for filter in photometric_points :
                futures[executor.submit(photometric_ML, df, filter,  output_path)] = f"filter {filter}"

            futures[executor.submit(radiosity_spectrum_ML(df,output_path))] = f"Radiosity spectrum"

        # Collect and print results as they complete
        for future in as_completed(futures):
            task_name = futures[future]
            try:
                result = future.result()
                print(f"{task_name} returned: {result}")
            except Exception as exc:
                print(f"{task_name} generated an exception: {exc}")
                
    
    load_grid_files = False
    if not load_grid_files :
        # Define the parameter ranges
        core = [5,10,20,30,40]
        f_sed = list(np.linspace(1, 5, 5))
        T_irr = [50,100,150,200,250]
        mass_values = list(np.logspace(0, 1, 50))
        metallicities = [-1,-0.5,0,0.5,1]

        # Generate all combinations once using itertools.product
        combinations = np.array(list(product(mass_values, core, f_sed, T_irr, metallicities)))

        # Create the DataFrame and assign columns using the combinations array
        df = pd.DataFrame(combinations, columns=['Mass_Jup', 'core_Earth', 'f_sed', 'T_irr', 'Met'])
    
    dfs = split_dataframe(df, n=23, col='Mass_Jup')
    
    f_s_tree = load_f_s(output_path)
    f_r_tree = load_f_r(output_path)
    f_ds_dt_tree_scaled, mean_evol = load_f_ds_dt(output_path)
    
    f_s_to_use = lambda x: f_s(f_s_tree, x)
    f_r_to_use = lambda x: f_r(f_r_tree, x)
    f_ds_dt_to_use = lambda x: f_ds_dt(f_ds_dt_tree_scaled, mean_evol, x)
        
    # Run functions in parallel using ThreadPoolExecutor
    with ThreadPoolExecutor() as executor:
        futures = {}
        # Start each function as a separate task
        for idx, frame in enumerate(dfs) :
            futures[executor.submit(create_age_grid, frame, (f_ds_dt_to_use, f_s_to_use, f_r_to_use), output_path, idx)] = f"frame {idx}"

        
        # Collect results as they complete
        for future in as_completed(futures):
            function_name = futures[future]
            try:
                result = future.result()
                print(f"{function_name} returned: {result}")
            except Exception as exc:
                print(f"{function_name} generated an exception: {exc}")
            
if __name__  ==  "__main__":
    main()