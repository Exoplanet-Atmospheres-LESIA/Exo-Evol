import sys
sys.path.insert(0, '..')

import numpy as np
import pandas as pd
from python_modules.grid_data_processing_module import load_grid
from python_modules.numerical_tools_module import interpolate_negatives_rowwise
from sklearn.ensemble import RandomForestRegressor
from python_modules.regression_module import MLmodels
from python_modules.quality_module import QualityControl
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import gc

def main():
    #for ii in range(1000):
    n_estimators = 1000
    ML = MLmodels(n_estimators=n_estimators)
    ML.prepare_data()
    
    target = 'depth_radius'
    _  = ML.make_model(target)
    print('Completed depth')
    del _
    gc.collect()
    
    target = 'Radius_Jup'
    _  = ML.make_model(target)
    print('Completed Radius_Jup')
    del _
    gc.collect()
    
    target = 'S'
    _  = ML.make_model(target)
    print('Completed S')
    del _
    gc.collect()
    
    target = 'ds_dt'
    _  = ML.make_model(target)
    print('Completed ds_dt')
    del _
    gc.collect()
    
    target = 'flux_photo'
    _  = ML.make_model(target)
    print('Completed flux')
    del _
    gc.collect()
    
    target = 'Tstandard'
    _ = ML.make_model(target)
    print('Completed Tstandard')
    del _
    gc.collect()
    
    _ = ML.age_regressor()
    print('Completed age')
    del _
    gc.collect()
        
if __name__ == '__main__':
    main()