"""
Created on Thu May 18 11:50:52 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

from itertools import product
import os
from python_modules.exoris_module import make_exoris
from python_modules.exorem_module import make_exorem
from python_modules.link_planet_module import LinkPlanet
from python_modules.interface_module import input_paths
import time


def main():
    file_prefix = str(sys.argv[-2]) + '.' + str(sys.argv[-1])
    print(file_prefix)
    
    current_location = os.getcwd()
    print('Location of code : {}'.format(current_location))
    
    exoris_path = input_paths ('exoris')
    exorem_path = input_paths ('exorem')
    
    make_exoris(file_prefix,exoris_path)
    print('Exoris {} made'.format(file_prefix))
    
    make_exorem(file_prefix,exorem_path)
    print('Exorem {} made'.format(file_prefix))
    
    print('Initialising LinkPlanet class')
    time.sleep(float(sys.argv[-1]))
    planets = LinkPlanet(file_prefix,int(sys.argv[-1]))
    planets.run_make_grid(iterations=1000)
    
if __name__  ==  "__main__":
    main()