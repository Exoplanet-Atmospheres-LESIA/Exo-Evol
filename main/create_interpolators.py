"""
Created on Unknown

@author: cwilkinson
"""

import sys
sys.path.insert(0, '..')

from python_modules.interpolation_module import *
from python_modules.interface_module import *

def main() :
    print(os.getcwd())
    n_tasks = float(sys.argv[-2])
    idx = float(sys.argv[-1])
    
    mass_range = list(interp_parameters('mass_range'))
    n_interpolarors = int(interp_parameters('n_interpolarors'))
    use_clouds = bool(interp_parameters('use_clouds'))
    planet_type = interp_parameters('planet_type')
    
    interp_intrinsic_temperature = bool(interp_parameters('interp_intrinsic_temperature'))
    interp_radius = bool(interp_parameters('interp_radius'))
    interp_entropy = bool(interp_parameters('interp_entropy'))
    interp_entropy_evol = bool(interp_parameters('interp_entropy_evol'))
    interp_age = bool(interp_parameters('interp_age'))
    interp_flux = bool(interp_parameters('interp_flux'))
    interp_profile = bool(interp_parameters('interp_flux'))
            
    path_intrinsic_temperature = str(interp_paths('output_file')) + str(interp_paths('folder_intrinsic_temperature'))
    path_radius = str(interp_paths('output_file')) + str(interp_paths('folder_radius'))
    path_entropy = str(interp_paths('output_file')) + str(interp_paths('folder_entropy'))
    path_entropy_evol = str(interp_paths('output_file')) + str(interp_paths('folder_entropy_evol'))
    path_age = str(interp_paths('output_file')) + str(interp_paths('folder_age'))
    path_flux = str(interp_paths('output_file')) + str(interp_paths('folder_flux'))
    path_profile = str(interp_paths('output_file')) + str(interp_paths('folder_profile'))
    
    CPUs_used = 0
    if interp_intrinsic_temperature & (idx == 0):
        print('Creating intrinsic temperature interpolator')
        create_T_int_interpolator_grid(mass_range,1,use_clouds,planet_type,path_intrinsic_temperature)
        print('\nIntrinsic temperature interpolator saved in {}\n'.format(path_intrinsic_temperature))
        n_tasks -= 1
    if interp_radius & (idx == 1):
        print('Creating radius interpolator')
        create_R_interpolator_grid(mass_range,1,use_clouds,planet_type,path_radius)
        print('\nRadius interpolator saved in {}\n'.format(path_radius))
        n_tasks -= 1
    if interp_entropy & (idx == 2):
        print('Creating entropy interpolator')
        create_S_interpolator_grid(mass_range,1,use_clouds,planet_type,path_entropy)
        print('\nEntropy interpolator saved in {}\n'.format(path_entropy))
        n_tasks -= 1
    if interp_entropy_evol & (idx == 3):
        print('Creating entropy evolution interpolator')
        create_dS_dt_interpolator_grid(mass_range,1,use_clouds,planet_type,path_entropy_evol)
        print('\nEntropy evolution interpolator saved in {}\n'.format(path_entropy_evol))
        n_tasks -= 1
    if interp_flux & (idx == 4):
        print('Creating flux interpolator')
        create_flux_interpolator_grid(mass_range,1,use_clouds,planet_type,path_flux,idx)
        print('\nFlux interpolator saved in {}\n'.format(path_flux))
        n_tasks -= 1
    if interp_profile & (idx == 5):
        print('Creating profile interpolator')
        create_profile_interpolator_grid(mass_range,n_interpolarors,use_clouds,planet_type,path_profile,idx)
        print('\nFlux interpolator saved in {}\n'.format(path_profile))
        n_tasks -= 1
    if interp_age & (idx == 6) :
        n_age_interpolators = 0
        mass_range = np.logspace(np.log10(mass_range[0]),np.log10(mass_range[1]),n_interpolarors)
        data_folder = path_age.split('data')[0]+'data/current_exoevol_grid/ker'
        df = load_grid(data_folder,planet_type,EOS='ker',Mass=[0,1e4],Met=[-2,2],
                            Core=[0,1e4],T_irr=[0,1e4],T_int=[0,1e4],clouds=use_clouds)
        create_age_interpolator_grid(df,n_age_interpolators,use_clouds,planet_type,path_age,path_entropy_evol,path_entropy,path_radius)
        print('\nAge evolution interpolator saved in {}\n'.format(path_age))

if __name__  ==  "__main__":
    main()