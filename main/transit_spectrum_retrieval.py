#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 15:00:52 2023

@author: cwilkinson
"""
import sys
sys.path.insert(0, '..')

import difflib
import glob
import os

from python_modules.fitting_observation_module import RetrieveTransit
from python_modules.plotting_tools_module import IdentifyTransit
from python_modules.physics_module import T_irr_at_planet


def main():
    IT = IdentifyTransit(
        planet = 'wasp39b')
    
    initial_parameters = [
            0.28,
            1,
            20,
            400,
            3,
            1e8]
    bounds = (
            [0.05,1],
            [-1,1.3],
            [0,100],
            [0,1000],
            [1,6],
            [1e4,1e11])

    planet_mass = [0.28, 0.03] # set to [0,0] for no mass constraint
    T_irr = T_irr_at_planet(0.9,5400.0,0.0486)
    n_walkers = 48
    n_iterations = 3000
    observed_spectrum = IT.observed_spectrum
    spectrum_name = 'wasp39b'
    arguments = ''
    output_file = '../data/retrievals/planets'
    distance_planet = 230.0
    planet_age = 0
    planet_age_error = 0
    reset = True

    RetrieveTransit(
        initial_parameters,
        T_irr,
        planet_mass,
        bounds,
        n_walkers,
        n_iterations,
        observed_spectrum,
        spectrum_name,
        arguments,
        output_file,
        distance_planet,
        planet_age,
        planet_age_error,
        reset,
        debug=False).MCMC()


if __name__ == "__main__":
    main()
