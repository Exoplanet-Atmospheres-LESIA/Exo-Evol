#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 15:00:52 2023

@author: cwilkinson
"""
import sys
sys.path.insert(0, '..')

import difflib
import glob
import os

from python_modules.fitting_observation_module import RetrieveObservation
from python_modules.instrument_module import identify_observation
from python_modules.interface_module import retrieval_parameters_direct, retrieval_paths_direct, load_spectrum



def main():
    print(os.getcwd())
    idx = int(sys.argv[-1])
    
    spectrum_name = retrieval_paths_direct('input_spectrum')
    spectrum_file = difflib.get_close_matches(
        spectrum_name, glob.glob('../data/spectra/*/*'), n=1, cutoff=0.2)[0]
    n_walkers = retrieval_parameters_direct('n_walkers')
    n_iterations = retrieval_parameters_direct('n_iterations')
    if retrieval_parameters_direct('use_name') :
        print('Using name')
        distance_planet, planet_age, planet_age_error = map(float, spectrum_file.split('/')[-1].replace('.txt','').split('_')[1:])
        print('Age of planet {}Myr'.format(planet_age))
        print('Distance to planet {}pc'.format(distance_planet))
    else :
        distance_planet = retrieval_parameters_direct('distance_planet')
        planet_age = retrieval_parameters_direct('planet_age')
        planet_age_error = retrieval_parameters_direct('planet_age_error')
        
    arguments = retrieval_paths_direct('arguments')
    regressor_type = retrieval_parameters_direct('regressor')
    interpolator = retrieval_parameters_direct('interpolator')
    output_file = retrieval_paths_direct('output_file')
    initial_parameters = [
        retrieval_parameters_direct('init_mass'),
        retrieval_parameters_direct('init_Met'),
        retrieval_parameters_direct('init_core'),
        retrieval_parameters_direct('init_Tint'),
        retrieval_parameters_direct('init_fsed'),
        retrieval_parameters_direct('init_kzz'),
        retrieval_parameters_direct('init_S0')]
    bounds = (
        retrieval_parameters_direct('bounds_mass'),
        retrieval_parameters_direct('bounds_Met'),
        retrieval_parameters_direct('bounds_core'),
        retrieval_parameters_direct('bounds_Tint'),
        retrieval_parameters_direct('bounds_fsed'),
        retrieval_parameters_direct('bounds_kzz'),
        retrieval_parameters_direct('bounds_S0'))
    T_irr = 50
    reset = True
    use_offsets = False
    print('Age of planet {}Myr'.format(planet_age))
    print('Distance to planet {}pc'.format(distance_planet))
    print('Retrieving {}'.format(spectrum_name))
    observed_spectrum = load_spectrum(spectrum_file)
    n_instruments = len(
        set(identify_observation(observed_spectrum)['instrument']))
    print('{} instruments detected : {}'.format(n_instruments,
                                                set(identify_observation(observed_spectrum)['instrument'])))
    if use_offsets:
        print('Using offsets')
        for n in range(n_instruments):
            initial_parameters += (1,)
            bounds += ((0.1, 3),)
    RetrieveObservation(
        initial_parameters,
        T_irr,
        bounds,
        n_walkers,
        n_iterations,
        observed_spectrum,
        spectrum_name,
        arguments,
        regressor_type,
        interpolator,
        output_file,
        distance_planet,
        planet_age,
        planet_age_error,
        reset,
        debug=True).MCMC()


if __name__ == "__main__":
    main()
