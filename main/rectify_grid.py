import sys
sys.path.append("..")

from itertools import product
import os
import glob
import pickle as pkl
from tqdm import tqdm
from python_modules.exorem_module import make_exorem, run_exorem_again
from python_modules.link_planet_module import LinkPlanet
from python_modules.interface_module import input_paths
import time

def main():
    
    file_prefix = str(sys.argv[-2]) + '.' + str(sys.argv[-1])
    print(file_prefix)
    
    current_location = os.getcwd()
    print('Location of code : {}'.format(current_location))
    
    exorem_path = input_paths ('exorem')

    make_exorem(file_prefix,exorem_path)
    print('Exorem {} made'.format(file_prefix))
    
    files = glob.glob(input_paths('output_file')+'/*.pkl')
    num_sublists = 24
    sublist_length = len(files) // num_sublists
    sublists = [files[i * sublist_length:(i + 1) * sublist_length] for i in range(num_sublists)]
    print(sublists)
    
    files = sublists[int(sys.argv[-1])-1]
    for file in tqdm(files) :
        print(file)
        model = pkl.load(open(file, 'rb'))
        #if ('new_depth' not in model.columns) or ('new_pressure' not in model.columns) or ('new_temperature' not in model.columns) :
        new_model = run_exorem_again(model,input_paths('exorem_data'),file_prefix)
        model['new_depth'] = [new_model['/outputs/spectra/transmission/transit_depth'].iloc[0]]
        model['new_pressure'] = [new_model['/outputs/layers/pressure'].iloc[0]]
        model['new_temperature'] = [new_model['/outputs/layers/temperature'].iloc[0]]
        model.to_pickle(file)
    
    

    
    
    
if __name__  ==  "__main__":
    main()