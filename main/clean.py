"""
Created on Wed Jan 10 17:01:00 2024

@author: cwilkinson
"""

import sys
sys.path.append("..")

import os
import glob

def main():
    exoris_folders = glob.glob('../temp/running_exoris/*')
    for folder in exoris_folders :
        os.system('rm -r {}'.format(folder))
        
    exorem_folders = glob.glob('../temp/running_exorem/*')
    for folder in exorem_folders :
        os.system('rm -r {}'.format(folder))

if __name__  ==  "__main__":
    main()