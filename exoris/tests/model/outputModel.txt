 ** Final model : 
        Core fraction :  1.2527E-02
        Rock fraction in the core :  5.3740E-01
        Helium mass fraction :  7.8700E-02
           R eq =   8.6717E+04 km ; difference with measure :   2.1295E-01
        J2 x 10^6 =   2.6616E+04 ; difference with measure :   8.1096E-01
        J4 x 10^6 =  -1.8626E+03 ; difference with measure :   2.1894E+00
