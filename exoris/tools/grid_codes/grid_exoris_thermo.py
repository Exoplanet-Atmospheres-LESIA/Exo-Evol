import os
import sys 
import numpy as np
from itertools import product
import random
import pandas as pd
from scipy.interpolate import interp1d, interp2d
import glob
import json
import time

""" Grid making code used to explore thermodynamic properties. 
Files containing isentropes are copied into the exoris/tests/example_isentropes folder
Code makes new directory in the folder where exoris is held *exoris_running*, 
exoris thermo explorer, will run there """

R_J = 69911000
M_J = 1.898*1e27
R_E = 6371*1e3
G_u = 6.674*1e-11


def main():
    os.chdir('../../bin')
    parameters = grid() 
    for parameter in parameters :
        print(parameter)
        input_file(parameter)
        run_exoris(parameter)
    return

def grid() :
    
    Ts = np.arange(150, 325, 25)
    core = [10]
    yhe = np.arange(0.25, 1, 0.1)
    mass = [5]
    eos_env = ['ker']
    eos_ice = ['ker_h2o']
    eos_core = ['ker_mgsio3']
    
    return list(product(mass,core,yhe,Ts,eos_env,eos_ice,eos_core))

def input_file(parameters) :
    mass, core, yhe, Ts, eos_env, eos_ice, eos_core = parameters
    P_link = 1
    M_E = 5.972*1e24
    M_J = 1.898*1e27
    R_J = 69911000

    rock = 0.5

    if len(glob.glob('../../bin/fort.1')) > 0:   
        os.remove('../../bin/fort.1')
    
    os.system('pwd')
    if core>1 :
        core = core*M_E/mass
        
    path = 'parameters.txt'
    with open(path,"w") as f:
        L = [" ! Parameter file \n"]
        L = L + ["&horedt_nml \n\n"]
        L = L + ["computation = 'model' \n"]
        L = L + ["Nint = 400 \n\n"] 
        L = L + ["prot = 9.925\n"]
        L = L + ["measured_r = ", str(R_J*1e2), " \n"] # In cm
        L = L + ["mass = ", str(np.round(mass*M_J*1e3,1)), " \n"] # In grams
        L = L + ["surface_T = ", str(np.round(Ts,4))," \n"] # In K
        L = L + ["surface_P = ", str(P_link)," \n"] # In bars
        L = L + ["core = ", str(np.round(core,6))," \n"]
        L = L + ["rock = ", str(np.round(rock,6))," \n"]
        L = L + ["yhe = ", str(np.round(yhe,6))," \n"]
        #L = L + ["yh2o = ", str(np.round(y_h2o,6))," \n"]
        L = L + ["eos_env = '", eos_env,"' \n"]
        L = L + ["eos_ice = '", eos_ice,"' \n"]
        L = L + ["eos_core = '", eos_core,"' \n"]
        #L = L + ["use_core = true\n"]
        L = L + ["verbose = true\n"]
        L = L + ["figure = false\n"]
        L = L + ["giant = true\n"]
        L = L + ["/\n"]
        L = L + [" "]

        f.writelines(L)
        f.close()
                
    return

def run_exoris(parameters):
    mass, core, yhe, Ts, eos_env, eos_ice, eos_core = parameters
    os.system("./horedt5")
    os.system('cp fort.1 '+'../../../data/EOS/exoris_adiabats/{}_{}.1'.format(yhe,Ts))
    os.remove('fort.1')
    return

if __name__  ==  "__main__":
    main()