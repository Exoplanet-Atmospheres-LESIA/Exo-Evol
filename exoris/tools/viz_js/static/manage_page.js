'use strict';

// Imports
var PlotData = DataClass.PlotData,
    VarClass = DataClass.VarClass,
    AppendCurrentValue = DataClass.AppendCurrentValue,
    AppendSectionTitle = DataClass.AppendSectionTitle

;


$(document).ready(function(){
  document.getElementById('jsonfile').addEventListener('change', handleFileSelect, false);
});


function handleFileSelect(evt) {
  var files = evt.target.files; // FileList object

  var f = files[0];

  var reader = new FileReader();

  // Closure to capture the file information.
    reader.onload = (function (theFile) {
        return function (e) {
          var JsonObj = e.target.result;
	  var data = JSON.parse(JsonObj);
	  //console.log(Object.keys(data));

	  var 
	  input_vars   = ["mass", "Rp","J2","J4", "Ts","Ps","prot"],
	  model_vars   = ["Nint", "eos_env", "eos_ice", "eos_rock"],
	  model_vars_input  = ["rock", "core", "yhe"],
	  results_vars = ["Req","J2","J4","rock", "core", "yhe"];


	  AppendSectionTitle('#here_plotted',"Plotted values");
	  PlotData(data);

	  AppendSectionTitle('#here_physical',"Physical Parameters");
	  for(var ii in input_vars){
	    var name = input_vars[ii];
	    AppendCurrentValue(
	      '#here_physical',
	      new VarClass(name,data.parameters.physical[name]),
	      true);
	  }


	  AppendSectionTitle(
	    '#here_model',
	    "Used model");
	  for(ii in model_vars){
	    name = model_vars[ii];
	    AppendCurrentValue(
	      '#here_model',
	      new VarClass(name,data.parameters.model[name])
	      ,true);
	  }


	  AppendSectionTitle(
	    '#here_model_input',
	    "Initial guess for the model");
	  for(ii in model_vars_input){
	    name = model_vars_input[ii];
	    AppendCurrentValue(
	      '#here_model_input',
	      new VarClass(name,data.parameters.model[name])
	      ,true);
	  }


	  AppendSectionTitle(
	    '#here_result',
	    "Results");
	  for(ii in results_vars){
	    name = results_vars[ii];
	    AppendCurrentValue(
	      '#here_result',
	      new VarClass(name,data[name])
	      ,true);
	  }





        };
    })(f);

    // Read in JSON as a data URL.
    reader.readAsText(f, 'UTF-8');
}
