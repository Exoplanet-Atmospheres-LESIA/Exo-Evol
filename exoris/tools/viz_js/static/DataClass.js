'use strict';

(function(exports){

  // data modified
  var filterdata = [];

  // axes
  var X;

  var margin = {top: 30, right: 40, bottom: 50, left: 70},
      width = 500 - margin.left - margin.right,
      height = 250 - margin.top - margin.bottom;

  var xdir = d3.scaleLinear().range([0, width]);
  var xAxis = d3.axisBottom().scale(xdir).ticks(5,d3.format('.4'));

  // class to represnt variables
  var VarClass = function(name,options){
    this.name = name;
    this.unit = '';
    this.symbol = '';
    this.color = 'black';
    this.dir = function(){return 0;};
    for(var ii in options){this[ii]=options[ii];}

  };


  X = new VarClass('R',{color:"black"});

  var graphs = {
    rho : new VarClass('rho',{color:"blue"}),
    T : new VarClass('T',{color:"red"}),
    P : new VarClass('P',{color:"green"})
  };


  var mousemove = function() {
    var x = d3.mouse(this)[0] ;
    // console.log('alla',this)

    d3.selectAll('.line').each(function(i){
      // console.log(this,d3.select(this));
      var pathEl = d3.select(this).node();

      var beginning = x, end = pathEl.getTotalLength();

      while (true) {
	var target = Math.floor((beginning + end) / 2);
	var pos = pathEl.getPointAtLength(target);
	if ((target === end || target === beginning) && pos.x !== x) {
          break;
	}
	if(pos.x > x){end = target;}
	else if(pos.x < x){beginning = target;}
	else{break;} //position found
      }

      var id = d3.select(this).attr('id').replace('path_','');

      d3.select("#focus_"+id)
	.attr("opacity", 1)
	.attr("cx", x)
	.attr("cy", pos.y);


      d3.select("#linex_focus_"+id)
	.attr("opacity", 1)
	.attr("x1", 0)
	.attr("x2", x)
	.attr("y1", pos.y)
	.attr("y2", pos.y)
      ;
      d3.select("#liney_focus_"+id)
	.attr("opacity", 1)
	.attr("x1", x)
	.attr("x2", x)
	.attr("y1", height)
	.attr("y2", pos.y)
      ;


      d3.select(".current_val."+id)
	.text(d3.format('.4')(graphs[id].dir.invert(pos.y)));
    });

    d3.select(".current_val."+X.name)
      .text(d3.format('.4')(xdir.invert(x)));
    
  };


  function PlotData(data){
    var param = data.parameters.physical,
	model = data.parameters.model;

    for(var ii=0; ii<model.Nint.val;ii++){
      var locobj = {};
      locobj[X.name] = data[X.name].val[ii];
      for(var nn in graphs){
	locobj[nn] = data[nn].val[ii];
      }
      filterdata.push(locobj);
    }

    X.unit = data['R'].unit;
    var x = X.name;

    AppendCurrentValue('#here_plotted',X);

    // Scale the range of the data for x's
    var xminmax = d3.extent(filterdata, function(d) { return d[x]; });
    xdir.domain(xminmax);


    // offset array for regions
    var offset_array = (
      function fillcolor(i){
	// percentuage for core
	var core = data.cindex.val/model.Nint.val*100;
	var ice  = data.iindex.val/model.Nint.val*100;
	return  [
	  {offset: "0%", color: "black"},
	  {offset: (core)+"%", color: "black"},	
	  {offset: (core+1)+"%", color: "blue"},		
	  {offset: (ice)+"%", color: "blue"},		
	  {offset: (ice+1)+"%", color: "lightblue"},		
	  {offset: "100%", color: "lightblue"}	
	];
      })();

    console.log(offset_array)
    console.log(model.Nint.val)

    for(var vv in graphs){
      graphs[vv].unit = data[vv].unit;

      var V = graphs[vv];

      // Scale the range of the data for y's
      V.dir = d3.scaleLinear().range([height, 0]);
      V.dir.domain(d3.extent(filterdata, function(d) { return d[vv]; }));


      // Define the area
      var area = d3.area()
	    .x(function(d) { return xdir(d[x]); })
	    .y0(height)
	    .y1(function(d) { return V.dir(d[vv]); })
      ;


      // Define the line
      var valueline = d3.line()
	    .x(function(d) { return xdir(d[x]); })
	    .y(function(d) { return V.dir(d[vv]); })
      ;


      var svg = d3.select("#here_plots_"+vv)
	    .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
	    .append("g")
            .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")");

      var line = svg.append("g");

      var focus = svg.append("g")
	    .attr('class','focus')
      	    .style("display", "none");


      // set the gradient
      svg.append("linearGradient")				
	.attr("id", "area-gradient")			
	.attr("gradientUnits", "userSpaceOnUse")	
	.attr("x1", 0).attr("y1", 0)			
	.attr("x2", xdir(xminmax[1])).attr("y2", 0)		
	.selectAll("stop")						
	.data( offset_array)					
	.enter().append("stop")			
	.attr("offset", function(d) { return d.offset; })	
	.attr("stop-color", function(d) { return d.color; })
	.attr("stop-opacity", ".8");

      // Add the valueline path.
      line.append("path")
      	.data([filterdata])
      // .attr("id", "path_"+vv)
      	.attr("class", "area")
      	.attr("d", area)
	.style('fill',"url(#area-gradient)")
      ;


      // Add the valueline path.
      var path = line.append("path")
	    .data([filterdata])
      	    .attr("id", "path_"+vv)
      	    .attr("class", "line")
      	    .attr("d", valueline)
      	    .style('stroke' , graphs[vv].color)
      ;

      // Add the X Axis
      svg.append("g")
	.attr("class", "axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis);

      svg.append("text")
	.attr("class", "axis label")
	.attr("x", (width))
	.attr("y", height+30)
	.text(x+'('+X.unit+')')
      ;


      // Add the Y Axis
      var yAxis = d3.axisLeft().scale(V.dir).ticks(5,d3.format('.4'));

      svg.append("g")
	.attr("class", "axis")
	.call(yAxis);

      svg.append("text")
	.attr("class", "axis label")
	.attr("x", -(height+margin.top)/2)
	.attr("y", -50)
	.attr("transform", "rotate(-90)")
	.text(vv+' ('+graphs[vv].unit+')')
      ;

      // append the rectangle to capture mouse
      line.append("rect")
	.attr("width", width)
	.attr("height", height)
	.style("fill", "none")
	.style("pointer-events", "all")
	.on("mouseover", function() { d3.selectAll(".focus").style("display", null); })
	.on("mouseout", function() { d3.selectAll(".focus").style("display", "none"); })
	.on("mousemove", mousemove);

      // append the circle at the intersection
      var circle =
            svg.append("circle")
	    .attr('id','focus_'+graphs[vv].name)
	    .attr("class","focus")
            .attr("r", 3)
            .attr("fill", "red");

      svg.append("line")
	.attr('id','linex_focus_'+graphs[vv].name)
	.attr("class","focus")
	.style('stroke','red')
	.style("stroke-dasharray", ("3, 3"))
      ;
      
      svg.append("line")
	.attr('id','liney_focus_'+graphs[vv].name)
	.attr("class","focus")
	.style('stroke','red')
	.style("stroke-dasharray", ("3, 3"))
      ;



      AppendCurrentValue('#here_plotted',graphs[vv]);

    }
  }


  function AppendCurrentValue(where,varClassInstance,first){
    first = first || false;
    var valcontainer = d3.select(where)
	  .append("p")
	  .attr("class","text")
	  .style("margin-left",'12px');

    valcontainer
      .append("text")
      .text(varClassInstance.name+ " = ")
    ;

    var value = (first) ? varClassInstance.val : "--";

    valcontainer
      .append("text")
      .attr("class","current_val "+varClassInstance.name)
      .text(value)
    ;
    if(varClassInstance.unit !== ''){
      valcontainer
	.append("text")
	.text(" ("+varClassInstance.unit+ ")")
      ;
    }
    if(varClassInstance.info !== ''){
      valcontainer
	.append("text")
	.attr("class","text-info")
	.text(varClassInstance.info)
	.style('margin-left','20px')
      ;

    }
  }

  function AppendSectionTitle(where,title){
    var valcontainer = d3.select(where)
	  .append("p")
	  .attr("class","text-title")
	  .style("margin-left",'4px')
	  .text(title)
    ; 
  }


  exports.PlotData = PlotData;
  exports.VarClass = VarClass;
  exports.AppendCurrentValue = AppendCurrentValue;
  exports.AppendSectionTitle = AppendSectionTitle;

})(this.DataClass = {});
