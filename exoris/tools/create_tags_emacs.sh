#!/bin/bash
# Create the file TAGS which can be used with emacs to navigate

SRC=$1
[ -z $SRC ]  && \
    SRC='Src' && \
    echo "using default sources location ($SRC)"


[ -z $(which etags) ] && \
    echo "etags is not installed so TAGS file cannot be created" && exit

[ ! -d $SRC ] && echo "Error : source directory ($SRC) does not exist"&& exit

find ./$SRC  -regex  '.*\(\.h\|.F90\)$' |etags -
#find  Src/ -name *.F90

# HOW TO USE IT IN EMACS
# Emacs provides several commands and keybindings for using tags.

# M-x visit-tags-table loads the TAGS file.

# M-. (find-tag) finds a tag in the buffer (from the current tags table).

# M-TAB (complete-symbol) completes keyword at point or displays a list of possible completions.

# M-* (pop-tag-mark) moves back to where M-. was last invoked.

# M-, (tags-loop-continue) continue the last tags-search or tags-query-replace command.

# M-x list-tags <RET> file <RET> will display a list of the tags defined in the program file file.

# M-x tags-apropos <RET> regexp <RET> displays a list of all tags matching regexp.
