SRCFILES = \
	 parameters_type.F90 \
	 calculations.F90 \
	 eosmod.F90 \
	 general.F90 \
	 horedt5.F90 \
	 m_constants.F90 \
	 m_minimization.F90 \
	 m_newtonraphson.F90 \
	 m_splines.F90 \
	 thermo.F90 \
	 types.F90 \
	 jsonfile.F90 \
