
!!============================================================
!!============================================================
!> @brief Traitement of json files
!!
!! This module contains the tools to operate on the json file
!! containing the input/output informations of a simulation
!!
!!
module jsonfile
 use m_constants
 implicit none

 private
 public :: jsonId, initJSON, wrtVarJSON, finishJSON, openSectionJSON, closeSectionJSON

 integer,parameter :: jsonId = 666
 integer :: jsonIndent

 interface wrtVarJSON
   module procedure wrtVarJSON_dp
   module procedure wrtVarJSON_bool
   module procedure wrtVarJSON_int
   module procedure wrtVarJSON_char
 end interface wrtVarJSON


contains


 !!============================================================
 !> @brief open the jsonfile
 !! @param[in] filename of json file
 subroutine initJSON(filename)
  character(*),intent(in) :: filename

  jsonIndent = 0
  open(unit = jsonId,file=trim(filename),status='replace')

  write(jsonId,'(a)') repeat(' ',jsonIndent)//'{'
  jsonIndent = jsonIndent + 2
 end subroutine initJSON

 !!============================================================
 !> @brief close the jsonfile
 subroutine finishJSON()
  write(jsonId,'(a1)')'}'
  close(jsonId)
 end subroutine finishJSON

 !!============================================================
 !> @brief open section the jsonfile
 !! @param[in] sectionname section to open
 !! @param[in] first if not preceded from comma
 subroutine openSectionJSON(sectionname,first)
  character(*),intent(in) :: sectionname
  logical,intent(in),optional :: first

  if(.not. present(first))then
    write(jsonId,'(a)') repeat(' ',jsonIndent)//','
  endif
  write(jsonId,'(a)') repeat(' ',jsonIndent)//'"'//trim(sectionname)//'" : {'
  jsonIndent = jsonIndent + 2

 end subroutine openSectionJSON

 !!============================================================
 !> @brief close section the jsonfile
 !!
 subroutine closeSectionJSON()
  jsonIndent = jsonIndent - 2
  write(jsonId,'(a)') repeat(' ',jsonIndent)//'}'
 end subroutine closeSectionJSON

 !!============================================================
 !> @brief saves a relevant DIMENSIONED array to json file
 !! @param[in] name array[2] variable name and units
 !! @param[in] values array of values
 subroutine wrtVarJSON_dp(name,values,first_element,description)
  character(*),intent(in) :: name(2)
  real(dp),intent(in) :: values(:)
  logical,intent(in),optional :: first_element
  character(*),intent(in),optional :: description

  integer :: n
  character(10) :: strformat

  n = size(values)-1

  if(.not. present(first_element)) then
    write(jsonId,'(x,a)')repeat(' ',jsonIndent)//','
  endif


  ! variables
  write(jsonId,'(a)') repeat(' ',jsonIndent)//'"'//trim(name(1)) // '" : {  '
  if(present(description))then
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)//'"info" : "' // description //'" , '
  endif
  write(jsonId,'(2x,a)')  repeat(' ',jsonIndent)//'"unit" : "' // trim(name(2)) //'" ,      '
  if(n==0) then
    write(jsonId,'(2x,a,es12.4)')  repeat(' ',jsonIndent)//'"val"  : ',values(:)
  else
    write(strformat,'(i10)')n
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)//'"val"  : [             '
    write(jsonId,'(2x,a,es12.4,'//trim(strformat)//'(",",es12.4))')repeat(' ',jsonIndent),values(:)
    write(jsonId,'(2x,a)')repeat(' ',jsonIndent)//']'
  end if
  write(jsonId,'(1x,a)')repeat(' ',jsonIndent)//'}'


 end subroutine wrtVarJSON_dp

 !!============================================================
 !> @brief saves a relevant DIMENSIONED array to json file
 !! @param[in] name array[2] variable name and units
 !! @param[in] values array of values
 subroutine wrtVarJSON_bool(name,values,first_element,description)
  character(*),intent(in) :: name(2)
  logical,intent(in) :: values(:)
  logical,intent(in),optional :: first_element
  character(*),intent(in),optional :: description

  integer :: n
  character(10) :: strformat
  character(5) :: value_ch

  n = size(values)-1

  if(.not. present(first_element)) then
    write(jsonId,'(x,a)')repeat(' ',jsonIndent)//','
  endif

  if(values(1))then
    value_ch = 'true'
  else
    value_ch = 'false'
  endif

  if(n>0)then
    print *,'ERROR wrtVarJSON_bool bool of size >1'
    stop
  endif

  ! variables
  write(jsonId,'(a)') repeat(' ',jsonIndent)//'"'//trim(name(1)) // '" : {  '
  if(present(description))then
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)//'"info" : "' // description //'" , '
  endif
  write(jsonId,'(2x,a)')  repeat(' ',jsonIndent)//'"unit" : "' // trim(name(2)) //'" ,      '
  write(jsonId,'(2x,a,a5)')  repeat(' ',jsonIndent)//'"val"  : ',value_ch
  write(jsonId,'(1x,a)')repeat(' ',jsonIndent)//'}'


 end subroutine wrtVarJSON_bool


 !!============================================================
 !> @brief saves a relevant DIMENSIONED array to json file
 !! @param[in] name array[2] variable name and units
 !! @param[in] values array of values
 subroutine wrtVarJSON_int(name,values,first_element,description)
  character(*),intent(in) :: name(2)
  integer,intent(in) :: values(:)
  logical,intent(in),optional :: first_element
  character(*),intent(in),optional :: description

  integer :: n
  character(10) :: strformat

  n = size(values)-1

  if(.not. present(first_element)) then
    write(jsonId,'(x,a)')repeat(' ',jsonIndent)//','
  endif

  ! variables
  write(jsonId,'(a)') repeat(' ',jsonIndent)//'"'//trim(name(1)) // '" : {  '
  if(present(description))then
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)//'"info" : "' // description //'" , '
  endif
  write(jsonId,'(2x,a)')  repeat(' ',jsonIndent)//'"unit" : "' // trim(name(2)) //'" ,      '
  if(n==0) then
    write(jsonId,'(2x,a,i12)')  repeat(' ',jsonIndent)//'"val"  : ',values(:)
  else
    write(strformat,'(i10)')n
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)//'"val"  : [             '
    write(jsonId,'(5x,a,i12,'//trim(strformat)//'(",",i12))') repeat(' ',jsonIndent),values(:)
    write(jsonId,'(2x,a)')repeat(' ',jsonIndent)//']'
  end if
  write(jsonId,'(1x,a)')repeat(' ',jsonIndent)//'}'

 end subroutine wrtVarJSON_int



 !!============================================================
 !> @brief saves a relevant DIMENSIONED array to json file
 !! @param[in] name array[2] variable name and units
 !! @param[in] values array of values
 subroutine wrtVarJSON_char(name,values,first_element,description)
  character(*),intent(in) :: name(2)
  character(*),intent(in) :: values(:)
  logical,intent(in),optional :: first_element
  character(*),intent(in),optional :: description

  integer :: n
  character(10) :: strformat

  n = size(values)-1


  if(.not. present(first_element)) then
    write(jsonId,'(x,a)')repeat(' ',jsonIndent)//','
  endif

  ! variables
    write(jsonId,'(a)') repeat(' ',jsonIndent)//'"'//trim(name(1)) // '" : {  '
  if(present(description))then
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)//'"info" : "' // description //'" , '
  endif
  write(jsonId,'(2x,a)')  repeat(' ',jsonIndent)//'"unit" : "' // trim(name(2)) //'" ,      '
  if(n==0) then
    write(jsonId,'(2x,a,a)') repeat(' ',jsonIndent)//'"val"  : "',trim(values(1))//'"'
  else
    write(strformat,'(i10)')n
    write(jsonId,'(2x,a)') repeat(' ',jsonIndent)// '"val"  : [             '
    write(jsonId,'(5x,a,"""",a12,"""",'//trim(strformat)&
      &          //'(",""",a12,""""))')repeat(' ',jsonIndent),values(:)
    write(jsonId,'(4x,a1)')']'
  endif
  write(jsonId,'(1x,a)')repeat(' ',jsonIndent)//'}'
  flush(jsonId)

 end subroutine wrtVarJSON_char


end module jsonfile
