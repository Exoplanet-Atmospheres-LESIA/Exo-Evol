
/*horedt5.h*/

/*******************************************************************
Configure file for EosRIS
Author: MMancini
Configuration date:  2018-11-21

This file is generated automatically by ~EosRIS//Config/local_setting.py
Do not change it manually!
*********************************************************************/

#ifndef HORODET5_H
#  define HORODET5_H

/* compilation flags*/
#  define __DATADIR './horedt5_data'

#endif
