
!!============================================================
!!============================================================
!!
!> @brief Module interfacing the thermo module with the various EOS
!!
!! Units : pressure in bar, everything else in CGS.\n
!! Every available EOS may have its own units system, be careful :\n
!! SCVH [1] : all in CGS \n
!! ANEOS : rho and T in CGS, pressures in bar, energies in kJ/g \n
!! The extensive quantites (like the entropy) are given as specific. \n
!! Each function is named like quantity_zone_variables, eg rho_env_PT gives rho(P,T) for the enveloppe. \n
!! \n
!! References : [1] Saumon, D.; Chabrier, G.; van Horn, H. M. ; Astrophysical Journal Supplement v.99, 1995 \n
!! References : [2] Hubbard, W. B.; Marley, M. S. ; Bulletin of the American Astronomical Society, Vol. 20, 1988
!!
!!
module eosmod

 use m_constants
 use parameters_type,  only : parameters
 use m_newtonraphson,  only : nr2_1, nr3_2
 use aneos,            only : aneosInit, S_an, dSdT_an,&
   &                          P_an_dunite, dPdrho_an_dunite,&
   &                          P_an, dPdrho_an,&
   &                          P_an_serpentine, dPdrho_an_serpentine, &
   &                          P_an_iron, dPdrho_an_iron
 use scvheos,          only : dSdP_scvh_PT_T, dSdT_scvh_PT_P, drhodP_scvh_PT_S,&
   &                          S_scvh_PT, rho_scvh_PT, init_sceos
 use ker_h_he,         only : rho_ker_PT, dSdP_ker_PT_T, S_ker_PT,&
   &                          dSdT_ker_PT_P, drhodP_ker_PT_S, init_ker
 use kerley_fe,        only : kerley_init_fe, P_fe_RT, dPdrho_kerfe
 use kerley_mgsio3,    only : kerley_init_mgsio3, P_mgsio3_RT, dPdrho_kermgsio3
 use kerley_h2o,       only : dSdT_kerh2o, S_h2o_rt, dpdrho_kerh2o,&
   &                          p_h2o_rt, find_rho_h2o, kerley_init_h2o


 implicit none

 private

 public :: init_eos, dSdP_env_PT, dTdP_env_PT_S, drhodP_env_PT_S,&
   &       rho_env_PS, rho_ice_PT, rho_env_PT, T_env_PS,&
   &       S_env_PT, rho_core_PT, dSdT_env_PT, rho_core2_PT,&
   &       rho_core1_PT, T_adia_PS

 real(dp),private :: c_poly=1.d4 !< This polytropic constant is chosen so that a n=1 polytrope has a density 1e-2 g/cc under a 1 bar pressure



 abstract interface
   function PRT_func(rho,T)
    use m_constants
    real(dp) :: PRT_func
    real(dp), intent (in) :: rho,T
   end function PRT_func
 end interface


 type PRT_type
   procedure(PRT_func),nopass,pointer :: P => null()
   procedure(PRT_func),nopass,pointer :: dPdrho => null()
  contains
   procedure,pass :: init => PRT_init
 end type PRT_type

 type(PRT_type) :: PRT_core, PRT_core1, PRT_core2


contains


 subroutine PRT_init(THIS,eos)
  class(PRT_type),intent(inout) :: THIS
  character(*),intent(in) :: eos

   select case(eos)
    case('hm')
      THIS%P => P_hm_RT
      THIS%dPdrho => dPdrho_hm_RT
      ! THIS%rho => PRT_rho

    case('aneos_dunite')
      THIS%P => P_an_dunite
      THIS%dPdrho => dPdrho_an_dunite

    case('aneos_serpentine' )
      THIS%P => P_an_serpentine
      THIS%dPdrho => dPdrho_an_serpentine

    case('aneos_iron' )
      THIS%P => P_an_iron
      THIS%dPdrho => dPdrho_an_iron

    case('aneos_ice',"ker_h2o" )
      THIS%P => P_ice_RT
      THIS%dPdrho => dPdrho_ice_RT

    case('ker_fe')
      THIS%P => P_fe_RT
      THIS%dPdrho => dPdrho_kerfe

    case('ker_mgsio3')
      THIS%P => P_mgsio3_RT
      THIS%dPdrho => dPdrho_kermgsio3

    case('poly')
      THIS%P => P_poly_RT
      THIS%dPdrho => dpdrho_poly

    case default
      print*, 'Error : unknown core EOS in eosmod : THIS%P '//eos
      stop

    end select
   end subroutine PRT_init





 !!============================================================
 !> @brief EOS initialization subroutine
 subroutine init_eos
  write(*, '(a,/,a)'), '','** Initializing EOS...'
  if ( parameters%envEOS == 'scvh' ) then
    call init_sceos()
  elseif(parameters%envEOS == 'ker') then
    call init_ker()
  elseif(parameters%envEOS ==  'ker_h2o') then
    call kerley_init_h2o()
  end if
  if (parameters%iceEOS == 'aneos_ice' .or. parameters%coreEOS == 'aneos_dunite' .or. &
    parameters%coreEOS == 'aneos_serpentine' .or. parameters%coreEOS == 'aneos_iron' ) then
    call aneosInit()
    !      else if(iceEOS == 'ker_h2o') then
    !            call kerley_init_h2o
  end if
  if( parameters%core1EOS == "ker_fe" .or. &
    & parameters%coreEOS  == "ker_fe" .or. &
    & parameters%core2EOS == "ker_fe" ) then
    call kerley_init_fe()
  endif
  if( parameters%core1EOS == "ker_mgsio3" .or. &
    & parameters%coreEOS  == "ker_mgsio3" .or. &
    & parameters%core2EOS == "ker_mgsio3" ) then
    call kerley_init_mgsio3()
  endif

  if(parameters%giant)then

    call PRT_core%init(parameters%coreEOS)

  else
    call PRT_core1%init(parameters%core1EOS)
    call PRT_core2%init(parameters%core2EOS)
  endif

  if( parameters%core1EOS ==  'ker_h2o' &
    & .or. parameters%coreEOS == 'ker_h2o' &
    & .or. parameters%core2EOS == 'ker_h2o' &
    & .or. parameters%iceEOS == 'ker_h2o') then
    if(parameters%envEOS/='ker_h2o') then
      call kerley_init_h2o()
    endif
  endif
 end subroutine init_eos


 !!============================================================
 !> @defgroup EnveloppeEOS
 !! @{

 !!============================================================
 !> @brief computes rho in the enveloppe from P,T,y
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @param[in] yhe mass fraction of helium
 !! @return density of the H/He mixture in g/cc
 function rho_env_PT(P,T,yhe)
  real(dp), intent(in) :: P,T,yhe
  real(dp)             :: rho_env_PT
  rho_env_PT = 0.d0
  select case(parameters%envEOS)
  case('scvh')
    rho_env_PT = RHO_scvh_PT(P/dynecm2_to_bar,T,yhe)
  case('ker')
    rho_env_PT = rho_ker_PT(P/GPa_to_bar,T, yhe)
  case('poly')
    rho_env_PT = (P/c_poly)**0.5d0
  case('ker_h2o')
    rho_env_PT = find_rho_h2o(T,P)
  case default
    print*, 'Error : unknown enveloppe EOS in eosmod:rho_env_PT' ; stop
  end select
 end function rho_env_PT

 !!============================================================
 !> @brief computes rho in the enveloppe from P,S,y
 !! @param[in] P pressure in bar
 !! @param[in] S entropy in erg/K/g
 !! @param[in] yhe mass fraction of helium
 !! @return density of the H/He mixture in g/cc
 function rho_env_PS(P,S,yhe)
  real(dp), intent(in) :: P,S,yhe
  real(dp)             :: rho_env_PS
  rho_env_PS = 0.d0
  select case(parameters%envEOS)
  case('scvh')
    rho_env_PS = RHO_scvh_PT(P/dynecm2_to_bar, T_env_PS(P,S,yhe),yhe)
  case('ker')
    rho_env_PS = rho_ker_pt(P,T_env_PS(P/GPa_to_bar,S,yhe), yhe)
  case('poly')
    rho_env_PS = sqrt(P/c_poly)
  case('ker_h2o')
    print *, " abinitio EOS"
    stop
  case default
    print*, 'Error : unknown enveloppe EOS in eosmod:rho_env_PS' ; stop
  end select
 end function rho_env_PS

 !!============================================================
 !> @brief computes (drho/dP)_S in the enveloppe from P,S,y
 !! @param[in] P pressure in bar
 !! @param[in] T teperature in K
 !! @param[in] yhe mass fraction of helium
 !! @return (drho/dP)_S of the H/He mixture in g/cc/bar
 function drhodP_env_PT_S(P,T,yhe)
  real(dp), intent(in) :: P,T,yhe
  real(dp)             :: drhodP_env_PT_S
  drhodP_env_PT_S = 0.d0
  if ( parameters%envEOS == 'scvh' ) then
    drhodP_env_PT_S = drhodP_scvh_PT_S(P/dynecm2_to_bar,T,yhe)
  elseif (parameters%envEOS == 'ker') then
    drhodP_env_PT_S = drhodP_ker_PT_S(P/GPa_to_bar,T,yhe)
  else if ( parameters%envEOS == 'poly' ) then
    drhodP_env_PT_S = 0.5d0/sqrt(P*c_poly)
  else
    print*, 'Error : unknown enveloppe EOS in eosmod:rho_env_PT' ; stop
  end if
 end function drhodP_env_PT_S

 !!============================================================
 !> @brief computes T in the enveloppe from P,S,y
 !! @param[in] P pressure in bar
 !! @param[in] S entropy in erg/K/g
 !! @param[in] yhe mass fraction of helium
 !! @return temperature of the H/He mixture in K
 function T_env_PS(P,S,yhe)
  real(dp), intent(in) :: P,S,yhe
  real(dp)             :: T_env_PS
  real(dp) :: T1, T2, S1, S2
  T_env_PS = 0.d0
  if (parameters%envEOS == 'scvh'.or. parameters%envEOS == 'ker') then
    T1 = 50.d0 ; T2 = 1000.d0
    S1 = S_env_PT(P,T1,yhe) - S
    S2 = S_env_PT(P,T2,yhe) - S
    findInterval : do while ( S1*S2 > 0 )
      T1 = T2 ; T2 = T1+1000.d0
      S1 = S_env_PT(P,T1,yhe) - S
      S2 = S_env_PT(P,T2,yhe) - S
    end do findInterval
    T_env_PS = nr3_2(S_env_PT,dSdT_env_PT,P,T1,T2,yhe,S)
  elseif ( parameters%envEOS == 'poly' ) then
    T_env_PS = 1.0d0  ! T is a dummy variable for the polytrope
  else
    print*, 'Error : unknown enveloppe EOS in eosmod:rho_env_PT' ; stop
  end if
 end function T_env_PS

 !!============================================================
    !> @brief computes T in the enveloppe from P,S
    !! @param[in] P pressure in bar
    !! @param[in] S entropy in erg/K/g
    !! @return temperature along the adiabat in K
   function T_adia_PS(P,S,Tmin)
     real(dp), intent(in) :: P,S,Tmin
     real(dp)             :: T_adia_PS
     real(dp) :: T1, T2, S1, S2,rho1,rho2,delta,T_test,rho_test,s_test

         T_adia_PS = 0.d0
         T1 = Tmin ; T2 = Tmin+100.d0
         rho1=rho_ice_pt(P,T1)
         rho2=rho_ice_pt(P,T2)
         S1 = S_h2o_RT(rho1,T1) - S
         S2 = S_h2o_RT(rho2,T2) - S
!         print*, " Sees", rho1,S1,rho2,S2,S, T1, T2, P
!         stop
       findInterval : do while ( S1*S2> 0 )
          T1 = T2 ; T2 = T1+100.d0
          rho1=rho_ice_pt(P,T1)
          rho2=rho_ice_pt(P,T2)
          S1 = S_h2o_RT(rho1,T1) - S
          S2 = S_h2o_RT(rho2,T2) - S
!          print*, " Sees", rho1,S1+S,rho2,S2+S,S, T1, T2, T_adia_PS
       end do findInterval
       delta=100.
!       do while(abs(delta)>1.e-5)
!          T_test=t1+(t2-t1)/2.d0
!          rho_test=rho_ice_pt(P,T_test)
!          S_test=s_h2o_RT(rho_test,T_test)
!           rho1=rho_ice_pt(P,T1)
!         rho2=rho_ice_pt(P,T2)
!         S1 = S_h2o_RT(rho1,T1)
!         S2 = S_h2o_RT(rho2,T2)
!         delta=t1-t2
!          if(s1>=s.and.s>s_test) then
!             t2=t_test
!          elseif(s<s_test.and.s>=s2) then
!             t1=t_test
!          else
!             print*, " problem s not bracketed", s, s1, s2, s_test
!          endif
!          print*, " rho1,s,s1,t1",rho1,S,S1,T1
!       enddo
!       T_adia_ps=t_test
       T_adia_PS = nr2_1(S_adia_PT,dSdT_adia_PT,P,T1,T2,S)

!        print*, " Sees", rho1,S1,rho2,S2,S, T1, T2, T_adia_PS
    end function T_adia_PS

    function S_adia_PT(T,P)
      real(dp),intent(in) :: P,T
      real(dp)            :: s_adia_PT
      real(dp)            :: rho
      S_adia_PT=0.d0
      rho=rho_ice_pt(P,T)
      S_adia_PT=s_h2o_RT(rho,t)
    end function S_adia_PT

    function dSdt_adia_PT(T,P)
      real(dp) :: dsdt_adia_pt
      real(dp), intent(in) :: P, T
      real(dp) :: dt,rho
      dt=0.01
      dsdt_adia_pt=(s_adia_pt(t+dt/2.d0,p)-s_adia_pt(t-dt/2.d0,p))/dt
!      rho=rho_ice_pt(rho,t)
!      dsdt_adia_pt=dsdt_kerh2o(rho,t)
     end function  dsdt_adia_pt

    !!============================================================
 !> @brief computes the adiabatic gradient in the enveloppe from P,S,y
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @param[in] yhe mass fraction of helium
 !! @return (dT/dP)_S in
 function dTdP_env_PT_S(P,T,yhe)
  real(dp), intent(in) :: P,T,yhe
  real(dp)             :: dTdP_env_PT_S
  dTdP_env_PT_S = 0.d0
  if ( parameters%envEOS == 'scvh' .or. parameters%envEOS == 'ker') then
    dTdP_env_PT_S = - dSdP_env_PT(P,T,yhe) / dSdT_env_PT(P,T,yhe)
  else if ( parameters%envEOS == 'poly' ) then
    dTdP_env_PT_S = 1.  ! abiabatic gradient is a dummy variable for the polytrope
  else
    print*, 'Error : unknown enveloppe EOS in eosmod: dTdP_env_PT_S' ; stop
  end if
 end function dTdP_env_PT_S


 !!============================================================
 !> @brief computes S in the enveloppe from P,T,y
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @param[in] yhe mass fraction of helium
 !! @return entropy of the H/He mixture in erg/K/g
 function S_env_PT(P,T,yhe)
  real(dp), intent(in) :: P,T,yhe
  real(dp)             :: S_env_PT
  S_env_PT = 0.d0
  if ( parameters%envEOS == 'scvh' ) then
    S_env_PT = S_scvh_PT(P/dynecm2_to_bar,T,yhe)
  else if ( parameters%envEOS == 'ker') then
    S_env_PT = S_KER_PT(P/GPa_to_bar,T,yhe)
  else if ( parameters%envEOS == 'poly' ) then
    S_env_PT = 1.0d0   ! This is dummy in the case of a polytrope
  else
    print*, 'Error : unknown enveloppe EOS in eosmod:S_env_PT' ; stop
  end if
 end function S_env_PT

 !!============================================================
 !> @brief computes (dS/dT)_P in the enveloppe from P,T,y
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @param[in] yhe mass fraction of helium
 !! @return derivative of the entropy w respect to T at fixed P of the H/He mixture in erg/K**2/g
 function dSdT_env_PT(P,T,yhe) ! at fixed pressure
  real(dp), intent(in) :: P,T,yhe
  real(dp)             :: dSdT_env_PT
  dSdT_env_PT = 0.d0
  if ( parameters%envEOS == 'scvh' ) then
    dSdT_env_PT = dSdT_scvh_PT_P(P/dynecm2_to_bar,T,yhe)
  else if (parameters%envEOS == 'ker') then
    dSdT_env_PT = dSdT_ker_PT_P(P,T,yhe)
  else if ( parameters%envEOS == 'poly' ) then
    dSdT_env_PT = 1.0d0   ! This is dummy in the case of a polytrope
  else
    print*, 'Error : unknown enveloppe EOS in eosmod:dSdT_env_PT' ; stop
  end if
 end function dSdT_env_PT

 !!============================================================
 !> @brief computes (dS/dP)_T in the enveloppe from P,T,y
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @param[in] yhe mass fraction of helium
 !! @return derivative of the entropy w respect to P at fixed T of the H/He mixture in erg/K/g/dyne/cm**2
 function dSdP_env_PT(P,T,yhe) ! at fixed temperature
  real(dp), intent(in) :: P,T,yhe
  real(dp)             :: dSdP_env_PT
  dSdP_env_PT = 0.d0
  if ( parameters%envEOS == 'scvh' ) then
    dSdP_env_PT = dSdP_scvh_PT_T(P/dynecm2_to_bar,T,yhe)
  else if ( parameters%envEOS == 'ker' ) then
    dSdP_env_PT = dSdP_KER_PT_T(P,T,yhe)
  else if ( parameters%envEOS == 'poly' ) then
    dSdP_env_PT = 1.0d0   ! This is dummy in the case of a polytrope
  else
    print*, 'Error : unknown enveloppe EOS in eosmod:dSdT_env_PT' ; stop
  end if
 end function dSdP_env_PT

 !> @}


 !!============================================================
 !> @defgroup IceEOS
 !! @{

 !!============================================================
 !> @brief computes P in the ice layer from rho,T
 !! @param[in] rho density in g/cc
 !! @param[in] T temperature in K
 !! @return pressure of the ice layer in bar
 function P_ice_RT(rho,T)
  real(dp) ,intent(in) :: rho,T
  real(dp)             :: P_ice_RT

  if ( parameters%iceEOS == 'hm' ) then

    P_ice_RT = rho**3.719 * exp(-2.756 - 0.271*rho + 0.00701*rho*rho) * Mbar_to_bar
  else if ( parameters%iceEOS == 'aneos_ice' ) then
    P_ice_RT = P_an(rho,T,'ice')
  else if ( parameters%iceEOS == 'poly' ) then
    P_ice_RT = c_poly*rho**2
  elseif ( parameters%iceEOS == 'ker_h2o') then
    P_ice_RT = p_h2O_RT(rho,T)
  else
    print*, 'Error : unknown ice EOS in eosmod:P_ice_RT' ; stop
  end if
 end function P_ice_RT

 !!============================================================
 !> @brief computes (dP/drho)_T in the ice layer from rho,T
 !! @param[in] rho density in g/cc
 !! @param[in] T temperature in K
 !! @return derivative of pressure w respect to rho at fixed T of the ice layer in bar/(g/cc)
 function dPdrho_ice_RT(rho,T)
  real(dp) ,intent(in) :: rho,T
  real(dp) :: dPdrho_ice_RT
  if ( parameters%iceEOS == 'hm' ) then
    dPdrho_ice_RT = ( 3.719d0 * rho**2.719d0 + rho**3.719d0 * &
      & ( -0.271d0 + 2.0d0*0.00701d0*rho ) ) * &
      &  exp ( -2.756d0 - 0.271d0*rho + 0.00701d0*rho**2 ) * Mbar_to_bar
  else if ( parameters%iceEOS == 'aneos_ice' ) then
    dPdrho_ice_RT = dPdrho_an(rho,T,'ice')
  else if ( parameters%iceEOS == 'poly' ) then
    dPdrho_ice_RT = 2.d0 * c_poly*rho
  else if( parameters%iceEOS == 'ker_h2o') then
    dPDrho_ice_RT = dPdrho_kerh2o(rho,T)
  else
    print*, 'Error : unknown ice EOS in eosmod:dPdrho_ice_RT' ; stop
  end if
 end function dPdrho_ice_RT

 !!============================================================
 !> @brief computes S in the ice layer from rho,T
 !! @param[in] rho density in g/cc
 !! @param[in] T temperature in K
 !! @return entropy of the ice layer in erg/K/g
 function S_ice_RT(rho,T)
  real(dp) ,intent(in) :: rho,T
  real(dp) :: S_ice_RT
  if ( parameters%iceEOS == 'hm' ) then
    S_ice_RT = 0.d0
  else if ( parameters%iceEOS == 'aneos_ice' ) then
    S_ice_RT = S_an(rho,T,'ice') * kJ_to_erg
  elseif ( parameters%iceEOS == 'ker_h2o') then
    s_ice_rt = S_h2o_rt(rho,T)
  else
    print*, 'Error : unknown ice EOS in eosmod:S_ice_RT' ; stop
  end if
 end function S_ice_RT

 !!============================================================
 !> @brief computes (dS/dT)_rho in the ice layer from rho,T
 !! @param[in] rho density in g/cc
 !! @param[in] T temperature in K
 !! @return derivative of the entropy with respect to T at fixed rho of the ice layer in erg/K**2/g
 function dSdT_ice_RT(rho,T)
  real(dp) ,intent(in) :: rho,T
  real(dp) :: dSdT_ice_RT
  if ( parameters%iceEOS == 'hm' ) then
    dSdT_ice_RT = 0.d0
  else if ( parameters%iceEOS == 'aneos_ice' ) then
    dSdT_ice_RT = dSdT_an(rho,T,'ice') * kJ_to_erg
  elseif ( parameters%iceEOS == 'ker_h2o') then
    dSdT_ice_RT = dSdT_kerh2o(rho,T)
  else
    print*, 'Error : unknown ice EOS in eosmod:dSdT_ice_PT' ; stop
  end if
 end function dSdT_ice_RT

 !!============================================================
 !> @brief computes rho in the ice layer from P,T
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @return density of the ice layer in g/cc
 function rho_ice_PT(P,T)
  real(dp), intent(in) :: P,T
  real(dp)             :: rho_ice_PT
  rho_ice_PT = 0.d0
  if ( parameters%iceEOS == 'poly' ) then
    rho_ice_PT = sqrt(P/c_poly)
  elseif( parameters%iceEOS == 'ker_h2o') then
    rho_ice_PT = nr2_1(P_ice_RT, dPdrho_ice_RT, T, 1.2d-5, 1000.d0, P)
  else
    rho_ice_PT = nr2_1(P_ice_RT, dPdrho_ice_RT, T, 1.0d0, 100.0d0, P)
    !print *, " rho_ice", rho_ice_pt, p, t
  end if
 end function rho_ice_PT

 !> @}



 function P_poly_RT(rho,T)
  real(dp),intent(in) :: rho,T
  real(dp) :: P_poly_RT

  P_poly_RT = c_poly * rho**2
 end function P_poly_RT


 function dpdrho_poly(rho,T)
  real(dp),intent(in) :: rho,T
  real(dp) :: dpdrho_poly

  dpdrho_poly = 2.d0 * c_poly * rho
 end function dpdrho_poly


 function P_hm_RT(rho,T)
  real(dp),intent(in) :: rho,T
  real(dp) :: P_hm_RT

  P_hm_RT = rho**4.406d0 * &
    & exp(-6.579d0 - 0.176d0*rho + 0.00202d0 * rho**2) * Mbar_to_bar
 end function P_hm_RT


 function dPdrho_hm_RT(rho,T)
  real(dp),intent(in) :: rho,T
  real(dp) :: dPdrho_hm_RT

  dPdrho_hm_RT = ( 4.406d0*rho**3.406d0 + rho**4.406d0 * ( -0.176d0 + 2.0d0*0.00202d0*rho ) ) &
    * exp(-6.579d0 - 0.176d0*rho + 0.00202d0*rho**2) * Mbar_to_bar
 end function dPdrho_hm_RT


 !!============================================================
 !> @brief computes rho in the rocky core from P,T
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @return density of the core in g/cc
 function rho_core_PT(P,T)
  real(dp), intent(in) :: P,T
  real(dp)             :: rho_core_PT
  rho_core_PT = 0.d0
  if ( parameters%coreEOS == 'poly' ) then
    rho_core_PT = sqrt(P/c_poly)
  elseif( parameters%coreEOS == 'ker_h2o') then
    rho_core_PT = nr2_1(PRT_core%P, PRT_core%dPdrho, T, 1.2d-5, 1000.d0, P)
  else
    rho_core_PT = nr2_1(PRT_core%P, PRT_core%dPdrho, T, 1.0d0, 100.0d0, P)
!          print*, "p_core", P,T,p_core_rt(1.d0,T),p_core_rt(100.d0,T)
  end if
 end function rho_core_PT

 !====new add

 !> @}


 !!============================================================
 !> @brief computes rho in the rocky core from P,T
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @return density of the core in g/cc
 function rho_core1_PT(P,T)
  real(dp), intent(in) :: P,T
  real(dp)             :: rho_core1_PT
  rho_core1_PT = 0.d0
  if ( parameters%core1EOS == 'poly' ) then
    rho_core1_PT = sqrt(P/c_poly)
  else
    rho_core1_PT = nr2_1(PRT_core1%P, PRT_core1%dPdrho, T,1.d0,100.0d0, P)
   ! rho_core1_PT = nr2_1(P_core1_RT, dPdrho_core1_RT, T,4.005d0,19.0d0,P)
         print*, "rho_core1_PT", rho_core1_pt,P,T
  end if
 end function rho_core1_PT


 !!============================================================
 !> @brief computes rho in the rocky core from P,T
 !! @param[in] P pressure in bar
 !! @param[in] T temperature in K
 !! @return density of the core in g/cc
 function rho_core2_PT(P,T)
  real(dp), intent(in) :: P,T
  real(dp)             :: rho_core2_PT
  rho_core2_PT = 0.d0
  if ( parameters%core2EOS == 'poly' ) then
    rho_core2_PT = sqrt(P/c_poly)
  else
    rho_core2_PT = nr2_1(PRT_core2%P, PRT_core2%dPdrho, T,1.0d0, 100.0d0, P)
    !rho_core2_PT = nr2_1(P_core2_RT, dPdrho_core2_RT, T,1.2d-5, 1000.0d0, P)
         print*," rho_core2_pt", rho_core2_pt,p,t
  end if
 end function rho_core2_PT

 !> @}

end module eosmod
