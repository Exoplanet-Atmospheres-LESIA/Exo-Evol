module ker_h_he

    use kerley_h   ! energies in eV/partic ; pressure in GPa
    use kerley_he   ! energies in Ha/partic ; pressure in GPa
    implicit none

    private :: k_B_ev
    public :: s_ker_pt,rho_ker_pt,drhodp_ker_pt_s,dsdp_ker_pt_t,dsdt_ker_pt_p
    double precision, save :: k_B_ev = 8.6173324d-5
    double precision, save :: Ha_ev = 27.2114
    double precision, save :: Na = 6.022d23
    
    contains

      subroutine init_ker
        call KERLEY_INIT_h()
        call KERLEY_INIT_he()
      end subroutine init_ker
      
     
      double precision function s_ker_pt(P,T,y)
      double precision, intent(in) :: P, T, y
      double precision :: rho_H, rho_He, Smix
      ! Smix not currently used
      ! Entropy converted into cgs (1-0.75*y) -> Number of particles in one gram of H/He mixture
  
        rho_H  = find_rho_H(T,P)
        rho_He = find_rho_He(T,P)
      ! ideal mixing entropy in k_B / ion

	Smix = -(((4-4*y)/(4-3*y))*log((4-4*y)/(4-3*y)) + y/(4-3*y)*log(y/(4-3*y)))
        S_ker_pt = (S_H_rt(rho_H,T) * (4-4*y)/(4-3*y) + S_He_RT(rho_He,T) * y/(4-3*y) + Smix*k_B_ev)
        
      end function s_ker_pt

      double precision function rho_ker_pt(P,T,y)
      double precision, intent(in) :: P,T,y
      double precision :: rho_H, rho_He
        rho_H  = find_rho_H(T,P)
        rho_He = find_rho_He(T,P)
        rho_ker_pt = rho_H * rho_He / ( y*rho_H + (1-y)*rho_He )   
      end function rho_ker_pt

      double precision function drhodp_ker_pt_s(P,T,y)
      double precision, intent(in) :: P, T, y 
      drhodp_ker_pt_s=1.d0
!      dRHOdP_SCVH_PT_S = rho/p * ( thermo_sc(8) +  thermo_sc(7) * thermo_sc(11) ) 
      end function drhodp_ker_pt_s
      
!     (dS/dP)_T (P,T)
      double precision function dsdp_ker_pt_t(P,T,y)
      double precision, intent(in) :: P, T, y

      dsdp_ker_pt_t=1.d0
!      dSdP_SCVH_PT_T = S/T * thermo_sc(10)
      end function dsdp_ker_pt_t  

!     (dS/dT)_P (P,T)
      double precision function dsdt_ker_pt_p(P,T,y)
      double precision, intent(in) :: P, T, y 
      double precision :: dt
      dt=1.d0

      dsdt_ker_pt_p=(s_ker_pt(p,t+dt,y)-s_ker_pt(p,t,y))/dt
      end function  dsdt_ker_pt_p
            
end module ker_h_he
