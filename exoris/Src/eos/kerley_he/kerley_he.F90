

!---------------------------------------------------------!
!---------------------- Kerley He EOS --------------------!
!-------- Pressure in bar, everything else in CGS --------!
!---- specific extesive quantities (energy entropy...) ---!
!-------------- adapted by A. Licari (2013) --------------!
!---------------------------------------------------------!

module kerley_he

!  USE nrtype ; USE nrutil, ONLY : nrerror,swap
!  USE nr, ONLY : splie2, splin2
 use m_constants, only : open_with_priority
 use m_splines

  IMPLICIT NONE

  private
  public :: KERLEY_INIT_he, P_He_RT, E_He_rt, S_He_rt, find_rho_He

  INTEGER, PARAMETER :: dims = 3
  DOUBLE PRECISION, parameter :: eV_Ha = 3.674932453d-2,&
       m_H = 1.6726d-24,&
       m_He = 6.644656d-24,&
       dynesc_HaA3 = 2.293712569d-14,&
       kbar_GPa = 1.0d-1,&
       GPa_HaA3 = 2.293712569d-4,&
       conv_MJ_Ha = 2.293712569d23

  INTEGER :: j
  INTEGER :: i, rhoint, tempint, NRHO, NTEMP, first_lines
  DOUBLE PRECISION :: NRHOfl, NTEMPfl, rho1,rho2,rho3
  DOUBLE PRECISION, DIMENSION(:), allocatable :: rho_tab, temp_tab
  DOUBLE PRECISION, DIMENSION(:,:), allocatable :: pressure, energy, entropy
  DOUBLE PRECISION, DIMENSION(:,:), allocatable :: pressureder, energyder, entropyder
  DOUBLE PRECISION, save :: m_part
  CHARACTER (len=8), save :: name
  logical :: exist

contains

  SUBROUTINE KERLEY_INIT_He


   ! Ici : EOS He
!   call open_with_priority(unit=30,filename='a5764.dat')
    call open_with_priority(unit=30,filename='He_abinitio_FS.dat')

   m_part = m_He
    name= 'He'
    first_lines =7

    DO i=1,first_lines
       READ(30,*)
    ENDDO

    READ(30, "(5e16.4)") NRHOfl, NTEMPfl, rho1, rho2, rho3
    NRHO = int(NRHOfl) ; NTEMP = int(NTEMPfl)
    ALLOCATE(rho_tab(NRHO)) ;  ALLOCATE(temp_tab(NTEMP))
    ALLOCATE(pressure(NRHO,NTEMP)) ; ALLOCATE(energy(NRHO,NTEMP)) ;  ALLOCATE(entropy(NRHO,NTEMP))
    ALLOCATE(pressureder(NRHO,NTEMP)) ; ALLOCATE(energyder(NRHO,NTEMP)) ; ALLOCATE(entropyder(NRHO,NTEMP))

    ! lecture du fichier de Kerley
    rho_tab(1) = rho1 ; rho_tab(2) = rho2 ; rho_tab(3) = rho3
    rhoint = 4
    DO WHILE (rhoint<=NRHO)
       IF (rhoint+4 <= NRHO) THEN
          READ(30, "(5e16.4)") rho_tab(rhoint:rhoint+4)
       ELSE
          READ(30, "(5e16.4)") rho_tab(rhoint:NRHO), temp_tab(1:4-(NRHO-rhoint))
          tempint = 4-(NRHO-rhoint)+1
       ENDIF
       rhoint = rhoint + 5
    ENDDO

    DO WHILE (tempint<=NTEMP)
       IF (tempint+4 <= NTEMP) THEN
          READ(30, "(5e16.4)") temp_tab(tempint:tempint+4)
       ELSE
          READ(30, "(5e16.4)") temp_tab(tempint:NTEMP), pressure(1:4-(NTEMP-tempint),1)
          rhoint = 4-(NTEMP-tempint)+1
       ENDIF
       tempint = tempint + 5
    ENDDO

    tempint = 1
    DO WHILE (tempint<=NTEMP)
       DO WHILE (rhoint<=NRHO)
          IF (rhoint+4 <= NRHO) THEN
             READ(30, "(5e16.4)") pressure(rhoint:rhoint+4, tempint)
          ELSE
             IF (tempint /= NTEMP) THEN
                READ(30, "(5e16.4)") pressure(rhoint:NRHO, tempint), pressure(1:4-(NRHO-rhoint),tempint+1)
             ELSE
                READ(30, "(5e16.4)") pressure(rhoint:NRHO, tempint), energy(1:4-(NRHO-rhoint),1)
             ENDIF
          ENDIF
          rhoint = rhoint + 5
       ENDDO
       rhoint = 4-(NRHO-(rhoint-5))+1
       tempint = tempint+1
    ENDDO

    tempint = 1
    DO WHILE (tempint<=NTEMP)
       DO WHILE (rhoint<=NRHO)
          IF (rhoint+4 <= NRHO) THEN
             READ(30, "(5e16.4)") energy(rhoint:rhoint+4, tempint)
          ELSE
             IF (tempint /= NTEMP) THEN
                READ(30, "(5e16.4)") energy(rhoint:NRHO, tempint), energy(1:4-(NRHO-rhoint),tempint+1)
             ELSE
                READ(30, "(5e16.4)") energy(rhoint:NRHO, tempint), entropy(1:4-(NRHO-rhoint),1)
                write(67,*) entropy(1:4-(NRHO-rhoint),1)
             ENDIF
          ENDIF
          rhoint = rhoint + 5
       ENDDO
       rhoint = 4-(NRHO-(rhoint-5))+1
       tempint = tempint+1
    ENDDO

    tempint = 1
    DO WHILE (tempint<=NTEMP)
       DO WHILE (rhoint<=NRHO)
          IF (rhoint+4 <= NRHO) THEN
             READ(30, "(5e16.4)") entropy(rhoint:rhoint+4, tempint)
             
          ELSE
             IF (tempint /= NTEMP) THEN
                READ(30, "(5e16.4)") entropy(rhoint:NRHO, tempint), entropy(1:4-(NRHO-rhoint),tempint+1)
             ELSE
                READ(30, *) entropy(rhoint:NRHO, tempint)
             ENDIF
          ENDIF
          rhoint = rhoint + 5
       ENDDO
       rhoint = 4-(NRHO-(rhoint-5))+1
       tempint = tempint+1
    ENDDO
    close(30)

    !initialisation des splines
    CALL splie2(rho_tab,temp_tab,pressure,nrho,ntemp,pressureder)
    CALL splie2(rho_tab,temp_tab,energy,nrho,ntemp,energyder)
    CALL splie2(rho_tab,temp_tab,entropy,nrho,ntemp,entropyder)
    ! print *, 'Initialisation des splines bicubiques terminees'
!     do i=1,ntemp
!       write(93,*) ' '
!       write(93,*) '# ntemp',temp_tab(i)
!       do j=1,nrho
!          write(93,*) rho_tab(j),pressure(j,i),entropy(j,i)
!       enddo
!    enddo

  END SUBROUTINE KERLEY_INIT_He

   FUNCTION P_He_rt(rho,T)
    double precision, intent(in) :: rho,T
    double precision :: p_he_rt
    call splin2(rho_tab,temp_tab,pressure,pressureder,nrho,ntemp,rho,T,P_He_RT)
  END FUNCTION P_He_rt

  double precision FUNCTION E_He_RT(rho,T)
    double precision, intent(in) :: rho,T
    call splin2(rho_tab,temp_tab,energy,energyder,nrho,ntemp,rho,T,E_he_RT)
    E_HE_RT=E_HE_RT*10.d0**10
  END FUNCTION E_He_RT

  double precision FUNCTION S_He_RT(rho,T)
    double precision, intent(in) :: rho,T
!    write(97,*) 'call splin'
    call splin2(rho_tab,temp_tab,entropy,entropyder,nrho,ntemp,rho,T,S_he_rt)
    S_he_rt=S_he_rt*10.d0**10
!    write(97,*) 'in SHe',rho,T,S_He_RT
  END FUNCTION S_He_RT

  !#######################################################################
  FUNCTION find_rho_He(T,P) ! ,rho_tab,temp_tab,pressure,pressureder)
    IMPLICIT NONE

    DOUBLE PRECISION, intent(in) :: T,P
    !    DOUBLE PRECISION, DIMENSION(:), intent(in) :: rho_tab, temp_tab
    !    DOUBLE PRECISION, DIMENSION(:,:), intent(in) :: pressure
    !    DOUBLE PRECISION, DIMENSION(:,:), intent(in) :: pressureder
    DOUBLE PRECISION :: find_rho_He

    !    INTEGER, PARAMETER :: dims = 3
    INTEGER :: nchange,i
    DOUBLE PRECISION :: rho_deb, rho_fin, f_deb, f_fin, acc, rho
    DOUBLE PRECISION, dimension(dims) :: rho_deb_tab, rho_fin_tab

    rho = 0.0d0
    f_fin = func2(T, rho, P, rho_tab,temp_tab,pressure,pressureder)
    nchange = 0
    DO WHILE (nchange .lt. 1)
       rho_deb = rho
       f_deb = f_fin
       rho = rho+0.1d0

       f_fin = func2(T, rho, P, rho_tab,temp_tab,pressure,pressureder)
!           print *, T, f_fin
       IF (f_fin * f_deb .le. 0.0d0) THEN
          nchange = nchange + 1
          rho_deb_tab(nchange) = rho_deb
          rho_fin_tab(nchange) = rho
       ENDIF
    ENDDO

    IF (nchange ==0) THEN
       print *, 'la il y a un probleme car pas de changement de signe'
       print *, f_fin, f_deb
    ELSE
       DO i = 1, nchange
          rho_deb = rho_deb_tab(i)
          rho_fin = rho_fin_tab(i)
          acc = 1.0d-6
          rho = rtflsp2(rho_deb, rho_fin, acc,T,P,rho_tab,temp_tab,pressure,pressureder)
       ENDDO
    ENDIF
    find_rho_He = rho
    RETURN

  END FUNCTION find_rho_He
  !#######################################################################
  FUNCTION func2(T,x,P,rho_tab,temp_tab,pressure,pressureder)
!    USE nr, ONLY : splin2
    use m_splines
    IMPLICIT NONE
    INTEGER :: nrho,ntemp
    DOUBLE PRECISION, intent(in) :: T,P,x
    DOUBLE PRECISION, DIMENSION(:), intent(in) :: rho_tab, temp_tab
    DOUBLE PRECISION, DIMENSION(:,:), intent(in) :: pressure
    DOUBLE PRECISION, DIMENSION(:,:), intent(in) :: pressureder
    DOUBLE PRECISION :: func2,splin
    nrho=size(rho_tab);ntemp=size(temp_tab)
    call splin2(rho_tab,temp_tab,pressure,pressureder,nrho,ntemp,x,T,splin)
    func2 = P - splin
    RETURN
  END FUNCTION func2

  FUNCTION rtflsp2(x1,x2,xacc,T,P, rho_tab,temp_tab,pressure,pressureder)
!    USE nrtype; USE nrutil, ONLY : nrerror,swap
    use m_splines
    IMPLICIT NONE

    DOUBLE PRECISION, INTENT(IN) :: x1,x2,xacc,T,P
    DOUBLE PRECISION, DIMENSION(:), intent(in) :: rho_tab, temp_tab
    DOUBLE PRECISION, DIMENSION(:,:), intent(in) :: pressure
    DOUBLE PRECISION, DIMENSION(:,:), intent(in) :: pressureder
    DOUBLE PRECISION :: rtflsp2

    INTEGER, PARAMETER :: MAXIT=30
    INTEGER :: j
    DOUBLE PRECISION :: del,dx,f,fh,fl,xh,xl,dum
    fl=func2(T,x1,P,rho_tab,temp_tab,pressure,pressureder)
    fh=func2(T,x2,P,rho_tab,temp_tab,pressure,pressureder)
    if ((fl > 0.0 .and. fh > 0.0) .or. &
         (fl < 0.0 .and. fh < 0.0)) then
         print*, 'rtflsp2: root must be bracketed between arguments'
    endif
    if (fl < 0.0) then
       xl=x1
       xh=x2
    else
       xl=x2
       xh=x1
!       call swap(fl,fh)
       dum=xl
       xl=xh
       xh=dum
    end if
    dx=xh-xl
    do j=1,MAXIT
       rtflsp2=xl+dx*fl/(fl-fh)
       f=func2(T,rtflsp2,P,rho_tab,temp_tab,pressure,pressureder)
       if (f < 0.0) then
          del=xl-rtflsp2
          xl=rtflsp2
          fl=f
       else
          del=xh-rtflsp2
          xh=rtflsp2
          fh=f
       end if
       dx=xh-xl
       if (abs(del) < xacc .or. f == 0.0) RETURN
    end do
    print*, 'rtflsp2 exceed maximum iterations'
    STOP
  END FUNCTION rtflsp2



end module kerley_he
