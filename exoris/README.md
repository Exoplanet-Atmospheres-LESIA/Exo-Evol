

# Horedt5 : computing giants' planets structure
###                         Author : Adrien Licari version 2015
###                                  Stephane Mazevet version depuis 2016

## Purpose

The code performs a computation of a rotating planet model using the
Zharkov-Trubitsyn theory of figures.
The planet is assumed to have an isothermal core divided between a rocky core
and an ice layer, and an isentropic H/He enveloppe.
Given a mass for the planet, three parameters can be adjusted : the mass fraction
of the core over the total mass (core), the mass fraction of rocks into the core
(rock), and the Helium mass fraction in the enveloppe (yhe).
Two modes are possible :
- simply compute the model ; Req is taken as a first guess, and J2 and J4 are dummy
  inputs
- perform a minimization (simplex method) or a root finding (3D-Newton-Raphson
  method) in order to find the parameters (core,rock,yhe) that best match a
  given set of measurements Req, J2, J4

## Configuration and Compilation
The compilation has to be done in a directory, the "build"
directory, where all the object file will be created.
When the compilation is successfully terminated then executable
"horedt5" and its libraries horedt5_data will be installed in
the directory, the "prefix" directory.
"build" and "prefix" directories, and other variables are defined
during the configuration.

### Configuration
- In the repository folder (~EosRIS) type :
  `./configure OPTIONS`
- To have the list of options and the usage type :
  `./configure -h`

**NB**  the folder where to install the binary (default ~EosRIS/bin) has to exists

### Compilation
- Enter in the "build" directory (default ~EosRIS/obj)
- To compile the code :
  `make`
- make the documentation :
  `make doc`
- Others :
  `make help`
- Installation in the "prefix" directory (default ~EosRIS/bin)
  `make install`


## Running the code

The execution directory must parameters.txt file

The parameters.txt file's structure is :
- Number of mesh grid intervals
- Mass (in g), Req (in cm), J2, J4
- core, rock, yhe
- rotation period (in hours)
- surface temperature (in K), surface pressure (in bar)
- enveloppe eos, ice layer eos, core eos
- minimization (0=no,other=yes), useSimplex(0=no,other=yes),
- verbose(0=no,other=yes)

### Example for Jupiter ithout minimization :
~~~~
300
1.8986d30 7.1492d9 0.14697e-2 -5.84e-4
0.0125 0.5 0.378
9.924
165.0 1.0
scvh hm hm
0 0 1
~~~~

The available EOS are :
- enveloppe : scvh [1], poly, ker_h_he (H, He ab initio) [3]
- ice layer : hm [2], poly, aneos_ice (ANEOS ice EOS), ker_h2o (H2O ab initio) [4]
- rock core : hm [2], poly, aneos_dunite (ANEOS Dunite EOS),
  aneos_serpentine (ANEOS Serpentine EOS), aneos_iron (ANEOS Iron EOS), ker_fe, ker_mgsio3 [5]
poly being a polytrope of index n=1 and constant defined such as P(1e-2 g/cc)=1bar


## References
[1] Saumon, D.; Chabrier, G.; van Horn, H. M. : Astrophysical Journal Supplement v.99, 1995
[2] Hubbard, W. B.; Marley, M. S. : Bulletin of the American Astronomical Society, Vol. 20, 1988
[3] S. Mazevet et al. A&A 2021
[4] S. Mazevet et al. A&A 2019
[5] S. Mazevet et al. A&A 2022

