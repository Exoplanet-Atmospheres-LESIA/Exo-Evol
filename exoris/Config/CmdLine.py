#!/usr/bin/python
###############################################
# Name   : CmdLine.py
# File   : Hades/tools/Analyser/CmdLine.py
# Project: Hades
# Author : MMancini
# Date   : 12/05/2013
# Function:
#  Analyse the Argument of the command line for Hades analyser
###############################################

__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '11/10/2013'
__version__   = '1.0'

"""Analyse the Argument of the command line for Hades analyser"""

import argparse
import local_setting as locSet

# Error function
def prt_error(error_str):
    print("!!! Error: %s \n...exiting..." %error_str)
    sys.exit(0)



def cmd_line():
    """Command line parser"""
    parser = argparse.ArgumentParser(
        description='Configure for the {0} project'.format(locSet.project))


    parser.add_argument('--no-openmp',dest='haveomp',action='store_false',
                        help='Desactive OpenMP. (default actived)')

    parser.add_argument('--debug',dest='havedebug',action='store_true',
                        help='Compile with HAVE_DEBUG. (default no)')

    # parser.add_argument('--timing',dest='havetiming',action='store_true',
    #                     help='Compile with HAVE_TIMING. (default no)')

    parser.add_argument('--prefix',
                        metavar='INSTALL_PATH',dest='prefix',
                        default=locSet.install,
                        help='Installation path (default {0}).'.format(locSet.install))

    parser.add_argument('--build', metavar='BUILD_PATH',dest='build',
                        default=locSet.build,
                        help='Build path (default {0}).'.format(locSet.build))

    parser.add_argument('--f90',metavar='FORTRAN_COMPILER',dest='f90',default='mpif90',
                        help='Fortan compiler to use (mpif90,mpiifort,ifort,gfortran)')

    parser.add_argument('--fflags',metavar='FFLAGS',dest='fflags',default='',
                        help='Compiler flags.')

    parser.add_argument('--libs', metavar='LIBS',dest='generic_libs',default='',
                        help='Add libraries (generic). Ex: --libs=\'-L$PATH_TO_LIB -lmylib\'')

    parser.add_argument('--incs', metavar='INCLUDE_PATH',dest='generic_incs',default='',
                        help='Add includes path (generic). Ex: --incs=I$PATH_TO_INCS')



    return parser


if __name__=='__main__':
    parser = cmd_line()
    args = parser.parse_args()
    print args
