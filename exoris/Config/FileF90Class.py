#!/user/bin/python3
###############################################
# Name   : FileF90Class.py
# File   : Hades/Config/FileF90Class.py
# Project: Hades
# Author : MMancini
# Date   : 06/09/2013
# Function:
#  Class to detect dependences between fortran modules
###############################################


__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '08/10/2013'
__version__   = '1.0'


class FileF90(object):
    """Class to detect dependences between fortran modules"""
    def __init__(self,fnamepath,haves={}):
        self.fnamepath = fnamepath

        self.name,self.ext = (fnamepath.split('/')[-1]).split('.')
        self.int_name = ''
        self.path = fnamepath.replace(self.name,'')
        self.type = ''
        self.public = 1  # if the module or procedure is public
        self.using = []
        self.procedure = []
        self.variables = []
        self.used = []
        self.called = []
        self.Init_Parse(haves)
        self.Advance_Parse()
        #print self

    def __str__(self):
        string = \
            '\n File name  : '+ self.name +\
            '\n path       : '+ self.path +\
            '\n int name   : '+ self.int_name +\
            '\n type       : '+ self.type +\
            '\n ext        : '+ self.ext
        strused = '\n using      : '
        for ii in self.using:
            strused +=  ii+' '
        string += strused
        strused = '\n procedures : '
        for ii in self.procedure :
            strused +=  ii+' '
        strused = '\n called     : '
        for ii in self.called :
            strused +=  ii+' '
        string += strused

        return string

    def Init_Parse(self,haves={}):
        with open(self.fnamepath) as ff:
            lines = ff.readlines()
        # delete comment and leading white spaces
        lines = list(line.lstrip().strip() for line in lines if not line.lstrip().startswith('!'))

        # delete blank lines and join line with &
        tmp_lines = []
        ii = 0

        control_haves = 0
        while ii < len(lines) :
            # HAVES-------------
            if lines[ii].startswith('#ifdef') :
                ihave = lines[ii].split()[1]
                if ihave in haves.keys() and not haves[ihave] : control_haves += 1
                #print '---', lines[ii],control_haves

            if lines[ii].startswith('#ifndef') :
                ihave = lines[ii].split()[1]
                if ihave in haves.keys() and  haves[ihave] : control_haves += 1
                #print '---', lines[ii],control_haves

            if lines[ii] == '#else'  and control_haves!=0: control_haves -= 1

            if lines[ii] == '#endif' and control_haves!=0:
                control_haves -= 1
                #print '---',lines[ii],control_haves

            if control_haves != 0 :
                ii += 1
                continue
            # -----------------

            # blank line
            if lines[ii] == '' :
                ii += 1
                continue

            newline = lines[ii]

            # continuation line
            continua = 0
            while newline.endswith('&'):
                continua = 1 # tag for continuation
                ii += 1
                newline = newline[:-1]+lines[ii]

            if continua == 1 :
                newline = newline.replace('&','')

            if newline.startswith('if(') or \
                    newline.startswith('if (') or \
                    newline.startswith(' do ') or\
                    newline.startswith('end if') or\
                    newline.startswith('end do') or\
                    newline.startswith('enddo') or\
                    newline.startswith('endif') :
                ii += 1
                continue

            tmp_lines.append(newline)
            ii += 1


        # print lines
        # print tmp_lines

        # get internal name  (by reading the last line containg 'end TYPE name')
        lastline = tmp_lines[-1].split()
        #print self
        #print lastline
        if lastline[0].lower() == 'end' :
            if self.ext == 'f':
                self.int_name = self.name
            else:
                # modern fortran case
                self.type = lastline[1]
                self.int_name = lastline[-1]
        else:
            print 'WARNING : fortran \"end\" not found in file: '+self.fnamepath

        self.lines = tmp_lines
        self.len = len(tmp_lines)
        #print self


    def Advance_Parse(self):

        # get module to load and public
        if self.type == 'module':
            for ll in self.lines:

                llsplit = ll.replace(',',' ').split()
                if llsplit[0] == 'use' :
                    # deleting 'use' and ',only...'
                    self.using.append(llsplit[1].split(',')[0])

                # see if the module is private
                if llsplit[0] in ( 'private','public'):
                    if len(llsplit) == 1:
                        if llsplit=='contains': break
                        self.public = (0 if  llsplit[0] =='private' else 1)
                    else:
                        if llsplit[0] == 'public' and llsplit[1] == '::' :
                            self.procedure += llsplit[2:]
                        if  llsplit[0] =='public::' :
                            self.procedure += llsplit[1:]



        else:
            ii = 0
            while ii < self.len :
                iline = self.lines[ii]
                if iline =='interface':
                    # Extracting old-style interface
                    while self.lines[ii] != 'end interface':
                        if self.lines[ii].split()[0] in ('subroutine','function'):
                            self.using.append(self.lines[ii].split()[1].split('(')[0])
                        ii += 1
                    continue

                llsplit = iline.replace(',',' ').split()
                if llsplit[0] == 'use' :
                    # deleting 'use' and ',only...'
                    self.using.append(llsplit[1].split(',')[0])

                if llsplit[0] in ('subroutine','function') :
                    # deleting 'use' and ',only...'
                    self.procedure.append(llsplit[1].split('(')[0])

                # break on continue
                if iline =='contains': break

                # Essai for calls

                # if llsplit[0] == 'call' :
                #     #print llsplit
                #     called = llsplit[1].split('(')[0]
                #     if called.find('%') == -1 : # unuseful bound calls
                #         self.called.append(called)

                ii += 1

        self.using = list(set(self.using))


if __name__=='__main__':
    a = FileF90('Src/Coupling/Get_Input_Config.F90',{'HAVE_MPI':False})
    # a = FileF90('Src/Radiation/SplinePack/m_bilin_spline.F90')
    # a = FileF90('Src/Coupling/initialization.F90')
    # a = FileF90('Src/Basis/defs_mpitype.F90')
    # a = FileF90('Src/Radiation/RadiationIntegral/multiplicateurs.F90')
    # a = FileF90('Src/Main/hades.F90')
 #   a = FileF90('Src/Radiation/Fermeture/mlagrange.F90')
#os.chdir(realpath)
