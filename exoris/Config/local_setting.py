#!/usr/bin/python
###############################################
# Name   : local_setting.py
# File   : Hades/Config/local_setting.py
# Project: Hades
# Author : MMancini
# Date   : 27/05/2015
# Function:
#  Contains the most possible number of local parameters
#  specific to Hades, in attent to generalize the configuration tool
#  to others software
###############################################

__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '27/05/2015'
__version__   = '1.0'


project       = "EosRIS"
mainexec      = "horedt5"
src           = "Src"
install       = "./bin"
inc_h_dir     = ""
build         = "./obj"
otherexec     = "create_table copy_table"
doxyfile      = "tools/Doxyfile"
manual_target = """

create_table : a2b.exe 3a2b.exe 4a2b.exe 2a2b.exe a2b_ppt.exe #5a2b.exe
\t mkdir -p -p $(BUILD_DIR)/$(MAINEXEC)_data
\t cp $(wildcard $(SRC_DIR)/eos/scvheos/*.a) .
\t cp $(wildcard $(SRC_DIR)/eos/scvheos/*.a2) .
\t for ii in $^; do ./$$ii;done
\t mv *.b *.b2 $(BUILD_DIR)/$(MAINEXEC)_data/.

copy_table : $(wildcard $(SRC_DIR)/eos/*/*.dat)
\t @cp -f $^ $(BUILD_DIR)/$(MAINEXEC)_data/.
"""

manual_install = """
\t cp -f  $(BUILD_DIR)/$(MAINEXEC)_data $(INSTALL_DIR)/.
"""

manual_clean = """
\t rm -f $(BUILD_DIR)/*.exe
\t rm -rf $(BUILD_DIR)/*.a $(BUILD_DIR)/*.a2
\t rm -rf $(BUILD_DIR)/$(MAINEXEC)_data
"""


manual_install = """
\t cp -rf $(BUILD_DIR)/$(MAINEXEC)_data  $(INSTALL_DIR)/.

"""


def create_header(dbuild,dinstall) :
    from string import Template
    from datetime import date
    from os import path


    """Create or manipule the header file."""
    conffile = Template("""
/*${mainexec}.h*/

/*******************************************************************
Configure file for EosRIS
Author: MMancini
Configuration date:  $conf_date

This file is generated automatically by ~EosRIS//Config/local_setting.py
Do not change it manually!
*********************************************************************/

#ifndef HORODET5_H
#  define HORODET5_H

/* compilation flags*/
#  define __DATADIR '${dinstall}/${mainexec}_data'

#endif
"""
    ).substitute({
        'mainexec' : mainexec,
        'conf_date': date.today().__str__(),
        'dinstall' : path.abspath(dinstall)
    })

    with open(dbuild+'/'+mainexec+'.h','w') as f:
        f.write(conffile)
