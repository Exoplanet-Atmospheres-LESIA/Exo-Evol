#!/user/bin/python
###############################################
# Name   : os_find.py
# File   : Hades/Config/os_find.py
# Project: Hades
# Author : MMancini
# Date   : 12/10/2013
# Function:
#  Get os informations

###############################################

__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '12/10/2013'
__version__   = '1.0'

import sys
import platform

class osinfo():
    def __init__(self):
        self.system = platform.system()
        if self.system == 'Darwin':  self.dist ='none'
        if self.system == 'Linux':  self.dist = platform.linux_distribution()[0]

        self.bits = (64 if sys.maxsize >2**32 else 32)

    def __str__(self):
        return self.system+' '+str(self.bits)+'bits ('+self.dist+')'

if __name__=='__main__':
    a = osinfo() 
    print a
