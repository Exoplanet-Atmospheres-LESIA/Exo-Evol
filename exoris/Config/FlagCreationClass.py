#!/user/bin/python
###############################################
# Name   : FlagCreationClass.py
# File   : Hades/Config/FlagCreationClass.py
# Project: Hades
# Author : MMancini
# Date   : 12/10/2013
# Function:
#  Manipulate input values to be adapt to the make file creation

###############################################

__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '12/10/2013'
__version__   = '1.0'

import os
import sys
from which import which
from subprocess  import Popen, PIPE

import local_setting as locSet


from os_find import osinfo


def get_env_var(key) :
    """Get an environment variable."""
    return (os.environ.get(key) if os.environ.has_key(key) else '')

def setPrefix(stringa,prefix):
    """Controls if a str stringa has a prefix, if not it is added to
    the ouput.
    """
    a = stringa.lstrip()
    if not a.startswith(prefix) and  a != '' :
        a = prefix+a
    return a



def getCompiler(compiler_cmd):
    """Return compiler type: gnu, intel,..."""
    fc_compiler = get_env_var('FC')

    cmd = Popen(compiler_cmd+' --version', shell=True, stdout=PIPE,stderr=PIPE)
    std_out,std_err = cmd.communicate()
    if std_err != '':
        raise ValueError("parallel compiler: "+compiler_cmd+" not found !")
    if "GNU" in std_out:
        fc_compiler = "gfortran"
    elif "IFORT" in std_out:
        fc_compiler = "ifort"

    if fc_compiler == '' :
        raise Exception("""
              The fortran underlying MPI was not detected.
              Try to launch the command : export FC=$("""+compiler_cmd+""" -show|awk \'{print $1}\')
              and relaunch configuration.""")


    return fc_compiler

class FlagCreation():

    def __init__(self,args):

        ##################################
        # Initial Assignement
        ##################################
        self.__dict__ = args.__dict__

        # Define command
        self.cmd = sys.argv

        ##################################
        # Detect system
        ##################################
        self.os = osinfo()
        if self.os.system  not in ('Linux','Darwin'):
            raise Exception('Makedependences not run on this system :'+self.os.system)

        ##################################
        # Set directories
        ##################################

        # Determine the position of the package
        self.package = os.path.dirname(os.path.realpath(os.path.dirname(sys.argv[0])))

        # Source Path
        self.src = os.path.realpath(self.package+'/'+locSet.src)
        if not os.path.isdir(self.src) :
            raise Exception('The directory: '+self.src+' does not exist')

        # Get Build Path, Build dir
        self.build = os.path.realpath(self.build)

        # Create the build dir is does not exist or if coincide with package
        if self.build == self.package :
            self.build = self.build+'/'+locSet.build
        if not os.path.isdir(self.build) :
            try :
                os.mkdir(self.build)
                print 'Created build dir: '+self.build
            except:
                raise Exception('Directory :'+self.build+' cannot be created')

        # Installation Path
        if self.prefix == '' : self.prefix = self.build
        if not os.path.isdir(self.prefix) :
            raise Exception('The directory: '+self.prefix+' does not exist')
        self.prefix = os.path.realpath(self.prefix)


        # Change to build dir to create Makefile
        os.chdir(self.build)

        self.package = os.path.relpath(self.package)
        self.src     = os.path.relpath(self.src)
        self.prefix  = os.path.relpath(self.prefix)
        self.build   = os.path.relpath(self.build)

        # Include array
        self.generic_incs = setPrefix(self.generic_incs,'-I')
        self.incs = ['-I'+self.src+'/'+locSet.inc_h_dir ,
                     '-I'+self.build, 
                     self.generic_incs ]


        self.generic_libs = setPrefix(self.generic_libs,'-L')
        self.libs = [ self.generic_libs ]

        ##################################
        # Control on environement variables
        ##################################

        # F90
        # if not self.havempi :
        #     if self.f90 == '' : self.f90 = get_env_var('F90')
        #     # no environement found so leave the variable
        #     if self.f90 == '' : self.f90 = '$(F90)'
        #     self.fc = self.f90
        # else:
        #     if self.f90 == '' : self.f90 = 'mpif90'
        #if self.f90 == '' : self.f90 = 'mpif90'


        # get compiler from
        self.fc = getCompiler(self.f90)

        # FFLAGS
        # first control environement
        if self.fflags == '' : self.fflags = get_env_var('FFLAGS')

        # if no environement found so leave the variable
        if self.fflags == '' : self.fflags = '$(FFLAGS)'


        # Haves
        self.haves = '' # Have for Makefile
        self.ifdef = {} # based on Haves create dict containing ifdef line in F90 file

        haves = []
        if self.havedebug  :
            haves.append(' -DHAVE_DEBUG')
            self.ifdef['HAVE_DEBUG'] = True
        # if self.havetiming  :
        #     haves.append(' -DHAVE_TIMING')
        #     self.ifdef['HAVE_TIMING'] = True
        self.haves = ' '.join(haves)
        if self.haveomp  :
            haves.append(' -DHAVE_OMP')
            self.ifdef['HAVE_OMP'] = True
            self.fflags += ' -fopenmp'
        self.haves = ' '.join(haves)


        # test doxygen
        try:
            self.doxygen = which('doxygen')[0]
        except:
            self.doxygen = '$(DOXYGEN)'

        ##################################
        # Echo resuming on analysed files
        ##################################
        stringo = ''.join(['#' for ii in range(1,80) ])
        stringa = ''.join(['_' for ii in range(1,80) ])
        print
        print stringo
        print ' Configuration for {0:s}:'.format(locSet.project.title())
        print '  System                  =', self.os
        print stringo
        print
        print '  Package path            =', os.path.realpath(self.package)
        print '  Sources path            =', os.path.realpath(self.src)
        print '  Installation path       =', os.path.realpath(self.prefix)
        print '  Build path              =', os.path.realpath(self.build)
        print '  Makefile location       =', os.path.realpath(self.build)
        print
        print stringa
        print ' Using : '
        print stringa
        print '  OpenMP                  = '+('enabled' if(self.haveomp) else 'disabled')
        print
        print stringa
        print ' Dependences : '
        print stringa
        print '  LIBS (other)            = '+self.generic_libs
        print '  INCS (other)            = '+self.generic_incs
        print '  DOXYGEN                 = '+self.doxygen
        print
        print stringa
        print ' Flags : '
        print stringa
        print '  F90                      = '+self.f90
        print '  FFLAGS                   = '+self.fflags
        print '  HAVES                    = '+self.haves
        print
