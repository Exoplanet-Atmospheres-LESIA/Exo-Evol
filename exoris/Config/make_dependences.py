#!/usr/bin/python
###############################################
# Name   : make_dependences.py
# File   : Hades/Config/make_dependences.py
# Project: Hades
# Author : MMancini
# Date   : 06/09/2013
# Function:
#  Script to detect dependences between fortran modules
#  2 steps:
#  a) the files list analysed to file all module,procededure contained
#     in a file

###############################################

__author__    = "Marco Mancini (MM)"
__modifiers__ = ''
__date__      = '08/10/2013'
__version__   = '1.0'

import os
import glob
from string import Template
from subprocess import call
from which import which

# Import Parser for Fortran files
from FileF90Class import FileF90

# Importe Class for the template of the Makefile
from TemplateMakeClass import TemplateMake

# Import for command line
from CmdLine import cmd_line

# Manipulate input
from FlagCreationClass import FlagCreation

# contains infomations concerning the program
import local_setting as locSet

# Parse command line
parser = cmd_line()
args = parser.parse_args()

# Manipulate input
Flags = FlagCreation(args)


############################
# Hades Rules
############################


class incfile():
    def __init__(self,path,name):
        self.name = name
        self.path = path
        files = []
        with open(self.topaph()) as ff:
            for ii in ff.readlines():
                if ii.startswith('#') : continue
                a = ii.strip().split('\\')
                if a == [''] : continue
                files.append((''.join(a)).replace(' ',''))
        self.src = files[1:]

        self.object = ''

    def __str__(self):
        outstr =  ' File    :'+self.topaph()
        outstr += '\n contains:'
        for ii in self.src : outstr += '\n          '+ii
        return outstr

    def topaph(self):
        return os.path.join(self.path,self.name)

    def tolist(self):
        return [ os.path.join(self.path,ii) for ii in self.src ]



def find_filelist(dirname,excludedir={}):
    incfiles = []
    for r,d,f in os.walk(dirname):
        contbreak = False
        for iexcl in excludedir:
            if r.find(iexcl)!= -1 :
                contbreak = True
                break
        if contbreak : continue
        for ifile in f:
            if ifile.endswith("filelist.inc"):
                #incfiles.append(os.path.join(r,files))
                #incfiles.append([r,ifile])
                incfiles.append(incfile(r,ifile))
    #for ii in incfiles : print "-#-",ii
    return incfiles

def find_deps(me,mydict,myset,count):
    """Create set contaning all dependences of a file """
    if len(me.using) == 0 : return

    #control if there are cross reference (DOES NOT WORK WELL)
    if count > len(mydict) :
        raise Exception('Possible crosse reference: '+me.name)
        StopIteration('Possible crosse reference: '+me.name)

    for ii in me.using:
        try:
            myset.add(mydict[ii].name+'.o')
            count += 1
            #print mydict[ii].name,len(myset)
            find_deps(mydict[ii],mydict,myset,count)
        except: pass


# def find(self, key):
#     gen = self._find(key)
#     try:
#         yield gen.next()
#     except StopIteration:
#         raise KeyError(key)
#     for item in gen:
#         yield item


depend_dict = {}    # all files dependences
targets = {}        # targets (program)
nomod_file = {}     # all fashon files (subroutine or function)
listdir = set()

exclude = [] # contains list of excluded directories



for incf in find_filelist(Flags.src,exclude):
    allfile = []
    allfile.extend(incf.tolist())
    try :
        a = '/'.join(incf.tolist()[0].split('/')[:-1])
        listdir.add( a.replace(Flags.src,'$(SRC_DIR)'))
    except:
        pass
    for ii in allfile:
        a = FileF90(ii,Flags.ifdef)
        depend_dict[a.int_name] = a
        if a.type == 'program' : targets[a.int_name] = a
        if a.type not in ('program','module') : nomod_file[a.int_name] = a
        #print depend_dict[a.int_name]


# phony_list contains the phony string
programs_list = ' '.join(targets[ii].name for ii in targets )

# targets_str_list contains
targets_str_list = []
for ii in targets:
    string = targets[ii].name+' : '+targets[ii].name+'.o '
    iiobj = set()
    targets[ii].using.extend([ nomod_file[jj].name for jj in nomod_file])

    # find dependenes
    find_deps(targets[ii],depend_dict,iiobj,0)

    string += ' '.join(list(iiobj))
    if targets[ii].name == locSet.mainexec :
        targets_str_list = [string] + targets_str_list
    else :
        targets_str_list.append(string)

targets_str_total = ''
for ii in targets_str_list:
    targets_str_total += ii
    targets_str_total += '\n\t $(F90) $(FFLAGS) $(INCS) $(HAVES) -o $@ $^ $(LIBS)'
    targets_str_total += '\n\n'

object_str = ''
for ii in depend_dict:
    object_str += "{0} :".format(depend_dict[ii].name+'.o')
    for jj in depend_dict[ii].using:
        try:
            object_str += " {0}".format(depend_dict[jj].name+'.o')
        except:
            pass
    object_str += '\n'


############################
# Create the Makefile
############################

make_struc = TemplateMake(
    project         = locSet.project,
    madeby          = ' '.join(Flags.cmd),
    SRC_DIR         = Flags.src,
    BUILD_DIR       = Flags.build,
    INSTALL_DIR     = Flags.prefix,
    SYSTEM          = Flags.os.__str__(),
    F90             = Flags.f90,
    MAINEXEC        = locSet.mainexec,
    FFLAGS          = Flags.fflags,
    HAVES           = Flags.haves,
    INCS            = ' '.join(Flags.incs),
    LIBS            = ' '.join(Flags.libs),
    PROGS           = programs_list,
    PHONY           = programs_list + ' ' + locSet.otherexec,
    VPATH           = "\t"+(" \\\n\t").join(listdir),
    TARGETS         = targets_str_total,
    OBJECTS         = object_str,
    DOXYGEN         = Flags.doxygen,
    DOXYFILE        = Flags.package+'/'+locSet.doxyfile,
    MANUAL_TARGET   = locSet.manual_target,
    MANUAL_CLEAN    = locSet.manual_clean,
    MANUAL_INSTALL  = locSet.manual_install
    )


make_struc.create()

# Launch bash simple configuration
tmpflag = Flags.fflags.replace("$(FFLAGS)","")
# call(Flags.package+'/Config/create_header.sh '+tmpflag,shell=True)

############################
# Final print
############################
stringo = ''.join(['#' for ii in range(1,80) ])
print( stringo )
print( '  Makefile successfully created.' )
print( '  To build {0:s}:'.format(locSet.mainexec) )
print( '        cd '+os.path.realpath(Flags.build) )
print( '        make -j' )
print( '        make install \n' )
print( '  Note: To build other packages related to {0:s} ({1:s},doc,..), please look at the Makefile help :'
       .format(locSet.project,locSet.otherexec))
print( '        cd '+os.path.realpath(Flags.build) )
print( '        make help' )
print( '')

locSet.create_header(Flags.build,Flags.prefix)

#if __name__=='__main__':
    # a = File_f90('Src/Coupling/m_LoadData.F90')
#os.chdir(realpath)
