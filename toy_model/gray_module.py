"""
Created on Fri Jun 16 11:46:13 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

import numpy as np
import pandas as pd

from scipy.interpolate import interp1d

from nasa_coef_module import *
from exoplanet_data_module import *
from numerical_tools_module import *
from initiation_module import *
from output_module import *
from exorem_module import *
from exoris_module import *

import warnings
warnings.filterwarnings("ignore")

""" This is an incomplete module to compute semi-analytical gray atmospheres 
based on Guillot et al. and Valencia et al. """

def T_profile(P, T_eff, met, g, species=['H2', 'He'], mass_fractions=[1-0.246, 0.246]):
    """
    Computes the temperature profile of a semi-analytical gray atmosphere.

    Args:
        P (array-like): Pressure values.
        T_eff (float): Effective temperature.
        met (float): Metallicity.
        g (float): Surface gravity.
        species (list, optional): List of species in the atmosphere. Defaults to ['H2', 'He'].
        mass_fractions (list, optional): List of mass fractions for the species. Defaults to [1-0.246, 0.246].

    Returns:
        DataFrame: Dataframe containing the temperature profile and related information.

    Examples:
        df = T_profile(P, T_eff, met, g)
        # Returns a dataframe with the temperature profile and related information.
    """

    T = [T_0(T_eff)]
    g = g * np.ones(len(P))
    is_convective = []
    c = np.zeros([11, len(P)])
    tau_ = []
    flag = False
    P_link = np.nan
    T_at_P = np.nan

    if len(species) == 2:
        mass_fractions = [1 - (0.246 + met * 0.0134), 0.246 + met * 0.0134]

    for ii in range(1, len(P)):
        c[:, ii] = coefficients(T[-1])

        if ii > 1:
            adiabat = (np.log(T[-1]) - np.log(T[-2])) / (np.log(P[ii - 1]) - np.log(P[ii - 2]))
            if (adiabat > (get_PT_slope(species, mass_fractions, T[-1]))) and (not flag):
                P_link = P[ii - 1] * 1e-5
                T_at_P = T[ii - 1]
                flag = True

        if flag:
            T.append(profile_adiabatique(T, P, met, c, g[ii], species, mass_fractions))
            is_convective.append(1)
        else:
            T.append(((3 / 4) * T_eff**4 * ((2 / 3) + tau(T, P, met, c, g[ii])))**(1 / 4))
            is_convective.append(0)

        tau_.append(tau(T, P, met, c, g[ii]))

    df = pd.DataFrame()
    df['is_gray'] = [1]
    df['T_at_P'] = [T_at_P]
    df['g_at_P'] = [interp1d(P, g)(1e5)]
    df['P_link'] = [P_link]
    df['temperature'] = [T]
    df['pressure'] = [P]
    df['gravity'] = [g]
    df['T_eff'] = [T_eff]
    df['is_convective'] = [is_convective]

    return df


def profile_adiabatique(T, P, met, c, g, species, mass_fractions):
    """
    Computes the adiabatic temperature profile.

    Args:
        T (array-like): Temperature values.
        P (array-like): Pressure values.
        met (float): Metallicity.
        c (array-like): Coefficients.
        g (float): Surface gravity.
        species (list): List of species in the atmosphere.
        mass_fractions (list): List of mass fractions for the species.

    Returns:
        float: Adiabatic temperature.

    Examples:
        T_adiabat = profile_adiabatique(T, P, met, c, g, species, mass_fractions)
        # Returns the adiabatic temperature.
    """

    T = T[-1] * (P[-1] / P[-2])**(get_PT_slope(species, mass_fractions, T[-1]))
    return T


def T_0(T_int):
    """
    Computes the temperature at the optical depth

    Args:
        T_int (float): Internal temperature.

    Returns:
        float: Temperature at the optical depth.

    Examples:
        T_unity = T_0(T_int)
        # Returns the temperature at the optical depth.
    """

    T = ((3 / 4) * T_int**4 * (2 / 3))**(1 / 4)
    return T


def coefficients(T):
    """
    Computes the coefficients based on the temperature.

    Args:
        T (float): Temperature.

    Returns:
        array-like: Coefficients.

    Examples:
        coeff = coefficients(T)
        # Returns the coefficients based on the temperature.
    """

    c = np.zeros(11)
    c[0] = -37.50
    c[1] = 0.00105
    c[2] = 3.2610
    c[3] = 0.84315
    c[4] = -2.339

    if T <= 800:
        c[5] = -14.051
        c[6] = 3.055
        c[7] = 0.024
        c[8] = 1.877
        c[9] = -0.445
        c[10] = 0.8321

    if T > 800:
        c[5] = 82.241
        c[6] = -55.456
        c[7] = 8.754
        c[8] = 0.7048
        c[9] = -0.0414
        c[10] = 0.8321

    return c


def tau(T, P, met, coeff, g):
    """
    Computes the optical depth.

    Args:
        T (array-like): Temperature values.
        P (array-like): Pressure values.
        met (float): Metallicity.
        coeff (array-like): Coefficients.
        g (float): Surface gravity.

    Returns:
        float: Optical depth.

    Examples:
        optical_depth = tau(T, P, met, coeff, g)
        # Returns the optical depth.
    """

    tau = 0

    for jj in range(0, len(T) - 1):
        c = coeff[:, jj]
        kappa_low = kappa_low_p(T[jj], P[jj], met, c)
        kappa_high = kappa_high_p(T[jj], P[jj], met, c)
        kappa = (kappa_low + kappa_high)
        m = (P[jj + 1] - P[jj]) / g
        tau += kappa * m

    return tau


def kappa(T, P, met):
    """
    Computes the opacity.

    Args:
        T (float): Temperature.
        P (float): Pressure.
        met (float): Metallicity.

    Returns:
        float: Opacity.

    Examples:
        opacity = kappa(T, P, met)
        # Returns the opacity.
    """

    c = coefficients(T)
    kappa_low = kappa_low_p(T, P, met, c)
    kappa_high = kappa_high_p(T, P, met, c)

    return (kappa_low + kappa_high)


def kappa_low_p(T, P, met, c):
    """
    Computes the low pressure opacity.

    Args:
        T (float): Temperature.
        P (float): Pressure.
        met (float): Metallicity.
        c (array-like): Coefficients.

    Returns:
        float: Low pressure opacity.

    Examples:
        low_pressure_opacity = kappa_low_p(T, P, met, c)
        # Returns the low pressure opacity.
    """

    kappa = 10**(c[0] * (np.log10(T) - c[1] * np.log10(P) - c[2])**2 + (c[3] * met + c[4]))  # in cm2/g

    return kappa * 1e-4 / (1e-3)  # converting to kg/m**2


def kappa_high_p(T, P, met, c):
    """
    Computes the high pressure opacity.

    Args:
        T (float): Temperature.
        P (float): Pressure.
        met (float): Metallicity.
        c (array-like): Coefficients.

    Returns:
        float: High pressure opacity.

    Examples:
        high_pressure_opacity = kappa_high_p(T, P, met, c)
        # Returns the high pressure opacity.
    """

    P = P * 10  # Converting to dyn/cm-2 from Pa

    kappa = 10**((c[5] + c[6] * np.log10(T) + c[7] * np.log10(T**2))
                 + np.log10(P) * (c[8] + c[9] * np.log10(T))
                 + met * c[10] * (0.5 + (1 / np.pi) * np.arctan((np.log10(T) - 2.5) / 0.2)))  # in cm2/g

    return kappa * 1e-4 / (1e-3)  # converting to kg/m**2


def get_T_1bar(T, P):
    """
    Interpolates the temperature value at 1 bar pressure.

    Args:
        T (array-like): Temperature values.
        P (array-like): Pressure values.

    Returns:
        float: Temperature at 1 bar pressure.

    Examples:
        T_1bar = get_T_1bar(T, P)
        # Returns the temperature at 1 bar pressure.
    """

    return float(interp1d(np.array(P) * 1e-5, np.array(T))(1))
