import os
import glob
import numpy as np
import pandas as pd
import sys
from scipy.interpolate import interp1d, interp2d
from scipy.signal import medfilt
from datetime import datetime, timedelta
import re
import matplotlib.pyplot as plt 
import matplotlib as mpl
from statistics import mode
import plotly.figure_factory as ff
import warnings
import random
import re
from scipy.signal import savgol_filter
import time
import h5py 
warnings.filterwarnings("ignore")
from scipy.interpolate import LinearNDInterpolator
import matplotlib as mpl
from colour import Color
from itertools import product
mpl.rcParams.update(mpl.rcParamsDefault)
from itertools import groupby
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from gray_profile import *
import pickle as pkl
import pyarrow.feather as feather

def bilinear_interp(df, xi, yi):
    """
    Bilinear interpolation on a non-regular grid with missing values.
    
    Parameters:
    x (ndarray): 1D array of x-coordinates.
    y (ndarray): 1D array of y-coordinates.
    z (ndarray): 1D array of values at grid points.
    xi (float): x-coordinate at which to interpolate.
    yi (float): y-coordinate at which to interpolate.
    
    Returns:
    zi (float): Interpolated value at (xi, yi).
    """
    
    val_pivot_df = df[[df.columns[0],df.columns[1],df.columns[2]]].pivot_table(index=df.columns[0], columns=df.columns[1], values=df.columns[2])
    x = val_pivot_df.index
    y = val_pivot_df.columns
    z = val_pivot_df.to_numpy()
    
    # Find the indices of the four grid points surrounding (xi, yi).
    
    i0 = np.searchsorted(x, xi, side='right') - 1
    i1 = i0 + 1
    
    j0 = np.searchsorted(y, yi, side='right') - 1
    j1 = j0 + 1
    
    non_valid = True
    while non_valid :
        change = [False,False]
        if np.isnan(z[i0, j0]) or np.isnan(z[i1, j0]):
            j0 = j0-1
            change[0] = True
        if np.isnan(z[i0, j1]) or np.isnan(z[i1, j1]):
            j1 = j1+1
            change[1] = True
        if (True in change) :
            non_valid = True
        else :
            non_valid = False
    
    # Compute the distances between (xi, yi) and the four grid points.
    dx = xi - x[i0]
    dy = yi - y[j0]
    dxi = x[i1] - xi
    dyi = y[j1] - yi
    
    # Compute the weights for the four grid points.
    w00 = dxi * dyi
    w10 = dx * dyi
    w01 = dxi * dy
    w11 = dx * dy
    
    # Compute the interpolated value using bilinear interpolation.
    zi = w00 * z[i0, j0] + w10 * z[i1, j0] + w01 * z[i0, j1] + w11 * z[i1, j1]
    
    return zi/np.sum([w00,w10,w01,w11])
