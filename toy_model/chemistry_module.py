"""
Created on Thu Jul 20 17:42:32 2023

@author: cwilkinson
"""

import numpy as np
import pandas as pd
import regex as re
from nasa_coef_module import *

class system:
    def __init__(self, temperature, pressure, states, species, elements, b_0 , debug):
#   Temperature, pressure, states, species, b_0, debug -> system.__init__ -> []
        
# Initiate main variables as global variables of the class
        
        self.R=8314.51 # Perfect gasses constant
        
        # Number of elements in each state initiated at zero
        self.NL=0 # Number of liquids
        self.NG=0 # Number of gasses
        
        # Thermodynamic state of the system
        self.T = temperature # Temperature
        self.P = pressure/(1.01325) # Pressure  pressure normalized by atmospheric pressure in bars
        
        # Species and chemical states to include
        self.states=states # Chemical state
        self.species=species # Chemical species to include
        self.elements=elements # Elements to consider
        self.chems=[] # Setting array for chemicals to be added after test
        self.flag=[] # Setting array to flag chemicals removed
        
        # Elemental properties
        self.a = self.coefficients() # Stochiometric coefficients
        self.b_0=b_0 # Natural abundance of each element
        self.pi=np.zeros(self.a.shape[0]) #Setting lagrange multipliers array
        
        # Debug mode to track progression anf find errors
        self.debug=debug
        
        # Thermodynamic data calculated from NASA pilynomials
        [self.mu_0,self.h, self.s,self.cp, self.abundances] = self.thermo()
        
        # Set the intial sums of the species in gasse and liquide states
        self.total_n=0.1 # Sum of gasses
        self.total_nl=0.05 # Sum of liquids
        self.total=0.1
        # Hold the initial guesses for the abundances.
        self.initial=self.abundances
     
    def coefficients(self):
#   [] -> coefficients -> a  

# Establish the stoechiometric coefficients matrix, this sub-program relies on no regular expression libraries
# It performs iteratif searches with all the combinations physically possible of the elements inputed
        
        elements=self.elements # Elements used (to be definied in __init__)
        species=self.species
        a=np.zeros([len(elements),len(species)]) # Setting the matrix, size: (elements X species)

        for i in range (len(species)): # Run through the species inputed
            spli=re.sub( r"([A-Z])", r" \1", species[i]).split()
            for j in spli:
                spl=re.sub(r"([0-9]+)", r" \1", j).split()
                for k in range (len(elements)):
                    for l in spl:
                        if elements[k]==l:
                            if len(spl)>1:
                                a[k,i]=spl[1]
                            else:
                                a[k,i]=1

                                
        a= a[~np.all(a == 0, axis=1)].astype(int)  
        #print(a)
        return a #Return matrix as integers
     
    def thermo(self):
#  [] -> thermo -> [mu,h,s,cp,n] 

# Using the NASA's polynomial coefficients, the sub-program calculates the thermodynamic properties of species
# at the pressure and temperature conditions. Sub-program compatible with 7 and 9 NASA coefficient .dat files.
        
        self.indice_liq=0 # For futur use, the indexe of the liquid species are set
        states=self.states # Setting locally the states of interest
        chems=self.species # Setting locally the chemicals of interest
        T=self.T # Setting locally the temperature
        R=self.R # Setting locally the perfect gasses constant
        
        
        # autmatically put gas state first, then liquid, solid and cristal
        state=states # States wanted 
        matches = [] # Setting array used to indexe matched species
        found_names=[] # Setting array used to list the name of matched species
        chem_state=[] # Setting array used to record state of each species
        self.names=[] # Setting global array used to records chemicals that are available
        for s in state: # Cycle through desired states
            for c in chems: # Cycle through desired species
                search=c+s # Set search variable, takes name of all possible combinations of desired species and states
                if type(get_coefs_of(search)) == tuple :
                    print(search)
                    found_names.append(c) # Record species found
                    chem_state.append(s) # Record state of species found
                    self.names.append(search) # Record name and state of species found
                    if s=='': # If species is in gas state 
                        self.NG+=1 # Count number of gasses
                    if s=='(L)': # If species is in liquid state
                        self.indice_liq+=1 # Count number of liquids
                        self.NL+=1 # Count number of liquids
        
        
        # Calculating thermodynamic parameters from coefficients:
        mu=[] # Setting chemical potential array for each species of interest
        h=[] # Setting Enthalpie array for each species of interest
        s=[] # Setting Entropy array for each species of interest
        cp=[] # Setting capacity at constant pressure array for each species of interest
        for chemical in self.names : # Cycle through all chemicals matched in NASA file
            Heat_capacity,Enthalpy,Entropy,chem_pot = evaluate_thermo(T, get_coefs_of(chemical))
            mu.append(chem_pot)
            h.append(Enthalpy)
            s.append(Entropy)
            cp.append(Heat_capacity)

        
        # Setting initial guesses
        n=np.ones(len(mu)) # set array of initial guesses of length of chemicals in use
        n=n*0.1/(self.NG) # Set initial guess of gass species using NASA reference paper proposition 
        n[self.NG:self.NG+self.indice_liq]=0.1/(self.indice_liq) #Set intial guess of liquid species (to be fine tuned)

        return mu,h,s,cp,n # Return thermodynamic properties and intial guess.   
    
    def Jacobien(self):
#   [] -> Jacobien() -> [J]

# Define the Jacobien matrix of the system, built by parts. Note, the size of this matrix will vary with the
# number of condensed elements considered, the max size is : (All chemicals + sum gasses + sum liquids + all elements)**2

        a=self.a # Import the coefficients matrix as a local variable
        NG=self.NG # Import the number of species in gas phase
        NS=self.NS # Import the current number of solids
        NL=self.NL # Import the current number of liquids
        
        size=(NG+NL+NS)+a.shape[0]+1+1*(NL>0) # Define the subsequent size of the Jacobien (as expressed above)
        J=np.zeros([size,size]) # Set the Jacobien matrix
        
        #The code below contains the construction of each part of the matrix which can see if sun-program called on
        temp = np.zeros((NG+NL, NG+NL), int) # Set a temporary sub matrix of size (gasses + current liquids)**2
        np.fill_diagonal(temp, 1) # Filling the diagonal of the temporary sub matrix (Identity matrix)
        J[0:NG+NL,0:NG+NL]=temp # Inserting the temporary matrix into the set Jacobien
        
        J[0:NG,NG+NL+NS]=-1 # Inserting -1's into the column corresponding to the correction on the total of gass species (gass thermo eqs)
        
        J[0:(NG),(NG+NL+NS+1+1*(NL>0)):]=-a.T[:NG,:] # Inserting the coefficients into the colums corresponding to the pi coeffs
        
        J[-1-(1*(NL>0)),0:NG]=(self.abundances[0:NG]) # Inserting the chemical abundances on the last row (mass balance eq)
        J[-1-(1*(NL>0)),NG+NL+NS]=-self.total_n # Inserting the total of gass species in the last row (mass balance eq)
        
        for i in range (len(self.chems)): # Cycle through all extra chemicals added which passed the phase test
            J[NG+i,(NG+NL+NS+1+1*(NL>0)):]=-a.T[self.chems[i],:] # Adding the coefficients of the new species
            J[NG:NG+NL,NG+NL+NS+1*(NL>0)]=-1 # Inserting -1 into column corresponding to the corretion on total liquid species
            J[-1,NG+i]=(self.abundances[self.chems[i]]) # Inserting liquid species abundancies into new last column (mass balance eq)
            J[-1,NG+NL+NS+1]=-self.total_nl # Inserting the total of liquid species into new the last row (mass balance eq)
    
        for i in range (NG): # Cycle through gas species to fill columns for each species
            for j in range (a.shape[0]): # Cycle through all element coefficients
                J[(NG+NL+NS)+j,i]=(self.abundances[i])*a[j,i] # Inserting multpliclation (abundances*coefs) for chemical abundance balance eqs
                
        for i in range (len(self.chems)): # Cycle through added species
            for j in range (a.shape[0]): # Cycle through coefficients
                J[(NG+NL+NS)+j,NG+i]=(self.abundances[self.chems[i]])*a[j,self.chems[i]] # Inserting multiplication for added species

        #if NL>0:
        #    row=np.zeros(size)
         #   colomn=np.zeros(size+1)
          #  J = np.vstack([J, row])
           # J=np.insert(J,J.shape[1], colomn, axis=1) 
            #J[-1,J.shape[1]-1]=-self.total
            #J[-1,NG+NL+NS+1]=self.total_nl 
            #J[-1,NG+NL+NS]=self.total_n
        #print (J)
        
        return J  # Return completed Jacobien matrix

    def Func(self):
#   [] -> Func() -> [F]

# Define the Function (B in AX=B) of the system, built by parts. Note, the size of the returned array will vary with the
# number of condensed elements considered, the max size is : (All chemicals + sum gasses + sum liquids + all elements)

        R=self.R # Import the perfect gasses constant locally
        P=self.P # Import the pressure locally
        T=self.T # Import the temperature locally
        mu_0=self.mu_0 # Import the standardchemical potentials of the species locally (calculated previously)
        
        a=self.a # Import the coefficients matrix as a loval variable
        NG=self.NG # Import the number of species in gas phase
        NS=self.NS # Import the current number of solids
        NL=self.NL # Import the current number of liquids
        
        F  = np.zeros((NG+NL+NS)+a.shape[0]+1+1*(NL>0)) # Setting the function with correct size
        mu = np.zeros(a.shape[1]) # Setting the chemical potential (non standard) array 
     
        for i in range (NG): # Cycle through all Gas species
            mu[i]=mu_0[i]+R*T*(np.log(self.abundances[i]/self.total_n))+R*T*np.log(P) # Calculate the chemical potential in given conditions
            
        F[0:NG]=mu[0:NG]/(R*T) # Fill function with normalised chemical potentials
        
        F[-1-(1*(NL>0))]= -self.total_n+sum(self.abundances[:NG]) # Fill the last value of function, mass balance eq (gasses)
        #-self.total_nl+sum(self.abundances[self.chems[:]])
       
        for i in range (len(self.chems)): # Cycle through potentially added species
            mu[self.chems[i]]=mu_0[self.chems[i]]+R*T*(np.log(self.abundances[self.chems[i]]/self.total_nl)) # Calculate the cemical potentials for liquids
            F[self.chems[i]]=mu[self.chems[i]] /(R*T) # Fill function with normalised chemical potentials (liquids)
            F[-1]= -self.total_nl+sum(self.abundances[self.chems[:]]) # Fill the new last value of function, mass balance equation (liquids)
            #-self.total_n+sum(self.abundances[:NG])
        
        for j in range (a.shape[0]): # Cycle through coefficients
            term1=0 # Set temporary variable used for sum abundancies*coefficients
            for i in range (NG): # Cycle through gass species
                term1+=self.abundances[i]*a[j,i] # Evaluate sum of elements i per species for gasses
            for i in range (len(self.chems)): # Cycle through added species
                term1+=self.abundances[self.chems[i]]*a[j,self.chems[i]] # Evaluate sum of elements i per species for liquids 
            F[(NG+NL)+j]+=term1-self.b_0[j] # Add abundance constraints to function
        
        #if NL>0:
         #   row=[0]
          #  F = np.insert(F,F.shape[0], row, axis=0)
            #F[-1]= self.total-self.total_n-self.total_nl
        #print(F)
        
        return F # Return completed Function array (B of AX=B)
       
    
    def iteration(self):
#   [] -> iteration() -> [Lbda (iteration variable)]

# Here we define the control factor (lbda) used to obtain new estimates of the variables. This factor is defined with
# empirical rules detailed in the NASA publication, here it has been expanded for liquids. The control factor 
# is recalculated at each iteration in order to converge correctly (step size adequate).

            NG=self.NG # Import the number of species in gas phase
            NS=self.NS # Import the current number of solids
            NL=self.NL # Import the current number of liquids
            
            size=18.420681 # Size parameter used to compare magnitude of mol fraction
            
            # Setting the sub control factors used subsequently to evaluate lbda. The setting allows the user to disable tests as required
            lambda_1=1 
            lambda_2=1
            lambda_3=1
            lambda_4=1
            
            # Setting the arrays that will indicate which mol fractions are under or over the size parameter 
            # For gasses:
            over=[] # Array for gas mol fraction above size parameter
            under=[] # Array for gas mol fraction below size parameter
            # For liquids:
            underl=[] # Array for liquid mol fraction under size parameter
            overl=[] # Array for liquid mol fraction above size parameter            
            
            for i in range(NG): # Cycle through gasses, for verification
                ratio=np.log(self.abundances[i]/self.total_n) # Calculate mol fraction for gas species
                if ((ratio<=-size) & (self.corr[i]>=0)): # Check of ratio is below size paramater with positive abundance corrections (in log scale)
                    under.append(i) # Add correponding species indice if above is true
                if (ratio>-size): # Check if ratio is above size parameter
                    over.append(i) # Add correponding species indice if above is true 
                    
            for i in range(len(self.chems)): # Cycle through added liquids, for verification
                ratio=np.log(self.abundances[self.chems[i]]/self.total_nl) # Calculate mol fraction for liquid species
                if ((ratio<=-size) & (self.corr[i]>=0)): # Check of ratio is below size paramater with positive abundance corrections (in log scale) 
                    underl.append(i) # Add correponding species indice if above is true
                if (ratio>-size): # Check if ratio is above size parameter
                    overl.append(i) # Add correponding species indice if above is true 
            
            # Below we evaluate the control factors used to evaluate the main control factor (iterative step):
            # 'This prevents a gaseous species with a small mole fraction from increasing to a mole fraction greater than 1e-4. '
            
            if len(over)>0:  # Change lambda_1 if the 'over' array is not empty
                temp=np.zeros(len(over)+1+(1*(NL>0))) # Set temporary array that will be used to define 'lambda_1'
                for i in range (len(over)): # Cycle through 'over'
                    temp[i]=(abs(self.corr[over[i]])) # Calculate the absolute value of the gas species corrections (log scaling)
            
                temp[-1]=(5*abs(self.corr[NG+NL+NS])) # Calculate absolute value multiplied by 5 of the correction on the total gas species (log scale)
      
                lambda_1=2/np.amax(temp) # Define lambda_1 from temp
            else: # If 'over' empty
                lambda_1=1 #Set lambda_1 to 1, base value for lbda (control factor)
            
            
            if len(under)>0: # Change lambda_1 if the 'under' array is not empty
                temp=np.zeros(len(under)) # Set temporary array that will be used to define 'lambda_2'
                for i in range (len(under)): # Cycle through 'under'
                    temp[i]=(abs((-np.log(self.abundances[under[i]]/self.total_n)-9.2103404)/(self.corr[under[i]]-self.corr[NG+NL+NS]))) # Calculate values for each mol fraction in under
                lambda_2=np.amin(temp) # Define lambda_2 from temp
            else: # If 'under' empty
                lambda_2=1 # Set lambda_2 to 1, base value for lbda (control factor)
        
            # That which is done above is the same below for the liquids, this needs to be verified!
            
            if len(overl)>0:
                temp=np.zeros(len(overl)+1+(1*(NL>0)))
                for i in range (len(overl)):
                    temp[i]=(abs(self.corr[overl[i]])) 
            
                temp[-1]=(5*abs(self.corr[NG+NL+NS+1]))
      
                lambda_3=(2/np.amax(temp))
            else:
                lambda_3=1
            
            
            if len(underl)>0:
                temp=np.zeros(len(underl))
                temp[-1]=1
                for i in range (len(underl)):
                    temp[i]=(abs((-np.log(self.abundances[underl[i]]/self.total_nl)-9.2103404)/(self.corr[underl[i]]-self.corr[NG+NL+NS+1])))
                lambda_4=(np.amin(temp))
            else:
                lambda_4=1
                
            
            self.lbda=min(1,lambda_1,lambda_2,lambda_3,lambda_4) # Control factor is defined as the smallest value between all lambdas or 1 otherwise
            
            
            if self.debug==1 : # Verification, the iteration sub-program allows for users to visualise 'lbda' at each iteration
                print("Lambda: \n {}\n".format(self.lbda)) # Print value of 'lbda'
            
            return self.lbda # Return control factor 
    
    
    def convergence(self):
#   [] -> iteration() -> [Lbda (iteration variable)]

# In order to evaluate whether the iterative procedure can stop and that a reasonable level of convergence has been reached,
# we use 5 tests on to verify if the corrections qre small compared to other parameters. With the condensed phases used, these
# test might need reviewing better.
        
        NG=self.NG # Import the number of species in gas phase
        NS=self.NS # Import the current number of solids
        NL=self.NL # Import the current number of liquids
        a=self.a # Import the coefficients matrix as a loval variable
        
        # The tests are intially set as arrays as multuple booleans will be used to define the final boolean.
        
        test1=[]
        test2=[]
        test3=[]
        test4=[]
        test5=[]

        # The correction to each gas species (log scale)  times the abundance of that species is compared to the sum of all species (not total variable)   
        for i in range (NG):
            test1.append((self.abundances[i]*np.absolute(self.corr[i]))/sum(self.abundances[:(NG+NL)]) <=0.5e-7)   
        test1=sum(test1)==len(test1)

        # The correction to each liquid species (log scale)  times the abundance of that species is compared to the sum of all species (not total variable)   
        for i in range (len(self.chems)):
            test2.append((self.abundances[self.chems[i]]*np.absolute(self.corr[NG+i]))/sum(self.abundances[:(NG+NL)]) <=0.5e-7)  
        test2=sum(test2)==len(test2)
            
        # The correction to the of total gas species (log scale)  times the total is compared to the sum of all species (not total variable)   
        test3=(self.total_n*np.absolute(self.corr[NG+NL]))/sum(self.abundances[:(NG+NL)]) <=0.5e-7
        
        # The correction to the total liquid species (log scale)  times the total is compared to the sum of all species (not total variable)
        test4=True
        if self.NL>0:
            test4=(self.total_nl*np.absolute(self.corr[NG+NL+1]))/sum(self.abundances[:(NG+NL)]) <=0.5e-7
        else:
            test4=True
       
        # The convergence of the mass towards that of the weighted elemental values is verified
        temp=0
        for j in range (a.shape[0]):
            for i in range (NG):
                temp+=((self.abundances[i])*a[j,i])
            for i in range (len(self.chems)):
                temp+=((self.abundances[self.chems[i]])*a[j,self.chems[i]])
            test5.append(abs(self.b_0[j]-temp<= max(self.b_0)*1e-7))          
        test5=sum(test5)==len(test5) 
            
            
        if self.debug==1 : # Verification, the convergence sub-program allows for user to visualise the test values at each iteration
            print("Tests: \n {} {} {} {} {}\n".format(test1, test2, test3, test4, test5)) # Print test results 1 to 5
        
        return [test1, test2, test3, test4, test5] # Returns the test results 
     
    
    def Test_condensed(self):
#   [] -> Test_condensed() -> []        

# This sub-program will verify whether a condensed species reduces the total Gibbs energy of the system, if so,
# this species will be added to the iterative correction process and a new convergence will be searched.
# The sub-program ca equally remove species if after convergence they no longer satisfy the Gibbs minimization test.

        R=self.R # Import the perfect gasses constant locally
        P=self.P # Import the pressure locally
        T=self.T # Import the temperature locally
        mu_0=self.mu_0 # Import the standardchemical potentials of the species locally (calculated previously)
        a=self.a # Import the coefficients matrix as a loval variable
        
        temp=0 # Setting temporary variable used to evaluate second term in Gibbs test (see Report)
        test=[0] # Setting array for all condensend species to test in order to find the lowest
        
        for i in range (self.NG,a.shape[1]): # Cycle through condensed species (if present)
            temp=mu_0[i]/(R*T) # Evaluate first term (normalised chemical potential)
            for j in range (a.shape[0]): # Cycle through elements
                temp+=-(a[j,i]*self.pi[j]) # Calculate second term and substract it from first term for each species 
            test.append(temp) # Append array of tested species
            
        chems_used=self.chems[:] # Save current condensed species in use
        #print("Phase test: {} \n" .format(test))   # Show test results for inspection 
        i=0 # Set iterator
        while i<len(self.chems): # We use a while loop here to be able to modulate the length if elements removed
            if test[chems_used[i]-self.NG+1]>3 : # Test for used species
                #print('element removed, listed as: {}'.format(self.chems[i])) # Print for user that an element is removed if test failed
                self.chems.pop(i) # Remove element from list of added species
                self.NL=self.NL-1 # Reduce number of liquids in use
                return # If a species is removed, a new convergence needs to be found so we quit the sub-program
            i+=1
                
        if self.debug==1: # Verification, the Test_condensed sub-program allows for user to visualise the phqse test values at each iteration
            print("Phase test: {} \n" .format(test)) # Print phase test values for each species (test[0] not a species)
    
        # Before adding a new species, we need to set the test to zero for all species that are still included as to not add them twice
        for i in range(len(self.chems)):
            test[self.chems[i]-self.NG+1]=0
        
        # If new species satisfy the test, we take the smallest one.
        if np.min(test)<0:
            for i in range (len(test)):
                if test[i]==np.min(test):
                    lowest=i+self.NG-1 # Translate test indexing to global indexing
                    #print('new element added listed as: {}'.format(lowest)) # Inform user of a new species added
                    #print('Abundances currently: {}'.format(self.abundances)) # Inform user of current state of abundances
                    #print('********************')
                    self.chems.append(lowest) # Add species to list of added species
                    #print(self.chems) 
                    self.NL=self.NL+1 # Add a nez liquid element to number of liquid elements
                    #print('Number of liquids: {}'.format(self.NL)) # Inform user of number of liquid elements in use
                    self.state.append('NL') # Futur work on solid/liquid species in use
        
                  
    def solver(self):
#   [] -> solver() -> [Normalized abundances]        

# This sub-program will call on all the su-programs above to calculate the corrections to be apllied to the variables as well 
# as call on the su-programs to know whether to continue the iterations and to add new species
    
        init=self.abundances # Hold locally the inital guesses 
        test=[False,False,False,False,False] # Set all tests at False to be sure too initiate the iterations
        self.chems=[] # Set the array that will hold the indexes of the new condesend species to add
        self.state=[] # Set the array that will hold the state of the new condensed species to add (to be used once program validated for liquids)
        self.pi=np.zeros(len(init)) # Initiate the array used for the lagrangian multipliers
        iteration=0 # Intiate the iteration counter
        while ((test[0] & test[1] & test[2] & test[3] & test[4]) == False): # Continue iterations so long as at least one test false
            #print(j)
            NG=self.NG # Hold locally the number of gass species considered
            print('Number of gasses {}'.format(NG))
            NS=self.NS # Hold locally the number of solid species consiedered at this step 
            print('Number of solids {}'.format(NS))
            NL=self.NL # Hold locally the number of liquid species consiedered at this step 
            print('Number of liquids {}'.format(NL))
            J=self.Jacobien() # Hold locally the Jacobi matrix by calling on the sub-program
            print(J)
            F=self.Func() # Hold locally the function by calling on the sub-program locally
            print(F)
            a=self.a # Hold locally the stoechiometric coefficients
            print(a)
            
            self.corr=np.linalg.solve(J,-F) # Solve the system using the numpy library linear algebra solver
            
            self.lbda=self.iteration() # Obtain the corrections to be applied using the sub-program
            
            for i in range (NG): # Apply correction on all gas species
                self.abundances[i]=self.abundances[i]*np.exp(self.lbda*self.corr[i])
                
            for i in range (len(self.chems)): # Apply correction on all condensed species considered at this iteration
                self.abundances[self.chems[i]]=self.abundances[self.chems[i]]*np.exp(self.lbda*self.corr[NG+i])
            
            for i in range (a.shape[0]): # Update the langrangian multiplier coefficients
                self.pi[i]=self.corr[NG+NL+NS+1+1*(NL>0)+i]
            
            self.total_n=self.total_n*np.exp(self.lbda*self.corr[NG+NL+NS]) #+ 1e-10 # Apply correction to total gas species
            
            if NL>0: # Apply correction to total liquid species
                self.total_nl=self.total_nl*np.exp(self.lbda*self.corr[NG+NL+NS+1*(NL>0)])+ 1e-10#-> absolument nécessaire ce terme correctif
                
            test=self.convergence() # Use sub-program to determine the state of converegnce
            
            before=len(self.chems) # Record number of condensed species added before following test
            if ((test[0] & test[1] & test[2] & test[3] & test[4]) == True): # If convergence obtained
                self.Test_condensed() # Execute sub-program to determine if new species need to be added or removed
                if len(self.chems) != before: # If new species added
                    test[0]=test[1]=test[2]=test[3]=test[4]=False # Reset tests to seek a new convergence with a new set of species
                            
            if iteration>10000: # Fail safe, if program can not converge, stop after 10000 by setting all tests to true
                test[0]=True
                test[1]=True
                test[2]=True
                test[3]=True
                test[4]=True
                print("\033[1m" + "Warning! One or more conditions are not satisfied after 1e3 iterations, please enter debug mode \n" + "\033[0m")
            
            if self.debug==1 : # Debug mode information on each iteration step
                print("Iteration {} : \n  The abundances are: \n {} \n".format(iteration, self.abundances))
                #print("Iteration {} : \n  The abundances are: \n {} \n".format(j, self.abundances))
                print("Total n gas: {} \n" .format(self.total_n))
                print("Total n liquid: {} \n" .format(self.total_nl))
                print("Total : {} \n" .format(self.total))
                print("Additional chems listed: {} \n" .format(self.chems))
            
            iteration+=1 # Add to iteration counter
        
        #print('Abundances currently: {}'.format(self.abundances))
        out=np.zeros(len(self.abundances)) # Set array used to return abundances
        out[:self.NG]=self.abundances[:self.NG] # Insert gas abundances
        for i in range (len(self.chems)): # Insert condensed abundances
                out[self.chems[i]]=self.abundances[self.chems[i]] 
        
        return out[:]/sum(self.abundances[:(self.NG+self.NL+self.NS)]), self.names # Return abundances and species names use