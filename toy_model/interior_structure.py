#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 09:55:34 2023

@author: cwilkinson
"""

import pandas as pd
import scipy
from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import RegularGridInterpolator
from scipy import integrate
import matplotlib as mpl
from scipy.interpolate import interpn
import matplotlib.pyplot as plt
import numpy as np
from numerical_tools import *
from gray_profile import *
from scipy.signal import medfilt

gamma = 7/5
G = 6.6743e-11
R_J = 69911000
M_E = 5.972*1e24
R_E = 6371000
M_J = 1.898*1e27
mu = 0.0023
R = 8.314

def load_thermo_tables () :
    # Load hydrogen thermodynamic table and define columns (P,T,rho,S) in (bar,K,g/cc,MJ/kg/K)
    names = ['log T [K]','log P [GPa]','log rho [g/cc]','log U [MJ/kg]','log S [MJ/kg/K]','dlrho/dlT_P','dlrho/dlP_T','dlS/dlT_P','dlS/dlP_T','grad_ad']
    H = pd.read_csv('../data/dataDirEOS2021/TABLE_H_TP_v1',sep='\s+',header=1, index_col=False, names=names)
    H = H.apply(pd.to_numeric, errors='coerce').dropna()
    H['P'] = (1e9/1e5)*10**H['log P [GPa]']
    H['T'] = 10**H['log T [K]']
    H['rho'] = 10**H['log rho [g/cc]']
    H['S'] = 10**H['log S [MJ/kg/K]']
                             
    # Load helium thermodynamic table and define columns (P,T,rho,S) in (bar,K,g/cc,MJ/kg/K)          
    He = pd.read_csv('../data/dataDirEOS2021/TABLE_HE_TP_v1',sep='\s+',header=1, index_col=False, names=names)
    He = He.apply(pd.to_numeric, errors='coerce').dropna()
    He['P'] = (1e9/1e5)*10**He['log P [GPa]']
    He['T'] = 10**He['log T [K]']
    He['rho'] = 10**He['log rho [g/cc]']
    He['S'] = 10**He['log S [MJ/kg/K]']
    
    # Load hydrogen/helium thermodynamic table at 0.2919 mass fraction and define columns (P,T,rho,S) in (bar,K,g/cc,MJ/kg/K)     
    # Table not used
    H_He = pd.read_csv('../data/dataDirEOS2021/TABLEEOS_2021_TP_Y0292_v1',sep='\s+',header=1, index_col=False, names=names)
    H_He = H_He.apply(pd.to_numeric, errors='coerce').dropna()
    H_He['P'] = (1e9/1e5)*10**H_He['log P [GPa]']
    H_He['T'] = 10**H_He['log T [K]']
    H_He['rho'] = 10**H_He['log rho [g/cc]']
    H_He['S'] = 10**H_He['log S [MJ/kg/K]']

    return H, He, H_He

def thermo_mix_at_y(y) :
    # We suppose that the hydrogen and helium dataframes are of the shape (T,P) -> (rho,S) and that they hare given on the same (T,P grid)
    
    data = pd.DataFrame()
    H, He ,H_He = load_thermo_tables () # Load hydrogen and helium thermodynamic tables
    data['T'] = list(H['T'])
    data['P'] = list(H['P'])
    data['S'] = (1-y)*H['S']+y*He['S'] # Set a new DataFrame from the prexisting Hydrogen DataFrame
    data['rho'] = ((1-y)/H['rho']+y/He['rho'])**(-1)
    return data

def isentrope (data,T,P) :
    S = bilinear_interp(data[['T','P','S']],T,P) # Perform bilinear interpolation to get the entropy value at (T,P)
    
    # Create empty lists to hold the new temperature and density values
    T_new = []
    rho_new = []
    
    new = pd.DataFrame() # Create an empty DataFrame to hold the new temperature, density, and pressure values
    
    # Iterate over a sorted list of unique pressure values in data
    for P in np.sort(list(set(data['P']))) :
        df_ = data[data['P']==P]  # Get the subset of data that has pressure equal to P
        df_['surrounding_values'] = np.abs(df_['S']-S) # Calculate the absolute difference between the entropy values in df_ and the interpolated entropy value S to interpolate around S
        df_ = df_.sort_values('surrounding_values',ascending=True).iloc[:15].drop_duplicates('surrounding_values') # Sort the subset by the absolute difference and take the ten points with the smallest difference
        
        # Use cubic spline interpolation to get the temperature and density values corresponding to the interpolated entropy value S
        T_new.append(float(interp1d(df_['S'],df_['T'],bounds_error=False,fill_value=np.nan, kind='linear')(S)))
        rho_new.append(float(interp1d(df_['S'],df_['rho'],bounds_error=False,fill_value=np.nan, kind='linear')(S)))

    # Assign the pressure, temperature, and density values to the new DataFrame
    new['P'] = np.sort(list(set(data['P'])))
    new['rho'] = rho_new
    new['T'] = T_new
    
    return new.sort_values('P') # Return the new DataFrame

def interpolate_isentrope(isen_values,P) :
    isen_values = isen_values[isen_values['T'].isin(list(medfilt(isen_values['T'],kernel_size=13)))]
    isen_values = isen_values[(isen_values['P']<=np.max(P)) & (isen_values['P']>=np.min(P))] # restrict isentrope values to within the given range of pressures
    
    # interpolate new temperature and density values using cubic interpolation
    T_new = interp1d(isen_values['P'],isen_values['T'],bounds_error=False,fill_value=np.nan, kind='cubic')(P)
    rho_new = interp1d(isen_values['P'],isen_values['rho'],bounds_error=False,fill_value=np.nan, kind='cubic')(P)
    
    # create a new DataFrame to store the interpolated values
    new = pd.DataFrame()
    new['P'] = P
    new['rho'] = rho_new
    new['T'] = T_new
    
    return new # return the new DataFrame with interpolated values

def rho_at_P(isen_values,P) :
    # Get the density at a given pressure along a given isentrope
    rho_new = interp1d(isen_values['P'],isen_values['rho'],bounds_error=False,fill_value=np.nan, kind='linear')(P)
    return float(rho_new)
    
def T_at_P(isen_values,P) :
    # Get the Temperature at a given pressure along a given isentrope
    T_new = interp1d(isen_values['P'],isen_values['T'],bounds_error=False,fill_value=np.nan, kind='linear')(P)
    return float(T_new)

def f_rho_g(isen_values,P,g) :
    return -rho_at_P(isen_values,P*1e-5)*(1e3)*g # Return the right hand side of the HSE equation

def adjust_R(M_planet, R_guess,radius,rho) :
    # Reverse the radius and density arrays
    radius = np.array(radius[::-1])
    rho = np.array(rho[::-1])
    
    total_mass = 4*np.pi*integrate.trapezoid((radius**2)*rho, x=radius) # Calculate the total mass of the planet using trapezoidal integration
    R_new = R_guess*(M_planet/total_mass) # Calculate the new radius based on the estimated mass and total mass
    error = np.abs(R_new-R_guess)/R_guess # Calculate the error between the new radius and the old radius
    
    return R_new, error

def evaluate_mass_above(radius,rho) :
    radius = np.array(radius[::-1]) # Reverse the order of the radius array using slicing to start from the innermost point
    rho = np.array(rho[::-1]) # Reverse the order of the density array using slicing to match the order of the radius array
    mass = 4*np.pi*integrate.trapezoid((radius**2)*rho, x=radius) # Integrate the density profile with respect to the radius to compute the total mass above a given radius
    
    return mass  # return the mass above the curent radius


def interior_structure(M_planet,P_top,T_top,y) :
    # Function to calculate the interior structure of a planet given its mass, 
    # atmospheric pressure and temperature, and the fraction of hydrogen (y) in the atmosphere.

    # Set initial values and constants
    R_guess = 1e8 # Initial guess for the planet's radius
    erreur = 100 # Initial error
    iteration_profile = 0 # Counter for the number of iterations
    tol = 1e-5 # Tolerance for the error

    # Calculate the properties of the atmosphere at the top
    data = thermo_mix_at_y(y) # Get the thermodynamic table at the given fraction of hydrogen
    isen_values = isentrope (data,T_top,P_top) # Function that calculates the isentropic profile given the atmospheric properties

    # Iterate until the error is less than the tolerance
    while (erreur > tol) or (iteration_profile==0) :
        h = -R_guess/1e2 # Step size for integration
        radius = [R_guess] # Initialize list of radii with the initial guess
        pressure = [P_top*1e5] # Initialize list of pressures with the given top pressure (converted to Pa)
        rho = [rho_at_P(isen_values,pressure[0]*1e-5)*(1e3)] # Calculate the density at the top
        temperature = [T_top] # Initialize list of temperatures with the given top temperature
        idx = 0 # Counter for the current index in the lists
        stop_flag = False # Flag to stop the integration if the radius becomes negative
        gravity = [G*M_planet/R_guess**2] # Initialize list of gravitational accelerations with the initial guess
        
        # Integrate inward until the radius becomes negative or the integration reaches the center
        while (not stop_flag) :
            k1 = h*f_rho_g(isen_values,pressure[idx],gravity[idx]) # Calculate the derivative of the density with respect to the radius
            k2 = h*f_rho_g(isen_values,pressure[idx]+k1/2,gravity[idx]) # Use the midpoint method to calculate k2
            k3 = h*f_rho_g(isen_values,pressure[idx]+k2/2,gravity[idx]) # Use the midpoint method to calculate k3
            k4 = h*f_rho_g(isen_values,pressure[idx]+k3,gravity[idx]) # Use the midpoint method to calculate k4
            
            # Check if the next radius is negative, if so stop the integration
            if radius[idx]+h > 0 :
                # Calculate the next values and add them to the lists
                radius.append(radius[idx]+h)
                pressure.append(pressure[idx]+k1/6+k2/3+k3/3+k4/6) # Calculate the next pressure value using RK4 methode
                rho.append(rho_at_P(isen_values,pressure[idx+1]*1e-5)*(1e3))  # Interpolate density at given pressure
                gravity.append(G*np.abs(M_planet-evaluate_mass_above(radius,rho))/radius[-1]**2) # Calculate the gravity by substracting mass above given radius
                temperature.append(T_at_P(isen_values,pressure[idx+1]*1e-5)) # Interpolate temperature at given pressure
            else :
                stop_flag = True # Raise stop flag if next step will result in negative radius

            idx = idx+1 # Increment the index to evaluate next layer

        R_guess, erreur = adjust_R(M_planet, R_guess,radius,rho) # Adjust the radius to match the mass of the planet and calculate the error
        print('Radius {:.5f} Jupiter, erreur {:.5e}'.format(R_guess/R_J,erreur))

        iteration_profile +=1 # Increment number of iterations to reach convergence

    print('Final radius {:.2f} Jupter'.format(R_guess/R_J))
    
    return radius,pressure,rho,gravity,temperature