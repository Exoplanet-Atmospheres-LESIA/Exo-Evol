#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 15:55:05 2021

@author: cwilkinson
"""
import numpy as np
import matplotlib.pyplot as plt
import time
import h5py 
from scipy.interpolate import interp1d
from scipy.integrate import solve_ivp
from scipy.interpolate import LinearNDInterpolator
import random
import glob
import pandas as pd
import re
from nasa_coef import *


def T_profile(P,T_eff,met,g,species=['H2','He'],mass_fractions=[1-0.246,0.246]) :
    T = [T_0(T_eff)]
    g = g*np.ones(len(P))
    is_convective = []
    c = np.zeros([11,len(P)])
    tau_ = []
    flag = False
    P_link = np.nan
    T_at_P = np.nan
    if len(species) == 2 :
        mass_fractions = [1-(0.246+met*0.0134),0.246+met*0.0134]
    for ii in range(1, len(P)):
        c[:,ii] = coefficients(T[-1])
        if ii > 1 :
            adiabat = (np.log(T[-1])-np.log(T[-2]))/(np.log(P[ii-1])-np.log(P[ii-2]))
            if (adiabat >= (get_PT_sloap(species,mass_fractions,T[-1]))) & (not flag) :
                P_link = P[ii]
                T_at_P = T[ii-1]
                flag = True
        if flag :
            T.append(profile_adiabatique (T,P,met,c,g[ii],species,mass_fractions))
            is_convective.append(1)
        else :
            T.append(((3/4) * T_eff**4*((2/3) + tau(T,P,met,c,g[ii])))**(1/4)) 
            is_convective.append(0)
        tau_.append(tau(T,P,met,c,g[ii]))
    
    df = pd.DataFrame()
    df['T_at_P'] = [T_at_P]
    df['g_at_P'] = [g[0]]
    df['P_link'] = [P_link]
    df['temperature'] = [T]
    df['pressure'] = [P]
    df['T_eff'] = [T_eff]
    df['is_convective'] = [is_convective]
        
    return df

def profile_adiabatique (T,P,met,c,g,species,mass_fractions) :
    T = T[-1] * (P[-1]/P[-2])**(get_PT_sloap(species,mass_fractions,T[-1]))
    #T = T*(2/7)*(((P[-1]-P[0]))/P[0]) + T
    return T
    
def T_0(T_int) :
    T = ((3/4)*T_int**4*(2/3))**(1/4)
    return T

def coefficients(T) :
    c = np.zeros(11) 
    c[0] = -37.50
    c[1] = 0.00105
    c[2] = 3.2610
    c[3] = 0.84315
    c[4] = -2.339   
    if T<=800 :
        c[5] = -14.051
        c[6] = 3.055
        c[7] = 0.024
        c[8] = 1.877
        c[9] = -0.445
        c[10] = 0.8321       
    if T>800 :
        c[5] = 82.241
        c[6] = -55.456
        c[7] = 8.754
        c[8] = 0.7048
        c[9] = -0.0414
        c[10] = 0.8321      
    return c
        
def tau(T,P,met,coeff,g) :
    tau = 0
    for jj in range(0,len(T)-1) :
        c = coeff[:,jj]
        kappa_low = kappa_low_p(T[jj],P[jj],met,c)
        kappa_high = kappa_high_p(T[jj],P[jj],met,c)
        kappa = (kappa_low + kappa_high)#**(-1)
        m = (P[jj+1]-P[jj])/g 
        tau += kappa * m
    
    return tau


def kappa(T,P,met) :
    c = coefficients(T)
    kappa_low = kappa_low_p(T,P,met,c)
    kappa_high = kappa_high_p(T,P,met,c)
    
    return (kappa_low + kappa_high)


def kappa_low_p(T,P,met,c) :
    
    kappa = 10**(c[0]*(np.log10(T) - c[1]*np.log10(P)-c[2])**2 \
                 + (c[3]*met + c[4])) # in cm2/g

    return kappa*1e-4/(1e-3) # converting to kg/m**2
    
def kappa_high_p(T,P,met,c) :
    P = P*10 # Converting to dyn/cm-2 from pa
    
    kappa = 10**((c[5] + c[6]*np.log10(T) + c[7]*np.log10(T**2)) \
                 + np.log10(P) * (c[8] + c[9]*np.log10(T)) \
                     + met*c[10]*(0.5 + (1/np.pi)*np.arctan((np.log10(T)-2.5)/0.2))) # in cm2/g
    return kappa*1e-4/(1e-3) # converting to kg/m**2





