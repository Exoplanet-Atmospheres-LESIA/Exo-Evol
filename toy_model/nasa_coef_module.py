"""
Created on Tue Jan 31 13:14:59 2023

@author: cwilkinson
"""

import sys
sys.path.append("..")

import numpy as np
import matplotlib.pyplot as plt 

import re

import warnings
warnings.filterwarnings("ignore")

def get_coefs_from_line(line) :
    values = re.findall(r"[-+]?(?:\d*\.*\d+)", line)
    coefs = []
    for value in values :
        if ((value[0] == '-') or  (value[0] == '+')) & (len(value)<5):
            coefs[-1] =  coefs[-1]*10**float(value)
        else :
            coefs.append(float(value))
        
    return coefs

def get_coefs_of(chemical) :
    dat_file = './data/thermo_datasets/nasa9.dat'
    data = open(dat_file, "rb")
    lines = data.readlines()
    lines = [line.decode("utf-8") for line in lines]
    chemical_found = False
    for ii in range(0,len(lines)) :
        if (chemical+' ') in (lines[ii].split(' ')[0]+' ') :
            chemical_found = True
            index = ii
            first_T_range = [float(value) for value in lines[index+2].split(' ') if value !=''][:2]
            coefficients_first = get_coefs_from_line(lines[index+3])
            coefficients_first += get_coefs_from_line(lines[index+4])
            use_third_range = True
            
            if (lines[index+5][0] == ' ') :
                second_T_range = [float(value) for value in lines[index+5].split(' ') if value !=''][:2]
                coefficients_second = get_coefs_from_line(lines[index+6])
                coefficients_second += get_coefs_from_line(lines[index+7])
            else :
                second_T_range = -1
                coefficients_second = None
                use_third_range = False
                
            if (lines[index+8][0] == ' ') & (use_third_range) :
                third_T_range = [float(value) for value in lines[index+8].split(' ') if value !=''][:2]
                coefficients_third = get_coefs_from_line(lines[index+9])
                coefficients_third += get_coefs_from_line(lines[index+10])
            else :
                third_T_range = -1
                coefficients_third = None
    
    if chemical_found :         
        return first_T_range,coefficients_first,second_T_range,coefficients_second,third_T_range,coefficients_third
    else :
        return False
            
def evaluate_thermo(T, coefficients) :
    first_T_range,coefficients_first,second_T_range,coefficients_second,third_T_range,coefficients_third = coefficients
    
    if type(third_T_range) == list :
        if T > third_T_range[1] :
            T = third_T_range[1]
    elif type(second_T_range) == list :
        if T > second_T_range[1] :
            T = second_T_range[1]
    else :
        if T > first_T_range[1] :
            T = first_T_range[1]
    
    if T < first_T_range[0] :
        T = first_T_range[0]
        
    if (T>=first_T_range[0]) & (T<first_T_range[1]):
        c = coefficients_first
   
    if type(second_T_range) == list :
        if (T>second_T_range[0]) & (T<=second_T_range[1]):
            c = coefficients_second
 
    if type(third_T_range) == list :
        if (T>third_T_range[0]) & (T<=third_T_range[1]):
            c = coefficients_third
    
    Heat_capacity = c[0]*T**(-2) + c[1]*T**(-1) + c[2] + c[3]*T + c[4]*T**2 + c[5]*T**3 + c[6]*T**4
    Enthalpy = -c[0]*T**-2 + (c[1]/T)*np.log(T) + c[2] + c[3]*T/2 + c[4]*T**2/3 + c[5]*T**3/4 + c[6]*T**4/5 + c[7]/T
    Entropy = -c[0]/2*T**-2 - c[1]*T**-1 + c[2]*np.log(T) + c[3]*T + c[4]/2*T**2 + c[5]/3*T**3 + c[6]/4*T**4 + c[8]
    mu = (-c[0]/2.)*T**(-2.) + (c[1]/T)*(1+np.log(T)) + c[2]*(1-np.log(T)) - (c[3]/2.)*T - (c[4]/6.)*T**2. - (c[5]/12.)*T**3. - (c[6]/20.)*T**4. + (c[7]/T) - c[8]

    return Heat_capacity,Enthalpy,Entropy,mu


def get_Cp_mix(chemicals,mass_fractions,T) :
    Cp = 0
    for chemical, fraction in zip(chemicals,mass_fractions) :
        coefficients = get_coefs_of(chemical)
        Heat_capacity,Enthalpy,Entropy = evaluate_thermo(T, coefficients)
        Cp += fraction*Heat_capacity
        
    return Cp

def get_adiabat_idx(chemicals,mass_fractions,T) :
    Cp = get_Cp_mix(chemicals,mass_fractions,T)*8.314
    gamma = Cp/(Cp-8.314)
    return gamma

def get_PT_sloap(chemicals,mass_fractions,T) :
    gamma = get_adiabat_idx(chemicals,mass_fractions,T)
    return (gamma-1)/gamma
    
        
