import sys
sys.path.append("../../Exo-Evol")

import pandas as pd
from python_modules.interpolation_module import *
from python_modules.exoplanet_data_module import *
from python_modules.instrument_module import *
from python_modules.fitting_observation_module import *
from python_modules.plotting_tools_module import *


df = load_grid('./../../data/current_exoevol_grid/ker',planet_type='kk',EOS='ker',Mass=[9,10],Met=[1,1.5],
                                Core=[0,40],T_irr=[0,150],T_int=[950,1200],clouds=True,jobs=10,exception=False)

df = df[df['file'] == '../../data/planets/test_clouds_5kk/10.0000_950.0_0_12.0_0.010383_2.0_ker_500.pkl'].iloc[-1:]

with open( '../../data/planets/test_clouds_5kk/10.0000_950.0_0_12.0_0.010383_2.0_ker_500.pkl', 'rb') as f:
    df_view = pkl.load(f)
    
interp = get_closest_interpolator(10,50,'radiosity')
model_wavelength = load_pkl('./../../data/interpolators/flux/example_'+'_'.join(interp[1].split('_')[-2:]))['wavelength_up'].iloc[0]
flux = interp[0](10,np.log10(12),33,950,2)
flux, radius = flux[:-1], flux[-1]
flux_model = np.array(convert_radiosity(df_view['radiosity_up'].iloc[0],df_view['wavenumber_up'].iloc[0]))
flux_model = diluted_planet(flux_model, radius, 41)
plt.plot(model_wavelength, flux_model)
plt.plot(model_wavelength,diluted_planet(flux, radius, 41),label='Interpolated model',color='k')
plt.xlim([1.5,3])
#plt.show()

Radius = df['Radius_Jup'].iloc[-1]
df[['Mass_Jup','Met','T_int','core','f_sed']].iloc[-1]
D = 41.2441
model_wavelengths,model_flux = (wavenumber_to_wavelength(df['wavenumber_up'].iloc[0]),  convert_radiosity(df['radiosity_up'].iloc[0],df['wavenumber_up'].iloc[0]))
model_flux = diluted_planet(model_flux, Radius, D)

df_spectro = read_spectro_docs()
df_photo = read_filter_docs()
observed_spectrum = pd.DataFrame()

observed_spectrum['wavelength'] = list(df_spectro['gravity']) + [float(df_photo[df_photo['name'] == 'SPHERE B_H']['wl'].iloc[0])]
observed_spectrum['flux'] = 1

observed_spectrum = identify_observation(observed_spectrum)
adapted_model = model_at_observation(observed_spectrum,model_wavelengths,model_flux)

example_spectrum_K = load_spectrum('./../spectra/spectra_list/HR8799_c_2019_11_02_YJH_spectrum_paco_full_GPI_jwst.txt')
example_spectrum_K = example_spectrum_K[example_spectrum_K['wavelength'].round(4).isin(list(df_spectro['gravity'].round(4)))]
adapted_model['erreur'] = list(example_spectrum_K['rand_error']) + [0.1*adapted_model['model_flux'].iloc[-1]]

offset_ks = [1]
offset_hs = [1]

for offset_h in offset_hs :
    for offset_k in offset_ks :
        for idx in range(1,2) :
            rand_flux = []
            for flux, erreur in zip(adapted_model['model_flux'], adapted_model['erreur']) :  
                if flux == adapted_model['model_flux'].iloc[-1] :
                    rand_flux.append(random.gauss(offset_h*flux, erreur))
                else :
                    rand_flux.append(random.gauss(offset_k*flux, erreur))
                #rand_flux.append(random.gauss(offset_k*flux, erreur))

            adapted_model['randomized_flux'] = rand_flux

            name = '_'.join([str(ele) for ele in list(df[['Mass_Jup','Met','T_int','core','f_sed']].iloc[-1])])
            name += '_sol3_fact_{}_{}_{}'.format(offset_k,offset_h,idx)
            #name += '_sol2_fact_{}_{}'.format(offset_k,idx)
            #adapted_model[['wavelength','model_flux','erreur']].to_csv('../spectra/synthetic_spectra_list/{}.txt'.format(name),index=False)

            plt.plot(model_wavelengths,model_flux,'--',label='Model spectrum')
            plt.errorbar(adapted_model['wavelength'],adapted_model['randomized_flux'],yerr=adapted_model['erreur'] ,fmt='o',alpha=0.3,label='Noised/Offet (H band)')
            plt.errorbar(adapted_model['wavelength'],adapted_model['model_flux'],yerr=adapted_model['erreur'] ,fmt='o',alpha=0.5,label='K-band')
            plt.xlim([1.6,2.6])
            plt.legend()
            plt.xlabel(r'Wavelength $\mu m$')
            plt.ylabel(r'Flux $W/m^2/\mu m$')
            plt.title(r'Offset {}_{}X Flux'.format(offset_k,offset_h))
plt.show()