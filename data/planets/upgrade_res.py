import sys
sys.path.append("./../../Exo-Evol")
sys.path.append("./Exo-Evol")

import pandas as pd
import glob
import pyarrow.feather as feather

from python_modules.interpolation_module import *
from python_modules.exoplanet_data_module import *
from python_modules.instrument_module import *
from python_modules.fitting_observation_module import *
from python_modules.plotting_tools_module import *
from python_modules.exorem_module import *

files = glob.glob('./test_clouds_Fe_Mg2SiO4_5kk_corrected_corrected/*_500.pkl')
path_exorem = './../../Exo-Evol/exorem'
exorem_data = '../exorem_data/data'

for file in files :
    with open(file, "rb") as f:
        df = pkl.load(f).iloc[:1]
    
    if ('thermal_up' not in df.columns) or (len(df['thermal_up'].iloc[0])<1000) :
        T_profile = df['/outputs/layers/temperature'].iloc[0]
        P_profile = df['/outputs/layers/pressure'].iloc[0]
        Met = df['Met'].iloc[0]
        T_int = df['T_int'].iloc[0]
        T_irr = df['T_irr'].iloc[0]
        if ('cloud' in file) or ('f_sed' in df.columns) :
            f_sed = df['f_sed'].iloc[0]
        else :
            f_sed = -1
        g = float(interp1d(df['/outputs/layers/pressure'].iloc[0],df['/outputs/layers/gravity'].iloc[0])(1e5))
        R = df['/model_parameters/target/radius_1e5Pa'].iloc[0]    
        equilibrium = False

        df_up = upgrade_spectra_post(path_exorem,exorem_data,T_profile,P_profile,Met,T_int,T_irr,f_sed,g,R,equilibrium)
        df['thermal_up'] = [df_up['/outputs/spectra/emission/contributions/thermal'].iloc[0]]
        df.to_pickle(file)
        print(file)
        
for file in files :
    with open(file, "rb") as f:
        df = pkl.load(f).iloc[:1]

    if ('thermal_up' in df) and (len(df['thermal_up'].iloc[0])>1000):
        plt.figure()
        plt.plot(df['/outputs/layers/temperature'].iloc[0],np.array(df['/outputs/layers/pressure'].iloc[0])*1e-5,label='Mass : {:.2f} Radius : {:.2f}\nTint : {:.1f} Tirr : {:.1f}'.format(df['mass'].iloc[0]*1e-3/M_J,df['Req'].iloc[0]*1e-2/R_J,df['T_int'].iloc[0],df['T_irr'].iloc[0]))
        plt.plot(df['T'].iloc[0],df['P'].iloc[0])
        plt.gca().invert_yaxis()
        plt.yscale('log')
        plt.xscale('log')
        plt.axhline(y=df['P'].iloc[0][int(df['iindex'].iloc[0])], color='r', linestyle='--')
        #plt.axhline(y=df['P'].iloc[0][int(jump_idx)], color='k', linestyle='--')
        plt.legend()
        plt.xlabel('Temperature (K)')
        plt.ylabel('Pressure (bar)')
        plt.show()
        break