import os
import glob
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

from scipy.interpolate import interp1d
from itertools import product
import random
from sklearn.ensemble import  RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix, precision_score, recall_score, ConfusionMatrixDisplay

import pyarrow.feather as feather
import pickle as pkl
import h5py
from tqdm import tqdm

R_J = 69911000
M_J = 1.898*1e27
R_s = 696340000
M_s = 1.989*1e30
R_J = 69911000
M_J = 1.898*1e27
M_E = 5.972*1e24
G_u = 6.674*1e-11
Na = 6.022*1e23
Kb = 1.380649*1e-23


def join_profiles(dataset):
    R = 8.31446261815324  # Gas constant

    new_Ts = []
    new_Ps = []
    new_rhos = []
    new_rs = []

    for ii in range(0, len(dataset)):
        # Extract necessary values from the dataset for calculations
        P_rem = np.array(dataset['/outputs/layers/pressure'].iloc[ii])
        T_rem = np.array(dataset['/outputs/layers/temperature'].iloc[ii])
        mu_rem = np.array(dataset['/outputs/layers/molar_mass'].iloc[ii])
        r_rem = np.array(dataset['/outputs/levels/altitude'].iloc[ii]) + dataset['Req'].iloc[ii]*1e-2

        P_ris = dataset['P'].iloc[ii]
        T_ris = dataset['T'].iloc[ii]
        rho_ris = np.array(dataset['rho'].iloc[ii])*1e3
        r_ris = np.array(dataset['R'].iloc[ii]) * 1e-2

        # Calculate new values for pressure, temperature, density, and altitude
        new_P = np.array(list(P_rem[np.where(P_rem*1e-5 < dataset['P_link'].iloc[ii])[0]])[::-1])
        new_T = np.array(list(T_rem[np.where(P_rem*1e-5 < dataset['P_link'].iloc[ii])[0]])[::-1])
        new_mu = np.array(list(mu_rem[np.where(P_rem*1e-5 < dataset['P_link'].iloc[ii])[0]])[::-1])
        new_rho = (new_P * new_mu) / (R * new_T)
        new_r = np.array(list(r_rem[np.where(P_rem*1e-5 < dataset['P_link'].iloc[ii])[0]])[::-1])

        # Combine the new values with the original values
        new_P = np.array(list(new_P*1e-5) + list(P_ris)[::-1])
        new_T = np.array(list(new_T) + list(T_ris)[::-1])
        new_rho = np.array(list(new_rho) + list(rho_ris)[::-1])
        new_r = np.array(list(new_r) + list(r_ris)[::-1])

        # Append the new values to respective lists
        new_Ps.append(new_P)
        new_Ts.append(new_T)
        new_rhos.append(new_rho)
        new_rs.append(new_r)

    # Add the new profiles as columns to the dataset
    dataset['P_profile'] = new_Ps
    dataset['T_profile'] = new_Ts
    dataset['rho_profile'] = new_rhos
    dataset['R_profile'] = new_rs

    # Return the updated dataset
    return dataset

def interpolate_to_regular_grid(df) :
    P_max = 1e9
    P_min = 1e-4
    
    P_range = np.logspace(np.log10(P_min),np.log10(P_max),50)
    new_T = []
    new_dT = []
    for ii in range(0,len(df)):
        T = interp1d(df['P_profile'].iloc[ii],df['T_profile'].iloc[ii],bounds_error=False,fill_value=(np.min(df['P_profile'].iloc[ii]), np.max(df['P_profile'].iloc[ii])))(P_range)
        new_T.append(T)
        new_dT.append(np.append(np.diff(T),0))
    
    df['T_adj'] = new_T
    df['dT_adj'] = new_dT
    df['P_adj'] = [P_range for ii in range(len(new_T))]
    
    return df

def plot_it() :
    model = pkl.load(open('../ML_model/sort_profiles.pkl', 'rb'))
    files = glob.glob('./test_clouds_*/*500.pkl')
    random.shuffle(files)
    print(len(files))
    M_J = 1.898*1e27
    for file in files :
        #file = files[random.randint(0,len(files))]
        with open(file, "rb") as f:
            df = pkl.load(f)
        if 'quality' not in df.columns :
            join_profiles(df)
            df = interpolate_to_regular_grid(df)
            df[[f"P_level_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.P_adj.apply(pd.Series)
            df[[f"T_value_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.T_adj.apply(pd.Series)
            df[[f"dT_value_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.dT_adj.apply(pd.Series)
            features = df[[x for x in df.columns if 'T_value' in x]+['T_int','T_irr','Met','mass','f_sed','S']]
            features = features.dropna(axis=1)
            features = np.array(features)
            y_pred = model.predict(features)
            df['predicted_quality'] = y_pred
            
            plt.plot(np.diff(df['T_profile'].iloc[0])+np.mean(df['T_profile'].iloc[0]),df['P_profile'].iloc[0][:-1])
            plt.title(str(df['mass']*1e-3/M_J))
            #plt.xscale('log')

            plt.plot(df['T_profile'].iloc[0],df['P_profile'].iloc[0])
            plt.gca().invert_yaxis()
            plt.title('{}_{}_{}'.format(df['mass'].iloc[0]*1e-3/M_J,df['warning_flag_max_pressure'].iloc[0],df['predicted_quality'].iloc[0]))
            
            plt.yscale('log')
            plt.xscale('log')
            plt.xlabel('Temperature (K)')
            plt.ylabel('Pressure (bar)')
            plt.show()
            print(file)
            quality = int(float(input('1 or 0')))
            if (quality == 0) or (quality == 1) :
                print('Quality {}'.format(quality))
                df['quality'] = quality
            df.to_pickle(file)
    return

def remove_end_link() :
    M_J = 1.898*1e27
    files = glob.glob('./test_clouds_5kk/*pkl')
    for file in files :
        with open(file, "rb") as f:
            df = pkl.load(f)
        if df['warning_flag_max_pressure'].iloc[0] :
            os.remove(file)   
            print(file)
    return


def random_forest():
    files = glob.glob('./test_clouds*/*500.pkl')
    
    df = []
    n_found = 0
    for file in tqdm(files) :
        with open(file, "rb") as f:
            df_temp = pkl.load(f)
        if 'quality' in df_temp.columns:
            n_found += 1
            print(n_found)
            df.append(df_temp)
        if n_found == 400 :
            break
    df = pd.concat(df)
    df = join_profiles(df)
    df = interpolate_to_regular_grid(df)
    
    #df = df.drop([x for x in df.columns if 'T_value' in x],axis=1)
    #df = df.drop([x for x in df.columns if 'P_level' in x],axis=1)
    
    df[[f"P_level_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.P_adj.apply(pd.Series)
    df[[f"T_value_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.T_adj.apply(pd.Series)
    df[[f"dT_value_{i}" for i in range(len(df.P_adj.iloc[0]))]] = df.dT_adj.apply(pd.Series)

    features = df[[x for x in df.columns if 'T_value' in x]+['T_int','T_irr','Met','mass','f_sed','S']]
    features = features.dropna(axis=1)
    labels = np.array(df['quality'])
    train_features, test_features, train_labels, test_labels = train_test_split(features, labels, test_size = 0.25)
    rf = RandomForestClassifier()
    rf.fit(train_features, train_labels)
    y_pred = rf.predict(test_features)
    accuracy = accuracy_score(test_labels, y_pred)
    
    print("Accuracy:", accuracy)
    disp = ConfusionMatrixDisplay.from_estimator(
        rf,
        test_features,
        test_labels,
        display_labels=['No','Yes'],
        cmap=plt.cm.Blues,
        normalize='all',
    )
    disp.ax_.set_title('Random forest profile classifier\nConfusion matrix')
    print(disp.confusion_matrix)
    plt.savefig('./../../Confusion_matrix.pdf')
    plt.show()
    
    final_model = RandomForestClassifier()
    final_model.fit(features, labels)
    pkl.dump(final_model, open('../ML_model/sort_profiles.pkl', 'wb'))
    
